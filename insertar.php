<?php
// LA ACCION PRINCIPAL DE ESTA PAGINA ES INSERTAR UNA NUEVA EMPRESA

$r = '';
//INCLUIR SESION Y CONECCION
require($r.'incluir/session.php');
require($r.'incluir/connection.php');

if(isset($_POST['insertar'])){ // VALIDAMOS QUE POST INSERTAR SEA TRUE
	$cedula = $_POST['cedula'];
	$nombre = strtoupper(trim($_POST['nombre'])); // CAMBIAMOS EL TEXTO RECIBIDO A MAYUSCULA
	$empresa = $_POST['empresa'];
	$extension = $_POST['extension'];
	$ubicacion = strtoupper(trim($_POST['ubicacion'])); // CAMBIAMOS EL TEXTO RECIBIDO A MAYUSCULA
	$area = $_POST['area'];
	$cargo = strtoupper(trim($_POST['cargo'])); // CAMBIAMOS EL TEXTO RECIBIDO A MAYUSCULA
	$email = strtolower(trim($_POST['email'])); // CAMBIAMOS EL TEXTO RECIBIDO A MAYUSCULA
	$celular = $_POST['celular'];
	

	$qrycan = $db->query("SELECT * FROM directorio WHERE dircedula = '".$cedula."';");
	$rowcan = $qrycan->fetch(PDO::FETCH_ASSOC);
	$numcan = $qrycan->rowCount();
	
	if( $numcan == 0){
		if(move_uploaded_file($_FILES['firma']['tmp_name'], $r.'imagenes/firmas/'.$cedula.'.png')){
			$qry = $db->query("INSERT INTO directorio (dircedula, dirempresa, dirnombre, dirextension, dirubicacion, dirarea, dircargo, diremail, dircelular) VALUES ('$cedula', '$empresa', '$nombre', '$extension', '$ubicacion', '$area', '$cargo', '$email', '$celular');
			"); // INSERTAMOS NUEVO USUARIO
			if($qry) {
				$mensaje = 'Se inserto el usuario';
				header('Location:insertar.php?mensaje='.$mensaje);
			} else {
				$error = 'No se inserto el usuario';
				header('Location:insertar.php?error='.$error);
			}
		} else {
			//$error = 'No se pudo cargar la firma';
			//header('Location:insertar.php?error='.$error);
			$qry = $db->query("INSERT INTO directorio (dircedula, dirempresa, dirnombre, dirextension, dirubicacion, dirarea, dircargo, diremail, dircelular) VALUES ('$cedula', '$empresa', '$nombre', '$extension', '$ubicacion', '$area', '$cargo', '$email', '$celular');
			"); // INSERTAMOS NUEVO USUARIO
			if($qry) {
				$mensaje = 'Se inserto el usuario, pero recuerde que es importante que adjunte la firma';
				header('Location:insertar.php?mensaje='.$mensaje);
			} else {
				$error = 'No se inserto el usuario';
				header('Location:insertar.php?error='.$error);
			}
		}	
	} else { 
		$error = 'Ya existe un usuario con este numero de ceula.';
		header('Location:insertar.php?error='.$error);
	}

}
$mensaje = $_GET['mensaje'];
$error = $_GET['error'];
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<!-- INSERTAR JQUERYS Y ESTILOS NECESARIAS PARA CORRECTO FUNCIONAMIENTO Y VISUALIZACION -->
	<?php 
		require($r . 'incluir/src/head.php');
		require($r . 'incluir/src/head-form.php');
	?>
<script type="text/javascript" src="<?php echo $r ?>incluir/funciones.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#fecha').datepicker({ //VALIDACION DE CAMPO FECHA
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		maxDate: "+0D",
	}).keypress(function(event) { event.preventDefault() });
	$('.btninsertar').button({ icons: { primary: 'ui-icon ui-icon-check' }});
	$('#form').validationEngine({ // VALIDACION DE CAMPOS REQUERIDOS
		showOneMessage:true,
		onValidationComplete: function(form, status){
        	if(status) {               
            	carga();
            	return true;
			}
		}
	});
	$('#dialog-message').dialog({ //CARACTERISTICAS DE LA VENTANA MODAL
		height: 80,
		width: 'auto',
		modal: true
	});
	
});
</script>
<!-- ESTILOS EMBEBIDOS PARA MANEJAR FORMULARIO INDEPENDIENTEMENTE -->
<style>
#form { width:780px; margin:20px auto }
#form fieldset { padding:10px; display:block }
#form legend { font-weight: bold; margin-left:5px; padding:5px }
#form label { display:inline-block; width:130px; text-align:right; margin:0.3em 2% 0 0 }
#form p { margin:5px 0 }
</style>
</head>
<body>
<?php require($r.'incluir/src/login.php') ?>  <!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
<section id="principal">
<?php require($r.'incluir/src/cabeza.php') ?> <!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
<?php require($r.'incluir/src/menu.php') ?> <!-- INCLUIMOS MENU PRINCIPAL -->
<article id="cuerpo">

<article id="contenido">
<form id="form" name="form" enctype="multipart/form-data" action="insertar.php" method="post"> <!-- FORMULARIO ENVIADO POR POST A SI MISMO -->
<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
<legend class="ui-widget ui-widget-header ui-corner-all">Insertar usuario en directorio</legend>
<p>
<label for="cedula">Cedula:</label> <!-- CAMPO CEDULA-->
<input type="text" name="cedula" style="width:140px" id="id" class="id validate[required, custom[onlyNumberSp]] text-input" />
</p>
<p>
<label for="nombre">Nombre:</label> <!-- CAMPO NOMBRE -->
<input type="text" id="nombre" name="nombre" class="nombre validate[required] text-input" title="Digite el nombre del usuario" />
</p>
<p>
<label for="empresa">Empresa de vinculacion:</label> <!-- CAMPO EMPRESA -->
<select name="empresa" class="validate[required]">
<option value="">SELECCIONE</option>
<?php
$qry = $db->query("SELECT * FROM empresas ORDER BY empnombre");
while($row = $qry->fetch(PDO::FETCH_ASSOC))
    echo '<option value='.$row['empid'].'>'.$row['empnombre'].'</option>';
?>
</select>
</p>
<p>
<label for="extension">Extension:</label> <!-- CAMPO EXTENSION-->
<input type="text" name="extension" style="width:140px" class="validate[required, custom[onlyNumberSp]] text-input" />
</p>
<p>
<label for="ubicacion">Ubicacion:</label> <!-- CAMPO UBICACION-->
<input type="text" name="ubicacion" class="direccion validate[required] text-input" />
</p>
<p>
<label for="area">Area:</label> <!-- CAMPO AREA -->
<select name="area" class="validate[required]">
<option value="">SELECCIONE</option>
<?php
$qry = $db->query("SELECT * FROM areas ORDER BY arenombre");
while($row = $qry->fetch(PDO::FETCH_ASSOC))
    echo '<option value='.$row['areid'].'>'.$row['arenombre'].'</option>';
?>
</select>
</p>
<p>
<label for="cargo">Cargo:</label> <!-- CAMPO CARGO-->
<input type="text" name="cargo" class="text-input" />
</p>
<p>
<label for="email">E-mail:</label> <!-- CAMPO EMAIL -->
<input type="text" name="email" class="email validate[custom[email]]" />
</p>
<p>
<label for="celular">Celular:</label>  <!-- CAMPO CELULAR -->
<input type="text" name="celular" class="celular validate[custom[phone]] text-input" />
</p>

<label for="Firma">Firma:</label> <!-- CAMPO FIRMA -->
<input type="file" id="firma" name="firma" class="validate[checkFileType[png|PNG]] text-input" />
</p>
</br>
<p class="boton">
<button type="submit" class="btn btn-primary btninsertar" name="insertar" value="insertar">Insertar</button> <!-- BOTON INSERTAR -->
<button type="button" class="btnatras" onClick="carga(); location.href='directorio.php'">Atras</button>
</p>
</fieldset>
</form>
</article>
</article>
<?php require($r.'incluir/src/pie.php') ?> <!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
</section>
<?php
if(isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>'.$error.'</div>'; //VENTANA MODAL ERROR
elseif(isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>'.$mensaje.'</div>'; //VENTANA MODAL EXITOSO
?>
</body>
</html>