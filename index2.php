<?php
   $r = '';
   require($r . 'incluir/session.php');
   require($r . 'incluir/connection.php');
?>
<!doctype html>
<html lang="es">

<head>
	
	<?php 
		require($r . 'incluir/src/head.php');
	?>
   <script>
      $(document).ready(function() {
         $("#menu").menu();
      });
   </script>


   <style>
      table {
         border-collapse: separate;
         border-spacing: 3px;
         color: #fff;
         border:none;
      }

      td,
      th {
         color: blue;
         font-size: 15px;
         border:none;
      }

      p {
         font-size: 18px;
         font-weight: bolder;
         color: blue;
      }

      .center-x{
         display: flex;
         justify-content: center;
      }
   </style>
</head>

<body>
   <?php require($r . 'incluir/src/login.php') ?>
   <section id="principal">
      <?php require($r . 'incluir/src/cabeza.php') ?>
      <?php require($r . 'incluir/src/menu.php') ?>
      <article id="cuerpo">
         <article class="mapa">
            <a class="current">Principal</a>
         </article>
         <article id="contenido">
            <div class="center-x">
               <?php
               $qrypermisos = $db->query("SELECT * FROM usuarios WHERE usuid = '" . $_SESSION['id']."'");
               $rowpermisos = $qrypermisos->fetch(PDO::FETCH_ASSOC);

               ?>
               <table cellpadding="0" cellspacing="0">
                  <td style="text-align:center;"><img src='incluir/img/motivacion1.png' width='40%'></td>
               </table>
               <?php
               ?>
            </div>
         </article>
      </article>
      <?php require($r . 'incluir/src/pie.php') ?>
   </section>
</body>

</html>