<?php
$r = '';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');

if(isset($_POST['modificar'])){ // VALIDAMOS QUE POST INSERTAR SEA TRUE
	$identificador = $_POST['identificador'];
	$cedula = $_POST['cedula'];
	$jefede = $_POST['jefe'];
	$nombre = strtoupper(trim($_POST['nombre'])); // CAMBIAMOS EL TEXTO RECIBIDO A MAYUSCULA
	$empresa = $_POST['empresa'];
	$extension = $_POST['extension'];
	$ubicacion = strtoupper(trim($_POST['ubicacion'])); // CAMBIAMOS EL TEXTO RECIBIDO A MAYUSCULA
	$area = $_POST['area'];
	$cargo = strtoupper(trim($_POST['cargo'])); // CAMBIAMOS EL TEXTO RECIBIDO A MAYUSCULA
	$email = strtolower(trim($_POST['email'])); // CAMBIAMOS EL TEXTO RECIBIDO A MAYUSCULA
	$celular = $_POST['celular'];
	

	$qrycan = $db->query("SELECT * FROM directorio WHERE dircedula = '".$cedula."';");
	$rowcan = $qrycan->fetch(PDO::FETCH_ASSOC);
	$numcan = $qrycan->rowCount();
	

	if (!empty($_FILES['firma']['tmp_name'])){
		if(move_uploaded_file($_FILES['firma']['tmp_name'], $r.'imagenes/firmas/'.$cedula.'.png')){
			$qry = $db->query("UPDATE directorio SET dircedula='$cedula', dirempresa='$empresa', dirnombre='$nombre', dirextension='$extension', dirubicacion='$ubicacion', dirarea='$area', dircargo='$cargo', diremail='$email', dircelular='$celular', dirjefede='$jefede' WHERE iddirectorio='$identificador';"); // MODIFICAR USUARIO
			if($qry) {
				$mensaje = 'Se modifico el usuario correctamente';
				header('Location:directorio.php?mensaje='.$mensaje);
			} else {
				$error = 'No se puedo modificar el usuario';
				header('Location:directorio.php?error='.$error);
			}
		} else {
			$error = 'No se pudo cargar la firma';
			header('Location:directorio.php?error='.$error);
		}	
	} else {
		$qry = $db->query("UPDATE directorio SET dircedula='$cedula', dirempresa='$empresa', dirnombre='$nombre', dirextension='$extension', dirubicacion='$ubicacion', dirarea='$area', dircargo='$cargo', diremail='$email', dircelular='$celular', dirjefede='$jefede'  WHERE iddirectorio='$identificador';	"); // MODIFICAR USUARIO
			if($qry) {
				$mensaje = 'Se modifico el usuario correctamente, recuerde la firma es muy importante anexarla.';
				header('Location:directorio.php?mensaje='.$mensaje);
			} else {
				$error = "No se pudo modificar el usuario ";
				header('Location:directorio.php?error='.$error);
			}
	}


}

$eliminar = $_GET['eliminar'];

if ($eliminar == 1){
	
	$id = $_GET['id'];
	
	$qryrevi = $db->query("SELECT * FROM directorio WHERE iddirectorio = '".$id."';");
	$rowrevi = $qryrevi->fetch(PDO::FETCH_ASSOC);

	$qryvali = $db->query("SELECT * FROM autorizaciones WHERE autotrabajador = '".$rowrevi['dircedula']."';");
	$numvali = $qryvali->rowCount();

	if($numvali == 0){
	$nombre = $_GET['nombre'];
	$qry = $db->query("DELETE FROM directorio WHERE iddirectorio = '".$id."';"); // DELETE DE USUARIO
	$num = $qry->rowCount();
	if ($num == 1){ 
		$mensaje = "Se ha eliminado exitosamente el usuario.";
		header('Location:directorio.php?mensaje='.$mensaje);
		//Log de informacion que se le envia a American Logistic 
			$qrylogsregister = $db->query("SELECT * FROM usuarios INNER JOIN perfiles ON usuperfil = perid WHERE usuid = ".$_SESSION['id']); //verificacion usuario por ID de sesion
			$rowlogsregister = $qrylogsregister->fetch(PDO::FETCH_ASSOC);	
			$qrylogsregister = $db->query("INSERT INTO logs ( usuario, id_usuario , ip_usuario , accion_usuario, fecha_accion) VALUES ( '".$rowlogsregister['usunombre']."', '".$_SESSION['id']."', '".$_SERVER['REMOTE_ADDR']."' , 'ELIMINO EL USUARIO CON ID $id Y NOMBRE $nombre DEL DIRECTORIO DE SEPIRO', '".date("Y-m-d H:i:s")."' );"); //anexo de informacion de accion a la BD tabla LOGS	
	} else {
		$error ="No se ha podido eliminar el usuario por favor contactese con el administrador";
		header('Location:directorio.php?error='.$error);
	} 
	} else {
		$error ="No se puede eliminar este usuario porque tiene gestiones en autorizaciones, <br>Contacte con el administrador para que lo deshabilite.";
		header('Location:directorio.php?error='.$error);
	}
}

$mensaje = $_GET['mensaje'];
$error = $_GET['error'];
?>
<!doctype html>
<html>
<head>
	<?php 
		require($r . 'incluir/src/head.php');
		require($r . 'incluir/src/head-form.php');
	?>	

<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
<script>
$(document).ready(function() {
	$("#menu").menu();
});

$(document).ready(function(){
	$('#tabla').dataTable({
		'bJQueryUI': true,
		'bStateSave': true,
		'sPaginationType': 'full_numbers',
		'oLanguage': {
			'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
		},
		'bSort': false,
	});
	$('.btnatras').button({ icons: { primary: 'ui-icon ui-icon-arrowthick-1-w' }});

	$('.msj_eliminar').click(function(e){
		e.preventDefault();
		var targetUrl = $(this).attr('href');
		var $dialog_link_follow_confirm = $('<div></div>').html("<p><span class='ui-icon ui-icon-alert' style='float:left; margin:2px 7px 7px 0;'></span>Esta seguro que desea eliminar esta persona del directorio?</p>").
		dialog({
			title: 'Porfavor confirmar',
			buttons :{
				'Si' : function() { 
					window.location.href = targetUrl; 
				},
				'No' : function() { 
					$(this).dialog("close"); 
				}
			},
			modal: true,
			width: 'auto',
			height: 'auto'
		}
	);
	$dialog_link_follow_confirm.dialog("open");
	});
	
	$('#dialog-message').dialog({
		height: 80,
		width: 'auto',
		modal: true
	});
	
});


</script>
</head>
<body>
<?php require($r.'incluir/src/login.php') ?>
<section id="principal">
<?php require($r.'incluir/src/cabeza.php') ?>
<?php require($r.'incluir/src/menu.php') ?>
<article id="cuerpo">
<article id="contenido">
<h2>Directorio Corporativo</h2>
<?php $qry = $db->query("SELECT * FROM usuarios LEFT JOIN areas ON areid = usuarea LEFT JOIN empresas ON empid = usuempresa WHERE usuperfil <> 56 order by usunombre;"); ?>
<table id="tabla">
<thead>
<tr>
<th title="Empresa"></th>
<th>Nombre</th>
<th>Extension</th>
<th>Ubicacion</th>
<th>Area</th>
<th>Cargo</th>
<th>Email</th>
<th>Celular</th>
</tr>
</thead>
<tbody>
<?php
while($row = $qry->fetch(PDO::FETCH_ASSOC)){

$qrypermisos = $db->query("SELECT * FROM usuarios INNER JOIN perfiles on perid = usuperfil WHERE usuid = ".$_SESSION['id'].";");
$rowpermisos = $qrypermisos->fetch(PDO::FETCH_ASSOC);

?>
<tr>
<td><img title="<?php echo $row['empnombre'] ?>" src="<?php echo $r ?>imagenes/iconos/empresa.png"/></td>
<td> <?php echo $row['usunombre'] ?></td>
<td align="center"><?php echo $row['usuextension'] ?></td>
<td align="center"> <?php echo $row['usudireccion']?> </td>
<td><?php echo $row['arenombre'] ?></td>
<td><?php echo $row['usucargo'] ?></td>
<td><?php echo $row['usuemail'] ?></td>
<td><?php echo $row['usutelefono'] ?></td>

<?php
}
?>
</tbody>
</table>

</article>
</article>
<?php require($r.'incluir/src/pie.php') ?>
</section>
<?php
if(isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>'.$error.'</div>';
elseif(isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>'.$mensaje.'</div>';
?>
<div id="modal" style="display:none"></div>
<div id="dialog2" style="display:none"></div>
</body>
</html>