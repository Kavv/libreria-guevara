<?php
$r = '';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

if (isset($_POST['modificar'])) { // VALIDAMOS QUE POST INSERTAR SEA TRUE
    $identificador = $_POST['identificador'];
    $cedula = $_POST['cedula'];
    $nombre = strtoupper(trim($_POST['nombre'])); // CAMBIAMOS EL TEXTO RECIBIDO A MAYUSCULA
    $empresa = $_POST['empresa'];
    $extension = $_POST['extension'];
    $ubicacion = strtoupper(trim($_POST['ubicacion'])); // CAMBIAMOS EL TEXTO RECIBIDO A MAYUSCULA
    $area = $_POST['area'];
    $cargo = strtoupper(trim($_POST['cargo'])); // CAMBIAMOS EL TEXTO RECIBIDO A MAYUSCULA
    $email = strtolower(trim($_POST['email'])); // CAMBIAMOS EL TEXTO RECIBIDO A MAYUSCULA
    $celular = $_POST['celular'];


    $qrycan = $db->query("SELECT * FROM directorio WHERE dircedula = '" . $cedula . "';");
    $rowcan = $qrycan->fetch(PDO::FETCH_ASSOC);
    $numcan = $qrycan->rowCount();


    if (!empty($_FILES['firma']['tmp_name'])) {
        if (move_uploaded_file($_FILES['firma']['tmp_name'], $r . 'imagenes/firmas/' . $cedula . '.png')) {
            $qry = $db->query("UPDATE directorio SET dircedula='$cedula', dirempresa='$empresa', dirnombre='$nombre', dirextension='$extension', dirubicacion='$ubicacion', dirarea='$area', dircargo='$cargo', diremail='$email', dircelular='$celular' WHERE iddirectorio='$identificador';"); // MODIFICAR USUARIO
            if ($qry) {
                $mensaje = 'Se modifico el usuario correctamente';
                header('Location:directorio.php?mensaje=' . $mensaje);
            } else {
                $error = 'No se puedo modificar el usuario';
                header('Location:directorio.php?error=' . $error);
            }
        } else {
            $error = 'No se pudo cargar la firma';
            header('Location:directorio.php?error=' . $error);
        }
    } else {
        $qry = $db->query("UPDATE directorio SET dircedula='$cedula', dirempresa='$empresa', dirnombre='$nombre', dirextension='$extension', dirubicacion='$ubicacion', dirarea='$area', dircargo='$cargo', diremail='$email', dircelular='$celular' WHERE iddirectorio='$identificador';	"); // MODIFICAR USUARIO
        if ($qry) {
            $mensaje = 'Se modifico el usuario correctamente, recuerde la firma es muy importante anexarla.';
            header('Location:directorio.php?mensaje=' . $mensaje);
        } else {
            $error = "No se pudo modificar el usuario ";
            header('Location:directorio.php?error=' . $error);
        }
    }


}

if (isset($_GET['mensaje'])) {
    $mensaje = $_GET['mensaje'];
}
if (isset($_GET['error'])) {
    $error = $_GET['error'];
}
?>
<!doctype html>
<html>
<head>
    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>

    <link rel="stylesheet" type="text/css"
          href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css"/>
    <script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
    <script>
        $(document).ready(function () {
            $("#menu").menu();
        });

        $(document).ready(function () {
            $('#tabla').dataTable({
                'bJQueryUI': true,
                'bStateSave': true,
                'sPaginationType': 'full_numbers',
                'oLanguage': {
                    'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
                },
                'bSort': false,
            });
            $('.btnatras').button({icons: {primary: 'ui-icon ui-icon-arrowthick-1-w'}});

            $('.msj_eliminar').click(function (e) {
                e.preventDefault();
                var targetUrl = $(this).attr('href');
                var $dialog_link_follow_confirm = $('<div></div>').html("<p><span class='ui-icon ui-icon-alert' style='float:left; margin:2px 7px 7px 0;'></span>Esta seguro que desea eliminar esta persona del directorio?</p>").dialog({
                        title: 'Porfavor confirmar',
                        buttons: {
                            'Si': function () {
                                window.location.href = targetUrl;
                            },
                            'No': function () {
                                $(this).dialog("close");
                            }
                        },
                        modal: true,
                        width: 'auto',
                        height: 'auto'
                    }
                );
                $dialog_link_follow_confirm.dialog("open");
            });

            $('#dialog-message').dialog({
                height: 80,
                width: 'auto',
                modal: true
            });

        });


    </script>
</head>
<body>
<?php require($r . 'incluir/src/login.php') ?>
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <?php require($r . 'incluir/src/menu.php') ?>
    <article id="cuerpo">
        <article id="contenido">
            <h2>PQRS Corporativo <?php echo date("d-m-Y") ?></h2>
            <div class="reporte">
                </a> <a href="insertarpqrs.php"><img src="imagenes/iconos/add.png" class="nota" title="Insertar PQRS">
                </a>
            </div>
            <?php $qry = $db->query("SELECT * FROM pqrs INNER JOIN directorio ON dircedula = pqrsimplicado WHERE pqrsimplicado = '" . $_SESSION['id'] . "' ORDER BY pqrsid desc; "); ?>
            <table id="tabla">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Fecha de Suceso</th>
                    <th>Tipo</th>
                    <th>Area</th>
                    <th>Implicado</th>
                    <th>Comentarios</th>
                    <th>Usuario</th>
                    <th title="Responder"></th>
                </tr>
                </thead>
                <tbody>
                <?php
                while ($row = $qry->fetch(PDO::FETCH_ASSOC)){

                $qrypermisos = $db->query("SELECT * FROM usuarios INNER JOIN perfiles on perid = usuperfil WHERE usuid = " . $_SESSION['id'] . ";");
                $rowpermisos = $qrypermisos->fetch(PDO::FETCH_ASSOC);

                $qryusuario = $db->query("SELECT * FROM directorio INNER JOIN usuarios on usuid = dircedula WHERE dircedula = " . $row['pqrsusuario'] . ";");
                $rowusuario = $qryusuario->fetch(PDO::FETCH_ASSOC);
                $usuario = $rowusuario['dirnombre'];

                if (empty($usuario)) {
                    $usuario = "NONE";
                }

                ?>
                <tr>
                    <td> <?php echo $row['pqrsid'] ?></td>
                    <td align="center"><?php echo $row['pqrsfechasuceso'] ?></td>
                    <td align="center"> <?php echo $row['pqrstipo'] ?> </td>
                    <td><?php echo $row['pqrsarea'] ?></td>
                    <td><?php echo $row['dirnombre'] ?></td>
                    <td><?php echo substr($row['pqrscomentarios'], 0, 30) . "..." ?></td>
                    <td><?php echo $usuario ?></td>
                    <td title="Responder"><a href="responderpqrs.php?resid=<?php echo $row['pqrsid']; ?>"
                                             title='Responder'><img
                                    src="<?php echo $r; ?>imagenes/iconos/email_go.png"/></a></td>
                    <?php
                    }
                    ?>
                </tbody>
            </table>

        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php') ?>
</section>
<?php
if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>';
elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>';
?>
<div id="modal" style="display:none"></div>
<div id="dialog2" style="display:none"></div>
</body>
</html>