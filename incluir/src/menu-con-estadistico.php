<?php include($r . 'incluir/emergentes.php'); ?>

<!-- CSS only -->
<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/css/menu.css"/>
<style>
    .active-drop:hover + .dropdown-menu {
        display: block !important;
    }

    .first-drop {
        top: 80% !important;
    }

    .dropdown-menu:hover {
        display: block !important;
    }

    .l-ninety {
        left: 90% !important;
    }

    .dropright {
        width: 100% !important;
    }

    #btn-menu-next {
        position: absolute;
        right: 10px;
        cursor: pointer;
        color: black;
    }

    #btn-menu-prev {
        position: absolute;
        right: 40px;
        cursor: pointer;
        color: black;
    }

    #btn-menu-next:hover, #btn-menu-prev:hover {
        color: #007bff;
    }
    
    .div-priority {
        height: 0;
        margin: .5rem 0;
        overflow: hidden;
        border-top: 2px solid #2d2d2d;
    }

</style>


<nav class="navbar navbar-expand-lg navbar-light bg-light">

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#nav-principal"
            aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="nav-principal">
        <ul class="navbar-nav">


            <?php if ($rowlog['peradmin'] == '1') { ?>
                <!-- PRIMER MODULO -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle active" data-dom="empresa" style="cursor:pointer">
                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/wrench.png"/>Administración
                    </a>
                    <div class="dropdown-menu first-drop">


                        <!-- 6 submodulo - Clientes -->
                        <?php if ($rowlog['peradmcli'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/user_red.png"/>
                                        Clientes
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/clientes/insertar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/clientes/consultar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>


                        <!-- 4 submodulo - Perfiles -->
                        <?php if ($rowlog['peradmper'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/group.png"/>
                                        Perfiles
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/perfiles/insertar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/perfiles/consultar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>


                        <!-- 5 submodulo - Usuarios -->
                        <?php if ($rowlog['peradmusu'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/usuario.png"/>
                                        Usuarios
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/usuarios/insertar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/usuarios/consultar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                        </a>

                                    </div>
                                </li>
                            </ul>
                            
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/usuario.png"/>
                                        Asesores
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/usuarios/agregar_asesor.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/usuarios/consultar_asesor.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                        </a>

                                    </div>
                                </li>
                            </ul>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/usuario.png"/>
                                        Cobrador
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/usuarios/agregar_cobrador.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/usuarios/consultar_cobrador.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                        </a>

                                    </div>
                                </li>
                            </ul>
                            
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/group.png"/>
                                        Grupos
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/grupos/insertar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/group_add.png"/>Insertar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/grupos/consultar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                        </a>

                                    </div>
                                </li>
                            </ul>
                        <?php } ?>

                        <!-- 11 submodulo - Medios de despacho -->
                        <?php if ($rowlog['peradmmde'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/lorry.png"/>
                                        Entregadores
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/mdespachos/insertar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/mdespachos/consultar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>


                        <!-- 7 submodulo - Proveedores -->
                        <?php if ($rowlog['peradmprv'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/user_gray.png"/>
                                        Proveedores
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/proveedores/insertar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/proveedores/consultar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>

                        <!-- 10 submodulo - Formas de pago -->
                        <?php if ($rowlog['peradmprv'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/creditcards.png"/>
                                        Formas de pago
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/fpagos/insertar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/fpagos/consultar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>
                        
                        <!-- 3 submodulo - E. geografica -->
                        <?php if ($rowlog['peradmege'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos//world.png"/>
                                        E. geografica
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <!-- Desde aquí son Sub Menus -->
                                        <ul class="navbar-nav">
                                            <li class="nav-item dropright">
                                                <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                                    <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/world.png"/>
                                                    Departamento
                                                </a>

                                                <div class="dropdown-menu l-ninety">
                                                    <a class="dropdown-item"
                                                       href="<?php echo $r ?>modulos/administracion/egeografica/deptos/insertar.php"
                                                       onclick="carga()">
                                                        <img class="pr-1"
                                                             src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar
                                                    </a>
                                                    <a class="dropdown-item"
                                                       href="<?php echo $r ?>modulos/administracion/egeografica/deptos/consultar.php"
                                                       onclick="carga()">
                                                        <img class="pr-1"
                                                             src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                                    </a>
                                                </div>
                                            </li>
                                        </ul>
                                        <ul class="navbar-nav">
                                            <li class="nav-item dropright">
                                                <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                                    <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/map.png"/>
                                                    Ciudad
                                                </a>

                                                <div class="dropdown-menu l-ninety">
                                                    <a class="dropdown-item"
                                                       href="<?php echo $r ?>modulos/administracion/egeografica/ciudades/insertar.php"
                                                       onclick="carga()">
                                                        <img class="pr-1"
                                                             src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar
                                                    </a>
                                                    <a class="dropdown-item"
                                                       href="<?php echo $r ?>modulos/administracion/egeografica/ciudades/consultar.php"
                                                       onclick="carga()">
                                                        <img class="pr-1"
                                                             src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                                    </a>
                                                </div>
                                            </li>
                                        </ul>
                                        <ul class="navbar-nav">
                                            <li class="nav-item dropright">
                                                <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                                    <img class="pr-1"
                                                         src="<?php echo $r ?>imagenes/iconos/empresa.png"/>
                                                    Sede
                                                </a>

                                                <div class="dropdown-menu l-ninety">
                                                    <a class="dropdown-item"
                                                       href="<?php echo $r ?>modulos/administracion/egeografica/sedes/insertar.php"
                                                       onclick="carga()">
                                                        <img class="pr-1"
                                                             src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar
                                                    </a>
                                                    <a class="dropdown-item"
                                                       href="<?php echo $r ?>modulos/administracion/egeografica/sedes/consultar.php"
                                                       onclick="carga()">
                                                        <img class="pr-1"
                                                             src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                                    </a>
                                                </div>
                                            </li>
                                        </ul>

                                    </div>
                                </li>
                            </ul>
                        <?php } ?>

                        <div class="div-priority"></div>

                        <!-- 1 submodulo - Parametros -->
                        <?php if ($rowlog['peradmpar'] == '1') { ?>
                            <a class="dropdown-item"
                               href="<?php echo $r ?>modulos/administracion/parametros/parametros.php"
                               onclick="carga()">
                                <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/cog.png"/>Parametros
                            </a>
                        <?php } ?>

                        <!-- 2 submodulo - Empresas -->
                        <?php if ($rowlog['peradmemp'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/empresa.png"/>
                                        Empresas
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/empresas/insertar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/empresas/consultar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>



                        <!-- 8 submodulo - Causales -->
                        <?php if ($rowlog['peradmege'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos//bullet_error.png"/>
                                        Causales
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <!-- Desde aquí son Sub Menus -->
                                        <ul class="navbar-nav">
                                            <li class="nav-item dropright">
                                                <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                                    <img class="pr-1"
                                                         src="<?php echo $r ?>imagenes/iconos/asterisk_orange.png"/>
                                                    Tipo de causal
                                                </a>

                                                <div class="dropdown-menu l-ninety">
                                                    <a class="dropdown-item"
                                                       href="<?php echo $r ?>modulos/administracion/causales/tcausales/insertar.php"
                                                       onclick="carga()">
                                                        <img class="pr-1"
                                                             src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar
                                                    </a>
                                                    <a class="dropdown-item"
                                                       href="<?php echo $r ?>modulos/administracion/causales/tcausales/consultar.php"
                                                       onclick="carga()">
                                                        <img class="pr-1"
                                                             src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                                    </a>
                                                </div>
                                            </li>
                                        </ul>
                                        <ul class="navbar-nav">
                                            <li class="nav-item dropright">
                                                <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                                    <img class="pr-1"
                                                         src="<?php echo $r ?>imagenes/iconos/asterisk_yellow.png"/>
                                                    Causal
                                                </a>

                                                <div class="dropdown-menu l-ninety">
                                                    <a class="dropdown-item"
                                                       href="<?php echo $r ?>modulos/administracion/causales/causales/insertar.php"
                                                       onclick="carga()">
                                                        <img class="pr-1"
                                                             src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar
                                                    </a>
                                                    <a class="dropdown-item"
                                                       href="<?php echo $r ?>modulos/administracion/causales/causales/consultar.php"
                                                       onclick="carga()">
                                                        <img class="pr-1"
                                                             src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                                    </a>
                                                </div>
                                            </li>
                                        </ul>

                                    </div>
                                </li>
                            </ul>
                        <?php } ?>



                        <!-- 12 submodulo - Mantenimiento -->
                        <?php if ($rowlog['peradmman'] == '1' or $rowlog['usuid'] == '1031128535' or $rowlog['usuperfil'] == 3) { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/cog.png"/>
                                        Mantenimiento
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/mantenimiento/pdfs.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/pdf.png"/>Cambio PDF
                                            de solicitudes
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/mantenimiento/cambiarid.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/vcard.png"/>Cambiar
                                            cedula de cliente
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/mantenimiento/confac.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/tab_edit.png"/>Regenerar
                                            factura
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/mantenimiento/camempresa.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/empresa.png"/>Cambio
                                            de Empresa en Solicitud
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>

                        <!-- 13 submodulo - Autorizaciones -->
                        <?php if ($rowlog['peradmaut'] == '1' or $rowlog['usuid'] == '80083610' or $rowlog['usuid'] == '77155629') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/flag_blue.png"/>
                                        Autorizaciones
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/autorizacion/aut_salida.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/flag_green.png"/>Aut.
                                            Salida
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/autorizacion/consultar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>


                        <!-- 14 submodulo - G. Documental -->
                        <?php if ($rowlog['peradmgesdoc'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/page_white_text.png"/>
                                        G. Documental
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/gdocumental/insertar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/page.png"/>Insertar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/gdocumental/consultar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/gdocumental/solicitar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/page_add.png"/>Solicitar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/gdocumental/lisprestamos.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/page_find.png"/>L.
                                            Solicitudes
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>

                        <!-- 15 submodulo - Papeleria y Aseo -->
                        <?php if ($rowlog['peradmgesdoc'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/page_white_text.png"/>
                                        Papeleria y Aseo
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/gdocumental/insertarproducto.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/page.png"/>Crear
                                            Producto
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/gdocumental/stock.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/page_add.png"/>Inventario
                                            PyA
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/gdocumental/papeyaseo.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/page.png"/>Insertar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/gdocumental/consultarpape.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/gdocumental/solicitarpape.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/page_add.png"/>Solicitar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/gdocumental/lisprestamospape.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/page_find.png"/>L.
                                            Solicitudes
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>

                        <!-- fin submodulos -->
                    </div>
                </li>

            <?php } ?>

            
            <!-- X Modulo - PUBLICO -->
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle active" data-dom="empresa" href="#">
                    <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/eye.png"/>Publico
                </a>
                <div class="dropdown-menu first-drop">
                    <!-- 1 submodulo - AGREGAR CONTRATO -->
                    <!-- Aqui se debe asignar un permiso para quienes trabajan con los contratos -->
                        
                    <a class="dropdown-item"
                        href="<?php echo $r ?>modulos/administracion/clientes/consultar.php"
                        onclick="carga()">
                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/user_red.png"/>Ver clientes
                    </a>
                        
                    <a class="dropdown-item"
                        href="<?php echo $r ?>modulos/principal/contrato/ver_contrato.php"
                        onclick="carga()">
                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/page.png"/>Ver Contratos
                    </a>

                    <a class="dropdown-item"
                        href="<?php echo $r ?>modulos/principal/recibos/seguimiento/rastrear_recibos.php"
                        onclick="carga()">
                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/page_find.png"/>Seguimiento de recibos
                    </a>
                </div>
            </li>
            
            <?php if ($rowlog['percontrato'] == '1') { ?>
                <!-- X Modulo - Contrato -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle active" data-dom="empresa" href="#">
                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/page.png"/>Contratos
                    </a>
                    <div class="dropdown-menu first-drop">
                        <!-- 1 submodulo - AGREGAR CONTRATO -->
                        <!-- Aqui se debe asignar un permiso para quienes trabajan con los contratos -->
                        <?php if ($rowlog['percontratoadd'] == '1') { ?>

                            <a class="dropdown-item"
                               href="<?php echo $r ?>modulos/principal/contrato/insertar.php"
                               onclick="carga()">
                                <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/page_add.png"/>
                                Agregar contrato
                            </a>
                            
                        <?php } ?>
                            
                        <a class="dropdown-item"
                            href="<?php echo $r ?>modulos/principal/contrato/consultar.php"
                            onclick="carga()">
                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/page_find.png"/>
                            Ver contratos
                        </a>
                        
                        <?php if ($rowlog['percontratoadd'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/usuario.png"/>
                                        Asesores
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/usuarios/agregar_asesor.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/usuarios/consultar_asesor.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                        </a>

                                    </div>
                                </li>
                            </ul>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/usuario.png"/>
                                        Cobrador
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/usuarios/agregar_cobrador.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/usuarios/consultar_cobrador.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                        </a>

                                    </div>
                                </li>
                            </ul>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/group.png"/>
                                        Grupos
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/grupos/insertar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/group_add.png"/>Insertar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/grupos/consultar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                        </a>

                                    </div>
                                </li>
                            </ul>
                        <?php } ?>
                            

                    </div>
                </li>
            <?php } ?>

            
            <?php if ($rowlog['perrecibo'] == '1') { ?>
                <!-- X Modulo - Recibos -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle active" data-dom="recibos" href="#">
                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/script.png"/>Recibos
                    </a>
                    <div class="dropdown-menu first-drop">
                        <!-- 1 submodulo - AGREGAR RECIBOS -->
                        <!-- Aqui se debe asignar un permiso para quienes trabajan con los contratos -->
                        <?php if ($rowlog['perpagadoadd'] == '1') { ?>

                            <a class="dropdown-item"
                               href="<?php echo $r ?>modulos/principal/recibos/insertar.php"
                               onclick="carga()">
                                <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/script_add.png"/>
                                Agregar Recibos Pagados
                            </a>
                        <?php } ?>
                        
                        <?php if ($rowlog['perpagadoedit'] == '1') { ?>
                            <a class="dropdown-item"
                               href="<?php echo $r ?>modulos/principal/recibos/editar.php"
                               onclick="carga()">
                                <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/script_edit.png"/>
                                Editar Recibos Pagados
                            </a>
                        <?php } ?>
                        
                        <?php if ($rowlog['percobrarseadd'] == '1') { ?>
                            <a class="dropdown-item" href="<?php echo $r ?>modulos/principal/recibos/recibos_a_cobrarse_insertar_v2.php" onclick="carga()">
                                <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/script_add.png"/>
                                Agregar Recibos a Cobrarse
                            </a>
                        <?php } ?>

                        <?php if ($rowlog['percobrarseedit'] == '1') { ?>
                            <a class="dropdown-item" href="<?php echo $r ?>modulos/principal/recibos/recibos_a_cobrarse_editar_v2.php" onclick="carga()">
                                <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/script_edit.png"/>
                                Editar Recibos a Cobrarse
                            </a>
                        <?php } ?>

                        <?php if ($rowlog['percobrarseadd'] == '1') { ?>
                            <a class="dropdown-item" href="<?php echo $r ?>modulos/principal/recibos/reportes/parametros_rac.php" onclick="carga()">
                                <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/printer.png"/>
                                Imprimir recibos a cobrarse
                            </a>
                            <a class="dropdown-item" href="<?php echo $r ?>modulos/principal/recibos/reportes/rac_consulta.php" onclick="carga()">
                                <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/page_white_magnify.png"/>
                                Consultar contratos a cobrarse
                            </a>
                            
                            <a class="dropdown-item"
                                href="<?php echo $r ?>modulos/principal/recibos/seguimiento/rastrear_recibos.php"
                                onclick="carga()">
                                <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/page_find.png"/>Seguimiento de recibos
                            </a>
                            <a class="dropdown-item"
                                href="<?php echo $r ?>modulos/principal/recibos/ajustes/ajustes.php"
                                onclick="carga()">
                                <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/cog.png"/>Ajuste de contrato
                            </a>
                        <?php } ?>
                        
                        
                    </div>
                </li>
            <?php } ?>

            
            <?php if ($rowlog['perbodega'] == '1') { ?>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle active" data-dom="empresa" href="#">
                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/box.png"/>Bodega
                    </a>
                    <div class="dropdown-menu first-drop">

                        <!-- 9 submodulo - Productos -->
                        <?php if ($rowlog['peradmprv'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/user_gray.png"/>
                                        Productos
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/productos/insertar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/productos/consultar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>

                        <!--DOCUMENTOS BODEGA-->
                        <?php if ($rowlog['perboddoc'] == '1' || $_SESSION['id'] == '1022949677' || $_SESSION['id'] == '1031128535') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/layout.png"/>
                                        Documentos
                                    </a>
                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/bodega/documentos/entrada/principal.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/layout.png"/>
                                            D. Entrada
                                        </a>
                                        
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/bodega/documentos/salida/principal.php"
                                           onclick="carga()">
                                           <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/layout_delete.png"/>
                                           D. Salida
                                        </a>

                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/bodega/documentos/masivo/establecer_en_cero.php"
                                           onclick="carga()">
                                           <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/layout_delete.png"/>
                                           D. Salida Masiva
                                        </a>

                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/bodega/documentos/consultar.php"
                                           onclick="carga()"><img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>
                                           Consulta
                                        </a>
                                    </div>
                                    
                                    <a class="dropdown-item" href="<?php echo $r ?>modulos/principal/contrato/consultar.php" onclick="carga()">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/page_find.png"/>
                                        Facturar
                                    </a>
                                </li>
                            </ul>
                        <?php } ?>
                        <!--FIN DOCUMENTOS BODEGA-->

                        <!--                        KARDE BODEGA-->
                        <?php if ($rowlog['perbodkar'] == '1' || $_SESSION['id'] == '1022949677') { ?>
                            <a class="dropdown-item" href="<?php echo $r ?>modulos/bodega/conkardex.php"
                               onclick="carga()">
                                <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/layers.png"/>Kardex
                            </a>
                        <?php } ?>

                        <div class="div-priority"></div>
                        <!--                        FIN KARDEX BODEGA-->

                        <!--                        DESPACHO BODEGAS-->
                        <?php if ($rowlog['perboddes'] == '1' || $_SESSION['id'] == '1022949677' || $_SESSION['id'] == '1031128535' || $_SESSION['id'] == '1030526237') { ?>

                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item"" href="#">
                                    <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/lorry.png"/>
                                    Despacho
                                    </a>
                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/bodega/lisdespacho.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/page_copy.png"/>
                                            Solicitudes a
                                            despachar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/bodega/bandespacho.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/folder_page.png"/>
                                            Bandeja
                                            de
                                            despacho</a>

                                    </div>
                                    </a>
                                </li>
                            </ul>
                        <?php } ?>
                        <!--                        FIN DESPACHO BODEGA-->

                        <!--                        PLANILLAS BODEGAS-->

                        <?php if ($rowlog['perbodpla'] == '1' || $_SESSION['id'] == '1022949677' || $_SESSION['id'] == '1031128535') { ?>

                            <a class="dropdown-item"
                               href="<?php echo $r ?>modulos/bodega/conplanilla.php"
                               onclick="carga()">
                                <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/table.png"/>
                                Planilla
                            </a>
                        <?php } ?>

                        <!--                        FIN PLANILLAS BODEGAS-->

                        <!--                        REPORTES BODEGA-->

                        <?php if ($rowlog['perbodrep'] == '1' || $_SESSION['id'] == '1022949677') { ?>

                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item"" href="#">
                                    <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/report.png"/>
                                    Reportes
                                    </a>
                                    <div class="dropdown-menu l-ninety">

                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/bodega/reportes/condescarga.php"
                                           onclick="carga()">
                                            <img class="pr-1"
                                                 src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>
                                            Entregados
                                        </a>

                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/bodega/reportes/coninventario.php"
                                           onclick="carga()">
                                            <img class="pr-1"
                                                 src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Inventario
                                        </a>

                                    </div>
                                    </a>
                                </li>
                            </ul>
                        <?php } ?>
                        <!--                        FIN REPORTES BODEGA-->
                    </div>
                </li>
            <?php } ?>
            
            <?php if ($can_see_consult || $can_see_report) { ?>
                <!-- X Modulo - Contrato -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle active" data-dom="empresa" href="#">
                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/report.png"/>Reportes
                    </a>
                    <div class="dropdown-menu first-drop">
                        <!-- 1 submodulo - AGREGAR CONTRATO -->
                        <!-- Aqui se debe asignar un permiso para quienes trabajan con los contratos -->
                        <?php if ($can_see_consult) { ?>

                            <a class="dropdown-item"
                               href="<?php echo $r ?>modulos/consultas/index.php"
                               onclick="carga()">
                                <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/application_view_list.png"/>
                                Consultas
                            </a>
                        <?php } ?>
                        <?php if ($can_see_report) { ?>
                            
                            <a class="dropdown-item"
                               href="<?php echo $r ?>modulos/informes/index.php"
                               onclick="carga()">
                                <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/book.png"/>
                                Informes
                            </a>
                            
                        <?php } ?>

                    </div>
                </li>
            <?php } ?>

            <?php if ($rowlog['persolicitud'] == '1') { ?>
                <!-- Tercer Modulo -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle active" data-dom="empresa" href="#">
                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/group.png"/>Solicitudes
                    </a>
                    <div class="dropdown-menu first-drop">

                        <!-- 1 submodulo - insertar S. Normal -->
                        <?php if ($rowlog['persolnor'] == '1' || $_SESSION['id'] == '20371048' || $_SESSION['id'] == '1110451657' || $_SESSION['id'] == '1032408225' || $_SESSION['id'] == '1018410939') { ?>
                            <a class="dropdown-item"
                               href="<?php echo $r ?>modulos/solicitudes/insertar.php?tipo=NORMAL"
                               onclick="carga()">
                                <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/script.png"/>Insertar
                                S. Normal
                            </a>
                        <?php } ?>
                        <!-- 2 Submodulo - Insertar S. Estimulacion -->
                        <?php if ($rowlog['persolest'] == '1' || $_SESSION['id'] == '1110451657' || $_SESSION['id'] == '1032408225' || $_SESSION['id'] == '1018410939') { ?>
                            <a class="dropdown-item"
                               href="<?php echo $r ?>modulos/solicitudes/insertar.php?tipo=ESTIMULACION"
                               onclick="carga()">
                                <img class="pr-1"
                                     src="<?php echo $r ?>imagenes/iconos/script_code_red.png"/>Insertar S.
                                Estimulacion
                            </a>
                        <?php } ?>

                        <!-- 3 submodulo - Anular -->
                        <?php if ($rowlog['persolanu'] == '1') { ?>
                            <a class="dropdown-item" href="<?php echo $r ?>modulos/solicitudes/anular.php"
                               onclick="carga()">
                                <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/script_delete.png"/>Anular
                            </a>
                        <?php } ?>

                        <!-- 4 submodulo - Consultar -->
                        <?php if ($rowlog['persolcon'] == '1' || $rowlog['usustaff'] == '1') { ?>
                            <a class="dropdown-item"
                               href="<?php echo $r ?>modulos/solicitudes/consultar.php"
                               onclick="carga()">
                                <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                            </a>
                        <?php } ?>

                        <!-- 5 submodulo - Screen -->
                        <?php if ($rowlog['persolscr'] == '1') { ?>
                            <a class="dropdown-item" href="<?php echo $r ?>modulos/solicitudes/screen.php"
                               onclick="carga()">
                                <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/layout_edit.png"/>Screen
                            </a>
                        <?php } ?>

                        <!-- 6 submodulo - Check Fisico -->
                        <?php if ($rowlog['persolche'] == '1' or $rowlog['usuid'] == '1077034420') { ?>
                            <a class="dropdown-item"
                               href="<?php echo $r ?>modulos/solicitudes/confisicos.php"
                               onclick="carga()">
                                <img class="pr-1"
                                     src="<?php echo $r ?>imagenes/iconos/application_view_list.png"/>Check
                                fisico
                            </a>
                        <?php } ?>

                        <!-- 7 submodulo - Soporte -->
                        <?php if ($rowlog['persolsopor'] == '1') { ?>
                            <a class="dropdown-item"
                               href="<?php echo $r ?>modulos/solicitudes/consoporte.php"
                               onclick="carga()">
                                <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/computer.png"/>Soportes
                            </a>
                        <?php } ?>


                        <!-- 8 submodulo - Reporte -->
                        <?php if ($rowlog['persolrepor'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/report.png"/>
                                        Reportes
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/solicitudes/consolgestiones.php"
                                           onclick="carga()">
                                            <img class="pr-1"
                                                 src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Notas
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/solicitudes/consolscreen.php"
                                           onclick="carga()">
                                            <img class="pr-1"
                                                 src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Screen
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>

                        <!-- 9 submodulo - Mis Solicitudes -->
                        <?php if ($rowlog['persolprop'] == '1') { ?>
                            <a class="dropdown-item"
                               href="<?php echo $r ?>modulos/solicitudes/consultar_prop.php"
                               onclick="carga()">
                                <img class="pr-1"
                                     src="<?php echo $r ?>imagenes/iconos/page_white_text.png"/>Mis
                                Solicitudes
                            </a>
                        <?php } ?>
                    </div>
                </li>

            <?php } ?>

            <?php if ($rowlog['perfactura'] == '1') { ?>
                <!-- Cuarto Modulo - FACTURACION -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle active" data-dom="empresa" href="#">
                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/note.png"/>Facturacion
                    </a>
                    <div class="dropdown-menu first-drop">

                        <!-- 1 submodulo - Facturar -->
                        <?php if ($rowlog['perfacfac'] == '1') { ?>
                            <a class="dropdown-item" href="<?php echo $r ?>modulos/facturacion/facturar.php"
                               onclick="carga()">
                                <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/note_add.png"/>Facturar
                            </a>
                        <?php } ?>
                        <!-- 2 Submodulo - Consultar -->
                        <?php if ($rowlog['perfaccon'] == '1') { ?>
                            <a class="dropdown-item" href="<?php echo $r ?>modulos/facturacion/consultar.php"
                               onclick="carga()">
                                <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                            </a>
                        <?php } ?>

                        <!-- 3 submodulo - Reporte -->
                        <?php if ($rowlog['perfacrep'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/report.png"/>
                                        Reportes
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/facturacion/reportes/conventas.php"
                                           onclick="carga()">
                                            <img class="pr-1"
                                                 src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Ventas
                                            detalladas
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/facturacion/reportes/conresumen.php"
                                           onclick="carga()">
                                            <img class="pr-1"
                                                 src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Resumen
                                            ventas
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/facturacion/reportes/conproducto.php"
                                           onclick="carga()">
                                            <img class="pr-1"
                                                 src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Ventas
                                            x producto
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/facturacion/reportes/excelproductos.php">
                                            <img class="pr-1"
                                                 src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Productos
                                            Solicitados
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>

                        <!-- 4 submodulo - Reportes Call -->
                        <?php if ($rowlog['perfacrepcall'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/report.png"/>
                                        Reportes Call
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/facturacion/reportes/concall.php"
                                           onclick="carga()">
                                            <img class="pr-1"
                                                 src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Detalle
                                            Factura
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>

                    </div>
                </li>
            <?php } ?>


            <!--FIN DE MODULO DE BODEGA-->
            <?php if ($rowlog['pertesoreria'] == '1') { ?>
                <!-- Sexto Modulo - Tesoreria -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle active" data-dom="empresa" href="#">
                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/money.png"/>Tesoreria
                    </a>
                    <div class="dropdown-menu first-drop">
                        <!-- 1 submodulo - Costos y Gastos -->
                        <?php if ($rowlog['pertesoreria'] == '1' or $_SESSION['id'] == '1024499151' or $_SESSION['id'] == '1030539847') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/money_add.png"/>
                                        Costos y Gastos
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/tesoreria/insertar_cyg.php" onclick="carga()">
                                            <img class="pr-1"
                                                 src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Insertar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/tesoreria/consultar_cyg.php">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/tesoreria/con_c_pago_cyg.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/money_add.png"/>Confirmar
                                            Pago
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>

                        <!-- 2 submodulo - Saldos de Banco -->
                        <?php if ($rowlog['pertesoreria'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/money_add.png"/>
                                        Saldos de Banco
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/tesoreria/con_cheques_sb.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/money_add.png"/>Validar
                                            Cheques
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/tesoreria/consultar_cheques_sb.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>

                        <!-- 3 submodulo - Nominas -->
                        <?php if ($rowlog['pertesoreria'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/usuario.png"/>
                                        Nominas
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/tesoreria/insernomina.php" onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/money_add.png"/>Insertar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/tesoreria/consultarnomina.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>

                        <!-- 4 submodulo - Aux Movilizacion -->
                        <?php if ($rowlog['pertesoreria'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/car.png"/>
                                        Aux Movilizacion
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/tesoreria/insertransporte.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/money_add.png"/>Insertar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/tesoreria/consultartransporte.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>

                        <!-- 5 submodulo - Doc. Equivalente -->
                        <?php if ($rowlog['pertesoreria'] == '1' or $_SESSION['id'] == '52440297') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/script.png"/>
                                        Doc. Equivalente
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/tesoreria/inserdocequi.php" onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/money_add.png"/>Insertar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/tesoreria/consultardocequi.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>

                        <!-- 6 submodulo - Proveedores -->
                        <?php if ($rowlog['pertesoreria'] == '1' or $_SESSION['id'] == '52440297') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/user_gray.png"/>
                                        Proveedores
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/tesoreria/proveedores/insertar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/tesoreria/proveedores/consultar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>

                        <!-- 7 submodulo - Bancos -->
                        <?php if ($rowlog['pertesoreria'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/empresa.png"/>
                                        Bancos
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/tesoreria/bancos/insertar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/tesoreria/bancos/consultar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>

                        <!-- 8 submodulo - Mensajeria -->
                        <?php if ($rowlog['pertesoreria'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/box.png"/>
                                        Mensajeria
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/tesoreria/insertarmensa.php" onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/tesoreria/consultarmensa.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>

                        <!-- 9 submodulo - Reportes -->
                        <?php //if ($rowlog['pertesoreria'] == '1') { ?>
                        <!-- <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/report.png" />
                                        Reportes
                                    </a>
                                    
                                    <div  class="dropdown-menu l-ninety">
                                        <a class="dropdown-item" href="<?php echo $r ?>modulos/tesoreria/con_planilla_diaria.php" onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png" />Planilla diaria de caja
                                        </a>
                                    </div>
                                </li>   
                            </ul> -->
                        <?php //} ?>
                    </div>
                </li>
            <?php } ?>



            <?php if ($rowlog['percartera'] == '1') { ?>
                <!-- Septimo Modulo - Cartera -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle active" data-dom="empresa" href="#">
                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/briefcase.png"/>Cartera
                    </a>
                    <div class="dropdown-menu first-drop">
                        <!-- 1 submodulo - Notas -->
                        <?php if ($rowlog['percarnot'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/layout.png"/>
                                        Notas
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/cartera/notas/NC/listar_cartera.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/layout_delete.png"/>Nota
                                            de Credito
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/cartera/notas/ND/listar_cartera.php">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/layout_add.png"/>Nota
                                            de Debito
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/cartera/notas/consultar.php" onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>

                        <!-- 2 submodulo - Consultar cartera -->
                        <?php if ($rowlog['percarcon'] == '1') { ?>
                            <a class="dropdown-item" href="<?php echo $r ?>modulos/cartera/consultar.php"
                               onclick="carga()">
                                <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar cartera
                            </a>
                        <?php } ?>

                        <!-- 3 submodulo - Promotores -->
                        <?php if ($rowlog['percarpro'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/user_suit.png"/>
                                        Promotores
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/cartera/promotores/conasignar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/arrow_in.png"/>Asignar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/cartera/promotores/conreasignar.php">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/arrow_inout.png"/>Reasignar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/cartera/promotores/condesasignar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/arrow_out.png"/>Desasignar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/cartera/promotores/traspaso.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/arrow_switch.png"/>Traspaso
                                            promotor
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>


                        <!-- 4 submodulo - Aplicar pago -->
                        <?php if ($rowlog['percarpag'] == '1') { ?>
                            <a class="dropdown-item"
                               href="<?php echo $r ?>modulos/cartera/pagos/individual/consultar.php" onclick="carga()">
                                <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/money.png"/>Aplicar Pago
                            </a>
                        <?php } ?>


                        <!-- 5 submodulo - Consulta Masiva -->
                        <?php //if ($rowlog['percarmas'] == '1' || $_SESSION['id'] == '1077034420') { ?>
                        <!-- <a class="dropdown-item" href="<?php echo $r ?>modulos/cartera/consulta_masiva.php" onclick="carga()">
                                <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consulta Masiva
                            </a> -->
                        <?php //} ?>

                        <!-- 6 submodulo - Cambiar fecha -->
                        <?php if ($rowlog['percarfec'] == '1') { ?>
                            <a class="dropdown-item" href="<?php echo $r ?>modulos/cartera/fecha/consultar.php"
                               onclick="carga()">
                                <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/calendar.png"/>Cambiar fecha
                            </a>
                        <?php } ?>

                        <!-- 7 submodulo - Clientes Nuevos -->
                        <!-- ORIGINALMENTE NO TIENE PERMISO -->
                        <!-- IMPORTANTE AJUSTAR -->
                        <a class="dropdown-item" href="<?php echo $r ?>modulos/cartera/clinuevos.php" onclick="carga()">
                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Cuotas
                        </a>

                        <!-- 8 submodulo - Cartera castigadas -->
                        <?php if ($rowlog['percarcas'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/punish.png"/>
                                        Castigada
                                    </a>
                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/cartera/castigada/consultar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/layout_delete.png"/>A
                                            Castigar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/cartera/castigada/concastigada.php">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/layout_add.png"/>Castigadas
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/cartera/castigada/conrecuperar.php">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/layout_add.png"/>A
                                            Recuperar
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>


                        <!-- 9 submodulo - Datacredito -->
                        <?php //if ($rowlog['percardat'] == '1') { ?>
                        <!-- <ul class="navbar-nav">
							<li class="nav-item dropright">
								<a class="dropdown-toggle active-drop dropdown-item" href="#">
									<img class="pr-1" src="<?php echo $r ?>imagenes/iconos/datacredito.png" />
									Datacredito
								</a>
								
								<div class="dropdown-menu l-ninety">
									<ul class="navbar-nav">
										<li class="nav-item dropright">
											<a class="dropdown-toggle active-drop dropdown-item" href="#">
												<img class="pr-1" src="<?php echo $r ?>imagenes/iconos/text_padding_top.png" />
												Generar plano
											</a>
											
											<div  class="dropdown-menu l-ninety">
												<a class="dropdown-item" href="<?php echo $r ?>modulos/cartera/datacredito/principal1.php" onclick="carga()">
													<img class="pr-1" src="<?php echo $r ?>imagenes/iconos/empresa.png" />IdenCorp
												</a>
												<a class="dropdown-item" href="<?php echo $r ?>modulos/cartera/datacredito/principal2.php" onclick="carga()">
													<img class="pr-1" src="<?php echo $r ?>imagenes/iconos/empresa.png" />Coopincorp
												</a>
											</div>
										</li>
									</ul>
									<a class="dropdown-item" href="<?php echo $r ?>modulos/cartera/datacredito/consultar.php" onclick="carga()">
										<img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png" />Consulta cuenta
									</a>
									
								</div>
							</li>
						</ul> -->
                        <?php //} ?>


                        <!-- 10 submodulo - Reportes -->
                        <?php if ($rowlog['percarrep'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/report.png"/>
                                        Reportes
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/cartera/reportes/conresumen.php"
                                           onclick="carga()">
                                            <img class="pr-1"
                                                 src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Resumen
                                            cartera
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/cartera/reportes/lisresmoro.php"
                                           onclick="carga()">
                                            <img class="pr-1"
                                                 src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Resumen
                                            morosidad
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/cartera/reportes/condetmoro.php"
                                           onclick="carga()">
                                            <img class="pr-1"
                                                 src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Detalle
                                            morosidad
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/cartera/reportes/congestion.php"
                                           onclick="carga()">
                                            <img class="pr-1"
                                                 src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Gestion
                                            Diaria
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/cartera/reportes/conpagocontado.php"
                                           onclick="carga()">
                                            <img class="pr-1"
                                                 src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Clientes
                                            Contado
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/cartera/reportes/concalificacion.php"
                                           onclick="carga()">
                                            <img class="pr-1"
                                                 src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Califi.
                                            Producto
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/cartera/reportes/congencartera.php"
                                           onclick="carga()">
                                            <img class="pr-1"
                                                 src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Generar
                                            Cartera
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/cartera/reportes/convamos1.php"
                                           onclick="carga()">
                                            <img class="pr-1"
                                                 src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Como
                                            Vamos
                                        </a>
                                        <a class="dropdown-item" href="<?php echo $r ?>modulos/cartera/deterioro.php"
                                           onclick="carga()">
                                            <img class="pr-1"
                                                 src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Deterioro
                                            390
                                        </a>
                                        <a class="dropdown-item" href="<?php echo $r ?>modulos/cartera/deterioroFin.php"
                                           onclick="carga()">
                                            <img class="pr-1"
                                                 src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Deterioro
                                            final
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/cartera/Reportes/Consolidado.php"
                                           onclick="carga()">
                                            <img class="pr-1"
                                                 src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Consolidado
                                        </a>

                                    </div>
                                </li>
                            </ul>
                        <?php } ?>


                        <!-- 11 submodulo - Paz y Salvo -->
                        <?php if ($rowlog['percarpaz'] == '1' || $_SESSION['id'] == '1031128535') { ?>
                            <a class="dropdown-item" href="<?php echo $r ?>modulos/cartera/certificados/consultar.php"
                               onclick="carga()">
                                <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/pdf.png"/>Paz y Salvo
                            </a>
                        <?php } ?>
                    </div>
                </li>
            <?php } ?>




            <?php if ($rowlog['percaja'] == '1') { ?>
                <!-- Octavo Modulo - Caja -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle active" data-dom="empresa" href="#">
                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/coins.png"/>Caja
                    </a>
                    <div class="dropdown-menu first-drop">
                        <!-- 1 submodulo - Consultar Recibos -->
                        <a class="dropdown-item" href="<?php echo $r ?>modulos/caja/consultar.php" onclick="carga()">
                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar recibo
                        </a>

                        <!-- 2 submodulo - Reportes -->
                        <ul class="navbar-nav">
                            <li class="nav-item dropright">
                                <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                    <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/report.png"/>
                                    Reportes
                                </a>

                                <div class="dropdown-menu l-ninety">
                                    <a class="dropdown-item"
                                       href="<?php echo $r ?>modulos/caja/reportes/conmediosmagneticos.php"
                                       onclick="carga()">
                                        <img class="pr-1"
                                             src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>M.
                                        Magneticos 1
                                    </a>
                                    <a class="dropdown-item"
                                       href="<?php echo $r ?>modulos/caja/reportes/conmediosmagneticos_1.php">
                                        <img class="pr-1"
                                             src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>M.
                                        Magneticos 2
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </li>
            <?php } ?>



            <?php if ($rowlog['percomision'] == '1' || $rowlog['usustaff'] == '1') { ?>
                <!-- Noveno Modulo - Comisiones -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle active" data-dom="empresa" href="#">
                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/valor.png"/>Comisiones
                    </a>
                    <div class="dropdown-menu first-drop">
                        <!-- 1 submodulo - C.Normal -->
                        <?php if ($rowlog['percominormal'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/script.png"/>
                                        C. Normal
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/comisiones/normal/asesor.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/database_gear.png"/>Asesor
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/comisiones/normal/director.php">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/database_gear.png"/>Director
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/comisiones/normal/gcomercial.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/database_gear.png"/>G.
                                            comercial
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/comisiones/normal/ggeneral.php">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/database_gear.png"/>G.
                                            general
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/comisiones/normal/adicional.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/database_gear.png"/>Adicional
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/comisiones/normal/relacionista.php">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/database_gear.png"/>Relacionista
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>
                        <!-- 2 submodulo - C.Libranza -->
                        <?php // if ($rowlog['percomilibranza'] == '1') { ?>
                        <!-- <ul class="navbar-nav">
                            <li class="nav-item dropright">
                                <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                    <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/script_code.png" />
                                    C. Libranza
                                </a>
                                
                                <div  class="dropdown-menu l-ninety">
                                    <a class="dropdown-item" href="<?php echo $r ?>modulos/comisiones/normal/asesor.php" onclick="carga()">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/database_gear.png" />Asesor
                                    </a>
                                </div>
                            </li>
                        </ul> -->
                        <?php // } ?>

                        <!-- 3 submodulo - C.Estimulacion -->
                        <?php // if ($rowlog['percomiestimulacion'] == '1') { ?>
                        <!-- <ul class="navbar-nav">
							<li class="nav-item dropright">
								<a class="dropdown-toggle active-drop dropdown-item" href="#">
									<img class="pr-1" src="<?php echo $r ?>imagenes/iconos/script_code_red.png" />
									C. Estimulacion
								</a>
								
								<div  class="dropdown-menu l-ninety">
									<a class="dropdown-item" href="<?php echo $r ?>modulos/comisiones/normal/asesor.php" onclick="carga()">
										<img class="pr-1" src="<?php echo $r ?>imagenes/iconos/database_gear.png" />Asesor
									</a>
								</div>
							</li>
                        </ul> -->
                        <?php // } ?>


                        <!-- 4 submodulo - Mi Comision -->
                        <?php if ($rowlog['percomiprop'] == '1' || $rowlog['usustaff'] == '1' || $_SESSION['id'] == '20371048') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos//money.png"/>
                                        Mi Comision
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <!-- Desde aquí son Sub Menus -->
                                        <ul class="navbar-nav">
                                            <li class="nav-item dropright">
                                                <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                                    <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/script.png"/>
                                                    C. Normal
                                                </a>

                                                <div class="dropdown-menu l-ninety">
                                                    <a class="dropdown-item"
                                                       href="<?php echo $r ?>modulos/comisiones/normal/asesor_prop.php"
                                                       onclick="carga()">
                                                        <img class="pr-1"
                                                             src="<?php echo $r ?>imagenes/iconos/database_gear.png"/>Asesor
                                                    </a>
                                                    <a class="dropdown-item"
                                                       href="<?php echo $r ?>modulos/comisiones/normal/relaci_prop.php"
                                                       onclick="carga()">
                                                        <img class="pr-1"
                                                             src="<?php echo $r ?>imagenes/iconos/database_gear.png"/>Relacionista
                                                    </a>
                                                    <a class="dropdown-item"
                                                       href="<?php echo $r ?>modulos/comisiones/normal/adicio_prop.php"
                                                       onclick="carga()">
                                                        <img class="pr-1"
                                                             src="<?php echo $r ?>imagenes/iconos/database_gear.png"/>Adicional
                                                    </a>
                                                </div>
                                            </li>
                                        </ul>
                                        <!-- <ul class="navbar-nav">
                                            <li class="nav-item dropright">
                                                <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                                    <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/script_code.png"/>
                                                    C. Libranza
                                                </a>

                                                <div class="dropdown-menu l-ninety">
                                                </div>
                                            </li>
                                        </ul>
                                        <ul class="navbar-nav">
                                            <li class="nav-item dropright">
                                                <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                                    <img class="pr-1"
                                                         src="<?php echo $r ?>imagenes/iconos/script_code_red.png"/>
                                                    C. Estimulacion
                                                </a>

                                                <div class="dropdown-menu l-ninety">
                                                </div>
                                            </li>
                                        </ul> -->

                                    </div>
                                </li>
                            </ul>
                        <?php } ?>
                    </div>
                </li>
            <?php } ?>

            <?php if ($rowlog['perestadistico'] == '1' ) { ?>
                <!-- Noveno Modulo - Estadisticos -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle active" data-dom="empresa" href="#">
                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/chart_bar.png"/>Estadisticos
                    </a>
                    <div class="dropdown-menu first-drop">
                        <!-- 1 submodulo - Ventas -->
                        <?php if ($rowlog['perestavent'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/calculator.png"/>
                                        Ventas
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/estadisticos/ventas/condetven.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/usuario.png"/>Detallado
                                            x asesor
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/estadisticos/ventas/condetvenrela.php">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/usuario.png"/>Detallado
                                            x Rela
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/estadisticos/ventas/condetgru.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/group.png"/>Detallado
                                            x grupo
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/estadisticos/ventas/conresve.php">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/usuario.png"/>Resumen
                                            asesores
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/estadisticos/ventas/conresvenrela.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/usuario.png"/>Resumen
                                            Relacioni
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/estadisticos/ventas/conresgru.php">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/group.png"/>Resumen
                                            grupos
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/estadisticos/ventas/conrespro.php">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/cd.png"/>Resumen
                                            Productos
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>

                        <!-- 2 submodulo - Reportes -->
                        <?php if ($rowlog['perestarepor'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/calculator.png"/>
                                        Reportes
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/estadisticos/reportes/conscreen.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/group.png"/>Screen
                                            Asesores
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/estadisticos/reportes/conestado.php">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/report.png"/>E.
                                            Pedidos
                                        </a>
                                    </div>
                                </li>


                            </ul>
                        <?php } ?>
                    </div>
                </li>
            <?php } ?>


        </ul>
        <div id="btn-menu-prev">
            <svg width="25px" height="25px" viewBox="0 0 16 16" class="bi bi-arrow-left-circle-fill" fill="currentColor"
                 xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd"
                      d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-4.5.5a.5.5 0 0 0 0-1H5.707l2.147-2.146a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708-.708L5.707 8.5H11.5z"/>
            </svg>
        </div>

        <div id="btn-menu-next">
            <svg width="25px" height="25px" viewBox="0 0 16 16" class="bi bi-arrow-right-circle-fill"
                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd"
                      d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-11.5.5a.5.5 0 0 1 0-1h5.793L8.146 5.354a.5.5 0 1 1 .708-.708l3 3a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708-.708L10.293 8.5H4.5z"/>
            </svg>
        </div>
    </div>
</nav>

<script>
    /* var temp;
    $(".active-drop").hover(function(){
        var obj = this.dataset.dom;
        $("#"+obj).dropdown('toggle');
    });

    $(".active-drop").blur(function(){
        var obj = this.dataset.dom;
        $("#"+obj).dropdown('hide')
    });
    function active_drop(obj)
    {
        $("#"+obj).dropdown('show')
    } */
</script>

<script>
    var menu_options = $("li.nav-item.dropdown").length;
    var limit_show = 6;
    if (menu_options > 6) {
        for (i = limit_show; i < menu_options; i++)
            $("li.nav-item.dropdown").eq(i).css("display", "none");
    } else {
        $("#btn-menu-next").css("display", "none");
        $("#btn-menu-prev").css("display", "none");
    }
    var limit_hide = 7;
    var hide = <?php
        if (isset($_SESSION["active_option_menu"])) {
            echo $_SESSION["active_option_menu"];
        } else {
            echo 0;
        }
        ?>

        function actual_menu() {
            for (i = 0; i < hide; i++) {
                $("li.nav-item.dropdown").eq(i).css("display", "none");
                var show = limit_show + i;
                $("li.nav-item.dropdown").eq(show).css("display", "block");
            }
        }

    if (hide > 0)
        actual_menu();


    $("#btn-menu-next").click(function () {
        if (hide < limit_hide) {
            $("li.nav-item.dropdown").eq(hide).css("display", "none");
            var show = limit_show + hide;
            $("li.nav-item.dropdown").eq(show).css("display", "block");
            hide++;
            $.get('<?php echo $r ?>incluir/src/index_menu.php?index=' + hide);
        }
    });
    $("#btn-menu-prev").click(function () {
        if (hide > 0) {
            hide--;
            $("li.nav-item.dropdown").eq(hide).css("display", "block");
            var show = limit_show + hide;
            $("li.nav-item.dropdown").eq(show).css("display", "none");
            $.get('<?php echo $r ?>incluir/src/index_menu.php?index=' + hide);
        }
    });

    // Controla la opcion activa
    var actual = null;
    // Controla si se dio click en una opcion del nav
    var click_nav = false;
    $(".nav-link").click(function () {
        // Si actual ya posee un elemento, se oculpa para mostrar el nuevo
        if (actual != null)
            actual.css('display', 'none');

        // Asignamos el elemento .first-drop correspondiente
        actual = $(this).siblings(".first-drop");
        // mostramos las opciones del menu
        actual.css('display', 'block');

        click_nav = true;

        // Siempre que se de click en un elemento del menu debemos hacer unbind del la funcion click del body
        $("body").unbind("click");

        $("body").click(function () {
            if ($("body").hasClass("cerrar-menu") && !click_nav) {
                actual.css('display', 'none');
                $("body").removeClass("cerrar-menu");
                $("body").unbind("click");
            } else {
                $("body").addClass("cerrar-menu");
                click_nav = false;
            }
        });
    });

</script>
