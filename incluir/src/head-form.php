
    
	<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/validation/css/validationEngine.jquery.css" />
	<script type="text/javascript" src="<?php echo $r ?>incluir/validation/js/languages/jquery.validationEngine-es.js"></script>
	<script type="text/javascript" src="<?php echo $r ?>incluir/validation/js/jquery.validationEngine.js"></script>
    <script type="text/javascript" src="<?php echo $r ?>incluir/funciones.js"></script>

	<script type="text/javascript">
		var show_carga_modal = true;
		$(document).ready(function() {
			
			$('#form').validationEngine({
				showOneMessage: true,
				onValidationComplete: function(form, status) {
					if (status) {
						if(show_carga_modal)
						carga();
						return true;
					}
				}
			});
			$('#dialog-message').dialog({
				height: 'auto',
				width: 'auto',
				modal: true
			});
			
			$(".fecha").prop("autocomplete", "off");
		});
		
		function validation_file(url)
		{
			var http = new XMLHttpRequest();
			http.open('HEAD', url, false);
			http.send();
			return http.status;
		}

	</script>