
    <div id="loading" class="position-fixed " style="display:none; z-index: 1100;left: 0;background-color: #0000006b;height:100%;right: 0;top: 0;bottom: 0;">
        <div class="d-flex justify-content-center align-items-center" style="height: 100%;">
            <div class="col-lg-10 col-xs-2 text-center">
                <label style="color: #d4d4d4;font-size: 30px!important;font-family: fantasy;background: #000000a6;border-radius: 50px;padding-left: 20px;padding-right: 20px; padding-top:5px; padding-bottom:5px;">
                Este proceso puede demorar varios segundos <br>No recargue la página <br>Espere pacientemente
                </label>
                <div class="progress" style="width:100%;">
                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                </div>
            </div>
        </div>
    </div>