<?php
$qrylog = $db->query("SELECT * FROM usuarios INNER JOIN perfiles ON usuperfil = perid WHERE usuid = '".$_SESSION['id']."'");
$rowlog = $qrylog->fetch(PDO::FETCH_ASSOC);

// Puede ver consultas?
$can_see_consult = $db->query("SELECT cdiid FROM perfil_reporte INNER JOIN consultasdinamicas ON reporte = cdiid WHERE perfil = ".$rowlog['perid']." AND tipo = 1")->fetch(PDO::FETCH_ASSOC);

// Puede ver informes?
$can_see_report = $db->query("SELECT idiid FROM perfil_reporte INNER JOIN informesdinamicos ON reporte = idiid WHERE hide is false AND perfil = ".$rowlog['perid']." AND tipo = 2")->fetch(PDO::FETCH_ASSOC);

?>

<script>
function carga(){
	$('#carga').dialog('open');
}
$(document).ready(function(){
	$(document).keydown(function(e){
		var code = (e.keyCode ? e.keyCode : e.which);
		if(code == 116) {
			e.preventDefault();
		}
	});
	jClock = function(jDate, jHora, jMin, jSec) { $("#hora-servidor").html(jDate + ', ' + jHora + ':' + jMin + ':' + jSec); }
	var jDate = '<?=date('d/m/Y') ?>';
	var jHora = '<?=date('H') ?>';
	var jMin = '<?=date('i') ?>';
	var jSec = '<?=date('s') ?>';
	jClock(jDate, jHora,jMin,jSec);
	var jClockInterval = setInterval(function(){
		jSec++;
		if(jSec >= 60){
			jMin++;
			if(jMin >= 60){
				jHora++;
				if(jHora > 23){
					jHora = '00';
				}else if (jHora < 10){
					jHora = '0'+jHora;
				}
				jMin = '00';
			} else if(jMin < 10){
				jMin = '0'+jMin;
			}
 			jSec = '00';
		}else if(jSec < 10){
			jSec = '0'+jSec;
		}
 		jClock(jDate, jHora,jMin,jSec);
	}, 1000);
	$('#carga').dialog({
		autoOpen:false,
		modal:true,
		draggable: false,
		resizable:false
	}).parent('.ui-dialog').find('.ui-dialog-titlebar-close').remove();
	$(".msj_sesion").click(function(e){
		e.preventDefault();
		var targetUrl = $(this).attr("href");
		var $dialog_link_follow_confirm = $('<div></div>').html("<p><span class='ui-icon ui-icon-person' style='float:left; margin:2px 7px 7px 0;'></span>Esta seguro que desea salir de la sesion?</p>").
		dialog({
			title: 'Porfavor confirmar',
			buttons :{
				"Si" : function() { 
					window.location.href = targetUrl; 
				},
				"No" : function() { 
					$(this).dialog("close"); 
				}
			},
			modal: true,
			width: 'auto',
			height: "auto"
		});
		$dialog_link_follow_confirm.dialog("open");
	});
});
</script>
<div id="carga" title="Cargando..." align="center">
<p><br /><img src="<?php echo $r ?>imagenes/loader.gif" style="height:60px; width:60px" /></p>
</div>
<section id="toppanel">
	<div class="tab">
	<ul class="login">
	<li class="left">&nbsp;</li>
	<li title="usuario"><?php echo $rowlog['usunombre'] ?></li>
	<li class="sep">|</li>
	<li title="fecha y hora"><div id="hora-servidor"></div></li>
	<li class="sep">|</li>
    <li><a id="close" class="close msj_sesion" href="<?php echo $r ?>incluir/logout.php" title="cerrar sesion"></a></li>
	<li class="right">&nbsp;</li>
	</ul> 
	</div>
</section>