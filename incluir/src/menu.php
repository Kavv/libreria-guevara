<?php include($r . 'incluir/emergentes.php'); ?>

<!-- CSS only -->
<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/css/menu.css"/>
<style>
    .active-drop:hover + .dropdown-menu {
        display: block !important;
    }

    .first-drop {
        top: 80% !important;
    }

    .dropdown-menu:hover {
        display: block !important;
    }

    .l-ninety {
        left: 90% !important;
    }

    .dropright {
        width: 100% !important;
    }

    #btn-menu-next {
        position: absolute;
        right: 10px;
        cursor: pointer;
        color: black;
    }

    #btn-menu-prev {
        position: absolute;
        right: 40px;
        cursor: pointer;
        color: black;
    }

    #btn-menu-next:hover, #btn-menu-prev:hover {
        color: #007bff;
    }
    
    .div-priority {
        height: 0;
        margin: .5rem 0;
        overflow: hidden;
        border-top: 2px solid #2d2d2d;
    }

</style>


<nav class="navbar navbar-expand-lg navbar-light bg-light">

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#nav-principal"
            aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="nav-principal">
        <ul class="navbar-nav">


            <?php if ($rowlog['peradmin'] == '1') { ?>
                <!-- PRIMER MODULO -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle active" data-dom="empresa" style="cursor:pointer">
                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/wrench.png"/>Administración
                    </a>
                    <div class="dropdown-menu first-drop">


                        <?php if ($rowlog['peradmper'] == '1') { ?>
                            <a class="dropdown-item"
                               href="<?php echo $r ?>modulos/administracion/auditoria/ver_logs.php"
                               onclick="carga()">
                                <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/eye.png"/>Auditoria
                            </a>
                        <?php } ?>

                        <!-- 6 submodulo - Clientes -->
                        <?php if ($rowlog['peradmcli'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/user_red.png"/>
                                        Clientes
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/clientes/insertar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/clientes/consultar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>


                        <!-- 4 submodulo - Perfiles -->
                        <?php if ($rowlog['peradmper'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/group.png"/>
                                        Perfiles
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/perfiles/insertar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/perfiles/consultar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>


                        <!-- 5 submodulo - Usuarios -->
                        <?php if ($rowlog['peradmusu'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/usuario.png"/>
                                        Usuarios
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/usuarios/insertar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/usuarios/consultar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                        </a>

                                    </div>
                                </li>
                            </ul>
                            
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/usuario.png"/>
                                        Asesores
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/usuarios/agregar_asesor.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/usuarios/consultar_asesor.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                        </a>

                                    </div>
                                </li>
                            </ul>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/usuario.png"/>
                                        Cobrador
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/usuarios/agregar_cobrador.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/usuarios/consultar_cobrador.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                        </a>

                                    </div>
                                </li>
                            </ul>
                            
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/group.png"/>
                                        Grupos
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/grupos/insertar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/group_add.png"/>Insertar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/grupos/consultar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                        </a>

                                    </div>
                                </li>
                            </ul>
                        <?php } ?>

                        <!-- 3 submodulo - E. geografica -->
                        <?php if ($rowlog['peradmege'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos//world.png"/>
                                        E. geografica
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <!-- Desde aquí son Sub Menus -->
                                        <ul class="navbar-nav">
                                            <li class="nav-item dropright">
                                                <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                                    <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/world.png"/>
                                                    Departamento
                                                </a>

                                                <div class="dropdown-menu l-ninety">
                                                    <a class="dropdown-item"
                                                       href="<?php echo $r ?>modulos/administracion/egeografica/deptos/insertar.php"
                                                       onclick="carga()">
                                                        <img class="pr-1"
                                                             src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar
                                                    </a>
                                                    <a class="dropdown-item"
                                                       href="<?php echo $r ?>modulos/administracion/egeografica/deptos/consultar.php"
                                                       onclick="carga()">
                                                        <img class="pr-1"
                                                             src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                                    </a>
                                                </div>
                                            </li>
                                        </ul>
                                        <ul class="navbar-nav">
                                            <li class="nav-item dropright">
                                                <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                                    <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/map.png"/>
                                                    Ciudad
                                                </a>

                                                <div class="dropdown-menu l-ninety">
                                                    <a class="dropdown-item"
                                                       href="<?php echo $r ?>modulos/administracion/egeografica/ciudades/insertar.php"
                                                       onclick="carga()">
                                                        <img class="pr-1"
                                                             src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar
                                                    </a>
                                                    <a class="dropdown-item"
                                                       href="<?php echo $r ?>modulos/administracion/egeografica/ciudades/consultar.php"
                                                       onclick="carga()">
                                                        <img class="pr-1"
                                                             src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                                    </a>
                                                </div>
                                            </li>
                                        </ul>

                                    </div>
                                </li>
                            </ul>
                        <?php } ?>

                        <!-- fin submodulos -->
                    </div>
                </li>

            <?php } ?>

            
            <!-- X Modulo - PUBLICO -->
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle active" data-dom="empresa" href="#">
                    <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/eye.png"/>Publico
                </a>
                <div class="dropdown-menu first-drop">
                    <!-- 1 submodulo - AGREGAR CONTRATO -->
                    <!-- Aqui se debe asignar un permiso para quienes trabajan con los contratos -->
                        
                    <a class="dropdown-item"
                        href="<?php echo $r ?>modulos/administracion/clientes/consultar.php"
                        onclick="carga()">
                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/user_red.png"/>Ver clientes
                    </a>
                        
                    <a class="dropdown-item"
                        href="<?php echo $r ?>modulos/principal/contrato/ver_contrato.php"
                        onclick="carga()">
                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/page.png"/>Ver Contratos
                    </a>

                    <a class="dropdown-item"
                        href="<?php echo $r ?>modulos/principal/recibos/seguimiento/rastrear_recibos.php"
                        onclick="carga()">
                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/page_find.png"/>Seguimiento de recibos
                    </a>
                </div>
            </li>
            
            <?php if ($rowlog['percontrato'] == '1') { ?>
                <!-- X Modulo - Contrato -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle active" data-dom="empresa" href="#">
                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/page.png"/>Contratos
                    </a>
                    <div class="dropdown-menu first-drop">
                        <!-- 1 submodulo - AGREGAR CONTRATO -->
                        <!-- Aqui se debe asignar un permiso para quienes trabajan con los contratos -->
                        <?php if ($rowlog['percontratoadd'] == '1') { ?>

                            <a class="dropdown-item"
                               href="<?php echo $r ?>modulos/principal/contrato/insertar.php"
                               onclick="carga()">
                                <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/page_add.png"/>
                                Agregar contrato
                            </a>
                            
                        <?php } ?>
                            
                        <a class="dropdown-item"
                            href="<?php echo $r ?>modulos/principal/contrato/consultar.php"
                            onclick="carga()">
                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/page_find.png"/>
                            Ver contratos
                        </a>
                        
                        <?php if ($rowlog['percontratoadd'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/usuario.png"/>
                                        Asesores
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/usuarios/agregar_asesor.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/usuarios/consultar_asesor.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                        </a>

                                    </div>
                                </li>
                            </ul>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/usuario.png"/>
                                        Cobrador
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/usuarios/agregar_cobrador.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/usuarios/consultar_cobrador.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                        </a>

                                    </div>
                                </li>
                            </ul>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/group.png"/>
                                        Grupos
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/grupos/insertar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/group_add.png"/>Insertar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/grupos/consultar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                        </a>

                                    </div>
                                </li>
                            </ul>
                        <?php } ?>
                            

                    </div>
                </li>
            <?php } ?>

            
            <?php if ($rowlog['perrecibo'] == '1') { ?>
                <!-- X Modulo - Recibos -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle active" data-dom="recibos" href="#">
                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/script.png"/>Recibos
                    </a>
                    <div class="dropdown-menu first-drop">
                        <!-- 1 submodulo - AGREGAR RECIBOS -->
                        <!-- Aqui se debe asignar un permiso para quienes trabajan con los contratos -->
                        <?php if ($rowlog['perpagadoadd'] == '1') { ?>

                            <a class="dropdown-item"
                               href="<?php echo $r ?>modulos/principal/recibos/cobrados/recibos_cobrados.php"
                               onclick="carga()">
                                <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/script_add.png"/>
                                Agregar Recibos Pagados
                            </a>
                        <?php } ?>
                        
                        
                        <?php if ($rowlog['percobrarseadd'] == '1') { ?>
                            <a class="dropdown-item" href="<?php echo $r ?>modulos/principal/recibos/recibos_a_cobrarse_insertar_v2.php" onclick="carga()">
                                <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/script_add.png"/>
                                Agregar Recibos a Cobrarse
                            </a>
                        <?php } ?>

                        <?php if ($rowlog['percobrarseedit'] == '1') { ?>
                            <a class="dropdown-item" href="<?php echo $r ?>modulos/principal/recibos/recibos_a_cobrarse_editar_v2.php" onclick="carga()">
                                <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/script_edit.png"/>
                                Editar Recibos a Cobrarse
                            </a>
                        <?php } ?>
                            
                        <?php if ($rowlog['perimprimirrecibos'] == '1') { ?>
                            <a class="dropdown-item" href="<?php echo $r ?>modulos/principal/recibos/reportes/parametros_rac.php" onclick="carga()">
                                <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/printer.png"/>
                                Imprimir recibos a cobrarse
                            </a>
                        <?php } ?>
                        <?php if ($rowlog['percobrarseadd'] == '1') { ?>
                            <a class="dropdown-item" href="<?php echo $r ?>modulos/principal/recibos/reportes/rac_consulta.php" onclick="carga()">
                                <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/page_white_magnify.png"/>
                                Consultar contratos a cobrarse
                            </a>
                            
                            <a class="dropdown-item"
                                href="<?php echo $r ?>modulos/principal/recibos/seguimiento/rastrear_recibos.php"
                                onclick="carga()">
                                <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/page_find.png"/>Seguimiento de recibos
                            </a>
                            <a class="dropdown-item"
                                href="<?php echo $r ?>modulos/principal/recibos/ajustes/ajustes.php"
                                onclick="carga()">
                                <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/cog.png"/>Ajuste de contrato
                            </a>
                        <?php } ?>
                        
                        
                    </div>
                </li>
            <?php } ?>

            
            <?php if ($rowlog['perbodega'] == '1') { ?>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle active" data-dom="empresa" href="#">
                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/box.png"/>Bodega
                    </a>
                    <div class="dropdown-menu first-drop">

                        <!-- 9 submodulo - Productos -->
                        <?php if ($rowlog['peradmpro'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/user_gray.png"/>
                                        Productos
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/productos/insertar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/productos/consultar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>

                        <!--DOCUMENTOS BODEGA-->
                        <?php if ($rowlog['perboddoc'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/layout.png"/>
                                        Documentos
                                    </a>
                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/bodega/documentos/entrada/principal.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/layout.png"/>
                                            D. Entrada
                                        </a>
                                        
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/bodega/documentos/salida/principal.php"
                                           onclick="carga()">
                                           <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/layout_delete.png"/>
                                           D. Salida
                                        </a>

                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/bodega/documentos/masivo/establecer_en_cero.php"
                                           onclick="carga()">
                                           <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/layout_delete.png"/>
                                           D. Salida Masiva
                                        </a>

                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/bodega/documentos/consultar.php"
                                           onclick="carga()"><img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>
                                           Consulta
                                        </a>
                                    </div>
                                    
                                    <?php if ($rowlog['percontratofac'] == '1' ) { ?>
                                    <a class="dropdown-item" href="<?php echo $r ?>modulos/principal/contrato/consultar.php" onclick="carga()">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/page_find.png"/>
                                        Facturar
                                    </a>
                                    <?php } ?>
                                </li>
                            </ul>
                            
                        <?php } ?>
                        <!--FIN DOCUMENTOS BODEGA-->

                        <!--                        KARDE BODEGA-->
                        <?php if ($rowlog['perbodkar'] == '1' ) { ?>
                            <a class="dropdown-item" href="<?php echo $r ?>modulos/bodega/conkardex.php"
                               onclick="carga()">
                                <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/layers.png"/>Kardex
                            </a>
                        <?php } ?>

                    </div>
                </li>
            <?php } ?>
            
            
            <!--FIN DE MODULO DE TESORERIA-->
            <?php if ($rowlog['pertesoreria'] == '1') { ?>
                <!-- Sexto Modulo - Tesoreria -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle active" data-dom="empresa" href="#">
                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/money.png"/>Tesoreria
                    </a>
                    <div class="dropdown-menu first-drop">
                        <!-- 1 submodulo - Costos y Gastos -->
                        <?php if ($rowlog['pertesoreria'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/money_add.png"/>
                                        Costos y Gastos
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/tesoreria/insertar_cyg.php" onclick="carga()">
                                            <img class="pr-1"
                                                 src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Insertar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/tesoreria/consultar_cyg.php">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>

                        <!-- 6 submodulo - Proveedores -->
                        <?php if ($rowlog['pertesoreria'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/user_gray.png"/>
                                        Proveedores
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/tesoreria/proveedores/insertar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/tesoreria/proveedores/consultar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>


                    </div>
                </li>
            <?php } ?>

            <?php if ($can_see_consult || $can_see_report) { ?>
                <!-- X Modulo - Contrato -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle active" data-dom="empresa" href="#">
                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/report.png"/>Reportes
                    </a>
                    <div class="dropdown-menu first-drop">
                        <!-- 1 submodulo - AGREGAR CONTRATO -->
                        <!-- Aqui se debe asignar un permiso para quienes trabajan con los contratos -->
                        <?php if ($can_see_consult) { ?>

                            <a class="dropdown-item"
                               href="<?php echo $r ?>modulos/consultas/index.php"
                               onclick="carga()">
                                <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/application_view_list.png"/>
                                Consultas
                            </a>
                        <?php } ?>
                        <?php if ($can_see_report) { ?>
                            
                            <a class="dropdown-item"
                               href="<?php echo $r ?>modulos/informes/index.php"
                               onclick="carga()">
                                <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/book.png"/>
                                Informes
                            </a>
                            
                        <?php } ?>

                    </div>
                </li>
            <?php } ?>

        </ul>
        <div id="btn-menu-prev">
            <svg width="25px" height="25px" viewBox="0 0 16 16" class="bi bi-arrow-left-circle-fill" fill="currentColor"
                 xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd"
                      d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-4.5.5a.5.5 0 0 0 0-1H5.707l2.147-2.146a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708-.708L5.707 8.5H11.5z"/>
            </svg>
        </div>

        <div id="btn-menu-next">
            <svg width="25px" height="25px" viewBox="0 0 16 16" class="bi bi-arrow-right-circle-fill"
                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd"
                      d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-11.5.5a.5.5 0 0 1 0-1h5.793L8.146 5.354a.5.5 0 1 1 .708-.708l3 3a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708-.708L10.293 8.5H4.5z"/>
            </svg>
        </div>
    </div>
</nav>

<script>
    /* var temp;
    $(".active-drop").hover(function(){
        var obj = this.dataset.dom;
        $("#"+obj).dropdown('toggle');
    });

    $(".active-drop").blur(function(){
        var obj = this.dataset.dom;
        $("#"+obj).dropdown('hide')
    });
    function active_drop(obj)
    {
        $("#"+obj).dropdown('show')
    } */
</script>

<script>
    var menu_options = $("li.nav-item.dropdown").length;
    var limit_show = 7;
    if (menu_options > 7) {
        for (i = limit_show; i < menu_options; i++)
            $("li.nav-item.dropdown").eq(i).css("display", "none");
    } else {
        $("#btn-menu-next").css("display", "none");
        $("#btn-menu-prev").css("display", "none");
    }
    var limit_hide = 5;
    var hide = <?php
        if (isset($_SESSION["active_option_menu"])) {
            echo $_SESSION["active_option_menu"];
        } else {
            echo 0;
        }
        ?>

        function actual_menu() {
            for (i = 0; i < hide; i++) {
                $("li.nav-item.dropdown").eq(i).css("display", "none");
                var show = limit_show + i;
                $("li.nav-item.dropdown").eq(show).css("display", "block");
            }
        }

    if (hide > 0)
        actual_menu();


    $("#btn-menu-next").click(function () {
        if (hide < limit_hide) {
            $("li.nav-item.dropdown").eq(hide).css("display", "none");
            var show = limit_show + hide;
            $("li.nav-item.dropdown").eq(show).css("display", "block");
            hide++;
            $.get('<?php echo $r ?>incluir/src/index_menu.php?index=' + hide);
        }
    });
    $("#btn-menu-prev").click(function () {
        if (hide > 0) {
            hide--;
            $("li.nav-item.dropdown").eq(hide).css("display", "block");
            var show = limit_show + hide;
            $("li.nav-item.dropdown").eq(show).css("display", "none");
            $.get('<?php echo $r ?>incluir/src/index_menu.php?index=' + hide);
        }
    });

    // Controla la opcion activa
    var actual = null;
    // Controla si se dio click en una opcion del nav
    var click_nav = false;
    $(".nav-link").click(function () {
        // Si actual ya posee un elemento, se oculpa para mostrar el nuevo
        if (actual != null)
            actual.css('display', 'none');

        // Asignamos el elemento .first-drop correspondiente
        actual = $(this).siblings(".first-drop");
        // mostramos las opciones del menu
        actual.css('display', 'block');

        click_nav = true;

        // Siempre que se de click en un elemento del menu debemos hacer unbind del la funcion click del body
        $("body").unbind("click");

        $("body").click(function () {
            if ($("body").hasClass("cerrar-menu") && !click_nav) {
                actual.css('display', 'none');
                $("body").removeClass("cerrar-menu");
                $("body").unbind("click");
            } else {
                $("body").addClass("cerrar-menu");
                click_nav = false;
            }
        });
    });

</script>
