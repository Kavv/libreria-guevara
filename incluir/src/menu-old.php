<?php include($r . 'incluir/emergentes.php'); ?>

<!-- CSS only -->
<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/css/menu.css"/>
<style>
    .active-drop:hover + .dropdown-menu {
        display: block !important;
    }

    .first-drop {
        top: 80% !important;
    }

    .dropdown-menu:hover {
        display: block !important;
    }

    .l-ninety {
        left: 90% !important;
    }

    .dropright {
        width: 100% !important;
    }

    #btn-menu-next {
        position: absolute;
        right: 10px;
        cursor: pointer;
        color: black;
    }

    #btn-menu-prev {
        position: absolute;
        right: 40px;
        cursor: pointer;
        color: black;
    }

    #btn-menu-next:hover, #btn-menu-prev:hover {
        color: #007bff;
    }

</style>


<nav class="navbar navbar-expand-lg navbar-light bg-light">

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#nav-principal"
            aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="nav-principal">
        <ul class="navbar-nav">


            <?php if ($rowlog['peradmin'] == '1' or $rowlog['usuid'] == '80083610' or $rowlog['usuid'] == '77155629') { ?>
                <!-- PRIMER MODULO -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle active" data-dom="empresa" style="cursor:pointer">
                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/wrench.png"/>Administración
                    </a>
                    <div class="dropdown-menu first-drop">

                        <!-- 1 submodulo - Parametros -->
                        <?php if ($rowlog['peradmpar'] == '1') { ?>
                            <a class="dropdown-item"
                               href="<?php echo $r ?>modulos/administracion/parametros/parametros.php"
                               onclick="carga()">
                                <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/cog.png"/>Parametros
                            </a>
                        <?php } ?>

                        <!-- 2 submodulo - Empresas -->
                        <?php if ($rowlog['peradmemp'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/empresa.png"/>
                                        Empresas
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/empresas/insertar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/empresas/consultar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>

                        <!-- 3 submodulo - E. geografica -->
                        <?php if ($rowlog['peradmege'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos//world.png"/>
                                        E. geografica
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <!-- Desde aquí son Sub Menus -->
                                        <ul class="navbar-nav">
                                            <li class="nav-item dropright">
                                                <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                                    <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/world.png"/>
                                                    Departamento
                                                </a>

                                                <div class="dropdown-menu l-ninety">
                                                    <a class="dropdown-item"
                                                       href="<?php echo $r ?>modulos/administracion/egeografica/deptos/insertar.php"
                                                       onclick="carga()">
                                                        <img class="pr-1"
                                                             src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar
                                                    </a>
                                                    <a class="dropdown-item"
                                                       href="<?php echo $r ?>modulos/administracion/egeografica/deptos/consultar.php"
                                                       onclick="carga()">
                                                        <img class="pr-1"
                                                             src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                                    </a>
                                                </div>
                                            </li>
                                        </ul>
                                        <ul class="navbar-nav">
                                            <li class="nav-item dropright">
                                                <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                                    <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/map.png"/>
                                                    Ciudad
                                                </a>

                                                <div class="dropdown-menu l-ninety">
                                                    <a class="dropdown-item"
                                                       href="<?php echo $r ?>modulos/administracion/egeografica/ciudades/insertar.php"
                                                       onclick="carga()">
                                                        <img class="pr-1"
                                                             src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar
                                                    </a>
                                                    <a class="dropdown-item"
                                                       href="<?php echo $r ?>modulos/administracion/egeografica/ciudades/consultar.php"
                                                       onclick="carga()">
                                                        <img class="pr-1"
                                                             src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                                    </a>
                                                </div>
                                            </li>
                                        </ul>
                                        <ul class="navbar-nav">
                                            <li class="nav-item dropright">
                                                <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                                    <img class="pr-1"
                                                         src="<?php echo $r ?>imagenes/iconos/empresa.png"/>
                                                    Sede
                                                </a>

                                                <div class="dropdown-menu l-ninety">
                                                    <a class="dropdown-item"
                                                       href="<?php echo $r ?>modulos/administracion/egeografica/sedes/insertar.php"
                                                       onclick="carga()">
                                                        <img class="pr-1"
                                                             src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar
                                                    </a>
                                                    <a class="dropdown-item"
                                                       href="<?php echo $r ?>modulos/administracion/egeografica/sedes/consultar.php"
                                                       onclick="carga()">
                                                        <img class="pr-1"
                                                             src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                                    </a>
                                                </div>
                                            </li>
                                        </ul>

                                    </div>
                                </li>
                            </ul>
                        <?php } ?>

                        <!-- 4 submodulo - Perfiles -->
                        <?php if ($rowlog['peradmper'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/group.png"/>
                                        Perfiles
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/perfiles/insertar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/perfiles/consultar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>


                        <!-- 5 submodulo - Usuarios -->
                        <?php if ($rowlog['peradmusu'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/usuario.png"/>
                                        Usuarios
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/usuarios/insertar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/usuarios/consultar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                        </a>

                                        <ul class="navbar-nav">
                                            <li class="nav-item dropright">
                                                <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                                    <img class="pr-1"
                                                         src="<?php echo $r ?>imagenes/iconos/porcentaje.png"/>
                                                    Comisiones
                                                </a>

                                                <div class="dropdown-menu l-ninety">
                                                    <a class="dropdown-item"
                                                       href="<?php echo $r ?>modulos/administracion/usuarios/comasesor.php"
                                                       onclick="carga()">
                                                        <img class="pr-1"
                                                             src="<?php echo $r ?>imagenes/iconos/add.png"/>Asesores
                                                    </a>
                                                    <a class="dropdown-item"
                                                       href="<?php echo $r ?>modulos/administracion/usuarios/comrelac.php"
                                                       onclick="carga()">
                                                        <img class="pr-1"
                                                             src="<?php echo $r ?>imagenes/iconos/add.png"/>Relacionistas
                                                    </a>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>


                        <!-- 6 submodulo - Clientes -->
                        <?php if ($rowlog['peradmcli'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/user_red.png"/>
                                        Clientes
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/clientes/insertar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/clientes/consultar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/clientes/exportar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/bullet_disk.png"/>Exportar
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>

                        <!-- 7 submodulo - Proveedores -->
                        <?php if ($rowlog['peradmprv'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/user_gray.png"/>
                                        Proveedores
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/proveedores/insertar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/proveedores/consultar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>

                        <!-- 8 submodulo - Causales -->
                        <?php if ($rowlog['peradmege'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos//bullet_error.png"/>
                                        Causales
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <!-- Desde aquí son Sub Menus -->
                                        <ul class="navbar-nav">
                                            <li class="nav-item dropright">
                                                <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                                    <img class="pr-1"
                                                         src="<?php echo $r ?>imagenes/iconos/asterisk_orange.png"/>
                                                    Tipo de causal
                                                </a>

                                                <div class="dropdown-menu l-ninety">
                                                    <a class="dropdown-item"
                                                       href="<?php echo $r ?>modulos/administracion/causales/tcausales/insertar.php"
                                                       onclick="carga()">
                                                        <img class="pr-1"
                                                             src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar
                                                    </a>
                                                    <a class="dropdown-item"
                                                       href="<?php echo $r ?>modulos/administracion/causales/tcausales/consultar.php"
                                                       onclick="carga()">
                                                        <img class="pr-1"
                                                             src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                                    </a>
                                                </div>
                                            </li>
                                        </ul>
                                        <ul class="navbar-nav">
                                            <li class="nav-item dropright">
                                                <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                                    <img class="pr-1"
                                                         src="<?php echo $r ?>imagenes/iconos/asterisk_yellow.png"/>
                                                    Causal
                                                </a>

                                                <div class="dropdown-menu l-ninety">
                                                    <a class="dropdown-item"
                                                       href="<?php echo $r ?>modulos/administracion/causales/causales/insertar.php"
                                                       onclick="carga()">
                                                        <img class="pr-1"
                                                             src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar
                                                    </a>
                                                    <a class="dropdown-item"
                                                       href="<?php echo $r ?>modulos/administracion/causales/causales/consultar.php"
                                                       onclick="carga()">
                                                        <img class="pr-1"
                                                             src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                                    </a>
                                                </div>
                                            </li>
                                        </ul>

                                    </div>
                                </li>
                            </ul>
                        <?php } ?>

                        <!-- 9 submodulo - Productos -->
                        <?php if ($rowlog['peradmprv'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/user_gray.png"/>
                                        Productos
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/productos/insertar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/productos/consultar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>

                        <!-- 10 submodulo - Formas de pago -->
                        <?php if ($rowlog['peradmprv'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/creditcards.png"/>
                                        Formas de pago
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/fpagos/insertar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/fpagos/consultar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>

                        <!-- 11 submodulo - Medios de despacho -->
                        <?php if ($rowlog['peradmmde'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/lorry.png"/>
                                        Medios de despacho
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/mdespachos/insertar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/mdespachos/consultar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>

                        <!-- 12 submodulo - Mantenimiento -->
                        <?php if ($rowlog['peradmman'] == '1' or $rowlog['usuid'] == '1031128535' or $rowlog['usuperfil'] == 3) { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/cog.png"/>
                                        Mantenimiento
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/mantenimiento/pdfs.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/pdf.png"/>Cambio PDF
                                            de solicitudes
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/mantenimiento/cambiarid.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/vcard.png"/>Cambiar
                                            cedula de cliente
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/mantenimiento/confac.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/tab_edit.png"/>Regenerar
                                            factura
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/mantenimiento/camempresa.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/empresa.png"/>Cambio
                                            de Empresa en Solicitud
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>

                        <!-- 13 submodulo - Autorizaciones -->
                        <?php if ($rowlog['peradmaut'] == '1' or $rowlog['usuid'] == '80083610' or $rowlog['usuid'] == '77155629') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/flag_blue.png"/>
                                        Autorizaciones
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/autorizacion/aut_salida.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/flag_green.png"/>Aut.
                                            Salida
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/autorizacion/consultar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>


                        <!-- 14 submodulo - G. Documental -->
                        <?php if ($rowlog['peradmgesdoc'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/page_white_text.png"/>
                                        G. Documental
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/gdocumental/insertar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/page.png"/>Insertar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/gdocumental/consultar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/gdocumental/solicitar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/page_add.png"/>Solicitar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/gdocumental/lisprestamos.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/page_find.png"/>L.
                                            Solicitudes
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>

                        <!-- 15 submodulo - Papeleria y Aseo -->
                        <?php if ($rowlog['peradmgesdoc'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/page_white_text.png"/>
                                        Papeleria y Aseo
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/gdocumental/insertarproducto.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/page.png"/>Crear
                                            Producto
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/gdocumental/stock.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/page_add.png"/>Inventario
                                            PyA
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/gdocumental/papeyaseo.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/page.png"/>Insertar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/gdocumental/consultarpape.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/gdocumental/solicitarpape.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/page_add.png"/>Solicitar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/administracion/gdocumental/lisprestamospape.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/page_find.png"/>L.
                                            Solicitudes
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>

                        <!-- fin submodulos -->
                    </div>
                </li>

            <?php } ?>

            <!---->
            <!--            MODULO DE CALL CENTER-->
            <!--            -->

            <?php if ($rowlog['percallcenter'] == '1' or $rowlog['usuid'] == '77155629' or $rowlog['usuid'] == '1077034420' or $rowlog['usuid'] == '80083610' or $rowlog['usuid'] == '413472' or $_SESSION['id'] == '1077034420') { ?>
                <!-- Segundo Modulo -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle active" data-dom="empresa" href="#">
                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/group.png"/>Call Center
                    </a>
                    <div class="dropdown-menu first-drop">

                        <!-- 1 submodulo -->
                        <?php if ($rowlog['percallllamar'] == '1') { ?>
                            <a class="dropdown-item" href="<?php echo $r ?>modulos/callcenter/llamadas/llamar.php"
                               onclick="carga()">
                                <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/phone.png"/>Gestion Llamadas
                            </a>
                        <?php } ?>

                        <?php if ($rowlog['percallcampana'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/flag_green.png"/>
                                        Campañas
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/callcenter/llamadas/cargarcampa.php"
                                           onclick="carga()"><img class="pr-1"
                                                                  src="<?php echo $r ?>imagenes/iconos/flag_green.png"/>Cargar
                                            Campaña</a>
                                        <a href="<?php echo $r ?>modulos/callcenter/llamadas/consultar_cam.php"
                                           onclick="carga()" class="dropdown-item">
                                            <img src="<?php echo $r ?>imagenes/iconos/zoom.png" class="pr-1">Consultar
                                            campaña
                                        </a>

                                    </div>
                                </li>
                            </ul>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/group.png"/>
                                        Grupos
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/callcenter/llamadas/creargrupo.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/group.png"/>Crear
                                            grupo
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/callcenter/llamadas/congrupos.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                        </a>
                                    </div>
                                </li>
                            </ul>


                        <?php } ?>

                        <ul class="navbar-nav">
                            <li class="nav-item dropright">
                                <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                    <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/flag_green.png"/>
                                    Prospectos
                                </a>

                                <div class="dropdown-menu l-ninety">
                                    <?php if ($rowlog['percallgeneral'] == '1') { ?>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/callcenter/llamadas/insertar_pros.php"
                                           onclick="carga()">
                                            <img src="<?php echo $r ?>imagenes/iconos/user_add.png"/>Insertar Prospecto
                                        </a>
                                    <?php } ?>
                                    <?php if ($rowlog['percallcampana'] == '1') { ?>
                                        <a href="<?php echo $r ?>modulos/callcenter/llamadas/referidos.php"
                                           onclick="carga()" class="dropdown-item">
                                            <img src="<?php echo $r ?>imagenes/iconos/user_gray.png"> Referidos
                                        </a>
                                        <a href="<?php echo $r ?>modulos/callcenter/llamadas/pros_activos.php"
                                           onclick="carga()"
                                           class="dropdown-item">
                                            <img src="<?php echo $r ?>imagenes/iconos/user_red.png"> Prospectos activos
                                        </a>
                                    <?php } ?>
                                    <?php if ($rowlog['percallgeneral'] == '1') { ?>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/callcenter/llamadas/consultar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consulta
                                            prospecto
                                        </a>
                                    <?php } ?>
                                </div>
                            </li>
                        </ul>
                        <?php if ($rowlog['percallgeneral'] == '1') { ?>


                            <a class="dropdown-item"
                               href="<?php echo $r ?>modulos/callcenter/llamadas/lisseguimiento.php"
                               onclick="carga()">
                                <img src="<?php echo $r ?>imagenes/iconos/book_addresses.png"/>Seguimiento
                            </a>
                            <a class="dropdown-item" href="<?php echo $r ?>modulos/callcenter/llamadas/agendado.php"
                               onclick="carga()">
                                <img src="<?php echo $r ?>imagenes/iconos/date.png"/>Agendado
                            </a>
                        <?php } ?>

                        <!--                        CAPACITACIONES-->
                        <?php if ($rowlog['percallcapaci'] == '1' or $rowlog['usuid'] == '77155629' or $rowlog['usuid'] == '80083610' or $rowlog['usuid'] == '413472' || $_SESSION['id'] == '1077034420') { ?>
                            <a class="dropdown-item"
                               href="<?php echo $r ?>modulos/callcenter/llamadas/capacitacion.php"
                               onclick="carga()">
                                <img src="<?php echo $r ?>imagenes/iconos/date.png"/>Capacitaciones
                            </a>
                        <?php } ?>

                        <!--                        FIN CAPACITACIONES-->

                        <!--                        SEMAFORO CALL CENTER-->
                        <?php if ($rowlog['percallsemaforo'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/lightbulb.png"/>
                                        Semaforo
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="javascript:AbrirCentrado('<?php echo $r ?>modulos/callcenter/llamadas/semaforo.php');">
                                            <img class="pr-1"
                                                 src="<?php echo $r ?>imagenes/iconos/lightbulb.png"/>Semaforo
                                            hoy
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/callcenter/llamadas/consemaforo.php"
                                           onclick="carga()">
                                            <img class="pr-1"
                                                 src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Semaforo
                                            antiguo
                                        </a>

                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/callcenter/llamadas/consemaforofechas.php"
                                           onclick="carga()">
                                            <img class="pr-1"
                                                 src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Semaforo
                                            Rangos
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>
                        <!--                        FIN SEMAFORO CALL CENTER-->

                        <!--                        REPORTES DE CALL CENTER-->
                        <?php if ($rowlog['percallrepo'] == '1' or $rowlog['usuid'] == '77155629' or $rowlog['usuid'] == '80083610' or $rowlog['usuid'] == '413472') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/report.png"/>
                                        Reportes
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/callcenter/llamadas/reportes/repgestiones.php"
                                           onclick="carga()">
                                            <img class="pr-1"
                                                 src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Gestiones
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/callcenter/llamadas/reportes/reprealizadas.php"
                                           onclick="carga()">
                                            <img class="pr-1"
                                                 src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>C.
                                            Realizadas
                                        </a>

                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/callcenter/llamadas/reportes/repintegral.php"
                                           onclick="carga()">
                                            <img class="pr-1"
                                                 src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Reporte
                                            integral
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>

                        <!--                        FIN DE REPORTES DE CALL CENTER-->


                        <!--                        VIATICOS CALL CENTER-->
                        <?php if ($rowlog['percallviaticos'] == '1' or $rowlog['usuid'] == '1077034420') { ?>

                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1"
                                             src="<?php echo $r ?>imagenes/iconos/report_user.png"/>
                                        Viaticos
                                    </a>

                                    <div class="dropdown-menu l-ninety">

                                        <?php if ($rowlog['percallviainser'] == '1' or $rowlog['usuid'] == '1077034420') { ?>
                                            <a class="dropdown-item"
                                               href="<?php echo $r ?>modulos/callcenter/llamadas/viaticos.php"
                                               onclick="carga()">
                                                <img class="pr-1"
                                                     src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Insertar
                                                viaticos
                                            </a>
                                        <?php } ?>

                                        <?php if ($rowlog['percallviavalor'] == '1' or $rowlog['usuid'] == '1077034420') { ?>
                                            <a class="dropdown-item"
                                               href="<?php echo $r ?>modulos/callcenter/llamadas/viaticos_2.php"
                                               onclick="carga()">
                                                <img class="pr-1"
                                                     src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Valor
                                                viaticos
                                            </a>
                                        <?php } ?>

                                        <?php if ($rowlog['percallviacons'] == '1' or $rowlog['usuid'] == '1077034420') { ?>
                                            <a class="dropdown-item"
                                               href="<?php echo $r ?>modulos/callcenter/llamadas/consultarviaticos.php"
                                               onclick="carga()">
                                                <img class="pr-1"
                                                     src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Consultar
                                                Viaticos
                                            </a>
                                        <?php } ?>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>

                        <!--                        FIN VIATICOS CALL CENTER-->
                    </div>
                </li>

            <?php } ?>

            <!---->
            <!--            FIN MODULO DE CALL CENTER-->
            <!--            -->


            <?php if ($rowlog['persolicitud'] == '1' || $rowlog['usustaff'] == '1' || $_SESSION['id'] == '20371048' || $_SESSION['id'] == '1110451657' || $_SESSION['id'] == '1032408225' || $_SESSION['id'] == '1018410939' or $rowlog['usuid'] == '1077034420') { ?>
                <!-- Tercer Modulo -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle active" data-dom="empresa" href="#">
                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/group.png"/>Solicitudes
                    </a>
                    <div class="dropdown-menu first-drop">

                        <!-- 1 submodulo - insertar S. Normal -->
                        <?php if ($rowlog['persolnor'] == '1' || $_SESSION['id'] == '20371048' || $_SESSION['id'] == '1110451657' || $_SESSION['id'] == '1032408225' || $_SESSION['id'] == '1018410939') { ?>
                            <a class="dropdown-item"
                               href="<?php echo $r ?>modulos/solicitudes/insertar.php?tipo=NORMAL"
                               onclick="carga()">
                                <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/script.png"/>Insertar
                                S. Normal
                            </a>
                        <?php } ?>
                        <!-- 2 Submodulo - Insertar S. Estimulacion -->
                        <?php if ($rowlog['persolest'] == '1' || $_SESSION['id'] == '1110451657' || $_SESSION['id'] == '1032408225' || $_SESSION['id'] == '1018410939') { ?>
                            <a class="dropdown-item"
                               href="<?php echo $r ?>modulos/solicitudes/insertar.php?tipo=ESTIMULACION"
                               onclick="carga()">
                                <img class="pr-1"
                                     src="<?php echo $r ?>imagenes/iconos/script_code_red.png"/>Insertar S.
                                Estimulacion
                            </a>
                        <?php } ?>

                        <!-- 3 submodulo - Anular -->
                        <?php if ($rowlog['persolanu'] == '1') { ?>
                            <a class="dropdown-item" href="<?php echo $r ?>modulos/solicitudes/anular.php"
                               onclick="carga()">
                                <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/script_delete.png"/>Anular
                            </a>
                        <?php } ?>

                        <!-- 4 submodulo - Consultar -->
                        <?php if ($rowlog['persolcon'] == '1' || $rowlog['usustaff'] == '1') { ?>
                            <a class="dropdown-item"
                               href="<?php echo $r ?>modulos/solicitudes/consultar.php"
                               onclick="carga()">
                                <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                            </a>
                        <?php } ?>

                        <!-- 5 submodulo - Screen -->
                        <?php if ($rowlog['persolscr'] == '1') { ?>
                            <a class="dropdown-item" href="<?php echo $r ?>modulos/solicitudes/screen.php"
                               onclick="carga()">
                                <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/layout_edit.png"/>Screen
                            </a>
                        <?php } ?>

                        <!-- 6 submodulo - Check Fisico -->
                        <?php if ($rowlog['persolche'] == '1' or $rowlog['usuid'] == '1077034420') { ?>
                            <a class="dropdown-item"
                               href="<?php echo $r ?>modulos/solicitudes/confisicos.php"
                               onclick="carga()">
                                <img class="pr-1"
                                     src="<?php echo $r ?>imagenes/iconos/application_view_list.png"/>Check
                                fisico
                            </a>
                        <?php } ?>

                        <!-- 7 submodulo - Soporte -->
                        <?php if ($rowlog['persolsopor'] == '1') { ?>
                            <a class="dropdown-item"
                               href="<?php echo $r ?>modulos/solicitudes/consoporte.php"
                               onclick="carga()">
                                <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/computer.png"/>Soportes
                            </a>
                        <?php } ?>


                        <!-- 8 submodulo - Reporte -->
                        <?php if ($rowlog['persolrepor'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/report.png"/>
                                        Reportes
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/solicitudes/consolgestiones.php"
                                           onclick="carga()">
                                            <img class="pr-1"
                                                 src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Notas
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/solicitudes/consolscreen.php"
                                           onclick="carga()">
                                            <img class="pr-1"
                                                 src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Screen
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>

                        <!-- 9 submodulo - Mis Solicitudes -->
                        <?php if ($rowlog['persolprop'] == '1') { ?>
                            <a class="dropdown-item"
                               href="<?php echo $r ?>modulos/solicitudes/consultar_prop.php"
                               onclick="carga()">
                                <img class="pr-1"
                                     src="<?php echo $r ?>imagenes/iconos/page_white_text.png"/>Mis
                                Solicitudes
                            </a>
                        <?php } ?>
                    </div>
                </li>

            <?php } ?>

            <?php if ($rowlog['perfactura'] == '1') { ?>
                <!-- Cuarto Modulo - FACTURACION -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle active" data-dom="empresa" href="#">
                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/note.png"/>Facturacion
                    </a>
                    <div class="dropdown-menu first-drop">

                        <!-- 1 submodulo - Facturar -->
                        <?php if ($rowlog['perfacfac'] == '1') { ?>
                            <a class="dropdown-item" href="<?php echo $r ?>modulos/facturacion/facturar.php"
                               onclick="carga()">
                                <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/note_add.png"/>Facturar
                            </a>
                        <?php } ?>
                        <!-- 2 Submodulo - Consultar -->
                        <?php if ($rowlog['perfaccon'] == '1') { ?>
                            <a class="dropdown-item" href="<?php echo $r ?>modulos/facturacion/consultar.php"
                               onclick="carga()">
                                <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                            </a>
                        <?php } ?>

                        <!-- 3 submodulo - Reporte -->
                        <?php if ($rowlog['perfacrep'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/report.png"/>
                                        Reportes
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/facturacion/reportes/conventas.php"
                                           onclick="carga()">
                                            <img class="pr-1"
                                                 src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Ventas
                                            detalladas
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/facturacion/reportes/conresumen.php"
                                           onclick="carga()">
                                            <img class="pr-1"
                                                 src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Resumen
                                            ventas
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/facturacion/reportes/conproducto.php"
                                           onclick="carga()">
                                            <img class="pr-1"
                                                 src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Ventas
                                            x producto
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/facturacion/reportes/excelproductos.php">
                                            <img class="pr-1"
                                                 src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Productos
                                            Solicitados
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>

                        <!-- 4 submodulo - Reportes Call -->
                        <?php if ($rowlog['perfacrepcall'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/report.png"/>
                                        Reportes Call
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/facturacion/reportes/concall.php"
                                           onclick="carga()">
                                            <img class="pr-1"
                                                 src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Detalle
                                            Factura
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>

                    </div>
                </li>
            <?php } ?>

            <!---->
            <!--            MODULO DE BODEGA-->
            <!--            -->
            <?php if ($rowlog['perbodega'] == '1' || $_SESSION['id'] == '1022949677' || $_SESSION['id'] == '1031128535' || $_SESSION['id'] == '1030526237') { ?>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle active" data-dom="empresa" href="#">
                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/box.png"/>Bodega
                    </a>
                    <div class="dropdown-menu first-drop">


                        <!--                        DOCUMENTOS BODEGA-->
                        <?php if ($rowlog['perboddoc'] == '1' || $_SESSION['id'] == '1022949677' || $_SESSION['id'] == '1031128535') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item"" href="#">
                                    <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/layout.png"/>
                                    Documentos
                                    </a>
                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/bodega/documentos/entrada/principal.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/layout.png"/>
                                            D. Entrada
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/bodega/documentos/entrada/dv/principal.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/layout_add.png"/>Devoluciones</a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/bodega/documentos/salida/principal.php"
                                           onclick="carga()"><img class="pr-1"
                                                                  src="<?php echo $r ?>imagenes/iconos/layout_delete.png"/>D.
                                            Salida</a>

                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/bodega/documentos/consultar.php"
                                           onclick="carga()"><img class="pr-1"
                                                                  src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consulta</a>
                                    </div>
                                    </a>
                                </li>
                            </ul>
                        <?php } ?>
                        <!--FIN DOCUMENTOS BODEGA-->

                        <!--                        KARDE BODEGA-->
                        <?php if ($rowlog['perbodkar'] == '1' || $_SESSION['id'] == '1022949677') { ?>
                            <a class="dropdown-item" href="<?php echo $r ?>modulos/bodega/conkardex.php"
                               onclick="carga()">
                                <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/layers.png"/>Kardex
                            </a>
                        <?php } ?>

                        <!--                        FIN KARDEX BODEGA-->

                        <!--                        DESPACHO BODEGAS-->
                        <?php if ($rowlog['perboddes'] == '1' || $_SESSION['id'] == '1022949677' || $_SESSION['id'] == '1031128535' || $_SESSION['id'] == '1030526237') { ?>

                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item"" href="#">
                                    <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/lorry.png"/>
                                    Despacho
                                    </a>
                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/bodega/lisdespacho.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/page_copy.png"/>
                                            Solicitudes a
                                            despachar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/bodega/bandespacho.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/folder_page.png"/>
                                            Bandeja
                                            de
                                            despacho</a>

                                    </div>
                                    </a>
                                </li>
                            </ul>
                        <?php } ?>
                        <!--                        FIN DESPACHO BODEGA-->

                        <!--                        PLANILLAS BODEGAS-->

                        <?php if ($rowlog['perbodpla'] == '1' || $_SESSION['id'] == '1022949677' || $_SESSION['id'] == '1031128535') { ?>

                            <a class="dropdown-item"
                               href="<?php echo $r ?>modulos/bodega/conplanilla.php"
                               onclick="carga()">
                                <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/table.png"/>
                                Planilla
                            </a>
                        <?php } ?>

                        <!--                        FIN PLANILLAS BODEGAS-->

                        <!--                        REPORTES BODEGA-->

                        <?php if ($rowlog['perbodrep'] == '1' || $_SESSION['id'] == '1022949677') { ?>

                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item"" href="#">
                                    <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/report.png"/>
                                    Reportes
                                    </a>
                                    <div class="dropdown-menu l-ninety">

                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/bodega/reportes/condescarga.php"
                                           onclick="carga()">
                                            <img class="pr-1"
                                                 src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>
                                            Entregados
                                        </a>

                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/bodega/reportes/coninventario.php"
                                           onclick="carga()">
                                            <img class="pr-1"
                                                 src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Inventario
                                        </a>

                                    </div>
                                    </a>
                                </li>
                            </ul>
                        <?php } ?>
                        <!--                        FIN REPORTES BODEGA-->
                    </div>
                </li>
            <?php } ?>

            <!--            FIN DE MODULO DE BODEGA-->


            <?php if ($rowlog['pertesoreria'] == '1' or $_SESSION['id'] == '52440297' or $_SESSION['id'] == '1024499151' or $_SESSION['id'] == '1030539847') { ?>
                <!-- Sexto Modulo - Tesoreria -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle active" data-dom="empresa" href="#">
                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/money.png"/>Tesoreria
                    </a>
                    <div class="dropdown-menu first-drop">
                        <!-- 1 submodulo - Costos y Gastos -->
                        <?php if ($rowlog['pertesoreria'] == '1' or $_SESSION['id'] == '1024499151' or $_SESSION['id'] == '1030539847') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/money_add.png"/>
                                        Costos y Gastos
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/tesoreria/insertar_cyg.php" onclick="carga()">
                                            <img class="pr-1"
                                                 src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Insertar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/tesoreria/consultar_cyg.php">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/tesoreria/con_c_pago_cyg.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/money_add.png"/>Confirmar
                                            Pago
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>

                        <!-- 2 submodulo - Saldos de Banco -->
                        <?php if ($rowlog['pertesoreria'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/money_add.png"/>
                                        Saldos de Banco
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/tesoreria/con_cheques_sb.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/money_add.png"/>Validar
                                            Cheques
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/tesoreria/consultar_cheques_sb.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>

                        <!-- 3 submodulo - Nominas -->
                        <?php if ($rowlog['pertesoreria'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/usuario.png"/>
                                        Nominas
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/tesoreria/insernomina.php" onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/money_add.png"/>Insertar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/tesoreria/consultarnomina.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>

                        <!-- 4 submodulo - Aux Movilizacion -->
                        <?php if ($rowlog['pertesoreria'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/car.png"/>
                                        Aux Movilizacion
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/tesoreria/insertransporte.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/money_add.png"/>Insertar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/tesoreria/consultartransporte.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>

                        <!-- 5 submodulo - Doc. Equivalente -->
                        <?php if ($rowlog['pertesoreria'] == '1' or $_SESSION['id'] == '52440297') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/script.png"/>
                                        Doc. Equivalente
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/tesoreria/inserdocequi.php" onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/money_add.png"/>Insertar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/tesoreria/consultardocequi.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>

                        <!-- 6 submodulo - Proveedores -->
                        <?php if ($rowlog['pertesoreria'] == '1' or $_SESSION['id'] == '52440297') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/user_gray.png"/>
                                        Proveedores
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/tesoreria/proveedores/insertar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/tesoreria/proveedores/consultar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>

                        <!-- 7 submodulo - Bancos -->
                        <?php if ($rowlog['pertesoreria'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/empresa.png"/>
                                        Bancos
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/tesoreria/bancos/insertar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/tesoreria/bancos/consultar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>

                        <!-- 8 submodulo - Mensajeria -->
                        <?php if ($rowlog['pertesoreria'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/box.png"/>
                                        Mensajeria
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/tesoreria/insertarmensa.php" onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/tesoreria/consultarmensa.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>

                        <!-- 9 submodulo - Reportes -->
                        <?php //if ($rowlog['pertesoreria'] == '1') { ?>
                        <!-- <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/report.png" />
                                        Reportes
                                    </a>
                                    
                                    <div  class="dropdown-menu l-ninety">
                                        <a class="dropdown-item" href="<?php echo $r ?>modulos/tesoreria/con_planilla_diaria.php" onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png" />Planilla diaria de caja
                                        </a>
                                    </div>
                                </li>   
                            </ul> -->
                        <?php //} ?>
                    </div>
                </li>
            <?php } ?>



            <?php if ($rowlog['percartera'] == '1' || $_SESSION['id'] == '1077034420' || $_SESSION['id'] == '1031128535') { ?>
                <!-- Septimo Modulo - Cartera -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle active" data-dom="empresa" href="#">
                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/briefcase.png"/>Cartera
                    </a>
                    <div class="dropdown-menu first-drop">
                        <!-- 1 submodulo - Notas -->
                        <?php if ($rowlog['percarnot'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/layout.png"/>
                                        Notas
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/cartera/notas/NC/listar_cartera.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/layout_delete.png"/>Nota
                                            de Credito
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/cartera/notas/ND/listar_cartera.php">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/layout_add.png"/>Nota
                                            de Debito
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/cartera/notas/consultar.php" onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>

                        <!-- 2 submodulo - Consultar cartera -->
                        <?php if ($rowlog['percarcon'] == '1') { ?>
                            <a class="dropdown-item" href="<?php echo $r ?>modulos/cartera/consultar.php"
                               onclick="carga()">
                                <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar cartera
                            </a>
                        <?php } ?>

                        <!-- 3 submodulo - Promotores -->
                        <?php if ($rowlog['percarpro'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/user_suit.png"/>
                                        Promotores
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/cartera/promotores/conasignar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/arrow_in.png"/>Asignar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/cartera/promotores/conreasignar.php">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/arrow_inout.png"/>Reasignar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/cartera/promotores/condesasignar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/arrow_out.png"/>Desasignar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/cartera/promotores/traspaso.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/arrow_switch.png"/>Traspaso
                                            promotor
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>


                        <!-- 4 submodulo - Aplicar pago -->
                        <?php if ($rowlog['percarpag'] == '1') { ?>
                            <a class="dropdown-item"
                               href="<?php echo $r ?>modulos/cartera/pagos/individual/consultar.php" onclick="carga()">
                                <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/money.png"/>Aplicar Pago
                            </a>
                        <?php } ?>


                        <!-- 5 submodulo - Consulta Masiva -->
                        <?php //if ($rowlog['percarmas'] == '1' || $_SESSION['id'] == '1077034420') { ?>
                        <!-- <a class="dropdown-item" href="<?php echo $r ?>modulos/cartera/consulta_masiva.php" onclick="carga()">
                                <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consulta Masiva
                            </a> -->
                        <?php //} ?>

                        <!-- 6 submodulo - Cambiar fecha -->
                        <?php if ($rowlog['percarfec'] == '1') { ?>
                            <a class="dropdown-item" href="<?php echo $r ?>modulos/cartera/fecha/consultar.php"
                               onclick="carga()">
                                <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/calendar.png"/>Cambiar fecha
                            </a>
                        <?php } ?>

                        <!-- 7 submodulo - Clientes Nuevos -->
                        <!-- ORIGINALMENTE NO TIENE PERMISO -->
                        <!-- IMPORTANTE AJUSTAR -->
                        <a class="dropdown-item" href="<?php echo $r ?>modulos/cartera/clinuevos.php" onclick="carga()">
                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Cuotas
                        </a>

                        <!-- 8 submodulo - Cartera castigadas -->
                        <?php if ($rowlog['percarcas'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/punish.png"/>
                                        Castigada
                                    </a>
                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/cartera/castigada/consultar.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/layout_delete.png"/>A
                                            Castigar
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/cartera/castigada/concastigada.php">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/layout_add.png"/>Castigadas
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/cartera/castigada/conrecuperar.php">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/layout_add.png"/>A
                                            Recuperar
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>


                        <!-- 9 submodulo - Datacredito -->
                        <?php //if ($rowlog['percardat'] == '1') { ?>
                        <!-- <ul class="navbar-nav">
							<li class="nav-item dropright">
								<a class="dropdown-toggle active-drop dropdown-item" href="#">
									<img class="pr-1" src="<?php echo $r ?>imagenes/iconos/datacredito.png" />
									Datacredito
								</a>
								
								<div class="dropdown-menu l-ninety">
									<ul class="navbar-nav">
										<li class="nav-item dropright">
											<a class="dropdown-toggle active-drop dropdown-item" href="#">
												<img class="pr-1" src="<?php echo $r ?>imagenes/iconos/text_padding_top.png" />
												Generar plano
											</a>
											
											<div  class="dropdown-menu l-ninety">
												<a class="dropdown-item" href="<?php echo $r ?>modulos/cartera/datacredito/principal1.php" onclick="carga()">
													<img class="pr-1" src="<?php echo $r ?>imagenes/iconos/empresa.png" />IdenCorp
												</a>
												<a class="dropdown-item" href="<?php echo $r ?>modulos/cartera/datacredito/principal2.php" onclick="carga()">
													<img class="pr-1" src="<?php echo $r ?>imagenes/iconos/empresa.png" />Coopincorp
												</a>
											</div>
										</li>
									</ul>
									<a class="dropdown-item" href="<?php echo $r ?>modulos/cartera/datacredito/consultar.php" onclick="carga()">
										<img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png" />Consulta cuenta
									</a>
									
								</div>
							</li>
						</ul> -->
                        <?php //} ?>


                        <!-- 10 submodulo - Reportes -->
                        <?php if ($rowlog['percarrep'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/report.png"/>
                                        Reportes
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/cartera/reportes/conresumen.php"
                                           onclick="carga()">
                                            <img class="pr-1"
                                                 src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Resumen
                                            cartera
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/cartera/reportes/lisresmoro.php"
                                           onclick="carga()">
                                            <img class="pr-1"
                                                 src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Resumen
                                            morosidad
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/cartera/reportes/condetmoro.php"
                                           onclick="carga()">
                                            <img class="pr-1"
                                                 src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Detalle
                                            morosidad
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/cartera/reportes/congestion.php"
                                           onclick="carga()">
                                            <img class="pr-1"
                                                 src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Gestion
                                            Diaria
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/cartera/reportes/conpagocontado.php"
                                           onclick="carga()">
                                            <img class="pr-1"
                                                 src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Clientes
                                            Contado
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/cartera/reportes/concalificacion.php"
                                           onclick="carga()">
                                            <img class="pr-1"
                                                 src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Califi.
                                            Producto
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/cartera/reportes/congencartera.php"
                                           onclick="carga()">
                                            <img class="pr-1"
                                                 src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Generar
                                            Cartera
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/cartera/reportes/convamos1.php"
                                           onclick="carga()">
                                            <img class="pr-1"
                                                 src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Como
                                            Vamos
                                        </a>
                                        <a class="dropdown-item" href="<?php echo $r ?>modulos/cartera/deterioro.php"
                                           onclick="carga()">
                                            <img class="pr-1"
                                                 src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Deterioro
                                            390
                                        </a>
                                        <a class="dropdown-item" href="<?php echo $r ?>modulos/cartera/deterioroFin.php"
                                           onclick="carga()">
                                            <img class="pr-1"
                                                 src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Deterioro
                                            final
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/cartera/Reportes/Consolidado.php"
                                           onclick="carga()">
                                            <img class="pr-1"
                                                 src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Consolidado
                                        </a>

                                    </div>
                                </li>
                            </ul>
                        <?php } ?>


                        <!-- 11 submodulo - Paz y Salvo -->
                        <?php if ($rowlog['percarpaz'] == '1' || $_SESSION['id'] == '1031128535') { ?>
                            <a class="dropdown-item" href="<?php echo $r ?>modulos/cartera/certificados/consultar.php"
                               onclick="carga()">
                                <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/pdf.png"/>Paz y Salvo
                            </a>
                        <?php } ?>
                    </div>
                </li>
            <?php } ?>




            <?php if ($rowlog['percaja'] == '1' or $rowlog['usuid'] == '123456789') { ?>
                <!-- Octavo Modulo - Caja -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle active" data-dom="empresa" href="#">
                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/coins.png"/>Caja
                    </a>
                    <div class="dropdown-menu first-drop">
                        <!-- 1 submodulo - Consultar Recibos -->
                        <a class="dropdown-item" href="<?php echo $r ?>modulos/caja/consultar.php" onclick="carga()">
                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar recibo
                        </a>

                        <!-- 2 submodulo - Reportes -->
                        <ul class="navbar-nav">
                            <li class="nav-item dropright">
                                <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                    <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/report.png"/>
                                    Reportes
                                </a>

                                <div class="dropdown-menu l-ninety">
                                    <a class="dropdown-item"
                                       href="<?php echo $r ?>modulos/caja/reportes/conmediosmagneticos.php"
                                       onclick="carga()">
                                        <img class="pr-1"
                                             src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>M.
                                        Magneticos 1
                                    </a>
                                    <a class="dropdown-item"
                                       href="<?php echo $r ?>modulos/caja/reportes/conmediosmagneticos_1.php">
                                        <img class="pr-1"
                                             src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>M.
                                        Magneticos 2
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </li>
            <?php } ?>



            <?php if ($rowlog['percomision'] == '1' || $rowlog['usustaff'] == '1' || $rowlog['usuid'] == '52765484' || $_SESSION['id'] == '20371048') { ?>
                <!-- Noveno Modulo - Comisiones -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle active" data-dom="empresa" href="#">
                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/valor.png"/>Comisiones
                    </a>
                    <div class="dropdown-menu first-drop">
                        <!-- 1 submodulo - C.Normal -->
                        <?php if ($rowlog['percominormal'] == '1' || $rowlog['usuid'] == '52765484') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/script.png"/>
                                        C. Normal
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/comisiones/normal/asesor.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/database_gear.png"/>Asesor
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/comisiones/normal/director.php">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/database_gear.png"/>Director
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/comisiones/normal/gcomercial.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/database_gear.png"/>G.
                                            comercial
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/comisiones/normal/ggeneral.php">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/database_gear.png"/>G.
                                            general
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/comisiones/normal/adicional.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/database_gear.png"/>Adicional
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/comisiones/normal/relacionista.php">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/database_gear.png"/>Relacionista
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>
                        <!-- 2 submodulo - C.Libranza -->
                        <?php // if ($rowlog['percomilibranza'] == '1') { ?>
                        <!-- <ul class="navbar-nav">
                            <li class="nav-item dropright">
                                <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                    <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/script_code.png" />
                                    C. Libranza
                                </a>
                                
                                <div  class="dropdown-menu l-ninety">
                                    <a class="dropdown-item" href="<?php echo $r ?>modulos/comisiones/normal/asesor.php" onclick="carga()">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/database_gear.png" />Asesor
                                    </a>
                                </div>
                            </li>
                        </ul> -->
                        <?php // } ?>

                        <!-- 3 submodulo - C.Estimulacion -->
                        <?php // if ($rowlog['percomiestimulacion'] == '1') { ?>
                        <!-- <ul class="navbar-nav">
							<li class="nav-item dropright">
								<a class="dropdown-toggle active-drop dropdown-item" href="#">
									<img class="pr-1" src="<?php echo $r ?>imagenes/iconos/script_code_red.png" />
									C. Estimulacion
								</a>
								
								<div  class="dropdown-menu l-ninety">
									<a class="dropdown-item" href="<?php echo $r ?>modulos/comisiones/normal/asesor.php" onclick="carga()">
										<img class="pr-1" src="<?php echo $r ?>imagenes/iconos/database_gear.png" />Asesor
									</a>
								</div>
							</li>
                        </ul> -->
                        <?php // } ?>


                        <!-- 4 submodulo - Mi Comision -->
                        <?php if ($rowlog['percomiprop'] == '1' || $rowlog['usustaff'] == '1' || $_SESSION['id'] == '20371048') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos//money.png"/>
                                        Mi Comision
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <!-- Desde aquí son Sub Menus -->
                                        <ul class="navbar-nav">
                                            <li class="nav-item dropright">
                                                <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                                    <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/script.png"/>
                                                    C. Normal
                                                </a>

                                                <div class="dropdown-menu l-ninety">
                                                    <a class="dropdown-item"
                                                       href="<?php echo $r ?>modulos/comisiones/normal/asesor_prop.php"
                                                       onclick="carga()">
                                                        <img class="pr-1"
                                                             src="<?php echo $r ?>imagenes/iconos/database_gear.png"/>Asesor
                                                    </a>
                                                    <a class="dropdown-item"
                                                       href="<?php echo $r ?>modulos/comisiones/normal/relaci_prop.php"
                                                       onclick="carga()">
                                                        <img class="pr-1"
                                                             src="<?php echo $r ?>imagenes/iconos/database_gear.png"/>Relacionista
                                                    </a>
                                                    <a class="dropdown-item"
                                                       href="<?php echo $r ?>modulos/comisiones/normal/adicio_prop.php"
                                                       onclick="carga()">
                                                        <img class="pr-1"
                                                             src="<?php echo $r ?>imagenes/iconos/database_gear.png"/>Adicional
                                                    </a>
                                                </div>
                                            </li>
                                        </ul>
                                        <!-- <ul class="navbar-nav">
                                            <li class="nav-item dropright">
                                                <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                                    <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/script_code.png"/>
                                                    C. Libranza
                                                </a>

                                                <div class="dropdown-menu l-ninety">
                                                </div>
                                            </li>
                                        </ul>
                                        <ul class="navbar-nav">
                                            <li class="nav-item dropright">
                                                <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                                    <img class="pr-1"
                                                         src="<?php echo $r ?>imagenes/iconos/script_code_red.png"/>
                                                    C. Estimulacion
                                                </a>

                                                <div class="dropdown-menu l-ninety">
                                                </div>
                                            </li>
                                        </ul> -->

                                    </div>
                                </li>
                            </ul>
                        <?php } ?>
                    </div>
                </li>
            <?php } ?>

            <?php if ($rowlog['perestadistico'] == '1' || $_SESSION['id'] == '1077034420' || $_SESSION['id'] == '52440297' || $_SESSION['id'] == '1110451657') { ?>
                <!-- Noveno Modulo - Estadisticos -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle active" data-dom="empresa" href="#">
                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/chart_bar.png"/>Estadisticos
                    </a>
                    <div class="dropdown-menu first-drop">
                        <!-- 1 submodulo - Ventas -->
                        <?php if ($rowlog['perestavent'] == '1' or $rowlog['usuid'] == '1077034420') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/calculator.png"/>
                                        Ventas
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/estadisticos/ventas/condetven.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/usuario.png"/>Detallado
                                            x asesor
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/estadisticos/ventas/condetvenrela.php">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/usuario.png"/>Detallado
                                            x Rela
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/estadisticos/ventas/condetgru.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/group.png"/>Detallado
                                            x grupo
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/estadisticos/ventas/conresve.php">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/usuario.png"/>Resumen
                                            asesores
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/estadisticos/ventas/conresvenrela.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/usuario.png"/>Resumen
                                            Relacioni
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/estadisticos/ventas/conresgru.php">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/group.png"/>Resumen
                                            grupos
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/estadisticos/ventas/conrespro.php">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/cd.png"/>Resumen
                                            Productos
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        <?php } ?>

                        <!-- 2 submodulo - Reportes -->
                        <?php if ($rowlog['perestarepor'] == '1') { ?>
                            <ul class="navbar-nav">
                                <li class="nav-item dropright">
                                    <a class="dropdown-toggle active-drop dropdown-item" href="#">
                                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/calculator.png"/>
                                        Reportes
                                    </a>

                                    <div class="dropdown-menu l-ninety">
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/estadisticos/reportes/conscreen.php"
                                           onclick="carga()">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/group.png"/>Screen
                                            Asesores
                                        </a>
                                        <a class="dropdown-item"
                                           href="<?php echo $r ?>modulos/estadisticos/reportes/conestado.php">
                                            <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/report.png"/>E.
                                            Pedidos
                                        </a>
                                    </div>
                                </li>


                            </ul>
                        <?php } ?>
                    </div>
                </li>
            <?php } ?>

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle active" data-dom="consultas" href="#">
                    <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/application_xp.png"/>Consultas
                </a>
                <div class="dropdown-menu first-drop">
                    <?php

                    $qryConsultas = $db->query("select * from consultasdinamicas");
                    while ($row = $qryConsultas->fetch(PDO::FETCH_ASSOC)) { ?>
                        <a class="dropdown-item"
                           href="<?php echo $r . 'modulos/consultas/consulta.php?id=' . $row['cdiid'] ?>">
                            <img class="pr-1"
                                 src="<?php echo $r ?>imagenes/iconos/application.png"/><?php echo $row['cdinombre'] ?>
                        </a>

                    <?php } ?>
                </div>
            </li>

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle active" data-dom="informes" href="#">
                    <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/pdf.png"/>Informes
                </a>
                <div class="dropdown-menu first-drop">
                    <?php

                    $qryConsultas = $db->query("select * from informesdinamicos");
                    while ($row = $qryConsultas->fetch(PDO::FETCH_ASSOC)) { ?>
                        <a class="dropdown-item"
                           href="<?php echo $r . 'modulos/informes/informe.php?id=' . $row['idiid'] ?>">
                            <img class="pr-1"
                                 src="<?php echo $r ?>imagenes/iconos/pdf.png"/><?php echo $row['idinombre'] ?>
                        </a>

                    <?php } ?>
                </div>
            </li>

            <?php if ($rowlog['persolicitud'] == '1') { ?>
                <!-- X Modulo - Contrato -->
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle active" data-dom="empresa" href="#">
                        <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/page.png"/>Contratos
                    </a>
                    <div class="dropdown-menu first-drop">
                        <!-- 1 submodulo - AGREGAR CONTRATO -->
                        <!-- Aqui se debe asignar un permiso para quienes trabajan con los contratos -->
                        <?php if ($rowlog['persolicitud'] == '1') { ?>

                            <a class="dropdown-item"
                               href="<?php echo $r ?>modulos/principal/contrato/insertar.php"
                               onclick="carga()">
                                <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/page_add.png"/>
                                Agregar contrato
                            </a>
                            
                            <a class="dropdown-item"
                               href="<?php echo $r ?>modulos/principal/contrato/consultar.php"
                               onclick="carga()">
                                <img class="pr-1" src="<?php echo $r ?>imagenes/iconos/page_find.png"/>
                                Ver contratos
                            </a>
                            
                        <?php } ?>

                    </div>
                </li>
            <?php } ?>

        </ul>
        <div id="btn-menu-prev">
            <svg width="25px" height="25px" viewBox="0 0 16 16" class="bi bi-arrow-left-circle-fill" fill="currentColor"
                 xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd"
                      d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-4.5.5a.5.5 0 0 0 0-1H5.707l2.147-2.146a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708-.708L5.707 8.5H11.5z"/>
            </svg>
        </div>

        <div id="btn-menu-next">
            <svg width="25px" height="25px" viewBox="0 0 16 16" class="bi bi-arrow-right-circle-fill"
                 fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd"
                      d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-11.5.5a.5.5 0 0 1 0-1h5.793L8.146 5.354a.5.5 0 1 1 .708-.708l3 3a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708-.708L10.293 8.5H4.5z"/>
            </svg>
        </div>
    </div>
</nav>

<nav id="nav">
    <ul>
        <?php if ($rowlog['peradmin'] == '1' or $rowlog['usuid'] == '80083610' or $rowlog['usuid'] == '77155629') { ?>
            <li class="nav-item"><span tabindex="1"><img
                            src="<?php echo $r ?>imagenes/iconos/wrench.png"/>Administrar</span>
                <ul>
                    <?php if ($rowlog['peradmpar'] == '1') { ?>
                        <li class="nav-item"><a href="<?php echo $r ?>modulos/administracion/parametros/parametros.php"
                                                onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/cog.png"/>Parametros</a>
                        </li>
                    <?php } ?>
                    <?php if ($rowlog['peradmemp'] == '1') { ?>
                        <li class="nav-item"><span tabindex="1"><img src="<?php echo $r ?>imagenes/iconos/empresa.png"/>Empresas</span>
                            <ul>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/administracion/empresas/insertar.php"
                                            onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar</a>
                                </li>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/administracion/empresas/consultar.php"
                                            onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar</a>
                                </li>
                            </ul>
                        </li>
                    <?php } ?>
                    <?php if ($rowlog['peradmege'] == '1') { ?>
                        <li class="nav-item"><span tabindex="1"><img src="<?php echo $r ?>imagenes/iconos/world.png"/>E. geografica</span>
                            <ul>
                                <li class="nav-item"><span tabindex="1"><img
                                                src="<?php echo $r ?>imagenes/iconos/world.png"/>Departamento</span>
                                    <ul>
                                        <li class="nav-item"><a
                                                    href="<?php echo $r ?>modulos/administracion/egeografica/deptos/insertar.php"
                                                    onclick="carga()"><img
                                                        src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar</a></li>
                                        <li class="nav-item"><a
                                                    href="<?php echo $r ?>modulos/administracion/egeografica/deptos/consultar.php"
                                                    onclick="carga()"><img
                                                        src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-item"><span tabindex="1"><img
                                                src="<?php echo $r ?>imagenes/iconos/map.png"/>Ciudad</span>
                                    <ul>
                                        <li class="nav-item"><a
                                                    href="<?php echo $r ?>modulos/administracion/egeografica/ciudades/insertar.php"
                                                    onclick="carga()"><img
                                                        src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar</a></li>
                                        <li class="nav-item"><a
                                                    href="<?php echo $r ?>modulos/administracion/egeografica/ciudades/consultar.php"
                                                    onclick="carga()"><img
                                                        src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-item"><span tabindex="1"><img
                                                src="<?php echo $r ?>imagenes/iconos/empresa.png"/>Sede</span>
                                    <ul>
                                        <li class="nav-item"><a
                                                    href="<?php echo $r ?>modulos/administracion/egeografica/sedes/insertar.php"
                                                    onclick="carga()"><img
                                                        src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar</a></li>
                                        <li class="nav-item"><a
                                                    href="<?php echo $r ?>modulos/administracion/egeografica/sedes/consultar.php"
                                                    onclick="carga()"><img
                                                        src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    <?php } ?>
                    <?php if ($rowlog['peradmper'] == '1') { ?>
                        <li class="nav-item"><span tabindex="1"><img src="<?php echo $r ?>imagenes/iconos/group.png"/>Perfiles</span>
                            <ul>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/administracion/perfiles/insertar.php"
                                            onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar</a>
                                </li>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/administracion/perfiles/consultar.php"
                                            onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar</a>
                                </li>
                            </ul>
                        </li>
                    <?php } ?>
                    <?php if ($rowlog['peradmusu'] == '1') { ?>
                        <li class="nav-item"><span tabindex="1"><img src="<?php echo $r ?>imagenes/iconos/usuario.png"/>Usuarios</span>
                            <ul>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/administracion/usuarios/insertar.php"
                                            onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar</a>
                                </li>
                                <li class="nav-item"><span tabindex="1"><img
                                                src="<?php echo $r ?>imagenes/iconos/porcentaje.png"/>Comisiones</span>
                                    <ul>
                                        <li class="nav-item"><a
                                                    href="<?php echo $r ?>modulos/administracion/usuarios/comasesor.php"
                                                    onclick="carga()"><img
                                                        src="<?php echo $r ?>imagenes/iconos/add.png"/>Asesores</a></li>
                                        <li class="nav-item"><a
                                                    href="<?php echo $r ?>modulos/administracion/usuarios/comrelac.php"
                                                    onclick="carga()"><img
                                                        src="<?php echo $r ?>imagenes/iconos/add.png"/>Relacionistas</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/administracion/usuarios/consultar.php"
                                            onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar</a>
                                </li>
                            </ul>
                        </li>
                    <?php } ?>
                    <?php if ($rowlog['peradmcli'] == '1') { ?>
                        <li class="nav-item"><span tabindex="1"><img
                                        src="<?php echo $r ?>imagenes/iconos/user_red.png"/>Clientes</span>
                            <ul>
                                <li class="nav-item"><a href="#" onclick="carga()"><img
                                                src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar</a></li>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/administracion/clientes/consultar.php"
                                            onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar</a>
                                </li>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/administracion/clientes/exportar.php"
                                            onclick="carga()"><img
                                                src="<?php echo $r ?>imagenes/iconos/bullet_disk.png"/>Exportar</a></li>
                            </ul>
                        </li>
                    <?php } ?>
                    <?php if ($rowlog['peradmprv'] == '1') { ?>
                        <li class="nav-item"><span tabindex="1"><img
                                        src="<?php echo $r ?>imagenes/iconos/user_gray.png"/>Proveedores</span>
                            <ul>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/administracion/proveedores/insertar.php"
                                            onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar</a>
                                </li>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/administracion/proveedores/consultar.php"
                                            onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar</a>
                                </li>
                            </ul>
                        </li>
                    <?php } ?>
                    <?php if ($rowlog['peradmcau'] == '1') { ?>
                        <li class="nav-item"><span tabindex="1"><img
                                        src="<?php echo $r ?>imagenes/iconos/bullet_error.png"/>Causales</span>
                            <ul>
                                <li class="nav-item"><span tabindex="1"><img
                                                src="<?php echo $r ?>imagenes/iconos/asterisk_orange.png"/>Tipo de causal</span>
                                    <ul>
                                        <li class="nav-item"><a
                                                    href="<?php echo $r ?>modulos/administracion/causales/tcausales/insertar.php"
                                                    onclick="carga()"><img
                                                        src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar</a></li>
                                        <li class="nav-item"><a
                                                    href="<?php echo $r ?>modulos/administracion/causales/tcausales/consultar.php"
                                                    onclick="carga()"><img
                                                        src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-item"><span tabindex="1"><img
                                                src="<?php echo $r ?>imagenes/iconos/asterisk_yellow.png"/>Causal</span>
                                    <ul>
                                        <li class="nav-item"><a
                                                    href="<?php echo $r ?>modulos/administracion/causales/causales/insertar.php"
                                                    onclick="carga()"><img
                                                        src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar</a></li>
                                        <li class="nav-item"><a
                                                    href="<?php echo $r ?>modulos/administracion/causales/causales/consultar.php"
                                                    onclick="carga()"><img
                                                        src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    <?php } ?>
                    <?php if ($rowlog['peradmpro'] == '1') { ?>
                        <li class="nav-item"><span tabindex="1"><img
                                        src="<?php echo $r ?>imagenes/iconos/user_gray.png"/>Productos</span>
                            <ul>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/administracion/productos/insertar.php"
                                            onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar</a>
                                </li>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/administracion/productos/consultar.php"
                                            onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar</a>
                                </li>
                            </ul>
                        </li>
                    <?php } ?>
                    <?php if ($rowlog['peradmfpa'] == '1') { ?>
                        <li class="nav-item"><span tabindex="1"><img
                                        src="<?php echo $r ?>imagenes/iconos/creditcards.png"/>Formas de pago</span>
                            <ul>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/administracion/fpagos/insertar.php"
                                            onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar</a>
                                </li>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/administracion/fpagos/consultar.php"
                                            onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar</a>
                                </li>
                            </ul>
                        </li>
                    <?php } ?>
                    <?php if ($rowlog['peradmmde'] == '1') { ?>
                        <li class="nav-item"><span tabindex="1"><img src="<?php echo $r ?>imagenes/iconos/lorry.png"/>Medios de despacho</span>
                            <ul>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/administracion/mdespachos/insertar.php"
                                            onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar</a>
                                </li>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/administracion/mdespachos/consultar.php"
                                            onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar</a>
                                </li>
                            </ul>
                        </li>
                    <?php } ?>

                    <?php if ($rowlog['peradmman'] == '1' or $rowlog['usuid'] == '1031128535' or $rowlog['usuperfil'] == 3) { ?>
                        <li class="nav-item"><span tabindex="1"><img src="<?php echo $r ?>imagenes/iconos/cog.png"/>Mantenimiento</span>
                            <ul>
                                <?php if ($rowlog['usuperfil'] == 3) { ?>
                                    <li class="nav-item"><a
                                                href="<?php echo $r ?>modulos/administracion/mantenimiento/pdfs.php"
                                                onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/pdf.png"/>Cambio
                                            de PDF</a></li>
                                <?php } else { ?>
                                    <li class="nav-item"><a
                                                href="<?php echo $r ?>modulos/administracion/mantenimiento/pdfs.php"
                                                onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/pdf.png"/>Cambio
                                            de PDF</a></li>
                                    <li class="nav-item"><a
                                                href="<?php echo $r ?>modulos/administracion/mantenimiento/cambiarid.php"
                                                onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/vcard.png"/>Cambiar
                                            cedula</a></li>
                                    <li class="nav-item"><a
                                                href="<?php echo $r ?>modulos/administracion/mantenimiento/confac.php"
                                                onclick="carga()"><img
                                                    src="<?php echo $r ?>imagenes/iconos/tab_edit.png"/>Regenerar
                                            factura</a></li>
                                    <li class="nav-item"><a
                                                href="<?php echo $r ?>modulos/administracion/mantenimiento/camempresa.php"
                                                onclick="carga()"><img
                                                    src="<?php echo $r ?>imagenes/iconos/empresa.png"/>Cambio de Empresa</a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </li>
                    <?php } ?>

                    <?php if ($rowlog['peradmaut'] == '1' or $rowlog['usuid'] == '80083610' or $rowlog['usuid'] == '77155629') { ?>
                        <li class="nav-item"><span tabindex="1"><img
                                        src="<?php echo $r ?>imagenes/iconos/flag_blue.png"/>Autorizaciones</span>
                            <ul>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/administracion/autorizacion/aut_salida.php"
                                            onclick="carga()"><img
                                                src="<?php echo $r ?>imagenes/iconos/flag_green.png"/>Aut. Salida</a>
                                </li>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/administracion/autorizacion/consultar.php"
                                            onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar</a>
                                </li>
                            </ul>
                        </li>
                    <?php } ?>

                    <?php if ($rowlog['peradmgesdoc'] == '1') { ?>
                        <li class="nav-item"><span tabindex="1"><img
                                        src="<?php echo $r ?>imagenes/iconos/page_white_text.png"/>G. Documental</span>
                            <ul>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/administracion/gdocumental/insertar.php"
                                            onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/page.png"/>Insertar</a>
                                </li>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/administracion/gdocumental/consultar.php"
                                            onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar</a>
                                </li>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/administracion/gdocumental/cargue_masivogd.php"
                                            onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/page_add.png"/>Cargue
                                        Masivo</a></li>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/administracion/gdocumental/lisprestamos.php"
                                            onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/page_find.png"/>L.
                                        Prestamos</a></li>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/administracion/gdocumental/solicitar.php"
                                            onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/page_add.png"/>Solicitar</a>
                                </li>
                            </ul>
                        </li>
                    <?php } ?>


                    <?php if ($rowlog['usuperfil'] == 30 or $rowlog['usuid'] == 1077034420 or $rowlog['usuid'] == 1030526237) { ?>
                        <li class="nav-item"><span tabindex="1"><img
                                        src="<?php echo $r ?>imagenes/iconos/page_white_text.png"/>Papeleria y Aseo</span>
                            <ul>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/administracion/gdocumental/papeyaseo.php"
                                            onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/page.png"/>Insertar</a>
                                </li>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/administracion/gdocumental/papeyaseogru.php"
                                            onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/page.png"/>Insertar
                                        x Grupo</a></li>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/administracion/gdocumental/insertarproducto.php"
                                            onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/page.png"/>Crear
                                        Producto</a></li>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/administracion/gdocumental/consultarpape.php"
                                            onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar</a>
                                </li>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/administracion/gdocumental/stock.php"
                                            onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/page_add.png"/>Stock</a>
                                </li>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/administracion/gdocumental/cargue_masivo.php"
                                            onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/page_add.png"/>Cargue
                                        Masivo</a></li>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/administracion/gdocumental/lisprestamospape.php"
                                            onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/page_find.png"/>L.
                                        Solicitudes</a></li>

                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/administracion/gdocumental/solicitarpape.php"
                                            onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/page_add.png"/>Solicitar
                                        Unidad</a></li>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/administracion/gdocumental/solicitarpapemasi1.php"
                                            onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/page_add.png"/>Solicitar
                                        Grupo</a></li>
                            </ul>
                        </li>
                    <?php } ?>

                </ul>
            </li>
        <?php } ?>


        <!-- -------------------------CALL------------------------------------------------ -->


        <?php if ($rowlog['percallcenter'] == '1' or $rowlog['usuid'] == '77155629' or $rowlog['usuid'] == '1077034420' or $rowlog['usuid'] == '80083610' or $rowlog['usuid'] == '413472' or $_SESSION['id'] == '1077034420') { ?>
            <li class="nav-item"><span tabindex="1"><img
                            src="<?php echo $r ?>imagenes/iconos/group.png"/>Call Center</span>
                <ul>
                    <?php if ($rowlog['percallllamar'] == '1') { ?>
                        <li class="nav-item"><a href="<?php echo $r ?>modulos/callcenter/llamadas/llamar.php"
                                                onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/phone.png"/>Gestion
                                Llamadas</a></li>
                    <?php } ?>
                    <?php if ($rowlog['percallcampana'] == '1') { ?>

                        <li class="nav-item"><a
                                    href="<?php echo $r ?>modulos/callcenter/llamadas/cargarcampa.php"
                                    onclick="carga()"><img
                                        src="<?php echo $r ?>imagenes/iconos/flag_green.png"/>Cargar Campaña</a>
                        </li>
                        <li class="nav-item"><a
                                    href="<?php echo $r ?>modulos/callcenter/llamadas/consultar_cam.php"
                                    onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consulta
                                Campaña</a>
                        </li>

                        <li class="nav-item"><span tabindex="1"><img src="<?php echo $r ?>imagenes/iconos/group.png"/>Grupos</span>
                            <ul>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/callcenter/llamadas/creargrupo.php"
                                            onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/group.png"/>Crear
                                        Grupo</a></li>
                                <li class="nav-item"><a href="<?php echo $r ?>modulos/callcenter/llamadas/congrupos.php"
                                                        onclick="carga()"><img
                                                src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar</a></li>
                            </ul>
                        </li>
                        <li class="nav-item"><a href="<?php echo $r ?>modulos/callcenter/llamadas/pros_activos.php"
                                                onclick="carga()"><img
                                        src="<?php echo $r ?>imagenes/iconos/user_red.png"/>Prospectos activos</a></li>
                        <li class="nav-item"><a href="<?php echo $r ?>modulos/callcenter/llamadas/referidos.php"
                                                onclick="carga()"><img
                                        src="<?php echo $r ?>imagenes/iconos/user_gray.png"/>Referidos</a></li>
                    <?php } ?>
                    <?php if ($rowlog['percallgeneral'] == '1') { ?>
                        <li class="nav-item"><a href="<?php echo $r ?>modulos/callcenter/llamadas/consultar.php"
                                                onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consulta
                                Prospecto</a></li>
                        <li class="nav-item"><a href="<?php echo $r ?>modulos/callcenter/llamadas/insertar_pros.php"
                                                onclick="carga()"><img class="pr-1"
                                                                       src="<?php echo $r ?>imagenes/iconos/user_add.png"/>Insertar
                                Prospecto</a></li>
                        <li class="nav-item"><a href="<?php echo $r ?>modulos/callcenter/llamadas/lisseguimiento.php"
                                                onclick="carga()"><img class="pr-1"
                                                                       src="<?php echo $r ?>imagenes/iconos/book_addresses.png"/>Seguimiento</a>
                        </li>
                        <li class="nav-item"><a href="<?php echo $r ?>modulos/callcenter/llamadas/agendado.php"
                                                onclick="carga()"><img class="pr-1"
                                                                       src="<?php echo $r ?>imagenes/iconos/date.png"/>Agendado</a>
                        </li>
                    <?php } ?>
                    <?php if ($rowlog['percallcapaci'] == '1' or $rowlog['usuid'] == '77155629' or $rowlog['usuid'] == '80083610' or $rowlog['usuid'] == '413472' || $_SESSION['id'] == '1077034420') { ?>
                        <li class="nav-item"><a href="<?php echo $r ?>modulos/callcenter/llamadas/capacitacion.php"
                                                onclick="carga()"><img class="pr-1"
                                                                       src="<?php echo $r ?>imagenes/iconos/date.png"/>Capacitaciones</a>
                        </li>
                    <?php } ?>
                    <?php if ($rowlog['percallsemaforo'] == '1') { ?>
                        <li class="nav-item"><span tabindex="1"><img
                                        src="<?php echo $r ?>imagenes/iconos/lightbulb.png"/>Semaforo</span>
                            <ul>
                                <li class="nav-item"><a
                                            href="javascript:AbrirCentrado('<?php echo $r ?>modulos/callcenter/llamadas/semaforo.php');"
                                            target="_blank"><img src="<?php echo $r ?>imagenes/iconos/lightbulb.png"/>Semaforo
                                        Hoy</a></li>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/callcenter/llamadas/consemaforo.php"
                                            onclick="carga()"><img
                                                src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Semaforo
                                        Antiguo</a></li>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/callcenter/llamadas/consemaforofechas.php"
                                            onclick="carga()"><img
                                                src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Semaforo
                                        Rangos</a></li>
                            </ul>
                        </li>
                    <?php } ?>
                    <?php if ($rowlog['percallrepo'] == '1' or $rowlog['usuid'] == '77155629' or $rowlog['usuid'] == '80083610' or $rowlog['usuid'] == '413472') { ?>
                        <li class="nav-item"><span tabindex="1"><img src="<?php echo $r ?>imagenes/iconos/report.png"/>Reportes</span>
                            <ul>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/callcenter/llamadas/reportes/repgestiones.php"
                                            onclick="carga()"><img
                                                src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Gestiones</a>
                                </li>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/callcenter/llamadas/reportes/reprealizadas.php"
                                            onclick="carga()"><img
                                                src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>C.Realizadas</a>
                                </li>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/callcenter/llamadas/reportes/repintegral.php"><img
                                                src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Reporte
                                        Integral</a></li>
                            </ul>
                        </li>
                    <?php } ?>
                    <?php if ($rowlog['percallviaticos'] == '1' or $rowlog['usuid'] == '1077034420') { ?>
                        <li class="nav-item"><span tabindex="1"><img
                                        src="<?php echo $r ?>imagenes/iconos/report_user.png"/>Viaticos</span>
                            <ul>
                                <?php if ($rowlog['percallviainser'] == '1' or $rowlog['usuid'] == '1077034420') { ?>
                                    <li class="nav-item"><a
                                                href="<?php echo $r ?>modulos/callcenter/llamadas/viaticos.php"
                                                onclick="carga()"><img
                                                    src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Insertar
                                            Viaticos</a></li>
                                <?php } ?>
                                <?php if ($rowlog['percallviavalor'] == '1' or $rowlog['usuid'] == '1077034420') { ?>
                                    <li class="nav-item"><a
                                                href="<?php echo $r ?>modulos/callcenter/llamadas/viaticos_2.php"
                                                onclick="carga()"><img
                                                    src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Valor
                                            Viaticos</a></li>
                                <?php } ?>
                                <?php if ($rowlog['percallviacons'] == '1' or $rowlog['usuid'] == '1077034420') { ?>
                                    <li class="nav-item"><a
                                                href="<?php echo $r ?>modulos/callcenter/llamadas/consultarviaticos.php"
                                                onclick="carga()"><img
                                                    src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Consultar
                                            Viaticos</a></li>
                                <?php } ?>
                            </ul>
                        </li>
                    <?php } ?>
                </ul>
            </li>
        <?php } ?>


        <!-- ------------------------------FIN CALL------------------------------------------------- -->


        <?php if ($rowlog['persolicitud'] == '1' || $rowlog['usustaff'] == '1' || $_SESSION['id'] == '20371048' || $_SESSION['id'] == '1110451657' || $_SESSION['id'] == '1032408225' || $_SESSION['id'] == '1018410939' or $rowlog['usuid'] == '1077034420') { ?>
            <li class="nav-item"><span tabindex="1"><img src="<?php echo $r ?>imagenes/iconos/page_white_text.png"/>Solicitudes</span>
                <ul>
                    <?php if ($rowlog['persolnor'] == '1' || $_SESSION['id'] == '20371048' || $_SESSION['id'] == '1110451657' || $_SESSION['id'] == '1032408225' || $_SESSION['id'] == '1018410939') { ?>
                        <li class="nav-item"><span tabindex="1"><img src="<?php echo $r ?>imagenes/iconos/script.png"/>S. Normal</span>
                            <ul>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/solicitudes/insertar.php?tipo=NORMAL"
                                            onclick="carga()"><img
                                                src="<?php echo $r ?>imagenes/iconos/script_add.png"/>Insertar</a></li>
                            </ul>
                        </li>
                    <?php } ?>

                    <?php if ($rowlog['persolest'] == '1' || $_SESSION['id'] == '1110451657' || $_SESSION['id'] == '1032408225' || $_SESSION['id'] == '1018410939') { ?>
                        <li class="nav-item"><span tabindex="1"><img
                                        src="<?php echo $r ?>imagenes/iconos/script_code_red.png"/>S. Estimulacion</span>
                            <ul>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/solicitudes/insertar.php?tipo=ESTIMULACION"
                                            onclick="carga()"><img
                                                src="<?php echo $r ?>imagenes/iconos/script_add.png"/>Insertar</a></li>
                            </ul>
                        </li>
                    <?php } ?>
                    <?php if ($rowlog['persolanu'] == '1') { ?>
                        <li class="nav-item"><a href="<?php echo $r ?>modulos/solicitudes/anular.php" onclick="carga()"><img
                                        src="<?php echo $r ?>imagenes/iconos/script_delete.png"/>Anular</a></li>
                    <?php } ?>
                    <?php if ($rowlog['persolcon'] == '1' || $rowlog['usustaff'] == '1') { ?>
                        <li class="nav-item"><a href="<?php echo $r ?>modulos/solicitudes/consultar.php"
                                                onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar</a>
                        </li>
                    <?php } ?>
                    <?php if ($rowlog['persolscr'] == '1') { ?>
                        <li class="nav-item"><a href="<?php echo $r ?>modulos/solicitudes/screen.php" onclick="carga()"><img
                                        src="<?php echo $r ?>imagenes/iconos/layout_edit.png"/>Screen</a></li>
                    <?php } ?>
                    <?php if ($rowlog['persolche'] == '1' or $rowlog['usuid'] == '1077034420') { ?>
                        <li class="nav-item"><a href="<?php echo $r ?>modulos/solicitudes/confisicos.php"
                                                onclick="carga()"><img
                                        src="<?php echo $r ?>imagenes/iconos/application_view_list.png"/>Check
                                fisico</a></li>
                    <?php } ?>
                    <?php if ($rowlog['persolsopor'] == '1') { ?>
                        <li class="nav-item"><a href="<?php echo $r ?>modulos/solicitudes/consoporte.php"
                                                onclick="carga()"><img
                                        src="<?php echo $r ?>imagenes/iconos/computer.png"/>Soportes</a></li>
                    <?php } ?>
                    <?php if ($rowlog['persolrepor'] == '1') { ?>
                        <li class="nav-item"><span tabindex="1"><img src="<?php echo $r ?>imagenes/iconos/report.png"/>Reportes</span>
                            <ul>
                                <li class="nav-item"><a title="Detalle Gestiones"
                                                        href="<?php echo $r ?>modulos/solicitudes/consolgestiones.php"
                                                        onclick="carga()"><img
                                                src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>D.
                                        Gestiones</a></li>
                                <li class="nav-item"><a title="Detalle Screen"
                                                        href="<?php echo $r ?>modulos/solicitudes/consolscreen.php"
                                                        onclick="carga()"><img
                                                src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>D.
                                        Screen</a></li>
                            </ul>
                        </li>
                    <?php } ?>
                    <?php if ($rowlog['persolprop'] == '1') { ?>
                        <li class="nav-item"><a href="<?php echo $r ?>modulos/solicitudes/consultar_prop.php"
                                                onclick="carga()"><img
                                        src="<?php echo $r ?>imagenes/iconos/page_white_text.png"/>Mis Solicitudes</a>
                        </li>
                    <?php } ?>
                </ul>
            </li>
        <?php } ?>

        <?php if ($rowlog['perfactura'] == '1') { ?>
            <li class="nav-item"><span tabindex="1"><img
                            src="<?php echo $r ?>imagenes/iconos/note.png"/>Facturacion</span>
                <ul>
                    <?php if ($rowlog['perfacfac'] == '1') { ?>
                        <li class="nav-item"><a href="<?php echo $r ?>modulos/facturacion/facturar.php"
                                                onclick="carga()"><img
                                        src="<?php echo $r ?>imagenes/iconos/note_add.png"/>Facturar</a></li>
                    <?php } ?>
                    <?php if ($rowlog['perfaccon'] == '1') { ?>
                        <li class="nav-item"><a href="<?php echo $r ?>modulos/facturacion/consultar.php"
                                                onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar</a>
                        </li>
                    <?php } ?>
                    <?php if ($rowlog['perfacrep'] == '1') { ?>
                        <li class="nav-item"><span tabindex="1"><img src="<?php echo $r ?>imagenes/iconos/report.png"/>Reportes</span>
                            <ul>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/facturacion/reportes/conventas.php"
                                            onclick="carga()"><img
                                                src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Ventas
                                        detalladas</a></li>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/facturacion/reportes/conresumen.php"
                                            onclick="carga()"><img
                                                src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Resumen
                                        ventas</a></li>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/facturacion/reportes/conproducto.php"
                                            onclick="carga()"><img
                                                src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Ventas
                                        x producto</a></li>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/facturacion/reportes/excelproductos.php"><img
                                                src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>E.
                                        Productos</a></li>
                            </ul>
                        </li>
                    <?php } ?>
                    <?php if ($rowlog['perfacrepcall'] == '1') { ?>
                        <li class="nav-item"><span tabindex="1"><img src="<?php echo $r ?>imagenes/iconos/report.png"/>Reportes Call</span>
                            <ul>
                                <li class="nav-item"><a href="<?php echo $r ?>modulos/facturacion/reportes/concall.php"
                                                        onclick="carga()"><img
                                                src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Detalle
                                        Fact.</a></li>
                            </ul>
                        </li>
                    <?php } ?>
                </ul>
            </li>
        <?php } ?>

        <?php if ($rowlog['perbodega'] == '1' || $_SESSION['id'] == '1022949677' || $_SESSION['id'] == '1031128535' || $_SESSION['id'] == '1030526237') { ?>
            <li class="nav-item"><span tabindex="1"><img src="<?php echo $r ?>imagenes/iconos/box.png"/>Bodega</span>
                <ul>
                    <?php if ($rowlog['perboddoc'] == '1' || $_SESSION['id'] == '1022949677' || $_SESSION['id'] == '1031128535') { ?>
                        <li class="nav-item"><span tabindex="1"><img src="<?php echo $r ?>imagenes/iconos/layout.png"/>Documentos</span>
                            <ul>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/bodega/documentos/entrada/principal.php"
                                            onclick="carga()"><img
                                                src="<?php echo $r ?>imagenes/iconos/layout_add.png"/>D. Entrada</a>
                                </li>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/bodega/documentos/entrada/dv/principal.php"
                                            onclick="carga()"><img
                                                src="<?php echo $r ?>imagenes/iconos/layout_add.png"/>Devoluciones</a>
                                </li>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/bodega/documentos/salida/principal.php"
                                            onclick="carga()"><img
                                                src="<?php echo $r ?>imagenes/iconos/layout_delete.png"/>D. Salida</a>
                                </li>
                                <li class="nav-item"><a href="<?php echo $r ?>modulos/bodega/documentos/consultar.php"
                                                        onclick="carga()"><img
                                                src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consulta</a></li>
                            </ul>
                        </li>
                    <?php } ?>
                    <?php if ($rowlog['perbodkar'] == '1' || $_SESSION['id'] == '1022949677') { ?>
                        <li class="nav-item"><a href="<?php echo $r ?>modulos/bodega/conkardex.php"
                                                onclick="carga()"><img
                                        src="<?php echo $r ?>imagenes/iconos/layers.png"/>Kardex</a></li>
                    <?php } ?>
                    <?php if ($rowlog['perboddes'] == '1' || $_SESSION['id'] == '1022949677' || $_SESSION['id'] == '1031128535' || $_SESSION['id'] == '1030526237') { ?>
                        <li class="nav-item"><span tabindex="1"><img src="<?php echo $r ?>imagenes/iconos/lorry.png"/>Despacho</span>
                            <ul>
                                <li class="nav-item"><a href="<?php echo $r ?>modulos/bodega/lisdespacho.php"
                                                        onclick="carga()"><img
                                                src="<?php echo $r ?>imagenes/iconos/page_copy.png"/>Solicitudes a
                                        despachar</a></li>
                                <li class="nav-item"><a href="<?php echo $r ?>modulos/bodega/bandespacho.php"
                                                        onclick="carga()"><img
                                                src="<?php echo $r ?>imagenes/iconos/folder_page.png"/>Bandeja de
                                        despacho</a></li>
                            </ul>
                        </li>
                    <?php } ?>
                    <?php if ($rowlog['perbodpla'] == '1' || $_SESSION['id'] == '1022949677' || $_SESSION['id'] == '1031128535') { ?>
                        <li class="nav-item"><span tabindex="1"><img src="<?php echo $r ?>imagenes/iconos/table.png"/>Planillas</span>
                            <ul>
                                <li class="nav-item"><a href="<?php echo $r ?>modulos/bodega/desplanilla.php"
                                                        onclick="carga()"><img
                                                src="<?php echo $r ?>imagenes/iconos/table_edit.png"/>Descarga de
                                        planilla</a></li>
                                <li class="nav-item"><span tabindex="1"><img
                                                src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar</span>
                                    <ul>
                                        <li class="nav-item"><a href="<?php echo $r ?>modulos/bodega/conplanilla.php"
                                                                onclick="carga()"><img
                                                        src="<?php echo $r ?>imagenes/iconos/table.png"/>x No. planilla</a>
                                        </li>
                                        <li class="nav-item"><a href="<?php echo $r ?>modulos/bodega/conplafecha.php"
                                                                onclick="carga()"><img
                                                        src="<?php echo $r ?>imagenes/iconos/date.png"/>x Fecha</a>
                                        <li class="nav-item"><a href="<?php echo $r ?>modulos/bodega/conempresa.php"
                                                                onclick="carga()"><img
                                                        src="<?php echo $r ?>imagenes/iconos/empresa.png"/>x Empresa</a>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    <?php } ?>
                    <?php if ($rowlog['perbodrep'] == '1' || $_SESSION['id'] == '1022949677') { ?>
                        <li class="nav-item"><span tabindex="1"><img src="<?php echo $r ?>imagenes/iconos/report.png"/>Reportes</span>
                            <ul>
                                <li class="nav-item"><a href="<?php echo $r ?>modulos/bodega/reportes/condescarga.php"
                                                        onclick="carga()"><img
                                                src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Entregados</a>
                                </li>
                                <li class="nav-item"><a href="<?php echo $r ?>modulos/bodega/reportes/coninventario.php"
                                                        onclick="carga()"><img
                                                src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Inventario</a>
                            </ul>
                        </li>
                    <?php } ?>
                </ul>
            </li>
        <?php } ?>

        <?php if ($rowlog['pertesoreria'] == '1' or $_SESSION['id'] == '52440297' or $_SESSION['id'] == '1024499151' or $_SESSION['id'] == '1030539847') { ?>
            <li class="nav-item"><span tabindex="1"><img
                            src="<?php echo $r ?>imagenes/iconos/money.png"/>Tesoreria</span>
                <ul>
                    <?php if ($rowlog['pertesoreria'] == '1' or $_SESSION['id'] == '1024499151' or $_SESSION['id'] == '1030539847') { ?>
                        <li class="nav-item"><span tabindex="1"><img
                                        src="<?php echo $r ?>imagenes/iconos/money_add.png"/>Costos y Gastos</span>
                            <ul>
                                <li class="nav-item"><a href="<?php echo $r ?>modulos/tesoreria/insertar_cyg.php"
                                                        onclick="carga()"><img
                                                src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Insertar</a>
                                </li>

                                <?php if ($rowlog['pertesoreria'] == '1') { ?>
                                    <li class="nav-item"><a title="Confirmar Pago"
                                                            href="<?php echo $r ?>modulos/tesoreria/con_c_pago_cyg.php"
                                                            onclick="carga()"><img
                                                    src="<?php echo $r ?>imagenes/iconos/money_add.png"/>C. Pago</a>
                                    </li>
                                    <li class="nav-item"><a title="Confirmar Pago Masivo"
                                                            href="<?php echo $r ?>modulos/tesoreria/con_pagomasivo_cyg.php"
                                                            onclick="carga()"><img
                                                    src="<?php echo $r ?>imagenes/iconos/money_add.png"/>C.P Masivo</a>
                                    </li>
                                    <li class="nav-item"><a href="<?php echo $r ?>modulos/tesoreria/consultar_cyg.php"
                                                            onclick="carga()"><img
                                                    src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar</a></li>
                                <?php } ?>
                            </ul>
                        </li>
                    <?php } ?>
                    <?php if ($rowlog['pertesoreria'] == '1') { ?>
                        <li class="nav-item"><span tabindex="1"><img
                                        src="<?php echo $r ?>imagenes/iconos/money_add.png"/>Saldos de Banco</span>
                            <ul>
                                <li class="nav-item"><a href="<?php echo $r ?>modulos/tesoreria/con_cheques_sb.php"
                                                        onclick="carga()"><img title="Validar Cheques"
                                                                               src="<?php echo $r ?>imagenes/iconos/money_add.png"/>V.
                                        Cheques</a></li>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/tesoreria/consultar_cheques_sb.php"
                                            onclick="carga()"><img title="Consultar Cheques"
                                                                   src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar</a>
                                </li>
                            </ul>
                        </li>
                    <?php } ?>
                    <?php if ($rowlog['pertesoreria'] == '1') { ?>
                        <li class="nav-item"><span tabindex="1"><img src="<?php echo $r ?>imagenes/iconos/usuario.png"/>Nominas</span>
                            <ul>
                                <li class="nav-item"><a href="<?php echo $r ?>modulos/tesoreria/insernomina.php"
                                                        onclick="carga()"><img title="Insertar Nueva Nomina"
                                                                               src="<?php echo $r ?>imagenes/iconos/money_add.png"/>Insertar</a>
                                </li>
                                <li class="nav-item"><a href="<?php echo $r ?>modulos/tesoreria/consultarnomina.php"
                                                        onclick="carga()"><img title="Consultar Nominas"
                                                                               src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar</a>
                                </li>
                            </ul>
                        </li>
                    <?php } ?>
                    <?php if ($rowlog['pertesoreria'] == '1') { ?>
                        <li class="nav-item"><span tabindex="1"><img src="<?php echo $r ?>imagenes/iconos/car.png"/>Aux Movilizacion</span>
                            <ul>
                                <li class="nav-item"><a href="<?php echo $r ?>modulos/tesoreria/insertransporte.php"
                                                        onclick="carga()"><img title="Insertar Aux Movilizacion"
                                                                               src="<?php echo $r ?>imagenes/iconos/money_add.png"/>Insertar</a>
                                </li>
                                <li class="nav-item"><a href="<?php echo $r ?>modulos/tesoreria/consultartransporte.php"
                                                        onclick="carga()"><img title="Consultar Aux Movilizacion"
                                                                               src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar</a>
                                </li>
                            </ul>
                        </li>
                    <?php } ?>
                    <?php if ($rowlog['pertesoreria'] == '1' or $_SESSION['id'] == '52440297') { ?>
                        <li class="nav-item"><span tabindex="1"><img src="<?php echo $r ?>imagenes/iconos/script.png"/>Doc. Equivalente</span>
                            <ul>
                                <li class="nav-item"><a href="<?php echo $r ?>modulos/tesoreria/inserdocequi.php"
                                                        onclick="carga()"><img title="Insertar Documento Equivalente"
                                                                               src="<?php echo $r ?>imagenes/iconos/money_add.png"/>Insertar</a>
                                </li>
                                <li class="nav-item"><a href="<?php echo $r ?>modulos/tesoreria/consultardocequi.php"
                                                        onclick="carga()"><img title="Consultar Documento Equivalente"
                                                                               src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar</a>
                                </li>
                            </ul>
                        </li>
                    <?php } ?>
                    <?php if ($rowlog['pertesoreria'] == '1' or $_SESSION['id'] == '52440297') { ?>
                        <li class="nav-item"><span tabindex="1"><img
                                        src="<?php echo $r ?>imagenes/iconos/user_gray.png"/>Proveedores</span>
                            <ul>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/tesoreria/proveedores/insertar.php"
                                            onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar</a>
                                </li>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/tesoreria/proveedores/consultar.php"
                                            onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar</a>
                                </li>
                            </ul>
                        </li>
                    <?php } ?>
                    <?php if ($rowlog['pertesoreria'] == '1') { ?>
                        <li class="nav-item"><span tabindex="1"><img src="<?php echo $r ?>imagenes/iconos/empresa.png"/>Bancos</span>
                            <ul>
                                <li class="nav-item"><a href="<?php echo $r ?>modulos/tesoreria/bancos/insertar.php"
                                                        onclick="carga()"><img
                                                src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar</a></li>
                                <li class="nav-item"><a href="<?php echo $r ?>modulos/tesoreria/bancos/consultar.php"
                                                        onclick="carga()"><img
                                                src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar</a></li>
                            </ul>
                        </li>
                    <?php } ?>
                    <?php if ($rowlog['pertesoreria'] == '1') { ?>
                        <li class="nav-item"><span tabindex="1"><img src="<?php echo $r ?>imagenes/iconos/box.png"/>Mensajeria</span>
                            <ul>
                                <li class="nav-item"><a href="<?php echo $r ?>modulos/tesoreria/insertarmensa.php"
                                                        onclick="carga()"><img
                                                src="<?php echo $r ?>imagenes/iconos/add.png"/>Insertar</a></li>
                                <li class="nav-item"><a href="<?php echo $r ?>modulos/tesoreria/consultarmensa.php"
                                                        onclick="carga()"><img
                                                src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar</a></li>
                            </ul>
                        </li>
                    <?php } ?>
                    <?php if ($rowlog['pertesoreria'] == '1') { ?>
                        <li class="nav-item"><span tabindex="1"><img src="<?php echo $r ?>imagenes/iconos/report.png"/>Reportes</span>
                            <ul>
                                <li class="nav-item"><a href="<?php echo $r ?>modulos/tesoreria/con_planilla_diaria.php"
                                                        onclick="carga()"><img title="Planilla diaria de caja"
                                                                               src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>P.D
                                        de caja</a></li>
                            </ul>
                        </li>
                    <?php } ?>
                </ul>
            </li>
        <?php } ?>

        <?php if ($rowlog['percartera'] == '1' || $_SESSION['id'] == '1077034420' || $_SESSION['id'] == '1031128535') { ?>
            <li class="nav-item"><span tabindex="1"><img
                            src="<?php echo $r ?>imagenes/iconos/briefcase.png"/>Cartera</span>
                <ul>
                    <?php if ($rowlog['percarnot'] == '1') { ?>
                        <li class="nav-item"><span tabindex="1"><img src="<?php echo $r ?>imagenes/iconos/layout.png"/>Notas</span>
                            <ul>
                                <li class="nav-item"><a href="<?php echo $r ?>modulos/cartera/notas/NC/principal.php"
                                                        onclick="carga()"><img
                                                src="<?php echo $r ?>imagenes/iconos/layout_delete.png"/>N. Credito</a>
                                </li>
                                <li class="nav-item"><a href="<?php echo $r ?>modulos/cartera/notas/ND/principal.php"
                                                        onclick="carga()"><img
                                                src="<?php echo $r ?>imagenes/iconos/layout_add.png"/>N. Debito</a></li>
                                <li class="nav-item"><a href="<?php echo $r ?>modulos/cartera/notas/consultar.php"
                                                        onclick="carga()"><img
                                                src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar</a></li>
                            </ul>
                        </li>
                    <?php } ?>
                    <?php if ($rowlog['percarcon'] == '1') { ?>
                        <li class="nav-item"><a href="<?php echo $r ?>modulos/cartera/consultar.php"
                                                onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar
                                cartera</a></li>
                    <?php } ?>
                    <?php if ($rowlog['percarpro'] == '1') { ?>
                        <li class="nav-item"><span tabindex="1"><img
                                        src="<?php echo $r ?>imagenes/iconos/user_suit.png"/>Promotores</span>
                            <ul>
                                <li class="nav-item"><a href="<?php echo $r ?>modulos/cartera/promotores/conasignar.php"
                                                        onclick="carga()"><img
                                                src="<?php echo $r ?>imagenes/iconos/arrow_in.png"/>Asignar</a></li>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/cartera/promotores/conreasignar.php"
                                            onclick="carga()"><img
                                                src="<?php echo $r ?>imagenes/iconos/arrow_inout.png"/>Reasignar</a>
                                </li>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/cartera/promotores/condesasignar.php"
                                            onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/arrow_out.png"/>Desasignar</a>
                                </li>
                                <li class="nav-item"><a href="#" onclick="carga()"><img
                                                src="<?php echo $r ?>imagenes/iconos/arrow_switch.png"/>Traspaso
                                        promotor</a></li>
                                <!--        <li  class="nav-item"><a href="<?php echo $r ?>modulos/cartera/promotores/automatica.php" onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/database_gear.png" />Automatica</a></li>--->
                            </ul>
                        </li>
                    <?php } ?>
                    <?php if ($rowlog['percarpag'] == '1') { ?>
                        <li class="nav-item"><span tabindex="1"><img src="<?php echo $r ?>imagenes/iconos/money.png"/>Aplicar pago</span>
                            <ul>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/cartera/pagos/individual/consultar.php"
                                            onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/money_add.png"/>Individual</a>
                                </li>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/cartera/pagos/mascuenta/principal.php"
                                            onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/money_add.png"/>Masivos
                                        x cuenta</a></li>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/cartera/pagos/mascliente/principal.php"
                                            onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/money_add.png"/>Masivos
                                        x cliente</a></li>
                                <li class="nav-item"><span tabindex="1"><img
                                                src="<?php echo $r ?>imagenes/iconos/text_padding_top.png"/>Generar planos </span>
                                    <ul>
                                        <li class="nav-item"><a
                                                    href="<?php echo $r ?>modulos/cartera/pagos/planos/efecty/consultar.php"
                                                    onclick="carga()"><img
                                                        src="<?php echo $r ?>imagenes/iconos/efecty.png"/>Efecty</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    <?php } ?>
                    <?php if ($rowlog['percarmas'] == '1' || $_SESSION['id'] == '1077034420') { ?>
                        <li class="nav-item"><a href="<?php echo $r ?>modulos/cartera/consulta_masiva.php"
                                                onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consulta
                                Masiva</a></li>
                    <?php } ?>
                    <?php if ($rowlog['percarfec'] == '1') { ?>
                        <li class="nav-item"><a href="<?php echo $r ?>modulos/cartera/fecha/consultar.php"
                                                onclick="carga()"><img
                                        src="<?php echo $r ?>imagenes/iconos/calendar.png"/>Cambiar fecha</a></li>
                    <?php } ?>
                    <li class="nav-item"><a href="<?php echo $r ?>modulos/cartera/clinuevos.php" onclick="carga()"><img
                                    src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Clientes Nuevos</a>
                        <?php if ($rowlog['percarcas'] == '1') { ?>
                    <li class="nav-item"><a href="<?php echo $r ?>modulos/cartera/cartera_castigada.php"
                                            onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/punish.png"/>Cartera
                            castigada</a></li>
                <?php } ?>
                    <?php if ($rowlog['percardat'] == '1') { ?>
                        <li class="nav-item"><span tabindex="1"><img
                                        src="<?php echo $r ?>imagenes/iconos/datacredito.png"/>Datacredito</span>
                            <ul>
                                <li class="nav-item"><span tabindex="1"><img
                                                src="<?php echo $r ?>imagenes/iconos/text_padding_top.png"/>Generar plano</span>
                                    <ul>
                                        <li class="nav-item"><a
                                                    href="<?php echo $r ?>modulos/cartera/datacredito/principal1.php"
                                                    onclick="carga()"><img
                                                        src="<?php echo $r ?>imagenes/iconos/empresa.png"/>IdenCorp</a>
                                        </li>
                                        <li class="nav-item"><a
                                                    href="<?php echo $r ?>modulos/cartera/datacredito/principal2.php"
                                                    onclick="carga()"><img
                                                        src="<?php echo $r ?>imagenes/iconos/empresa.png"/>Coopincorp</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-item"><a href="<?php echo $r ?>modulos/cartera/datacredito/consultar.php"
                                                        onclick="carga()"><img
                                                src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consulta cuenta</a>
                            </ul>
                        </li>
                    <?php } ?>
                    <?php if ($rowlog['percarrep'] == '1') { ?>
                        <li class="nav-item"><span tabindex="1"><img src="<?php echo $r ?>imagenes/iconos/report.png"/>Reportes</span>
                            <ul>
                                <!-- <li  class="nav-item"><a href="<?php echo $r ?>modulos/cartera/reportes/conprevisto.php" onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png" />Previstos</a></li> -->
                                <!-- <li  class="nav-item"><a href="<?php echo $r ?>modulos/cartera/reportes/conniff.php" onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png" />NIFF</a></li> -->
                                <!-- <li  class="nav-item"><a href="<?php echo $r ?>modulos/cartera/reportes/conniffpagos.php" onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png" />NIFF PAGOS</a></li> -->
                                <li class="nav-item"><a href="<?php echo $r ?>modulos/cartera/reportes/conresumen.php"
                                                        onclick="carga()"><img
                                                src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Resumen
                                        cartera</a>
                                <li class="nav-item"><a href="<?php echo $r ?>modulos/cartera/reportes/lisresmoro.php"
                                                        onclick="carga()"><img
                                                src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Resumen
                                        morosidad</a></li>
                                <li class="nav-item"><a href="<?php echo $r ?>modulos/cartera/reportes/condetmoro.php"
                                                        onclick="carga()"><img
                                                src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Detalle
                                        morosidad</a></li>
                                <li class="nav-item"><a href="<?php echo $r ?>modulos/cartera/reportes/congestion.php"
                                                        onclick="carga()"><img
                                                src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Gestion
                                        Diaria</a></li>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/cartera/reportes/conpagocontado.php"
                                            onclick="carga()"><img
                                                src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Clientes
                                        Contado</a></li>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/cartera/reportes/concalificacion.php"
                                            onclick="carga()"><img
                                                src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Califi.
                                        Producto</a></li>
                                <!-- <li  class="nav-item"><a href="<?php echo $r ?>modulos/cartera/reportes/conmediosmagneticos.php" onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png" />M. Magneticos 1</a></li> -->
                                <?php if ($_SESSION['id'] == '1024540606' || $_SESSION['id'] == '1024511399') { ?>
                                    <li class="nav-item"><a
                                                href="<?php echo $r ?>modulos/cartera/reportes/congencartera.php"
                                                onclick="carga()"><img
                                                    src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Generar
                                            Cartera</a></li>
                                <?php } ?>
                                <!-- <li  class="nav-item"><a href="<?php echo $r ?>modulos/cartera/reportes/congenniff.php" onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png" />Gnerar NIFF MORA</a></li> -->
                                <!-- <li  class="nav-item"><a href="<?php echo $r ?>modulos/cartera/reportes/congenniffpagos.php" onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png" />Gnerar NIFF PAGOS</a></li> -->
                                <li class="nav-item"><a href="<?php echo $r ?>modulos/cartera/reportes/convamos1.php"
                                                        onclick="carga()"><img
                                                src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Como
                                        Vamos</a></li>
                                <li class="nav-item"><a href="<?php echo $r ?>modulos/cartera/deterioro.php"
                                                        onclick="carga()">
                                        <img src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Deterioro
                                        390</a></li>
                                <li class="nav-item"><a href="<?php echo $r ?>modulos/cartera/deterioroFin.php"
                                                        onclick="carga()">
                                        <img src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Deterioro
                                        final</a></li>
                                <li class="nav-item"><a href="<?php echo $r ?>modulos/cartera/Reportes/Consolidado.php"
                                                        onclick="carga()">
                                        <img src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>Consolidado</a>
                                </li>
                            </ul>
                        </li>
                    <?php } ?>
                    <?php if ($rowlog['percarpaz'] == '1' || $_SESSION['id'] == '1031128535') { ?>
                        <li class="nav-item"><a
                                    href="<?php echo $r ?>modulos/cartera/certificados/solicitarcertificado.php"
                                    onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/pdf.png"/>Paz y
                                Salvo</a></li>
                    <?php } ?>
                </ul>
            </li>
        <?php } ?>

        <?php if ($rowlog['percaja'] == '1' or $rowlog['usuid'] == '123456789') { ?>
            <li class="nav-item"><span tabindex="1"><img src="<?php echo $r ?>imagenes/iconos/coins.png"/>Caja</span>
                <ul>
                    <li class="nav-item"><a href="<?php echo $r ?>modulos/caja/consultar.php" onclick="carga()"><img
                                    src="<?php echo $r ?>imagenes/iconos/zoom.png"/>Consultar recibo</a></li>
                    <li class="nav-item"><span tabindex="1"><img src="<?php echo $r ?>imagenes/iconos/report.png"/>Reportes</span>
                        <ul>
                            <li class="nav-item"><a href="<?php echo $r ?>modulos/caja/reportes/conmediosmagneticos.php"
                                                    onclick="carga()"><img
                                            src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>M.
                                    Magneticos 1</a></li>
                            <li class="nav-item"><a
                                        href="<?php echo $r ?>modulos/caja/reportes/conmediosmagneticos_1.php"
                                        onclick="carga()"><img
                                            src="<?php echo $r ?>imagenes/iconos/application_form_magnify.png"/>M.
                                    Magneticos 2</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
        <?php } ?>

        <?php if ($rowlog['percomision'] == '1' || $rowlog['usustaff'] == '1' || $rowlog['usuid'] == '52765484' || $_SESSION['id'] == '20371048') { ?>
            <li class="nav-item"><span tabindex="1"><img
                            src="<?php echo $r ?>imagenes/iconos/valor.png"/>Comisiones</span>
                <ul>
                    <?php if ($rowlog['percominormal'] == '1' || $rowlog['usuid'] == '52765484') { ?>
                        <li class="nav-item"><span tabindex="1"><img src="<?php echo $r ?>imagenes/iconos/script.png"/>C. Normal</span>
                            <ul>
                                <li class="nav-item"><a href="<?php echo $r ?>modulos/comisiones/normal/asesor.php"
                                                        onclick="carga()"><img
                                                src="<?php echo $r ?>imagenes/iconos/database_gear.png"/>Asesor</a></li>
                                <li class="nav-item"><a href="<?php echo $r ?>modulos/comisiones/normal/director.php"
                                                        onclick="carga()"><img
                                                src="<?php echo $r ?>imagenes/iconos/database_gear.png"/>Director</a>
                                </li>
                                <li class="nav-item"><a href="<?php echo $r ?>modulos/comisiones/normal/gcomercial.php"
                                                        onclick="carga()"><img
                                                src="<?php echo $r ?>imagenes/iconos/database_gear.png"/>G.
                                        comercial</a></li>
                                <li class="nav-item"><a href="<?php echo $r ?>modulos/comisiones/normal/ggeneral.php"
                                                        onclick="carga()"><img
                                                src="<?php echo $r ?>imagenes/iconos/database_gear.png"/>G. general</a>
                                </li>
                                <li class="nav-item"><a href="<?php echo $r ?>modulos/comisiones/normal/adicional.php"
                                                        onclick="carga()"><img
                                                src="<?php echo $r ?>imagenes/iconos/database_gear.png"/>Adicional</a>
                                </li>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/comisiones/normal/relacionista.php"
                                            onclick="carga()"><img
                                                src="<?php echo $r ?>imagenes/iconos/database_gear.png"/>Relacionista</a>
                                </li>
                            </ul>
                        </li>
                    <?php } ?>
                    <?php if ($rowlog['percomilibranza'] == '1') { ?>
                        <li class="nav-item"><span tabindex="1"><img
                                        src="<?php echo $r ?>imagenes/iconos/script_code.png"/>C. Libranza</span>
                        </li>
                    <?php } ?>
                    <?php if ($rowlog['percomiestimulacion'] == '1') { ?>
                        <li class="nav-item"><span tabindex="1"><img
                                        src="<?php echo $r ?>imagenes/iconos/script_code_red.png"/>C. Estimulacion</span>
                        </li>
                    <?php } ?>
                    <?php if ($rowlog['percomiprop'] == '1' || $rowlog['usustaff'] == '1' || $_SESSION['id'] == '20371048') { ?>
                        <li class="nav-item"><span tabindex="1"><img src="<?php echo $r ?>imagenes/iconos/money.png"/>Mi Comision</span>
                            <ul>
                                <li class="nav-item"><span tabindex="2"><img
                                                src="<?php echo $r ?>imagenes/iconos/script.png"/>C. Normal</span>
                                    <ul>
                                        <li class="nav-item"><a
                                                    href="<?php echo $r ?>modulos/comisiones/normal/asesor_prop.php"
                                                    onclick="carga()"><img
                                                        src="<?php echo $r ?>imagenes/iconos/database_gear.png"/>Asesor</a>
                                        </li>
                                        <li class="nav-item"><a
                                                    href="<?php echo $r ?>modulos/comisiones/normal/relaci_prop.php"
                                                    onclick="carga()"><img
                                                        src="<?php echo $r ?>imagenes/iconos/database_gear.png"/>Relacionista</a>
                                        </li>
                                        <li class="nav-item"><a
                                                    href="<?php echo $r ?>modulos/comisiones/normal/adicio_prop.php"
                                                    onclick="carga()"><img
                                                        src="<?php echo $r ?>imagenes/iconos/database_gear.png"/>Adicional</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-item"><span tabindex="1"><img
                                                src="<?php echo $r ?>imagenes/iconos/script_code.png"/>C. Libranza</span>
                                </li>
                                <li class="nav-item"><span tabindex="1"><img
                                                src="<?php echo $r ?>imagenes/iconos/script_code_red.png"/>C. Estimulacion</span>
                                </li>
                            </ul>
                        </li>
                    <?php } ?>
                </ul>
            </li>
        <?php } ?>

        <?php if ($rowlog['perestadistico'] == '1' || $_SESSION['id'] == '1077034420' || $_SESSION['id'] == '52440297' || $_SESSION['id'] == '1110451657') { ?>
            <li class="nav-item"><span tabindex="1"><img src="<?php echo $r ?>imagenes/iconos/chart_bar.png"/>Estadisticos</span>
                <ul>
                    <?php if ($rowlog['perestavent'] == '1' or $rowlog['usuid'] == '1077034420') { ?>
                        <li class="nav-item"><span tabindex="1"><img
                                        src="<?php echo $r ?>imagenes/iconos/calculator.png"/>Ventas</span>
                            <ul>
                                <li class="nav-item"><a href="<?php echo $r ?>modulos/estadisticos/ventas/condetven.php"
                                                        onclick="carga()"><img
                                                src="<?php echo $r ?>imagenes/iconos/usuario.png"/>Detallado x
                                        asesor</a></li>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/estadisticos/ventas/condetvenrela.php"
                                            onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/usuario.png"/>Detallado
                                        x Rela</a></li>
                                <li class="nav-item"><a href="<?php echo $r ?>modulos/estadisticos/ventas/condetgru.php"
                                                        onclick="carga()"><img
                                                src="<?php echo $r ?>imagenes/iconos/group.png"/>Detallado x grupo</a>
                                </li>
                                <li class="nav-item"><a href="<?php echo $r ?>modulos/estadisticos/ventas/conresven.php"
                                                        onclick="carga()"><img
                                                src="<?php echo $r ?>imagenes/iconos/usuario.png"/>Resumen asesores</a>
                                </li>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/estadisticos/ventas/conresvenrela.php"
                                            onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/usuario.png"/>Resumen
                                        Relacioni</a></li>
                                <li class="nav-item"><a href="<?php echo $r ?>modulos/estadisticos/ventas/conresgru.php"
                                                        onclick="carga()"><img
                                                src="<?php echo $r ?>imagenes/iconos/group.png"/>Resumen grupos</a></li>
                                <li class="nav-item"><a href="<?php echo $r ?>modulos/estadisticos/ventas/conrespro.php"
                                                        onclick="carga()"><img
                                                src="<?php echo $r ?>imagenes/iconos/cd.png"/>Resumen Productos</a></li>
                            </ul>
                        </li>
                    <?php } ?>
                    <?php if ($rowlog['perestarepor'] == '1') { ?>
                        <li class="nav-item"><span tabindex="1"><img
                                        src="<?php echo $r ?>imagenes/iconos/calculator.png"/>Reportes</span>
                            <ul>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/estadisticos/reportes/conscreen.php"
                                            onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/group.png"/>Screen
                                        Asesores</a></li>
                                <li class="nav-item"><a
                                            href="<?php echo $r ?>modulos/estadisticos/reportes/conestado.php"
                                            onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/report.png"/>E.
                                        Pedidos</a></li>
                            </ul>
                        </li>
                    <?php } ?>
                    <li class="nav-item"><a href="<?php echo $r ?>modulos/estadisticos/ventas/conresvenrela.php"
                                            onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/usuario.png"/>Resumen
                            Relac</a></li>
                    <li class="nav-item"><a href="<?php echo $r ?>modulos/estadisticos/envio_correo.php"
                                            onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/email_go.png"/>Pedido
                            Virtual</a></li>
                </ul>
            </li>
        <?php } ?>

        <?php /* if($rowlog['percapacitadores'] == '1'){ ?>
	<?php if($rowlog['percapainser'] == '1'){ ?>
	<li  class="nav-item"><span tabindex="1"><img src="<?php echo $r ?>imagenes/iconos/user_green.png" />Capacitadores</span>
		<ul>
		<li  class="nav-item"><a href="<?php echo $r ?>modulos/capacitadores/insertar.php" onclick="carga()"><img src="<?php echo $r ?>imagenes/iconos/user_add.png" />Insertar</a></li>
		</ul>
	</li>
	<?php } ?>
<?php } */ ?>


    </ul>
</nav>
<script>
    /* var temp;
    $(".active-drop").hover(function(){
        var obj = this.dataset.dom;
        $("#"+obj).dropdown('toggle');
    });

    $(".active-drop").blur(function(){
        var obj = this.dataset.dom;
        $("#"+obj).dropdown('hide')
    });
    function active_drop(obj)
    {
        $("#"+obj).dropdown('show')
    } */
</script>

<script>
    var menu_options = $("li.nav-item.dropdown").length;
    var limit_show = 6;
    if (menu_options > 6) {
        for (i = limit_show; i < menu_options; i++)
            $("li.nav-item.dropdown").eq(i).css("display", "none");
    } else {
        $("#btn-menu-next").css("display", "none");
        $("#btn-menu-prev").css("display", "none");
    }
    var limit_hide = 7;
    var hide = <?php
        if (isset($_SESSION["active_option_menu"])) {
            echo $_SESSION["active_option_menu"];
        } else {
            echo 0;
        }
        ?>

        function actual_menu() {
            for (i = 0; i < hide; i++) {
                $("li.nav-item.dropdown").eq(i).css("display", "none");
                var show = limit_show + i;
                $("li.nav-item.dropdown").eq(show).css("display", "block");
            }
        }

    if (hide > 0)
        actual_menu();


    $("#btn-menu-next").click(function () {
        if (hide < limit_hide) {
            $("li.nav-item.dropdown").eq(hide).css("display", "none");
            var show = limit_show + hide;
            $("li.nav-item.dropdown").eq(show).css("display", "block");
            hide++;
            $.get('<?php echo $r ?>incluir/src/index_menu.php?index=' + hide);
        }
    });
    $("#btn-menu-prev").click(function () {
        if (hide > 0) {
            hide--;
            $("li.nav-item.dropdown").eq(hide).css("display", "block");
            var show = limit_show + hide;
            $("li.nav-item.dropdown").eq(show).css("display", "none");
            $.get('<?php echo $r ?>incluir/src/index_menu.php?index=' + hide);
        }
    });

    // Controla la opcion activa
    var actual = null;
    // Controla si se dio click en una opcion del nav
    var click_nav = false;
    $(".nav-link").click(function () {
        // Si actual ya posee un elemento, se oculpa para mostrar el nuevo
        if (actual != null)
            actual.css('display', 'none');

        // Asignamos el elemento .first-drop correspondiente
        actual = $(this).siblings(".first-drop");
        // mostramos las opciones del menu
        actual.css('display', 'block');

        click_nav = true;

        // Siempre que se de click en un elemento del menu debemos hacer unbind del la funcion click del body
        $("body").unbind("click");

        $("body").click(function () {
            if ($("body").hasClass("cerrar-menu") && !click_nav) {
                actual.css('display', 'none');
                $("body").removeClass("cerrar-menu");
                $("body").unbind("click");
            } else {
                $("body").addClass("cerrar-menu");
                click_nav = false;
            }
        });
    });

</script>