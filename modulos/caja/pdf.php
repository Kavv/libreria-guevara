<?php
$r = '../../';
require($r.'incluir/connection.php');
require($r.'incluir/fpdf/fpdf.php');
$empresa = $_GET['empresa'];
$numero = $_GET['numero'];
$recibo = $_GET['recibo'];
$fecha1 = $_GET['fecha1'];
$fecha2 = $_GET['fecha2'];
$con = 'SELECT * FROM (movimientos INNER JOIN empresas ON movempresa = empid) INNER JOIN fpagos ON movfpago = fpaid';
$ord = 'ORDER BY movfecha DESC';
if($empresa != '' && $numero == ''  && $recibo == '' && $fecha1 == '') $sql = "$con WHERE movprefijo = 'RC' AND movempresa = $empresa $ord";
elseif($empresa == '' && $numero != '' && $recibo == '' && $fecha1 == '') $sql = "$con WHERE movprefijo = 'RC' AND movdocumento LIKE '%$numero%' $ord";
elseif($empresa == '' && $numero == '' && $recibo != '' && $fecha1 == '') $sql = "$con WHERE movprefijo = 'RC' AND movnumero LIKE '%$recibo%' $ord";
elseif($empresa == '' && $numero == '' && $recibo == '' && $fecha1 != '') $sql = "$con WHERE movprefijo = 'RC' AND movfecha BETWEEN '$fecha1' AND '$fecha2' $ord";
elseif($empresa != '' && $numero != '' && $recibo == '' && $fecha1 == '') $sql = "$con WHERE movprefijo = 'RC' AND movempresa = $empresa AND movdocumento LIKE '%$recibo%' $ord";
elseif($empresa != '' && $numero == '' && $recibo != '' && $fecha1 == '') $sql = "$con WHERE movprefijo = 'RC' AND movempresa = $empresa AND movnumero LIKE '%$recibo%' $ord";
elseif($empresa != '' && $numero == '' && $recibo == '' && $fecha1 != '') $sql = "$con WHERE movprefijo = 'RC' AND movempresa = $empresa AND movfecha BETWEEN '$fecha1' AND '$fecha2' $ord";
$qry = $db->query($sql);
class PDF extends FPDF{
	function Header(){
		global $fecha1;
		global $fecha2;
    	$this->SetFont('LucidaConsole','',8);
		$this->Cell(0,5,date('Y/m/d H:i:s'),0,1);
		$this->SetFont('LucidaConsole','',10);
		$this->Cell(0,5,'------------------------------------------------------------------------------------------',0,1,'C');
		$this->Cell(0,5,'RECIBOS DE CAJA '.$fecha1.' AL '.$fecha2,0,1,'C');
		$this->Cell(0,5,'------------------------------------------------------------------------------------------',0,1,'C');
    	$this->Ln(1);
	}
	function Footer(){
    	$this->SetY(-15);
    	$this->SetFont('LucidaConsole','',10);
		$this->Cell(0,5,'------------------------------------------------------------------------------------------',0,1,'C');
		$this->SetFont('LucidaConsole','',8);
    	$this->Cell(0,5,'Pagina '.$this->PageNo().'/{nb}',0,0,'R');
	}
}
?>
</head>
<body>
<?php require($r.'incluir/src/login.php') ?>
<section id="principal">
<?php require($r.'incluir/src/cabeza.php') ?>
<?php require($r.'incluir/src/menu.php') ?>
<article id="cuerpo">
<article class="mapa">
<a href="#">Principal</a><div class="mapa_div"></div><a class="current">Cartera</a>
</article>
<article id="contenido">
<h2>Listado de recibos de caja</h2>
<table id="tabla">
<thead>
<tr>
<th>Empresa</th>
<th>cuenta</th>
<th>Recibo</th>
<th>Valor</th>
<th>Descuento</th>
<th>Fecha</th>
<th>Forma de pago</th>
<th></th>
</tr>
</thead>
<tbody>
<?php
$total = 0;
$descuento = 0;
while($row = $qry->fetch(PDO::FETCH_ASSOC)){
?>
<tr>
<td title="<?php echo $row['empid'].' '.$row['empdigito'] ?>"><?php echo $row['empnombre'] ?></td>
<td align="center"><?php echo $row['movdocumento'] ?></td>
<td align="center"><?php echo 'RC-'.$row['movnumero'] ?></td>
<td align="right"><?php echo number_format($row['movvalor'],0,',','.') ?></td>
<td align="right"><?php echo number_format($row['movdescuento'],0,',','.') ?></td>
<td align="center"><img src="<?php echo $r ?>imagenes/iconos/date.png" title="<?php echo $row['movfecha'] ?>" /></td>
<td><?php echo $row['fpanombre'] ?></td>
<td align="center"><img src="<?php echo $r ?>imagenes/iconos/pdf.png" class="pdf" data-rel="../cartera/pdfrc.php?id1=<?php echo $row['movempresa'].'&id2='.$row['movnumero'] ?>" /></td>
</tr>
<?php
	$total = $total + $row['movvalor'];
	$descuento = $descuento + $row['movdescuento'];
}
?>
</tbody>
<tfoot>
<tr>
<td colspan="3"></td>
<td align="right" bgcolor="#D1CFCF"><?php echo number_format($total,0,',','.') ?></td>
<td align="right" bgcolor="#D1CFCF"><?php echo number_format($descuento,0,',','.') ?></td>
<td colspan="3"></td>
</tr>
</tfoot>
</table>
<p class="boton">
<button type="button" class="btnatras" onClick=" carga(); location.href = 'consultar.php'">atras</button>
</p>
</article>
</article>
<?php require($r.'incluir/src/pie.php') ?>
</section>
<div id="modal" style="display:none"></div>
</body>
</html>