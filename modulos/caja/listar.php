<?php
$r = '../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
require($r . 'incluir/funciones.php');

$empresa = $numero = $recibo = $fecha1 = $fecha2 = "";
if (isset($_POST['consultar'])) {
	$empresa = $_POST['empresa'];
	$numero = $_POST['numero'];
	$recibo = $_POST['recibo'];
	$fecha1 = $_POST['fecha1'];
	$fecha2 = $_POST['fecha2'];
} elseif(isset($_GET['empresa'])) {
	$empresa = $_GET['empresa'];
	$numero = $_GET['numero'];
	$recibo = $_GET['recibo'];
	$fecha1 = $_GET['fecha1'];
	$fecha2 = $_GET['fecha2'];
}
if ($fecha1 == '') {
	$error = 'Debe rellenar al menos el rango de fecha';
	header('Location:consultar.php?error=' . $error);
	exit();
}
$filtro = 'empresa=' . $empresa . '&numero=' . $numero . '&recibo=' . $recibo . '&fecha1=' . $fecha1 . '&fecha2=' . $fecha2;

$con = 'SELECT * FROM (movimientos INNER JOIN empresas ON movempresa = empid) ';
$ord = 'ORDER BY movfecha DESC';

$sql = crearConsulta(
	$con, $ord, 
	array(1,"movprefijo = 'RC'"),
	array($empresa, "movempresa LIKE '%$empresa%'"), 
	array($numero, "movdocumento = '$numero'"), 
	array($recibo, "movnumero = '$recibo'"),
	array($fecha1, "movfecha BETWEEN '$fecha1' AND '$fecha2'"));


	$qry = $db->query($sql);


?>
<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>

	<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
	<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#tabla').dataTable({
				'bJQueryUI': true,
				'sPaginationType': 'full_numbers',
				'oLanguage': {
					'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
				},
				'bSort': false,
			});
			$('.pdf').click(function() {
				newSrc = $(this).attr('data-rel');
				if (validation_file(newSrc) == 200)
					$('#modal').html("<iframe src='" + newSrc + "' width='100%' height='100%'></iframe>");
				else
					$('#modal').html("<spam>El PDF no fue encontrado</spam>");
				$('#modal').dialog({
					modal: true,
					width: '600',
					height: '800',
					title: 'PDF del documento'
				});
			});
		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a class="current">Cartera</a>
			</article>
			<!-- <div class="reporte">
				<img style="margin:0 3px;" src="../../imagenes/iconos/pdf.png" class="pdf" data-rel="pdf.php?<?php echo $filtro ?>" title="PDF de la Solicitud">
			</div> -->
			<article id="contenido">
				<h2>Listado de recibos de caja <?php ?></h2>
				<table id="tabla">
					<thead>
						<tr>
							<th>Empresa</th>
							<th>Factura</th>
							<th>Recibo</th>
							<th>Valor</th>
							<th>Descuento</th>
							<th>Fecha</th>
							<th>Forma de pago</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php
						$total = 0;
						$descuento = 0;
						while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
						?>
							<tr>
								<td title="<?php echo $row['empid'] . ' ' . $row['empdigito'] ?>"><?php echo $row['empnombre'] ?></td>
								<td align="center"><?php echo $row['movdocumento'] ?></td>
								<td align="center"><?php echo 'RC-' . $row['movnumero'] ?></td>
								<td align="right"><?php echo number_format($row['movvalor'], 2) ?></td>
								<td align="right"><?php echo number_format($row['movdescuento'], 2) ?></td>
								<td align="center"><img src="<?php echo $r ?>imagenes/iconos/date.png" title="<?php echo $row['movfecha'] ?>" /></td>
								<td align="center">
									<?php
										$qrynompago = $db->query("SELECT * FROM fpagos WHERE fpaid = '" . $row['movfpago'] . "';");
										$rownompago = $qrynompago->fetch(PDO::FETCH_ASSOC);
										echo $rownompago['fpanombre'];
									?>
								</td>
								<td align="center"><img src="<?php echo $r ?>imagenes/iconos/pdf.png" class="pdf" data-rel="../cartera/pdfrc.php?id1=<?php echo $row['movempresa'] . '&id2=' . $row['movnumero'] ?>" /></td>
							</tr>
						<?php
							$total = $total + $row['movvalor'];
							$descuento = $descuento + $row['movdescuento'];
						}
						?>
					</tbody>
					<tfoot>
						<tr>
							<td colspan="3"></td>
							<td align="right" bgcolor="#D1CFCF"><?php echo number_format($total, 2) ?></td>
							<td align="right" bgcolor="#D1CFCF"><?php echo number_format($descuento, 2) ?></td>
							<td colspan="3"></td>
						</tr>
					</tfoot>
				</table>
				<p class="boton">
					<button type="button" class="btn btn-primary btnatras" onClick=" carga(); location.href = 'consultar.php'">atras</button>
				</p>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<div id="modal" style="display:none"></div>
</body>

</html>