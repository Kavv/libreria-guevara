<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
?>
<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>

	<script type="text/javascript">
		$(document).ready(function() {
			$('#fecha1').datepicker({
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
			});
			$('#fecha2').datepicker({
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
			});
		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Cartera</a>
				<div class="mapa_div"></div><a class="current">Reportes</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="lismediosmagneticos_1.php" method="post">
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">Reporte - Medios Magneticos Menores a 1,000,000</legend>
						<p>
							<label for="empresa">Empresa:</label>
							<select name="empresa" class="validate[required]">
								<option value="">Selecciona</option>
								<?php
								$qry = $db->query("SELECT * FROM empresas ORDER BY empnombre");
								while ($row = $qry->fetch(PDO::FETCH_ASSOC))
									echo '<option value=' . $row['empid'] . '>' . $row['empnombre'] . '</option>';
								?>
							</select>
						</p>
						<p>
							<label for="fechas">Intervalo:</label>
							<input type="text" name="fecha1" id="fecha1" class="validate[required] text-input fecha" /> 
							<input type="text" name="fecha2" id="fecha2" class="validate[required] text-input fecha" />
						</p>
						<div class="row d-flex justify-content-center">
							<div class="col-md-12 col-lg-6 text-center">
								<button type="submit" class="btnconsulta btn btn-primary mt-2" name="consultar" value="Buscar">consultar</button> <!-- BOTON CONSULTAR -->
							</div>
						</div>
					</fieldset>
				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
</body>

</html>