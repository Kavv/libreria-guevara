<?php
	$r = '../../../';
	require($r . 'incluir/session.php');
	require($r . 'incluir/connection.php');
	$fecha1 = $_POST['fecha1'];
	$fecha2 = $_POST['fecha2'];
	$empresa = $_POST['empresa'];

	$filtro = 'fecha1=' . $fecha1 . '&fecha2=' . $fecha2 . '&empresa=' . $empresa;

	$sql = "SELECT * FROM movimientos WHERE movprefijo IN ('RC') AND DATE(movfecha) BETWEEN '" . $fecha1 . "' AND '" . $fecha2 . "' AND movempresa = '" . $empresa . "' ;";
	$qry = $db->query($sql);

	$qryempresa = $db->query("SELECT * FROM empresas WHERE empid = '" . $empresa . "';");
	$rowempresa = $qryempresa->fetch(PDO::FETCH_ASSOC);

?>
<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>

	<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
	<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#tabla').dataTable({
				'bJQueryUI': true,
				'bSort': false,
				'sPaginationType': 'full_numbers',
				'oLanguage': {
					'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
				},
			});
			$('.historico').click(function() {
				newHref = $(this).attr('data-rel');
				$('#dialog2').load(newHref).dialog({
					modal: true,
					width: 800
				});
			});
		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Cartera</a>
				<div class="mapa_div"></div><a class="current">Reportes</a>
			</article>
			<article id="contenido">
				<h2 style="text-align:center;"> FORMATO 1007 - Medios magneticos entre el invervalo del <?php echo $fecha1 . ' al ' . $fecha2 ?> menores a 1.000.000 de la empresa <?php echo $rowempresa['empnombre'] ?></h2>
				<div class="reporte">
					<a href="pdfmediosmagneticos_1.php?<?php echo $filtro ?>">
						<img src="<?php echo $r ?>imagenes/iconos/pdf.png" title="pdf" />
					</a> 
				</div>
				<table id="tabla">
					<thead>
						<tr>
							<th>Concepto</th>
							<th>Numero de Identificacion</th>
							<th>1er Apellido</th>
							<th>2do Apellido</th>
							<th>1er Nombre</th>
							<th>2do Nombre</th>
							<th>Razon Social</th>
							<th>Pais</th>
							<th title="Ingresos Recibidos">I.R</th>
							<th>Financiacion</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$total_u = 0;
						$mayores_to = 0;
						while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
							$sql1 = "SELECT * FROM clientes WHERE cliid = '" . $row['movtercero'] . "' ";
							$qry1 = $db->query($sql1);
							$row1 = $qry1->fetch(PDO::FETCH_ASSOC);
							/*
							$sql2 = "SELECT * FROM solicitudes WHERE solfactura = '".$row['movdocumento']."'; ";
							$qry2 = $db->query($sql2);
							$row2 = $qry2->fetch(PDO::FETCH_ASSOC);
							*/
							$valor_total = ($row['movvalor'] + $row['movfinan']);

							if (!empty($row['movfinan'])) {
								$financiacion = $row['movfinan'];
							} else {
								$financiacion = 0;
							}

							$total_u = $total_u + $valor_total;

							if ($valor_total <= 1000000) {
						?>
								<tr>
									<td align="center"><?php echo "4001"; ?></td>
									<td align="center"><?php echo $row1['cliid'] ?></td>
									<td align="center"><?php echo $row1['cliape1'] ?></td>
									<td align="center"><?php echo $row1['cliape2'] ?></td>
									<td align="center"><?php if ($row1['clitide'] == 1) {
															echo $row1['clinombre'];
														} ?></td>
									<td align="center"><?php echo $row1['clinom2'] ?></td>
									<td align="center"><?php if ($row1['clitide'] == 2) {
															echo $row1['clinombre'];
														} ?></td>
									<td align="center">521</td>
									<td align="center"><?php echo number_format($valor_total, 2); ?></td>
									<td align="center"><?php echo $financiacion ?></td>
								</tr>
						<?php
							} else {
								$mayores_to = $mayores_to + $valor_total;
							}
						}
						$total_fin =  $total_u;
						?>
						<tr>
							<td align="center">4001</td>
							<td align="center"></td>
							<td align="center"></td>
							<td align="center"></td>
							<td align="center">cuentas Mayores</td>
							<td align="center"></td>
							<td align="center"></td>
							<td align="center">521</td>
							<td align="center"><?php echo number_format($mayores_to, 2); ?></td>
							<td align="center"></td>
						</tr>
					<tfoot>
						<tr>
							<td align="center"></td>
							<td align="center"></td>
							<td align="center"></td>
							<td align="center"></td>
							<td align="center"></td>
							<td align="center"></td>
							<td align="center"></td>
							<td align="center"></td>
							<td align="center"><?php echo number_format($total_fin, 2); ?></td>
							<td align="center"></td>
						</tr>
					</tfoot>
					</tbody>
				</table>
				<p class="boton">
					<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href='conmediosmagneticos_1.php'">Atras</button>
				</p>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<div id="dialog2" title="Historico de solicitudes" style="display:none"></div>
</body>

</html>