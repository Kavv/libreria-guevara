<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
require($r . 'incluir/funciones.php');
require($r . 'incluir/fpdf/fpdf.php');

$fecha1 = $_GET['fecha1'];
$fecha2 = $_GET['fecha2'];
$empresa = $_GET['empresa'];

$sql = "SELECT * FROM movimientos WHERE movprefijo IN ('RC') AND DATE(movfecha) BETWEEN '" . $fecha1 . "' AND '" . $fecha2 . "' AND movempresa = '" . $empresa . "' ;";
$qry = $db->query($sql);

$qryempresa = $db->query("SELECT * FROM empresas WHERE empid = '" . $empresa . "';");
$rowempresa = $qryempresa->fetch(PDO::FETCH_ASSOC);

$nombre_empresa = $rowempresa['empnombre'];

class PDF extends FPDF
{
	function Header()
	{
		global $fecha1;
		global $fecha2;
		global $nombre_empresa;
		$this->SetFont('LucidaConsole', '', 6);
		$this->Cell(50, 5, date('Y/m/d'), 0, 1);
		$this->SetFont('LucidaConsole', '', 8);
		$this->Cell(280, 5, '--------------------------------------------------------------------------------------------------------------------------------------------------------------------', 0, 1, 'C');
		$this->Cell(280, 5, 'MEDIOS MAGNETICOS ENTRE EL INVERVALO DEL ' . $fecha1 . ' AL ' . $fecha2 . ' MENORES A UN MILLON DE LA EMPRESA ' . $nombre_empresa . ' ', 0, 1, 'C');
		$this->Cell(280, 5, '--------------------------------------------------------------------------------------------------------------------------------------------------------------------', 0, 1, 'C');
		$this->Ln(5);
	}

	function Footer()
	{
		$this->SetY(-15);
		$this->SetFont('LucidaConsole', '', 8);
		$this->Cell(0, 5, '----------------------------------------------------------------------------------------------------------------------------------------------------------------------', 0, 1, 'C');
		$this->SetFont('LucidaConsole', '', 8);
		$this->Cell(0, 5, 'Pagina ' . $this->PageNo() . '/{nb}', 0, 0, 'R');
	}
}

$pdf = new PDF();
$pdf->AddFont('LucidaConsole', '', 'lucon.php');
$pdf->AliasNbPages();
$pdf->AddPage('L');

$pdf->SetFont('LucidaConsole', '', 8);
$pdf->SetFillColor(220, 220, 220);
$pdf->SetX(8);
// Suma total 260
$pdf->Cell(25, 5, 'CONCEPTO', 1, 0, 'C', true);
$pdf->Cell(30, 5, 'NUM IDEN', 1, 0, 'C', true);
$pdf->Cell(30, 5, '1 APELLIDO', 1, 0, 'C', true);
$pdf->Cell(30, 5, '2 APELLIDO', 1, 0, 'C', true);
$pdf->Cell(30, 5, '1 NOMBRE', 1, 0, 'C', true);
$pdf->Cell(30, 5, '2 NOMBRE', 1, 0, 'C', true);
$pdf->Cell(30, 5, 'RAZON SOCIAL', 1, 0, 'C', true);
$pdf->Cell(15, 5, 'PAIS', 1, 0, 'C', true);
$pdf->Cell(25, 5, 'I.R', 1, 0, 'C', true);
$pdf->Cell(25, 5, 'FINANCIACION', 1, 1, 'C', true);

$i = 1;
$total_u = 0;
$mayores_to = 0;

while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
	$sql1 = "SELECT * FROM clientes WHERE cliid = '" . $row['movtercero'] . "' ";
	$qry1 = $db->query($sql1);
	$row1 = $qry1->fetch(PDO::FETCH_ASSOC);
	/*
	$sql2 = "SELECT * FROM solicitudes WHERE solfactura = '".$row['movdocumento']."'; ";
	$qry2 = $db->query($sql2);
	$row2 = $qry2->fetch(PDO::FETCH_ASSOC);
	*/
	$valor_total = ($row['movvalor'] + $row['movfinan']);

	if (!empty($row['movfinan'])) {
		$financiacion = $row['movfinan'];
	} else {
		$financiacion = 0;
	}

	$total_u = $total_u + $valor_total;

	if ($valor_total <= 1000000) {
		$nombre_re = "";
		$razon_social = "";
		if ($row1['clitide'] == 1) {
			$nombre_re = $row1['clinombre'];
		}
		if ($row1['clitide'] == 2) {
			$razon_social = $row1['clinombre'];
		}
		$pdf->SetFont('LucidaConsole', '', 7);
		if ($i % 2 == 0) $x = 255;
		else $x = 235;
		$pdf->SetFillColor($x, $x, $x);
		$pdf->SetX(8);
		$pdf->Cell(25, 5, '4001', 1, 0, 'L', true);
		$pdf->Cell(30, 5, $row1['cliid'], 1, 0, 'C', true);
		$pdf->Cell(30, 5, $row1['cliape1'], 1, 0, 'C', true);
		$pdf->Cell(30, 5, $row1['cliape2'], 1, 0, 'C', true);
		$pdf->Cell(30, 5, $nombre_re, 1, 0, 'C', true);
		$pdf->Cell(30, 5, $row1['clinom2'], 1, 0, 'C', true);
		$pdf->Cell(30, 5, $razon_social, 1, 0, 'C', true);
		$pdf->Cell(15, 5, '521', 1, 0, 'C', true);
		$pdf->Cell(25, 5, number_format($valor_total, 2), 1, 0, 'C', true);
		$pdf->Cell(25, 5, $financiacion, 1, 1, 'C', true);
		$i++;
	} else {
		$mayores_to = $mayores_to + $valor_total;
	}
}
$total_fin =  $total_u;

$pdf->SetFont('LucidaConsole', '', 7);
if ($i % 2 == 0) $x = 255;
else $x = 235;
$pdf->SetFillColor($x, $x, $x);
$pdf->SetX(8);
$pdf->Cell(25, 5, '4001', 1, 0, 'L', true);
$pdf->Cell(30, 5, '', 1, 0, 'C', true);
$pdf->Cell(30, 5, '', 1, 0, 'C', true);
$pdf->Cell(30, 5, '', 1, 0, 'C', true);
$pdf->Cell(30, 5, 'cuentas Mayores', 1, 0, 'C', true);
$pdf->Cell(30, 5, '', 1, 0, 'C', true);
$pdf->Cell(30, 5, '', 1, 0, 'C', true);
$pdf->Cell(15, 5, '521', 1, 0, 'C', true);
$pdf->Cell(25, 5, number_format($mayores_to, 2), 1, 0, 'C', true);
$pdf->Cell(25, 5, '', 1, 1, 'C', true);
$i++;

$pdf->SetFont('LucidaConsole', '', 7);
if ($i % 2 == 0) $x = 255;
else $x = 235;
$pdf->SetFillColor($x, $x, $x);
$pdf->SetX(8);
$pdf->Cell(25, 5, '', 1, 0, 'L', true);
$pdf->Cell(30, 5, '', 1, 0, 'C', true);;
$pdf->Cell(30, 5, '', 1, 0, 'C', true);
$pdf->Cell(30, 5, '', 1, 0, 'C', true);
$pdf->Cell(30, 5, '', 1, 0, 'C', true);
$pdf->Cell(30, 5, '', 1, 0, 'C', true);
$pdf->Cell(30, 5, '', 1, 0, 'C', true);
$pdf->Cell(15, 5, '', 1, 0, 'C', true);
$pdf->Cell(25, 5, number_format($total_fin, 2), 1, 0, 'C', true);
$pdf->Cell(25, 5, '', 1, 1, 'C', true);
$i++;


$pdf->Output('Medios magneticos entre el invervalo del ' . $fecha1 . ' a ' . $fecha2 . ' Mayores a un millon.pdf', 'd');
