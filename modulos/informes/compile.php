<?php

$r = '../../';

require($r . 'incluir/phpjasper/src/PHPJasper.php');

use PHPJasper\PHPJasper;


$folderUrl = __DIR__ . '/files/';
$folders = scandir($folderUrl);

foreach ($folders as $file) {
    $extension = pathinfo($file, PATHINFO_EXTENSION);

    if ($extension == 'jrxml') {

        $jasper = new PHPJasper;
        $jxrmlFileName = __DIR__ . '/files/' . $file;

        echo 'Compilando ' . $jxrmlFileName;
        echo '<br/>';
        $jasper->compile($jxrmlFileName)->execute();
    }
}

//echo '<br/>';
//echo '--------------- COMPILA LOS SUB REPORTES SI ES NECESARIO CON LO SIGUIENTE -----------------------------';
//echo '<br/>';
//
//
//$folderUrl = '../../incluir/phpjasper/bin/jasperstarter/bin/Sub/';
//$folders = scandir($folderUrl);
//
//foreach ($folders as $file) {
//    $extension = pathinfo($file, PATHINFO_EXTENSION);
//
//    if ($extension == 'jrxml') {
//
//        $jasper = new PHPJasper;
//        $jxrmlFileName = '../../incluir/phpjasper/bin/jasperstarter/bin/Sub/' . $file;
//
//        $jasper->compile($jxrmlFileName)->printOutput();
//        echo '<br/>';
//    }
//}