<?php

$r = '../../';

require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');


?>
<!doctype html>
<html lang="es">
<head>
    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>
    <link rel="stylesheet" type="text/css"
          href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css"/>
    <script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#tabla').dataTable({
                'bJQueryUI': true,
                'sPaginationType': 'full_numbers',
                'oLanguage': {
                    'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
                },
            });
        });
    </script>
</head>
<body>

<?php require($r . 'incluir/src/login.php') ?>
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <?php require($r . 'incluir/src/menu.php') ?>
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a href="#">Informes</a>
            <div class="mapa_div"></div>
            <a class="current">Lista de informes</a>
        </article>
        <article id="contenido">
            <div class="reporte">

            </div>
            <table id="tabla">
                <thead>
                <tr>
                    <th>Consulta</th>
                    <th>Concepto</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $perfil = $rowlog['perid'];
                $qryConsultas = $db->query("SELECT idiid, idinombre, ididescripcion FROM perfil_reporte INNER JOIN informesdinamicos ON reporte = idiid WHERE perfil = $perfil AND tipo = 2 AND hide is false ORDER BY idinombre ASC");
                while ($row = $qryConsultas->fetch(PDO::FETCH_ASSOC)) {

                    ?>
                    <tr>
                        <td title="<?php echo $row['idiid'] ?>">
                            <a onclick="go_to_report(<?php echo $row['idiid'] ?>);" style="cursor:pointer; color:black;"> <?php echo $row['idinombre'] ?> </a>
                        </td>
                        <td><?php echo $row['ididescripcion'] ?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </article>
    </article>
</body>
<script>
function go_to_report(reporte)
{
    var url = " <?php echo $r . 'modulos/informes/informe.php?id='?>" + reporte;
    // Abrir nuevo tab
    var win = window.open(url, '_blank');
    // Cambiar el foco al nuevo tab (punto opcional)
    win.focus();
}
</script>