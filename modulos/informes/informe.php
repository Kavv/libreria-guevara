<?php

$r = '../../';

require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

$idiId = $_GET['id'];

$nombre = $db->query("select idiNombre from informesdinamicos where idiId = $idiId")->fetchColumn();


$consultaSql = "select parametrodeinformedinamico.* " .
    "from informesdinamicos " .
    "         inner join parametrodeinformedinamico " .
    "                    on informesdinamicos.idiId = parametrodeinformedinamico.pidIdConsultaDinamica " .
    "where informesdinamicos.idiId = $idiId; ";

$qryParametros = $db->query($consultaSql);


?>

<!doctype html>
<html>
<head>

    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>
    <script type="text/javascript"
            src="<?php echo $r ?>incluir/jquery/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>

    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css"/>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

    <script type="text/javascript">

        $(document).ready(function () {

            // Reescribiendo carga, para no mostrar el modal de carga

            carga = function () {
            }

            $('.fecha').each(function (i, obj) {
                $(obj).datepicker({
                    dateFormat: 'yy-mm-dd',
                    changeMonth: true,
                    changeYear: true
                });
            });

            $('.selectpicker').each(function (i, obj) {
                $(obj).selectpicker();
            });

        });
    </script>
</head>
<body>
<?php require($r . 'incluir/src/login.php') ?>
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <?php require($r . 'incluir/src/menu.php') ?>
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a href="#">Consultas</a>
            <div class="mapa_div"></div>
            <a href="#"><?php echo $nombre ?></a>
        </article>
        <article id="contenido">
            <form id="form" name="form" action="informehelper.php" method="post">
                <fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
                    <legend class="ui-widget ui-widget-header ui-corner-all">Parametros</legend>
                    <p>
                        <input type="hidden" value="<?php echo $idiId ?>" name="idiid">
                    </p>


                    <?php
                    $consultaParametrosTipoCatalogo = "";
                    while ($row = $qryParametros->fetch(PDO::FETCH_ASSOC)) {

                        switch ($row['pidtipodeinput']) {
                            case "inpMoneda":
                                $consultaMonedas = "select * from Monedas order by espordefecto desc;";

                                $qryParametrosTipoCatalogo = $db->query($consultaMonedas);

                                ?>
                                <label for="<?php echo $row['pidnombreparametro'] ?>"><?php echo $row['piddescripcion'] ?></label>
                                <select id="<?php echo $row['pidnombreparametro'] ?>"
                                        name="<?php echo $row['pidnombreparametro'] ?>"
                                        class="<?php echo $row['pidclases'] ?> selectpicker"
                                        data-live-search="true">

                                    <?php
                                    while ($rowIn = $qryParametrosTipoCatalogo->fetch(PDO::FETCH_ASSOC)) {
                                        ?>
                                        <option value="<?php echo $rowIn['simbolo'] ?>"><?php echo $rowIn['nombre'] ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <?php
                                break;
                            case "inpEstado":

                                $consultaParametrosTipoCatalogo = "select estaid,estadescripcion from estadoscartera;";

                                $qryParametrosTipoCatalogo = $db->query($consultaParametrosTipoCatalogo);

                                ?>
                                <label for="<?php echo $row['pidnombreparametro'] ?>"><?php echo $row['piddescripcion'] ?></label>
                                <select id="<?php echo $row['pidnombreparametro'] ?>"
                                        name="<?php echo $row['pidnombreparametro'] ?>"
                                        class="<?php echo $row['pidclases'] ?> selectpicker"
                                        data-live-search="true">

                                    <?php
                                    while ($rowIn = $qryParametrosTipoCatalogo->fetch(PDO::FETCH_ASSOC)) {
                                        ?>
                                        <option value="<?php echo $rowIn['estaid'] ?>"><?php echo $rowIn['estadescripcion'] ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <?php
                                break;
                            case "inpEmpresa":

                                $consultaParametrosTipoCatalogo = "select empId,empNombre from empresas;";

                                $qryParametrosTipoCatalogo = $db->query($consultaParametrosTipoCatalogo);

                                ?>
                                <label for="<?php echo $row['pidnombreparametro'] ?>"><?php echo $row['piddescripcion'] ?></label>
                                <select id="<?php echo $row['pidnombreparametro'] ?>"
                                        name="<?php echo $row['pidnombreparametro'] ?>"
                                        class="<?php echo $row['pidclases'] ?> selectpicker"
                                        data-live-search="true">

                                    <?php
                                    while ($rowIn = $qryParametrosTipoCatalogo->fetch(PDO::FETCH_ASSOC)) {
                                        ?>
                                        <option value="<?php echo $rowIn['empid'] ?>"><?php echo $rowIn['empnombre'] ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <?php
                                break;
                            case "inpUsuario":

                                $consultaParametrosTipoCatalogo = "select usuId,usuNombre from usuarios;";

                                $qryParametrosTipoCatalogo = $db->query($consultaParametrosTipoCatalogo);

                                ?>
                                <label for="<?php echo $row['pidnombreparametro'] ?>"><?php echo $row['piddescripcion'] ?></label>
                                <select id="<?php echo $row['pidnombreparametro'] ?>"
                                        name="<?php echo $row['pidnombreparametro'] ?>"
                                        class="<?php echo $row['pidclases'] ?> selectpicker"
                                        data-live-search="true">

                                    <?php
                                    while ($rowIn = $qryParametrosTipoCatalogo->fetch(PDO::FETCH_ASSOC)) {
                                        ?>
                                        <option value="<?php echo $rowIn['usuid'] ?>"><?php echo $rowIn['usuid'] . ' / ' . $rowIn['usunombre'] ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <?php
                                break;
                            case "inpAsesor":

                                $consultaParametrosTipoCatalogo = "select usuId,usuNombre from usuarios where usuasesor = 1;";

                                $qryParametrosTipoCatalogo = $db->query($consultaParametrosTipoCatalogo);

                                ?>
                                <label for="<?php echo $row['pidnombreparametro'] ?>"><?php echo $row['piddescripcion'] ?></label>
                                <select id="<?php echo $row['pidnombreparametro'] ?>"
                                        name="<?php echo $row['pidnombreparametro'] ?>"
                                        class="<?php echo $row['pidclases'] ?> selectpicker"
                                        data-live-search="true">

                                    <?php
                                    while ($rowIn = $qryParametrosTipoCatalogo->fetch(PDO::FETCH_ASSOC)) {
                                        ?>
                                        <option value="<?php echo $rowIn['usuid'] ?>"><?php echo $rowIn['usuid'] . ' / ' . $rowIn['usunombre'] ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <?php
                                break;
                            case "inpCobrador":

                                $consultaParametrosTipoCatalogo = "select usuId,usuNombre from usuarios where usurelacionista = 1;";

                                $qryParametrosTipoCatalogo = $db->query($consultaParametrosTipoCatalogo);

                                ?>
                                <label for="<?php echo $row['pidnombreparametro'] ?>"><?php echo $row['piddescripcion'] ?></label>
                                <select id="<?php echo $row['pidnombreparametro'] ?>"
                                        name="<?php echo $row['pidnombreparametro'] ?>"
                                        class="<?php echo $row['pidclases'] ?> selectpicker"
                                        data-live-search="true">

                                    <?php
                                    while ($rowIn = $qryParametrosTipoCatalogo->fetch(PDO::FETCH_ASSOC)) {
                                        ?>
                                        <option value="<?php echo $rowIn['usuid'] ?>"><?php echo $rowIn['usuid'] . ' / ' . $rowIn['usunombre'] ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <?php
                                break;
                            case "inpDepartamento":

                                $consultaParametrosTipoCatalogo = "select  * from departamentos order by depnombre;";

                                $qryParametrosTipoCatalogo = $db->query($consultaParametrosTipoCatalogo);

                                ?>
                                <label for="<?php echo $row['pidnombreparametro'] ?>"><?php echo $row['piddescripcion'] ?></label>
                                <select id="<?php echo $row['pidnombreparametro'] ?>"
                                        name="<?php echo $row['pidnombreparametro'] ?>"
                                        class="<?php echo $row['pidclases'] ?> selectpicker"
                                        data-live-search="true">

                                    <?php
                                    while ($rowIn = $qryParametrosTipoCatalogo->fetch(PDO::FETCH_ASSOC)) {
                                        ?>
                                        <option value="<?php echo $rowIn['depid'] ?>"><?php echo $rowIn['depnombre'] ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <?php
                                break;
                            case "inpGrupo":

                                $consultaParametrosTipoCatalogo = "select  * from grupos;";

                                $qryParametrosTipoCatalogo = $db->query($consultaParametrosTipoCatalogo);

                                ?>
                                <label for="<?php echo $row['pidnombreparametro'] ?>"><?php echo $row['piddescripcion'] ?></label>
                                <select id="<?php echo $row['pidnombreparametro'] ?>"
                                        name="<?php echo $row['pidnombreparametro'] ?>"
                                        class="<?php echo $row['pidclases'] ?> selectpicker"
                                        data-live-search="true">

                                    <?php
                                    while ($rowIn = $qryParametrosTipoCatalogo->fetch(PDO::FETCH_ASSOC)) {
                                        ?>
                                        <option value="<?php echo $rowIn['gruid'] ?>"><?php echo $rowIn['grunombre'] ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <?php
                                break;
                            case "inpCiudad":

                                $consultaParametrosTipoCatalogo = "select  * from ciudades;";

                                $qryParametrosTipoCatalogo = $db->query($consultaParametrosTipoCatalogo);

                                ?>
                                <label for="<?php echo $row['pidnombreparametro'] ?>"><?php echo $row['piddescripcion'] ?></label>
                                <select id="<?php echo $row['pidnombreparametro'] ?>"
                                        name="<?php echo $row['pidnombreparametro'] ?>"
                                        class="<?php echo $row['pidclases'] ?> selectpicker"
                                        data-live-search="true">

                                    <?php
                                    while ($rowIn = $qryParametrosTipoCatalogo->fetch(PDO::FETCH_ASSOC)) {
                                        ?>
                                        <option value="<?php echo($rowIn['ciuid']) ?>"><?php echo $rowIn['ciunombre'] ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <?php
                                break;

                            default:
                                ?>
                                <p>
                                    <label for="<?php echo $row['pidnombreparametro'] ?>"><?php echo $row['piddescripcion'] ?></label>
                                    <input id="<?php echo $row['pidnombreparametro'] ?>"
                                           name="<?php echo $row['pidnombreparametro'] ?>"
                                           type="<?php echo $row['pidtipodeinput'] ?>"
                                           class="<?php echo $row['pidclases'] ?>"/>
                                </p>
                                <?php break;
                        }
                    } ?>
                    <p>
                    <div class="alert alert-dark mt-1" role="alert">
                        <label for="exportOption">Descargar informe como </label>
                        <select name="exportOption" id="exportOption" class="form-control">
                            <?php
                            $consultaExportesDados = "select  * from exportepermitidos;";

                            $consultaExportesDados = $db->query($consultaExportesDados);

                            while ($rowIn = $consultaExportesDados->fetch(PDO::FETCH_ASSOC)) {
                                ?>
                                <option value="<?php echo $rowIn["epextension"] ?>" <?php echo $rowIn["epextension"] == "pdf" ? "selected" : "" ?> ><?php echo $rowIn['expedescripcion'] ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    </p>
                    <button type="submit" class="btnconsulta btn btn-primary btn-block mt-2" name="consultar"
                            value="Buscar">consultar
                    </button>
                </fieldset>

            </form>
        </article>
    </article>
</section>
</body>
</html>