<?php

$r = '../../';

require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
require($r . 'incluir/phpjasper/src/PHPJasper.php');
require($r . 'incluir/funciones.php');

use PHPJasper\PHPJasper;

if (isset($_POST['idiid'])) {

    $idiId = $_POST['idiid'];

    $extension = $_POST['exportOption'];

    $rowInfo = $db->query("select * from informesdinamicos where hide is false  AND idiId = $idiId")->fetch(PDO::FETCH_ASSOC);

    $jasperFileName = $rowInfo['idinombrejasper'];
    $nombreInforme = $rowInfo['idinombre'];

    $outputName = nombreAlaeotorioDeArchivo(15, "", "");

    $input = __DIR__ . '/files/' . $jasperFileName;
    $output = __DIR__ . '/output/' . $nombreInforme . " - " . $outputName;
//    $fontDirectory = __DIR__ . '/Fuentes/';

    $options = [
        'format' => ["$extension"],
        'locale' => 'es_ES',
        'db_connection' => [
            'driver' => 'postgres',
            'username' => 'postgres',
            'password' => 'Kavv123xxx',
            'host' => 'libreria.c24uondhevic.us-east-2.rds.amazonaws.com',
            'database' => 'libreria',
            'port' => '5432'
        ]
    ];

    
/*     $options = [
        'format' => ["$extension"],
        'locale' => 'en',
        'db_connection' => [
            'driver' => 'postgres',
            'username' => 'postgres',
            'password' => 'Kavv',
            'host' => 'localhost',
            'database' => 'naciente',
            'port' => '5432'
        ]
    ]; */

    $buildingParams = [];

    foreach ($_POST as $key => $value) {
        if ($key != "idiid" && $key != "consultar" && $key != "exportOption") {
            $buildingParams += array("$key" => "$value");
        }
    }
    
    $options += array("params" => $buildingParams);

    $PHPJasper = new PHPJasper();
/* 

    echo $PHPJasper->process(
        $input,
        $output,
        $options
    )->output();
    exit(); */
    
    $PHPJasper->process(
        $input,
        $output,
        $options
    )->execute();

    $path = $output . '.' . $extension;

    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename="' . basename($path) . '"');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($path));
    flush(); // Flush system output buffer
    readfile($path);
    unlink($path);
    die();

}