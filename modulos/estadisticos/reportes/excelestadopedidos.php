<?php
$r = '../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
require($r.'incluir/phpexcel/Classes/PHPExcel.php');
require($r.'incluir/funciones.php');

$hoy = date('Y-m-d');

$fecha1 = $_GET['fecha1'];
$fecha2 = $_GET['fecha2'];
$consultar = $_GET['consultar'];


$sql = "SELECT * FROM capacitaciones INNER JOIN prospectos ON proid = capaprospectoid INNER JOIN usuarios ON usuid = capausuario LEFT JOIN pedsol ON pedsolcapacitacion = capaid  WHERE DATE(capafecha) BETWEEN '$fecha1' AND '$fecha2' ORDER BY capafecha desc";

$qry = $db->query($sql);


$titulo = "REPORTE ESTADO DE PEDIDOS $fecha1 - $fecha2 ";

$objPHPExcel = new PHPExcel();
$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN
		)
	)
);
$objPHPExcel->getActiveSheet()->getStyle('A1:AA2')->applyFromArray($styleArray);
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:AA1')
			->setCellValue('A1', $titulo);
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A2', 'CODIGO PROSPECTO')
			->setCellValue('B2', 'NOMBRE PROSPECTO')
			->setCellValue('C2', 'ID CAPACITACION')	
			->setCellValue('D2', 'FECHA DE INGRESO DE LA SOLICITUD')
			->setCellValue('E2', 'EMPRESA DE LA SOLICITUD')
			->setCellValue('F2', 'NUMERO DE LA SOLICITUD')
			->setCellValue('G2', 'FECHA DE SCREEN')
			->setCellValue('H2', 'ESTADO DE LA SOLICITUD')
			->setCellValue('I2', 'NOMBRE DE PRODUCTOS ADQUIRIDOS')
			->setCellValue('J2', 'VALOR TOTAL DE LA COMPRA')
			->setCellValue('K2', 'VALOR BASE DE LA COMPRA')
			->setCellValue('L2', 'NOMBRE CLIENTE')
			->setCellValue('M2', 'DOCUMENTO CLIENTE')
			->setCellValue('N2', 'CAPACITADOR')
			->setCellValue('O2', 'GERENTE CAPACITADOR')
			->setCellValue('P2', 'CAPACITACION')
			->setCellValue('Q2', 'FECHA CAPACITACION')
            ->setCellValue('R2', 'AÑO CAPACITACION')
			->setCellValue('S2', 'MES CAPACITACION')
			->setCellValue('T2', 'SEMANA CAPACITACION')
			->setCellValue('U2', 'DIA CAPACITACION')
			->setCellValue('V2', 'HORA CAPACITACION')	
			->setCellValue('W2', 'USUARIO')	
			->setCellValue('X2', 'GERENTE CONTAC CENTER')
			->setCellValue('Y2', 'DEPARTAMENTO')	
			->setCellValue('Z2', 'CIUDAD')
			->setCellValue('AA2', 'UNIDAD DE NEGOCIO');
			
$i = 3;

while($row = $qry->fetch(PDO::FETCH_ASSOC)){
	
	$sql2 = "SELECT * FROM solicitudes INNER JOIN clientes ON cliid = solcliente WHERE solid = '".$row['pedsolsolicitud']."' AND solempresa = '".$row['pedsolempresa']."';";

	$qry2 = $db->query($sql2);
	$row2 = $qry2->fetch(PDO::FETCH_ASSOC);
	
	$sql3 = "SELECT * FROM detsolicitudes INNER JOIN productos ON proid = detproducto WHERE detsolicitud = '".$row['pedsolsolicitud']."' AND detempresa = '".$row['pedsolempresa']."';";
	$qry3 = $db->query($sql3);
	
	$productos = "";
	while($row3 = $qry3->fetch(PDO::FETCH_ASSOC)){
		$productos .= $row3['detcantidad']."_".$row3['pronombre']." ";
	}
	
	if (!empty($row['capausuario'])){
		$qryrela = $db->query("SELECT * FROM usuarios WHERE usuid = '".$row['capausuario']."';");
		$rowrela = $qryrela->fetch(PDO::FETCH_ASSOC);
		$numrela = $qryrela->rowCount();
		if ($numrela == 1){
		$relacionista = $rowrela['usunombre'];
			$diasactivo = fechaDif($rowrela['usufechaingreso'],$hoy);
			if ($rowrela['usuperfil'] == 56){
				$estadorelacionista = "INACTIVO";
			} elseif ($diasactivo >= 60) {
				$estadorelacionista = "ANTIGUO";
			} elseif ($diasactivo < 60) {
				$estadorelacionista = "NUEVO";
			} elseif ($rowrela['usufechaingreso'] == '') {
				$estadorelacionista = "SIN FECHA DE INGRESO";
			} 
			if (!empty($rowrela['usujefecallcenter'])){
				$qryjeferela = $db->query("SELECT * FROM usuarios WHERE usuid = '".$rowrela['usujefecallcenter']."';");
				$rowjeferela = $qryjeferela->fetch(PDO::FETCH_ASSOC);
				$numjeferela = $qryjeferela->rowCount();
				if ($numjeferela == 1){
					$jeferelacionista = $rowjeferela['usunombre'];
				} else {
					$jeferelacionista = "NONE";	
				}
			} else {
			$jeferelacionista = "NONE";	
			}
		} else {
		$relacionista = "NONE";
		$estadorelacionista = "NONE";
		$jeferelacionista = "NONE";
		}
	} else {
		$relacionista = "NONE";
		$estadorelacionista = "NONE";
		$jeferelacionista = "NONE";
	}
	
	$qryca = $db->query("SELECT * FROM usuarios WHERE usuid = '".$row['capacapacitador']."';");
	$rowca = $qryca->fetch(PDO::FETCH_ASSOC);
	$numca = $qryca->rowCount();
	if ($numca > 0){
		$capacitador = $rowca['usunombre'];
		$qryjefeca = $db->query("SELECT * FROM usuarios WHERE usuid = '".$rowca['usudirjefe']."';");
		$rowjefeca = $qryjefeca->fetch(PDO::FETCH_ASSOC);
		$numjefeca = $qryjefeca->rowCount();
		if ($numjefeca > 0){
			$jefecapacitador = $rowjefeca['usunombre'];
		} else {
			$jefecapacitador = "";
		} 
	} elseif (empty($capacitador)) {
		$capacitador = "";
		$jefecapacitador = "";
	} else {
		$capacitador = "";
		$jefecapacitador = "";
	}
	
	if (!empty($row['prodepartamento'])){
	$qrydepar = $db->query("SELECT * FROM departamentos WHERE depid = '".$row['prodepartamento']."';");
	$rowdepar = $qrydepar->fetch(PDO::FETCH_ASSOC);
	$numdepar = $qrydepar->rowCount();
		if ($numdepar == 1){
		$departamento = $rowdepar['depnombre'];
		$qryciu = $db->query("SELECT * FROM ciudades WHERE ciudepto = '".$rowdepar['depid']."' AND ciuid = '".$row['prociudad']."' ;");
		$rowciu = $qryciu->fetch(PDO::FETCH_ASSOC);
		$numciu = $qryciu->rowCount();
			if ($numciu == 1){
			$ciudad = $rowciu['ciunombre'];
			} else {
			$ciudad = "NONE";
			} 
		} else {
		$departamento = "NONE";
		$ciudad = "NONE";
		}
	} else {
	$departamento = "NONE";
	$ciudad = "NONE";
	}
	
	
	$fechacapa = substr($row['capafecha'], 0, 10);
	$horacapa = substr($row['capafecha'], 10, 20);
	
	
	if (!empty($row['capauninegocio'])){
	$qryuni = $db->query("SELECT * FROM uninegocio WHERE uniid = '".$row['capauninegocio']."';");
	$rowuni = $qryuni->fetch(PDO::FETCH_ASSOC);
	$numuni = $qryuni->rowCount();
		if ($numuni == 1){
			$uninegocio = $rowuni['unidescripcion'];
		} else {
			$uninegocio = "NONE";
		}
	} else {
		$uninegocio = "NONE";
	}

	$dateone = date_create($row['capafecha']);
	$semanaCapacitacion = date_format($dateone, 'W');
	
	$datetwo = date_create($row['capafechausuario']);
	$semanaGestion = date_format($datetwo, 'W');

	$clientename = $row2['clinombre'].' '.$row2['clinom2'].' '.$row2['cliape1'].' '.$row2['cliape2'];
	
	$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':AA'.$i)->applyFromArray($styleArray);
	$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('A'.$i, $row['proid'])
    	->setCellValue('B'.$i, $row['pronombre'])
		->setCellValue('C'.$i, $row['capaid'])
		->setCellValue('D'.$i, $row2['solfecha'])
		->setCellValue('E'.$i, $row2['solempresa'])
		->setCellValue('F'.$i, $row2['solid'])
		->setCellValue('G'.$i, $row2['solfechscreen'])
		->setCellValue('H'.$i, $row2['solestado'])
		->setCellValue('I'.$i, $productos)
		->setCellValue('J'.$i, $row2['soltotal'])
		->setCellValue('K'.$i, $row2['solbase'])
		->setCellValue('L'.$i, $clientename)
		->setCellValue('M'.$i, $row2['cliid'])
		->setCellValue('N'.$i, $capacitador)
		->setCellValue('O'.$i, $jefecapacitador)
    	->setCellValue('P'.$i, $row['capanombre'])
		->setCellValue('Q'.$i, $fechacapa)
    	->setCellValue('R'.$i, substr($row['capafecha'], 0, 4))
		->setCellValue('S'.$i, substr($row['capafecha'], 5, 2))
		->setCellValue('T'.$i, $semanaCapacitacion)
		->setCellValue('U'.$i, substr($row['capafecha'], 8, 3))
		->setCellValue('V'.$i, $horacapa)
		->setCellValue('W'.$i, $relacionista)
		->setCellValue('X'.$i, $jeferelacionista)
		->setCellValue('Y'.$i, $departamento)
    	->setCellValue('Z'.$i, $ciudad)
		->setCellValue('AA'.$i, $uninegocio);
	$i++;

}
// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Estado de pedidos.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
?>