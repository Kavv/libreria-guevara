<?php
$r = '../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
$fecha1 = $_POST['fecha1'];
$fecha2 = $_POST['fecha2'];
$filtro = 'fecha1='.$fecha1.'fecha2='.$fecha2;
?>
<!doctype html>
<html>
<head>
	<?php 
		require($r . 'incluir/src/head.php');
		require($r . 'incluir/src/head-form.php');
	?>	

<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#tabla').dataTable({
		'bJQueryUI': true,
		'bStateSave': true,
		'bSort': false,
		'sPaginationType': 'full_numbers',
		'oLanguage': {
			'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
		},
	});
	$('.btnatras').button({ icons: { primary: 'ui-icon ui-icon-arrowthick-1-w' }});
});
</script>
</head>
<body>
<?php require($r.'incluir/src/login.php') ?>
<section id="principal">
<?php require($r.'incluir/src/cabeza.php') ?>
<?php require($r.'incluir/src/menu.php') ?>
<article id="cuerpo">
<article class="mapa">
<a href="#">Principal</a><div class="mapa_div"></div><a class="current">Estadisticos</a>
</article>
<article id="contenido">
<h2>Estadistico - Detalle screen de asesores del intervalo <?php echo $fecha1.' al '.$fecha2  ?></h2>
<table id="tabla">
<thead>
<tr>
<th>Asesor</th>
<th>Estado del Asesor</th>
<th title="Rechazado Screen" >R.S</th>
<th>Proceso</th>
<th>Facturado</th>
<th>Enviado</th>
<th>Cancelado</th>
<th>Entregado</th>
<th>Anulada</th>
<th>Borrador</th>
<th title="Rechazado Entrega" >R.E</th>
<th title="Aprobado Screen" >A.S</th>
<th>Bandeja</th>
</tr>
</thead>
<tbody> 
<?php
$qry = $db->query("SELECT * FROM usuarios WHERE usuasesor = '1' ORDER BY usunombre");
while($row = $qry->fetch(PDO::FETCH_ASSOC)){
	$qry2 = $db->query("SELECT * FROM solicitudes WHERE solasesor = '".$row['usuid']."' AND solestado = 'RECHAZADO SCREEN' AND solfecha BETWEEN '$fecha1' AND '$fecha2'");
	$num_rechazadas = $qry2->rowCount();
	$qry3 = $db->query("SELECT * FROM solicitudes WHERE solasesor = '".$row['usuid']."' AND solestado = 'PROCESO' AND solfecha BETWEEN '$fecha1' AND '$fecha2'");
	$num_proceso = $qry3->rowCount();
	$qry4 = $db->query("SELECT * FROM solicitudes WHERE solasesor = '".$row['usuid']."' AND solestado = 'ENVIADO' AND solfecha BETWEEN '$fecha1' AND '$fecha2'");
	$num_enviado = $qry4->rowCount();
	$qry5 = $db->query("SELECT * FROM solicitudes WHERE solasesor = '".$row['usuid']."' AND solestado = 'CANCELADO' AND solfecha BETWEEN '$fecha1' AND '$fecha2'");
	$num_cancelado = $qry5->rowCount();
	$qry6 = $db->query("SELECT * FROM solicitudes WHERE solasesor = '".$row['usuid']."' AND solestado = 'ENTREGADO' AND solfecha BETWEEN '$fecha1' AND '$fecha2'");
	$num_entregado = $qry6->rowCount();
	$qry7 = $db->query("SELECT * FROM solicitudes WHERE solasesor = '".$row['usuid']."' AND solestado = 'ANULADA' AND solfecha BETWEEN '$fecha1' AND '$fecha2'");
	$num_anulada = $qry7->rowCount();
	$qry8 = $db->query("SELECT * FROM solicitudes WHERE solasesor = '".$row['usuid']."' AND solestado = 'BORRADOR' AND solfecha BETWEEN '$fecha1' AND '$fecha2'");
	$num_borrador = $qry8->rowCount();
	$qry9 = $db->query("SELECT * FROM solicitudes WHERE solasesor = '".$row['usuid']."' AND solestado = 'RECHAZADO ENTREGA' AND solfecha BETWEEN '$fecha1' AND '$fecha2'");
	$num_rechazadoentrega = $qry9->rowCount();
	$qry10 = $db->query("SELECT * FROM solicitudes WHERE solasesor = '".$row['usuid']."' AND solestado = 'APROBADO SCREEN' AND solfecha BETWEEN '$fecha1' AND '$fecha2'");
	$num_aprobadoscreen = $qry10->rowCount();
	$qry11 = $db->query("SELECT * FROM solicitudes WHERE solasesor = '".$row['usuid']."' AND solestado = 'BANDEJA' AND solfecha BETWEEN '$fecha1' AND '$fecha2'");
	$num_bandeja = $qry11->rowCount();
	$qry12 = $db->query("SELECT * FROM solicitudes WHERE solasesor = '".$row['usuid']."' AND solestado = 'FACTURADO' AND solfecha BETWEEN '$fecha1' AND '$fecha2'");
	$num_facturado = $qry12->rowCount();

?>
<tr>
<td title="<?php echo $row['usuid'] ?>"><?php echo $row['usunombre'] ?></td>
<td align="center"><?php if($row['usuperfil'] == 56){ echo "INACTIVO";}else{ echo "ACTIVO";} ?></td>
<td align="center"><?php echo $num_rechazadas; ?></td>
<td align="center"><?php echo $num_proceso; ?></td>
<td align="center"><?php echo $num_facturado; ?></td>
<td align="center"><?php echo $num_enviado; ?></td>
<td align="center"><?php echo $num_cancelado; ?></td>
<td align="center"><?php echo $num_entregado; ?></td>
<td align="center"><?php echo $num_anulada; ?></td>
<td align="center"><?php echo $num_borrador; ?></td>
<td align="center"><?php echo $num_rechazadoentrega; ?></td>
<td align="center"><?php echo $num_aprobadoscreen; ?></td>
<td align="center"><?php echo $num_bandeja; ?></td>
</tr>
<?php
}
?>

</tbody>
</table>
<p class="boton">
<button type="button" class="btnatras" onClick="carga(); location.href='conscreen.php'">atras</button>
</p>
</article>
</article>
<?php require($r.'incluir/src/pie.php') ?>
</section>
</body>
</html>