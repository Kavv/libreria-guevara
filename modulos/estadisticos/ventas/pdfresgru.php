<?php
$r = "../../";
include($r."include/session.class.php");
include($r."include/mysql.class.php");
include($r."fpdf/fpdf.php");
$session = new Session;
$mysql = new MySql;
$session->iniciar($r);
$mysql->conectar();
$mysql->base();
$fecha1 = $_GET["fecha1"];
$fecha2 = $_GET["fecha2"];
class PDF extends FPDF{
	function Header(){
		global $fecha1;
		global $fecha2;
    	$this->SetFont('LucidaConsole','',8);
		$this->Cell(0,5,date('Y/m/d H:i:s'),0,1);
		$this->SetFont('LucidaConsole','',10);
		$this->Cell(0,5,'------------------------------------------------------------------------------------------',0,1,'C');
		$this->Cell(0,5,'ESTADISTICA DE VENTA RESUMEN DE GRUPOS DEL '.$fecha1.' AL '.$fecha2,0,1,'C');
		$this->Cell(0,5,'------------------------------------------------------------------------------------------',0,1,'C');
    	$this->Ln(1);
	}
	function Footer(){
    	$this->SetY(-15);
    	$this->SetFont('LucidaConsole','',10);
		$this->Cell(0,5,'------------------------------------------------------------------------------------------',0,1,'C');
		$this->SetFont('LucidaConsole','',8);
    	$this->Cell(0,5,'Pagina '.$this->PageNo().'/{nb}',0,0,'R');
	}
}

$pdf = new PDF();
$pdf->AddFont('LucidaConsole','','lucon.php');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('LucidaConsole','',9);
$pdf->SetFillColor(230,230,230);
$pdf->SetX(35);
$pdf->Cell(70,5,'GRUPO',0,0,'C',true);
$pdf->Cell(25,5,'SOLICITUD',0,0,'C',true);
$pdf->Cell(35,5,'BASE ESTADISTICOS',0,1,'C',true);
$qrygru = mysql_query("SELECT DISTINCT(usuover1) FROM usuarios WHERE usuover1 <> ''");
$i = 1;
while($rowgru = mysql_fetch_assoc($qrygru)){
	$rownom = mysql_fetch_assoc(mysql_query("SELECT usunombre FROM usuarios WHERE usuid = '".$rowgru["usuover1"]."'"));
	$qryven = mysql_query("SELECT * FROM usuarios WHERE usuover1 = '".$rowgru["usuover1"]."' OR usuid = '".$rowgru["usuover1"]."'");
	$fnlbase = 0;
	$fnltotal = 0;
	$fnlsol = 0;
	while($rowven = mysql_fetch_assoc($qryven)){
		$sqlsol = "SELECT * FROM solicitudes INNER JOIN clientes ON solcliente = cliid WHERE solfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND solasesor = '".$rowven["usuid"]."' AND solfactura <> '' ORDER BY solfecha";
		$qrysol = mysql_query($sqlsol);
		$numsol = mysql_num_rows($qrysol);
		if($numsol > 0){
			$ttlbase = 0;
			while($rowsol = mysql_fetch_assoc($qrysol)){
				$ttlbase = $ttlbase + $rowsol["solbase"];
			}
			$fnlbase = $fnlbase + $ttlbase;
			$fnlsol = $fnlsol + $numsol;
			$ultbase = $ultbase + $ttlbase;
			$ultsol = $ultsol + $numsol;
		}
	}
	$pdf->SetFont('LucidaConsole','',8);
	if($i%2 == 0) $x = 240;
	else $x = 255;
	$pdf->SetFillColor($x,$x,$x);
	$pdf->SetX(35);
	$pdf->Cell(70,5,'GRUPO DE '.$rownom["usunombre"],0,0,'',true);
	$pdf->Cell(25,5,number_format($fnlsol,0,',','.'),0,0,'C',true);
	$pdf->Cell(35,5,'$ '.number_format($fnlbase,0,',','.'),0,1,'R',true);
	$i++;
}
$pdf->SetFillColor(220,220,220);
$pdf->SetX(35);
$pdf->Cell(70,5,'TOTAL',0,0,'C',true);
$pdf->Cell(25,5,number_format($ultsol,0,',','.'),0,0,'C',true);
$pdf->Cell(35,5,'$ '.number_format($ultbase,0,',','.'),0,1,'R',true);
$pdf->Output("pdfresgru.pdf","d");
?>