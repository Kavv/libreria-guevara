<?php
$r = '../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
$asesor = $_POST['asesor'];
$fecha1 = $_POST['fecha1'];
$fecha2 = $_POST['fecha2'];
$filtro = 'asesor='.$asesor.'&fecha1='.$fecha1.'&fecha2='.$fecha2;
if($asesor == '') $qry = $db->query("SELECT * FROM usuarios WHERE usuasesor = '1'");
else $qry = $db->query("SELECT * FROM usuarios WHERE usuid = '$asesor'");
?>
<!doctype html>
<html>
<head>
	<?php 
		require($r . 'incluir/src/head.php');
		require($r . 'incluir/src/head-form.php');
	?>	

<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('.tabla').dataTable({
		'bJQueryUI': true,
		'bStateSave': true,
		'bSort': false,
		'sPaginationType': 'full_numbers',
		'oLanguage': {
			'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
		},
	});
	$('.btnatras').button({ icons: { primary: 'ui-icon ui-icon-arrowthick-1-w' }});
});
</script>
</head>
<body>
<?php require($r.'incluir/src/login.php') ?>
<section id="principal">
<?php require($r.'incluir/src/cabeza.php') ?>
<?php require($r.'incluir/src/menu.php') ?>
<article id="cuerpo">
<article class="mapa">
<a href="#">Principal</a><div class="mapa_div"></div><a class="current">Estadisticos</a>
</article>
<article id="contenido">
<h2>Estadistico venta detallada por asesor del <?php echo $fecha1.' al '.$fecha2 ?></h2>
<div class="reporte">
<a href="exceldetven.php?<?php echo $filtro ?>"><img src="<?php echo $r ?>imagenes/iconos/excel.png" title="Excel" /></a> <a href="#"><img src="<?php echo $r ?>imagenes/iconos/pdf.png" title="PDF" /></a>
</div>
<?php
while($row = $qry->fetch(PDO::FETCH_ASSOC)){
	$qry2 = $db->query("SELECT * FROM solicitudes INNER JOIN clientes ON solcliente = cliid WHERE solfecha BETWEEN '$fecha1' AND '$fecha2' AND solasesor = '".$row['usuid']."' AND solfactura <> ''");
	$num2 = $qry2->rowCount();
?>
<br /><br />
<h2><?php echo $row['usunombre'] ?></h2>
<table class="tabla">
<thead>
<tr>
<th>Solicitud</th>
<th>Factura</th>
<th>Cliente</th>
<th>Fecha</th>
<th>Base</th>
</tr>
</thead>
<tbody>
<?php
	$ttlbase = 0;
	while($row2 = $qry2->fetch(PDO::FETCH_ASSOC)){
?>
<tr>
<td align="center"><?php echo $row2['solid'] ?></td>
<td align="center"><?php echo 'FV-'.$row2['solfactura'] ?></td>
<td title="<?php echo $row2['solcliente'] ?>"><?php echo $row2['clinombre'].' '.$row2['clinom2'].' '.$row2['cliape1'].' '.$row2['cliape2'] ?></td>
<td align="center"><?php echo $row2['solfecha'] ?></td>
<td align="right"><?php echo number_format($row2['solbase'],0,',','.') ?></td>
</tr>
<?php
		$ttlbase = $ttlbase + $row2['solbase'];
	}
	$fnlbase = $fnlbase + $ttlbase;
	$fnlsol = $fnlsol + $num2;
?>
</tbody>
<tfoot>
<tr>
<td align="center"></td>
<td align="center" bgcolor="#D1CFCF"><?php echo $num2 ?></td>
<td align="center" colspan="2"></td>
<td align="right" bgcolor="#D1CFCF"><?php echo number_format($ttlbase,0,',','.') ?></td>
</tr>
</tfoot>
</table>
<?php
}
?>
<p class="boton">
<button type="button" class="btnatras" onClick="carga(); location.href='condetven.php'">atras</button>
</p>
</article>
</article>
<?php require($r.'incluir/src/pie.php') ?>
</section>
</body>
</html>