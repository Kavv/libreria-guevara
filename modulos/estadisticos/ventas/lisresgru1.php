<?php
$r = "../../";
include($r."include/session.class.php");
include($r."include/mysql.class.php");
$session = new Session;
$mysql = new MySql;
$session->iniciar($r);
$mysql->conectar();
$mysql->base();
$fecha1 = $_POST["fecha1"];
$fecha2 = $_POST["fecha2"];
$filtro = "fecha1=$fecha1&fecha2=$fecha2";
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>.:: IdenCorp ::.</title>
<link type="text/css" rel="stylesheet" href="<?php echo $r ?>estilo/estilo.css" />
<link type="text/css" rel="stylesheet" href="<?php echo $r ?>jquery/css/base/jquery-ui-1.10.2.custom.css">
<script type="text/javascript" src="<?php echo $r ?>jquery/js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="<?php echo $r ?>jquery/js/jquery-ui-1.10.2.custom.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$(".grilla:odd").css("background-color","#EAF2D3");
	$(".btnatras").button({ icons: { primary: "ui-icon ui-icon-arrowthick-1-w" }});
});
</script>
</head>
<body>
<?php include($r.'include/encabezado.php') ?>
<div class="contenedor">
<?php include($r.'include/menu.php') ?>
<div class="mapa">
<a href="<?php echo $r ?>index.php">Principal</a><div class="mapa_div"></div><a href="#">Estadisticas</a><div class="mapa_div"></div><a href="condetgru.php">Venta resumen de grupos</a><div class="mapa_div"></div><a class="current">Listado</a>
</div>
<div class="cuerpo centrado">
<h1 style="font-weight:bold; font-size:14px; background-color:#999; padding:5px; width:590px">ESTADISTICAS DE VENTA DETALLADA DE GRUPOS <?php echo "DEL ".$fecha1." AL ".$fecha2 ?><span style="float:right"><a href="pdfresgru.php?<?php echo $filtro ?>"><img src="<?php echo $r ?>imagenes/pdf.png" title="pdf" /></a> <a href="#"><img src="<?php echo $r ?>imagenes/excel.png" title="excel" /></a></span></h1><br />
<table width="600px">
<thead>
<tr>
<th width="60%">GRUPO</th>
<th width="15%">SOLICITUD</th>
<th width="25%">BASE ESTADISTICOS</th>
</tr>
</thead>
<tbody>
<?php
$qrygru = mysql_query("SELECT DISTINCT(usuover1) FROM usuarios WHERE usuover1 <> ''");
while($rowgru = mysql_fetch_assoc($qrygru)){
	$rownom = mysql_fetch_assoc(mysql_query("SELECT usunombre FROM usuarios WHERE usuid = '".$rowgru["usuover1"]."'"));
	$qryven = mysql_query("SELECT * FROM usuarios WHERE usuover1 = '".$rowgru["usuover1"]."' OR usuid = '".$rowgru["usuover1"]."'");
	$fnlbase = 0;
	$fnlsol = 0;
	while($rowven = mysql_fetch_assoc($qryven)){
		$sqlsol = "SELECT * FROM solicitudes INNER JOIN clientes ON solcliente = cliid WHERE solfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND solasesor = '".$rowven["usuid"]."' AND solfactura <> '' ORDER BY solfecha";
		$qrysol = mysql_query($sqlsol);
		$numsol = mysql_num_rows($qrysol);
		if($numsol > 0){
			$ttlbase = 0;
			while($rowsol = mysql_fetch_assoc($qrysol)){
				$ttlbase = $ttlbase + $rowsol["solbase"];
			}
			$fnlbase = $fnlbase + $ttlbase;
			$fnlsol = $fnlsol + $numsol;
			$ultbase = $ultbase + $ttlbase;
			$ultsol = $ultsol + $numsol;
		}
	}
?>
<tr class="grilla">
<td title="<?php echo $rowgru["usuid"] ?>">GRUPO DE <?php echo $rownom["usunombre"] ?></td>
<td align="center"><?php echo number_format($fnlsol,0,',','.') ?></td>
<td align="right">$ <?php echo number_format($fnlbase,0,',','.') ?></td>
</tr>
<?php
}
?>
</tbody>
<tfoot>
<tr class="total">
<td align="center">TOTAL</td>
<td align="center"><?php echo number_format($ultsol,0,',','.') ?></td>
<td align="right">$ <?php echo number_format($ultbase,0,',','.') ?></td>
</tr>
</tfoot>
</table>
<button type="button" class="btnatras" onClick="location.href = 'conresgru.php'" style="margin-top:5px">atras</button>
</div>
</div>
</body>
</html>