<?php
$r = "../../";
include($r."include/session.class.php");
include($r."include/mysql.class.php");
include($r."fpdf/fpdf.php");
$session = new Session;
$mysql = new MySql;
$session->iniciar($r);
$mysql->conectar();
$mysql->base();
$fecha1 = $_GET["fecha1"];
$fecha2 = $_GET["fecha2"];
class PDF extends FPDF{
	function Header(){
		global $fecha1;
		global $fecha2;
    	$this->SetFont('LucidaConsole','',8);
		$this->Cell(0,5,date('Y/m/d H:i:s'),0,1);
		$this->SetFont('LucidaConsole','',10);
		$this->Cell(0,5,'------------------------------------------------------------------------------------------',0,1,'C');
		$this->Cell(0,5,'ESTADISTICA DE VENTA RESUMEN DE VENDEDORES DEL '.$fecha1.' AL '.$fecha2,0,1,'C');
		$this->Cell(0,5,'------------------------------------------------------------------------------------------',0,1,'C');
    	$this->Ln(1);
	}
	function Footer(){
    	$this->SetY(-15);
    	$this->SetFont('LucidaConsole','',10);
		$this->Cell(0,5,'------------------------------------------------------------------------------------------',0,1,'C');
		$this->SetFont('LucidaConsole','',8);
    	$this->Cell(0,5,'Pagina '.$this->PageNo().'/{nb}',0,0,'R');
	}
}

$pdf = new PDF();
$pdf->AddFont('LucidaConsole','','lucon.php');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('LucidaConsole','',9);
$pdf->SetFillColor(230,230,230);
$pdf->SetX(45);
$pdf->Cell(65,5,'VENDEDOR',0,0,'C',true);
$pdf->Cell(20,5,'SOLICITUD',0,0,'C',true);
$pdf->Cell(35,5,'BASE ESTADISTICO',0,1,'C',true);
$qryven = mysql_query("SELECT * FROM usuarios WHERE usuperfil = '3' OR usuperfil = '48' OR usuperfil = '60' ORDER BY usunombre");
$i = 1;
while($rowven = mysql_fetch_assoc($qryven)){
	$sqlsol = "SELECT * FROM solicitudes INNER JOIN movimientos ON (solempresa = movempresa  AND solfactura = movnumero) WHERE solfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND solasesor = '".$rowven["usuid"]."' AND movprefijo = 'FV' AND movestado = 'FACTURADO'";
	$qrysol = mysql_query($sqlsol);
	$numsol = mysql_num_rows($qrysol);
	if($numsol > 0){
		$ttlbase = 0;
		$ttltotal = 0;
		while($rowsol = mysql_fetch_assoc($qrysol)){
			$ttlbase = $ttlbase + $rowsol["solbase"];
		}
		$pdf->SetFont('LucidaConsole','',8);
		if($i%2 == 0) $x = 240;
		else $x = 255;
		$pdf->SetFillColor($x,$x,$x);
		$pdf->SetX(45);
		$pdf->Cell(65,5,$rowven["usunombre"],0,0,'',true);
		$pdf->Cell(20,5,number_format($numsol,0,',','.'),0,0,'C',true);
		$pdf->Cell(35,5,'$ '.number_format($ttlbase,0,',','.'),0,1,'R',true);
		$fnlbase = $fnlbase + $ttlbase;
		$fnlsol = $fnlsol + $numsol;
		$i++;
	}
}
$pdf->SetFillColor(210,210,210);
$pdf->SetX(45);
$pdf->Cell(65,5,'TOTAL VENDEDORES',0,0,'C',true);
$pdf->Cell(20,5,number_format($fnlsol,0,',','.'),0,0,'C',true);
$pdf->Cell(35,5,'$ '.number_format($fnlbase,0,',','.'),0,1,'R',true);
$pdf->Output("pdfresven.pdf","d");
?>