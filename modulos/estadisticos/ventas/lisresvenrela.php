<?php
$r = '../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
$fecha1 = $_POST['fecha1'];
$fecha2 = $_POST['fecha2'];

$ide = $_SESSION['id'];
$rowusuario = $db->query("SELECT * FROM usuarios INNER JOIN perfiles ON usuperfil = perid WHERE usuid = ".$ide.";")->fetch(PDO::FETCH_ASSOC);
$perfil = $rowusuario['usuperfil'];

$filtro = 'fecha1='.$fecha1.'&fecha2='.$fecha2;
?>
<!doctype html>
<html>
<head>
	<?php 
		require($r . 'incluir/src/head.php');
		require($r . 'incluir/src/head-form.php');
	?>	

<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#tabla').dataTable({
		'bJQueryUI': true,
		'bStateSave': true,
		'bSort': false,
		'sPaginationType': 'full_numbers',
		'oLanguage': {
			'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
		},
	});
	$('.btnatras').button({ icons: { primary: 'ui-icon ui-icon-arrowthick-1-w' }});
});
</script>
</head>
<body>
<?php require($r.'incluir/src/login.php') ?>
<section id="principal">
<?php require($r.'incluir/src/cabeza.php') ?>
<?php require($r.'incluir/src/menu.php') ?>
<article id="cuerpo">
<article class="mapa">
<a href="#">Principal</a><div class="mapa_div"></div><a class="current">Estadisticos</a>
</article>
<article id="contenido">
<h2>Estadistico - resumen de venta por relacionistas del <?php echo $fecha1.' al '.$fecha2 ?></h2>
<div class="reporte">
<a href="excelvenrela.php?<?php echo $filtro ?>"><img src="<?php echo $r ?>imagenes/iconos/excel.png" title="Excel" /></a> <a href="#"><img src="<?php echo $r ?>imagenes/iconos/pdf.png" title="PDF" /></a>
</div>
<table id="tabla">
<thead>
<tr>
<th>Relacionista</th>
<th>Estado</th>
<th>Solicitud</th>
<th>Base</th>
</tr>
</thead>
<tbody>
<?php
/*if ($perfil == '98'){
$qry = $db->query("SELECT * FROM usuarios WHERE usucallcenter = '1' AND usuid = '".$ide."' ORDER BY usunombre");	
} else {*/
$qry = $db->query("SELECT * FROM usuarios WHERE usucallcenter = '1' ORDER BY usunombre");
/*}*/
while($row = $qry->fetch(PDO::FETCH_ASSOC)){
	$qry2 = $db->query("SELECT * FROM solicitudes INNER JOIN movimientos ON (solempresa = movempresa  AND solfactura = movnumero) WHERE solfecha BETWEEN '$fecha1' AND '$fecha2' AND solrelacionista = '".$row['usuid']."' AND movprefijo = 'FV' AND movestado = 'FACTURADO'");
	$ttlbase = 0;
	if($qry2){
	$num2 = $qry2->rowCount();
	while($row2 = $qry2->fetch(PDO::FETCH_ASSOC)){
		$ttlbase = $ttlbase + $row2['solbase'];
	}
if ($num2 > 0 or $ttlbase > 0){
?>
<tr>
<td title="<?php echo $row['usuid'] ?>"><?php echo $row['usunombre'] ?></td>
<td align="center"><?php if($row['usuperfil'] == 56){ echo "DESACTIVADO";}else{ echo "ACTIVO";} ?></td>
<td align="center"><?php echo number_format($num2,0,',','.') ?></td>
<td align="right"><?php echo number_format($ttlbase,0,',','.') ?></td>
</tr>
<?php

	$fnlbase = $fnlbase + $ttlbase;
	$fnlsol = $fnlsol + $num2;
}
}
}
?>

</tbody>
<tfoot>
<tr>
<td align="center"></td>
<td align="center"></td>
<td align="center" bgcolor="#D1CFCF"><?php echo $fnlsol ?></td>
<td align="right" bgcolor="#D1CFCF"><?php echo number_format($fnlbase,0,',','.') ?></td>
</tr>
</tfoot>
</table>
<p class="boton">
<button type="button" class="btnatras" onClick="carga(); location.href='conresvenrela.php'">Atras</button>
</p>
</article>
</article>
<?php require($r.'incluir/src/pie.php') ?>
</section>
</body>
</html>