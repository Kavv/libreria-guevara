<?php
$r = '../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
require($r.'incluir/phpexcel/Classes/PHPExcel.php');

$fecha1 = $_GET['fecha1'];
$fecha2 = $_GET['fecha2'];

$ide = $_SESSION['id'];
$rowusuario = $db->query("SELECT * FROM usuarios INNER JOIN perfiles ON usuperfil = perid WHERE usuid = ".$ide.";")->fetch(PDO::FETCH_ASSOC);
$perfil = $rowusuario['usuperfil'];

$filtro = 'fecha1='.$fecha1.'&fecha2='.$fecha2;


$titulo = "RESUMEN VENTA POR ASESORES DEL $fecha1 - $fecha2 ";



$objPHPExcel = new PHPExcel();
$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN
		)
	)
);
$objPHPExcel->getActiveSheet()->getStyle('A1:P2')->applyFromArray($styleArray);
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:P1')
			->setCellValue('A1', $titulo);
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A2', 'ASESOR')
			->setCellValue('B2', 'ESTADO ASESOR')	
            ->setCellValue('C2', 'SOLICITUDES ')
			->setCellValue('D2', 'BASE')
			->setCellValue('E2', 'DEPARTAMENTO')
			->setCellValue('F2', 'CIUDAD')
            ->setCellValue('G2', 'ID SOLICITUD')
			->setCellValue('H2', 'NUMERO FACTURA')
			->setCellValue('I2', 'FECHA SOLICITUD')
			->setCellValue('J2', 'JEFE ASESOR')
			->setCellValue('K2', 'NOMBRE CLIENTE')
			->setCellValue('L2', 'MES  SOLICITUD')
			->setCellValue('M2', 'RELACIONISTA')
			->setCellValue('N2', 'GERENTE RELACIONISTA')
			->setCellValue('O2', 'VALOR MENSUAL')
			->setCellValue('P2', 'PRODUCTOS ADQUIRIDOS')
			;
			
$i = 3;

$qry = $db->query("SELECT * FROM usuarios WHERE usuasesor = '1' ORDER BY usunombre");

while($row = $qry->fetch(PDO::FETCH_ASSOC)){
	
	
	
	$qry2 = $db->query("SELECT * FROM solicitudes INNER JOIN movimientos ON (solempresa = movempresa  AND solfactura = movnumero) INNER JOIN clientes ON solcliente = cliid WHERE solfecha BETWEEN '$fecha1' AND '$fecha2' AND solasesor = '".$row['usuid']."' AND movprefijo = 'FV' AND movestado = 'FACTURADO'");
	$ttlbase = 0;

	
	if($qry2){
	$num2 = $qry2->rowCount();
	while($row2 = $qry2->fetch(PDO::FETCH_ASSOC)){
		$nomcliente = $row2['clinombre'].' '.$row2['clinom2'].' '.$row2['cliape1'].' '.$row2['cliape2'];	
		$ttlbase = $row2['solbase'];
		$num3 = 1;
	
	if ($num2 > 0 or $ttlbase > 0){	
	
		if($row['usuperfil'] == 56){ $estado = "DESACTIVADO";}else{ $estado = "ACTIVO";}

		if (!empty($row2['clidepresidencia'])){
		$qrydepar = $db->query("SELECT * FROM departamentos WHERE depid = '".$row2['clidepresidencia']."';");
		$row2depar = $qrydepar->fetch(PDO::FETCH_ASSOC);
		$numdepar = $qrydepar->rowCount();
			if ($numdepar == 1){
			$departamento = $row2depar['depnombre'];
			$qryciu = $db->query("SELECT * FROM ciudades WHERE ciudepto = '".$row2depar['depid']."' AND ciuid = '".$row2['cliciuresidencia']."' ;");
			$row2ciu = $qryciu->fetch(PDO::FETCH_ASSOC);
			$numciu = $qryciu->rowCount();
				if ($numciu == 1){
				$ciudad = $row2ciu['ciunombre'];
				} else {
				$ciudad = "NONE";
				} 
			} else {
			$departamento = "NONE";
			$ciudad = "NONE";
			}
		} else {
		$departamento = "NONE";
		$ciudad = "NONE";
		}

		if (!empty($row['usujefeasesor'])){
			$qryjefease = $db->query("SELECT * FROM usuarios WHERE usuid = '".$row['usujefeasesor']."';");
			$rowjefease = $qryjefease->fetch(PDO::FETCH_ASSOC);
			$numjefease = $qryjefease->rowCount();
			if ($numjefease == 1){
				$jefeasesor = $rowjefease['usunombre'];
			} else {
				$jefeasesor = "NONE";	
			}
		} else {
		$jefeasesor = "NONE";	
		}
		
	if (!empty($row2['solrelacionista'])){
		$qryrela = $db->query("SELECT * FROM usuarios WHERE usuid = '".$row2['solrelacionista']."';");
		$rowrela = $qryrela->fetch(PDO::FETCH_ASSOC);
		$numrela = $qryrela->rowCount();
		if ($numrela == 1){
		$relacionista = $rowrela['usunombre'];
			if (!empty($rowrela['usujefecallcenter'])){
				$qryjeferela = $db->query("SELECT * FROM usuarios WHERE usuid = '".$rowrela['usujefecallcenter']."';");
				$rowjeferela = $qryjeferela->fetch(PDO::FETCH_ASSOC);
				$numjeferela = $qryjeferela->rowCount();
				if ($numjeferela == 1){
					$jeferelacionista = $rowjeferela['usunombre'];
				} else {
					$jeferelacionista = "NONE";	
				}
			} else {
			$jeferelacionista = "NONE";	
			}
		} else {
		$relacionista = "NONE";
		$jeferelacionista = "NONE";
		}
	} else {
		$relacionista = "NONE";
		$jeferelacionista = "NONE";
	}

	$qryproductos = $db->query("SELECT * FROM detsolicitudes INNER JOIN productos on detproducto = proid WHERE detempresa = '".$row2['solempresa']."' AND detsolicitud =  '".$row2['solid']."';");
	$productos = '';
	while($rowproductos = $qryproductos->fetch(PDO::FETCH_ASSOC)){
		$productos  .= "  ".$rowproductos['pronombre']." ; ";
		//break();
	}	
	
	$qrycuota = $db->query(" SELECT * FROM carteras WHERE carempresa = '".$row2['solempresa']."' AND carfactura =  '".$row2['solfactura']."';;");
	$rowcuota = $qrycuota->fetch(PDO::FETCH_ASSOC);
	$cuota = $rowcuota['carcuota'];
	
	$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':P'.$i)->applyFromArray($styleArray);
	$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('A'.$i, $row['usunombre'])
    	->setCellValue('B'.$i, $estado)
		->setCellValue('C'.$i, $num3)
		->setCellValue('D'.$i, $ttlbase)
		->setCellValue('E'.$i, $departamento)
		->setCellValue('F'.$i, $ciudad)
    	->setCellValue('G'.$i, $row2['solid'])
		->setCellValue('H'.$i, $row2['solfactura'])
		->setCellValue('I'.$i, $row2['solfecha'])
		->setCellValue('J'.$i, $jefeasesor)
		->setCellValue('K'.$i, $nomcliente)
		->setCellValue('L'.$i, substr($row2['solfecha'], 5, 2))
		->setCellValue('M'.$i, $relacionista)
		->setCellValue('N'.$i, $jeferelacionista)
		->setCellValue('O'.$i, $cuota)
		->setCellValue('P'.$i, $productos)
		;
	$i++;

	$fnlbase = $fnlbase + $ttlbase;
	$fnlsol = $fnlsol + $num2;
	}
	}
}
}
// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Estadisticos resumen venta por asesores.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
?>