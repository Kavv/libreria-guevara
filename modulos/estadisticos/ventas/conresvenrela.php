<?php
$r = '../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
?>
<!doctype html>
<html>
<head>
<title>. : : S o p h y a : : .</title>
<meta charset="utf-8" />
<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/css/normalize.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/jquery/css/smoothness/jquery-ui-1.10.3.custom.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/css/css.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/css/menu.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/validation/css/validationEngine.jquery.css" />
<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/js/jquery-ui-1.10.3.custom.js"></script>
<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>
<script type="text/javascript" src="<?php echo $r ?>incluir/validation/js/languages/jquery.validationEngine-es.js"></script>
<script type="text/javascript" src="<?php echo $r ?>incluir/validation/js/jquery.validationEngine.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#form').validationEngine({
		showOneMessage:true,
		onValidationComplete: function(form, status){
			if(status == true) {
				carga();
				return true;
			}
		}
	});
	$('#fecha1').datepicker({
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		maxDate: '+0D',
		onClose: function( selectedDate ) {
			$('#fecha2').datepicker('option', 'minDate', selectedDate );
		}
	});
	$('#fecha2').datepicker({
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		maxDate: '+0D',
		onClose: function( selectedDate ) {
			$('#fecha1').datepicker('option', 'maxDate', selectedDate );
		}
	});
	$('.btnconsulta').button({ icons: { primary: 'ui-icon ui-icon-search' }});
});
</script>
<style type="text/css">
#form form{ width:350px }
#form fieldset{ padding:15px; display:block; width:350px; margin:20px auto }
#form legend{ font-weight: bold; margin-left:5px; padding:5px }
#form label{ display:inline-block; width:80px; text-align:right; float:left; margin:0.3em 2% 0 0 }
#form p{ margin:5px 0 }
</style>
</head>
<body>
<?php require($r.'incluir/src/login.php') ?>
<section id="principal">
<?php require($r.'incluir/src/cabeza.php') ?>
<?php require($r.'incluir/src/menu.php') ?>
<article id="cuerpo">
<article class="mapa">
<a href="#">Principal</a><div class="mapa_div"></div><a class="current">Estadisticos</a>
</article>
<article id="contenido">
<form id="form" name="form" action="lisresvenrela.php" method="post">
<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
<legend class="ui-widget ui-widget-header ui-corner-all">Estadistico - resumen de venta relacionistas </legend>
<p>
<label for="fechas">Intervalo:</label>
<input type="text" name="fecha1" id="fecha1" class="validate[required] text-input fecha" /> <input type="text" name="fecha2" id="fecha2" class="validate[required] text-input fecha" />
</p>
						<div class="row">
							<div class="col-md-12 col-lg-6">
								<button type="submit" class="btnconsulta btn btn-primary btn-block mt-2" name="consultar" value="Buscar">consultar</button> <!-- BOTON CONSULTAR -->
							</div>
							<div class="col-md-12 col-lg-6">
								<button type="submit" class="btnconsulta btn btn-primary btn-block mt-2" name="show-all" value="Buscar">Mostrar Todo</button>							</div>
						</div>
</fieldset>
</form>
</article>
</article>
<?php require($r.'incluir/src/pie.php') ?>
</section>
</body>
</html>