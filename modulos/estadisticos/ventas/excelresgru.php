<?php
$r = '../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
require($r.'incluir/phpexcel/Classes/PHPExcel.php');

$director = $_GET['director'];
$fecha1 = $_GET['fecha1'];
$fecha2 = $_GET['fecha2'];
$filtro = 'fecha1='.$fecha1.'&fecha2='.$fecha2;
if ($director == 'TODOS'){
$qryprin = $db->query("SELECT * FROM usuarios WHERE usudirector = '1' order by usunombre asc");
} else {
$qryprin = $db->query("SELECT * FROM usuarios WHERE usudirector = '1' AND usuid = '".$director."' order by usunombre asc");
}

$filtro = 'fecha1='.$fecha1.'&fecha2='.$fecha2;


$titulo = "RESUMEN VENTA POR GRUPOS DEL $fecha1 - $fecha2 ";



$objPHPExcel = new PHPExcel();
$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN
		)
	)
);
$objPHPExcel->getActiveSheet()->getStyle('A1:I2')->applyFromArray($styleArray);
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:I1')
			->setCellValue('A1', $titulo);
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A2', 'ASESOR')
			->setCellValue('B2', 'ESTADO ASESOR')	
            ->setCellValue('C2', 'SOLICITUDES ')
			->setCellValue('D2', 'BASE')
			->setCellValue('E2', 'DEPARTAMENTO')
			->setCellValue('F2', 'CIUDAD')
            ->setCellValue('G2', 'ID SOLICITUD')
			->setCellValue('H2', 'FECHA SOLICITUD')
			->setCellValue('I2', 'JEFE ASESOR');
			
$i = 3;



while($rowprin = $qryprin->fetch(PDO::FETCH_ASSOC)){
$qry = $db->query("SELECT * FROM usuarios WHERE usuasesor = '1' AND usudirjefe ='".$rowprin['usuid']."'  ORDER BY usunombre ");
	

	while($row = $qry->fetch(PDO::FETCH_ASSOC)){
	$qry2 = $db->query("SELECT * FROM solicitudes INNER JOIN movimientos ON (solempresa = movempresa  AND solfactura = movnumero) INNER JOIN clientes ON solcliente = cliid WHERE solfecha BETWEEN '$fecha1' AND '$fecha2' AND solasesor = '".$row['usuid']."' AND movprefijo = 'FV' AND movestado = 'FACTURADO'");
	$ttlbase = 0;
	$num2 = $qry2->rowCount();
	while($row2 = $qry2->fetch(PDO::FETCH_ASSOC)){
		$ttlbase = $row2['solbase'];
		$num3 = 1;
	
	if ($num2 > 0 or $ttlbase > 0){
	
		if($row['usuperfil'] == 56){ $estado = "DESACTIVADO";}else{ $estado = "ACTIVO";}

		if (!empty($row2['clidepresidencia'])){
		$qrydepar = $db->query("SELECT * FROM departamentos WHERE depid = '".$row2['clidepresidencia']."';");
		$row2depar = $qrydepar->fetch(PDO::FETCH_ASSOC);
		$numdepar = $qrydepar->rowCount();
			if ($numdepar == 1){
			$departamento = $row2depar['depnombre'];
			$qryciu = $db->query("SELECT * FROM ciudades WHERE ciudepto = '".$row2depar['depid']."' AND ciuid = '".$row2['cliciuresidencia']."' ;");
			$row2ciu = $qryciu->fetch(PDO::FETCH_ASSOC);
			$numciu = $qryciu->rowCount();
				if ($numciu == 1){
				$ciudad = $row2ciu['ciunombre'];
				} else {
				$ciudad = "NONE";
				} 
			} else {
			$departamento = "NONE";
			$ciudad = "NONE";
			}
		} else {
		$departamento = "NONE";
		$ciudad = "NONE";
		}

		if (!empty($row['usudirjefe'])){
			$qryjefease = $db->query("SELECT * FROM usuarios WHERE usuid = '".$row['usudirjefe']."';");
			$rowjefease = $qryjefease->fetch(PDO::FETCH_ASSOC);
			$numjefease = $qryjefease->rowCount();
			if ($numjefease == 1){
				$jefeasesor = $rowjefease['usunombre'];
			} else {
				$jefeasesor = "NONE";	
			}
		} else {
		$jefeasesor = "NONE";	
		}
	
	$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':I'.$i)->applyFromArray($styleArray);
	$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('A'.$i, $row['usunombre'])
    	->setCellValue('B'.$i, $estado)
		->setCellValue('C'.$i, $num3)
		->setCellValue('D'.$i, $ttlbase)
		->setCellValue('E'.$i, $departamento)
		->setCellValue('F'.$i, $ciudad)
    	->setCellValue('G'.$i, $row2['solid'])
		->setCellValue('H'.$i, $row2['solfecha'])
		->setCellValue('I'.$i, $jefeasesor);
	$i++;

	$fnlbase = $fnlbase + $ttlbase;
	$fnlsol = $fnlsol + $num2;
	}
	}
	}
}

// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Estadisticos resumen venta por grupos.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
?>