<?php
$r = "../../";
include($r."include/session.class.php");
include($r."include/mysql.class.php");
include($r."fpdf/fpdf.php");
$session = new Session;
$mysql = new MySql;
$session->iniciar($r);
$mysql->conectar();
$mysql->base();
$director = $_GET["director"];
$fecha1 = $_GET["fecha1"];
$fecha2 = $_GET["fecha2"];
class PDF extends FPDF{
	function Header(){
		global $fecha1;
		global $fecha2;
    	$this->SetFont('LucidaConsole','',8);
		$this->Cell(0,5,date('Y/m/d H:i:s'),0,1);
		$this->SetFont('LucidaConsole','',10);
		$this->Cell(0,5,'------------------------------------------------------------------------------------------',0,1,'C');
		$this->MultiCell(0,5,'ESTADISTICAS DE VENTA DETALLADA POR GRUPOS DEL '.$fecha1.' AL '.$fecha2,0,'C');
		$this->Cell(0,5,'------------------------------------------------------------------------------------------',0,1,'C');
    	$this->Ln(1);
	}

	function Footer(){
    	$this->SetY(-15);
    	$this->SetFont('LucidaConsole','',10);
		$this->Cell(0,5,'------------------------------------------------------------------------------------------',0,1,'C');
		$this->SetFont('LucidaConsole','',8);
    	$this->Cell(0,5,'Pagina '.$this->PageNo().'/{nb}',0,0,'R');
	}
}

$pdf = new PDF();
$pdf->AddFont('LucidaConsole','','lucon.php');
$pdf->AliasNbPages();
$pdf->AddPage();
$qrygru = mysql_query("SELECT * FROM usuarios WHERE usuid = '".$director."'");
while($rowgru = mysql_fetch_assoc($qrygru)){
	$pdf->SetFont('LucidaConsole','',10);
	$pdf->Cell(0,5,'GRUPO DE '.$rowgru["usunombre"],0,1,'C');
	$qryven = mysql_query("SELECT * FROM usuarios WHERE usuover1 = '".$rowgru["usuid"]."' OR usuid = '".$director."'");
	$fnlbase = 0;
	$fnlsol = 0;
	while($rowven = mysql_fetch_assoc($qryven)){
	$sqlsol = "SELECT * FROM solicitudes INNER JOIN clientes ON solcliente = cliid WHERE solfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND solasesor = '".$rowven["usuid"]."' AND solfactura <> '' ORDER BY solfecha";
	$qrysol = mysql_query($sqlsol);
	$numsol = mysql_num_rows($qrysol);
	$pdf->SetFont('LucidaConsole','',9);
	$pdf->SetFillColor(230,230,230);
	$pdf->SetX(20);
	if($numsol > 0){
		$pdf->Cell(170,5,$rowven["usunombre"],0,1,'C',true);
		$pdf->SetFillColor(230,230,230);
		$pdf->SetX(20);
		$pdf->Cell(65,5,'CLIENTE',0,0,'C',true);
		$pdf->Cell(25,5,'SOLICITUD',0,0,'C',true);
		$pdf->Cell(25,5,'FACTURA',0,0,'C',true);
		$pdf->Cell(25,5,'FECHA',0,0,'C',true);
		$pdf->Cell(30,5,'BASE ESTADISTICOS',0,1,'C',true);
		$ttlbase = 0;
		while($rowsol = mysql_fetch_assoc($qrysol)){
			if($i%2 == 0) $x = 255;
			else $x = 240;
			$pdf->SetFont('LucidaConsole','',8);
			$pdf->SetFillColor($x,$x,$x);
			$pdf->SetX(20);
			$pdf->Cell(65,5,$rowsol["clinombre"],0,0,'',true);
			$pdf->Cell(25,5,$rowsol["solid"],0,0,'C',true);
			$pdf->Cell(25,5,'FV-'.$rowsol["solfactura"],0,0,'C',true);
			$pdf->Cell(25,5,$rowsol["solfecha"],0,0,'C',true);
			$pdf->Cell(30,5,'$ '.number_format($rowsol["solbase"],0,',','.'),0,1,'R',true);
			$i++;
			$ttlbase = $ttlbase + $rowsol["solbase"];
		}
		$fnlbase = $fnlbase + $ttlbase;
		$fnlsol = $fnlsol + $numsol;
		$ultbase = $ultbase + $ttlbase;
		$ultsol = $ultsol + $numsol;
		$pdf->SetFillColor(230,230,230);
		$pdf->SetX(20);
		$pdf->Cell(65,5,'TOTAL VENDEDOR',0,0,'C',true);
		$pdf->Cell(25,5,$numsol,0,0,'C',true);
		$pdf->Cell(80,5,'$ '.number_format($ttlbase,0,',','.'),0,1,'R',true);
	}
}
$pdf->SetFillColor(220,220,220);
$pdf->SetX(20);
$pdf->Cell(65,5,'TOTAL GRUPO',0,0,'C',true);
$pdf->Cell(25,5,$fnlsol,0,0,'C',true);
$pdf->Cell(80,5,'$ '.number_format($fnlbase,0,',','.'),0,1,'R',true);
}
$pdf->SetFillColor(210,210,210);
$pdf->SetX(20);
$pdf->Cell(65,5,'TOTAL',0,0,'C',true);
$pdf->Cell(25,5,$ultsol,0,0,'C',true);
$pdf->Cell(80,5,'$ '.number_format($ultbase,0,',','.'),0,1,'R',true);
$pdf->Output("pdfdetgru.pdf","d");
?>