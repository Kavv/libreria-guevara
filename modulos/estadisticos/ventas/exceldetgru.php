<?php
$r = '../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
require($r.'incluir/phpexcel/Classes/PHPExcel.php');

$director = $_GET['director'];
$fecha1 = $_GET['fecha1'];
$fecha2 = $_GET['fecha2'];
$filtro = 'director='.$director.'&fecha1='.$fecha1.'&fecha2='.$fecha2;
if($director == '') $qry = $db->query("SELECT * FROM usuarios WHERE usudirector = '1'");
else $qry = $db->query("SELECT * FROM usuarios WHERE usuid = '$director'");

$ide = $_SESSION['id'];
$rowusuario = $db->query("SELECT * FROM usuarios INNER JOIN perfiles ON usuperfil = perid WHERE usuid = ".$ide.";")->fetch(PDO::FETCH_ASSOC);
$perfil = $rowusuario['usuperfil'];



$titulo = "ESTADISTICO DE VENTA DETALLADA POR GRUPO DEL $fecha1 - $fecha2 ";



$objPHPExcel = new PHPExcel();
$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN
		)
	)
);
$objPHPExcel->getActiveSheet()->getStyle('A1:I2')->applyFromArray($styleArray);
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:I1')
			->setCellValue('A1', $titulo);
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A2', 'SOLICITUD')
			->setCellValue('B2', 'FACTURA')	
            ->setCellValue('C2', 'CLIENTE ')
			->setCellValue('D2', 'FECHA')
			->setCellValue('E2', 'BASE')
			->setCellValue('F2', 'DEPARTAMENTO')
            ->setCellValue('G2', 'CIUDAD')
			->setCellValue('H2', 'ASESOR')
			->setCellValue('H2', 'DIRECTOR');
			
$i = 3;

while($row = $qry->fetch(PDO::FETCH_ASSOC)){
	$qry2 = $db->query("SELECT * FROM (solicitudes INNER JOIN clientes ON solcliente = cliid) INNER JOIN usuarios ON solasesor = usuid WHERE solfecha BETWEEN '$fecha1' AND '$fecha2' AND solover1 = '".$row['usuid']."' AND solfactura <> ''");
	$num2 = $qry2->rowCount();

while($row2 = $qry2->fetch(PDO::FETCH_ASSOC)){
	
	
		if($row['usuperfil'] == 56){ $estado = "DESACTIVADO";}else{ $estado = "ACTIVO";}

		if (!empty($row2['clidepresidencia'])){
		$qrydepar = $db->query("SELECT * FROM departamentos WHERE depid = '".$row2['clidepresidencia']."';");
		$row2depar = $qrydepar->fetch(PDO::FETCH_ASSOC);
		$numdepar = $qrydepar->rowCount();
			if ($numdepar == 1){
			$departamento = $row2depar['depnombre'];
			$qryciu = $db->query("SELECT * FROM ciudades WHERE ciudepto = '".$row2depar['depid']."' AND ciuid = '".$row2['cliciuresidencia']."' ;");
			$row2ciu = $qryciu->fetch(PDO::FETCH_ASSOC);
			$numciu = $qryciu->rowCount();
				if ($numciu == 1){
				$ciudad = $row2ciu['ciunombre'];
				} else {
				$ciudad = "NONE";
				} 
			} else {
			$departamento = "NONE";
			$ciudad = "NONE";
			}
		} else {
		$departamento = "NONE";
		$ciudad = "NONE";
		}
		
		$clientename = $row2['clinombre'].' '.$row2['clinom2'].' '.$row2['cliape1'].' '.$row2['cliape2'];

	
		if (!empty($row2['solasesor'])){
		$qryase = $db->query("SELECT * FROM usuarios WHERE usuid = '".$row2['solasesor']."';");
		$rowase = $qryase->fetch(PDO::FETCH_ASSOC);
		$numase = $qryase->rowCount();
			if ($numase == 1){
				$asesor = $rowase['usunombre'];
			} else {
				$asesor = "NONE";
			}
		} else {
			$asesor = "NONE";
		}
	
	$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':I'.$i)->applyFromArray($styleArray);
	$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('A'.$i, $row2['solid'])
    	->setCellValue('B'.$i, $row2['solfactura'])
		->setCellValue('C'.$i, $clientename)
		->setCellValue('D'.$i, $row2['solfecha'])
		->setCellValue('E'.$i, $row2['solbase'])
		->setCellValue('F'.$i, $departamento)
    	->setCellValue('G'.$i, $ciudad)
		->setCellValue('H'.$i, $asesor)
		->setCellValue('I'.$i, $row['usunombre']);
		
	$i++;



}
}
// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Estadisticos detalle venta grupos.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
?>