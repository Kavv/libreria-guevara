<?php
$r = '../../';
//INCLUIR SESION Y CONECCION
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
require ($r.'incluir/phpmailer/PHPMailer/class.phpmailer.php');
require ($r.'incluir/phpmailer/PHPMailer/class.smtp.php');

$mensaje = $_GET['mensaje'];
$error = $_GET['error'];

if($_POST['validar']){	
	
	$solicitud = $_POST['num_sol'];
	$correo = $_POST['correo'];
	$empresa = $_POST['empresa'];
	
	$filtro = 'num_sol='.$solicitud.'&empresa='.$empresa.'&correo='.$correo;
	
	$qryvali = $db->query(" SELECT * FROM solicitudes WHERE solempresa = '$empresa' AND solid = '$solicitud';");
	$num = $qryvali->rowCount();
	
	if ($num > 0){
		$error = "Ya existe una solicitud con ese numero asociada a esa empresa.";
		header('Location:envio_correo.php?error='.$error);
	} else {
		if ($empresa == '900051528' or $empresa == '900813686'){
		header('Location:pdf_numsoli.php?'.$filtro);
		}else{
		$error = "Actualmente no se posee un PDF para esta empresa, no es posible enviarlo.";
		header('Location:envio_correo.php?error='.$error);	
		}
	}
	
}

if($_GET['enviar'] == 1){

	$solicitud = $_GET['num_sol'];
	$empresa = $_GET['empresa'];
	$correo = $_GET['correo'];	
	
	$qryusuario = $db->query("SELECT * FROM usuarios INNER JOIN perfiles ON usuperfil = perid WHERE usuid = ".$_SESSION['id']); //verificacion usuario por ID de sesion
	$rowusuario = $qryusuario->fetch(PDO::FETCH_ASSOC);	


	$body             = '
		<p><center><img src="http://190.0.26.178/usa/imagenes/logos/logoiden.png" /></center></p>
		<div style="border:2px solid green; padding:8px; -webkit-box-shadow: 0px 0px 37px -2px rgba(0,0,0,0.75); -moz-box-shadow: 0px 0px 37px -2px rgba(0,0,0,0.75); box-shadow: 0px 0px 37px -2px rgba(0,0,0,0.75);">
		<p>Buen Dia!</p>
		
		<p style="margin-bottom:40px;">Cualquier inquietud con gusto la atendere.</p>
		<b>'.$rowusuario['usunombre'].' </b><br> 
		Capacitador <br>
		Idencorp Colombia <br>
		Cel.: '.$rowusuario['usutelefono'].'   <br>
		Tel.:  + 57 1 795 6210  '.$rowusuario['usuExtencion'].' <br> 
		e-Mail: d.entrenamiento@idencorp.co <br> 
		Web Site www.co 
		</p> 
		<img  src="http://190.0.26.178/usa/imagenes/logos.png" /> <br>
		<p style="color:green"> <img  src="http://190.0.26.178/usa/imagenes/casa.png" /> Cuidemos del medio ambiente. <br> Por favor no imprimas este e-mail si no es necesario.</p>
		</p> 
		<p style="font-size:11px;">Los signos de puntuacion y caracteres especiales han sido omitidos de manera predeterminada para evitar inconvenientes con los diferentes servidores de correo.</p>
		</div>
	';		
	
	$mail             = new PHPMailer();
	
	$mail->IsSMTP(); // telling the class to use SMTP
	$mail->Host       = "smtp.gmail.com"; // SMTP server
	$mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
	// 1 = errors and messages
	// 2 = messages only
	$mail->SMTPAuth   = true;                  // enable SMTP authentication
	$mail->SMTPSecure = "ssl";                 // sets the prefix to the servier
	$mail->Host       = "smtp.gmail.com";      // sets GMAIL as the SMTP server
	$mail->Port       = 465;                   // set the SMTP port for the GMAIL server	
	$mail->Username   = "d.entrenamiento@idencorp.co";  // GMAIL username
	$mail->Password   = "IdC0l387";            // GMAIL password
	$mail->SetFrom($rowusuario['usuemail'], $rowusuario['usunombre']);
	$mail->AddReplyTo($rowusuario['usuemail'], $rowusuario['usunombre']);
	$mail->Subject    = "Invitacion a capacitacion - Solicitud Numero ".$solicitud;
	$mail->IsHTML(true);
	$mail->MsgHTML($body);
	//$mail->AddEmbeddedImage($r.'imagenes/jackmiller.jpg', 'imagen','IDENCORP','base64','image/jpeg');
	$archivo = $empresa.'.pdf';
	$mail->AddAttachment($archivo,$archivo);
	$address = $correo;
	$address_copy = $email2;
	$mail->AddAddress($address);
	//$mail->AddCC($address_copy);
	if(!$mail->Send()) {
	$error = "No se pudo enviar el email por la siguiente causa: " . $mail->ErrorInfo;
	header('Location:envio_correo.php?error='.$error);
	} else{ 
	$mensaje = "El mensaje ha sido enviado correctamente";
	header('Location:envio_correo.php?mensaje='.$mensaje); 
	}
	
}
?>
<!doctype html>
<html>
<head>

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
<script type="text/javascript">

$(document).ready(function(){
				
	$('#form').validationEngine({ //VALIDACION DE CAMPOS REQUERIDOS EN FORMULARIO
		showOneMessage:true,
		onValidationComplete: function(form, status){
        	if(status) {               
            	carga();
            	return true;
			}
		}
	});

	
	$('.btnvalidar').button({ icons: { primary: 'ui-icon ui-icon-check' }});
	$('#dialog-message').dialog({ //CARACTERISTICAS DE LA VENTANA MODAL 
		height: 80,
		width: 'auto',
		modal: true
	});

});
</script>
<!-- ESTILOS EMBEBIDOS PARA MANEJAR FORMULARIO INDEPENDIENTEMENTE -->
<style type="text/css">
#form { width: 750px; margin:5px auto }
#form fieldset { padding:10px; display:block }
#form legend { font-weight: bold; margin-left:5px; padding:5px }
#form label { display:inline-block; width:100px; text-align:right; margin:0.3em 2px 0 0 }
#form .label { display:inline-block; width:100px; text-align:right; margin:0.3em 2px 0 0 }
#form p { margin:5px 0 }
</style>
</head>
<body>
<?php require($r.'incluir/src/login.php') ?> <!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
<section id="principal">
<?php require($r.'incluir/src/cabeza.php') ?> <!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
<?php require($r.'incluir/src/menu.php') ?> <!-- INCLUIMOS MENU PRINCIPAL -->
<article id="cuerpo">
<article class="mapa">
<a href="#">Principal</a><div class="mapa_div"></div><a href="#">Estadisticos</a><div class="mapa_div"></div><a href="#">Principal</a><div class="mapa_div"></div><a class="current">Envio de Correo</a>
</article>
<article id="contenido">
<form id="form" name="form" action="envio_correo.php" method="post"> <!-- ENVIO FORMULARIO POR POST -->
<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
<legend class="ui-widget ui-widget-header ui-corner-all">Envio de Correo</legend>
</br>
		<p>
		<label for="empresa">Empresa:</label> 
		<select id="empresa" name="empresa" class="validate[required]">
		<option value="">SELECCIONE</option>
		<?php
		$qry = $db->query("SELECT * FROM empresas ORDER BY empnombre");
		while($row = $qry->fetch(PDO::FETCH_ASSOC)) {
			echo '<option value='.$row['empid'].'>'.$row['empnombre'].'</option>';
		}
		?>
		</select>
		</p>
		
		<p>
		<label title ='Numero de Soliciryd' >Num Solicitud: </label>
		<input type='text' name='num_sol' id='num_sol' class='validate[required, custom[onlyNumberSp]] text-input'  />
		</p>

		<label for="correo"> Correo: </label>
		<input style="width:220px" type="text" name="correo" id="correo" class="email validate[required, custom[email]] text-input" /> 
		</p>
		
		</br>
	<p class="boton">
	<button type="submit" class="btnvalidar" name="validar" value="validar">Validar</button> <!-- BOTON VALIDAR -->
	</p>
</fieldset>
</form>
</article>
</article>
<?php require($r.'incluir/src/pie.php') ?>  <!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
</section>
<?php
if($_GET['error']) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>'.$_GET['error'].'</div>'; // MENSAJE MODAL ERROR
if($_GET['mensaje']) echo '<div id="dialog-message" title="Exitoso"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>'.$_GET['mensaje'].'</div>'; // MENSAJE MODAL EXITOSO
?>
</body>
</html>