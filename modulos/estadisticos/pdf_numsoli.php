<?php
$r = '../../';
require($r.'incluir/fpdf/fpdf.php');
require($r.'incluir/fpdf/fpdi.php');

$solicitud = $_GET['num_sol'];
$empresa = $_GET['empresa'];
$correo = $_GET['correo'];

$filtro = 'num_sol='.$solicitud.'&empresa='.$empresa.'&correo='.$correo;

if ($empresa == '900051528'){
	
	$pdf = new FPDI();                      
	$pagecount = $pdf->setSourceFile($empresa.'.pdf');
	for($i = 1; $i <=  $pagecount; $i++){
		$tplidx = $pdf->importPage($i);
		$specs = $pdf->getTemplateSize($tplidx);
		$pdf->addPage('P' , 'Legal');
		//$pdf->addPage($specs['h'] > $specs['w'] ? 'P' : 'L');
		$pdf->useTemplate($tplidx);
		$pdf->SetFont('Arial','B',17);
		
		$pdf->SetY(12);
		$pdf->SetX(185);
		$pdf->SetFillColor(250,250,250);
		$pdf->SetTextColor(220,50,50);
		$pdf->Cell(22,10,$solicitud,0,1,'C',true);

		$pdf->SetFont('Arial','B',16);
		$pdf->SetY(288);
		$pdf->SetX(185);
		$pdf->SetFillColor(230,230,230);
		$pdf->SetTextColor(220,50,50);
		$pdf->Cell(20,7,$solicitud,0,1,'C',true);
		
	}
	$pdf->Output($empresa.'.pdf');
	//$pdf->Output();

	header('Location:envio_correo.php?enviar=1&'.$filtro); 
	
} else if ($empresa == '900813686'){
	
	$pdf = new FPDI();                      
	$pagecount = $pdf->setSourceFile($empresa.'.pdf');
	for($i = 1; $i <=  $pagecount; $i++){
		$tplidx = $pdf->importPage($i);
		$specs = $pdf->getTemplateSize($tplidx);
		$pdf->addPage('P' , 'Legal');
		//$pdf->addPage($specs['h'] > $specs['w'] ? 'P' : 'L');
		$pdf->useTemplate($tplidx);
		$pdf->SetFont('Arial','B',18);
		
		$pdf->SetY(17);
		$pdf->SetX(180);
		$pdf->SetFillColor(250,250,250);
		$pdf->SetTextColor(220,50,50);
		$pdf->Cell(22,10,$solicitud,0,1,'C',true);

		$pdf->SetFont('Arial','B',16);
		$pdf->SetY(288);
		$pdf->SetX(181);
		$pdf->SetFillColor(230,230,230);
		$pdf->SetTextColor(220,50,50);
		$pdf->Cell(20,7,$solicitud,0,1,'C',true);
		
	}
	$pdf->Output($empresa.'.pdf');
	//$pdf->Output();

	header('Location:envio_correo.php?enviar=1&'.$filtro); 

} else {
	$error = "Actualmente no hay PDF para esta empresa";
	header('Location:envio_correo.php?error='.$error); 
}
?>