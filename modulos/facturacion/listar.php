<?php
	$r = '../../';
	require($r . 'incluir/session.php');
	require($r . 'incluir/connection.php');
	require($r . 'incluir/funciones.php');
	$empresa = $numero = $cliente = $estado = $fecha1 = $fecha2 = "";
	if (isset($_POST['consultar'])) {
		$empresa = $_POST['empresa'];
		$numero = $_POST['numero'];
		$cliente = $_POST['cliente'];
		$estado = $_POST['estado'];
		$fecha1 = $_POST['fecha1'];
		$fecha2 = $_POST['fecha2'];
	} elseif (isset($_GET['empresa'])) {
		$empresa = $_GET['empresa'];
		$numero = $_GET['numero'];
		$cliente = $_GET['cliente'];
		$estado = $_GET['estado'];
		$fecha1 = $_GET['fecha1'];
		$fecha2 = $_GET['fecha2'];
	}

	$filtro = 'empresa=' . $empresa . '&numero=' . $numero . '&cliente=' . $cliente . '&estado=' . $estado . '&fecha1=' . $fecha1 . '&fecha2=' . $fecha2;
	$con = 'SELECT * FROM ((movimientos INNER JOIN empresas ON movempresa = empid) INNER JOIN clientes ON movtercero = cliid) LEFT JOIN solicitudes ON (movempresa = solempresa AND movnumero = solfactura)';
	$ord = 'ORDER BY movfecha DESC, movnumero DESC';


	/* Consulta dinamica sql */

	/* Los parametros de la consulta sql se genera dinamicamente 
		en base a los datos recibidos para delimitar los resultados */
	$parameters = [];
	array_push($parameters, "movprefijo = 'FV'");
	if ($empresa != "")
		array_push($parameters, "movempresa LIKE '%$empresa%'");
	if ($numero != "")
		array_push($parameters, "movnumero LIKE '%$numero%'");
	if ($cliente != "")
		array_push($parameters, "movtercero LIKE '%$cliente%'");
	if ($cliente != "")
		array_push($parameters, "solasesor LIKE '%$asesor%'");
	if ($estado != "")
		array_push($parameters, "movestado = '$estado'");
	if ($fecha1 != "")
		array_push($parameters, "movfecha BETWEEN '$fecha1' AND '$fecha2'");

	// Consulta base
	$sql = $con;
	foreach ($parameters as $index => $parameter) {
		// Se agregan los parametros del WHERE
		if ($index == 0)
			$sql .= " WHERE " . $parameter;
		else
			$sql .= " AND " . $parameter;
	}
	// Se completa la consulta
	$sql .= $ord;

	$qry = $db->query($sql);
?>
<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>

	<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
	<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#tabla').dataTable({
				'bJQueryUI': true,
				'sPaginationType': 'full_numbers',
				'oLanguage': {
					'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
				},
				'bSort': false,
			});

			$('.pdf').click(function() {
				newSrc = $(this).attr('data-rel');
				if (validation_file(newSrc) == 200)
					$('#modal').html("<iframe src='" + newSrc + "' width='100%' height='100%'></iframe>");
				else
					$('#modal').html("<spam>El PDF no fue encontrado</spam>");
				$('#modal').dialog({
					modal: true,
					width: '800',
					height: '900',
					title: 'PDF de la factura'
				});
			});


			$('.msj_anular').click(function(e) {
				e.preventDefault();
				var targetUrl = $(this).attr('href');
				var $dialog_link_follow_confirm = $('<div></div>').html("<p><span class='ui-icon ui-icon-alert' style='float:left; margin:2px 7px 7px 0;'></span>Esta seguro que desea anular la factura?</p>").
				dialog({
					title: 'Porfavor confirmar',
					buttons: {
						'Si': function() {
							window.location.href = targetUrl;
						},
						'No': function() {
							$(this).dialog("close");
						}
					},
					modal: true,
					width: 'auto',
					height: 'auto'
				});
				$dialog_link_follow_confirm.dialog("open");
			});
		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a class="current">Solicitudes</a>
			</article>
			<article id="contenido">
				<h2>Listado de facturas <?php echo $estado;
										if ($fecha1 != "") echo " desde " . $fecha1 . " al " . $fecha2 ?></h2>
				<table id="tabla">
					<thead>
						<tr>
							<th>Empresa</th>
							<th>Factura</th>
							<th>Solicitud</th>
							<th>Cliente</th>
							<th>Fecha</th>
							<th>Estado</th>
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php
						$i = 0;
						while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
						?>
							<tr>
								<td title="<?php echo $row['empid'] . ' ' . $row['empdigito'] ?>"><?php echo $row['empnombre'] ?></td>
								<td align="center"><?php echo $row['movnumero'] ?></td>
								<td align="center"><?php echo $row['solid'] ?></td>
								<td title="<?php echo $row['cliid'] . ' ' . $row['clidigito'] ?>"><?php echo $row['clinombre'] . ' ' . $row['clinom2'] . ' ' . $row['cliape1'] . ' ' . $row['cliape2'] ?></td>
								<td align="center"><img src="<?php echo $r ?>imagenes/iconos/date.png" title="<?php echo $row['movfecha'] ?>" /></td>
								<td><?php echo $row['movestado'] ?></td>
								<td align="center"><img src="<?php echo $r ?>imagenes/iconos/pdf.png" class="pdf" data-rel="<?php echo $r . 'pdf/facturas/' . $row['movempresa'] . '/' . $row['movnumero'] . '.pdf' ?>" title="Factura" /></td>
								<td align="center">
								<?php
								if ($rowlog['perfacfac'] == '1' && $row['movestado'] == 'FACTURADO') {
								?>
									<a href="anular.php?<?php echo '&empresa=' . $row['movempresa'] . '&id=' . $row['solid'] . '&numero=' . $row['movnumero'] ?>" class="msj_anular" title="anular"><img src="<?php echo $r ?>imagenes/iconos/cancelar.png" class="grayscale" /></a>
								<?php
								} else echo '<img src="' . $r . 'imagenes/iconos/cancelar.png" class="gray" />';
								?>
								</td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
				<p class="boton">
					<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href='consultar.php'">Atras</button>
				</p>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<div id="modal" style="display:none"></div>
</body>

</html>