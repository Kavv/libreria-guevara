<?php
/* ESTO REALMENTE IMPORTA? */
$r = "../";
include($r."include/session.class.php");
include($r."include/mysql.class.php");
$session = new Session;
$mysql = new MySql;
$session->iniciar($r);
$mysql->conectar();
$mysql->base();
include($r."fpdf/fpdf.php");

$nit = $_GET["nit"];
$prefijo = $_GET["prefijo"];
$numero = $_GET["numero"];
$cedula = $_GET["cedula"];
$estado = $_GET["estado"];
$fecha1 = $_GET["fecha1"];
$fecha2 = $_GET["fecha2"];

if($nit != "" && $numero == "" && $estado == "") $sql = "SELECT * FROM ((movimientos INNER JOIN empresas ON movempresa = empid) INNER JOIN clientes ON movtercero = cliid) LEFT JOIN solicitudes ON (movempresa = solempresa AND movnumero = solfactura) WHERE movempresa = '".$nit."' AND movprefijo = '".$prefijo."' ORDER BY movfecha, movnumero";
elseif($nit == "" && $numero != "" && $estado == "") $sql = "SELECT * FROM ((movimientos INNER JOIN empresas ON movempresa = empid) INNER JOIN clientes ON movtercero = cliid) LEFT JOIN solicitudes ON (movempresa = solempresa AND movnumero = solfactura) WHERE movnumero LIKE '%".$numero."%' AND movprefijo = '".$prefijo."' ORDER BY movfecha, movnumero";
elseif($nit != "" && $numero != "" && $estado == "") $sql = "SELECT * FROM ((movimientos INNER JOIN empresas ON movempresa = empid) INNER JOIN clientes ON movtercero = cliid) LEFT JOIN solicitudes ON (movempresa = solempresa AND movnumero = solfactura) WHERE movempresa = '".$nit."' AND movnumero LIKE '%".$numero."%' AND movprefijo = '".$prefijo."' ORDER BY movfecha, movnumero";
elseif($cedula != "") $sql = "SELECT * FROM ((movimientos INNER JOIN empresas ON movempresa = empid) INNER JOIN clientes ON movtercero = cliid) LEFT JOIN solicitudes ON (movempresa = solempresa AND movnumero = solfactura) WHERE movtercero = '".$cedula."' AND movprefijo LIKE '%".$numero."%' ORDER BY movfecha, movnumero";
elseif($fecha1 != "" && $estado == "") $sql = "SELECT * FROM ((movimientos INNER JOIN empresas ON movempresa = empid) INNER JOIN clientes ON movtercero = cliid) LEFT JOIN solicitudes ON (movempresa = solempresa AND movnumero = solfactura) WHERE movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = '".$prefijo."' ORDER BY movfecha, movnumero";
elseif($estado != "") $sql = "SELECT * FROM ((movimientos INNER JOIN empresas ON movempresa = empid) INNER JOIN clientes ON movtercero = cliid) LEFT JOIN solicitudes ON (movempresa = solempresa AND movnumero = solfactura) WHERE movprefijo = '".$prefijo."' AND movestado = '".$estado."' ORDER BY movfecha, movnumero";
else $sql = "SELECT * FROM ((movimientos INNER JOIN empresas ON movempresa = empid) INNER JOIN clientes ON movtercero = cliid) LEFT JOIN solicitudes ON (movempresa = solempresa AND movnumero = solfactura) WHERE movprefijo = '".$prefijo."' ORDER BY movfecha, movnumero";

$qry = mysql_query($sql);
$num = mysql_num_rows($qry);

class PDF extends FPDF{
	function Header(){
		global $estado;
		global $fecha1;
		global $fecha2;
    	$this->SetFont('LucidaConsole','',8);
		$this->Cell(0,5,date('Y/m/d H:i:s'),0,1);
		$this->SetFont('LucidaConsole','',11);
		$this->Cell(0,5,'------------------------------------------------------------------------------------------------------------------------',0,1,'C');
		if($fecha1 != "") $this->Cell(0,5,'LISTADO DE FACTURAS '.$estado.' DEL '.$fecha1.' AL '.$fecha2,0,1,'C');
    	else $this->Cell(0,5,'LISTADO DE FACTURAS '.$estado,0,1,'C');
		$this->Cell(0,5,'------------------------------------------------------------------------------------------------------------------------',0,1,'C');
    	$this->Ln(5);
	}

	function Footer(){
    	$this->SetY(-15);
    	$this->SetFont('LucidaConsole','',11);
		$this->Cell(0,5,'------------------------------------------------------------------------------------------------------------------------',0,1,'C');
		$this->SetFont('LucidaConsole','',8);
    	$this->Cell(0,5,'Pagina '.$this->PageNo().'/{nb}',0,0,'R');
	}
}

$pdf = new PDF();
$pdf->AddFont('LucidaConsole','','lucon.php');
$pdf->AliasNbPages();
$pdf->AddPage('L');

$pdf->SetFont('LucidaConsole','',10);
$pdf->SetFillColor(220,220,220);
$pdf->SetX(18);
$pdf->Cell(90,5,'EMPRESA',0,0,'C',true);
$pdf->Cell(20,5,'SOLICITUD',0,0,'C',true);
$pdf->Cell(30,5,'FACTURA',0,0,'C',true);
$pdf->Cell(25,5,'FECHA',0,0,'C',true);
$pdf->Cell(65,5,'CLIENTE',0,0,'C',true);
$pdf->Cell(30,5,'ESTADO',0,1,'C',true);
$i = 1;
$pdf->SetFont('LucidaConsole','',8);
while($row = mysql_fetch_assoc($qry)){
	if($i%2 == 0) $x = 235;
	else $x = 250;
	$pdf->SetFillColor($x,$x,$x);
	$pdf->SetX(18);
	$pdf->Cell(90,5,$row["empnombre"],0,0,'',true);
	$pdf->Cell(20,5,$row["solid"],0,0,'C',true);
	$pdf->Cell(30,5,$row["movnumero"],0,0,'C',true);
	$pdf->Cell(25,5,$row["movfecha"],0,0,'C',true);
	$pdf->Cell(65,5,$row["clinombre"].' '.$row["clinom2"].' '.$row["cliape1"].' '.$row["cliape2"],0,0,'',true);
	$pdf->Cell(30,5,$row["movestado"],0,1,'',true);
	$i++;
}

$pdf->Output("facturas.pdf","d");
?>