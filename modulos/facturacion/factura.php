<?php
	$r = '../../';
	require($r . 'incluir/session.php');
	require($r . 'incluir/connection.php');
	require($r . 'incluir/funciones.php');

	$fecha = date('Y-m-d');
	$empresa = $_GET['empresa'];
	$id = $_GET['id'];
	$rowpar = $db->query("SELECT * FROM parametros")->fetch(PDO::FETCH_ASSOC);
	$qry = $db->query("SELECT * FROM (solicitudes INNER JOIN clientes ON solcliente = cliid) INNER JOIN empresas ON solempresa = empid INNER JOIN usuarios ON solasesor = usuid WHERE solempresa = '$empresa' AND solid = '$id'");

	$row = $qry->fetch(PDO::FETCH_ASSOC);

	// Verificamos si no se encuentra facturada
	if($row['solestado'] == "FACTURADO")
	{
		header('Location:facturar.php');
		exit();
	}

	$qry2 = $db->query("SELECT * FROM detsolicitudes WHERE detempresa = '$empresa' AND detsolicitud = '$id'");
	while ($row2 = $qry2->fetch(PDO::FETCH_ASSOC)) {
		$row3 = $db->query("SELECT * FROM productos WHERE proid = '" . $row2['detproducto'] . "'")->fetch(PDO::FETCH_ASSOC);
		if ($row2['detcantidad'] > $row3['procantidad'])
		{
			header('Location:facturar.php');
			exit();
		}
	}

	if ($row['solncuota'] > 0) {
		$cuota = round((($row['soltotal'] - $row['solcuota']) / $row['solncuota']),2);
		// Formula [(cuota mensual * (1 - (1 + financiacion)^-#cuotas )) / financiacion ]
		@$vlrpresente = round(($cuota * (1 - pow((1 + $rowpar['parfinanciacion']), -$row['solncuota']))) / ($rowpar['parfinanciacion']),2);
	} else {
		$vlrpresente = $row['soltotal'];
	}
	$financiacion = ($row['soltotal'] - $vlrpresente);
	// Genera el valor consecutivo de un tipo de movimiento (FV) de una empresa en especifico
	$rowmax = $db->query("SELECT MAX(movnumero)+1 AS ultimo FROM movimientos WHERE movempresa = '$empresa' AND movprefijo = 'FV'")->fetch(PDO::FETCH_ASSOC);
	if ($rowmax['ultimo'] == '')
		$rowmax['ultimo'] = 1;

	//No me parece necesario el uso de zerofill
	//$numero = zerofill($rowmax['ultimo'], 10);
	$numero  = $rowmax['ultimo'];
?>
<!doctype html>
<html>

<head>

	<?php
		require($r . 'incluir/src/head.php');
		require($r . 'incluir/src/head-form.php');
	?>

	<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
	<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>

	<script type="text/javascript">
		$(document).ready(function() {

			$('#tabla').dataTable({
				'bPaginate': false,
				'bLengthChange': false,
				'bFilter': false,
				'bSort': false,
				'bInfo': false,
				'bAutoWidth': false,
				'bJQueryUI': true
			});

			$('#total').keyup(function(event) {
				if($("#ncuota").val() > 0)
				{
					var nuecuota = ($('#total').val() - $('#cuotaini').val()) / $('#ncuota').val();
					$('#cuota').val(nuecuota.toFixed(2));
					var porcentaje = parseFloat($('#porcentaje').val());
					var vlrpresente = (nuecuota * (1 - Math.pow((1 + porcentaje), -$('#ncuota').val()))) / porcentaje;
				}
				else
					var vlrpresente = parseFloat($('#total').val());

				$('#totsin').val(vlrpresente.toFixed(2));
				var financiacion = $('#total').val() - $('#totsin').val();
				$('#finan').val(financiacion.toFixed(2));
			});

			$('.pdf').click(function() {
				console.log("entrA");
				newSrc = $(this).attr('data-rel');
				if (validation_file(newSrc) == 200)
					$('#modal').html("<iframe src='" + newSrc + "' width='100%' height='100%'></iframe>");
				else
					$('#modal').html("<spam>El PDF no fue encontrado</spam>");

				$('#modal').dialog({
					modal: true,
					width: '600',
					height: '800',
					title: 'PDF de la solicitud'
				});
				console.log("sale");
			});
			$('.historico').click(function() {
				newHref = $(this).attr('data-rel');
				$('#dialog2').load(newHref).dialog({
					modal: true,
					width: 800
				});
			});
		});
	</script>
	<style type="text/css">
		#tabla thead {
			display: none
		}
	</style>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a class="current">Facturacion</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="finalizar.php" method="post">
					<input type="hidden" name="id" value="<?php echo $id ?>" readonly />

					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">Factura</legend>
						<p>
							<img src="<?php echo $r ?>imagenes/logos/<?php echo $empresa . "/" . $row['logo']?>" class="img-fluid" title="<?php echo $row['empnombre'] ?>">
							<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12">
								<legend class="ui-widget ui-widget-header ui-corner-all">Factura de venta <?php if ($row['solncuota'] > 0) echo 'a plazos';
																											else echo 'de contado'; ?></legend>
								<div align="center">Pdf de la Solicitud <img style="cursor:pointer;" src="<?php echo $r ?>imagenes/iconos/pdf.png" class="pdf" data-rel="<?php echo $r . 'pdf/solicitudes/' . $row['solempresa'] . '/' . $row['solid'] . '.pdf' ?>" title="Solicitud en PDF" /></div>
								<div align="center">Historico de la Solicitud <img style="cursor:pointer;" src="<?php echo $r ?>imagenes/iconos/comments.png" class="historico" data-rel="notas.php?empresa=<?php echo $empresa . '&id=' . $id ?>" title="Historia de la solicitud" /></div> 
								<?php
								if ($row['solposcontado'] == 1) {
									echo "<div align='center'> <strong style='color:red;'> PAGO CON POSIBLE CONTADO </strong> </div></br>";
								}
								?>
								<div class="row">
									<div class="col-md-6  col-sm-12">
										<input type="text" value="FV" class="documento" readonly />
									</div>
									<div class="col-md-6  col-sm-12">
										<input type="text" name="numero" class="consecutivo" value="<?php echo $rowmax['ultimo'] ?>" readonly />
									</div>
								</div>
								
								<input type="text" id="fecha" class="fecha" name="fecha" value="<?php echo $fecha ?>" readonly />

								<div align="center">
									<input type="hidden" id="porcentaje" name="porcentaje" value="<?php echo $rowpar['parfinanciacion'] ?>">
								</div>
							</fieldset>
						</p>
						<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12">
							<legend class="ui-widget ui-widget-header ui-corner-all">Informacion</legend>
							<input type="hidden" name="empresa" value="<?php echo $empresa ?>" readonly size="12" />
							<label for="vendedor"><strong>Asesor:</strong></label> 
							<input type="text" name="vendedor" class="usuario" value="<?php echo $row['solasesor'] ?>" readonly /> 
							| <label class="not-w"><?php echo $row['usunombre'] ?></label><br />
							<label for="cliente"><strong>Cliente:</strong></label> 
							<input type="text" name="cliente" class="usuario" value="<?php echo $row['solcliente'] ?>" readonly /> 
							| <label class="not-w"><?php echo $row["clinombre"] . ' ' . $row['clinom2'] . ' ' . $row['cliape1'] . ' ' . $row['cliape2'] ?></label>
						</fieldset>
						<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12">
							<legend class="ui-widget ui-widget-header ui-corner-all">Detalle de la Solicitud</legend>
							<table id="tabla">
								<thead>
									<tr>
										<th>Codigo</th>
										<th>Producto</th>
										<th>Cantidad</th>
										<th>Precio</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$qry = $db->query("SELECT * FROM detsolicitudes INNER JOIN productos ON detproducto = proid WHERE detempresa = '$empresa' AND detsolicitud = '$id'");
									while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
										$qry2 = $db->query("UPDATE productos SET proestado = 'PROCESO' WHERE proid = '" . $row2['detproducto'] . "'");
									?>
										<tr>
											<td><?php echo $row2['proid'] ?></td>
											<td align="left"><?php echo $row2['pronombre'] ?></td>
											<td><input class="cantidad" value="<?php echo $row2['detcantidad'] ?>" readonly /></td>
											<td align="right"><input class="valor" value="<?php echo $row2['proprecio'] ?>" readonly /></td>
										</tr>
									<?php
									}
									?>
								</tbody>

								<?php
								//Log de acciones realizadas por usuario en la BD	
								$qrylogsregister = $db->query("SELECT * FROM usuarios INNER JOIN perfiles ON usuperfil = perid WHERE usuid = '" . $_SESSION['id'] . "'"); //verificacion usuario por ID de sesion
								$rowlogsregister = $qrylogsregister->fetch(PDO::FETCH_ASSOC);
								$qrylogsregister = $db->query("INSERT INTO logs ( usuario, id_usuario , ip_usuario , accion_usuario, fecha_accion) VALUES ( '" . $rowlogsregister['usunombre'] . "', '" . $_SESSION['id'] . "', '" . $_SERVER['REMOTE_ADDR'] . "' , 'INGRESO A FACTURAR EL CLIENTE  " . $row["clinombre"] . " " . $row["clinom2"] . " " . $row["cliape1"] . " " . $row["cliape2"] . " NUM DOCUMENTO - " . $row["cliid"] . "  FV " . $rowmax['ultimo'] . " Y EMPRESA " . $empresa . "  '  , '" . date("Y-m-d H:i:s") . "' );"); 
								?>

								<tfoot>
									<tr>
										<td colspan="4">
											<div style="float:right">
											<label>Total sin financiacion: </label> 
											<input class="valor" type="text" id="totsin" name="totsin" value="<?php echo $vlrpresente ?>" readonly />
										</div>
										</td>
									</tr>
									<tr>
										<td colspan="4">
											<div style="float:right">
											<label>Financiacion:</label> 
											<input class="valor" type="text" id="finan" name="finan" value="<?php echo $financiacion ?>" readonly />
										</div>
										</td>
									</tr>
									<tr>
										<td colspan="4">
											<div style="float:right">
											<label>Total:</label> 
											<input class="valor" type="text" id="total" name="total" value="<?php echo $row['soltotal'] ?>" required />
										</div>
										</td>
									</tr>
									<tr>
										<td colspan="4">
											<div style="float:right">
											<label>Base:</label> 
											<input type="text" id="base" name="base" class="valor" value="<?php echo $row['solbase'] ?>" required />
										</div>
										</td>
									</tr>
								</tfoot>
							</table>
						</fieldset>
						<br>
						<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12">
							<legend class="ui-widget ui-widget-header ui-corner-all">Forma de pago</legend>
							<label>Cuota inicial:</label> 
							<input id="cuotaini" name="cuotaini" class="valor" value="<?php echo $row['solcuota'] ?>" readonly />
							<label>Cantidad de cuotas:</label>
							<input type="text" value="<?php echo $row['solncuota']?>" id="ncuota" name="ncuota" class="cantidad" readonly /> 
							<?php
							if ($row['solncuota'] > 0) {
								$dia = substr($row['solcompromiso'], 8, 2);
								echo '
								<label>Cuotas de: </label> 
								<input id="cuota" name="cuota" class="valor" value="' . $cuota . '" readonly /> 
								<label class="d-flex justify-content-center">Los ' . $dia . ' de cada mes - desde el ' . $row['solcompromiso'] . '</label>' ?></td>
							<?php
							}
							?>
						</fieldset>
						<br>

<!-- 						<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12">
							<legend class="ui-widget ui-widget-header ui-corner-all">Forma de Despacho</legend> -->
							<!-- ALCARAR QUE ES ESTO? -->
							<!-- Aqui se establecia si era oficina o americanlogisticsa-->
							<!-- No estaba desarrollado y de igual forma no es escalable a NIC -->
							<!-- Queda pendiente evaliar si aqui no se deberian de mostrar los medios de despacho -->
							<!-- <center>
								<label for="despacho">Despachar a: </label>
								<select name="despacho" class="validate[required]" required >
								<option value='0'>OFICINA</option>
								</select>
							</center>
						</fieldset> -->

						<br><br><br>
						<p class="boton">
							<button type="button" class="btn btn-danger btncancelar" onClick="carga(); location.href = 'cancelar.php?empresa=<?php echo $empresa . '&id=' . $id ?>'">cancelar</button>
							<button type="submit" class="btn btn-primary btnfinalizar" name="finalizar" value="finalizar">finalizar</button>
						</p>
					</fieldset>
				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<div id="modal" style="display:none"></div>
	<div id="dialog2" title="Historico de la solicitud <?php echo $row['solid'] ?>" style="display:none"></div>
</body>

</html>