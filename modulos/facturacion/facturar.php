<?php
	$r = '../../';
	require($r . 'incluir/session.php');
	require($r . 'incluir/connection.php');

	@$error = $_GET['error'];
	@$mensaje = $_GET['mensaje'];

	$qry = $db->query("SELECT * FROM ((solicitudes INNER JOIN empresas ON solempresa = empid) INNER JOIN clientes ON solcliente = cliid) INNER JOIN usuarios ON solasesor = usuid WHERE solestado = 'APROBADO SCREEN' OR solestado = 'APROBADO' ORDER BY solfechreg");
?>
<!doctype html>
<html>

<head>
	<meta charset="utf-8">
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>

	<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
	<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#tabla').dataTable({
				'bJQueryUI': true,
				'sPaginationType': 'full_numbers',
				'oLanguage': {
					'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
				},
				'bSort': false,
			});
			$('.pdf').click(function() {
				newSrc = $(this).attr('data-rel');
				if (validation_file(newSrc) == 200)
					$('#modal').html("<iframe src='" + newSrc + "' width='100%' height='100%'></iframe>");
				else
					$('#modal').html("<spam>El PDF no fue encontrado</spam>");
				$('#modal').dialog({
					modal: true,
					width: '600',
					height: '800',
					title: 'PDF de la solicitud'
				});
			});
		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a class="current">Facturacion</a>
			</article>
			<article id="contenido">
				<h2>Listado de solicitudes a facturar</h2>
				<div class="reporte">

				</div>
				<table id="tabla">
					<thead>
						<tr>
							<th>Empresa</th>
							<th>Solicitud</th>
							<th>Cliente</th>
							<th>Asesor</th>
							<th>Creación</th>
							<th>Fin Creación</th>
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php
						while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
						?>
							<tr>
								<td title="<?php echo $row['empid'] . ' ' . $row['empdigito'] ?>"><?php echo $row['empnombre'] ?></td>
								<td align="center"><?php echo $row['solid'] ?></td>
								<td title="<?php echo $row['cliid'] . ' ' . $row['clidigito'] ?>"><?php echo $row['clinombre'] . ' ' . $row['clinom2'] . ' ' . $row['cliape1'] ?> <?php echo $row['cliape2'] ?></td>
								<td title="<?php echo $row['usuid'] ?>"><?php echo $row['usunombre'] ?></td>
								<td align="center"><img src="<?php echo $r ?>imagenes/iconos/date.png" title="<?php echo $row['solfecha'] ?>" /></td>
								<td align="center"><img src="<?php echo $r ?>imagenes/iconos/date.png" title="<?php echo $row['solfechreg'] ?>" /></td>
								<td align="center"><img src="<?php echo $r ?>imagenes/iconos/pdf.png" class="pdf" data-rel="<?php echo $r . 'pdf/solicitudes/' . $row['solempresa'] . '/' . $row['solid'] . '.pdf' ?>" title="Solicitud en PDF" /></td>
								<?php
									/* Se realizan las consultas necesarias para evaluar si la cantidad de los productos 
									especificados en el detalle de la solicitud son menores a la cantidad que hay en
									el inventario actualmente */
									$qry2 = $db->query("SELECT * FROM detsolicitudes WHERE detempresa = '" . $row['solempresa'] . "' AND detsolicitud = '" . $row['solid'] . "'");
									$bandera = 0;
									while ($row2 = $qry2->fetch(PDO::FETCH_ASSOC)) {
										$row3 = $db->query("SELECT * FROM productos WHERE proid = '" . $row2['detproducto'] . "'")->fetch(PDO::FETCH_ASSOC);
										if ($row2['detcantidad'] > $row3['procantidad'])
										{
											$bandera = 1;
											$producto_falante = $row3['pronombre'];
										}
									}

									/* ESTO REALMENTE IMPORTA? */
									/* Este codigo ya estaba comentariado, pero no recuerdo si fui yo quien lo comentario previamente,
									queda pendiente a evaluar debido a que incluye "movimientos" los cuales no han sido abordados aun */


									/*		
									$rowprimero = $db->query("SELECT * FROM detsolicitudes INNER JOIN productos ON detproducto = proid WHERE detempresa = ".$row['solempresa']." AND detsolicitud = '".$row['solid']."'")->fetch(PDO::FETCH_ASSOC);

									$qrypri = $db->query("SELECT * FROM detmovimientos INNER JOIN movimientos ON (movempresa = dmoempresa AND movprefijo = dmoprefijo AND movnumero = dmonumero) 
									WHERE movfecha <= '$hoy' AND dmoempresa = ".$row['solempresa']." and dmoproducto = '".$rowprimero['detproducto']."' ORDER BY movfecha desc LIMIT 1;");
									while($rowpri = $qrypri->fetch(PDO::FETCH_ASSOC)){
										if($rowprimero['detcantidad'] > $rowpri['dmoinventario']){
											$bandera = 1;
											$producto_fal = $rowpri['detproducto'];
										}
									}	
									*/
								?>
								<td align="center">
									<?php
									/* bandera define si cumple o no los requisitos para que la solicitud pueda ser validada */
									if ($bandera == 1) {
										echo 'No tiene existencia, del producto ' . $producto_falante; 
									} else {
										echo '<a href="factura.php?empresa=' . $row['solempresa'] . '&id=' . $row['solid'] . '" title="facturar" onClick="carga()"><img src="' . $r . 'imagenes/iconos/tick.png" class="grayscale" title="Facturar" /></a>';
									}
									?>
								</td>
							</tr>
						<?php
						}
						?>
					</tbody>
				</table>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<?php
	if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>';
	elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>';
	?>
	<div id="modal" style="display:none"></div>
</body>

</html>