<?php
	$r = '../../';
	require($r.'incluir/fpdf/fpdf.php');
	require($r.'incluir/fpdf/fpdi.php');
	$empresa = $_GET['empresa'];
	$factura = $_GET['factura'];
	$pdf = new FPDI();                      
	$pagecount = $pdf->setSourceFile($r.'pdf/facturas/'.$empresa.'/'.$factura.'.pdf');
	for($i = 1; $i <=  $pagecount; $i++){
		$tplidx = $pdf->importPage($i);
		$specs = $pdf->getTemplateSize($tplidx);
		$pdf->addPage($specs['h'] > $specs['w'] ? 'P' : 'L');
		$pdf->useTemplate($tplidx);
		$pdf->SetFont('Arial','B',50);
		$pdf->SetTextColor(255,0,0);
		$pdf->SetY(140);
		$pdf->Cell(0,35,'A  N  U  L  A  D  A',0,0,'C');
	}
	$pdf->Output($r.'pdf/facturas/'.$empresa.'/'.$factura.'.pdf');
	$pdf->Output();
?>