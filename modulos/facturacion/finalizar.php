<?php
	$r = '../../';
	require($r . 'incluir/session.php');
	require($r . 'incluir/connection.php');

	if(isset($_POST['empresa']))
	{
		$empresa = $_POST['empresa'];
		$id = $_POST['id'];
		$numero = $_POST['numero'];
		$base = $_POST['base'];
		// Valor no se estaba utilizando
		$valor = $_POST['totsin'];

		$porcentaje = $_POST['porcentaje'];
		$financiacion = $_POST['finan'];
		$total = $_POST['total'];
		$cliente = $_POST['cliente'];
		$fecha = $_POST['fecha'];
		$saldo = $total - $_POST['cuotaini'];
		if(isset($_POST['cuota']))
			$cuota = $_POST['cuota'];
		$ncuota = $_POST['ncuota'];
		$data = 0;
		if(isset($_POST['data']))
			$data = $_POST['data'];
	
		$despacho = $_POST['despacho'];
	
	
		// Se evaluo y se realizo el cambio de $total por $valor en la insercion del movimiento
		$qry = $db->query("INSERT INTO movimientos(movempresa, movprefijo, movnumero, movtercero, movdocumento, movfecha, movvalor, movfinan, movdescuento, movsaldo, movestado) VALUES 
												  ('$empresa', 'FV', '$numero', '$cliente', '$numero', '$fecha', $valor, $financiacion, 0, $saldo, 'FACTURADO')");
		$qry = $db->query("SELECT * FROM detsolicitudes INNER JOIN productos ON detproducto = proid WHERE detempresa = '$empresa' AND detsolicitud = '$id'");
		while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
			$qry2 = $db->query("INSERT INTO detmovimientos (dmoempresa, dmoprefijo, dmonumero, dmoproducto, dmocantidad, dmounitario, dmototal, dmoprecio, dmoptotal) VALUES 
															('$empresa', 'FV', '$numero', '" . $row['detproducto'] . "', " . $row['detcantidad'] . ", " . $row['procosto'] . ", " . $row['detcantidad'] * $row['procosto'] . ", " . $row['proprecio'] . ", " . $row['detcantidad'] * $row['proprecio'] . ")");
			$qry2 = $db->query("UPDATE productos SET procantidad = procantidad - " . $row['detcantidad'] . ", proestado = 'OK' WHERE proid = '" . $row['detproducto'] . "'");
			$row2 = $db->query("SELECT * FROM productos WHERE proid = '" . $row['detproducto'] . "'")->fetch(PDO::FETCH_ASSOC);
			$qry2 = $db->query("UPDATE detmovimientos SET dmoinventario = " . $row2['procantidad'] . ", dmovinventario = " . $row2['procosto'] . ", dmotinventario = dmoinventario * dmovinventario WHERE dmoempresa = '$empresa' AND dmoprefijo = 'FV' AND dmonumero = '$numero' AND dmoproducto = '" . $row2['proid'] . "'");
		}
		
		$qry2 = $db->query("UPDATE solicitudes SET soltotal = $total, solbase = $base, solestado = 'FACTURADO', solfactura = '$numero', solusufac = '" . $_SESSION['id'] . "', solfechafac = NOW() WHERE solempresa = '$empresa' AND solid = '$id'");
	
		if ($ncuota > 0) {
			// Al existir cuotas se crea una nueva cartera
			$qry2 = $db->query("INSERT INTO carteras (carempresa, carfactura, carcliente, cartotal, carcuota, carsaldo, carncuota, carfecha, carestado, cardata) VALUES ('$empresa', '$numero', '$cliente', $saldo, $cuota, $saldo, $ncuota, '$fecha', 'ACTIVA', '$data')") or die($db->errorInfo()[2]);
			//Tambien se generan los registros que controlan cada cuota
			$row = $db->query("SELECT * FROM solicitudes INNER JOIN empresas ON solempresa = empid WHERE solempresa = '$empresa' AND solid = '$id'")->fetch(PDO::FETCH_ASSOC);
	
			$i = 1;
			$fechacom = $fechapago = $row['solcompromiso'];

			while($i <= $ncuota){
				$qry = $db->query("INSERT INTO detcarteras (dcaempresa, dcafactura, dcacuota, dcafecha, dcaestado) VALUES ('$empresa', '$numero', $i, '$fechacom', 'ACTIVA')");
				$saldo = $saldo - $cuota;
				$i++;
				$fechacom = date('Y-m-d', strtotime($fechacom.' next month'));
			}
		}
		
		header('Location:finalizar.php?empresa=' . $empresa . '&id=' . $id);
		exit();
	}
	else
	{
		$empresa = $_GET['empresa'];
		$id = $_GET['id'];
	}
?>
<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a class="current">Facturacion</a>
			</article>
			<article id="contenido">
				<p align="center">
					<iframe src="pdffactura.php?empresa=<?php echo $empresa . '&id=' . $id ?>" class="col-sm-12 col-md-10" height="900"></iframe>
				</p>
				<p class="boton">
					<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href='facturar.php'">atras</button>
				</p>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
</body>

</html>