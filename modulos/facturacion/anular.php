<?php
	$r = '../../';
	require($r . 'incluir/session.php');
	require($r . 'incluir/connection.php');
	require($r . 'incluir/funciones.php');
	$empresa = $_GET['empresa'];
	$factura = $_GET['numero'];
	if(!isset($_GET['ready']))
	{
		$id = $_GET['id'];
		$rmax = $db->query("SELECT MAX(movnumero)+1 AS ultimo FROM movimientos WHERE movempresa = '$empresa' AND movprefijo = 'AN'")->fetch(PDO::FETCH_ASSOC);
		if ($rmax['ultimo'] == '')
			$rmax['ultimo'] = 1;
		$numero = $rmax['ultimo'];
		$row = $db->query("SELECT * FROM movimientos WHERE movempresa = '$empresa' AND movprefijo = 'FV' AND movnumero = '$factura'")->fetch(PDO::FETCH_ASSOC);
		
		// Verificamos si el proceso ya se ha realizado anteriormente (validacion inyeccion)
		$qry = $db->query("SELECT * FROM movimientos WHERE movempresa = '$empresa' AND movprefijo = 'FV' AND movestado = 'ANULADA' AND movnumero = '$factura'");
		if($qry->rowCount() == 0)
		{
			$qry = $db->query("UPDATE solicitudes SET solestado = 'CANCELADO' WHERE solempresa = '$empresa' AND solid = '$id'");
			$qry = $db->query("UPDATE movimientos SET movestado = 'ANULADA' WHERE movempresa = '$empresa' AND movprefijo = 'FV' AND movnumero = '$factura'");
			$qry = $db->query("INSERT INTO movimientos(movempresa, movprefijo, movnumero, movdocumento, movfecha, movvalor, movestado) VALUES ('$empresa', 'AN', '$numero', '$factura', NOW(), " . $row['movvalor'] . " + " . $row['movfinan'] . ", 'FINALIZADO')");
			$qry = $db->query("DELETE FROM carteras WHERE carempresa = '$empresa' AND carfactura = '$factura'");
			$qry = $db->query("DELETE FROM detcarteras WHERE dcaempresa = '$empresa' AND dcafactura = '$factura'");
			$qry = $db->query("SELECT * FROM detsolicitudes INNER JOIN productos ON detproducto = proid WHERE detempresa = '$empresa' AND detsolicitud = '$id'");
			/* NO SE ESTA ELIMINADO EL MOVIMIENTO NI EL DETALLE DEL MOVIMIENTO */
			while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
				$qry2 = $db->query("UPDATE productos SET procantidad = procantidad + " . $row['detcantidad'] . " WHERE proid = '" . $row['detproducto'] . "'");
				$qry2 = $db->query("INSERT INTO detmovimientos (dmoempresa, dmoprefijo, dmonumero, dmoproducto, dmocantidad, dmounitario, dmototal, dmoprecio, dmoptotal, dmoinventario, dmovinventario, dmotinventario) VALUES ('$empresa', 'AN', '$numero', '" . $row['detproducto'] . "', " . $row['detcantidad'] . ", " . $row['procosto'] . ", " . $row['detcantidad'] . " * " . $row['procosto'] . ", " . $row['detprecio'] . ", " . $row['detcantidad'] . " * " . $row['detprecio'] . ", " . $row['procantidad'] . " + " . $row['detcantidad'] . ", " . $row['procosto'] . ", (" . $row['procantidad'] . " + " . $row['detcantidad'] . ")" . " * " . $row['procosto'] . ")");
			}
		}
		
		header('Location:anular.php?empresa=' . $empresa . '&numero=' . $factura . '&ready=1');
		exit();
	}
	
?>
<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a class="current">Facturacion</a>
			</article>
			<article id="contenido">
				<p align="center">
					<iframe src="pdfanular.php?empresa=<?php echo $empresa . '&factura=' . $factura ?>" width="800" height="900"></iframe>
				</p>
				<p class="boton">
					<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href = 'consultar.php'">atras</button>
				</p>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
</body>

</html>