<?php
$r = '../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
require($r.'incluir/phpexcel/Classes/PHPExcel.php');
$empresa = $_GET['empresa'];
$fecha1 = $_GET['fecha1'];
$fecha2 = $_GET['fecha2'];
$contado = 0;
$ctaini = 0;
$credito = 0;
$finan = 0;
$costo = 0;
$filtro = 'empresa='.$empresa.'&fecha1='.$fecha1.'&fecha2='.$fecha2;
if($empresa != ''){
	$sql = "SELECT * FROM empresas WHERE empid = $empresa";
	$row4 = $db->query($sql)->fetch(PDO::FETCH_ASSOC);
}else $sql = "SELECT * FROM empresas";
$qry = $db->query($sql);
while($row = $qry->fetch(PDO::FETCH_ASSOC)){
	$qry2 = $db->query("SELECT * FROM movimientos INNER JOIN solicitudes ON (movempresa = solempresa AND movnumero = solfactura) WHERE movempresa = ".$row['empid']." AND movfecha BETWEEN '$fecha1' AND '$fecha2' AND movprefijo = 'FV'");
	while($row2 = $qry2->fetch(PDO::FETCH_ASSOC)){
		$qry3 = $db->query("SELECT * FROM detmovimientos WHERE dmoempresa = ".$row['empid']." AND dmonumero = '".$row2['movnumero']."'");
		while($row3 = $qry3->fetch(PDO::FETCH_ASSOC))
			$costo = $costo + $row3['dmototal'];
		if($row2['solncuota'] > 0){
			$credito = $credito + $row2['movvalor'];
			$finan = $finan + $row2['movfinan'];
			$ctaini = $ctaini + $row2['solcuota'];
		}else $contado = $contado + $row2['movvalor'];
	}
}
$objPHPExcel = new PHPExcel();
$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN
		)
	)
);
$objPHPExcel->getActiveSheet()->getStyle('A1:B7')->applyFromArray($styleArray);
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:B1');
if($empresa != "")
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'REPORTE DE VENTAS DETALLADAS DESDE '.$fecha1.' AL '.$fecha2);
else
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'REPORTE DE VENTAS DETALLADAS DESDE '.$fecha1.' AL '.$fecha2.' DE LA EMPRESA '.$row4['empnombre']);	
$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A2', 'VR. VENTAS BRUTAS ENTRE EL INTERVALO DE FECHAS:')
    ->setCellValue('A3', 'VR. VENTA A CONTADO ENTRE EL INTERVALO DE FECHAS:')
    ->setCellValue('A4', 'VR. CUOTAS INICIALES ENTRE EL INTERVALO DE FECHAS:')
    ->setCellValue('A5', 'VR. VENTAS A CREDITO ENTRE EL INTERVALO DE FECHAS')
	->setCellValue('A6', 'VR. FINANCIADO ENTRE EL INTERVALO DE FECHAS:')
	->setCellValue('A7', 'VR. COSTOS ENTRE EL INTERVALO DE FECHAS:');

$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('B2', $credito + $contado + $finan)
    ->setCellValue('B3', $contado)
	->setCellValue('B4', $ctaini)
    ->setCellValue('B5', $credito)
    ->setCellValue('B6', $finan)
	->setCellValue('B7', $costo);

// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="resumen.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
?>