<?php
$r = '../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
include($r.'incluir/fpdf/fpdf.php');
$empresa = $_GET['empresa'];
$fecha1 = $_GET['fecha1'];
$fecha2 = $_GET['fecha2'];
$contado = 0;
$ctaini = 0;
$credito = 0;
$finan = 0;
$costo = 0;
$filtro = 'empresa='.$empresa.'&fecha1='.$fecha1.'&fecha2='.$fecha2;
if($empresa != 'TODAS'){

$sql = "SELECT * FROM empresas WHERE empid = $empresa";
$rowemp = $db->query($sql)->fetch(PDO::FETCH_ASSOC);

$sqlgeneral = "SELECT * FROM solicitudes INNER JOIN movimientos ON solfactura = movnumero AND movempresa = solempresa  WHERE movempresa = '".$empresa."' AND movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'FV' AND movestado <> 'ANULADA'"; // qry general ventas bruto


// VENTAS A CONSORCIO MENTOR
$sql1 = "SELECT * FROM solicitudes INNER JOIN movimientos ON solfactura = movnumero AND movempresa = solempresa  WHERE movempresa = '".$empresa."' AND movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'FV' AND movtercero = '900440758' AND movestado <> 'ANULADA'"; 
// COOPERATIVA MULTIACTIVA
$sql2 = "SELECT * FROM solicitudes INNER JOIN movimientos ON solfactura = movnumero AND movempresa = solempresa  WHERE movempresa = '".$empresa."' AND movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'FV' AND movtercero = '900306685' AND movestado <> 'ANULADA'"; 
// COOPERATIVA INTERACTIVA
$sql3 = "SELECT * FROM solicitudes INNER JOIN movimientos ON solfactura = movnumero AND movempresa = solempresa  WHERE movempresa = '".$empresa."' AND movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'FV' AND movtercero = '900299709' AND movestado <> 'ANULADA'";
// IDENCORP
$sql4 = "SELECT * FROM solicitudes INNER JOIN movimientos ON solfactura = movnumero AND movempresa = solempresa  WHERE movempresa = '".$empresa."' AND movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'FV' AND movtercero = '900051528' AND movestado <> 'ANULADA'";
// ESCUELA DE DESARROLLO
$sql5 = "SELECT * FROM solicitudes INNER JOIN movimientos ON solfactura = movnumero AND movempresa = solempresa  WHERE movempresa = '".$empresa."' AND movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'FV' AND movtercero = '900487926' AND movestado <> 'ANULADA'";
// SOPHYA
$sql6 = "SELECT * FROM solicitudes INNER JOIN movimientos ON solfactura = movnumero AND movempresa = solempresa  WHERE movempresa = '".$empresa."' AND movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'FV' AND movtercero = '900813686' AND movestado <> 'ANULADA'";
// SEVEN ACTIVE
$sql7 = "SELECT * FROM solicitudes INNER JOIN movimientos ON solfactura = movnumero AND movempresa = solempresa  WHERE movempresa = '".$empresa."' AND movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'FV' AND movtercero = '900249109' AND movestado <> 'ANULADA'";
// SION
$sql8 = "SELECT * FROM solicitudes INNER JOIN movimientos ON solfactura = movnumero AND movempresa = solempresa  WHERE movempresa = '".$empresa."' AND movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'FV' AND movtercero = '900487915' AND movestado <> 'ANULADA'";


$sqlventascredito = "SELECT * FROM solicitudes INNER JOIN movimientos ON solfactura = movnumero  AND movempresa = solempresa  WHERE movempresa = '".$empresa."' AND movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'FV' AND  solncuota > 1 AND movestado <> 'ANULADA' ;"; // qry ventas a credito
$sqlventascontado = "SELECT * FROM solicitudes INNER JOIN movimientos ON solfactura = movnumero  AND movempresa = solempresa  WHERE movempresa = '".$empresa."' AND movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'FV' AND  solncuota <= 1 AND movestado <> 'ANULADA';"; // qry ventas de contado
$sqlfinanciado = "SELECT * FROM solicitudes INNER JOIN movimientos ON solfactura = movnumero  AND movempresa = solempresa  WHERE movempresa = '".$empresa."' AND movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'FV' AND  movfinan > 0 AND movestado <> 'ANULADA';"; // qry financiado

$sqlcuotasini =  "SELECT * FROM solicitudes INNER JOIN movimientos ON solfactura = movnumero  WHERE movempresa = '".$empresa."' AND movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'FV' AND  solcuota > 0 AND movestado = 'FACTURADO';"; // qry cuotas ini

$sqlcostos = "SELECT * FROM movimientos INNER JOIN solicitudes ON (movempresa = solempresa AND movnumero = solfactura) INNER JOIN detmovimientos ON (dmoempresa = movempresa AND dmonumero = movnumero) WHERE movempresa = ".$empresa." AND movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'FV'"; //QRY COSTOS

$sqlND = "SELECT * FROM  movimientos  WHERE movempresa = '".$empresa."' AND movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'ND' AND movestado <> 'ANULADA'"; // qry ND = notas debito
$sqlNC = "SELECT * FROM  movimientos  WHERE movempresa = '".$empresa."' AND movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'NC' AND movestado <> 'ANULADA'"; // qry NC = notas credito
$sqlDV = "SELECT * FROM  movimientos  WHERE movempresa = '".$empresa."' AND movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'DV' AND movestado <> 'ANULADA'"; // qry DV = devoluciones

}else{

$sqlgeneral = "SELECT * FROM solicitudes INNER JOIN movimientos ON solfactura = movnumero AND movempresa = solempresa  WHERE movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'FV' AND movestado <> 'ANULADA'"; // qry general ventas bruto



// VENTAS A CONSORCIO MENTOR
$sql1 = "SELECT * FROM solicitudes INNER JOIN movimientos ON solfactura = movnumero AND movempresa = solempresa  WHERE  movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'FV' AND movtercero = '900440758' AND movestado <> 'ANULADA'"; 
// COOPERATIVA MULTIACTIVA
$sql2 = "SELECT * FROM solicitudes INNER JOIN movimientos ON solfactura = movnumero AND movempresa = solempresa  WHERE  movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'FV' AND movtercero = '900306685' AND movestado <> 'ANULADA'"; 
// COOPERATIVA INTERACTIVA
$sql3 = "SELECT * FROM solicitudes INNER JOIN movimientos ON solfactura = movnumero AND movempresa = solempresa  WHERE  movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'FV' AND movtercero = '900299709' AND movestado <> 'ANULADA'";
// IDENCORP
$sql4 = "SELECT * FROM solicitudes INNER JOIN movimientos ON solfactura = movnumero AND movempresa = solempresa  WHERE  movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'FV' AND movtercero = '900051528' AND movestado <> 'ANULADA'";
// ESCUELA DE DESARROLLO
$sql5 = "SELECT * FROM solicitudes INNER JOIN movimientos ON solfactura = movnumero AND movempresa = solempresa  WHERE  movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'FV' AND movtercero = '900487926' AND movestado <> 'ANULADA'";
// SOPHYA
$sql6 = "SELECT * FROM solicitudes INNER JOIN movimientos ON solfactura = movnumero AND movempresa = solempresa  WHERE  movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'FV' AND movtercero = '900813686' AND movestado <> 'ANULADA'";
// SEVEN ACTIVE
$sql7 = "SELECT * FROM solicitudes INNER JOIN movimientos ON solfactura = movnumero AND movempresa = solempresa  WHERE  movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'FV' AND movtercero = '900249109' AND movestado <> 'ANULADA'";
// SION
$sql8 = "SELECT * FROM solicitudes INNER JOIN movimientos ON solfactura = movnumero AND movempresa = solempresa  WHERE  movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'FV' AND movtercero = '900487915' AND movestado <> 'ANULADA'";



$sqlventascredito = "SELECT * FROM solicitudes INNER JOIN movimientos ON solfactura = movnumero AND movempresa = solempresa   WHERE movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'FV' AND  solncuota > 1 AND movestado <> 'ANULADA' ;"; // qry ventas a credito
$sqlventascontado = "SELECT * FROM solicitudes INNER JOIN movimientos ON solfactura = movnumero AND movempresa = solempresa   WHERE movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'FV' AND  solncuota <= 1 AND movestado <> 'ANULADA';"; // qry ventas de contado
$sqlfinanciado = "SELECT * FROM solicitudes INNER JOIN movimientos ON solfactura = movnumero AND movempresa = solempresa   WHERE movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'FV' AND  movfinan > 0 AND movestado <> 'ANULADA';"; // qry financiado

$sqlcuotasini =  "SELECT * FROM solicitudes INNER JOIN movimientos ON solfactura = movnumero AND movempresa = solempresa   WHERE movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'FV' AND  solcuota > 0 AND movestado = 'FACTURADO' AND movestado <> 'ANULADA'"; // qry cuotas ini

$sqlcostos = "SELECT * FROM movimientos INNER JOIN detmovimientos ON (dmoempresa = movempresa AND dmonumero = movnumero) WHERE movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'FV'"; //QRY COSTOS

$sqlND = "SELECT * FROM  movimientos  WHERE movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'ND' AND movestado <> 'ANULADA'"; // qry ND = notas debito
$sqlNC = "SELECT * FROM  movimientos  WHERE movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'NC' AND movestado <> 'ANULADA'"; // qry NC = notas credito
$sqlDV = "SELECT * FROM  movimientos  WHERE movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'DV' AND movestado <> 'ANULADA'"; // qry DV = devoluciones

}

$qrygeneral = $db->query($sqlgeneral); // consulta general ventas bruto
while($rowgeneral = $qrygeneral->fetch(PDO::FETCH_ASSOC)) {
$venbrutageneral = $venbrutageneral + $rowgeneral['movvalor'];
}

// ----------------------
$qry1 = $db->query($sql1); 
while($row1 = $qry1->fetch(PDO::FETCH_ASSOC)) {
$venbruta1 = $venbruta1 + $row1['movvalor'];
}

$qry2 = $db->query($sql2); 
while($row2 = $qry1->fetch(PDO::FETCH_ASSOC)) {
$venbruta2 = $venbruta2 + $row2['movvalor'];
}

$qry3 = $db->query($sql3); 
while($row3 = $qry3->fetch(PDO::FETCH_ASSOC)) {
$venbruta3 = $venbruta3 + $row3['movvalor'];
}

$qry4 = $db->query($sql4); 
while($row4 = $qry4->fetch(PDO::FETCH_ASSOC)) {
$venbruta4 = $venbruta4 + $row4['movvalor'];
}

$qry5 = $db->query($sql5); 
while($row5 = $qry5->fetch(PDO::FETCH_ASSOC)) {
$venbruta5 = $venbruta5 + $row5['movvalor'];
}

$qry6 = $db->query($sql6); 
while($row6 = $qry6->fetch(PDO::FETCH_ASSOC)) {
$venbruta6 = $venbruta6 + $row6['movvalor'];
}

$qry7 = $db->query($sql7); 
while($row7 = $qry7->fetch(PDO::FETCH_ASSOC)) {
$venbruta7 = $venbruta7 + $row7['movvalor'];
}

$qry8 = $db->query($sql8); 
while($row8 = $qry8->fetch(PDO::FETCH_ASSOC)) {
$venbruta8 = $venbruta8 + $row8['movvalor'];
}
// **********************

$qryventascredito = $db->query($sqlventascredito); // consulta ventas a credito 
while($rowventascredito = $qryventascredito->fetch(PDO::FETCH_ASSOC)) { 
$credito = $credito + $rowventascredito['movvalor'];	
}

$qryventascontado = $db->query($sqlventascontado); // consulta ventas de contado
while($rowventascontado = $qryventascontado->fetch(PDO::FETCH_ASSOC)) { 
$contado = $contado + $rowventascontado['movvalor'];	
}

$qryfinanciado = $db->query($sqlfinanciado); // consulta financiado
while($rowfinanciado = $qryfinanciado->fetch(PDO::FETCH_ASSOC)) { 
$finantotal = $finantotal + $rowfinanciado['movfinan'];
}

$qrycuotasini = $db->query($sqlcuotasini); // consulta cuotas iniciales
while($rowcuotasini = $qrycuotasini->fetch(PDO::FETCH_ASSOC)) { 
$ctaini = $ctaini + $rowcuotasini['solcuota'];
}

$qrycosto = $db->query($sqlcostos); // consulta para NOTAS DEBITO
while($rowcosto = $qrycosto->fetch(PDO::FETCH_ASSOC)) { 
$costo = $costo  + $rowcosto['dmototal'];
}


$qryND = $db->query($sqlND); // consulta para NOTAS DEBITO
while($rowND = $qryND->fetch(PDO::FETCH_ASSOC)) { 
$ndebito = $ndebito  + $rowND['movvalor'];
}

$qryNC = $db->query($sqlNC); // consulta para NOTAS CREDITO
while($rowNC = $qryNC->fetch(PDO::FETCH_ASSOC)) { 
$ncredito = $ncredito + $rowNC['movvalor'];
}

$qryDV = $db->query($sqlDV); // consulta para DEVOLUCIONES
while($rowDV = $qryDV->fetch(PDO::FETCH_ASSOC)) { 
$devoluciones = $devoluciones  + $rowDV['movvalor'];
}



$pdf = new FPDF();
$pdf->AddFont('LucidaConsole','','lucon.php');
$pdf->AliasNbPages();
$pdf->AddPage('P');
$pdf->SetFont('LucidaConsole','',8);
$pdf->Cell(0,5,date('Y/m/d'),0,1);
$pdf->SetFont('LucidaConsole','',11);
$pdf->Cell(0,5,'---------------------------------------------------------------------------------',0,1,'C');
$pdf->Cell(0,5,'REPORTE DE VENTAS DETALLADAS DESDE '.$fecha1.' AL '.$fecha2,0,1,'C');
if($empresa != 'TODAS'){ $pdf->Cell(0,5,'DE LA EMPRESA '.$rowemp['empnombre'],0,1,'C'); }else{ $pdf->Cell(0,5,'DE TODAS LAS EMPRESAS',0,1,'C');}
$pdf->Cell(0,5,'---------------------------------------------------------------------------------',0,1,'C');
$pdf->Ln(5);
$pdf->SetFillColor(220,220,220);
$pdf->SetX(25);
$pdf->Cell(120,5,'VR. VENTAS BRUTAS ENTRE EL INTERVALO DE FECHAS: ',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(40,5,number_format($venbrutageneral,0,',','.'),1,1,'R',true);

$pdf->SetFillColor(220,220,220);
$pdf->SetX(25);
$pdf->Cell(120,5,'VR. VENTAS A CONSORCIO MENTOR: ',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(40,5,number_format($venbruta1,0,',','.'),1,1,'R',true);
$pdf->SetFillColor(220,220,220);
$pdf->SetX(25);
$pdf->Cell(120,5,'VR. VENTAS A COOPERATIVA MULTIACTIVA: ',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(40,5,number_format($venbruta2,0,',','.'),1,1,'R',true);
$pdf->SetFillColor(220,220,220);
$pdf->SetX(25);
$pdf->Cell(120,5,'VR. VENTAS A COOPERATIVA INTERACTIVA: ',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(40,5,number_format($venbruta3,0,',','.'),1,1,'R',true);
$pdf->SetFillColor(220,220,220);
$pdf->SetX(25);
$pdf->Cell(120,5,'VR. VENTAS A IDENCORP: ',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(40,5,number_format($venbruta4,0,',','.'),1,1,'R',true);
$pdf->SetFillColor(220,220,220);
$pdf->SetX(25);
$pdf->Cell(120,5,'VR. VENTAS A ESCUELA DE DESARROLLO: ',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(40,5,number_format($venbruta5,0,',','.'),1,1,'R',true);
$pdf->SetFillColor(220,220,220);
$pdf->SetX(25);
$pdf->Cell(120,5,'VR. VENTAS A SOPHYA: ',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(40,5,number_format($venbruta6,0,',','.'),1,1,'R',true);
$pdf->SetFillColor(220,220,220);
$pdf->SetX(25);
$pdf->Cell(120,5,'VR. VENTAS A SEVEN ACTIVE: ',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(40,5,number_format($venbruta7,0,',','.'),1,1,'R',true);
$pdf->SetFillColor(220,220,220);
$pdf->SetX(25);
$pdf->Cell(120,5,'VR. VENTAS A SION: ',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(40,5,number_format($venbruta8,0,',','.'),1,1,'R',true);

$pdf->SetFillColor(220,220,220);
$pdf->SetX(25);
$pdf->Cell(120,5,'VR. VENTA A CONTADO ENTRE EL INTERVALO DE FECHAS: ',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(40,5,number_format($contado,0,',','.'),1,1,'R',true);
$pdf->SetFillColor(220,220,220);
$pdf->SetX(25);
$pdf->Cell(120,5,'VR. CUOTAS INICIALES ENTRE EL INTERVALO DE FECHAS: ',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(40,5,number_format($ctaini,0,',','.'),1,1,'R',true);
$pdf->SetFillColor(220,220,220);
$pdf->SetX(25);
$pdf->Cell(120,5,'VR. VENTAS A CREDITO ENTRE EL INTERVALO DE FECHAS: ',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(40,5,number_format($credito,0,',','.'),1,1,'R',true);
$pdf->SetFillColor(220,220,220);
$pdf->SetX(25);
$pdf->Cell(120,5,'VR. FINANCIADO ENTRE EL INTERVALO DE FECHAS: ',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(40,5,number_format($finantotal,0,',','.'),1,1,'R',true);


$pdf->SetFillColor(220,220,220);
$pdf->SetX(25);
$pdf->Cell(120,5,'VR. COSTOS ENTRE EL INTERVALO DE FECHAS: ',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(40,5,number_format($costo,0,',','.'),1,1,'R',true);


$pdf->SetFillColor(220,220,220);
$pdf->SetX(25);
$pdf->Cell(120,5,'VR. NOTAS DEBITO ENTRE EL INTERVALO DE FECHAS: ',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(40,5,number_format($ndebito,0,',','.'),1,1,'R',true);


$pdf->SetFillColor(220,220,220);
$pdf->SetX(25);
$pdf->Cell(120,5,'VR. NOTAS CREDITO ENTRE EL INTERVALO DE FECHAS: ',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(40,5,number_format($ncredito,0,',','.'),1,1,'R',true);



$pdf->SetFillColor(220,220,220);
$pdf->SetX(25);
$pdf->Cell(120,5,'VR. DEVOLUCIONES ENTRE EL INTERVALO DE FECHAS: ',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(40,5,number_format($devoluciones,0,',','.'),1,1,'R',true);

$pdf->Ln(10);
$pdf->Cell(0,5,'-----------------------------------------------------------------------------------',0,1,'C');
$pdf->Output('REPORTE DE VENTAS DETALLADAS DESDE '.$fecha1.' AL '.$fecha2.'.pdf','d');
?>