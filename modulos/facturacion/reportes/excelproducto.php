<?php
$r = '../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
require($r.'incluir/phpexcel/Classes/PHPExcel.php');
$producto = $_GET['producto'];
$fecha1 = $_GET['fecha1'];
$fecha2 = $_GET['fecha2'];
$filtro = 'producto='.$producto.'&fecha1='.$fecha1.'&fecha2='.$fecha2;
if($producto != '') 
	$sql = "SELECT * FROM productos WHERE proid = $producto";
else 
	$sql = "SELECT * FROM productos";
$qry = $db->query($sql);
$objPHPExcel = new PHPExcel();
$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN
		)
	)
);
$objPHPExcel->getActiveSheet()->getStyle('A1:E2')->applyFromArray($styleArray);
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:E1')
			->setCellValue('A1', 'REPORTE DE PRODUCTOS VENDIDOS ENTRE '.$fecha1.' AL '.$fecha2);
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A2', 'CODIGO')
            ->setCellValue('B2', 'NOMBRE')
            ->setCellValue('C2', 'CANTIDAD')
            ->setCellValue('D2', 'COSTOS')
			->setCellValue('E2', 'VENTAS');	
$i = 3;
while($row = $qry->fetch(PDO::FETCH_ASSOC)){
	$cantidad = 0;
	$costo = 0;
	$venta = 0;
	$qry2 = $db->query("SELECT * FROM movimientos INNER JOIN detmovimientos ON (movempresa = dmoempresa AND movnumero = dmonumero) WHERE movprefijo = 'FV' AND dmoproducto = '".$row['proid']."' AND movfecha BETWEEN '$fecha1' AND '$fecha2'");
	while($row2 = $qry2->fetch(PDO::FETCH_ASSOC)){
		if($row2['dmoprefijo' ] == 'AN')
		{
			$cantidad -= $row2['dmocantidad'];
		}
		else
		{
			$cantidad += $row2['dmocantidad'];
			$costo += $row2['dmototal'];
			$venta += $row2['dmoptotal'];
		}
	}
	$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':E'.$i)->applyFromArray($styleArray);
	$objPHPExcel->getActiveSheet()->getStyle('A'.$i)->getNumberFormat()->setFormatCode('000000');
	$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('A'.$i, $row['proid'])
    	->setCellValue('B'.$i, $row['pronombre'])
		->setCellValue('C'.$i, $cantidad)
    	->setCellValue('D'.$i, $costo)
    	->setCellValue('E'.$i, $venta);
	$i++;
}
// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="producto.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
?>