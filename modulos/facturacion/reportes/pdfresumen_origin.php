<?php
$r = '../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
include($r.'incluir/fpdf/fpdf.php');
$empresa = $_GET['empresa'];
$fecha1 = $_GET['fecha1'];
$fecha2 = $_GET['fecha2'];
$contado = 0;
$ctaini = 0;
$credito = 0;
$finan = 0;
$costo = 0;
$filtro = 'empresa='.$empresa.'&fecha1='.$fecha1.'&fecha2='.$fecha2;
if($empresa != ''){
	$sql = "SELECT * FROM empresas WHERE empid = $empresa";
	$row4 = $db->query($sql)->fetch(PDO::FETCH_ASSOC);
}else $sql = "SELECT * FROM empresas";
$qry = $db->query($sql);
while($row = $qry->fetch(PDO::FETCH_ASSOC)){
	$qry2 = $db->query("SELECT * FROM movimientos INNER JOIN solicitudes ON (movempresa = solempresa AND movnumero = solfactura) WHERE movempresa = ".$row['empid']." AND movfecha BETWEEN '$fecha1' AND '$fecha2' AND movprefijo = 'FV'");
	while($row2 = $qry2->fetch(PDO::FETCH_ASSOC)){
		$qry3 = $db->query("SELECT * FROM detmovimientos WHERE dmoempresa = ".$row['empid']." AND dmonumero = '".$row2['movnumero']."'");
		while($row3 = $qry3->fetch(PDO::FETCH_ASSOC))
			$costo = $costo + $row3['dmototal'];
		if($row2['solncuota'] > 0){
			$credito = $credito + $row2['movvalor'];
			$finan = $finan + $row2['movfinan'];
			$ctaini = $ctaini + $row2['solcuota'];
		}else $contado = $contado + $row2['movvalor'];
	}
}
$pdf = new FPDF();
$pdf->AddFont('LucidaConsole','','lucon.php');
$pdf->AliasNbPages();
$pdf->AddPage('P');
$pdf->SetFont('LucidaConsole','',8);
$pdf->Cell(0,5,date('Y/m/d H:i:s'),0,1);
$pdf->SetFont('LucidaConsole','',11);
$pdf->Cell(0,5,'---------------------------------------------------------------------------------',0,1,'C');
$pdf->Cell(0,5,'REPORTE DE VENTAS DETALLADAS DESDE '.$fecha1.' AL '.$fecha2,0,1,'C');
if($empresa != '') $pdf->Cell(0,5,'DE LA EMPRESA '.$row4['empnombre'],0,1,'C');
$pdf->Cell(0,5,'---------------------------------------------------------------------------------',0,1,'C');
$pdf->Ln(5);
$pdf->SetFillColor(220,220,220);
$pdf->SetX(25);
$pdf->Cell(120,5,'VR. VENTAS BRUTAS ENTRE EL INTERVALO DE FECHAS: ',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(40,5,number_format($credito + $contado + $finan,0,',','.'),1,1,'R',true);
$pdf->SetFillColor(220,220,220);
$pdf->SetX(25);
$pdf->Cell(120,5,'VR. VENTA A CONTADO ENTRE EL INTERVALO DE FECHAS: ',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(40,5,number_format($contado,0,',','.'),1,1,'R',true);
$pdf->SetFillColor(220,220,220);
$pdf->SetX(25);
$pdf->Cell(120,5,'VR. CUOTAS INICIALES ENTRE EL INTERVALO DE FECHAS: ',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(40,5,number_format($ctaini,0,',','.'),1,1,'R',true);
$pdf->SetFillColor(220,220,220);
$pdf->SetX(25);
$pdf->Cell(120,5,'VR. VENTAS A CREDITO ENTRE EL INTERVALO DE FECHAS: ',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(40,5,number_format($credito,0,',','.'),1,1,'R',true);
$pdf->SetFillColor(220,220,220);
$pdf->SetX(25);
$pdf->Cell(120,5,'VR. FINANCIADO ENTRE EL INTERVALO DE FECHAS: ',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(40,5,number_format($finan,0,',','.'),1,1,'R',true);
$pdf->SetFillColor(220,220,220);
$pdf->SetX(25);
$pdf->Cell(120,5,'VR. COSTOS ENTRE EL INTERVALO DE FECHAS: ',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(40,5,number_format($costo,0,',','.'),1,0,'R',true);
$pdf->Ln(10);
$pdf->Cell(0,5,'-----------------------------------------------------------------------------------',0,1,'C');
$pdf->Output('resumen.pdf','d');
?>