<?php
$r = '../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
include($r.'incluir/fpdf/fpdf.php');
$empresa = $_GET['empresa'];
$fecha1 = $_GET['fecha1'];
$fecha2 = $_GET['fecha2'];
if($empresa != '') 
	$sql = "SELECT * FROM (movimientos INNER JOIN empresas ON movempresa = empid) INNER JOIN solicitudes ON (movempresa = solempresa AND movnumero = solfactura) WHERE movempresa = '$empresa' AND movprefijo = 'FV' AND movfecha BETWEEN '$fecha1' AND '$fecha2' ORDER BY movempresa, movnumero";
else 
	$sql = "SELECT * FROM (movimientos INNER JOIN empresas ON movempresa = empid) INNER JOIN solicitudes ON (movempresa = solempresa AND movnumero = solfactura) WHERE movprefijo = 'FV' AND movfecha BETWEEN '$fecha1' AND '$fecha2' ORDER BY movempresa, movnumero";
$qry = $db->query($sql);
$num = $qry->rowCount();
class PDF extends FPDF{
	function Header(){
		global $db;
		global $empresa;
		global $fecha1;
		global $fecha2;
		if($empresa != "") $row2 = $db->query("SELECT * FROM empresas WHERE empid = '$empresa'")->fetch(PDO::FETCH_ASSOC);
    	$this->SetFont('LucidaConsole','',8);
		$this->Cell(0,5,date('Y/m/d H:i:s'),0,1);
		$this->SetFont('LucidaConsole','',11);
		$this->Cell(0,5,'-----------------------------------------------------------------------------------------------------------------------',0,1,'C');
		$this->Cell(0,5,'RESUMEN DE VENTAS DETALLADA DESDE '.$fecha1.' AL '.$fecha2,0,1,'C');
		if($empresa != "") $this->Cell(0,5,'DE LA EMPRESA '.$row2['empnombre'],0,1,'C');
		$this->Cell(0,5,'-----------------------------------------------------------------------------------------------------------------------',0,1,'C');
    	$this->Ln(5);
	}

	function Footer(){
    	$this->SetY(-15);
    	$this->SetFont('LucidaConsole','',11);
		$this->Cell(0,5,'-----------------------------------------------------------------------------------------------------------------------',0,1,'C');
		$this->SetFont('LucidaConsole','',8);
    	$this->Cell(0,5,'Pagina '.$this->PageNo().'/{nb}',0,0,'R');
	}
}
$pdf = new PDF('L');
$pdf->AddFont('LucidaConsole','','lucon.php');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('LucidaConsole','',10);
$pdf->SetFillColor(220,220,220);
$pdf->Cell(75,5,'EMPRESA',1,0,'C',true);
$pdf->Cell(30,5,'#FACTURA',1,0,'C',true);
$pdf->Cell(30,5,'FECHA',1,0,'C',true);
$pdf->Cell(35,5,'CONTADO',1,0,'C',true);
$pdf->Cell(35,5,'CREDITO',1,0,'C',true);
$pdf->Cell(35,5,'FINANCIACION',1,0,'C',true);
$pdf->Cell(35,5,'CUOTA INICIAL',1,1,'C',true);
$credito = 0;
$contado = 0;
$ctaini = 0;
$finantotal = 0;
$i = 1;
$pdf->SetFont('LucidaConsole','',8);
while($row = $qry->fetch(PDO::FETCH_ASSOC)){
	// Es para especificar el color de la fila en caso de ser par
	if($i%2 == 0) $x = 235;
	else $x = 250;
	$pdf->SetFillColor($x,$x,$x);
	if($row['movestado'] == 'ANULADA'){
		$pdf->SetFillColor(150,150,150);
	}
	
	$pdf->Cell(75,5,$row['empnombre'],1,0,'',true);
	$pdf->Cell(30,5,$row['movnumero'],1,0,'C',true);
	$pdf->Cell(30,5,$row['movfecha'],1,0,'C',true);
	if($row['solncuota'] == 0){
		$pdf->Cell(35,5,number_format($row['movvalor'],2),1,0,'R',true);
		$pdf->Cell(35,5,'',1,0,'',true);
	}else{
		$pdf->Cell(35,5,'',1,0,'R',true);
		$pdf->Cell(35,5,number_format($row['movvalor'],2),1,0,'R',true);
	}
	$pdf->Cell(35,5,number_format($row['movfinan'],2),1,0,'R',true);
	$pdf->Cell(35,5,number_format($row['solcuota'],2),1,1,'R',true);
	
	if($row['movestado'] != 'ANULADA')
	{
		if($row['solncuota'] == 0)
			$contado = $contado + $row['movvalor'];
		else
			$credito = $credito + $row['movvalor'];
		$finantotal = $finantotal + $row['movfinan'];
		$ctaini = $ctaini + $row['solcuota'];
	}

	$i++;
}
$pdf->SetFillColor(255,255,255);
$pdf->Cell(75,5,'',0,0,'',true);
$pdf->Cell(30,5,'',0,0,'',true);
$pdf->SetFillColor(255,255,255);
$pdf->Cell(30,5,'',0,0,'',true);
$pdf->SetFillColor(220,220,220);
$pdf->Cell(35,5,number_format($contado,2),1,0,'R',true);
$pdf->Cell(35,5,number_format($credito,2),1,0,'R',true);
$pdf->Cell(35,5,number_format($finantotal,2),1,0,'R',true);
$pdf->Cell(35,5,number_format($ctaini,2),1,1,'R',true);
$pdf->SetFillColor(255,255,255);
$pdf->Cell(135,5,'',0,0,'',true);
$pdf->SetFillColor(220,220,220);
$pdf->Cell(140,5,number_format($contado + $credito + $finantotal,2),1,0,'R',true);
$pdf->Output('ventas.pdf','d');
?>