<?php
	$r = '../../../';
	require($r.'incluir/session.php');
	require($r.'incluir/connection.php');
	require($r.'incluir/phpexcel/Classes/PHPExcel.php');

	$empresa = $numero = $cliente = $estado = $fecha1 = $fecha2 = "";

	if(isset($_POST['consultar'])){
		$empresa = $_POST['empresa'];
		$numero = $_POST['numero'];
		$cliente = $_POST['cliente'];
		$estado = $_POST['estado'];
		$fecha1 = $_POST['fecha1'];
		$fecha2 = $_POST['fecha2'];
	} elseif ($_GET['empresa']) {
		$empresa = $_GET['empresa'];
		$numero = $_GET['numero'];
		$cliente = $_GET['cliente'];
		$estado = $_GET['estado'];
		$fecha1 = $_GET['fecha1'];
		$fecha2 = $_GET['fecha2'];
	}
	$filtro = 'empresa='.$empresa.'&numero='.$numero.'&cliente='.$cliente.'&estado='.$estado.'&fecha1='.$fecha1.'&fecha2='.$fecha2;
	$con = 'SELECT * FROM ((movimientos INNER JOIN empresas ON movempresa = empid) INNER JOIN clientes ON movtercero = cliid) LEFT JOIN solicitudes ON (movempresa = solempresa AND movnumero = solfactura)';
	$ord = 'ORDER BY movfecha DESC, movnumero DESC';


			/* Los parametros de la consulta sql se genera dinamicamente 
			en base a los datos recibidos para delimitar los resultados */
			$parameters = [];
			array_push($parameters, "movprefijo = 'FV'");
			if ($empresa != "")
				array_push($parameters, "movempresa LIKE '%$empresa%'");
			if ($numero != "")
				array_push($parameters, "movnumero LIKE '%$numero%'");
			if ($cliente != "")
				array_push($parameters, "movtercero LIKE '%$cliente%'");
			if ($estado != "")
				array_push($parameters, "movestado = '$estado'");
			if ($fecha1 != "")
				array_push($parameters, "movfecha BETWEEN '$fecha1' AND '$fecha2'");
		
			// Consulta base
			$sql = $con;
			foreach ($parameters as $index => $parameter) {
				// Se agregan los parametros del WHERE
				if ($index == 0)
					$sql .= " WHERE " . $parameter;
				else
					$sql .= " AND " . $parameter;
			}
			// Se completa la consulta
			$sql .= $ord;
		

	$qry = $db->query($sql);


	$titulo = "LISTADO DE LAS FACTURAS DESDE $fecha1 HASTA $fecha2";


	$objPHPExcel = new PHPExcel();
	$styleArray = array(
		'borders' => array(
			'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN
			)
		)
	);
	$objPHPExcel->getActiveSheet()->getStyle('A1:AI2')->applyFromArray($styleArray);
	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:AI1')
				->setCellValue('A1', $titulo);
	$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A2', 'FECHA FACTURACION')
				->setCellValue('B2', 'NUM FACTURA')
				->setCellValue('C2', 'ESTADO DE FACTURA')	
				->setCellValue('D2', 'IDENTIFICACION CLIENTE')	
				->setCellValue('E2', 'NOMBRES DEL CLIENTE')	
				->setCellValue('F2', 'TEL RESIDENCIA')	
				->setCellValue('G2', 'TEL COMERCIO')	
				->setCellValue('H2', 'CELULAR')	
				->setCellValue('I2', 'TEL ADICIONAL')	
				->setCellValue('J2', 'EMAIL UNO')
				->setCellValue('K2', 'DIRECCION')	
				->setCellValue('L2', 'CIUDAD')	
				->setCellValue('M2', 'DEPARTAMENTO')

				->setCellValue('N2', 'REFERENCIA 1')	
				->setCellValue('O2', 'REF1 TELEFONO')	
				->setCellValue('P2', 'REF1 CELULAR')
				
				->setCellValue('Q2', 'REFERENCIA 2')	
				->setCellValue('R2', 'REF2 TELEFONO')	
				->setCellValue('S2', 'REF2 CELULAR')
				
				->setCellValue('T2', 'REFERENCIA 3')	
				->setCellValue('U2', 'REF3 TELEFONO')	
				->setCellValue('V2', 'REF3 CELULAR')

				->setCellValue('W2', 'NIT EMPRESA DEL CLIENTE')	
				->setCellValue('X2', 'NOMBRE EMPRESA CLIENTE')
				->setCellValue('Y2', 'CARGO')	
				->setCellValue('Z2', 'ANTIGUEDAD')	
				->setCellValue('AA2', 'SALARIO')	
				->setCellValue('AB2', 'ESTADO CIVIL')	
				->setCellValue('AC2', 'NUMERO DE HIJOS')
				->setCellValue('AD2', 'ASESOR')	
				->setCellValue('AE2', 'GERENTE ASESOR')	
				->setCellValue('AF2', 'RELACIONISTA')	
				->setCellValue('AG2', 'GERENTE CONTAC CENTER')	
				->setCellValue('AH2', 'VALOR FACTURADO')
				->setCellValue('AI2', 'VALOR BASE COMISIONABLE');
	$i = 3;
	while($row = $qry->fetch(PDO::FETCH_ASSOC)){
		
		$departamento = "-";
		$ciudad = "-";
		$relacionista = "-";
		$asesor = "-";
		$jeferelacionista = "-"; 
		$jefeasesor = "-";
		if (!empty($row['clidepresidencia'])){
		$qrydepar = $db->query("SELECT * FROM departamentos WHERE depid = '".$row['clidepresidencia']."';");
		$rowdepar = $qrydepar->fetch(PDO::FETCH_ASSOC);
		$numdepar = $qrydepar->rowCount();
			if ($numdepar == 1){
			$departamento = $rowdepar['depnombre'];
			$qryciu = $db->query("SELECT * FROM ciudades WHERE ciudepto = '".$rowdepar['depid']."' AND ciuid = '".$row['cliciuresidencia']."' ;");
			$rowciu = $qryciu->fetch(PDO::FETCH_ASSOC);
			$numciu = $qryciu->rowCount();
				if ($numciu == 1){
				$ciudad = $rowciu['ciunombre'];
				} 
			} 
		} 

		if (!empty($row['solrelacionista'])){
			$qryrela = $db->query("SELECT * FROM usuarios WHERE usuid = '".$row['solrelacionista']."';");
			$rowrela = $qryrela->fetch(PDO::FETCH_ASSOC);
			$numrela = $qryrela->rowCount();
			if ($numrela == 1){
			$relacionista = $rowrela['usunombre'];
				if (!empty($rowrela['usujefecallcenter'])){
					$qryjeferela = $db->query("SELECT * FROM usuarios WHERE usuid = '".$rowrela['usujefecallcenter']."';");
					$rowjeferela = $qryjeferela->fetch(PDO::FETCH_ASSOC);
					$numjeferela = $qryjeferela->rowCount();
					if ($numjeferela == 1){
						$jeferelacionista = $rowjeferela['usunombre'];
					}
				} 
			}
		}
		
		
		
		if (!empty($row['solasesor'])){
			$qryase = $db->query("SELECT * FROM usuarios WHERE usuid = '".$row['solasesor']."';");
			$rowase = $qryase->fetch(PDO::FETCH_ASSOC);
			$numase = $qryase->rowCount();
			if ($numase == 1){
			$asesor = $rowase['usunombre'];
				if (!empty($rowase['usujefeasesor'])){
					$qryjefease = $db->query("SELECT * FROM usuarios WHERE usuid = '".$rowase['usujefeasesor']."';");
					$rowjefease = $qryjefease->fetch(PDO::FETCH_ASSOC);
					$numjefease = $qryjefease->rowCount();
					if ($numjefease == 1){
						$jefeasesor = $rowjefease['usunombre'];
					}
				}
			} 
		}
		
		$nombre_cli = $row['clinombre'].' '.$row['clinom2'].' '.$row['cliape1'].' '.$row['cliape2'];
		
		$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':AI'.$i)->applyFromArray($styleArray);
		$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A'.$i, $row['movfecha'])
			->setCellValue('B'.$i, $row['movnumero'])
			->setCellValue('C'.$i, $row['movestado'])
			->setCellValue('D'.$i, $row['cliid'])
			->setCellValue('E'.$i, $nombre_cli)
			->setCellValue('F'.$i, $row['clitelresidencia'])
			->setCellValue('G'.$i, $row['clitelcomercio'])
			->setCellValue('H'.$i, $row['clicelular'])
			->setCellValue('I'.$i, $row['cliteladicional'])
			->setCellValue('J'.$i, $row['cliemail'])
			->setCellValue('K'.$i, $row['clidirresidencia'])
			->setCellValue('L'.$i, $ciudad)
			->setCellValue('M'.$i, $departamento)
			->setCellValue('N'.$i, $row['clirefnombre1'])
			->setCellValue('O'.$i, $row['clireftelefono1'])
			->setCellValue('P'.$i, $row['clirefcelular1'])
			->setCellValue('Q'.$i, $row['clirefnombre2'])
			->setCellValue('R'.$i, $row['clireftelefono2'])
			->setCellValue('S'.$i, $row['clirefcelular2'])
			->setCellValue('T'.$i, $row['clirefnombre3'])
			->setCellValue('U'.$i, $row['clireftelefono3'])
			->setCellValue('V'.$i, $row['clirefcelular3'])
			->setCellValue('W'.$i, $row['clinitempresa'])
			->setCellValue('X'.$i, $row['cliempresa'])
			->setCellValue('Y'.$i, $row['clicargo'])
			->setCellValue('Z'.$i, $row['cliantiguedad'])
			->setCellValue('AA'.$i, $row['clisalario'])
			->setCellValue('AB'.$i, $row['cliestado'])
			->setCellValue('AC'.$i, $row['clihijos'])
			->setCellValue('AD'.$i, $asesor)
			->setCellValue('AE'.$i, $jefeasesor)
			->setCellValue('AF'.$i, $relacionista)
			->setCellValue('AG'.$i, $jeferelacionista)
			->setCellValue('AH'.$i, $row['soltotal'])
			->setCellValue('AI'.$i, $row['solbase']);
		$i++;
	}
	// Redirect output to a client’s web browser (Excel2007)
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="Listado Facturas.xlsx"');
	header('Cache-Control: max-age=0');
	// If you're serving to IE 9, then the following may be needed
	header('Cache-Control: max-age=1');

	// If you're serving to IE over SSL, then the following may be needed
	header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
	header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header ('Pragma: public'); // HTTP/1.0

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');
