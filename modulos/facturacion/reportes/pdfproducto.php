<?php
$r = '../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
include($r.'incluir/fpdf/fpdf.php');
$producto = $_GET['producto'];
$fecha1 = $_GET['fecha1'];
$fecha2 = $_GET['fecha2'];
$filtro = 'producto='.$producto.'&fecha1='.$fecha1.'&fecha2='.$fecha2;
if($producto != '') 
	$sql = "SELECT * FROM productos WHERE proid = $producto";
else 
	$sql = "SELECT * FROM productos";
$qry = $db->query($sql);
class PDF extends FPDF{
	function Header(){
		global $fecha1;
		global $fecha2;
    	$this->SetFont('LucidaConsole','',8);
		$this->Cell(50,5,date('Y/m/d H:i:s'),0,1);
		$this->SetFont('LucidaConsole','',11);
		$this->Cell(0,5,'----------------------------------------------------------------------------------',0,1,'C');
    	$this->Cell(0,5,'REPORTE DE PRODUCTOS VENDIDOS ENTRE '.$fecha1.' AL '.$fecha2,0,1,'C');
		$this->Cell(0,5,'----------------------------------------------------------------------------------',0,1,'C');
    	$this->Ln(5);
	}

	function Footer(){
    	$this->SetY(-15);
    	$this->SetFont('LucidaConsole','',11);
		$this->Cell(0,5,'----------------------------------------------------------------------------------',0,1,'C');
		$this->SetFont('LucidaConsole','',8);
    	$this->Cell(0,5,'Pagina '.$this->PageNo().'/{nb}',0,0,'R');
	}
}

$pdf = new PDF();
$pdf->AddFont('LucidaConsole','','lucon.php');
$pdf->AliasNbPages();
$pdf->AddPage('P');
$pdf->SetFont('LucidaConsole','',10);
$pdf->SetFillColor(220,220,220);
$pdf->Cell(25,5,'CODIGO',0,0,'C',true);
$pdf->Cell(85,5,'NOMBRE',0,0,'L',true);
$pdf->Cell(20,5,'CANTIDAD',0,0,'C',true);
$pdf->Cell(30,5,'COSTO',0,0,'C',true);
$pdf->Cell(30,5,'VENTA',0,1,'C',true);
$i = 1;
while($row = $qry->fetch(PDO::FETCH_ASSOC)){
	$cantidad = 0;
	$costo = 0;
	$venta = 0;
	$qry2 = $db->query("SELECT * FROM movimientos INNER JOIN detmovimientos ON (movempresa = dmoempresa AND movnumero = dmonumero) WHERE movprefijo = 'FV' AND dmoproducto = '".$row['proid']."' AND movfecha BETWEEN '$fecha1' AND '$fecha2'");
	while($row2 = $qry2->fetch(PDO::FETCH_ASSOC)){
		if($row2['dmoprefijo' ] == 'AN')
		{
			$cantidad -= $row2['dmocantidad'];
		}
		else
		{
			$cantidad += $row2['dmocantidad'];
			$costo += $row2['dmototal'];
			$venta += $row2['dmoptotal'];
		}
	}
	$pdf->SetFont('LucidaConsole','',8);
	if($i%2 == 0) $x = 235;
	else $x = 250;
	$pdf->SetFillColor($x,$x,$x);
	$pdf->Cell(25,5,$row['proid'],0,0,'C',true);
	$pdf->Cell(85,5,$row['pronombre'],0,0,'L',true);
	$pdf->Cell(20,5,number_format($cantidad,2),0,0,'C',true);
	$pdf->Cell(30,5,number_format($costo,2),0,0,'C',true);
	$pdf->Cell(30,5,number_format($venta,2),0,1,'C',true);
	$i++;
}
$pdf->Output('producto.pdf','d');
?>