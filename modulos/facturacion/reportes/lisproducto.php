<?php
	$r = '../../../';
	require($r . 'incluir/session.php');
	require($r . 'incluir/connection.php');
	$producto = $_POST['producto'];
	$fecha1 = $_POST['fecha1'];
	$fecha2 = $_POST['fecha2'];
	$filtro = 'producto=' . $producto . '&fecha1=' . $fecha1 . '&fecha2=' . $fecha2;
	if ($producto != '')
		$sql = "SELECT * FROM productos WHERE proid = '$producto'";
	else
		$sql = "SELECT * FROM productos";
	$qry = $db->query($sql);
?>
<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>

	<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
	<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#tabla').dataTable({
				'bStateSave': true,
				'bJQueryUI': true,
				'sPaginationType': 'full_numbers',
				'oLanguage': {
					'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
				},
			});
		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Facturacion</a>
				<div class="mapa_div"></div><a class="current">Reportes</a>
			</article>
			<article id="contenido">
				<h2>Ventas x producto desde <?php echo $fecha1 . ' al ' . $fecha2 ?></h2>
				<div class="reporte">
					<a href="pdfproducto.php?<?php echo $filtro ?>">
						<img src="<?php echo $r ?>imagenes/iconos/pdf.png" title="pdf" />
					</a> 
					<a href="excelproducto.php?<?php echo $filtro ?>">
						<img src="<?php echo $r ?>imagenes/iconos/excel.png" title="csv" />
					</a>
				</div>
				<table id="tabla">
					<thead>
						<th>Codigo</th>
						<th>Nombre</th>
						<th>Cantidad</th>
						<th>Costos</th>
						<th>Ventas</th>
						</tr>
					</thead>
					<tbody>
						<?php
						while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
							$cantidad = 0;
							$costo = 0;
							$venta = 0;
							$qry2 = $db->query("SELECT * FROM movimientos INNER JOIN detmovimientos ON (movempresa = dmoempresa AND movnumero = dmonumero) WHERE movprefijo = 'FV'  AND dmoproducto = '" . $row['proid'] . "' AND movfecha BETWEEN '$fecha1' AND '$fecha2'");
							while ($row2 = $qry2->fetch(PDO::FETCH_ASSOC)) {
								if($row2['dmoprefijo' ]== 'AN')
								{
									$cantidad -= $row2['dmocantidad'];

								}
								else
								{
									$cantidad += $row2['dmocantidad'];
									$costo += $row2['dmototal'];
									$venta += $row2['dmoptotal'];
								}
							}
						?>
							<tr>
								<td><?php echo $row['proid'] ?></td>
								<td><?php echo $row['pronombre'] ?></td>
								<td><?php echo number_format($cantidad, 0, ',', '.') ?></td>
								<td><?php echo number_format($costo, 0, ',', '.') ?></td>
								<td><?php echo number_format($venta, 0, ',', '.') ?></td>
							</tr>
						<?php
						}
						?>
					</tbody>
				</table>
				<p class="boton">
					<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href='conproducto.php'">atras</button>
				</p>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
</body>

</html>