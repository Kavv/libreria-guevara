<?php
	$r = '../../../';
	require($r.'incluir/session.php');
	require($r.'incluir/connection.php');
	include($r.'incluir/phpexcel/Classes/PHPExcel.php');
	$empresa = $_GET['empresa'];
	$fecha1 = $_GET['fecha1'];
	$fecha2 = $_GET['fecha2'];
	if($empresa != ''){
		$sql = "SELECT * FROM (movimientos INNER JOIN empresas ON movempresa = empid) INNER JOIN solicitudes ON (movempresa = solempresa AND movnumero = solfactura) WHERE movempresa = '$empresa' AND movprefijo = 'FV' AND movfecha BETWEEN '$fecha1' AND '$fecha2' ORDER BY movempresa, movnumero";
		$row2 = $db->query("SELECT * FROM empresas WHERE empid = '$empresa'")->fetch(PDO::FETCH_ASSOC);
	}else $sql = "SELECT * FROM (movimientos INNER JOIN empresas ON movempresa = empid) INNER JOIN solicitudes ON (movempresa = solempresa AND movnumero = solfactura) WHERE movprefijo = 'FV' AND movfecha BETWEEN '$fecha1' AND '$fecha2' ORDER BY movempresa, movnumero";
	$qry = $db->query($sql);
	$objPHPExcel = new PHPExcel();
	$styleArray = array(
		'borders' => array(
			'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN
			)
		)
	);
	$objPHPExcel->getActiveSheet()->getStyle('A1:H2')->applyFromArray($styleArray);
	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:H1');
	if($empresa == "")
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'RESUMEN DE VENTAS DETALLADA DESDE '.$fecha1.' AL '.$fecha2);
	else
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'RESUMEN DE VENTAS DETALLADA DESDE '.$fecha1.' AL '.$fecha2.' DE LA EMPRESA '.$row2['empnombre']);	
	$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A2', 'EMPRESA')
				->setCellValue('B2', '#FACTURA')
				->setCellValue('C2', 'FECHA')
				->setCellValue('D2', 'CONTADO')
				->setCellValue('E2', 'CREDITO')
				->setCellValue('F2', 'FINANCIACION')
				->setCellValue('G2', 'CUOTA INICIAL')
				->setCellValue('H2', 'ESTADO');
	$credito = 0;
	$contado = 0;
	$ctaini = 0;
	$finantotal = 0;
	$i = 3;
	while($row = $qry->fetch(PDO::FETCH_ASSOC)){
		$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':H'.$i)->applyFromArray($styleArray);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$i, $row['empnombre'])
		->setCellValue('B'.$i, $row['movnumero'])
		->setCellValue('C'.$i, $row['movfecha']);		
		if($row['solncuota'] == '0'){
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$i, $row['movvalor'])
			->setCellValue('E'.$i, 0);
		}else{
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$i, 0)
			->setCellValue('E'.$i, $row['movvalor']);
		}
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$i, $row['movfinan'])
		->setCellValue('G'.$i, $row['solcuota'])
		->setCellValue('H'.$i, $row['movestado']);
		
		if($row['movestado'] != 'ANULADA'){
			
			if($row['solncuota'] == '0')
				$contado = $contado + $row['movvalor'];
			else
				$credito = $credito + $row['movvalor'];
			$finantotal = $finantotal + $row['movfinan'];
			$ctaini = $ctaini + $row['solcuota'];
		}
		$i++;
	}
	$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':G'.$i)->applyFromArray($styleArray);
	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$i.':C'.$i);
	$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('A'.$i, 'TOTALES PARCIALES:')
		->setCellValue('D'.$i, $contado)
		->setCellValue('E'.$i, $credito)
		->setCellValue('F'.$i, $finantotal)
		->setCellValue('G'.$i, $ctaini);
	$i++;
	$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':G'.$i)->applyFromArray($styleArray);
	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$i.':F'.$i);
	$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('A'.$i, 'TOTAL FINAL:')
		->setCellValue('G'.$i, $contado + $credito + $finantotal);

	// Redirect output to a client’s web browser (Excel2007)
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="ventas.xlsx"');
	header('Cache-Control: max-age=0');
	// If you're serving to IE 9, then the following may be needed
	header('Cache-Control: max-age=1');

	// If you're serving to IE over SSL, then the following may be needed
	header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
	header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header ('Pragma: public'); // HTTP/1.0

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');
?>