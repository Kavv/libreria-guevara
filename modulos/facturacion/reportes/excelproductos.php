<?php
	$r = '../../../';
	require($r.'incluir/session.php');
	require($r.'incluir/connection.php');
	require($r.'incluir/phpexcel/Classes/PHPExcel.php');


	$qry2 = $db->query("SELECT solempresa,solid,solestado,proid,pronombre,detcantidad FROM solicitudes 
	INNER JOIN detsolicitudes on (detsolicitud = solid and detempresa = solempresa)
	INNER JOIN productos on detproducto = proid
	WHERE solestado in ('APROBADO SCREEN','PROCESO');");
	$num2 = $qry2->rowCount();
		

		
	$titulo = 'LISTADO DE PRODUCTOS SOLICITADOS AUN SIN FACTURAR';

	$objPHPExcel = new PHPExcel();
	$styleArray = array(
		'borders' => array(
			'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN
			)
		)
	);
	$objPHPExcel->getActiveSheet()->getStyle('A1:F2')->applyFromArray($styleArray);
	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:F1')
				->setCellValue('A1', $titulo);
	$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A2', 'EMPRESA')
				->setCellValue('B2', 'SOLICITUD')	
				->setCellValue('C2', 'ESTADO SOLICITUD')
				->setCellValue('D2', 'ID PRODUCTO')
				->setCellValue('E2', 'NOMBRE PRODUCTO')
				->setCellValue('F2', 'CANTIDAD');	
	$i = 3;
	while($row2 = $qry2->fetch(PDO::FETCH_ASSOC)){
		
			
		$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':F'.$i)->applyFromArray($styleArray);
		$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A'.$i, $row2['solempresa'])
			->setCellValue('B'.$i, $row2['solid'])
			->setCellValue('C'.$i, $row2['solestado'])
			->setCellValue('D'.$i, $row2['proid'])
			->setCellValue('E'.$i, $row2['pronombre'])
			->setCellValue('F'.$i, $row2['detcantidad']);
		$i++;

	}
	// Redirect output to a client’s web browser (Excel2007)
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="Listado Productos.xlsx"');
	header('Cache-Control: max-age=0');
	// If you're serving to IE 9, then the following may be needed
	header('Cache-Control: max-age=1');

	// If you're serving to IE over SSL, then the following may be needed
	header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
	header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header ('Pragma: public'); // HTTP/1.0

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');
?>