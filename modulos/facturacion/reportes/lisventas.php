<?php
	$r = '../../../';
	require($r . 'incluir/session.php');
	require($r . 'incluir/connection.php');
	
	$empresa = $_POST['empresa'];
	$fecha1 = $_POST['fecha1'];
	$fecha2 = $_POST['fecha2'];
	$filtro = 'empresa=' . $empresa . '&fecha1=' . $fecha1 . '&fecha2=' . $fecha2;
	if ($empresa != '') {
		$sql = "SELECT * FROM (movimientos INNER JOIN empresas ON movempresa = empid) INNER JOIN solicitudes ON (movempresa = solempresa AND movnumero = solfactura) WHERE movempresa = '$empresa' AND movprefijo = 'FV' AND movfecha BETWEEN '$fecha1' AND '$fecha2' ORDER BY movempresa, movnumero";
		$row2 = $db->query("SELECT * FROM empresas WHERE empid = '$empresa'")->fetch(PDO::FETCH_ASSOC);
	} else $sql = "SELECT * FROM (movimientos INNER JOIN empresas ON movempresa = empid) INNER JOIN solicitudes ON (movempresa = solempresa AND movnumero = solfactura) WHERE movprefijo = 'FV' AND movfecha BETWEEN '$fecha1' AND '$fecha2' ORDER BY movempresa, movnumero";
	$qry = $db->query($sql);
	$num = $qry->rowCount();
?>
<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>

	<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
	<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#tabla').dataTable({
				'bStateSave': true,
				'bJQueryUI': true,
				'sPaginationType': 'full_numbers',
				'oLanguage': {
					'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
				},
				'aoColumnDefs': [{
					'bSortable': false,
					'aTargets': [2, 3, 4, 5, 6, 7]
				}],
				'aaSorting': [
					[0, 'asc'],
					[1, 'asc']
				],
			});
			$('.pdf').click(function() {
				newSrc = $(this).attr('data-rel');
				if (validation_file(newSrc) == 200)
					$('#modal').html("<iframe src='" + newSrc + "' width='100%' height='100%'></iframe>");
				else
					$('#modal').html("<spam>El PDF no fue encontrado</spam>");
				$('#modal').dialog({
					modal: true,
					width: '800',
					height: '900',
					title: 'PDF de la factura'
				});
			});
		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Facturacion</a>
				<div class="mapa_div"></div><a class="current">Reportes</a>
			</article>
			<article id="contenido">
				<h2>Resumen de ventas detallada desde <?php echo $fecha1 . ' al ' . $fecha2;
														if ($empresa != '') echo ' de la empresa ' . $row2['empnombre'] ?></h2>
				<div class="reporte">
					<a href="pdfventas.php?<?php echo $filtro ?>" target="_blank">
						<img src="<?php echo $r ?>imagenes/iconos/pdf.png" title="pdf" />
					</a> 
					<a href="excelventas.php?<?php echo $filtro ?>">
						<img src="<?php echo $r ?>imagenes/iconos/excel.png" title="csv" />
					</a>
				</div>
				<table id="tabla">
					<thead>
						<th>Empresa</th>
						<th>#Factura</th>
						<th>Fecha</th>
						<th>PDF</th>
						<th>Contado</th>
						<th>Credito</th>
						<th>Fiananciacion</th>
						<th>Cuota inicial</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$credito = 0;
						$contado = 0;
						$ctaini = 0;
						$finantotal = 0;
						while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
							if ($row['movestado'] == 'ANULADA') 
							{
								echo '
									<tr style="background-color:#f09999!important">
									<td style="background-color:#f09999!important" title="' . $row['movempresa'] . '">' . $row['empnombre'] . '</td>
									<td style="background-color:#f09999!important" align="center">' . $row['movnumero'] . '</td>';
							}
							else
							{
								echo '
									<tr>
									<td title="' . $row['movempresa'] . '">' . $row['empnombre'] . '</td>
									<td align="center">' . $row['movnumero'] . '</td>';
									if ($row['solncuota'] == '0')
										$contado = $contado + $row['movvalor'];
									else
										$credito = $credito + $row['movvalor'];
									$finantotal = $finantotal + $row['movfinan'];
									$ctaini = $ctaini + $row['solcuota'];
							}
							echo '
								<td align="center"><img src="' . $r . 'imagenes/iconos/date.png" title="' . $row['movfecha'] . '" /></td>
								<td align="center"><img src="' . $r . 'imagenes/iconos/pdf.png" class="pdf" data-rel="' . $r . 'pdf/facturas/' . $row['movempresa'] . '/' . $row['movnumero'] . '.pdf" title="Factura" /></td>';
							if ($row['solncuota'] == '0') 
							{
								echo '
									<td align="right">' . number_format($row['movvalor'], 2, '.', ',') . '</td>
									<td></td>';
							} 
							else 
							{
								echo '
									<td></td>
									<td align="right">' . number_format($row['movvalor'], 2, '.', ',') . '</td>';
							}
							echo '
								<td align="right">' . number_format($row['movfinan'], 2, '.', ',') . '</td>
								<td align="right">' . number_format($row['solcuota'], 2, '.', ',') . '</td>
								</tr>';
						}
						?>
					</tbody>
					<tfoot>
						<tr>
							<td colspan="4"></td>
							<td align="right" bgcolor="#DFDFDF"><?php echo number_format($contado, 2, '.', ',') ?></td>
							<td align="right" bgcolor="#DFDFDF"><?php echo number_format($credito, 2, '.', ',') ?></td>
							<td align="right" bgcolor="#DFDFDF"><?php echo number_format($finantotal, 2, '.', ',') ?></td>
							<td align="right" bgcolor="#DFDFDF"><?php echo number_format($ctaini, 2, '.', ',') ?></td>
						</tr>
						<tr>
							<td colspan="4"></td>
							<!--- <td colspan="4" align="right" bgcolor="#CFCFCF"><strong><?php echo number_format($contado + $credito + $finantotal + $ctaini, 2, '.', ',') ?></strong></td> --->
							<!--- CAMBIO REALIZADO EL 06 ABRIL DE 2015 DEBIDO A QUE ESTA SUMANDO DOBLE, CTAINICIAL Y CONTADO. APROBADO POR NIYIRETH --->
							<td colspan="4" align="right" bgcolor="#CFCFCF"><strong><?php echo number_format($contado + $credito + $finantotal, 2, '.', ',') ?></strong></td>
						</tr>
					</tfoot>
				</table>
				<p class="boton">
					<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href='conventas.php'">atras</button>
				</p>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<div id="modal" style="display:none"></div>
</body>

</html>