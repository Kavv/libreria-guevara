<?php
$r = '../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
$empresa = $_POST['empresa'];
$fecha1 = $_POST['fecha1'];
$fecha2 = $_POST['fecha2'];
$contado = 0;
$ctaini = 0;
$credito = 0;
$finan = 0;
$costo = 0;
$filtro = 'empresa='.$empresa.'&fecha1='.$fecha1.'&fecha2='.$fecha2;
if($empresa != ''){
	$sql = "SELECT * FROM empresas WHERE empid = $empresa";
	$row4 = $db->query($sql)->fetch(PDO::FETCH_ASSOC);
}else $sql = "SELECT * FROM empresas";
$qry = $db->query($sql);
while($row = $qry->fetch(PDO::FETCH_ASSOC)){
	$qry2 = $db->query("SELECT * FROM movimientos INNER JOIN solicitudes ON (movempresa = solempresa AND movnumero = solfactura) WHERE movempresa = ".$row['empid']." AND movfecha BETWEEN '$fecha1' AND '$fecha2' AND movprefijo = 'FV'");
	while($row2 = $qry2->fetch(PDO::FETCH_ASSOC)){
		$qry3 = $db->query("SELECT * FROM detmovimientos WHERE dmoempresa = ".$row['empid']." AND dmonumero = '".$row2['movnumero']."'");
		while($row3 = $qry3->fetch(PDO::FETCH_ASSOC))
			$costo = $costo + $row3['dmototal'];
		if($row2['solncuota'] > 0){
			$credito = $credito + $row2['movvalor'];
			$finan = $finan + $row2['movfinan'];
			$ctaini = $ctaini + $row2['solcuota'];
		}else $contado = $contado + $row2['movvalor'];
	}
}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
	<?php 
		require($r . 'incluir/src/head.php');
		require($r . 'incluir/src/head-form.php');
	?>	

<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#tabla').dataTable({
		'bPaginate': false,
        'bLengthChange': false,
        'bFilter': false,
		'bSort': false,
        'bInfo': false,
        'bAutoWidth': false,
		'bStateSave': true,
		'bJQueryUI': true
	});
	$('.btnatras').button({ icons: { primary: 'ui-icon ui-icon-arrowthick-1-w' }});
});
</script>
</head>
<body>
<?php require($r.'incluir/src/login.php') ?>
<section id="principal">
<?php require($r.'incluir/src/cabeza.php') ?>
<?php require($r.'incluir/src/menu.php') ?>
<article id="cuerpo">
<article class="mapa">
<a href="#">Principal</a><div class="mapa_div"></div><a href="#">Facturacion</a><div class="mapa_div"></div><a class="current">Reportes</a>
</article>
<article id="contenido">
<h2>Resumen resumen de ventas desde <?php  echo $fecha1.' al '.$fecha2; if($empresa != '') echo ' de la empresa '.$row4['empnombre'] ?></h2>
<div class="reporte">
<a href="pdfresumen.php?<?php echo $filtro ?>"><img src="<?php echo $r ?>imagenes/iconos/pdf.png" title="pdf" /></a> <a href="excelresumen.php?<?php echo $filtro ?>"><img src="<?php echo $r ?>imagenes/iconos/excel.png" title="csv" /></a>
</div>
<table id="tabla">
<thead>
<th></th><th></th>
</thead>
<tbody>
<tr><td>VR. Ventas brutas entre el intervalo de fechas: </td><td align="right"><?php echo number_format($credito + $contado + $finan,0,',','.') ?></td></tr>
<tr><td>VR. Ventas de contado entre el intervalo de fechas: </td><td align="right"><?php echo number_format($contado,0,',','.') ?></td></tr>
<tr><td>VR. Cuotas iniciales entre el intervalo de fechas: </td><td align="right"><?php echo number_format($ctaini,0,',','.') ?></td></tr>
<tr><td>VR. Ventas a credito entre el intervalo de fechas: </td><td align="right"><?php echo number_format($credito,0,',','.') ?></td></tr>
<tr><td>VR. Financiado entre el intervalo de fechas: </td><td align="right"><?php echo number_format($finan,0,',','.') ?></td></tr>
<tr><td>VR. Costos entre el intervalo de fechas: </td><td align="right"><?php echo number_format($costo,0,',','.') ?></td></tr>
</tbody>
</table>
<p class="boton">
<button type="button" class="btnatras" onClick="carga(); location.href = 'conresumen.php'">atras</button>
</p>
</article>
</article>
<?php require($r.'incluir/src/pie.php') ?>
</section>
</body>
</html>