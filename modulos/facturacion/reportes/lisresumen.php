<?php
	$r = '../../../';
	require($r . 'incluir/session.php');
	require($r . 'incluir/connection.php');
	$empresa = $_POST['empresa'];
	$fecha1 = $_POST['fecha1'];
	$fecha2 = $_POST['fecha2'];
	$contado = 0;
	$ctaini = 0;
	$credito = 0;
	$finan = 0;
	$costo = 0;
	$filtro = 'empresa=' . $empresa . '&fecha1=' . $fecha1 . '&fecha2=' . $fecha2;
	
	$total = 0;
	// En caso de especificar una empresa
	if ($empresa != 'TODAS') 
	{
		// Obtener los datos de la empresa seleccionada para usarlo en el encabezado de la tabla
		$sql = "SELECT * FROM empresas WHERE empid = '$empresa'";
		$rowemp = $db->query($sql)->fetch(PDO::FETCH_ASSOC);
		// Los movimientos en un rango de tiempo relacionados a la empresa seleccionada
		$sqlgeneral = "SELECT * FROM solicitudes INNER JOIN movimientos ON solfactura = movnumero AND movempresa = solempresa  WHERE movempresa = '" . $empresa . "' AND movfecha BETWEEN '" . $fecha1 . "' AND '" . $fecha2 . "' AND movprefijo = 'FV' AND movestado <> 'ANULADA'"; // qry general ventas bruto	
	}
	else
	{
		// Retorna todas los movimientos en un rango de tiempo sin importar la empresa
		$sqlgeneral = "SELECT * FROM solicitudes INNER JOIN movimientos ON solfactura = movnumero AND movempresa = solempresa  WHERE movfecha BETWEEN '" . $fecha1 . "' AND '" . $fecha2 . "' AND movprefijo = 'FV' AND movestado <> 'ANULADA'"; // qry general ventas bruto
	}

	$query_general = $db->query($sqlgeneral);
	// Este arreglo se utilizara como matriz para almacenar las ventas totales
	// Utilizando una agrupacion de modo:  [empresa] [cliente]
	$ventas = [];
	// Se recorren todos los movimientos
	while($movimiento = $query_general->fetch(PDO::FETCH_ASSOC))
	{
		// El valor total de la venta
		$valor_total = $movimiento['movvalor'] + $movimiento['movfinan'];
		// La sumatoria de todas las ventas
		$total += $valor_total;
		// Si ya existe la empresa en la matriz entonces
		if(isset($ventas[$movimiento['movempresa']]))
		{
			// Si ya existe la empresa y el cliente en la matriz entonces 
			if(isset($ventas[$movimiento['movempresa']][$movimiento['movtercero']]))
			{
				// Se suma el valor existente con el valor nuevo. 
				$ventas[$movimiento['movempresa']][$movimiento['movtercero']] += $valor_total; 
			}
			else
			{
				// Se define el cliente en la matriz y se le asigna su primer valor
				$ventas[$movimiento['movempresa']][$movimiento['movtercero']] = $valor_total; 
			}
		}
		else
		{
			// Se define la empresa y el cliente en la matriz asignado su primer valor
			$ventas[$movimiento['movempresa']][$movimiento['movtercero']] = $valor_total; 
		}
	}
?>
<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>

	<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
	<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#tabla').dataTable({
				'bPaginate': false,
				'bLengthChange': false,
				'bFilter': false,
				'bSort': false,
				'bInfo': false,
				'bAutoWidth': false,
				'bJQueryUI': true
			});
		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Facturacion</a>
				<div class="mapa_div"></div><a class="current">Reportes</a>
			</article>
			<article id="contenido">
				<h2>
					<?php
					if ($empresa != 'TODAS') {
						echo "Ventas totales desde " . $fecha1 . ' al ' . $fecha2 . ' de la empresa ' . $rowemp['empnombre'];
					} else {
						echo "Ventas totales de todas las empresas desde " . $fecha1 . " al " . $fecha2;
					}
					?>
				</h2>
				<div class="reporte">
					<!-- IMPORTANTE AJUSTAR -->
					<!-- <a href="pdfresumen.php?<?php echo $filtro ?>">
						<img src="<?php echo $r ?>imagenes/iconos/pdf.png" title="pdf" />
					</a> 
					<a href="excelresumen.php?<?php echo $filtro ?>">
						<img src="<?php echo $r ?>imagenes/iconos/excel.png" title="csv" />
					</a> -->
				</div>
				<table id="tabla">
					<thead>
						<th>Empresa</th>
						<th>Cliente</th>
						<th>Venta Total</th>
					</thead>
					<tbody>
					<?php
						// Se recorre la matriz primeramente por las empresas
						foreach ($ventas as $empresa => $empresas) {
							// Obtenemos los datos de la empresa
							$sql = "SELECT * FROM empresas WHERE empid = '$empresa'";
							$query = $db->query($sql)->fetch(PDO::FETCH_ASSOC);
							$nombre_empresa = $query['empnombre'];
							// Se recorren la empresa ya que posee 1 o mas clientes con el valor de la venta total
							foreach ($empresas as $cliente => $total_ventas) {
								// Obtenemos los datos del cliente
								$sql = "SELECT * FROM clientes WHERE cliid = '$cliente'";
								$query = $db->query($sql)->fetch(PDO::FETCH_ASSOC);
								$nombre_cliente = $query['clinombre'] ." ". $query['clinom2'] ." ". $query['cliape1'] ." ". $query['cliape2'];
								
								if($query)
								{
									echo "
										<tr>
										<td> $nombre_empresa </td>
										<td> $nombre_cliente </td>
										<td> $total_ventas </td>
									";
								}
							}
						}				
					?>
					</tbody>
					<tfoot>
						<tr>
							<td colspan="2" class="text-right"><strong>VENTA TOTAL<strong></td>
							<td><strong><?php echo $total; ?></strong></td>
						</tr>
					</tfoot>
				</table>
				<p class="boton">
					<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href = 'conresumen.php'">atras</button>
				</p>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
</body>

</html>