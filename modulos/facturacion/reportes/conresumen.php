<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
?>
<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$("#fecha1").datepicker({
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
				maxDate: "+0D",
				onClose: function(selectedDate) {
					$("#fecha2").datepicker("option", "minDate", selectedDate);
				}
			}).keypress(function(event) {
				event.preventDefault()
			});
			$("#fecha2").datepicker({
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
				maxDate: "+0D",
				onClose: function(selectedDate) {
					$("#fecha1").datepicker("option", "maxDate", selectedDate);
				}
			}).keypress(function(event) {
				event.preventDefault()
			});

			/* Valida que ambas fechas sean especificadas */
			$('#fecha1').change(function() {
				both_date();
			});

			$('#fecha2').change(function() {
				both_date();
			});

			function both_date() {
				if ($('#fecha2').val() != "" || $('#fecha1').val() != "") {
					$('#fecha1').attr("required", true);
					$('#fecha2').attr("required", true);
				} else {
					$('#fecha1').removeAttr("required", false);
					$('#fecha2').removeAttr("required", false);
				}
			}
		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Facturacion</a>
				<div class="mapa_div"></div><a class="current">Reportes</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="lisresumen.php" method="post">
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">Reporte resumen de ventas</legend>
						<p>
							<label for="empresa">Empresa:</label>
							<select name="empresa" class="empresa">
								<option value="TODAS">TODAS</option>
								<?php
								$qry = $db->query("SELECT * FROM empresas ORDER BY empnombre");
								while ($row = $qry->fetch(PDO::FETCH_ASSOC))
									echo '<option value=' . $row['empid'] . '>' . $row['empnombre'] . '</option>';
								?>
							</select>
						</p>
						<p>
							<label for="fecha">Fechas:</label>
							<input type="text" id="fecha1" name="fecha1" class="fecha validate[required]" /> - <input type="text" id="fecha2" name="fecha2" class="fecha validate[required]" />
						</p>
						<p class="boton">
							<button type="submit" class="btn btn-primary btnconsulta">consultar</button>
						</p>
					</fieldset>
				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
</body>

</html>