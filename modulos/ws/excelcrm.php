<?php
$r = '../../';
require($r.'incluir/phpexcel/Classes/PHPExcel.php');
require($r.'incluir/funciones.php');

$dsn = 'mysql:host=localhost;dbname=sophya';
$username = 'root';
$password = 'IdenCorp2013';
$db = new PDO($dsn, $username, $password);

$hoy = date('Y-m-d');

	$documento = $_GET['documento'];
	$tipodoc = $_GET['tipodoc'];


$sql = "SELECT * FROM clientes INNER JOIN carteras on carcliente = cliid INNER JOIN solicitudes on solcliente = cliid  WHERE cliid = '".$documento."'";
$qry = $db->query($sql);
$row = $qry->fetch(PDO::FETCH_ASSOC);

$objPHPExcel = new PHPExcel();
$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN
		)
	)
);
$objPHPExcel->getActiveSheet()->getStyle('A1:N1')->applyFromArray($styleArray);
$objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1', 'IDENTIFICACION')
		->setCellValue('B1', 'TIPO DE IDENTIFICACION')	
        ->setCellValue('C1', 'NOMBRE CLIENTE')
		->setCellValue('D1', 'NUMERO DE FACTURA')
		->setCellValue('E1', 'EMPRESA')
		->setCellValue('F1', 'VALOR TOTAL')
        ->setCellValue('G1', 'EMAIL')
		->setCellValue('H1', 'SALDO TOTAL')
        ->setCellValue('I1', 'DIAS EN MORA')
		->setCellValue('J1', 'VALOR EN MORA')
		->setCellValue('K1', 'DESCRIPCION FACTURA')
		->setCellValue('L1', 'EFECTY')
		->setCellValue('M1', 'CODIGO')	
		->setCellValue('N1', 'MENSAJE');
			
$i = 2;

	
	if ($row['clitide'] == 1){
		$tipoidentificacion = "CEDULA DE CIUDADANIA";
	} elseif($row->clitide == 2) {
		$tipoidentificacion = "NIT";
	} else {
		$tipoidentificacion = "NONE";
	}
	
	$serviceCode = $row['carfactura'];
	$solicitud = $row['solid'];
	$accountId  = $row['carempresa'];
	
	$qry2 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$accountId ."' AND dcafactura = '".$serviceCode."' AND dcaestado = 'ACTIVA' ORDER BY dcacuota ASC");
	$row2 = $qry2->fetch(PDO::FETCH_ASSOC);
	$num2 = $qry2->rowCount();
	
	$qry3 = $db->query("SELECT count(dcacuota) AS CuotasMora FROM detcarteras WHERE dcaempresa = '".$accountId ."' AND dcafactura = '".$serviceCode."' AND dcaestado = 'ACTIVA'");
	$row3 = $qry3->fetch(PDO::FETCH_ASSOC);
	$num3 = $qry3->rowCount();
	
	$qry4 = $db->query("SELECT * FROM detsolicitudes INNER JOIN productos on detproducto = proid WHERE detempresa = '".$accountId ."' AND detsolicitud = '".$solicitud."' ");
	while($row4 = $qry4->fetch(PDO::FETCH_ASSOC)){
		$descripcion  .= $row4['detcantidad']."_".$row4['pronombre']." ";
	}

	
	$diasmora = fechaDif($row2['dcafecha'], date('Y-m-d'));
	$valormora = ($row3['CuotasMora']*$row['carcuota']);	

	if($diasmora <= 0){
		$mora = 0;
		$valormoratotal = 0;
	} else {
		if($row2['dcavalor'] <> 0){
		$valormoratotal = ($valormora - $row2['dcavalor']);
		} else {
			$valormoratotal = $valormora;
		}
		$mora = $diasmora;
	}
	
	$identificacion = $row['cliid'];
	$nombre = $row['clinombre']." ".$row['clinom2']." ".$row['cliape1']." ".$row['cliape2'];
	$serviceCode = $row['carfactura'];
	$accountId  = $row['carempresa'];
	$total = $row['carsaldo'];
	$email = $row['cliemail'];
	$saldototal = $row['carsaldo'];
	$serviceCodeEfecty = '110362';
	$codigo = "00";
	$mensaje = "Facturas Encontradas";
	$tax = "";
	$bdev = "";
	$experiddate = "";
	
	if ($num2 == 0){
		$tipoidentificacion = "";
		$nombre = "";
		$serviceCode = "";
		$accountId  = "";
		$total = "";
		$email = "";
		$saldototal = "";
		$mora = "";
		$valormoratotal = "";
		$descripcion  = "";
		$serviceCodeEfecty = "";
		$codigo = '01';
		$mensaje = "Facturas no Encontradas";
		$tax = "";
		$bdev = "";
		$expireddate = "";
	}
	
	
	$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':N'.$i)->applyFromArray($styleArray);
	$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('A'.$i, $identificacion)
    	->setCellValue('B'.$i, $tipoidentificacion)
		->setCellValue('C'.$i, $nombre)
		->setCellValue('D'.$i, $serviceCode)
		->setCellValue('E'.$i, $accountId)
		->setCellValue('F'.$i, $total)
    	->setCellValue('G'.$i, $email)
		->setCellValue('H'.$i, $saldototal)
    	->setCellValue('I'.$i, $mora)
		->setCellValue('J'.$i, $valormoratotal)
		->setCellValue('K'.$i, $descripcion)
		->setCellValue('L'.$i, $serviceCodeEfecty)
		->setCellValue('M'.$i, $codigo)
		->setCellValue('N'.$i, $mensaje);
	$i++;

// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Web Service Sophya '.$identificacion.'.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
?>