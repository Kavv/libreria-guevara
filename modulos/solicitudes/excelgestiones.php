<?php
	$r = '../../';
	require($r.'incluir/session.php');
	require($r.'incluir/connection.php'); 
	require($r.'incluir/phpexcel/Classes/PHPExcel.php');

	$hoy = date('Y-m-d');

	if(isset($_POST['consultar'])){

		$fecha1 = $_POST['fecha1'];
		$fecha2 = $_POST['fecha2'];
	} elseif(isset($_GET['fecha1'])) {
		$fecha1 = $_GET['fecha1'];
		$fecha2 = $_GET['fecha2'];
	}
	

	$filtro = 'fecha1='.$fecha1.'&fecha2='.$fecha2;

	$con = "SELECT * FROM hissolicitudes
	INNER JOIN empresas ON empid = hsoempresa 
	";
	$ord = 'ORDER BY hsofecha ASC ;';

	if($fecha1 != '') 
		$sql = "$con WHERE DATE(hsofecha) BETWEEN '$fecha1' AND '$fecha2' $ord";
	else
		$sql = $con . " " . $ord;


	$qry = $db->query($sql);

	$objPHPExcel = new PHPExcel();
	$styleArray = array(
		'borders' => array(
			'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN
			)
		)
	);
	$objPHPExcel->getActiveSheet()->getStyle('A1:J2')->applyFromArray($styleArray);
	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:J1')
				->setCellValue('A1', 'LISTADO DE SOLICITUDES');
	$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A2', 'NOMBRE EMPRESA')
				->setCellValue('B2', 'NUMERO SOLICITUD')	
				->setCellValue('C2', 'FACTURA')
				->setCellValue('D2', 'ESTADO')
				->setCellValue('E2', 'CLIENTE')			
				->setCellValue('F2', 'FECHA DE SCREEN')
				->setCellValue('G2', 'GESTION')
				->setCellValue('H2', 'FECHA DE GESTION')
				->setCellValue('I2', 'USUARIO SCREEN')
				->setCellValue('J2', 'ASESOR')
				;
				
	$i = 3;
	while($row = $qry->fetch(PDO::FETCH_ASSOC)){
		$cliente = $factura = $estado = $fecha_screen = $usuario_screen = $cliente_id = $capacitador = "";

		$row1 = $db->query("SELECT * FROM  solicitudes INNER JOIN clientes ON cliid = solcliente INNER JOIN usuarios ON usuid = solususcreen WHERE solid  = '".$row['hsosolicitud']."' AND solempresa = '".$row['hsoempresa']."'")->fetch(PDO::FETCH_ASSOC);
		if($row1)
		{
			$cliente = $row1['clinombre'].' '.$row1['clinom2'].' '.$row1['cliape1'].' '.$row1['cliape2'];
			$factura = $row1['solfactura'];
			$estado = $row1['solestado'];
			$fecha_screen = $row1['solfechscreen'];
			$usuario_screen = $row1['usunombre'];
			
			$qryca = $db->query("SELECT * FROM usuarios WHERE usuid = '".$row1['solasesor']."';");
			$rowca = $qryca->fetch(PDO::FETCH_ASSOC);
			if($capacitador)
				$capacitador = $rowca['usunombre'];
		}

		$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':J'.$i)->applyFromArray($styleArray);
		$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A'.$i, $row['empnombre'])
			->setCellValue('B'.$i, $row['hsosolicitud'])
			->setCellValue('C'.$i, $factura)
			->setCellValue('D'.$i, $estado)
			->setCellValue('E'.$i, $cliente)
			->setCellValue('F'.$i, $fecha_screen)
			->setCellValue('G'.$i, $row['hsonota'])
			->setCellValue('H'.$i, $row['hsofecha'])
			->setCellValue('I'.$i, $usuario_screen)
			->setCellValue('J'.$i, $capacitador);
		$i++;
	}
	// Redirect output to a client’s web browser (Excel2007)
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="Reporte Gestiones.xlsx"');
	header('Cache-Control: max-age=0');
	// If you're serving to IE 9, then the following may be needed
	header('Cache-Control: max-age=1');

	// If you're serving to IE over SSL, then the following may be needed
	header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
	header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header ('Pragma: public'); // HTTP/1.0

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');
?>