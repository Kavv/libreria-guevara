<?php
	$r = '../../';
	require($r . 'incluir/session.php');
	require($r . 'incluir/connection.php');
	if (isset($_POST['consultar'])) {
		$asesor = $_POST['asesor'];
		$estado = $_POST['estado'];
		$fecha1 = $_POST['fecha1'];
		$fecha2 = $_POST['fecha2'];
	} else {
		$estado = $_GET['estado'];
		$fecha1 = $_GET['fecha1'];
		$fecha2 = $_GET['fecha2'];
	}
	$filtro = 'asesor=' . $asesor . '&estado=' . $estado . '&fecha1=' . $fecha1 . '&fecha2=' . $fecha2;

	// Si el usuario tiene el permiso de call center entonces aunque quisiera ver todas las solicitudes
	// No podra, solo le mostrara las solicitudes en las que se encuentra asignado como relacionista
	$qrypermisos = $db->query("SELECT * FROM usuarios WHERE usuid = " . $_SESSION['id'] . ";");
	$rowpermisos = $qrypermisos->fetch(PDO::FETCH_ASSOC);
	if ($rowpermisos['usuperfil'] == 98) {
		$anex = " AND solrelacionista = " . $_SESSION['id'] . "";
	} else {
		$anex = "";
	}
	$con = 'SELECT * FROM ((solicitudes INNER JOIN empresas ON solempresa = empid) LEFT JOIN clientes ON solcliente = cliid) INNER JOIN usuarios ON solasesor = usuid';
	$ord =  $anex . ' ORDER BY solfecha DESC';

	/* Los parametros de la consulta sql se genera dinamicamente 
	en base a los datos recibidos para delimitar los resultados */
	$parameters = [];
	if ($asesor != "" && $asesor != "TODOS")
		array_push($parameters, "solasesor = '$asesor'");
	if ($fecha1 != "")
		array_push($parameters, "solfecha BETWEEN '$fecha1' AND '$fecha2'");
	if ($estado != "")
		array_push($parameters, "solestado = '$estado'");
	// Consulta base
	$sql = $con;
	foreach ($parameters as $index => $parameter) {
		// Se agregan los parametros del WHERE
		if ($index == 0)
			$sql .= " WHERE " . $parameter;
		else
			$sql .= " AND " . $parameter;
	}
	// Se completa la consulta
	$sql .= $ord;

	$qry = $db->query($sql);
?>
<!doctype html>
<html>

<head>
	<?php
		require($r . 'incluir/src/head.php');
		require($r . 'incluir/src/head-form.php');
	?>

	<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
	<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#tabla').dataTable({
				'bJQueryUI': true,
				'sPaginationType': 'full_numbers',
				'oLanguage': {
					'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
				},
				'bSort': false,
			});
			$('.pdf').click(function() {
				newSrc = $(this).attr('data-rel');
				if (validation_file(newSrc) == 200)
					$('#modal').html("<iframe src='" + newSrc + "' width='100%' height='100%'></iframe>");
				else
					$('#modal').html("<spam>El PDF no fue encontrado</spam>");
				$('#modal').dialog({
					modal: true,
					width: '600',
					height: '800',
					title: 'PDF de la solicitud'
				});
			});
			$('.historico').click(function() {
				newHref = $(this).attr('data-rel');
				$('#dialog2').load(newHref).dialog({
					modal: true,
					width: 800
				});
			});

		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a class="current">Solicitudes</a>
			</article>
			<article id="contenido">
				<h2>Listado de solicitudes <?php ?></h2>
				<table id="tabla">
					<thead>
						<tr>
							<th title="Empresa">Empresa</th>
							<th title="Numero de solicitud">Solicitud</th>
							<th>Cliente</th>
							<th>Asesor</th>
							<th title="Fecha de la solicitud">Fecha Solicitud</th>
							<th>Estado</th>
							<th>Factura</th>
							<th title="Fecha de despacho">F.D</th>
							<th title="Fecha de entrega">F.E</th>
							<th title="Planilla de entrega">P.E</th>
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php
						while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
						?>
							<tr>
								<td title="<?php echo $row['empid'] . ' ' . $row['empdigito'] ?>"><?php echo $row['empnombre'] ?></td>
								<td align="center"><?php echo $row['solid'] ?></td>
								<td title="<?php echo $row['cliid'] . ' ' . $row['clidigito'] ?>"><?php echo $row['clinombre'] . ' ' . $row['clinom2'] . ' ' . $row['cliape1'] . ' ' . $row['cliape2'] ?></td>
								<td title="<?php echo $row['usuid'] ?>"><?php echo $row['usunombre'] ?></td>
								<td align="center"><?php echo $row['solfecha'] ?></td>
								<td>
									<?php
									if ($row['solestado'] == 'RECHAZADO SCREEN')
									{
										$row2 = $db->query("SELECT * FROM tcausales INNER JOIN causales ON tcaid = cautcausal WHERE tcaid = " . $row['soltcascreen'] . " AND cauid = " . $row['solcauscreen'])->fetch(PDO::FETCH_ASSOC);
										echo '<span title="' . $row2['tcanombre'] . ' - ' . $row2['caunombre'] . '">' . $row['solestado'] . '</spam>';
									}
									elseif ($row['solestado'] == 'RECHAZADO ENTREGA')
									{
										$row2 = $db->query("SELECT * FROM tcausales INNER JOIN causales ON tcaid = cautcausal WHERE tcaid = " . $row['soltcadescarga'] . " AND cauid = " . $row['solcaudescarga'])->fetch(PDO::FETCH_ASSOC);
										echo '<span title="' . $row2['tcanombre'] . ' - ' . $row2['caunombre'] . '">' . $row['solestado'] . '</spam>';
									}
									else
										echo '<span>' . $row['solestado'] . '</span>';
									
									?>
								</td>
								<td align="center"><?php echo $row['solfactura'] ?></td>
								<td align="center"><img src="<?php echo $r ?>imagenes/iconos/date.png" title="<?php echo $row['solfechdespacho'] ?>" /></td>
								<td align="center"><img src="<?php echo $r ?>imagenes/iconos/date.png" title="<?php echo $row['solfechdescarga'] ?>" /></td>
								<td align="center"><?php echo $row['solplanilla'] ?></td>
								<!-- Se elimino de aquí la opcion guia ya que no es aplicable a NIC -->
								<td align="center"> 
									<a href="historico_solicitudes.php?<?php echo 'empresa=' . $row['solempresa'] . '&id=' . $row['solid'] ?>" target='_blank'> 
										<img src="<?php echo $r ?>imagenes/iconos/comments.png" title="Historico Solicitud" /> 
									</a> 
								</td>
								<td align="center"><img src="<?php echo $r ?>imagenes/iconos/pdf.png" class="pdf" data-rel="<?php echo $r . 'pdf/solicitudes/' . $row['solempresa'] . '/' . $row['solid'] . '.pdf' ?>" title="Solicitud" /></td>
							</tr>
						<?php
						}
						?>
					</tbody>
				</table>
				<p class="boton">
					<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href='consultar_prop.php'">atras</button>
				</p>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<?php
	if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>';
	elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>';
	?>
	<div id="modal" style="display:none"></div>
	<div id="dialog2" style="display:none"></div>
</body>

</html>