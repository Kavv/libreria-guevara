<?php
	$r = '../../';
	require($r . 'incluir/session.php');
	require($r . 'incluir/connection.php');
	if (isset($_POST['adinota'])) {
		$empresa = $_POST['empresa'];
		$id = $_POST['id'];
		$nota = trim($_POST['nota']);
		$screen = $_POST['screen'];
		$row = $db->query("INSERT INTO hissolicitudes (hsoempresa, hsosolicitud, hsofecha, hsousuario, hsonota) VALUES ('$empresa', '$id', NOW(), '" . $_SESSION['id'] . "', '$nota')");
		header('Location:form4.php?empresa=' . $empresa . '&id=' . $id . '&screen=' . $screen);
		exit();
	} elseif (isset($_POST['siguiente'])) {
		$empresa = $_POST['empresa'];
		$id = $_POST['id'];
		$screen = $_POST['screen'];
		// Verifica exista minimo un producto agregado al detalle de la solicitud
		$num = $db->query("SELECT * FROM detsolicitudes WHERE detempresa = '$empresa' AND detsolicitud = '$id'")->rowCount();
		if ($num < 1) {
			$error = 'Debe adicionar al menos un producto a la solicitud';
			header('Location:form3.php?empresa=' . $empresa . '&id=' . $id . '&error=' . $error);
			exit();
		}
		$total = $_POST['total'];
		$base = $_POST['base'];
		$cuota = $_POST['cuota'];
		$ncuota = $_POST['ncuota'];
		$qry = $db->query("UPDATE solicitudes SET soltotal = $total, solbase = $base, solncuota = $ncuota, solcuota = '$cuota' WHERE solempresa = '$empresa' AND solid = '$id'");
	} else {
		$empresa = $_GET['empresa'];
		$id = $_GET['id'];
		$screen = $_GET['screen'];
	}
	$row = $db->query("SELECT * FROM ((solicitudes INNER JOIN clientes ON solcliente = cliid) INNER JOIN empresas ON solempresa = empid) LEFT JOIN fpagos ON solfpago = fpaid WHERE solempresa = '$empresa' AND solid = '$id'")->fetch(PDO::FETCH_ASSOC);
?>
<!doctype html>
<html>

<head>

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>

	<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
	<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>

	<script type="text/javascript">
		$(document).ready(function() {
			$('#tabla').dataTable({
				'bJQueryUI': true,
				'bFilter': false,
				'bLengthChange': false,
				'bSort': false,
				'sPaginationType': 'full_numbers',
				'oLanguage': {
					'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
				},
			});
			$('#fecha').datepicker({
				dateFormat: 'yy-mm-dd',
			}).keypress(function(event) {
				event.preventDefault()
			});
			$('#cobro').change(function(event) {
				var id1 = $('#cliente').val();
				var id2 = $('#cobro').find(':selected').val();
				$('#datcobro').load('<?php echo $r ?>incluir/carga/cobro.php?id1=' + id1 + '&id2=' + encodeURIComponent(id2));
			});
			$("#entrega").change(function(event) {
				var id1 = $("#cliente").val();
				var id2 = $("#entrega").find(':selected').val();
				$("#datentrega").load('<?php echo $r ?>incluir/carga/entrega.php?id1=' + id1 + '&id2=' + encodeURIComponent(id2));
			});
			$('.pdf').click(function() {
				newSrc = $(this).attr('data-rel');
				if(validation_file(newSrc) == 200)
					$('#modal').html("<iframe src='" + newSrc + "' width='100%' height='100%'></iframe>");
				else
					$('#modal').html("<spam>El PDF no fue encontrado</spam>");
				
				$('#modal').dialog({
					modal: true,
					width: '600',
					height: '800',
					title: 'PDF de la solicitud'
				});
			});
			$('.historico').click(function() {
				newHref = $(this).attr('data-rel');
				$('#dialog2').load(newHref).dialog({
					modal: true,
					width: 800
				});
			});
			$('.nota').click(function() {
				$('#campo').dialog({
					height: 'auto',
					width: '600px',
					modal: true
				});
			})
		});
	</script>
	<style type="text/css">
		#tabla thead {
			display: none
		}
		.pdf, .historico, .nota{
			cursor: pointer;
		}
	</style>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a class="current">Solicitudes</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="<?php if ($screen == '1') echo 'formscreen.php';
													else echo 'form5.php' ?>" method="post">
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-sm-12 col-md-8 col-lg-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">Solicitud - Hoja 4 
							<img src="<?php echo $r ?>imagenes/iconos/pdf.png" class="pdf" data-rel="<?php echo $r . 'pdf/solicitudes/' . $empresa . '/' . $id . '.pdf' ?>" title="Solicitud en PDF" /> 
							<img src="<?php echo $r ?>imagenes/iconos/application_edit.png" class="historico" data-rel="notas.php?empresa=<?php echo $empresa . '&id=' . $id ?>" title="Historia de la solicitud" /> 
							<img src="<?php echo $r ?>imagenes/iconos/add.png" class="nota" title="Agregar nota" /></legend>
						<p>
							<img src="<?php echo $r ?>imagenes/logos/<?php echo $empresa . "/" . $row['logo']?>" class="img-fluid" title="<?php echo $row['empnombre'] ?>">
							<input type="hidden" name="empresa" value="<?php echo $empresa ?>" />
							<input type="hidden" id="cliente" name="cliente" value="<?php echo $row['cliid'] ?>">
							<div align="center">Solicitud</div>
							<div align="center"><input type="text" class="pedido" name="id" value="<?php echo $id ?>" readonly /></div>
							<div align="center">Fecha</div>
							<div align="center"><input type="text" class="fecha" value="<?php echo $row['solfecha'] ?>" readonly /></div>
						</p>
						<br>
						<br>
						<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12">
							<legend class="ui-widget ui-widget-header ui-corner-all">Informacion de la solicitud</legend>
							<input type="hidden" name="screen" value="<?php echo $screen ?>" />
							<input type="hidden" name="asesor" value="<?php echo $row['solasesor'] ?>" />
							<p>
								<label>cobros: </label>
								<select id="cobro" name="cobro" class="validate[required] text-input">
									<?php
									// Si ya fue establecida la direccion de cobro entonces se coloca atutomaticamente
									if ($row['solcobro'] != '')
										echo '<option value="' . $row['solcobro'] . '">' . $row['solcobro'] . '</option>';
									
									echo '<option value="">SELECCIONE</option>';
									// Muestra las posibles direcciones de cobro del cliente
									if ($row['clidirresidencia'] != '' && $row['solcobro'] != $row['clidirresidencia'])
										echo '<option value="' . $row['clidirresidencia'] . '">' . $row['clidirresidencia'] . '</option>';
									if ($row['clidircomercio'] != '' && $row['solcobro'] != $row['clidircomercio'])
										echo '<option value="' . $row['clidircomercio'] . '">' . $row['clidircomercio'] . '</option>';
									if ($row['clidiradicional'] != '' && $row['solcobro'] != $row['clidiradicional'])
										echo '<option value="' . $row['clidiradicional'] . '">' . $row['clidiradicional'] . '</option>';
									?>
								</select>
								<span id="datcobro"></span>
								<label>Entrega: </label>
								<select id="entrega" name="entrega" class="validate[required]">
									<?php
									// Si ya fue establecida la direccion de entrega entonces se coloca atutomaticamente
									if ($row['solentrega'] != '')
										echo '<option value="' . $row["solentrega"] . '">' . $row["solentrega"] . '</option>';

									echo '<option value="">SELECCIONE</option>';
									
									// Muestra las posibles direcciones de entrega del cliente
									if ($row['clidirresidencia'] != '' && $row['solentrega'] != $row['clidirresidencia'])
										echo '<option value="' . $row['clidirresidencia'] . '">' . $row['clidirresidencia'] . '</option>';
									if ($row['clidircomercio'] != '' && $row['solentrega'] != $row['clidircomercio'])
										echo '<option value="' . $row['clidircomercio'] . '">' . $row['clidircomercio'] . '</option>';
									if ($row["clidiradicional"] != "" && $row['solentrega'] != $row['clidiradicional'])
										echo '<option value="' . $row['clidiradicional'] . '">' . $row['clidiradicional'] . '</option>';
									?>
								</select>
								<span id="datentrega"></span>
							</p>
							<p>
								<label>Compromiso de pago: </label>
								<input type="text" id="fecha" class="fecha validate[required]" name="compromiso" value="<?php echo $row['solcompromiso'] ?>" title="Escoga fecha de compromiso" />
								
								<label class="label">Forma de pago C.I.: </label>
								<select name="fpago" class=" validate[required]">
									<?php
									if ($row['solfpago'] != '')
										echo '<option value="' . $row['solfpago'] . '">' . $row['fpanombre'] . '</option>';
									
									echo '<option value="">SELECCIONE</option>';
									$qry = $db->query("SELECT * FROM fpagos WHERE fpaid <> '" . $row['solfpago'] . "' ORDER BY fpanombre");
									while ($row2 = $qry->fetch(PDO::FETCH_ASSOC))
										echo '<option value="' . $row2['fpaid'] . '">' . $row2['fpanombre'] . '</option>';
									?>
								</select>
							</p>
							<p>
								<label>Relacionista: </label>
								<select name="relacionista">
									<?php
									if ($row['solrelacionista'] != '') {
										$row2 = $db->query("SELECT * FROM usuarios WHERE usuid = '" . $row['solrelacionista'] . "'")->fetch(PDO::FETCH_ASSOC);
										echo '<option value="' . $row2['usuid'] . '">' . $row2['usunombre'] . '</option>';
									}
									echo '<option value="">NINGUNO</option>';
									//Retorna los usuarios que sean relacionista o parte del call center
									$qry = $db->query("SELECT * FROM usuarios WHERE usuid <> '" . $row['solrelacionista'] . "' AND (usurelacionista = '1' OR usuperfil = '98') ORDER BY usunombre");
									while ($row2 = $qry->fetch(PDO::FETCH_ASSOC))
										echo '<option value="' . $row2['usuid'] . '">' . $row2['usunombre'] . '</option>';
									?>
								</select>
							</p>
						</fieldset>
						<p class="boton">
							<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href = 'form3.php?empresa=<?php echo $empresa . '&id=' . $id . '&screen=' . $screen ?>'">atras</button>
							<button type="submit" class="btn btn-primary btnsiguiente" name="siguiente" value="siguiente">siguiente</button>
						</p>
					</fieldset>
				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<div id="campo" title="Agregar historico" style="display:none">
		<form id="form" name="form" action="form4.php" method="post">
			<input type="hidden" name="empresa" value="<?php echo $empresa ?>">
			<input type="hidden" name="id" value="<?php echo $id ?>">
			<input type="hidden" name="screen" value="<?php echo $screen ?>">
			<input type="text" name="nota" required title="Digite la nota" />
			<button type="submit" class="btn btn-success btn-block btnmas" name="adinota" value="adinota">Agregar</button>
	</div>
	<div id="modal" style="display:none"></div>
	<div id="dialog2" style="display:none"></div>
</body>

</html>