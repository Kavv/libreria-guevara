<?php
$r = '../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

$fecha1 = $fecha2 = $estado = "";

if (isset($_GET['modificar'])) {
	$id1 = $_GET['id1'];
	$fecha_hora_soporte = $_GET['fecha'] . " " . $_GET['hora'];
	$qry = $db->query("UPDATE soportes SET soporfechasoporte = '$fecha_hora_soporte' WHERE soporid = $id1");
	if ($qry) {
		$mensaje = 'Se Modifico Correctamente el Soporte';
	} else {
		$error = 'No se pudo modificar el soporte porfavor intente nuevamente o conctactese con el encargado';
	}
}

if (isset($_GET['validar'])) {
	$id1 = $_GET['id1'];
	$qry = $db->query("UPDATE soportes SET soporvalidado = '1' WHERE soporid = $id1");
	$mensaje = 'Se Valido Correctamente el Soporte';
}

if (isset($_GET['error'])) {
	$id1 = $_GET['id1'];
	$qry = $db->query("UPDATE soportes SET soporvalidado = '2' WHERE soporid = $id1");
	$mensaje = 'Se Marco el soporte como error.';
}

if (isset($_POST['consultar'])) {
	$fecha1 = $_POST['fecha1'];
	$fecha2 = $_POST['fecha2'];
	$estado = $_POST['estado'];
} elseif (isset($_GET['fecha1'])) {
	$fecha1 = $_GET['fecha1'];
	$fecha2 = $_GET['fecha2'];
	$estado = $_GET['estado'];
}

$filtro = 'fecha1=' . $fecha1 . '&fecha2=' . $fecha2 . '&estado=' . $estado;

$con = 'SELECT * FROM soportes 
INNER  JOIN empresas ON empid = soporempresa 
INNER JOIN clientes ON cliid = soporcliente 
INNER JOIN usuarios ON usuid = soporusuario';

$ord = ' ORDER BY soporfechasoporte ASC ;';

/* Los parametros de la consulta sql se genera dinamicamente 
en base a los datos recibidos para delimitar los resultados */
$parameters = [];
if($fecha1 != "")
	array_push($parameters, "DATE(soporfechasoporte) BETWEEN '$fecha1' AND '$fecha2'");
if($estado != "")
	array_push($parameters, "soporvalidado = '$estado'" );

// Consulta base
$sql = $con;
foreach ($parameters as $index => $parameter) {
	// Se agregan los parametros del WHERE
	if($index == 0)
		$sql .= " WHERE " . $parameter;
	else
		$sql .= " AND " . $parameter;
}
// Se completa la consulta
$sql .= $ord;
$qry = $db->query($sql);

?>
<!doctype html>
<html>

<head>
	<meta charset="utf-8">
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>

	<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
	<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#tabla').dataTable({
				'bJQueryUI': true,
				'sPaginationType': 'full_numbers',
				'oLanguage': {
					'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
				},
				'bSort': false,
			});
			$('.btnatras').button({
				icons: {
					primary: 'ui-icon ui-icon-arrowthick-1-w'
				}
			});
			$('#dialog-message').dialog({
				height: 80,
				width: 'auto',
				modal: true
			});
			$('.historico').click(function() {
				newHref = $(this).attr('data-rel');
				$('#dialog2').load(newHref).dialog({
					modal: true,
					width: 800
				});
			});
			$('.msj_validar').click(function(e) {
				e.preventDefault();
				var targetUrl = $(this).attr('href');
				var $dialog_link_follow_confirm = $('<div></div>').html("<p><span class='ui-icon ui-icon-alert' style='float:left; margin:2px 7px 7px 0;'></span>Esta seguro que desea validar este soporte?</p>").
				dialog({
					title: 'Porfavor confirmar',
					buttons: {
						'Si': function() {
							window.location.href = targetUrl;
						},
						'No': function() {
							$(this).dialog("close");
						}
					},
					modal: true,
					width: 'auto',
					height: 'auto'
				});
				$dialog_link_follow_confirm.dialog("open");
			});

			$('.msj_error').click(function(e) {
				e.preventDefault();
				var targetUrl = $(this).attr('href');
				var $dialog_link_follow_confirm = $('<div></div>').html("<p><span class='ui-icon ui-icon-alert' style='float:left; margin:2px 7px 7px 0;'></span>Esta seguro que desea marcar este soporte como error?</p>").
				dialog({
					title: 'Porfavor confirmar',
					buttons: {
						'Si': function() {
							window.location.href = targetUrl;
						},
						'No': function() {
							$(this).dialog("close");
						}
					},
					modal: true,
					width: 'auto',
					height: 'auto'
				});
				$dialog_link_follow_confirm.dialog("open");
			});

		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Solicitudes</a>
				<div class="mapa_div"></div><a class="current">Soportes</a>
			</article>
			<article id="contenido">
				<h2>Listado de soportes</h2>
				<div class="reporte">
					<a href="#"><img src="<?php echo $r ?>imagenes/iconos/pdf.png" title="pdf" /></a> <a href="excelsoportes.php?<?php echo $filtro ?>"><img src="<?php echo $r ?>imagenes/iconos/excel.png" title="csv" /></a>
				</div>
				<table id="tabla">
					<thead>
						<tr>
							<th title="Id">Id</th>
							<th title="Empresa">Empresa</th>
							<th title="Numero de solicitud">Solicitud</th>
							<th>Factura</th>
							<th>Cliente</th>
							<th title="Fecha de Soporte">F.S</th>
							<th>Usuario</th>
							<th>E. Soporte</th>
							<th>Comentarios</th>
							<th></th>
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php
						while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
						?>
							<tr>
								<td align="center"><?php echo $row['soporid'] ?></td>
								<td title="<?php echo $row['empid'] . ' ' . $row['empdigito'] ?>"><?php echo $row['empnombre'] ?></td>
								<td align="center"><?php echo $row['soporsolicitud'] ?></td>
								<td align="center"><?php echo $row['soporfactura'] ?></td>
								<td title="<?php echo $row['cliid'] . ' ' . $row['clidigito'] ?>"><?php echo $row['clinombre'] . ' ' . $row['clinom2'] . ' ' . $row['cliape1'] . ' ' . $row['cliape2'] ?></td>
								<td align="center"><?php echo $row['soporfechasoporte'] ?></td>
								<td align="center"><?php echo $row['usunombre'] ?></td>
								<td align="center"><?php if ($row['soporvalidado'] == 1) {
														echo "REALIZADO";
													} else if ($row['soporvalidado'] == 0) {
														echo "PENDIENTE";
													} else if ($row['soporvalidado'] == 2) {
														echo "ERROR";
													}  ?></td>
								<td align="center"><?php echo $row['soporcomentarios'] ?></td>
								<?php
								$qrypermisos = $db->query("SELECT * FROM usuarios WHERE usuid = " . $_SESSION['id'] . ";");
								$rowpermisos = $qrypermisos->fetch(PDO::FETCH_ASSOC);
								if (($rowpermisos['usuperfil'] == 30 or $rowpermisos['usuperfil'] == 63) and $row['soporvalidado'] <> 2) {
								?>
									<td align="center"><a href="listarsopor.php?id1=<?php echo $row['soporid'] . '&validar=1&' . $filtro ?>" class="msj_validar" title="Validar"><img src="<?php echo $r ?>imagenes/iconos/accept.png" /></a></td>
									<td align="center"><a href="modificarsopor.php?id1=<?php echo $row['soporid'] . '&' . $filtro ?>" title="Cambiar Fecha"><img src="<?php echo $r ?>imagenes/iconos/lapiz.png" /></a></td>
									<td align="center"><a href="listarsopor.php?id1=<?php echo $row['soporid'] . '&error=1&' . $filtro ?>" class="msj_error" title="Error"><img src="<?php echo $r ?>imagenes/iconos/cancel.png" /></a></td>
								<?php } else { ?>
									<td align="center"><img src="<?php echo $r ?>imagenes/iconos/accept.png" class="gray" /></td>
									<td align="center"><img src="<?php echo $r ?>imagenes/iconos/lapiz.png" class="gray" /></td>
									<td align="center"><img src="<?php echo $r ?>imagenes/iconos/cancel.png" class="gray" /></td>
								<?php } ?>
							</tr>
						<?php
						}
						?>
					</tbody>
				</table>
				<p class="boton">
					<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href='consoporte.php'">Atras</button>
				</p>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<?php
	if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>';
	elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>';
	?>
	<div id="modal" style="display:none"></div>
	<div id="dialog2" style="display:none"></div>
</body>

</html>