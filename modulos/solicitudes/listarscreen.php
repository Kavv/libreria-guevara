<?php
	$r = '../../';
	require($r . 'incluir/session.php');
	require($r . 'incluir/connection.php');

	if (isset($_POST['consultar'])) {
		$fecha1 = $_POST['fecha1'];
		$fecha2 = $_POST['fecha2'];
	} elseif(isset($_GET['fecha1'])) {
		$fecha1 = $_GET['fecha1'];
		$fecha2 = $_GET['fecha2'];
	}

	$filtro = 'fecha1=' . $fecha1 . '&fecha2=' . $fecha2;

	$con = 'SELECT * FROM solicitudes
	INNER JOIN empresas ON empid = solempresa 
	INNER JOIN clientes ON cliid = solcliente 
	LEFT JOIN usuarios ON usuid = solususcreen
	LEFT JOIN tcausales ON tcaid = soltcascreen';

	$ord = 'ORDER BY solfecha ASC ;';

	if ($fecha1 != '') 
		$sql = "$con WHERE DATE(solfecha) BETWEEN '$fecha1' AND '$fecha2' $ord";
	else
		$sql = $con . " " . $ord;

	$qry = $db->query($sql);
?>
<!doctype html>
<html>

<head>
	<meta charset="utf-8">
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>

	<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
	<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#tabla').dataTable({
				'bJQueryUI': true,
				'sPaginationType': 'full_numbers',
				'oLanguage': {
					'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
				},
				'bSort': false,
			});
		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Solicitudes</a>
				<div class="mapa_div"></div><a class="current">Reportes</a>
			</article>
			<article id="contenido">
				<h2>Reportes Screen</h2>
				<div class="reporte">
					<a href="excelscreen.php?<?php echo $filtro ?>">
						<img src="<?php echo $r ?>imagenes/iconos/excel.png" title="csv" />
					</a>
				</div>
				<table id="tabla">
					<thead>
						<tr>
							<th title="Empresa">Empresa</th>
							<th title="Numero de solicitud">Solicitud</th>
							<th>Factura</th>
							<th>Cliente</th>
							<th title="Fecha de Screen">F.S</th>
							<th>Estado</th>
							<th>Tipo de Causal</th>
							<th>Causal</th>
							<th>Usuario Screen</th>
							<th>Asesor</th>
						</tr>
					</thead>
					<tbody>
						<?php
						while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
							$row1 = $db->query("SELECT * FROM causales WHERE cautcausal = '" . $row['soltcascreen'] . "' AND cauid = '" . $row['solcauscreen'] . "'")->fetch(PDO::FETCH_ASSOC);
							$causal = "";
							if($row1)
								$causal = $row1['caunombre'];
	
							$qryca = $db->query("SELECT * FROM usuarios WHERE usuid = '" . $row['solasesor'] . "';");
							$rowca = $qryca->fetch(PDO::FETCH_ASSOC);
							// Nombre del asesor
							if($rowca)
								$capacitador = $rowca['usunombre'];
						?>
							<tr>
								<td title="<?php echo $row['empid'] . ' ' . $row['empdigito'] ?>"><?php echo $row['empnombre'] ?></td>
								<td align="center"><?php echo $row['solid'] ?></td>
								<td align="center"><?php echo $row['solfactura'] ?></td>
								<td title="<?php echo $row['cliid'] . ' ' . $row['clidigito'] ?>"><?php echo $row['clinombre'] . ' ' . $row['clinom2'] . ' ' . $row['cliape1'] . ' ' . $row['cliape2'] ?></td>
								<td align="center"><img src="<?php echo $r ?>imagenes/iconos/date.png" title="<?php echo $row['solfechscreen'] ?>" /></td>
								<td align="center"><?php echo $row['solestado'] ?></td>
								<td align="center"><?php echo $row['tcanombre'] ?></td>
								<td align="center"><?php echo $causal ?></td>
								<td align="center"><?php echo $row['usunombre'] ?></td>
								<td align="center"><?php echo $capacitador ?></td>
							</tr>
						<?php
						}
						?>
					</tbody>
				</table>
				<p class="boton">
					<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href='consolscreen.php'">Atras</button>
				</p>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<?php
	if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>';
	elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>';
	?>
	<div id="modal" style="display:none"></div>
	<div id="dialog2" style="display:none"></div>
</body>

</html>