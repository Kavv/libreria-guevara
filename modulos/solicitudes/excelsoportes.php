<?php
$r = '../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php'); 
require($r.'incluir/phpexcel/Classes/PHPExcel.php');

$hoy = date('Y-m-d');



if(isset($_POST['consultar'])){
	$fecha1 = $_POST['fecha1'];
	$fecha2 = $_POST['fecha2'];
	$estado = $_POST['estado'];
}else{
	$fecha1 = $_GET['fecha1'];
	$fecha2 = $_GET['fecha2'];
	$estado = $_GET['estado'];
}

if($fecha1 == '' && $estado == ''){
	$error = 'Debe rellenar al menos un campo.';
	header('Location:consoporte.php?error='.$error);
	exit();
}

$filtro = 'fecha1='.$fecha1.'&fecha2='.$fecha2.'&estado='.$estado;

$con = 'SELECT * FROM soportes 
INNER  JOIN empresas ON empid = soporempresa 
INNER JOIN clientes ON cliid = soporcliente 
INNER JOIN usuarios ON usuid = soporusuario';
$ord = 'ORDER BY soporfechasoporte ASC ;';

if($fecha1 != '' && $estado == '') $sql = "$con WHERE DATE(soporfechasoporte) BETWEEN '$fecha1' AND '$fecha2' $ord";
elseif($fecha1 != '' && $estado != '') $sql = "$con WHERE DATE(soporfechasoporte) BETWEEN '$fecha1' AND '$fecha2'AND soporvalidado = '$estado'  $ord";
elseif($fecha1 == '' && $estado != '') $sql = "$con WHERE soporvalidado = '$estado'  $ord";

$qry = $db->query($sql);

$objPHPExcel = new PHPExcel();
$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN
		)
	)
);
$objPHPExcel->getActiveSheet()->getStyle('A1:I2')->applyFromArray($styleArray);
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:I1')
			->setCellValue('A1', 'LISTADO DE SOPORTES');
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A2', 'ID')
			->setCellValue('B2', 'EMPRESA')	
            ->setCellValue('C2', 'SOLICITUD')
			->setCellValue('D2', 'FACTURA')
			->setCellValue('E2', 'CLIENTE')			
			->setCellValue('F2', 'FECHA DE SOPORTE')
			->setCellValue('G2', 'USUARIO')
            ->setCellValue('H2', 'ESTADO SOPORTE')
			->setCellValue('I2', 'COMENTARIOS')
			;
			
$i = 3;
while($row = $qry->fetch(PDO::FETCH_ASSOC)){
	
	$cliente = $row['clinombre'].' '.$row['clinom2'].' '.$row['cliape1'].' '.$row['cliape2'];
	if($row['soporvalidado'] == 1){ $estado = "REALIZADO";} else if ($row['soporvalidado'] == 0){ $estado = "PENDIENTE";}

	$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':I'.$i)->applyFromArray($styleArray);
	$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('A'.$i, $row['soporid'])
    	->setCellValue('B'.$i, $row['empnombre'])
		->setCellValue('C'.$i, $row['soporsolicitud'])
		->setCellValue('D'.$i, $row['soporfactura'])
		->setCellValue('E'.$i, $cliente)
		->setCellValue('F'.$i, $row['soporfechasoporte'])
		->setCellValue('G'.$i, $row['usunombre'])
    	->setCellValue('H'.$i, $estado)
		->setCellValue('I'.$i, $row['soporcomentarios']);
	$i++;
}
// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Reporte Soportes.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
?>