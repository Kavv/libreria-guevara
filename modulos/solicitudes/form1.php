<?php
$r = '../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
$screen = "";
if (isset($_POST['adinota'])) {
	$empresa = $_POST['empresa'];
	$id = $_POST['id'];
	$nota = trim($_POST['nota']);
	$screen = $_POST['screen'];
	$row = $db->query("INSERT INTO hissolicitudes (hsoempresa, hsosolicitud, hsofecha, hsousuario, hsonota) VALUES ('$empresa', '$id', NOW(), '" . $_SESSION['id'] . "', '$nota')");
	header('Location:form1.php?empresa=' . $empresa . '&id=' . $id . '&screen=' . $screen);
	exit();
} elseif (isset($_POST['insertar'])) {
	$empresa = $_POST['empresa'];
	$id = $_POST['id'];
	$tipo = $_POST['tipo'];
	$fecha = $_POST['fecha'];
	$cliente = $_POST['cliente'];
	$num = $db->query("SELECT * FROM clientes WHERE cliid = '$cliente'")->rowCount();
	if ($num < 1) {
		$sql = "INSERT INTO clientes (cliid, clitide) VALUES ('$cliente', 1)";
		$db->query($sql);
	}
	// Consultamos si la solicitud ya existe en caso de ser asi, no se agregara un registro nuevo
	$qry = $db->query("SELECT * FROM solicitudes WHERE solempresa = '$empresa' AND solid = '$id'");
	if($qry->rowCount() == 0)
		$qry = $db->query("INSERT INTO solicitudes (solempresa, solid, soltipo, solfecha, solcliente, solasesor, solestado) VALUES ('$empresa', '$id', '$tipo', '$fecha', '$cliente', '" . $_SESSION['id'] . "', 'BORRADOR')");
} else {
	$empresa = $_GET['empresa'];
	$id = $_GET['id'];
	@$screen = $_GET['screen'];
}
$row = $db->query("SELECT * FROM (solicitudes INNER JOIN clientes ON solcliente = cliid) INNER JOIN empresas ON solempresa = empid WHERE solempresa = '$empresa' AND solid = '$id'")->fetch(PDO::FETCH_ASSOC);
?>
<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#depresidencia').change(function(event) {
				var id1 = $('#depresidencia').find(':selected').val();
				$('#ciuresidencia').load('<?php echo $r ?>incluir/carga/ciudades.php?id1=' + id1);
			});
			$('#depcomercio').change(function(event) {
				var id1 = $('#depcomercio').find(':selected').val();
				$('#ciucomercio').load('<?php echo $r ?>incluir/carga/ciudades.php?id1=' + id1);
			});
			$('#depadicional').change(function(event) {
				var id1 = $('#depadicional').find(':selected').val();
				$('#ciuadicional').load('<?php echo $r ?>incluir/carga/ciudades.php?id1=' + id1);
			});
			$('.pdf').click(function() {
				newSrc = $(this).attr('data-rel');
				if(validation_file(newSrc) == 200)
					$('#modal').html("<iframe src='" + newSrc + "' width='100%' height='100%'></iframe>");
				else
					$('#modal').html("<spam>El PDF no fue encontrado</spam>");
				
				$('#modal').dialog({
					modal: true,
					width: '600',
					height: '800',
					title: 'PDF de la solicitud'
				});
			});
			$('.historico').click(function() {
				newHref = $(this).attr('data-rel');
				$('#dialog2').load(newHref).dialog({
					modal: true,
					width: 800
				});
			});
			$('.nota').click(function() {
				$('#campo').dialog({
					height: 'auto',
					width: '600px',
					modal: true
				});
			});
		});
	</script>
</head>
<style>
	.pdf, .historico, .nota{
		cursor: pointer;
	}
</style>
<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a class="current">Solicitudes</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="form2.php" method="post">
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-sm-12 col-md-8 col-lg-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">Solicitud - Hoja 1 
							<img src="<?php echo $r ?>imagenes/iconos/pdf.png" class="pdf" data-rel="<?php echo $r . 'pdf/solicitudes/' . $empresa . '/' . $id . '.pdf' ?>" title="Solicitud en PDF" /> 
							<img src="<?php echo $r ?>imagenes/iconos/application_edit.png" class="historico" data-rel="notas.php?empresa=<?php echo $empresa . '&id=' . $id ?>" title="Historia de la solicitud" /> 
							<img src="<?php echo $r ?>imagenes/iconos/add.png" class="nota" title="Agregar nota" />
						</legend>
						<p>
							<img src="<?php echo $r ?>imagenes/logos/<?php echo $empresa . "/" . $row['logo']?>" class="img-fluid" title="<?php echo $row['empnombre'] ?>">
							<input type="hidden" name="empresa" value="<?php echo $empresa ?>">
							<div align="center">Solicitud</div>
							<div align="center"><input type="text" name="id" class="pedido" value="<?php echo $id ?>" readonly /></div>
							<div align="center">Fecha</div>
							<div align="center"><input type="text" class="fecha" value="<?php echo $row['solfecha'] ?>" readonly /></div>
						</p>
						<br>
						<br>
						<?php if ($screen != '') { ?>
							<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12">
								<legend class="ui-widget ui-widget-header ui-corner-all">Observaciones</legend>
								<p>
									<textarea placeholder="Observaciones..." name="observacion" id="observacion" rows="7" class="form-control">
										<?php
											if ($row['solobservacion'] != '') {
												echo $row['solobservacion'];
											}
										?>
									</textarea>
								</p>
							</fieldset>
							<br>
						<?php } ?>
						<?php if ($row['clidigito'] == '') { ?>
							<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12">
								<legend class="ui-widget ui-widget-header ui-corner-all">Datos personales</legend>
								<p>
									<input type="hidden" name="screen" value="<?php echo $screen ?>" />
									<label>Identificacion: </label>
									<input type="text" name="cliente" class="nombre2" value="<?php echo $row['cliid'] ?>" readonly />
									<label>Nombre 1: </label>
									<input readonly type="text" name="nombre" class="nombre2 validate[required, custom[onlyLetterSp]] text-input" value="<?php echo $row['clinombre'] ?>" title="Digite primer nombre" />
									<label>Nombre 2: </label>
									<input readonly type="text" name="nom2" class="nombre2" value="<?php echo $row['clinom2'] ?>" title="Digite segundo nombre" />
								</p>
								<p>
									<label>Apellido 1: </label>
									<input readonly type="text" name="ape1" class="nombre2 validate[required, custom[onlyLetterSp]] text-input" value="<?php echo $row['cliape1'] ?>" title="Digite primer apellido" />
									<label>Apellido 2: </label>
									<input readonly type="text" name="ape2" class="nombre2" value="<?php echo $row['cliape2'] ?>" title="Digite segundo apellido" />
									<label>Edad: </label>
									<input readonly type="text" name="edad" class="edad validate[custom[onlyNumberSp]] text-input" value="<?php echo $row['cliedad'] ?>" title="Digite edad" />
								</p>
								<p>
									<label>E-mail: </label>
									<input type="text" name="email" class="email validate[custom[email]] text-input" value="<?php echo $row['cliemail'] ?>" />
									<label>Celular: </label>
									<input type="text" name="celular" class="telefono validate[required, custom[phone]] text-input" value="<?php echo $row['clicelular'] ?>" title="Digite numero celular" />
								</p>
							</fieldset>
						<?php
						} else {
						?>
							<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12">
								<legend class="ui-widget ui-widget-header ui-corner-all">Datos empresariales</legend>
								<p>
									<input type="hidden" name="screen" value="<?php echo $screen ?>" />
									<label>Nit: </label><input type="text" name="cliente" class="id" value="<?php echo $row['cliid'] ?>" readonly /> <input type="text" name="dv" id="dv" size="1" value="<?php echo $row['clidigito'] ?>" readonly />
									<label>Razon social: </label><input type="text" name="nombre" class="nombre validate[required, custom[onlyLetterSp]] text-input" value="<?php echo $row['clinombre'] ?>" title="Digite la razon social" />
									<label>Tiempo: </label><input type="text" name="edad" class="edad validate[required, custom[onlyNumberSp]] text-input" value="<?php echo $row['cliedad'] ?>" title="Digite edad" />
								</p>
								<p>
									<label>E-mail: </label><input type="text" name="email" class="email validate[required, custom[email]] text-input" value="<?php echo $row['cliemail'] ?>" />
									<label>Celular: </label><input type="text" name="celular" class="telefono validate[required, custom[phone]] text-input" value="<?php echo $row['clicelular'] ?>" title="Digite numero celular" />
								</p>
							</fieldset>
						<?php } ?>
						<br>
						<?php if ($row['clidigito'] == '') { ?>
							<!-- Si el cliente no es una empresa se solicita esto -->
							<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12">
								<legend class="ui-widget ui-widget-header ui-corner-all">Datos ubicacion residencia</legend>
								<p>
									<label>Departamento:</label>
									<select id="depresidencia" name="depresidencia" class="validate[required] text-input">
										<?php
										if ($row['clidepresidencia'] != '') {
											$row2 = $db->query("SELECT * FROM departamentos WHERE depid = '" . $row['clidepresidencia'] . "'")->fetch(PDO::FETCH_ASSOC);
											echo '<option value=' . $row['clidepresidencia'] . '>' . $row2['depnombre'] . '</option>';
										}
										echo '<option value="">SELECCIONE</option>';
										$qry = $db->query("SELECT * FROM departamentos WHERE depid <> '" . $row['clidepresidencia'] . "' ORDER BY depnombre");
										while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
											echo '<option value=' . $row2['depid'] . '>' . $row2['depnombre'] . '</option>';
										}
										?>
									</select>
									<label>Ciudad: </label>
									<select name="ciuresidencia" id="ciuresidencia" class="validate[required] text-input">
										<?php
										if ($row['cliciuresidencia'] != '') {
											$row2 = $db->query("SELECT * FROM ciudades WHERE ciudepto = '" . $row['clidepresidencia'] . "' AND ciuid = '" . $row['cliciuresidencia'] . "'")->fetch(PDO::FETCH_ASSOC);
											echo '<option value=' . $row['cliciuresidencia'] . '>' . $row2['ciunombre'] . '</option>';
										}
										echo '<option value="">SELECCIONE</option>';
										$qry = $db->query("SELECT * FROM ciudades WHERE ciudepto = '" . $row['clidepresidencia'] . "' AND ciuid <> '" . $row['cliciuresidencia'] . "' ORDER BY ciunombre");
										while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
											echo '<option value=' . $row2['depid'] . '>' . $row2['depnombre'] . '</option>';
										}
										?>
									</select>
								</p>
								<p>
									<label>Barrio: </label><input type="text" name="barresidencia" class="barrio validate[required] text-input" value="<?php echo $row['clibarresidencia'] ?>" />
									<label>Direccion: </label><input type="text" name="dirresidencia" class="direccion validate[required] text-input" value="<?php echo $row['clidirresidencia'] ?>" />
								</p>
								<p>
									<label>Telefono: </label><input type="text" name="telresidencia" class="telefono validate[required, custom[phone]] text-input" value="<?php echo $row['clitelresidencia'] ?>" />
								</p>
							</fieldset>
							<br>
							<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12">
								<legend class="ui-widget ui-widget-header ui-corner-all">Datos ubicacion comercial</legend>
								<p>
									<label>Departamento:</label>
									<select id="depcomercio" name="depcomercio" class=" text-input">
										<?php
										if ($row['clidepcomercio'] != '') {
											$row2 = $db->query("SELECT * FROM departamentos WHERE depid = '" . $row['clidepcomercio'] . "'")->fetch(PDO::FETCH_ASSOC);
											echo '<option value=' . $row['clidepcomercio'] . '>' . $row2['depnombre'] . '</option>';
										}
										echo '<option value="">SELECCIONE</option>';
										$qry = $db->query("SELECT * FROM departamentos WHERE depid <> '" . $row['clidepcomercio'] . "' ORDER BY depnombre");
										while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
											echo '<option value=' . $row2['depid'] . '>' . $row2['depnombre'] . '</option>';
										}
										?>
									</select>
									<label>Ciudad:</label>
									<select name="ciucomercio" id="ciucomercio" class=" text-input">
										<?php
										if ($row['cliciucomercio'] != '') {
											$row2 = $db->query("SELECT * FROM ciudades WHERE ciudepto = '" . $row['clidepcomercio'] . "' AND ciuid = '" . $row['cliciucomercio'] . "'")->fetch(PDO::FETCH_ASSOC);
											echo '<option value=' . $row['cliciucomercio'] . '>' . $row2['ciunombre'] . '</option>';
										}
										echo '<option value="">SELECCIONE</option>';
										$qry = $db->query("SELECT * FROM ciudades WHERE ciudepto = '" . $row['clidepcomercio'] . "' AND ciuid <> '" . $row['cliciucomercio'] . "' ORDER BY ciunombre");
										while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
											echo '<option value=' . $row2['depid'] . '>' . $row2['depnombre'] . '</option>';
										}
										?>
									</select>
								</p>
								<p>
									<label>Barrio: </label><input type="text" name="barcomercio" class="barrio text-input" value="<?php echo $row['clibarcomercio'] ?>" size="20" />
									<label>Direccion: </label><input type="text" name="dircomercio" class="direccion text-input" value="<?php echo $row['clidircomercio'] ?>" />
								</p>
								<p>
									<label>Telefono: </label><input type="text" name="telcomercio" class="telefono validate[custom[phone]] text-input" value="<?php echo $row['clitelcomercio'] ?>" />
								</p>
							</fieldset>
							<br>
							<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12">
								<legend class="ui-widget ui-widget-header ui-corner-all">Datos ubicacion adicional</legend>
								<p>
									<label>Departamento:</label>
									<select id="depadicional" name="depadicional">
										<?php
										if ($row['clidepadicional'] != '') {
											$row2 = $db->query("SELECT * FROM departamentos WHERE depid = '" . $row['clidepadicional'] . "'")->fetch(PDO::FETCH_ASSOC);
											echo '<option value=' . $row['clidepadicional'] . '>' . $row2['depnombre'] . '</option>';
										}
										echo '<option value="">SELECCIONE</option>';
										$qry = $db->query("SELECT * FROM departamentos WHERE depid <> '" . $row['clidepadicional'] . "' ORDER BY depnombre");
										while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
											echo '<option value=' . $row2['depid'] . '>' . $row2['depnombre'] . '</option>';
										}
										?>
									</select>
									<label>Ciudad:</label>
									<select name="ciuadicional" id="ciuadicional">
										<?php
										if ($row['cliciuadicional'] != '') {
											$row2 = $db->query("SELECT * FROM ciudades WHERE ciudepto = '" . $row['clidepadicional'] . "' AND ciuid = '" . $row['cliciuadicional'] . "'")->fetch(PDO::FETCH_ASSOC);
											echo '<option value=' . $row['cliciuadicional'] . '>' . $row2['ciunombre'] . '</option>';
										}
										echo '<option value="">SELECCIONE</option>';
										$qry = $db->query("SELECT * FROM ciudades WHERE ciudepto = '" . $row['clidepadicional'] . "' AND ciuid <> '" . $row['cliciuadicional'] . "' ORDER BY ciunombre");
										while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
											echo '<option value=' . $row2['depid'] . '>' . $row2['depnombre'] . '</option>';
										}
										?>
									</select>
								</p>
								<p>
									<label>Barrio: </label><input type="text" name="baradicional" value="<?php echo $row['clibaradicional'] ?>" size="20" />
									<label>Direccion: </label><input type="text" name="diradicional" class="direccion" value="<?php echo $row['clidiradicional'] ?>" />
								</p>
								<p>
									<label>Telefono: </label><input type="text" name="teladicional" class="telefono validate[custom[phone]] text-input" value="<?php echo $row['cliteladicional'] ?>" />
								</p>
							</fieldset>
							<br>
							<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12">
								<legend class="ui-widget ui-widget-header ui-corner-all">Datos adicionales</legend>
								<p>
									<label class="label">Empresa: </label>
									<input type="text" name="trabajo" class="empresa text-input" value="<?php echo $row['cliempresa'] ?>" />
									<label class="label">Cargo: </label>
									<input type="text" name="cargo" class="cargo text-input" value="<?php echo $row['clicargo'] ?>" />
								</p>
								<p>
									<label>Antiguedad <em>(En meses)</em> </label>
									<input type="text" name="antiguedad" class="validate[custom[onlyNumberSp]] text-input" value="<?php echo $row['cliantiguedad'] ?>" style="" title="Digite la antiguedad en la empresa en meses" placeholder="Antiguedad en meses"/>
									<label class="label">Salario: </label>
									<input type="text" class="valor validate[custom[onlyNumberSp]] text-input" name="salario" value="<?php echo $row['clisalario'] ?>" title="Digite el salario" />
									
									<label>Estado civil: </label>
									<label class="not-w mr-0">SOLTERO(a) </label>
									<input type="radio" name="estado" class="mr-2" value="SOLTERO(A)" <?php if ($row['cliestado'] == 'SOLTERO(A)') echo 'checked' ?> />
									<label class="not-w mr-0">CASADO(a) </label>
									<input type="radio" class="mr-2" name="estado" value="CASADO(A)" <?php if ($row['cliestado'] == 'CASADO(A)') echo 'checked' ?> />
								</p>
								<p>
									<label>Vivienda: </label>
									<label class="not-w mr-0">PROPIA</label> </label>
									<input type="radio" class="mr-2" name="vivienda" class="" value="PROPIA" <?php if ($row['clivivienda'] == 'PROPIA') echo 'checked' ?> />
									<label class="not-w mr-0">ALQUILER </label>
									<input type="radio" class="mr-2" name="vivienda" value="ALQUILER" <?php if ($row['clivivienda'] == 'ALQUILER') echo 'checked' ?> />
									<label class="not-w mr-0">FAMILIAR </label>
									<input type="radio" class="mr-2" name="vivienda" value="FAMILIAR" <?php if ($row['clivivienda'] == 'FAMILIAR') echo 'checked' ?> />
									
									<label>Hijos: </label>
									<input type="text" name="hijos" class="edad validate[custom[onlyNumberSp]]" value="<?php echo $row['clihijos'] ?>" title="Digite el numero de hijos" /></td>
								</p>
						<?php
						} else {
						?>
								<!-- Si es una empresa se solicita esto -->
								<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12">
									<legend class="ui-widget ui-widget-header ui-corner-all">Datos ubicacion empresarial</legend>
									<p>
										<label>Departamento:</label>
										<select id="depresidencia" name="depresidencia" class="validate[required] text-input">
											<?php
											if ($row['clidepresidencia'] != '') {
												$row2 = $db->query("SELECT * FROM departamentos WHERE depid = '" . $row['clidepresidencia'] . "'")->fetch(PDO::FETCH_ASSOC);
												echo '<option value=' . $row['clidepresidencia'] . '>' . $row2['depnombre'] . '</option>';
											}
											echo '<option value="">SELECCIONE</option>';
											$qry = $db->query("SELECT * FROM departamentos WHERE depid <> '" . $row['clidepresidencia'] . "' ORDER BY depnombre");
											while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
												echo '<option value=' . $row2['depid'] . '>' . $row2['depnombre'] . '</option>';
											}
											?>
										</select>
										<label>Ciudad: </label>
										<select name="ciuresidencia" id="ciuresidencia" class="validate[required] text-input">
											<?php
											if ($row['cliciuresidencia'] != '') {
												$row2 = $db->query("SELECT * FROM ciudades WHERE ciudepto = '" . $row['clidepresidencia'] . "' AND ciuid = '" . $row['cliciuresidencia'] . "'")->fetch(PDO::FETCH_ASSOC);
												echo '<option value=' . $row['cliciuresidencia'] . '>' . $row2['ciunombre'] . '</option>';
											}
											echo '<option value="">SELECCIONE</option>';
											$qry = $db->query("SELECT * FROM ciudades WHERE ciudepto = '" . $row['clidepresidencia'] . "' AND ciuid <> '" . $row['cliciuresidencia'] . "' ORDER BY ciunombre");
											while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
												echo '<option value=' . $row2['depid'] . '>' . $row2['depnombre'] . '</option>';
											}
											?>
										</select>
									</p>
									<p>
										<label>Barrio: </label><input type="text" name="barresidencia" class="barrio validate[required] text-input" value="<?php echo $row['clibarresidencia'] ?>" />
										<label>Direccion: </label><input type="text" name="dirresidencia" class="direccion validate[required] text-input" value="<?php echo $row['clidirresidencia'] ?>" />
									</p>
									<p>
										<label>Telefono: </label><input type="text" name="telresidencia" class="telefono validate[required, custom[phone]] text-input" value="<?php echo $row['clitelresidencia'] ?>" />
									</p>
								</fieldset>
								<br>
								<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12">
									<legend class="ui-widget ui-widget-header ui-corner-all">Datos ubicacion adicional</legend>
									<p>
										<label>Departamento:</label>
										<select id="depcomercio" name="depcomercio">
											<?php
											if ($row['clidepcomercio'] != '') {
												$row2 = $db->query("SELECT * FROM departamentos WHERE depid = '" . $row['clidepcomercio'] . "'")->fetch(PDO::FETCH_ASSOC);
												echo '<option value=' . $row['clidepcomercio'] . '>' . $row2['depnombre'] . '</option>';
											}
											echo '<option value="">SELECCIONE</option>';
											$qry = $db->query("SELECT * FROM departamentos WHERE depid <> '" . $row['clidepcomercio'] . "' ORDER BY depnombre");
											while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
												echo '<option value=' . $row2['depid'] . '>' . $row2['depnombre'] . '</option>';
											}
											?>
										</select>
										<label>Ciudad:</label>
										<select name="ciucomercio" id="ciucomercio">
											<?php
											if ($row['cliciucomercio'] != '') {
												$row2 = $db->query("SELECT * FROM ciudades WHERE ciudepto = '" . $row['clidepcomercio'] . "' AND ciuid = '" . $row['cliciucomercio'] . "'")->fetch(PDO::FETCH_ASSOC);
												echo '<option value=' . $row['cliciucomercio'] . '>' . $row2['ciunombre'] . '</option>';
											}
											echo '<option value="">SELECCIONE</option>';
											$qry = $db->query("SELECT * FROM ciudades WHERE ciudepto = '" . $row['clidepcomercio'] . "' AND ciuid <> '" . $row['cliciucomercio'] . "' ORDER BY ciunombre");
											while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
												echo '<option value=' . $row2['depid'] . '>' . $row2['depnombre'] . '</option>';
											}
											?>
										</select>
									</p>
									<p>
										<label>Barrio: </label>
										<input type="text" name="barcomercio" class="barrio" value="<?php echo $row['clibarcomercio'] ?>" size="20" />
										<label>Direccion: </label>
										<input type="text" name="dircomercio" class="direccion" value="<?php echo $row['clidircomercio'] ?>" />
									</p>
									<p>
										<label>Telefono: </label>
										<input type="text" name="telcomercio" class="telefono" value="<?php echo $row['clitelcomercio'] ?>" />
									</p>
								</fieldset>
							</fieldset>
						<?php } ?>
					</fieldset>
					<p class="boton">
						<?php
						if ($screen == '1') echo '<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href = \'screen.php\'">atras</button> ';
						?>
						<button type="submit" class="btn btn-primary btnsiguiente" name="siguiente" value="siguiente">siguiente</button>
					</p>
					</fieldset>
				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<div id="campo" title="Agregar historico" style="display:none">
		<form id="form" name="form" action="form1.php" method="post">
			<input type="hidden" name="empresa" value="<?php echo $empresa ?>">
			<input type="hidden" name="id" value="<?php echo $id ?>">
			<input type="hidden" name="screen" value="<?php echo $screen ?>">
			<input type="text" name="nota" required title="Digite la nota" />
			<button type="submit" class="btn btn-success btn-block btnmas" name="adinota" value="adinota">Agregar</button>
		</form>
	</div>
	<div id="modal" style="display:none"></div>
	<div id="dialog2" style="display:none"></div>
</body>

</html>