<?php
	$r = '../../';
	require($r.'incluir/session.php');
	require($r.'incluir/connection.php');
	require($r.'incluir/phpexcel/Classes/PHPExcel.php');

	$hoy = date('Y-m-d');


	if(isset($_POST['consultar'])){

		$fecha1 = $_POST['fecha1'];
		$fecha2 = $_POST['fecha2'];
	} elseif(isset($_GET['fecha1'])) {
		$fecha1 = $_GET['fecha1'];
		$fecha2 = $_GET['fecha2'];
	}

	$filtro = 'fecha1='.$fecha1.'&fecha2='.$fecha2;

	$con = 'SELECT * FROM solicitudes
	INNER JOIN empresas ON empid = solempresa 
	INNER JOIN clientes ON cliid = solcliente 
	LEFT JOIN usuarios ON usuid = solususcreen
	LEFT JOIN tcausales ON tcaid = soltcascreen';
	$ord = 'ORDER BY solfecha ASC ;';

	if ($fecha1 != '') 
		$sql = "$con WHERE DATE(solfecha) BETWEEN '$fecha1' AND '$fecha2' $ord";
	else
		$sql = $con . " " . $ord;

	$qry = $db->query($sql);

	$objPHPExcel = new PHPExcel();
	$styleArray = array(
		'borders' => array(
			'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN
			)
		)
	);
	$objPHPExcel->getActiveSheet()->getStyle('A1:J2')->applyFromArray($styleArray);
	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:J1')
				->setCellValue('A1', 'REPORTE SCREEN');
	$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A2', 'NOMBRE EMPRESA')
				->setCellValue('B2', 'NUMERO SOLICITUD')	
				->setCellValue('C2', 'FACTURA')
				->setCellValue('D2', 'CLIENTE')
				->setCellValue('E2', 'FECHA SCREEN')			
				->setCellValue('F2', 'ESTADO')
				->setCellValue('G2', 'TIPO DE CAUSAL')
				->setCellValue('H2', 'CAUSAL')
				->setCellValue('I2', 'USUARIO SCREEN')
				->setCellValue('J2', 'ASESOR')
				;
				
	$i = 3;
	while($row = $qry->fetch(PDO::FETCH_ASSOC)){
		
		$cliente = $row['clinombre'].' '.$row['clinom2'].' '.$row['cliape1'].' '.$row['cliape2'];

		$row1 = $db->query("SELECT * FROM causales WHERE cautcausal = '".$row['soltcascreen']."' AND cauid = '".$row['solcauscreen']."'")->fetch(PDO::FETCH_ASSOC);
		$causal = $rowca = $fecha_screen = "";
		if($row1)
			$causal = $row1['caunombre'];

		$qryca = $db->query("SELECT * FROM usuarios WHERE usuid = '".$row['solasesor']."';");
		$rowca = $qryca->fetch(PDO::FETCH_ASSOC);
		if($rowca)
			$capacitador = $rowca['usunombre'];

		if (empty($capacitador)) {$capacitador = "";}
		
		$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':J'.$i)->applyFromArray($styleArray);
		$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A'.$i, $row['empnombre'])
			->setCellValue('B'.$i, $row['solid'])
			->setCellValue('C'.$i, $row['solfactura'])
			->setCellValue('D'.$i, $cliente)
			->setCellValue('E'.$i, $row['solfechscreen']) 
			->setCellValue('F'.$i, $row['solestado'])
			->setCellValue('G'.$i, $row['tcanombre'])
			->setCellValue('H'.$i, $causal)
			->setCellValue('I'.$i, $row['usunombre'])
			->setCellValue('J'.$i, $capacitador);
		$i++;
	}
	// Redirect output to a client’s web browser (Excel2007)
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="Reporte Screen.xlsx"');
	header('Cache-Control: max-age=0');
	// If you're serving to IE 9, then the following may be needed
	header('Cache-Control: max-age=1');

	// If you're serving to IE over SSL, then the following may be needed
	header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
	header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header ('Pragma: public'); // HTTP/1.0

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');
?>