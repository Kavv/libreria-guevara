<?php
// LA ACCION PRINCIPAL ES ESTA PAGINA ES CONSULTAR UNA SOLICITUD
$r = '../../';
//INCLUIR SESION Y CONECCION
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
?>
<!doctype html>
<html>

<head>

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#fecha1').datepicker({ // VALIDACION DE LA FECHA UNO
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
				maxDate: '+0D',
				onClose: function(selectedDate) {
					$('#fecha2').datepicker('option', 'minDate', selectedDate);
				}
			}).keypress(function(event) {
				event.preventDefault()
			});
			$('#fecha2').datepicker({ // VALIDACION DE LA FECHA DOS
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
				maxDate: '+0D',
				onClose: function(selectedDate) {
					$('#fecha1').datepicker('option', 'maxDate', selectedDate);
				}
			}).keypress(function(event) {
				event.preventDefault()
			});
			
			/* Valida que ambas fechas sean especificadas */
			$('#fecha1').change(function(){
				both_date();
			});
			
			$('#fecha2').change(function(){
				both_date();
			});
			function both_date()
			{
				if($('#fecha2').val() != "" || $('#fecha1').val() != "")
				{
					$('#fecha1').attr("required",true);
					$('#fecha2').attr("required",true);
				}
				else
				{
					$('#fecha1').removeAttr("required",false);
					$('#fecha2').removeAttr("required",false);
				}
			}

		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
		<?php require($r . 'incluir/src/menu.php') ?>
		<!-- INCLUIMOS MENU PRINCIPAL -->
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Solicitudes</a>
				<div class="mapa_div"></div><a class="current">Consultar</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="listar.php" method="post">
					<!-- ENVIO FORMULARIO POR POST A LISTAR.PHP -->
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">Busqueda de solicitudes</legend>
						<p>
							<label for="empresa">Empresa:</label>
							<select id="empresa" name="empresa">
								<!-- CAMPO EMPRESA -->
								<option value="">TODOS</option>
								<?php
								$qry = $db->query("SELECT * FROM empresas ORDER BY empnombre");
								while ($row = $qry->fetch(PDO::FETCH_ASSOC))
									echo '<option value=' . $row['empid'] . '>' . $row['empnombre'] . '</option>'; // MOSTRAR EMPRESAS EN UN OPTION DENTRO DE UN SELECT
								?>
							</select>
						</p>
						<p>
							<label for="id">Solicitud:</label>
							<input type="text" name="id" class="pedido validate[custom[onlyNumberSp]] text-input" title="Digite numero de solicitud" /> <!-- CAMPO SOLICITUD -->
						</p>
						<p>
							<label for="cliente">Cliente:</label>
							<input type="text" name="cliente" class="id validate[custom[onlyNumberSp]] text-input" title="Digite la identificacion del cliente" /> <!-- CAMPO CLIENTE -->
						</p>
						<p>
							<label for="asesor">Asesor:</label>
							<select name="asesor" required>
								<!-- CAMPO ASESOR EN UN SELECT -->
								<?php
									echo '<option value="TODOS">TODOS</option>';
									$qry = $db->query("SELECT * FROM usuarios WHERE usuasesor = 1 ORDER BY usunombre"); // QRY PARA BUSCAR TODOS LOS ASESORES
									while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
										echo '<option value=' . $row['usuid'] . '>' . $row['usunombre'] . '</option>'; // ASESORES DENTRO DE UN OPTION
									}
								?>
							</select>
						</p>
						<p>
							<label for="estado">Estado:</label>
							<select name="estado">
								<!-- CAMPO ESTADO -->
								<option value="">TODOS</option>
								<option value="BORRADOR">BORRADOR</option>
								<option value="PROCESO">PROCESO</option>
								<option value="APROBADO SCREEN">APROBADO</option>
								<option value="FACTURADO">FACTURADO</option>
								<option value="BANDEJA">BANDEJA</option>
								<option value="RECHAZADO SCREEN">RECHAZADO SCREEN</option>
								<option value="RECHAZADO ENTREGA">RECHAZADO ENTREGA</option>
								<option value="ANULADA">ANULADA</option>
								<option value="CANCELADO">CANCELADO</option>
								<option value="ENVIADO">ENVIADO</option>
								<option value="ENTREGADO">ENTREGADO</option>
							</select>
						</p>
						<p>
							<label for="rango">Fechas:</label> <!-- CAMPOS FECHA -->
							<input type="text" id="fecha1" name="fecha1" class="fecha" /> <input type="text" id="fecha2" name="fecha2" class="fecha" />
						</p>
						<p class="boton">
							<button type="submit" class="btn btn-primary btnconsulta" name="consultar" value="consultar">Consultar</button><!-- BOTON CONSULTAR -->
						</p>
						</p>
					</fieldset>
				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
		<!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
	</section>
	<?php
	if (isset($_GET['error'])) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['error'] . '</div>'; // MENSAJE MODAL ERROR
	?>
</body>

</html>