<?php
$r = '../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
require($r.'incluir/phpexcel/Classes/PHPExcel.php');

$hoy = date('Y-m-d');

	$empresa = $_GET['empresa'];
	$id = $_GET['id'];
	$cliente = $_GET['cliente'];
	$asesor = $_GET['asesor'];
	$estado = $_GET['estado'];
	$fecha1 = $_GET['fecha1'];
	$fecha2 = $_GET['fecha2'];

$filtro = 'empresa='.$empresa.'&id='.$id.'&cliente='.$cliente.'&asesor='.$asesor.'&estado='.$estado.'&fecha1='.$fecha1.'&fecha2='.$fecha2;
$con = 'SELECT * FROM ((solicitudes INNER JOIN empresas ON solempresa = empid) LEFT JOIN clientes ON solcliente = cliid) INNER JOIN usuarios ON solasesor = usuid';
$ord = 'ORDER BY solfecha DESC';

if ($empresa != '' && $id == '' && $cliente == '' && $asesor == '' && $fecha1 == '' && $estado == '') $sql = "$con WHERE solempresa LIKE '%$empresa%' $ord";
elseif ($empresa == '' && $id != '' && $cliente == '' && $asesor == '' && $fecha1 == '' && $estado == '') $sql = "$con WHERE solid LIKE '%$id%' $ord";
elseif ($empresa == '' && $id == '' && $cliente != '' && $asesor == '' && $fecha1 == '' && $estado == '') $sql = "$con WHERE solcliente LIKE '%$cliente%' $ord";
elseif ($empresa == '' && $id == '' && $cliente == '' && $asesor != '' && $fecha1 == '' && $estado == '') $sql = "$con WHERE solasesor LIKE '%$asesor%' $ord";
elseif ($empresa == '' && $id != '' && $cliente == '' && $asesor != '' && $fecha1 == '' && $estado == '') $sql = "$con WHERE solid LIKE '%$id%' AND solasesor LIKE '%$asesor%' $ord";
elseif ($empresa == '' && $id == '' && $cliente == '' && $asesor == '' && $fecha1 != '' && $estado == '') $sql = "$con WHERE solfecha BETWEEN '$fecha1' AND '$fecha2' $ord";
elseif ($empresa == '' && $id == '' && $cliente == '' && $asesor == '' && $fecha1 == '' && $estado != '') $sql = "$con WHERE solestado = '$estado' $ord";
elseif ($empresa == '' && $id == '' && $cliente == '' && $asesor == '' && $fecha1 != '' && $estado != '') $sql = "$con WHERE solfecha BETWEEN '$fecha1' AND '$fecha2' AND solestado = '$estado' $ord";
elseif ($empresa != '' && $id == '' && $cliente == '' && $asesor == '' && $fecha1 == '' && $estado != '') $sql = "$con WHERE solempresa LIKE '%$empresa%' AND solestado = '$estado' $ord";
elseif ($empresa != '' && $id != '' && $cliente == '' && $asesor == '' && $fecha1 == '' && $estado == '') $sql = "$con WHERE solempresa LIKE '%$empresa%' AND solid LIKE '%$id%' $ord";
elseif ($empresa == '' && $id == '' && $cliente == '' && $asesor != '' && $fecha1 != '' && $estado == '') $sql = "$con WHERE solasesor LIKE '%$asesor%' AND solfecha BETWEEN '$fecha1' AND '$fecha2' $ord";
elseif ($empresa == '' && $id == '' && $cliente == '' && $asesor != '' && $fecha1 == '' && $estado != '') $sql = "$con WHERE solasesor LIKE '%$asesor%' AND solestado = '$estado' $ord";
elseif ($empresa == '' && $id == '' && $cliente == '' && $asesor != '' && $estado != '' && $fecha1 != '') $sql = "$con WHERE solasesor LIKE '%$asesor%' AND solfecha BETWEEN '$fecha1' AND '$fecha2' AND solestado = '$estado' $ord";
elseif ($empresa != '' && $id == '' && $cliente == '' && $asesor != '' && $fecha1 == '' && $estado == '') $sql = "$con WHERE solempresa LIKE '%$empresa%' AND solasesor LIKE '%$asesor%' $ord";
$qry = $db->query($sql);


$objPHPExcel = new PHPExcel();
$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN
		)
	)
);
$objPHPExcel->getActiveSheet()->getStyle('A1:AB2')->applyFromArray($styleArray);
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:AB1')
			->setCellValue('A1', 'LISTADO DE SOLICITUDES');
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A2', 'NOMBRE EMPRESA')
			->setCellValue('B2', 'NUMERO SOLICITUD')	
            ->setCellValue('C2', 'NOMBRE CLIENTE')
			->setCellValue('D2', 'ASESOR')
			->setCellValue('E2', 'JEFE ASESOR')			
			->setCellValue('F2', 'FECHA DE SOLICITUD')
			->setCellValue('G2', 'ESTADO DE SOLICITUD')
            ->setCellValue('H2', 'TIPO DE CAUSAL RECHAZO SCREEN')
			->setCellValue('I2', 'CAUSAL RECHAZO SCREN')
            ->setCellValue('J2', 'TIPO DE CAUSAL RECHAZO ENTREGA')
			->setCellValue('K2', 'CAUSAL RECHAZO ENTREGA')
			->setCellValue('L2', 'NUMERO FACTURA')
			->setCellValue('M2', 'FECHA DE DESPACHO')
			->setCellValue('N2', 'HORA DE DESPACHO')	
			->setCellValue('O2', 'FECHA DE ENTREGA')	
			->setCellValue('P2', 'HORA DE ENTREGA')	
			->setCellValue('Q2', 'PLANILLA')	
			->setCellValue('R2', 'GUIA')	
			->setCellValue('S2', 'EMPRESA DONDE TRABAJA EL CLIENTE')	
			->setCellValue('T2', 'DEPARTAMENTO DEL CLIENTE')	
			->setCellValue('U2', 'CIUDAD DEL CLIENTE')	
			->setCellValue('V2', 'RELACIONISTA')	
			->setCellValue('W2', 'GERENTE RELACIONISTA')
			->setCellValue('X2', 'AÑO DE LA SOLICITUD')
			->setCellValue('Y2', 'MES DE LA SOLICITUD')
			->setCellValue('Z2', 'SEMANA DE LA SOLICITUD')
			->setCellValue('AA2', 'BASE DE LA SOLICITUD')
			->setCellValue('AB2', 'SCREEN REALIZADO POR')
			;
			
$i = 3;
while($row = $qry->fetch(PDO::FETCH_ASSOC)){
	
	$cliente = $row['clinombre'].' '.$row['clinom2'].' '.$row['cliape1'].' '.$row['cliape2'];
	
	if($row['solestado'] == 'RECHAZADO SCREEN'){
		$row2 = $db->query("SELECT * FROM tcausales INNER JOIN causales ON tcaid = cautcausal WHERE tcaid = ".$row['soltcascreen']." AND cauid = ".$row['solcauscreen']);
		if($row2)
		{
			$row2 = $row2->fetch(PDO::FETCH_ASSOC);
			$causal_screen = $row2['caunombre'];
			$tipo_causal_screen = $row2['tcanombre'];
		}	
	}elseif($row['solestado'] == 'RECHAZADO ENTREGA'){
		$row2 = $db->query("SELECT * FROM tcausales INNER JOIN causales ON tcaid = cautcausal WHERE tcaid = ".$row['soltcadescarga']." AND cauid = ".$row['solcaudescarga']);
		if($row2)
		{
			$row2 = $row2->fetch(PDO::FETCH_ASSOC);
			$tipo_causal_entrega = $row2['tcanombre'];
			$causal_entrega = $row2['caunombre'];
		}
	}else {
		$tipo_causal_entrega = "";
		$causal_entrega = "";
		$tipo_causal_screen = "";
		$causal_screen = "";
	}
	
	if (!empty($row['clidepresidencia'])){
	$qrydepar = $db->query("SELECT * FROM departamentos WHERE depid = '".$row['clidepresidencia']."';");
	$rowdepar = $qrydepar->fetch(PDO::FETCH_ASSOC);
	$numdepar = $qrydepar->rowCount();
		if ($numdepar == 1){
		$departamento = $rowdepar['depnombre'];
		$qryciu = $db->query("SELECT * FROM ciudades WHERE ciudepto = '".$rowdepar['depid']."' AND ciuid = '".$row['cliciuresidencia']."' ;");
		$rowciu = $qryciu->fetch(PDO::FETCH_ASSOC);
		$numciu = $qryciu->rowCount();
			if ($numciu == 1){
			$ciudad = $rowciu['ciunombre'];
			} else {
			$ciudad = "NONE";
			} 
		} else {
		$departamento = "NONE";
		$ciudad = "NONE";
		}
	} else {
	$departamento = "NONE";
	$ciudad = "NONE";
	}
	
	if (!empty($row['solrelacionista'])){
		$qryrela = $db->query("SELECT * FROM usuarios WHERE usuid = '".$row['solrelacionista']."';");
		$rowrela = $qryrela->fetch(PDO::FETCH_ASSOC);
		$numrela = $qryrela->rowCount();
		if ($numrela == 1){
		$relacionista = $rowrela['usunombre'];
			if (!empty($rowrela['usujefecallcenter'])){
				$qryjeferela = $db->query("SELECT * FROM usuarios WHERE usuid = '".$rowrela['usujefecallcenter']."';");
				$rowjeferela = $qryjeferela->fetch(PDO::FETCH_ASSOC);
				$numjeferela = $qryjeferela->rowCount();
				if ($numjeferela == 1){
					$jeferelacionista = $rowjeferela['usunombre'];
				} else {
					$jeferelacionista = "NONE";	
				}
			} else {
			$jeferelacionista = "NONE";	
			}
		} else {
		$relacionista = "NONE";
		$jeferelacionista = "NONE";
		}
	} else {
		$relacionista = "NONE";
		$jeferelacionista = "NONE";
	}
	
	$qryjefeasesor= $db->query("SELECT * FROM usuarios WHERE usuid = '".$row['usujefeasesor']."';");
	$rowjefeasesor = $qryjefeasesor->fetch(PDO::FETCH_ASSOC);
	$numjefeasesor = $qryjefeasesor->rowCount();
	if ($numjefeasesor == 1){
		$jefeasesor = $rowjefeasesor['usunombre'];
	} else {
		$jefeasesor = "NONE";	
	}
	
	$qryscreen = $db->query("SELECT * FROM usuarios WHERE usuid = '".$row['solususcreen']."';");
	$rowscreen = $qryscreen->fetch(PDO::FETCH_ASSOC);
	$numscreen = $qryscreen->rowCount();
	if ($numscreen == 1){
		$screen = $rowscreen['usunombre'];
	} else {
		$screen = "NONE";	
	}
	
	$dateone = date_create($row['solfecha']);
	$semanaSolicitud = date_format($dateone, 'W');
	
	
	$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':AB'.$i)->applyFromArray($styleArray);
	$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('A'.$i, $row['empnombre'])
    	->setCellValue('B'.$i, $row['solid'])
		->setCellValue('C'.$i, $cliente)
		->setCellValue('D'.$i, $row['usunombre'])
		->setCellValue('E'.$i, $jefeasesor)
		->setCellValue('F'.$i, $row['solfecha'])
		->setCellValue('G'.$i, $row['solestado'])
    	->setCellValue('H'.$i, $tipo_causal_screen)
		->setCellValue('I'.$i, $causal_screen)
    	->setCellValue('J'.$i, $tipo_causal_entrega)
		->setCellValue('K'.$i, $causal_entrega)
		->setCellValue('L'.$i, $row['solfactura'])
		->setCellValue('M'.$i, substr($row['solfechdespacho'], 0, 10))
		->setCellValue('N'.$i, substr($row['solfechdespacho'], 10, 20))
		->setCellValue('O'.$i, substr($row['solfechdescarga'], 0, 10))
    	->setCellValue('P'.$i, substr($row['solfechdescarga'], 10, 20))
		->setCellValue('Q'.$i, $row['solplanilla'])
    	->setCellValue('R'.$i, $row['solNguia'])
		->setCellValue('S'.$i, $row['cliempresa'])
		->setCellValue('T'.$i, $departamento)
		->setCellValue('U'.$i, $ciudad)
		->setCellValue('V'.$i, $relacionista)
    	->setCellValue('W'.$i, $jeferelacionista)
		->setCellValue('X'.$i, substr($row['solfecha'], 0, 4))
		->setCellValue('Y'.$i, substr($row['solfecha'], 5, 2))
		->setCellValue('Z'.$i, $semanaSolicitud)
		->setCellValue('AA'.$i, $row['solbase'])
		->setCellValue('AB'.$i, $screen);
	$i++;
}
// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Listado Solicitudes.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
?>