<?php
$r = '../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
if (isset($_POST['adinota'])) {
	$id1 = $_POST['empresa'];
	$id2 = $_POST['id'];
	$nota = trim(strtoupper($_POST['nota']));
	$row = $db->query("INSERT INTO hissolicitudes (hsoempresa, hsosolicitud, hsofecha, hsousuario, hsonota) VALUES ('$id1', '$id2', NOW(), '" . $_SESSION['id'] . "', '$nota')");
	header('Location:ver.php?anular=1&id1=' . $id1 . '&id2=' . $id2);
} else {
	$id1 = $_GET['id1'];
	$id2 = $_GET['id2'];
}
if(!isset($_GET['anular']))
{
	$empresa = $_GET['empresa'];
	$id = $_GET['id'];
	$cliente = $_GET['cliente'];
	$asesor = $_GET['asesor'];
	$fecha1 = $_GET['fecha1'];
	$fecha2 = $_GET['fecha2'];
	$estado = $_GET['estado'];
	$filtro = 'empresa=' . $empresa . '&id=' . $id . '&cliente=' . $cliente . '&asesor=' . $asesor . '&estado=' . $estado . '&fecha1=' . $fecha1 . '&fecha2=' . $fecha2;

}

$row = $db->query("SELECT * FROM solicitudes INNER JOIN clientes ON solcliente = cliid INNER JOIN empresas ON solempresa = empid WHERE solempresa = '$id1' AND solid = '$id2'")->fetch(PDO::FETCH_ASSOC);
?>
<!doctype html>
<html>
<style>
	#form2 fieldset{
		padding: 10px;
		display: block;
		margin: 20px auto;
		width: 100%!important;
	}
	.pdf, .historico, .nota{
		cursor: pointer;
	}
</style>
<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<script type="text/javascript">
		$(document).ready(function() {
			$('.pdf').click(function() {
				newSrc = $(this).attr('data-rel');
				$('#modal').html("<iframe src='" + newSrc + "' width='100%' height='100%'></iframe>");
				$('#modal').dialog({
					modal: true,
					width: '600',
					height: '800',
					title: 'PDF de la solicitud'
				});
			});
			$('.historico').click(function() {
				newHref = $(this).attr('data-rel');
				$('#dialog2').load(newHref).dialog({
					modal: true,
					width: 800
				});
			});
			$('.nota').click(function() {
				$('#campo').dialog({
					height: 'auto',
					width: '600px',
					modal: true
				});
			});

		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Solicitudes</a>
				<div class="mapa_div"></div><a class="current">Screens</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="form2.php" method="post">
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">Solicitud 
							<img src="<?php echo $r ?>imagenes/iconos/pdf.png" class="pdf" data-rel="<?php echo $r . 'pdf/solicitudes/' . $row['solempresa'] . '/' . $row['solid'] . '.pdf' ?>" title="Solicitud en PDF" /> 
							<img src="<?php echo $r ?>imagenes/iconos/application_edit.png" class="historico" data-rel="notas.php?empresa=<?php echo $row['solempresa'] . '&id=' . $row['solid'] ?>" title="Historia de la solicitud" /> 
							<img src="<?php echo $r ?>imagenes/iconos/add.png" class="nota" title="Agregar nota" />
						</legend>
						<p>
							<img src="<?php echo $r ?>imagenes/logos/<?php echo $row['solempresa'] . "/" . $row['logo']?>" class="img-fluid" title="<?php echo $row['empnombre'] ?>">
							<div align="center">Solicitud</div>
							<div align="center"><input type="text" name="id" class="pedido" value="<?php echo $row['solid'] ?>" readonly /></div>
							<div align="center">Fecha</div>
							<div align="center"><input type="text" class="fecha" value="<?php echo $row['solfecha'] ?>" readonly /></div>
						</p>
						<br>
						<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12">
							<legend class="ui-widget ui-widget-header ui-corner-all">Datos personales</legend>
							<p>
								<label class="not-w">Identificacion: </label>
								<label class="not-w"><?php echo $row['cliid'] ?></label>
							</p>
							<p>
								<label class="not-w">Nombre 1: </label>
								<label class="not-w"><?php echo $row['clinombre'] ?></label>
							</p>
							<p>
								<label class="not-w">Nombre 2: </label>
								<label class="not-w"><?php echo $row['clinom2'] ?></label>
							</p>
							<p>
								<label  class="not-w">Apellido 1: </label>
								<label  class="not-w"><?php echo $row['cliape1'] ?></label>
							</p>
							<p>
								<label  class="not-w">Apellido 2: </label>
								<label  class="not-w"><?php echo $row['cliape2'] ?></label>
							</p>
							<p>
								<label  class="not-w">Edad: </label>
								<label  class="not-w"><?php echo $row['cliedad'] ?></label>
							</p>
							<p>
								<label  class="not-w">E-mail: </label>
								<label  class="not-w"><?php echo $row['cliemail'] ?></label>
							</p>
							<p>
								<label  class="not-w">Celular: </label>
								<label  class="not-w"><?php echo $row['clicelular'] ?></label>
							</p>
						</fieldset>
						<br>
						<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12">
							<legend class="ui-widget ui-widget-header ui-corner-all">Datos ubicacion residencia</legend>
							<p>
								<?php
								$row2 = $db->query("SELECT * FROM departamentos WHERE depid = '" . $row['clidepresidencia'] . "'")->fetch(PDO::FETCH_ASSOC);
								?>
								<label class="not-w">Departamento: </label>
								<label class="not-w"><?php echo $row2['depnombre'] ?></label>
							</p>
							<p>
								<?php
								$row2 = $db->query("SELECT * FROM ciudades WHERE ciudepto = '" . $row['clidepresidencia'] . "' AND ciuid = '" . $row['cliciuresidencia'] . "'")->fetch(PDO::FETCH_ASSOC);
								?>
								<label class="not-w">Ciudad: </label>
								<label class="not-w"><?php echo $row2['ciunombre'] ?></label>
							</p>
							<p>
								<label class="not-w">Barrio: </label>
								<label class="not-w"><?php echo $row['clibarresidencia'] ?></label>
							</p>
							<p>
								<label class="not-w">Direccion: </label>
								<label class="not-w"><?php echo $row['clidirresidencia'] ?></label>
							</p>
							<p>
								<label class="not-w">Telefono: </label>
								<label class="not-w"><?php echo $row['clitelresidencia'] ?></label>
							</p>
						</fieldset>
						<br>
						<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12">
							<legend class="ui-widget ui-widget-header ui-corner-all">Datos ubicacion comercial</legend>
							<p>
								<?php
								$row2 = $db->query("SELECT * FROM departamentos WHERE depid = '" . $row['clidepcomercio'] . "'")->fetch(PDO::FETCH_ASSOC);
								
								?>
								<label class="not-w">Departamento:</label>
								<label class="not-w"><?php echo @$row2['depnombre'] ?></label>
							</p>
							<p>
								<?php
								$row2 = $db->query("SELECT * FROM ciudades WHERE ciudepto = '" . $row['clidepcomercio'] . "' AND ciuid = '" . $row['cliciucomercio'] . "'")->fetch(PDO::FETCH_ASSOC);
								?>
								<label class="not-w">Ciudad:</label>
								<label class="not-w"><?php echo @$row2['ciunombre'] ?></label>
							</p>
							<p>
								<label class="not-w">Barrio: </label>
								<label class="not-w"><?php echo $row['clibarcomercio'] ?></label>
							</p>
							<p>
								<label class="not-w">Direccion:</label>
								<label class="not-w"><?php echo $row['clidircomercio'] ?></label>
							</p>
							<p>
								<label class="not-w">Telefono: </label>
								<label class="not-w"><?php echo $row['clitelcomercio'] ?></label>
							</p>
						</fieldset>
						<br>
						<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12">
							<legend class="ui-widget ui-widget-header ui-corner-all">Datos ubicacion adicional</legend>
							<p>
								<?php
								$row2 = $db->query("SELECT * FROM departamentos WHERE depid = '" . $row['clidepadicional'] . "'")->fetch(PDO::FETCH_ASSOC);
								?>
								<label class="not-w">Departamento:</label>
								<label class="not-w"><?php if($row2) echo $row2['depnombre'] ?></label>
							</p>
							<p>
								<?php
								$row2 = $db->query("SELECT * FROM ciudades WHERE ciudepto = '" . $row['clidepadicional'] . "' AND ciuid = '" . $row['cliciuadicional'] . "'")->fetch(PDO::FETCH_ASSOC);
								?>
								<label class="not-w">Ciudad:</label>
								<label class="not-w"><?php if($row2) echo $row2['ciunombre'] ?></label>
							</p>
							<p>
								<label class="not-w">Barrio: </label>
								<label class="not-w"><?php echo $row['clibaradicional'] ?></label>
							</p>
							<p>
								<label class="not-w">Direccion: </label>
								<label class="not-w"><?php echo $row['clidiradicional'] ?></label>
							</p>
							<p>
								<label class="not-w">Telefono: </label>
								<label class="not-w"><?php echo $row['cliteladicional'] ?></label>
							</p>
						</fieldset>
						<br>
						<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12">
							<legend class="ui-widget ui-widget-header ui-corner-all">Datos adicionales</legend>
							<p>
								<label class="not-w">Empresa: </label>
								<label class="not-w"><?php echo $row['cliempresa'] ?></label>
							</p>
							<p>
								<label class="not-w">Cargo: </label>
								<label class="not-w"><?php echo $row['clicargo'] ?></label>
							</p>
							<p>
								<label class="not-w">Antiguedad: </label>
								<label class="not-w"><?php echo $row['cliantiguedad'] ?></label>
							</p>
							<p>
								<label class="not-w">Salario: </label>
								<label class="not-w"><?php echo number_format($row['clisalario'], 0, ',', '.') ?></label>
							</p>
							<p>
								<label class="not-w">Estado civil: </label>
								<label class="not-w"><?php echo $row['cliestado'] ?></label>
							</p>
							<p>
								<label class="not-w">Vivienda: </label>
								<label class="not-w"><?php echo $row['clivivienda'] ?></label>
							</p>
							<p>
								<label class="not-w">Hijos: </label>
								<label class="not-w"><?php echo $row['clihijos'] ?></label>
							</p>
						</fieldset>
						<?php
						if ($row['cliestado'] == 'CASADO(A)') {
						?>
							<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12">
								<legend class="ui-widget ui-widget-header ui-corner-all">Informacion del conyuge</legend>
								<p>
									<label class="not-w">Nombre: </label>
									<label class="not-w"><?php echo $row['clinomfamiliar'] ?></label>
								</p>
								<p>
									<label class="not-w">Edad: </label>
									<label class="not-w"><?php echo $row['cliedafamiliar'] ?></label>
								</p>
								<p>
									<?php
									$row2 = $db->query("SELECT * FROM departamentos WHERE depid = '" . $row['clidepfamiliar'] . "'")->fetch(PDO::FETCH_ASSOC);
									?>
									<label class="not-w">Departamento: </label>
									<label class="not-w"><?php echo $row2['depnombre'] ?></label>
								</p>
								<p>
									<?php
									$row2 = $db->query("SELECT * FROM ciudades WHERE ciudepto = '" . $row['clidepfamiliar'] . "' AND ciuid = '" . $row['cliciufamiliar'] . "'")->fetch(PDO::FETCH_ASSOC);
									?>
									<label class="not-w">Ciudad: </label>
									<label class="not-w"><?php echo $row2['ciunombre'] ?></label>
								</p>
								<p>
									<label class="not-w">Barrio: </label>
									<label class="not-w"><?php echo $row['clibarfamiliar'] ?></label>
								</p>
								<p>
									<label class="not-w">Direccion: </label>
									<label class="not-w"><?php echo $row['clidirfamiliar'] ?></label>
								</p>
								<p>
									<label class="not-w">Telefono: </label>
									<label class="not-w"><?php echo $row['clitelfamiliar'] ?></label>
								</p>
								<p>
									<label class="not-w">Empresa: </label>
									<label class="not-w"><?php echo $row['cliempfamiliar'] ?>"</label>
								</p>
								<p>
									<label class="not-w">Cargo: </label>
									<label class="not-w">"<?php echo $row['clicarfamiliar'] ?>"</label>
								</p>
								<p>
									<label class="not-w">Antiguedad: </label>
									<label class="not-w"><?php echo $row['cliantfamiliar'] ?></label>
								</p>
								<p>
									<label class="not-w">Salario: </label>
									<label class="not-w"><?php echo number_format($row['clisalfamiliar'], 0, ',', '.') ?></label>
								</p>
							</fieldset>
							<br>
						<?php
						}
						?>
						<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12">
							<legend class="ui-widget ui-widget-header ui-corner-all">Referencias personales, familiares y/o comerciales</legend>
							<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12">
								<legend class="ui-widget ui-widget-header ui-corner-all">Primera referencia</legend>
								<p>
									<label class="not-w">Nombre: </label>
									<label class="not-w"><?php echo $row['clirefnombre1'] ?></label>
								</p>
								<p>
									<label class="not-w">Tel: </label>
									<label class="not-w"><?php echo $row['clireftelefono1'] ?></label>
								</p>
								<p>
									<label class="not-w">Cel: </label>
									<label class="not-w"><?php echo $row['clirefcelular1'] ?></label>
								</p>
							</fieldset>
							<br>
							<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12">
								<legend class="ui-widget ui-widget-header ui-corner-all">Segunda referencia</legend>
								<p>
									<label class="not-w">Nombre: </label>
									<label class="not-w"><?php echo $row['clirefnombre2'] ?></label>
								</p>
								<p>
									<label class="not-w">Tel: </label>
									<label class="not-w"><?php echo $row['clireftelefono2'] ?></label>
								</p>
								<p>
									<label class="not-w">Cel: </label>
									<label class="not-w"><?php echo $row['clirefcelular2'] ?></label>
								</p>
							</fieldset>
							<br>
							<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12">
								<legend class="ui-widget ui-widget-header ui-corner-all">Tercera referencia</legend>
								<p>
									<label class="not-w">Nombre: </label>
									<label class="not-w"><?php echo $row['clirefnombre3'] ?></label>
								</p>
								<p>
									<label class="not-w">Tel: </label>
									<label class="not-w"><?php echo $row['clireftelefono3'] ?></label>
								</p>
								<p>
									<label class="not-w">Cel: </label>
									<label class="not-w"><?php echo $row['clirefcelular3'] ?></label>
								</p>
							</fieldset>
							<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12">
								<legend class="ui-widget ui-widget-header ui-corner-all">Datos productos</legend>
								<?php
								$qry = $db->query("SELECT * FROM detsolicitudes INNER JOIN productos ON detproducto = proid WHERE detempresa = '" . $row['solempresa'] . "' AND detsolicitud = '" . $row['solid'] . "'");
								while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
								?>
									<p>
										<!-- Muestra el código  -->
										<input type="text" value="<?php echo $row2['pronombre'] ?>" style="width:52em" readonly />
										<div class="input-group mb-1">
											<div class="input-group-prepend">
												<span class="input-group-text" >Código</span>
											</div>
											<input type="text" class="not-w form-control" value="<?php echo $row2['proid'] ?>" readonly />
										</div>

										<!-- Muestra la cantidad seleccionada del producto -->
										<div class="input-group mb-3">
											<div class="input-group-prepend">
												<span class="input-group-text" >Cantidad</span>
											</div>
											<input value="<?php echo $row2['detcantidad'] ?>" class="cantidad not-w form-control" readonly />
										</div>
									</p>
								<?php
								}
								?>
							</fieldset>
						</fieldset>
					</fieldset>
				</form>
				<form id="form2" name="form2" action="" method="post">
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
						<p align="right">
							Total: <input type="text" class="valor" value="<?php echo $row['soltotal'] ?>" readonly /></div>
						</p>
						<p align="right">
							Base: <input type="text" class="valor" value="<?php echo $row['solbase'] ?>" readonly /></div>
						</p>
						<p align="right">
							Numero de cuotas: <input type="text" style="text-align: center; width: 3em" value="<?php echo $row['solncuota'] ?>" readonly>
						</p>
						<p align="right">
							Cuota inicial: <input type="text" class="valor" value="<?php echo $row['solcuota'] ?>" readonly />
						</p>
					</fieldset>
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
						<legend class="ui-widget ui-widget-header ui-corner-all text-center">Informacion de la solicitud</legend>
						<p>
							<label class="not-w">cobros: </label>
							<label class="not-w"><?php echo $row['solcobro'] ?></label>
						</p>
						<p>
							<label class="not-w">Entrega: </label>
							<label class="not-w"><?php echo $row['solentrega'] ?></label>
						</p>
						<p>
							<label>Compromiso: </label>
							<input type="text" class="fecha" value="<?php echo $row['solcompromiso'] ?>" readonly />
						</p>
						<p>
							<?php
								$fpago = $db->query("SELECT * FROM fpagos WHERE fpaid = '" . $row['solfpago'] . "'")->fetch(PDO::FETCH_ASSOC);
							?>
							<label class="label">Forma de pago C.I.: </label>
							<?php echo @$fpago['fpanombre'] ?>
						</p>
						<p>
							<?php
							$row2 = $db->query("SELECT * FROM usuarios WHERE usuid = '" . $row['solrelacionista'] . "'")->fetch(PDO::FETCH_ASSOC);
							?>
							<label class="not-w">Relacionista: </label>
							<label class="not-w"><?php echo @$row2['usunombre'] ?></label>
						</p>
					</fieldset>
					<p class="boton">
						<?php 
						if(!isset($_GET['anular'])) { ?>
							<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href = 'listar.php?<?php echo $filtro ?>'">atras</button>
						<?php } ?>
					</p>
					</fieldset>
				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<div id="campo" title="Agregar historico" style="display:none">
		<?php 
		if(!isset($_GET['anular'])) { ?>
		<form id="form" name="form" action="ver.php?<?php echo $filtro ?>" method="post">
		<?php } else { ?>
		<form id="form" name="form" action="ver.php?anular=1" method="post">
		<?php } ?>
			<input type="hidden" name="empresa" value="<?php echo $row['solempresa'] ?>">
			<input type="hidden" name="id" value="<?php echo $row['solid'] ?>">
			<input type="text" name="nota" required title="Digite la nota" />
			<button type="submit" class="btn btn-success btn-block btnmas" name="adinota" value="adinota">Agregar</button>
	</div>
	<div id="modal" style="display:none"></div>
	<div id="dialog2" style="display:none"></div>
</body>

</html>