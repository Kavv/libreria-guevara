<?php
	$r = '../../';
	require($r . 'incluir/session.php');
	require($r . 'incluir/connection.php');
	if (isset($_POST['adinota'])) {
		$empresa = $_POST['empresa'];
		$id = $_POST['id'];
		$nota = trim($_POST['nota']);
		$screen = $_POST['screen'];
		$row = $db->query("INSERT INTO hissolicitudes (hsoempresa, hsosolicitud, hsofecha, hsousuario, hsonota) VALUES ('$empresa', '$id', NOW(), '" . $_SESSION['id'] . "', '$nota')");
		header('Location:form2.php?empresa=' . $empresa . '&id=' . $id . '&screen=' . $screen);
		exit();
	} elseif (isset($_POST['siguiente'])) {
		$empresa = $_POST['empresa'];
		$id = $_POST['id'];
		$screen = $_POST['screen'];
		$cliente = $_POST['cliente'];
		$nombre = trim(strtoupper($_POST['nombre']));
		$nom2 = trim(strtoupper($_POST['nom2']));
		$ape1 = trim(strtoupper($_POST['ape1']));
		$ape2 = trim(strtoupper($_POST['ape2']));
		$barresidencia = trim(strtoupper($_POST['barresidencia']));
		$dirresidencia = trim(strtoupper($_POST['dirresidencia']));
		$barcomercio = trim(strtoupper($_POST['barcomercio']));
		$dircomercio = trim(strtoupper($_POST['dircomercio']));
		$baradicional = trim(strtoupper($_POST['baradicional']));
		$diradicional = trim(strtoupper($_POST['diradicional']));
		$email = trim(strtolower($_POST['email']));
		$trabajo = trim(strtoupper($_POST['trabajo']));
		$cargo = trim(strtoupper($_POST['cargo']));
		
		$estado = $vivienda = "";
		if(isset($_POST['estado']))
			$estado = $_POST['estado'];
		if(isset($_POST['vivienda']))
			$vivienda = $_POST['vivienda'];

		// EVAULAR ESTE PROCEDIMIENTO NO DEBERIA ACTUALIZAR ALGUNOS CAMPOS

		// Actualiza los datos de residencia del cliente
		$qry = $db->query("UPDATE clientes SET clidepresidencia = '" 
		. $_POST['depresidencia'] . "', cliciuresidencia = '" 
		. $_POST['ciuresidencia'] . "', clibarresidencia = '$barresidencia', 
		clidirresidencia = '$dirresidencia', clitelresidencia = '" 
		. $_POST['telresidencia'] . "' WHERE cliid = '$cliente'");
		
		// Actualiza los datos de locación del comercio
		$qry = $db->query("UPDATE clientes SET clidepcomercio = '" 
		. $_POST['depcomercio'] . "', cliciucomercio = '" . $_POST['ciucomercio'] 
		. "', clibarcomercio = '$barcomercio', clidircomercio = '$dircomercio', clitelcomercio = '" 
		. $_POST['telcomercio'] . "' WHERE cliid = '$cliente'");

		// Actualiza los datos de una direccion adicional
		$qry = $db->query("UPDATE clientes SET clidepadicional = '" 
		. $_POST['depadicional'] . "', cliciuadicional = '" . $_POST['ciuadicional'] 
		. "', clibaradicional = '$baradicional', clidiradicional = '$diradicional', cliteladicional = '" 
		. $_POST['teladicional'] . "' WHERE cliid = '$cliente'");
		
		// Actualiza los datos de contacto y personales de ingresos
		$qry = $db->query("UPDATE clientes SET cliemail = '$email', clicelular = '" 
		. $_POST['celular'] . "', cliempresa = '$trabajo', clicargo = '$cargo', cliantiguedad = '" 
		. $_POST['antiguedad'] . "', clisalario = '" . $_POST['salario'] . "', cliestado = '" 
		. $estado . "', clivivienda = '" . $vivienda . "', clihijos = '" 
		. $_POST['hijos'] . "' WHERE cliid = '$cliente'");

	} else {
		$empresa = $_GET['empresa'];
		$id = $_GET['id'];
		$screen = $_GET['screen'];
	}
	$row = $db->query("SELECT * FROM (solicitudes INNER JOIN clientes ON solcliente = cliid) INNER JOIN empresas ON solempresa = empid WHERE solempresa = '$empresa' AND solid = '$id'")->fetch(PDO::FETCH_ASSOC);
?>
<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#depfamiliar').change(function(event) {
				var id1 = $('#depfamiliar').find(':selected').val();
				$('#ciufamiliar').load('<?php echo $r ?>incluir/carga/ciudades.php?id1=' + id1);
			});
			$('.pdf').click(function() {
				newSrc = $(this).attr('data-rel');
				if(validation_file(newSrc) == 200)
					$('#modal').html("<iframe src='" + newSrc + "' width='100%' height='100%'></iframe>");
				else
					$('#modal').html("<spam>El PDF no fue encontrado</spam>");
				
				$('#modal').dialog({
					modal: true,
					width: '600',
					height: '800',
					title: 'PDF de la solicitud'
				});
			});
			$('.historico').click(function() {
				newHref = $(this).attr('data-rel');
				$('#dialog2').load(newHref).dialog({
					modal: true,
					width: 800
				});
			});
			$('.nota').click(function() {
				$('#campo').dialog({
					height: 'auto',
					width: '600px',
					modal: true
				});
			})
		});
	</script>
	<style>
		.pdf, .historico, .nota{
			cursor: pointer;
		}
	</style>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a class="current">Solicitudes</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="form3.php" method="post">
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-sm-12 col-md-8 col-lg-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">Solicitud - Hoja 2 &nbsp;<img src="<?php echo $r ?>imagenes/iconos/pdf.png" class="pdf" data-rel="<?php echo $r . 'pdf/solicitudes/' . $empresa . '/' . $id . '.pdf' ?>" title="Solicitud en PDF" /> <img src="<?php echo $r ?>imagenes/iconos/application_edit.png" class="historico" data-rel="notas.php?empresa=<?php echo $empresa . '&id=' . $id ?>" title="Historia de la solicitud" /> <img src="<?php echo $r ?>imagenes/iconos/add.png" class="nota" title="Agregar nota" /></legend>
						<p>
							<img src="<?php echo $r ?>imagenes/logos/<?php echo $empresa . "/" . $row['logo']?>" class="img-fluid" title="<?php echo $row['empnombre'] ?>">
							<input type="hidden" name="empresa" value="<?php echo $empresa ?>">
							<input type="hidden" name="cliente" value="<?php echo $row['cliid'] ?>">
							<div align="center">Solicitud</div>
							<div align="center"><input type="text" name="id" class="pedido" value="<?php echo $id ?>" readonly /></div>
							<div align="center">Fecha</div>
							<div align="center"><input type="text" class="fecha" value="<?php echo $row['solfecha'] ?>" readonly /></div>
						</p>
						<br>
						<br>
						<input type="hidden" name="screen" value="<?php echo $screen ?>" />
						<input type="hidden" name="estado" value="<?php echo $row['cliestado'] ?>" />
						<?php
						if ($row['cliestado'] == 'CASADO(A)') {
						?>
							<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12">
								<legend class="ui-widget ui-widget-header ui-corner-all">Informacion del conyuge</legend>
								<p>
									<label>Nombre: </label>
									<input type="text" name="nomfamiliar" class="nombre" value="<?php echo $row['clinomfamiliar'] ?>" title="Digite el nombre del conyuge" />
									<label>Edad: </label>
									<input type="text" name="edafamiliar" class="edad validate[custom[onlyNumberSp]] text-input" value="<?php echo $row['cliedafamiliar'] ?>" title="Digite la edad del conyuge" />
									<p>
										<label>Departamento: </label>
										<select id="depfamiliar" name="depfamiliar" class=" text-input">
											<?php
											if ($row['clidepfamiliar'] != '') {
												$row2 = $db->query("SELECT * FROM departamentos WHERE depid = '" . $row['clidepfamiliar'] . "'")->fetch(PDO::FETCH_ASSOC);
												echo '<option value=' . $row['clidepfamiliar'] . '>' . $row2['depnombre'] . '</option>';
											}
											echo '<option value="">SELECCIONE</option>';
											$qry = $db->query("SELECT * FROM departamentos WHERE depid <> '" . $row['clidepfamiliar'] . "' ORDER BY depnombre");
											while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
												echo '<option value=' . $row2['depid'] . '>' . $row2['depnombre'] . '</option>';
											}
											?>
										</select>
										<label>Ciudad: </label>
										<select name="ciufamiliar" id="ciufamiliar" class=" text-input">
											<?php
											if ($row['cliciufamiliar'] != '') {
												$row2 = $db->query("SELECT * FROM ciudades WHERE ciudepto = '" . $row['clidepfamiliar'] . "' AND ciuid = '" . $row['cliciufamiliar'] . "'")->fetch(PDO::FETCH_ASSOC);
												echo '<option value=' . $row['cliciufamiliar'] . '>' . $row2['ciunombre'] . '</option>';
											}
											echo '<option value="">SELECCIONE</option>';
											$qry = $db->query("SELECT * FROM ciudades WHERE ciudepto = '" . $row['clidepfamiliar'] . "' AND ciuid <> '" . $row['cliciufamiliar'] . "' ORDER BY ciunombre");
											while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
												echo '<option value=' . $row2['depid'] . '>' . $row2['depnombre'] . '</option>';
											}
											?>
										</select>
									</p>
									<p>
										<label>Barrio: </label><input type="text" name="barfamiliar" class="barrio text-input" value="<?php echo $row['clibarfamiliar'] ?>">
									</p>
									<p>
										<label>Direccion: </label><input type="text" name="dirfamiliar" class="direccion text-input" value="<?php echo $row['clidirfamiliar'] ?>" />
										<label>Telefono: </label><input type="text" name="telfamiliar" class="telefono validate[custom[phone]] text-input" value="<?php echo $row['clitelfamiliar'] ?>" />
									</p>
									<p>
										<label>Empresa: </label><input type="text" name="empfamiliar" class="empresa validate[] text-input" value="<?php echo $row['cliempfamiliar'] ?>" />
									</p>
									<p>
										<label>Cargo: </label><input type="text" name="carfamiliar" class="cargo" value="<?php echo $row['clicarfamiliar'] ?>" />
										<label>Antiguedad <em>(En meses)</em>: </label><input type="text" name="antfamiliar" value="<?php echo $row['cliantfamiliar'] ?>" title="Diguite la entiguedad del conyuge" placeholder="Antiguedad en meses"/>
										<label>Salario: </label><input type="text" class="valor validate[custom[onlyNumberSp]]" name="salfamiliar" value="<?php echo $row['clisalfamiliar'] ?>" title="Digite el salario del conyuge" />
									</p>
							</fieldset>
							<br>
						<?php
						}
						?>
						<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12">
							<legend class="ui-widget ui-widget-header ui-corner-all">Referencias personales, familiares y/o comerciales</legend>
							<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12">
								<legend class="ui-widget ui-widget-header ui-corner-all">Primera referencia</legend>
								<p>
									<label>Nombre:</label> 
									<input type="text" name="refnombre1" class="nombre validate[required]" value="<?php echo $row['clirefnombre1'] ?>" title="Digite el nombre de la primera referencia" />
									<label class="not-w">Tel:</label> 
									<input type="checkbox" onClick="reftelefono1.disabled = !this.checked" <?php if ($row['clireftelefono1'] != '') echo 'checked' ?> /> <input type="text" name="reftelefono1" class="telefono validate[required, custom[phone]] text-input" value="<?php echo $row['clireftelefono1'] ?>" <?php if ($row['clireftelefono1'] == '') echo 'disabled' ?> />
									<label class="not-w">Cel:</label> 
									<input type="checkbox" onClick="refcelular1.disabled = !this.checked" <?php if ($row['clirefcelular1'] != '') echo 'checked' ?> /> <input type="text" name="refcelular1" class="telefono validate[required, custom[phone]] text-input" value="<?php echo $row['clirefcelular1'] ?>" <?php if ($row['clirefcelular1'] == '') echo 'disabled' ?> />
								</p>
							</fieldset>
							<br>
							<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12">
								<legend class="ui-widget ui-widget-header ui-corner-all">Segunda referencia</legend>
								<p>
									<label>Nombre:</label> 
									<input type="text" name="refnombre2" class="nombre validate[required]" value="<?php echo $row['clirefnombre2'] ?>" title="Digite el nombre de la segunda referencia" />
									<label class="not-w">Tel:</label> 
									<input type="checkbox" onClick="reftelefono2.disabled = !this.checked" <?php if ($row['clireftelefono2'] != '') echo 'checked' ?> /> <input type="text" name="reftelefono2" class="telefono" value="<?php echo $row['clireftelefono2'] ?>" <?php if ($row['clireftelefono2'] == '') echo 'disabled' ?> required />
									<label class="not-w">Cel:</label> 
									<input type="checkbox" onClick="refcelular2.disabled = !this.checked" <?php if ($row["clirefcelular2"] != "") echo "checked" ?> /> <input type="text" name="refcelular2" class="telefono" value="<?php echo $row["clirefcelular2"] ?>" <?php if ($row["clirefcelular2"] == "") echo "disabled" ?> required />
								</p>
							</fieldset>
							<br>
							<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12">
								<legend class="ui-widget ui-widget-header ui-corner-all">Tercera referencia</legend>
								<p>
									<label>Nombre:</label> 
									<input type="text" name="refnombre3" class="nombre" value="<?php echo $row["clirefnombre3"] ?>" title="Digite el nombre de la tercera referencia" />
									<label class="not-w">Tel:</label> 
									<input type="checkbox" onClick="reftelefono3.disabled = !this.checked" <?php if ($row["clireftelefono3"] != "") echo "checked" ?> /> <input type="text" name="reftelefono3" class="telefono" value="<?php echo $row["clireftelefono3"] ?>" <?php if ($row["clireftelefono3"] == "") echo "disabled" ?> required />
									<labe class="not-w"l>Cel:</label> 
									<input type="checkbox" onClick="refcelular3.disabled = !this.checked" <?php if ($row["clirefcelular3"] != "") echo "checked" ?> /> <input type="text" name="refcelular3" class="telefono" value="<?php echo $row["clirefcelular3"] ?>" <?php if ($row["clirefcelular3"] == "") echo "disabled" ?> required />
								</p>
							</fieldset>
							<p class="boton">
								<button type="button" class="btn btn-primary btnatras" onClick=" carga(); location.href = 'form1.php?empresa=<?php echo $empresa . '&id=' . $id . '&screen=' . $screen ?>'">atras</button>
								<button type="submit" class="btn btn-primary btnsiguiente" name="siguiente" value="siguiente">siguiente</button>
							</p>
						</fieldset>
				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<div id="campo" title="Agregar historico" style="display:none">
		<form id="form" name="form" action="form2.php" method="post">
			<input type="hidden" name="empresa" value="<?php echo $empresa ?>">
			<input type="hidden" name="id" value="<?php echo $id ?>">
			<input type="hidden" name="screen" value="<?php echo $screen ?>">
			<input type="text" name="nota" required title="Digite la nota" />
			<button type="submit" class="btn btn-success btn-block btnmas" name="adinota" value="adinota">Agregar</button>
	</div>
	<div id="modal" style="display:none"></div>
	<div id="dialog2" style="display:none"></div>
</body>

</html>