<?php
	// LA ACCION PRINCIPAL DE ESTA PAGINA ES MOSTRAR LOS RESULTADOS DE LA CONSULTA GENERADA DESDE CONFISICOS.PHP
	// LUEGO DE HABER RECIBIDO LAS VARIABLES POR POST DESDE CONFISICOS.PHP PROCEDEMOS A EVALUAR ESAS VARIABLES PARA MOSTRAR RESULTADOS DE LA CONSULTA O MODIFICAR SI ASI CORRESPONDE

	//INCLUIR SESION Y CONECCION
	$r = '../../';
	require($r . 'incluir/session.php');
	require($r . 'incluir/connection.php');
	// VALIDAMOS SI ID1 PASADO POR GET ES TRUE
	if (isset($_GET['id1'])) {
		// ACTUALIZAMOS LA INFORMACION CON LOS NUEVOS DATOS RECIBIDOS
		$qry = $db->query("UPDATE solicitudes SET solfisico = '1' WHERE solempresa = '" . $_GET['id1'] . "' AND solid = '" . $_GET['id2'] . "'");
		// MENSAJE MODAL DE CONFIRMACION 
		$mensaje = 'Se confirmo entrega fisica de la solicitud.';
		//REDIRECCIONAMIENTO JUNTO CON EL MENSAJE MODAL CORREPONDIENTE
		header('Location:confisicos.php?mensaje=' . $mensaje);
		exit();
	}
	$id = "";
	// SE LE ASIGNA UNA VARIABLE A LO QUE SE RECIBA POR POST
	if(isset($_POST))
		$id = $_POST['id'];
	
	// FILTRO GENERAL PARA ENVIAR POR GET 
	$filtro = 'id=' . $id;

	// CONSULTAR SOLICITUDES DONDE EN EL CAMPO DE solfisico SE ENCUENTRE EN 0
	if ($id != '')
		$sql = "SELECT * FROM ((solicitudes INNER JOIN empresas ON solempresa = empid) INNER JOIN clientes ON solcliente = cliid) INNER JOIN usuarios ON usuid = solasesor WHERE solid LIKE '%$id%' AND solfisico = '0' ORDER BY solfecha";
	else
		$sql = "SELECT * FROM ((solicitudes INNER JOIN empresas ON solempresa = empid) INNER JOIN clientes ON solcliente = cliid) INNER JOIN usuarios ON usuid = solasesor WHERE solfisico = '0' ORDER BY solfecha"; //QRY  PARA MOSTRAR TODAS LAS SOLICITUDES DONDE EL CAMPO solfisico SE ENCUENTRA EN 0

	$qry = $db->query($sql);
?>
<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>

	<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
	<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>

	<script type="text/javascript">
		$(document).ready(function() {
			$('#tabla').dataTable({ //QRY PARA MANEJO Y VISUALIZACION DE TABLAS
				'bJQueryUI': true,
				'sPaginationType': 'full_numbers',
				'oLanguage': {
					'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
				},
				'bSort': false,
			});

			$('.pdf').click(function() { //CARACTERISTICAS DEL PDF A MOSTRAR
				newSrc = $(this).attr('data-rel');
				if (validation_file(newSrc) == 200)
					$('#modal').html("<iframe src='" + newSrc + "' width='100%' height='100%'></iframe>");
				else
					$('#modal').html("<spam>El PDF no fue encontrado</spam>");
				$('#modal').dialog({
					modal: true,
					width: '600',
					height: '800',
					title: 'PDF de la solicitud'
				});
			});

			$('.msj_check').click(function(e) { // MENSAJE DE VALIDACION O CONFIRMACION DE ACCIONES
				e.preventDefault();
				var targetUrl = $(this).attr('href');
				var $dialog_link_follow_confirm = $('<div></div>').html("<p><span class='ui-icon ui-icon-alert' style='float:left; margin:2px 7px 7px 0;'></span>Checkear esta solicitud como entregada fisicamente?</p>").
				dialog({
					title: 'Porfavor confirmar',
					buttons: {
						'Si': function() {
							window.location.href = targetUrl;
						},
						'No': function() {
							$(this).dialog("close");
						}
					},
					modal: true,
					width: 'auto',
					height: 'auto'
				});
				$dialog_link_follow_confirm.dialog('open');
			});
		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
		<?php require($r . 'incluir/src/menu.php') ?>
		<!-- INCLUIMOS MENU PRINCIPAL -->
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Solicitudes</a>
				<div class="mapa_div"></div><a class="current">Fisicos</a>
			</article>
			<article id="contenido">
				<h2>Listado de solicitudes no entregadas</h2>
				<!-- INICIO DE TABLA -->
				<table id="tabla">
					<thead>
						<tr>
							<th>Empresa</th>
							<th>Solicitud</th>
							<th>Factura</th>
							<th>Estado de Solicitud</th>
							<th>Cliente</th>
							<th>Asesor</th>
							<th>Fecha</th>
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php
						while ($row = $qry->fetch(PDO::FETCH_ASSOC)) { //INFORMACION DE LA CONSULTA
						?>
							<tr>
								<td title="<?php echo $row['empid'] . ' ' . $row['empdigito'] ?>"><?php echo $row['empnombre'] ?></td>
								<td align="center"><?php echo $row['solid'] ?></td>
								<td align="center"><?php echo $row['solfactura'] ?></td>
								<td align="center"><?php echo $row['solestado'] ?></td>
								<td title="<?php echo $row['cliid'] . ' ' . $row['clidigito'] ?>"><?php echo $row['clinombre'] . ' ' . $row['clinom2'] . ' ' . $row['cliape1'] . ' ' . $row['cliape2'] ?></td>
								<td title="<?php echo $row['solasesor'] ?>"><?php echo $row['usunombre'] ?></td>
								<td align="center"><img src="<?php echo $r ?>imagenes/iconos/date.png" title="<?php echo $row['solfecha'] ?>" /></td>
								<td align="center"><img src="<?php echo $r ?>imagenes/iconos/pdf.png" class="pdf" data-rel="<?php echo $r . 'pdf/solicitudes/' . $row['solempresa'] . '/' . $row['solid'] . '.pdf' ?>" title="Solicitud en PDF" /></td> <!-- PDF EN UN FRAME -->
								<td align="center"><a href="fisicos.php?id1=<?php echo $row['solempresa'] . '&id2=' . $row['solid'] ?>" class="msj_check" title="validar fisico"><img src="<?php echo $r ?>imagenes/iconos/tick.png" class="grayscale" /></a></td> <!-- CAMPO DE VALIDACION -->
							</tr>
						<?php
						}
						?>
					</tbody>
				</table>
				<p class="boton">
					<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href='confisicos.php'">atras</button> <!-- BOTON PARA VOLVER A CONFISICOS -->
				</p>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
		<!-- BOTON PARA VOLVER A LA PARTE SUPERIOR  -->
	</section>
	<div id="modal" style="display:none"></div>
</body>

</html>