<?php
	$r = '../../';
	require($r . 'incluir/session.php');
	require($r . 'incluir/connection.php');

	if (isset($_POST['consultar'])) {
		$fecha1 = $_POST['fecha1'];
		$fecha2 = $_POST['fecha2'];
	} elseif(isset($_GET['fecha1'])) {
		$fecha1 = $_GET['fecha1'];
		$fecha2 = $_GET['fecha2'];
	}

	$filtro = 'fecha1=' . $fecha1 . '&fecha2=' . $fecha2;

	$con = "SELECT * FROM hissolicitudes
	INNER JOIN empresas ON empid = hsoempresa 
	";
	$ord = 'ORDER BY hsofecha DESC ;';

	if ($fecha1 != '') 
		$sql = "$con WHERE DATE(hsofecha) BETWEEN '$fecha1' AND '$fecha2' $ord";
	else
		$sql = $con . " " . $ord;

	$qry = $db->query($sql);
?>
<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>

	<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
	<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#tabla').dataTable({
				'bJQueryUI': true,
				'sPaginationType': 'full_numbers',
				'oLanguage': {
					'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
				},
				'bSort': false,
			});

		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Solicitudes</a>
				<div class="mapa_div"></div><a class="current">Reportes</a>
			</article>
			<article id="contenido">
				<h2>Reportes de Notas <?php $sql ?></h2>
				<div class="reporte">
					<a href="excelgestiones.php?<?php echo $filtro ?>"
						><img src="<?php echo $r ?>imagenes/iconos/excel.png" title="csv" />
					</a>
				</div>
				<table id="tabla">
					<thead>
						<tr>
							<th title="Empresa">Empresa</th>
							<th title="Numero de solicitud">Solicitud</th>
							<th title="Numero de Factura">Factura</th>
							<th>Estado</th>
							<th>Cliente</th>
							<th title="Fecha de Screen">F.S</th>
							<th>Gestion</th>
							<th title="Fecha de Gestion">F.G</th>
							<th>Usuario Screen</th>
							<th>Asesor</th>
						</tr>
					</thead>
					<tbody>
						<?php
						while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
							$row1 = $db->query("SELECT * FROM  solicitudes INNER JOIN clientes ON cliid = solcliente INNER JOIN usuarios ON usuid = solususcreen WHERE solid  = '" . $row['hsosolicitud'] . "' AND solempresa = '" . $row['hsoempresa'] . "'")->fetch(PDO::FETCH_ASSOC);
							$nombre = $factura = $estado = $fecha_screen = $usuario_screen = $cliente_id = $capacitador = "";
							if($row1)
							{
								$nombre = $row1['clinombre'] . ' ' . $row1['clinom2'] . ' ' . $row1['cliape1'] . ' ' . $row1['cliape2'];
								$factura = $row1['solfactura'];
								$estado = $row1['solestado'];
								$fecha_screen = $row1['solfechscreen'];
								$usuario_screen = $row1['usunombre'];
								$cliente_id = $row1['cliid'];
								$qryca = $db->query("SELECT * FROM usuarios WHERE usuid = '" . $row1['solasesor'] . "';");
								$rowca = $qryca->fetch(PDO::FETCH_ASSOC);
								if($rowca)
									$capacitador = $rowca['usunombre'];
							}
						?>
							<tr>
								<td title="<?php echo $row['empid'] . ' ' . $row['empdigito'] ?>"><?php echo $row['empnombre'] ?></td>
								<td align="center"><?php echo $row['hsosolicitud'] ?></td>
								<td align="center"><?php echo $factura ?></td>
								<td align="center"><?php echo $estado ?></td>
								<td title="<?php echo $cliente_id ?>"><?php echo $nombre ?></td>
								<td align="center"><img src="<?php echo $r ?>imagenes/iconos/date.png" title="<?php echo $fecha_screen ?>" /></td>
								<td align="center"><?php echo $row['hsonota'] ?></td>
								<td align="center"><img src="<?php echo $r ?>imagenes/iconos/date.png" title="<?php echo $row['hsofecha'] ?>" /></td>
								<td align="center"><?php echo $usuario_screen ?></td>
								<td align="center"><?php echo $capacitador ?></td>
							</tr>
						<?php
						}
						?>
					</tbody>
				</table>
				<p class="boton">
					<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href='consolgestiones.php'">Atras</button>
				</p>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<?php
	if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>';
	elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>';
	?>
	<div id="modal" style="display:none"></div>
	<div id="dialog2" style="display:none"></div>
</body>

</html>