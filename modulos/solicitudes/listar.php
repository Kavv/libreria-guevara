<?php
$r = '../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

$cliente = $asesor = $estado = $fecha1 = $fecha2 = "";

if (isset($_GET['cancelar'])) {
	$id1 = $_GET['id1'];
	$id2 = $_GET['id2'];
	$qry = $db->query("UPDATE solicitudes SET solestado = 'CANCELADO' WHERE solempresa = '$id1' AND solid = '$id2'");
	$mensaje = 'Se cancelo la solicitud';
}
if (isset($_GET['reactivar'])) {
	$id1 = $_GET['id1'];
	$id2 = $_GET['id2'];
	$qry = $db->query("UPDATE solicitudes SET solestado = 'PROCESO' WHERE solempresa = '$id1' AND solid = '$id2'");
	$mensaje = 'Se reactivo la solicitud';
}
if (isset($_GET['borrar'])) {
	$id1 = $_GET['id1'];
	$id2 = $_GET['id2'];
	$row = $db->query("SELECT * FROM solicitudes WHERE solempresa = '$id1' AND solid = '$id2'")->fetch(PDO::FETCH_ASSOC);
	if ($row['solfactura'] == '') {
		$qry = $db->query("DELETE FROM solicitudes WHERE solempresa = '$id1' AND solid = '$id2'");
		$mensaje = 'Se borro la solicitud';
	} else $error = 'La solicitud tiene una factura asociada';
}
if (isset($_POST['consultar'])) {
	$empresa = $_POST['empresa'];
	$id = $_POST['id'];
	$cliente = $_POST['cliente'];
	$asesor = $_POST['asesor'];
	$estado = $_POST['estado'];
	$fecha1 = $_POST['fecha1'];
	$fecha2 = $_POST['fecha2'];
} elseif (isset($_GET['cliente'])) {
	$cliente = $_GET['cliente'];
	$asesor = $_GET['asesor'];
	$estado = $_GET['estado'];
	$fecha1 = $_GET['fecha1'];
	$fecha2 = $_GET['fecha2'];
}

if (isset($_GET['empresa']))
	$empresa = $_GET['empresa'];
if (isset($_GET['id']))
	$id = $_GET['id'];
if (isset($_GET['asesor']))
	$asesor = $_GET['asesor'];

if ($asesor == '') {
	$error = 'Debe seleccionar un asesor.';
	header('Location:consultar.php?error=' . $error);
	exit();
}

$filtro = 'empresa=' . $empresa . '&id=' . $id . '&cliente=' . $cliente . '&asesor=' . $asesor . '&estado=' . $estado . '&fecha1=' . $fecha1 . '&fecha2=' . $fecha2;
$con = 'SELECT * FROM ((solicitudes INNER JOIN empresas ON solempresa = empid) INNER JOIN clientes ON solcliente = cliid) LEFT JOIN usuarios ON solasesor = usuid';
$ord = ' ORDER BY solfecha DESC';

/* Los parametros de la consulta sql se genera dinamicamente 
en base a los datos recibidos para delimitar los resultados */
$parameters = [];
if($empresa != "")
	array_push($parameters, "solempresa LIKE '%$empresa%'" );
if($id != "")
	array_push($parameters, "solid LIKE '%$id%'" );
if($cliente != "")
	array_push($parameters, "solcliente LIKE '%$cliente%'" );
if($asesor != "" && $asesor != "TODOS")
	array_push($parameters, "solasesor = '$asesor'" );

if($fecha1 != "")
	array_push($parameters, "solfecha BETWEEN '$fecha1' AND '$fecha2'" );
if($estado != "")
	array_push($parameters, "solestado = '$estado'" );

// Consulta base
$sql = $con;
foreach ($parameters as $index => $parameter) {
	// Se agregan los parametros del WHERE
	if($index == 0)
		$sql .= " WHERE " . $parameter;
	else
		$sql .= " AND " . $parameter;
}
// Se completa la consulta
$sql .= $ord;

echo "<br><br>".$sql;

$qry = $db->query($sql);
?>
<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>

	<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
	<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#tabla').dataTable({
				'bJQueryUI': true,
				'sPaginationType': 'full_numbers',
				'oLanguage': {
					'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
				},
				'bSort': false,
			});
			$('.pdf').click(function() {
				newSrc = $(this).attr('data-rel');
				if(validation_file(newSrc) == 200)
					$('#modal').html("<iframe src='" + newSrc + "' width='100%' height='100%'></iframe>");
				else
					$('#modal').html("<spam>El PDF no fue encontrado</spam>");
				
				$('#modal').dialog({
					modal: true,
					width: '600',
					height: '800',
					title: 'PDF de la solicitud'
				});
			});

			$('.historico').click(function() {
				newHref = $(this).attr('data-rel');
				$('#dialog2').load(newHref).dialog({
					modal: true,
					width: 800
				});
			});
			$('.msj_cancelar').click(function(e) {
				e.preventDefault();
				var targetUrl = $(this).attr('href');
				var $dialog_link_follow_confirm = $('<div></div>').html("<p><span class='ui-icon ui-icon-alert' style='float:left; margin:2px 7px 7px 0;'></span>Esta seguro que desea cancelar la solicitud?</p>").
				dialog({
					title: 'Porfavor confirmar',
					buttons: {
						'Si': function() {
							window.location.href = targetUrl;
						},
						'No': function() {
							$(this).dialog("close");
						}
					},
					modal: true,
					width: 'auto',
					height: 'auto'
				});
				$dialog_link_follow_confirm.dialog("open");
			});
			$('.msj_eliminar').click(function(e) {
				e.preventDefault();
				var targetUrl = $(this).attr('href');
				var $dialog_link_follow_confirm = $('<div></div>').html("<p><span class='ui-icon ui-icon-alert' style='float:left; margin:2px 7px 7px 0;'></span>Esta seguro que desea eliminar la solicitud?</p>").
				dialog({
					title: 'Porfavor confirmar',
					buttons: {
						'Si': function() {
							window.location.href = targetUrl;
						},
						'No': function() {
							$(this).dialog("close");
						}
					},
					modal: true,
					width: 'auto',
					height: 'auto'
				});
				$dialog_link_follow_confirm.dialog("open");
			});
			$('.msj_reactivar').click(function(e) {
				e.preventDefault();
				var targetUrl = $(this).attr('href');
				var $dialog_link_follow_confirm = $('<div></div>').html("<p><span class='ui-icon ui-icon-locked' style='float:left; margin:2px 7px 7px 0;'></span>Esta seguro que desea reactivar la solicitud?</p>").
				dialog({
					title: 'Porfavor confirmar',
					buttons: {
						'Si': function() {
							window.location.href = targetUrl;
						},
						'No': function() {
							$(this).dialog("close");
						}
					},
					modal: true,
					width: 'auto',
					height: 'auto'
				});
				$dialog_link_follow_confirm.dialog("open");
			});
		});
		
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a class="current">Solicitudes</a>
			</article>
			<article id="contenido">
				<h2>Listado de solicitudes</h2>
				<div class="reporte">
					<a href="excellistar.php?<?php echo $filtro ?>"><img src="<?php echo $r ?>imagenes/iconos/excel.png" title="csv" /></a>
				</div>
				<table id="tabla">
					<thead>
						<tr>
							<th title="Empresa">Empresa</th>
							<th title="Numero de solicitud">Solicitud</th>
							<th>Cliente</th>
							<th>Asesor</th>
							<th title="Fecha de la solicitud">F.S</th>
							<th>Estado</th>
							<th>Factura</th>
							<th title="Fecha de despacho">F.D</th>
							<th title="Fecha de entrega">F.E</th>
							<th title="Planilla de entrega">P.E</th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php
						while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
						?>
							<tr>
								<!-- Nombre de la empresa -->
								<td title="<?php echo $row['empid'] . ' ' . $row['empdigito'] ?>"><?php echo $row['empnombre'] ?></td>
								<!-- ID de la solicitud -->
								<td align="center"><?php echo $row['solid'] ?></td>
								<!-- Nombre del cliente -->
								<td title="<?php echo $row['cliid'] . ' ' . $row['clidigito'] ?>"><?php echo $row['clinombre'] . ' ' . $row['clinom2'] . ' ' . $row['cliape1'] . ' ' . $row['cliape2'] ?></td>
								<!-- Nombre del asesor -->
								<td title="<?php echo $row['usuid'] ?>"><?php echo $row['usunombre'] ?></td>
								<!-- Fecha de la solicitud -->
								<td align="center">
									<?php 
										if($row['solfecha'] != "")
											echo '<img src="'.$r.'imagenes/iconos/date.png" title="'.$row['solfecha'].'" />';
									?>
								</td>
								<!-- Estado -->
								<td>
									<?php
									if ($row['solestado'] == 'RECHAZADO SCREEN') {
										$row2 = $db->query("SELECT * FROM tcausales INNER JOIN causales ON tcaid = cautcausal WHERE tcaid = " . $row['soltcascreen'] . " AND cauid = " . $row['solcauscreen'])->fetch(PDO::FETCH_ASSOC);
										echo '<span title="' . $row2['tcanombre'] . ' - ' . $row2['caunombre'] . '">' . $row['solestado'] . ' - ' . $row2['caunombre'] . '</spam>';
									} elseif ($row['solestado'] == 'RECHAZADO ENTREGA') {
										$row2 = $db->query("SELECT * FROM tcausales INNER JOIN causales ON tcaid = cautcausal WHERE tcaid = " . $row['soltcadescarga'] . " AND cauid = " . $row['solcaudescarga'])->fetch(PDO::FETCH_ASSOC);
										echo '<span title="' . $row2['tcanombre'] . ' - ' . $row2['caunombre'] . '">' . $row['solestado'] . '</spam>';
									} elseif (!empty($row['solestado'])) {
										echo '<span>' . $row['solestado'] . '</spam>';
									}
									?>
								</td>
								<!-- Numero de la factura -->
								<td align="center"><?php echo $row['solfactura'] ?></td>
								<!-- Fecha de salida del detalle de la solicitud -->
								<td align="center">
									<?php 
										if($row['solfechdespacho'] != "") 
											echo '<img src="'.$r.'imagenes/iconos/date.png" title="'.$row['solfechdespacho'].'" />';
									?>	
								</td>
								<!-- Fecha de entrega -->
								<td align="center">
									<?php 
										if($row['solfechdescarga'] != "") 
											echo '<img src="'.$r.'imagenes/iconos/date.png" title="'.$row['solfechdescarga'].'" />';
									?>	
								</td>
								<td align="center"><?php echo $row['solplanilla'] ?></td>
								<!-- Historial/Comentarios de la solicitud -->
								<td align="center"> <a href="historico_solicitudes.php?<?php echo 'empresa=' . $row['solempresa'] . '&id=' . $row['solid'] ?>" target='_blank'> <img src="<?php echo $r ?>imagenes/iconos/comments.png" title="Historico Solicitud" /> </a> </td>
								<!-- PDF de la solicitud(el cual es ingresado por el usuario) -->
								<td align="center"><img src="<?php echo $r ?>imagenes/iconos/pdf.png" class="pdf" data-rel="<?php echo $r . 'pdf/solicitudes/' . $row['solempresa'] . '/' . $row['solid'] . '.pdf' ?>" title="Solicitud" /></td>
								<!-- Permite ver el detalle de la solicitud -->
								<td align="center"><a href="ver.php?id1=<?php echo $row['solempresa'] . '&id2=' . $row['solid'] . '&' . $filtro ?>" onClick="carga()"><img src="<?php echo $r ?>imagenes/iconos/application_view_tile.png" title="Solicitud digitada" class="grayscale" /></a></td>
								<!-- Si tienes los permisos correspondientes podras visualizar el boton de editar  -->
								<td align="center">
									<?php
									if ($rowlog['persolmod'] == '1' && ($row['solestado'] == 'BORRADOR' || $row['solestado'] == 'PROCESO' || $row['solestado'] == 'APROBADO SCREEN' || $row['solestado'] == 'ENTREGADO')) {
									?>
										<a href="form1.php?empresa=<?php echo $row['solempresa'] . '&id=' . $row['solid'] ?>" title="modificar"><img src="<?php echo $r ?>imagenes/iconos/lapiz.png" class="grayscale" /></a>
									<?php
									} else echo '<img src="' . $r . 'imagenes/iconos/lapiz.png" class="gray" />';
									?>
								</td>
								<!-- Cancelar solicitud -->
								<td align="center">
									<?php
									if ($rowlog['persolcan'] == '1' && ($row['solestado'] == 'PROCESO' || $row['solestado'] == 'APROBADO SCREEN')) {
									?>
										<a href="listar.php?id1=<?php echo $row['solempresa'] . '&id2=' . $row['solid'] . '&cancelar=1&' . $filtro ?>" class="msj_cancelar" title="cancelar"><img src="<?php echo $r ?>imagenes/iconos/cancelar.png" class="grayscale" /></a>
									<?php
									} else echo '<img src="' . $r . 'imagenes/iconos/cancelar.png" class="gray" />';
									?>
								</td>
								<!-- Reactivar solicitud -->
								<td align="center">
									<?php
									if ($rowlog['persolrea'] == '1' && ($row['solestado'] == 'CANCELADO' || $row['solestado'] == 'RECHAZADO SCREEN')) {
									?>
										<a href="listar.php?id1=<?php echo $row['solempresa'] . '&id2=' . $row['solid'] . '&reactivar=1&' . $filtro ?>" class="msj_reactivar" title="reactivar"><img src="<?php echo $r ?>imagenes/iconos/arrow_refresh_small.png" class="grayscale" /></a>
									<?php
									} else echo '<img src="' . $r . 'imagenes/iconos/arrow_refresh_small.png" class="gray" />';
									?>
								</td>
								<!-- Borrar permanentemente la solicitud -->
								<td align="center">
									<?php
									if ($rowlog['persoleli'] == '1' && ($row['solestado'] == 'BORRADOR' || $row['solestado'] == 'PROCESO' || $row['solestado'] == 'APROBADO SCREEN')) {
									?>
										<a href="listar.php?id1=<?php echo $row['solempresa'] . '&id2=' . $row['solid'] . '&borrar=1&' . $filtro ?>" class="msj_eliminar" title="eliminar"><img src="<?php echo $r ?>imagenes/iconos/basura.png" class="grayscale" /></a>
									<?php
									} else echo '<img src="' . $r . 'imagenes/iconos/basura.png" class="gray" />';
									?>
								</td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
				<p class="boton">
					<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href='consultar.php'">Atras</button>
				</p>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<?php
	if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>';
	elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>';
	?>
	<div id="modal" style="display:none"></div>
	<div id="dialog2" style="display:none"></div>
</body>

</html>