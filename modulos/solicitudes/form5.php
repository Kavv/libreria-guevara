<?php
	$r = '../../';
	require($r . 'incluir/session.php');
	require($r . 'incluir/connection.php');
	if (isset($_POST['adinota'])) {
		$empresa = $_POST['empresa'];
		$id = $_POST['id'];
		$nota = trim($_POST['nota']);
		$screen = $_POST['screen'];
		$row = $db->query("INSERT INTO hissolicitudes (hsoempresa, hsosolicitud, hsofecha, hsousuario, hsonota) VALUES ('$empresa', '$id', NOW(), '" . $_SESSION['id'] . "', '$nota')");
		header('Location:form5.php?empresa=' . $empresa . '&id=' . $id . '&screen=' . $screen);
		exit();
	} elseif (isset($_POST['finalizar'])) {
		$empresa = $_POST['empresa'];
		$id = $_POST['id'];
		$fechhoy = date("Y-m-d");
		$mensaje = $error = "";
		$asesor = $db->query("SELECT solasesor FROM solicitudes WHERE solid='$id'")->fetch(PDO::FETCH_ASSOC);
		$asesor = $asesor['solasesor'];

		/* Se actualiza la solicitud, el estado sera en proceso
		solfecha y solfechaReg comparten la misma fecha why? */
		$qry = $db->query("UPDATE solicitudes SET solestado = 'PROCESO', solfecha = '" . $fechhoy . "'  ,solobservacion = '" . $_POST['observacion'] . "' , solfechreg = NOW() WHERE solempresa = '$empresa' AND solid = '$id'");
		if($qry)
		{
			$mensaje = 'Su solicitud ha pasado a proceso';
			header('Location:listar.php?empresa=' . $empresa . '&id=' . $id.'&asesor='.$asesor);
		}
		else
		{
			$error = 'Ocurrio un error con su solicitud, intentente nuevamente o contacte al encargado';
			header('Location:form5.php?empresa=' . $empresa . '&id=' . $id . '&error=' . $error);
			exit();
		}
		
		if ($_FILES['imagen']['size'] < 1600000) {
			if (move_uploaded_file($_FILES['imagen']['tmp_name'], $r . 'pdf/solicitudes/' . $empresa . '/' . $id . '.pdf')) {
				$mensaje .= "<br/>Se anexo el PDF exitosamente"; 		
			} else $mensaje .= '<br/>No se pudo cargar el archivo';
		} else $mensaje = '<br/>El PDF excede el tamano permitido (max. 1.5MB)';

		exit();

	} elseif (isset($_POST['siguiente'])) {
		$empresa = $_POST['empresa'];
		$id = $_POST['id'];
		$asesor = $_POST['asesor'];

		//Se especifican los detalle de la direccion de cobro 
		if (isset($_POST['depcobro'])) {
			$qry = $db->query("UPDATE solicitudes SET soldepcobro = '" . $_POST['depcobro'] . "', solciucobro = '" . $_POST['ciucobro'] . "', solbarcobro = '" . $_POST['barcobro'] . "', solcobro = '" . $_POST['cobro'] . "', soltelcobro = '" . $_POST['telcobro'] . "' WHERE solid = '$id' AND solempresa = '$empresa'");
		}
		//Se especifican los detalle de la direccion de entrega 
		if (isset($_POST['depentrega'])) {
			$qry = $db->query("UPDATE solicitudes SET soldepentrega = '" . $_POST['depentrega'] . "', solciuentrega = '" . $_POST['ciuentrega'] . "', solbarentrega = '" . $_POST['barentrega'] . "', solentrega = '" . $_POST['entrega'] . "', soltelentrega = '" . $_POST['telentrega'] . "' WHERE solid = '$id' AND solempresa = '$empresa'");
		}

		/* En la solicitud se detalla las comisiones de la misma en base al asesor seleccionado
		y en base al relacionista seleccionado, esos datos son ingresado en el modulo de usuarios
		durante la creacion o edicion de un registro */

		if ($asesor != "") {
			//Retorna los datos del asesor en caso de especificarse alguno
			$row = $db->query("SELECT * FROM usuarios WHERE usuid = '$asesor'")->fetch(PDO::FETCH_ASSOC);
			// Actualiza los datos relacionados al asesor
			if ($row)
				$qry = $db->query("UPDATE solicitudes SET solcomision = '" . $row['usucomision'] . "', 
				solcomision1 = '" . $row['usucomision1'] . "', solcomision2 = '" . $row['usucomision2'] . "', 
				solcomision3 = '" . $row['usucomision3'] . "', solover1 = '" . $row['usuover1'] . "', solpover1 = '"
					. $row['usupover1'] . "', solover2 = '" . $row['usuover2'] . "', solpover2 = '" . $row['usupover2'] . "', 
				solover3 = '" . $row['usuover3'] . "', solpover3 = '" . $row['usupover3'] . "', solover4 = '"
					. $row['usuover4'] . "', solpover4 = '" . $row['usupover4']
					. "' WHERE solempresa = '$empresa' AND solid = '$id'");
		}
		if ($_POST['relacionista'] != "") {
			//Retorna los datos del relacionista en caso de especificarse alguno
			$row2 = $db->query("SELECT * FROM usuarios WHERE usuid = '" . $_POST['relacionista'] . "'")->fetch(PDO::FETCH_ASSOC);
			if ($row2)
				$qry = $db->query("UPDATE solicitudes SET solrelacionista = '"
					. $row2['usuid'] . "', solprelacionista = '" . $row2['usucomision'] . "', soljeferel = '"
					. $row2['usujeferel'] . "', solpjeferel = '" . $row2['usupjeferel']
					. "' WHERE solempresa = '$empresa' AND solid = '$id'");
		}
		// Actualizamos la forma de pago y la fecha de compromiso
		$qry = $db->query("UPDATE solicitudes SET solcompromiso = '"
			. $_POST['compromiso'] . "', solfpago = '" . $_POST['fpago']
			. "' WHERE solempresa = '$empresa' AND solid = '$id'");
			
		header('Location:form5.php?empresa=' . $empresa . '&id=' . $id . '&screen=');
		exit();

	} else {
		$empresa = $_GET['empresa'];
		$id = $_GET['id'];
		$screen = $_GET['screen'];
	}
	$row = $db->query("SELECT * FROM (solicitudes INNER JOIN clientes ON solcliente = cliid) INNER JOIN empresas ON solempresa = empid WHERE solempresa = '$empresa' AND solid = '$id'")->fetch(PDO::FETCH_ASSOC);
?>
<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<script type="text/javascript">
		$(document).ready(function() {
			$('.pdf').click(function() {
				newSrc = $(this).attr('data-rel');
				if(validation_file(newSrc) == 200)
					$('#modal').html("<iframe src='" + newSrc + "' width='100%' height='100%'></iframe>");
				else
					$('#modal').html("<spam>El PDF no fue encontrado</spam>");
				
				$('#modal').dialog({
					modal: true,
					width: '600',
					height: '800',
					title: 'PDF de la solicitud'
				});
			});

			$('.pdf').click(function() {
				newSrc = $(this).attr('data-rel');
				if(validation_file(newSrc) == 200)
					$('#modal').html("<iframe src='" + newSrc + "' width='100%' height='100%'></iframe>");
				else
					$('#modal').html("<spam>El PDF no fue encontrado</spam>");
				
				$('#modal').dialog({
					modal: true,
					width: '600',
					height: '800',
					title: 'PDF de la solicitud'
				});
			});

			$('.historico').click(function() {
				newHref = $(this).attr('data-rel');
				$('#dialog2').load(newHref).dialog({
					modal: true,
					width: 800
				});
			});
			$('.nota').click(function() {
				$('#campo').dialog({
					height: 'auto',
					width: '600px',
					modal: true
				});
			})
		});
	</script>
	<style>
		.pdf, .historico, .nota{
			cursor: pointer;
		}
	</style>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a class="current">Solicitudes</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="form5.php" method="post" enctype="multipart/form-data">
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">Solicitud Normal - Carga de archivo 
							<img src="<?php echo $r ?>imagenes/iconos/pdf.png" class="pdf" data-rel="<?php echo $r . 'pdf/solicitudes/' . $empresa . '/' . $id . '.pdf' ?>" title="Solicitud en PDF" /> <img src="<?php echo $r ?>imagenes/iconos/application_edit.png" class="historico" data-rel="notas.php?empresa=<?php echo $empresa . '&id=' . $id ?>" title="Historia de la solicitud" /> <img src="<?php echo $r ?>imagenes/iconos/add.png" class="nota" title="Agregar nota" />
						</legend>
						<p>
							<img src="<?php echo $r ?>imagenes/logos/<?php echo $empresa . "/" . $row['logo']?>" class="img-fluid" title="<?php echo $row['empnombre'] ?>">
							<input type="hidden" name="empresa" value="<?php echo $empresa ?>">
							<div align="center">Solicitud</div>
							<div align="center"><input type="text" name="id" class="pedido" value="<?php echo $id ?>" readonly /></div>
							<div align="center">Fecha</div>
							<div align="center"><input type="text" class="fecha" value="<?php echo $row['solfecha'] ?>" readonly /></div>
						</p>
						<br>
						<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12">
							<legend class="ui-widget ui-widget-header ui-corner-all">Observaciones</legend>
							<p>
								<textarea placeholder="Digite su Observacion" name="observacion" id="observacion" rows="5" class="form-control"><?php
									if ($row['solobservacion'] != '') {
										echo $row['solobservacion'];
									}
								?></textarea>
							</p>
						</fieldset>
						<br>
						<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12">
							<legend class="ui-widget ui-widget-header ui-corner-all">Digitalizacion de la solicitud y observaciones</legend>
							<p>
								<label>PDF (max. 1.5 Mb): </label>
								<!-- Le removi el requerido -->
								<input type="file" name="imagen" class="form-control validate[checkFileType[pdf|PDF]]" />
							</p>
							<p class="boton">
								<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href = 'form4.php?empresa=<?php echo $empresa . '&id=' . $id . '&screen=' . $screen ?>'">atras</button>
								<button type="submit" class="btn btn-primary btnfinalizar" name="finalizar" value="finalizar">finalizar</button>
							</p>
						</fieldset>
					</fieldset>
				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<?php
	if (isset($_GET['error'])) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['error'] . '</div>';
	?>
	<div id="campo" title="Agregar historico" style="display:none">
		<form id="form" name="form" action="form4.php" method="post">
			<input type="hidden" name="empresa" value="<?php echo $empresa ?>">
			<input type="hidden" name="id" value="<?php echo $id ?>">
			<input type="hidden" name="screen" value="<?php echo $screen ?>">
			<input type="text" name="nota" required title="Digite la nota" />
			<button type="submit" class="btn btn-success btn-block btnmas" name="adinota" value="adinota">Agregar</button>
	</div>
	<div id="modal" style="display:none"></div>
	<div id="dialog2" style="display:none"></div>
</body>

</html>