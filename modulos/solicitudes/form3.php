<?php
	$r = '../../';
	require($r . 'incluir/session.php');
	require($r . 'incluir/connection.php');
	if (isset($_POST['adinota'])) {
		$empresa = $_POST['empresa'];
		$id = $_POST['id'];
		$nota = trim($_POST['nota']);
		$screen = $_POST['screen'];
		$row = $db->query("INSERT INTO hissolicitudes (hsoempresa, hsosolicitud, hsofecha, hsousuario, hsonota) VALUES ('$empresa', '$id', NOW(), '" . $_SESSION['id'] . "', '$nota')");
		header('Location:form3.php?empresa=' . $empresa . '&id=' . $id . '&screen=' . $screen);
		exit();
	} elseif (isset($_GET['producto'])) {
		$empresa = $_GET['empresa'];
		$id = $_GET['id'];
		$screen = $_GET['screen'];
		$producto = $_GET['producto'];
		$row = $db->query("SELECT * FROM detsolicitudes WHERE detsolicitud = '$id' AND detempresa = '$empresa' AND detproducto = '$producto'")->fetch(PDO::FETCH_ASSOC);
		if($row)
		{
			$qry = $db->query("DELETE FROM detsolicitudes WHERE detsolicitud = '$id' AND detempresa = '$empresa' AND detproducto = '$producto'");
			$qry = $db->query("UPDATE solicitudes SET soltotal = soltotal - (" . $row['detcantidad'] . " * " . $row['detprecio'] . "), solbase = solbase - (" . $row['detcantidad'] . " * " . $row['detprecio'] . ") WHERE solid = '$id' AND solempresa = '$empresa'");
		}
	} elseif (isset($_POST['adiproducto'])) {
		$empresa = $_POST['empresa'];
		$id = $_POST['id'];
		$screen = $_POST['screen'];
		$producto = $_POST['producto'];
		$cantidad = $_POST['cantidad'];
		$row = $db->query("SELECT * FROM productos WHERE proid = '$producto'")->fetch(PDO::FETCH_ASSOC);
		$qry = $db->query("INSERT INTO detsolicitudes (detsolicitud, detempresa, detproducto, detcantidad, detprecio) VALUES ('$id', '$empresa', '$producto', $cantidad, " . $row['proprecio'] . ")");
		if($qry)
			$qry = $db->query("UPDATE solicitudes SET soltotal = soltotal + ($cantidad * " . $row['proprecio'] . "), solbase = solbase + ($cantidad * " . $row["proprecio"] . ") WHERE solempresa = '$empresa' AND solid = '$id'");
	} elseif (isset($_POST['siguiente'])) {
		$empresa = $_POST['empresa'];
		$id = $_POST['id'];
		$screen = $_POST['screen'];
		$cliente = $_POST['cliente'];
		$nomfamiliar = $barfamiliar = $dirfamiliar = $empfamiliar = $carfamiliar = $refnombre1 = $refnombre2 = $refnombre3 = "";
		//En caso de estar casado
		if($_POST["estado"] == 'CASADO(A)')
		{
			$nomfamiliar = trim(strtoupper($_POST['nomfamiliar']));
			$barfamiliar = trim(strtoupper($_POST['barfamiliar']));
			$dirfamiliar = trim(strtoupper($_POST['dirfamiliar']));
			$empfamiliar = trim(strtoupper($_POST['empfamiliar']));
			$carfamiliar = trim(strtoupper($_POST['carfamiliar']));
			//Actualiza los datos del cliente estableciendo los datos de su esposo(a)
			$qry = $db->query("UPDATE clientes SET clinomfamiliar = '$nomfamiliar', cliedafamiliar = '" 
			. $_POST['edafamiliar'] . "', clidepfamiliar = '" . $_POST['depfamiliar'] . "', cliciufamiliar = '" 
			. $_POST['ciufamiliar'] . "', clibarfamiliar = '$barfamiliar', clidirfamiliar = '$dirfamiliar', 
			clitelfamiliar = '" . $_POST['telfamiliar'] . "', cliempfamiliar = '$empfamiliar', clicarfamiliar = 
			'$carfamiliar', cliantfamiliar = '" . $_POST['antfamiliar'] . "', clisalfamiliar = " 
			. $_POST['salfamiliar']
			. " WHERE cliid = '$cliente'");
		}

		$refnombre1 = trim(strtoupper($_POST['refnombre1']));
		$refnombre2 = trim(strtoupper($_POST['refnombre2']));
		$refnombre3 = trim(strtoupper($_POST['refnombre3']));
		$cel1 = $tel1 = $cel2 = $tel2 = $cel3 = $tel3 = NULL;
		// Actualiza los datos del cliente establesiendo sus referencias
		if($refnombre1 != "")
		{
			if(isset($_POST['refcelular1']))
			$cel1 = $_POST['refcelular1'];
			if(isset($_POST['reftelefono1']))
			$tel1 = $_POST['reftelefono1'];
			$qry = $db->query("UPDATE clientes SET clirefnombre1 = '$refnombre1', clireftelefono1 = '" 
			. $tel1 . "', clirefcelular1 = '" . $cel1 
			. "' WHERE cliid = '$cliente'");
		}
		
		if($refnombre2 != "")
		{
			if(isset($_POST['refcelular2']))
			$cel2 = $_POST['refcelular2'];
			if(isset($_POST['reftelefono2']))
			$tel2 = $_POST['reftelefono2'];
			$qry = $db->query("UPDATE clientes SET clirefnombre2 = '$refnombre2', clireftelefono2 = '" 
			. $tel2 . "', clirefcelular2 = '" . $cel2
			. "' WHERE cliid = '$cliente'");
		}

		if($refnombre3 != "")
		{
			if(isset($_POST['refcelular3']))
			$cel3 = $_POST['refcelular3'];
			if(isset($_POST['reftelefono3']))
			$tel3 = $_POST['reftelefono3'];
			$qry = $db->query("UPDATE clientes SET clirefnombre3 = '$refnombre3', clireftelefono3 = '" 
			. $tel3 . "', clirefcelular3 = '" . $cel3 
			. "' WHERE cliid = '$cliente'");
		}
	} else {
		$empresa = $_GET['empresa'];
		$id = $_GET['id'];
		$screen = $_GET['screen'];
	}
	$row = $db->query("SELECT * FROM (solicitudes INNER JOIN clientes ON solcliente = cliid) INNER JOIN empresas ON solempresa = empid WHERE solempresa = '$empresa' AND solid = '$id'")->fetch(PDO::FETCH_ASSOC);
?>
<!doctype html>
<html>

<head>
	<?php
		require($r . 'incluir/src/head.php');
		require($r . 'incluir/src/head-form.php');
	?>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#form2').validationEngine({
				showOneMessage: true,
				onValidationComplete: function(form, status) {
					if (status) {
						carga();
						return true;
					}
				}
			});

			function valor_cuota()
			{
				var total = $('#total').val();
				var ncuota = $('#ncuota').find(':selected').val()
				var cuota = $('#cuota').val();
				if(ncuota != 0)
					var vcuota = ((total - cuota) / ncuota).toFixed(2);
				else
					var vcuota = ((total - cuota)).toFixed(2);

				$('#vcuota').val(vcuota);
			}

			$('#ncuota').change(function(event) {
				var cuota = $('#ncuota').val();
				if (cuota == 0) {
					$('#cuota').val($('#total').val());
					$('#cuota').attr('readonly', true);
				} else {
					$('#cuota').attr('readonly', false);
					predefinido = $("#cuota").data().predefinido;
					if(predefinido != "")
						$('#cuota').val(predefinido);
					else
						$('#cuota').val(0);
				}
				valor_cuota();
			});
			$('#total').keyup(function(event) {
				valor_cuota();
			});
			$('#total').change(function(event) {
				valor_cuota();
			});
			
			$('#cuota').keyup(function(event) {
				valor_cuota();
			});

			$('.pdf').click(function() {
				newSrc = $(this).attr('data-rel');
				if(validation_file(newSrc) == 200)
					$('#modal').html("<iframe src='" + newSrc + "' width='100%' height='100%'></iframe>");
				else
					$('#modal').html("<spam>El PDF no fue encontrado</spam>");
				
				$('#modal').dialog({
					modal: true,
					width: '600',
					height: '800',
					title: 'PDF de la solicitud'
				});
			});
			$('.historico').click(function() {
				newHref = $(this).attr('data-rel');
				$('#dialog2').load(newHref).dialog({
					modal: true,
					width: 800
				});
			});
			$('.nota').click(function() {
				$('#campo').dialog({
					height: 'auto',
					width: '600px',
					modal: true
				});
			});
		
			$('#producto').change(function(event){
				var id = $("#producto").find(':selected').val();
				$('#precio_producto').load('precio_producto.php?producto='+id);
			});

			$('#ncuota').change();
		});
	</script>
	<style>
		.pdf, .historico, .nota{
			cursor: pointer;
		}
	</style>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a class="current">Solicitudes</a>
			</article>
			<article id="contenido" class="d-flex justify-content-center">
				<fieldset id="field" class="ui-widget ui-widget-content ui-corner-all col-sm-12 col-md-8 col-lg-6">
					<legend class="ui-widget ui-widget-header ui-corner-all text-center">Solicitud - Hoja 3 &nbsp;<img src="<?php echo $r ?>imagenes/iconos/pdf.png" class="pdf" data-rel="<?php echo $r . 'pdf/solicitudes/' . $empresa . '/' . $id . '.pdf' ?>" title="Solicitud en PDF" /> <img src="<?php echo $r ?>imagenes/iconos/application_edit.png" class="historico" data-rel="notas.php?empresa=<?php echo $empresa . '&id=' . $id ?>" title="Historia de la solicitud" /> <img src="<?php echo $r ?>imagenes/iconos/add.png" class="nota" title="Agregar nota" /></legend>
					<form id="form" name="form" action="form3.php" method="post">
						<p>
							<img src="<?php echo $r ?>imagenes/logos/<?php echo $empresa . "/" . $row['logo']?>" class="img-fluid" title="<?php echo $row['empnombre'] ?>">
							<input type="hidden" name="empresa" value="<?php echo $empresa ?>" />
							<div align="center">Solictud</div>
							<div align="center">
								<input type="text" name="id" value="<?php echo $id ?>" class="pedido" readonly />
							</div>
							<div align="center">Fecha</div>
							<div align="center">
								<input type="text" class="fecha" value="<?php echo $row['solfecha'] ?>" readonly />
							</div>
						</p>
						<br>
						<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12">
							<legend class="ui-widget ui-widget-header ui-corner-all">Datos productos</legend>
							<p>
								<input type="hidden" name="screen" value="<?php echo $screen ?>" />
								<select id="producto" name="producto" class="validate[required] text-input" style="width:58.2em">
									<option value="">SELECCIONE</option>
									<?php
									// Retorna todos los productos con estado = 0 y 
									// los prodcustos que no se encuentren en la sub 
									// consulta la cual retorna los productos ya seleccionados 
									// en la actual solicitud de la empresa ademas el tipo del 
									// producto debe ser PT(Producto Terminado)
									$qry = $db->query("SELECT * FROM productos WHERE prodesactivado = '0' AND proid NOT IN (SELECT detproducto FROM detsolicitudes WHERE detsolicitud = '$id' AND detempresa = '$empresa') AND protipo = 'PT' ORDER BY pronombre");
									while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
										echo '<option value="' . $row2['proid'] . '">' . $row2['pronombre'] . '</option>';
									}
									?>
								</select>
								<div id="precio_producto">

								</div>
								<div class="input-group mb-3">
									<div class="input-group-prepend">
										<span class="input-group-text" >Cantidad</span>
									</div>
									<input type="text" name="cantidad" class="not-w form-control cantidad validate[required, custom[onlyNumberSp], min[1]]" value="1" />
									<div class="input-group-append">
										<button type="submit" class="btn btn-success btnmas" name="adiproducto" value="adiproducto">+</button>
									</div>
								</div>
							</p>
							<label for="" class="col-md-12" style="text-align:center!important;"><strong>Productos Agregados</strong></label>
							<?php
							$qry = $db->query("SELECT * FROM detsolicitudes INNER JOIN productos ON detproducto = proid WHERE detsolicitud = '$id' AND detempresa = '$empresa'");
							$total = 0;
							while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
							?>
								<p>
									<input type="text" value="<?php echo $row2['pronombre'] ?>" style="width:52em" readonly />
									<div class="row">
										<div class="input-group mb-1 col-md-6">
											<div class="input-group-prepend">
												<span class="input-group-text" >Código</span>
											</div>
											<input type="text" class="not-w form-control" value="<?php echo $row2['proid'] ?>" readonly />
										</div>
										<div class="input-group mb-1 col-md-6">
											<div class="input-group-prepend">
												<span class="input-group-text" >Precio</span>
											</div>
											<input type="text" class="not-w form-control" value="<?php echo $row2['proprecio'] ?>" readonly />
										</div>

									</div>
									
									<div class="input-group mb-3">
										<div class="input-group-prepend">
											<span class="input-group-text" >Cantidad</span>
										</div>
										<input value="<?php echo $row2['detcantidad'] ?>" class="cantidad not-w form-control" readonly />
										<div class="input-group-append ">
											<button type="button" name="remproducto" class="btn btn-danger btnmenos " onclick="carga(); location.href = 'form3.php?<?php echo 'empresa=' . $empresa . '&id=' . $id . '&producto=' . $row2['detproducto'] . '&screen=' . $screen ?>'">-</button>
										</div>
									</div>
								</p>
							<?php
							}
							?>
						</fieldset>
					</form>
					<br>
					<form id="form2" name="form2" action="form4.php" method="post">
						<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12">
							<input type="hidden" name="empresa" value="<?php echo $empresa ?>" />
							<input type="hidden" name="id" value="<?php echo $id ?>" />
							<input type="hidden" name="screen" value="<?php echo $screen ?>" />
							<p  class="mt-3">
								<label>Total: </label>
								<input type="text" id="total" name="total" class="form-control valor validate[required, custom[onlyNumberSp]]] text-input" value="<?php echo $row['soltotal'] ?>" /></div>
							</p>
							<p >
								<label>Base: </label>
								<input type="text" id="base" name="base" class="form-control valor validate[required, custom[onlyNumberSp]] text-input" value="<?php echo $row['solbase'] ?>" /></div>
							</p>
							<p >
								<label>Numero de cuotas:</label>
								<select name="ncuota" id="ncuota" class="form-control validate[required] text-input" style="padding-left:2em;">
									<option value="">SELECCIONE UNA CUOTA</option>
									<?php
									/* la tabla parametros tiene un registro unico que define: 
									El minimo valor de cuota y 
									La maxima cantidad de cuotas 
									El porcentaje de financiamiento */
									$row2 = $db->query('SELECT * FROM parametros')->fetch(PDO::FETCH_ASSOC);
									for ($i = 0; $i <= $row2['parncuota']; $i++)
									{
										if($row['solncuota'] != $i)
											echo '<option value="' . $i . '">' . $i . '</option>';
										else
											echo '<option value="' . $i . '" selected >' . $i . '</option>';
									}
									?>
								</select>
							</p>
							<?php
							if ($row['soltipo'] != 'LIBRANZA') {
							?>
								<p >
									<label>Cuota inicial: </label>
									<input type="text" id="cuota" name="cuota" class="form-control valor  validate[required, custom[onlyNumberSp]] text-input" data-predefinido="<?php echo $row['solcuota'] ?>" <?php if ($row['solncuota'] == '0') { ?> readonly <?php } ?> >
								</p>
							<?php
							}
							?>
							<p >
								<?php
									if($row['solncuota'] != 0)
										$vcuota = ($row['soltotal'] - $row['solcuota']) / $row['solncuota'];
									else
										$vcuota = ($row['soltotal'] - $row['solcuota']);
								?>
								<label>Valor cuota: </label>
								<input type="text" id="vcuota" name="vcuota" class="form-control valor" value="<?php echo $vcuota ?>" readonly />
							</p>
							<p class="boton">
								<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href = 'form2.php?empresa=<?php echo $empresa . '&id=' . $id . '&screen=' . $screen ?>'">atras</button>
								<button type="submit" class="btn btn-primary btnsiguiente" name="siguiente" value="siguiente">siguiente</button>
							</p>
						</fieldset>
				</fieldset>
				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<?php
	if (isset($_GET['error'])) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['error'] . '</div>';
	?>
	<div id="campo" title="Agregar historico" style="display:none">
		<form id="form" name="form" action="form3.php" method="post">
			<input type="hidden" name="empresa" value="<?php echo $empresa ?>">
			<input type="hidden" name="id" value="<?php echo $id ?>">
			<input type="hidden" name="screen" value="<?php echo $screen ?>">
			<input type="text" name="nota" required title="Digite la nota" />
			<button type="submit" class="btn btn-success btn-block btnmas" name="adinota" value="adinota">Agregar</button>
	</div>
	<div id="modal" style="display:none"></div>
	<div id="dialog2" style="display:none"></div>
</body>

</html>