<?php
	$r = '../../';
	require($r . 'incluir/session.php');
	require($r . 'incluir/connection.php');
	require($r . 'incluir/phpmailer/PHPMailer/class.phpmailer.php');
	require($r . 'incluir/phpmailer/PHPMailer/class.smtp.php');
	require($r . 'incluir/mail/send_email.php');

	if (isset($_POST['adinota'])) {
		$empresa = $_POST['empresa'];
		$id = $_POST['id'];
		$nota = trim(strtoupper($_POST['nota']));
		$screen = $_POST['screen'];
		$row = $db->query("INSERT INTO hissolicitudes (hsoempresa, hsosolicitud, hsofecha, hsousuario, hsonota) VALUES ('$empresa', '$id', NOW(), '" . $_SESSION['id'] . "', '$nota')");
		header('Location:formscreen.php?empresa=' . $empresa . '&id=' . $id . '&screen=' . $screen);
		exit();
	} elseif (isset($_POST['validar'])) {


		if ($_POST['estado'] == 'RECHAZADO') {
			// T CAUSAL NOMBRE
			$qrytcausal = $db->query("SELECT * FROM tcausales WHERE tcaid = '" . $_POST['tcausal'] . "';");
			$rowtcausal = $qrytcausal->fetch(PDO::FETCH_ASSOC);
			// CAUSAL NOMBRE
			$qrycausal = $db->query("SELECT * FROM causales WHERE cautcausal = '" . $_POST['tcausal'] . "' AND cauid = '" . $_POST['causal'] . "';");
			$rowcausal = $qrycausal->fetch(PDO::FETCH_ASSOC);

			$qry = $db->query("UPDATE solicitudes SET solestado = 'RECHAZADO SCREEN', solususcreen = '" . $_SESSION['id'] . "', soltcascreen = " . $_POST['tcausal'] . ", solcauscreen = " . $_POST['causal'] . ", solfechscreen = NOW() WHERE solempresa = '" . $_POST['empresa'] . "' AND solid = '" . $_POST['id'] . "'");
			$mensaje = 'La solicitud se rechazo';
			//Capacitador a quien se le envia el email
			$qrycapacitador = $db->query("SELECT * from usuarios INNER JOIN solicitudes on solasesor = usuid where solid = '" . $_POST['id'] . "' AND solempresa = '" . $_POST['empresa'] . "';");
			$rowcapacitador = $qrycapacitador->fetch(PDO::FETCH_ASSOC);
			$capacitador_mail = $rowcapacitador['usuemail'];

			// Identificamos el registro del usuario que esta realizando el proceso
			$rowusuario = $db->query("SELECT * FROM usuarios WHERE usuid = '".$_SESSION['id']."'")->fetch(PDO::FETCH_ASSOC);

			//Envio de Email al capacitador notificando el rechazo de la solicitud		
			$mail             = new PHPMailer();
			$body             = '	<p>Buen Dia!</p>
				<p>El presente con el fin de comunicarle que se ha rechazado la solicitud #' . $_POST['id'] . " de la empresa " . $_POST['empresa'] . ' por ' . $rowtcausal['tcanombre'] . ' - ' . $rowcausal['caunombre'] . '</p>
				<p>Por favor intertar comunicarse con el TITULAR si es necesario, o si tienen informacion adicional sobre la 
				solicitud en mencion por favor enviarla al correo ' . $rowusuario["usuemail"] . ', 
				esto para REACTIVAR SU SOLICITUD y realizar la respectiva confirmacion de datos, para envio y entrega oportuna.</p>
				<p>Cordial saludo.<p>
				<p style="margin:2px 0px;"><b>Encarcado de confirmar los creditos</b></p>
				<p style="margin:2px 0px;">Analisis de Credito</p>
				<p style="font-size:11px;">Este correo ha sido enviado automaticamente desde SEPIRO, 
				se informa ademas que los signos de puntuacion y caracteres especiales han sido omitidos 
				de manera predeterminada para evitar inconvenientes con los diferentes servidores de correo</p>';
			
			$from = $reply_to = $rowusuario["usuemail"];
			$name = "Analisis Credito";
			$subject = "Rechazo de la solicitud - #" . $_POST['id'] . "";
			$to = $capacitador_mail;
			$cc = "";
			if (send_email($from, $name, $reply_to, $subject, $body, $to, $cc)) {
				$mensaje = "Se ha rechazado la solicitud <br/>Email enviado correctamente";
				header('Location:screen.php?mensaje=' . $mensaje);
			} 
			else
			{
				$error = "Se ha rechazado la solicitud <br/>No se pudo enviar el email por la siguiente causa: " . $mail->ErrorInfo . "<br/>Si el error persiste contactar con el responsable";
				header('Location:formscreen.php?error=' . $error);
			}
		} else {
			// Se actualiza la solicitud especificando los datos del usuario que aprobo
			// Se actualizan a NULL los campos revelantes a rechazos previos de la solicitud			
			$qry = $db->query("UPDATE solicitudes SET solestado = 'APROBADO SCREEN', solususcreen = '" . $_SESSION['id'] . "', soltcascreen = NULL, solcauscreen = NULL, solfechscreen = NOW() WHERE solempresa = '" . $_POST['empresa'] . "' AND solid = '" . $_POST['id'] . "'");

			$mensaje = 'La solicitud se aprobo';
			header('Location:screen.php?mensaje=' . $mensaje);
		}

		if ($_POST['contado'] == '1') {
			$qry = $db->query("UPDATE solicitudes SET solposcontado = '1' WHERE solempresa = '" . $_POST['empresa'] . "' AND solid = '" . $_POST['id'] . "';");
		}

		// Log de acciones realizadas por usuario en la BD	
		$qrylogsregister = $db->query("SELECT * FROM usuarios INNER JOIN perfiles ON usuperfil = perid WHERE usuid = '" . $_SESSION['id'] . "'"); //verificacion usuario por ID de sesion
		$rowlogsregister = $qrylogsregister->fetch(PDO::FETCH_ASSOC);
		if ($_POST['estado'] == 'APROBADO') {
			$mensaje_log = "SE HA " . $_POST['estado'] . " LA SOLICITUD NUMERO " . $_POST['id'] . " - " . $_POST['empresa'] . " ";
		} else {
			$mensaje_log = "SE HA " . $_POST['estado'] . " LA SOLICITUD NUMERO " . $_POST['id'] . " - " . $_POST['empresa'] . " CON TIPO DE CAUSAL = " . $rowtcausal['tcanombre'] . " Y CAUSAL " . $rowcausal['caunombre'] . "";
		}
		//anexo de informacion de accion a la BD tabla LOGS
		$qrylogsregister = $db->query("INSERT INTO logs ( usuario, id_usuario , ip_usuario , accion_usuario, fecha_accion) VALUES ( '" . $rowlogsregister['usunombre'] . "', '" . $_SESSION['id'] . "', '" . $_SERVER['REMOTE_ADDR'] . "' , '" . $mensaje_log . "' , '" . date("Y-m-d H:i:s") . "' );"); 

		exit();
	} elseif (isset($_POST['siguiente'])) {
		$empresa = $_POST['empresa'];
		$id = $_POST['id'];
		$cliente = $_POST['cliente'];
		$asesor = $_POST['asesor'];
		$screen = $_POST['screen'];
		if (isset($_POST['depcobro'])) {
			$qry = $db->query("UPDATE solicitudes SET soldepcobro = '" . $_POST['depcobro'] . "', solciucobro = '" . $_POST['ciucobro'] . "', solbarcobro = '" . $_POST['barcobro'] . "', solcobro = '" . $_POST['cobro'] . "', soltelcobro = '" . $_POST['telcobro'] . "' WHERE solid = '$id' AND solempresa = '$empresa'");
		}
		if (isset($_POST['depentrega'])) {
			$qry = $db->query("UPDATE solicitudes SET soldepentrega = '" . $_POST['depentrega'] . "', solciuentrega = '" . $_POST['ciuentrega'] . "', solbarentrega = '" . $_POST['barentrega'] . "', solentrega = '" . $_POST['entrega'] . "', soltelentrega = '" . $_POST['telentrega'] . "' WHERE solid = '$id' AND solempresa = '$empresa'");
		}
		
		if ($asesor != "") {
			//Retorna los datos del asesor en caso de especificarse alguno
			$row = $db->query("SELECT * FROM usuarios WHERE usuid = '$asesor'")->fetch(PDO::FETCH_ASSOC);
			// Actualiza los datos relacionados al asesor
			if ($row)
				$qry = $db->query("UPDATE solicitudes SET solcomision = '" . $row['usucomision'] . "', 
				solcomision1 = '" . $row['usucomision1'] . "', solcomision2 = '" . $row['usucomision2'] . "', 
				solcomision3 = '" . $row['usucomision3'] . "', solover1 = '" . $row['usuover1'] . "', solpover1 = '"
					. $row['usupover1'] . "', solover2 = '" . $row['usuover2'] . "', solpover2 = '" . $row['usupover2'] . "', 
				solover3 = '" . $row['usuover3'] . "', solpover3 = '" . $row['usupover3'] . "', solover4 = '"
					. $row['usuover4'] . "', solpover4 = '" . $row['usupover4']
					. "' WHERE solempresa = '$empresa' AND solid = '$id'");
		}
		if ($_POST['relacionista'] != "") {
			//Retorna los datos del relacionista en caso de especificarse alguno
			$row2 = $db->query("SELECT * FROM usuarios WHERE usuid = '" . $_POST['relacionista'] . "'")->fetch(PDO::FETCH_ASSOC);
			if ($row2)
				$qry = $db->query("UPDATE solicitudes SET solrelacionista = '"
					. $row2['usuid'] . "', solprelacionista = '" . $row2['usucomision'] . "', soljeferel = '"
					. $row2['usujeferel'] . "', solpjeferel = '" . $row2['usupjeferel']
					. "' WHERE solempresa = '$empresa' AND solid = '$id'");
		}
		// Actualizamos la forma de pago y la fecha de compromiso
		$qry = $db->query("UPDATE solicitudes SET solcompromiso = '"
			. $_POST['compromiso'] . "', solfpago = '" . $_POST['fpago']
			. "' WHERE solempresa = '$empresa' AND solid = '$id'");

	}
	else
	{
		$empresa = $_GET['empresa'];
		$id = $_GET['id'];
		$screen = $_GET['screen'];
	}
	$row = $db->query("SELECT * FROM (solicitudes INNER JOIN clientes ON solcliente = cliid) INNER JOIN empresas ON solempresa = empid WHERE solempresa = '$empresa' AND solid = '$id'")->fetch(PDO::FETCH_ASSOC);
?>
<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<script type="text/javascript">
		function habilitar(valor) {
			if (valor == 'RECHAZADO') {
				document.getElementById("tcausal").disabled = false;
				document.getElementById("causal").disabled = false;
			} else {
				document.getElementById("tcausal").disabled = true;
				document.getElementById("causal").disabled = true;
			}
		}

		function notacontadoaler(resp) {
			if (resp == '1') {
				$('#campo').dialog({
					height: 'auto',
					width: 680,
					modal: true
				});
			}
		}

		$(document).ready(function() {

			$('#tcausal').change(function(event) {
				var id1 = $('#tcausal').find(':selected').val();
				$('#causal').load('<?php echo $r ?>incluir/carga/causales.php?id1=' + id1);
			});
			$('.pdf').click(function() {
				newSrc = $(this).attr('data-rel');
				if(validation_file(newSrc) == 200)
					$('#modal').html("<iframe src='" + newSrc + "' width='100%' height='100%'></iframe>");
				else
					$('#modal').html("<spam>El PDF no fue encontrado</spam>");
				
				$('#modal').dialog({
					modal: true,
					width: '600',
					height: '800',
					title: 'PDF de la solicitud'
				});
			});
			$('.historico').click(function() {
				newHref = $(this).attr('data-rel');
				$('#dialog2').load(newHref).dialog({
					modal: true,
					width: 800
				});
			});
			$('.nota').click(function() {
				$('#campo').dialog({
					height: 'auto',
					width: 680,
					modal: true
				});
			})


		});

	</script>
	<style type="text/css">
		#antecedentes_bo {
			color: black;
			display: inline-block;
			width: 200px;
			text-align: center;
		}

		#antecedentes_bo:hover {
			font-weight: bold;
			font-size: 11px;
		}

		.pdf, .historico, .nota{
			cursor: pointer;
		}
	</style>
</head>
<?php
	if (isset($_POST['adinota'])) {/* SE TUVO QUE QUEMAR DE ESE MODO PARA QUE CARGARA SI NO SE HACE 18/09/2014 */
	?>

	<body onload="document.forms['form']['contado'].value = '1'">
	<?php
	}/* SE TUVO QUE QUEMAR DE ESE MODO PARA QUE CARGARA SI NO SE HACE 18/09/2014 */
	?>

	<body>
		<?php require($r . 'incluir/src/login.php') ?>
		<section id="principal">
			<?php require($r . 'incluir/src/cabeza.php') ?>
			<?php require($r . 'incluir/src/menu.php') ?>
			<article id="cuerpo">
				<article class="mapa">
					<a href="#">Principal</a>
					<div class="mapa_div"></div><a class="current">Solicitudes</a>
				</article>
				<article id="contenido">
					<form id="form" name="form" action="formscreen.php" method="post">
						<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
							<legend class="ui-widget ui-widget-header ui-corner-all">Validar screen 
								<img src="<?php echo $r ?>imagenes/iconos/pdf.png" class="pdf" data-rel="<?php echo $r . 'pdf/solicitudes/' . $empresa . '/' . $id . '.pdf' ?>" title="Solicitud en PDF" /> 
								<img src="<?php echo $r ?>imagenes/iconos/application_edit.png" class="historico" data-rel="notas.php?empresa=<?php echo $empresa . '&id=' . $id ?>" title="Historia de la solicitud" /> 
								<img src="<?php echo $r ?>imagenes/iconos/add.png" class="nota" title="Agregar nota" />
							</legend>
							<p>
								<img src="<?php echo $r ?>imagenes/logos/<?php echo $empresa . "/" . $row['logo']?>" class="img-fluid" title="<?php echo $row['empnombre'] ?>">
								<input type="hidden" name="empresa" value="<?php echo $empresa ?>" />
								<input type="hidden" id="cliente" name="cliente" value="<?php echo $row['cliid'] ?>">
								<div align="center">Solicitud</div>
								<div align="center"><input type="text" class="pedido" name="id" value="<?php echo $id ?>" readonly /></div>
								<div align="center">Fecha</div>
								<div align="center"><input type="text" class="fecha" value="<?php echo $row['solfecha'] ?>" readonly /></div>
							</p>
							<br>
							
							<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12">
								<legend class="ui-widget ui-widget-header ui-corner-all">Aprobacion de la solicitud</legend>
								<p>
									<label class="label">Posible Contado: </label>
									<select name="contado" id="contado" class="nota_contado" class="validate[required]" onChange="notacontadoaler(this.value)">
										<option value="">SELECCIONE</option>
										<option value="1">SI</option>
										<option value="0">NO</option>
									</select>
								</p>
								<p>
									<label class="label">Aprobacion: </label>
									<select id="estado" name="estado" class="validate[required]" onChange="habilitar(this.value)">
										<option value="">SELECCIONE</option>
										<option value="APROBADO">APROBADO</option>
										<option value="RECHAZADO">RECHAZADO</option>
									</select>
									<p>
										<label class="label">Tipo de causal:</label>
										<select id="tcausal" name="tcausal" class="validate[required]" disabled>
											<option value="">SELECCIONE</option>
											<?php
											$qry = $db->query("SELECT * FROM tcausales WHERE tcaestado = 0 ORDER BY tcanombre");
											while ($row2 = $qry->fetch(PDO::FETCH_ASSOC))
												echo '<option value="' . $row2["tcaid"] . '">' . $row2['tcanombre'] . '</option>';
											?>
										</select>
									</p>
									<p>
										<label class="label">Causal:</label>
										<select id="causal" name="causal" class="validate[required]" disabled>
											<option value="">SELECCIONE</option>
										</select>
									</p>
									<p class="boton">
										<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href = 'form4.php?empresa=<?php echo $empresa . '&id=' . $id . '&screen=' . $screen ?>'">atras</button>
										<button type="submit" class="btn btn-primary btnvalidar" name="validar" value="validar">Validar</button>
									</p>
								</p>
							</fieldset>
						</fieldset>
					</form>
				</article>
			</article>
			<?php require($r . 'incluir/src/pie.php') ?>
		</section>
		<?php
		if (isset($_GET['error'])) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['error'] . '</div>';
		?>
		<div id="campo" title="Agregar historico" style="display:none">
			<form id="form" name="form" action="formscreen.php" method="post">
				<input type="hidden" name="empresa" value="<?php echo $empresa ?>">
				<input type="hidden" name="id" value="<?php echo $id ?>">
				<input type="hidden" name="screen" value="<?php echo $screen ?>">
				<input type="text" name="nota" required title="Digite la nota" />
				<button type="submit" class="btn btn-success btn-block btnmas" name="adinota" value="adinota">Agregar</button>
			</form>
		</div>
		<div id="modal" style="display:none"></div>
		<div id="dialog2" style="display:none"></div>
	</body>

</html>