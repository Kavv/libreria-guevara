<?php
$r = '../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');

if(isset($_POST['consultar'])){
	$id1 = $_POST['id1'];
	$fecha1 = $_POST['fecha1'];
	$fecha2 = $_POST['fecha2'];
}else{
	$id1 = $_GET['id1'];
	$fecha1 = $_GET['fecha1'];
	$fecha2 = $_GET['fecha2'];
}

$filtro = 'fecha1='.$fecha1.'&fecha2='.$fecha2.'&id1='.$id1;

$qry1 = $db->query("SELECT * FROM soportes WHERE soporid = '".$id1."';");
$row1 = $qry1->fetch(PDO::FETCH_ASSOC);


?>
<!doctype html>
<html>
<head>
<title>. : : S o p h y a : : .</title>
<meta charset="utf-8" />
<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/css/normalize.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/jquery/css/smoothness/jquery-ui-1.10.3.custom.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/css/css.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/css/menu.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/validation/css/validationEngine.jquery.css" />
<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/js/jquery-ui-1.10.3.custom.js"></script>
<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>
<script type="text/javascript" src="<?php echo $r ?>incluir/validation/js/languages/jquery.validationEngine-es.js"></script>
<script type="text/javascript" src="<?php echo $r ?>incluir/validation/js/jquery.validationEngine.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#form').validationEngine({
		showOneMessage:true,
		onValidationComplete: function(form, status){
			if(status == true) {
				carga();
				return true;
			}
		}
	});
	$('#fecha').datepicker({
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		minDate: '0D',
	});
	$('#fecha2').datepicker({
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		minDate: '0D',
	});
	
	$('.btnmodificar').button({ icons: { primary: 'ui-icon ui-icon-pencil' }});
});
</script>
<style type="text/css">
#form form{ width:350px }
#form fieldset{ padding:10px; display:block; width:350px; margin:20px auto }
#form legend{ font-weight: bold; margin-left:5px; padding:5px }
#form label{ display:inline-block; text-align:right;  margin:0.3em 2% 0 0 }
#form p{ margin:5px 0 }
</style>
</head>
<body>
<?php require($r.'incluir/src/login.php') ?>
<section id="principal">
<?php require($r.'incluir/src/cabeza.php') ?>
<?php require($r.'incluir/src/menu.php') ?>
<article id="cuerpo">
<article class="mapa">
<a href="#">Principal</a><div class="mapa_div"></div><a href="#">Solicitudes</a><div class="mapa_div"></div><a class="current">Modificar Soporte</a>
</article>
<article id="contenido">
<form id="form" name="form" action="listarsopor.php?modificar=1&<?php echo $filtro; ?>" method="get">
<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
<legend class="ui-widget ui-widget-header ui-corner-all">Modificar Fecha de soporte</legend>

	<input type="hidden" value ="<?php echo $fecha1 ?>" name="fecha1" />
	<input type="hidden" value ="<?php echo $fecha2 ?>" name="fecha2" />
	<input type="hidden" value ="<?php echo $id1 ?>" name="id1" />
	<p>
	<label style="width:140px;" for="fechaactual"> Fecha Actual: </label>
	<input type="text" name="fechaactual" id="fechaactual" value="<?php echo $row1['soporfechasoporte'] ?>" class="validate[required]" readonly />
	</p>
	<p>
	<label style="width:160px;" for="hora">Nueva Hora de soporte: </label>
	<input type="time" name="hora" id="hora" value="" max="23:59:59" min="00:00:01" class="validate[required]" step="1">
	</p>
	<p>
	<label style="width:160px;" for="fecha">Nueva Fecha de soporte: </label>
	<input type="text" name="fecha" id="fecha" class="validate[required] text-input fecha" />
	</p>
	<br>
	<p class="boton">
	<button type="button" class="btnatras" onClick="carga(); location.href='listarsopor.php?<?php echo $filtro ?>'">Atras</button>  <!-- BOTON ATRAS -->
	<button type="submit" class="btn btn-primary btnmodificar" name="modificar" value="modificar">Modificar</button>  <!-- BOTON MODIFICAR -->
	</p>

</fieldset>
</form>
</article>
</article>
<?php require($r.'incluir/src/pie.php') ?>
</section>
</body>
</html>