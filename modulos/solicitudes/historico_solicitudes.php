<?php
$r = '../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

if(isset($_GET['id']))
	$id = $_GET['id'];
if(isset($_GET['empresa']))
	$empresa = $_GET['empresa'];

if (isset($_POST['adinota'])) {
	$empresa = $_POST['empresa'];
	$id = $_POST['id'];
	$nota = trim(strtoupper($_POST['nota']));
	$row = $db->query("INSERT INTO hissolicitudes (hsoempresa, hsosolicitud, hsofecha, hsousuario, hsonota) VALUES ('$empresa', '$id', NOW(), '" . $_SESSION['id'] . "', '$nota')");
	header("Location:historico_solicitudes.php?id=$id&empresa=$empresa");
	exit();
}	

if(isset($_GET['eliminar']))
{
	$comment =  $db->query("SELECT * FROM hissolicitudes where hsoid = ".$_GET['eliminar'])->fetch(PDO::FETCH_ASSOC);
	if($comment)
	{
		$id = $comment['hsosolicitud'];
		$empresa = $comment['hsoempresa'];
		//Eliminamos el comentario
		$db->query("DELETE FROM hissolicitudes WHERE hsoid = ".$_GET['eliminar']);
		header("Location:historico_solicitudes.php?id=$id&empresa=$empresa");
		exit();
	}
}

?>
<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
	<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
	
	<script>
		$(document).ready(function() {
			$("#menu").menu();
		});

		$(document).ready(function() {
			$('#tabla').dataTable({
				'bJQueryUI': true,
				'sPaginationType': 'full_numbers',
				'oLanguage': {
					'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
				},
				'bSort': false,
			});
			$('.nota').click(function() {
				$('#campo').dialog({
					height: 'auto',
					width: '600px',
					modal: true
				});
			});
			
			$('.msj_eliminar').click(function(e) {
				e.preventDefault();
				var targetUrl = $(this).attr('href');
				var $dialog_link_follow_confirm = $('<div></div>').html("<p><span class='ui-icon ui-icon-alert' style='float:left; margin:2px 7px 7px 0;'></span>Esta seguro que desea eliminar el comentario?</p>").
				dialog({
					title: 'Porfavor confirmar',
					buttons: {
						'Si': function() {
							window.location.href = targetUrl;
						},
						'No': function() {
							$(this).dialog("close");
						}
					},
					modal: true,
					width: 'auto',
					height: 'auto'
				});
				$dialog_link_follow_confirm.dialog("open");
			});
		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article id="contenido">
				<h2>Historico Solicitud #<?php echo $id ?></h2>
				<div class="reporte">
					<button title="Agregar Nueva Nota" class="btn btn-success nota">Agregar Nota</button>
				</div>
				<?php $qry = $db->query("SELECT * FROM hissolicitudes WHERE hsosolicitud = '$id' AND hsoempresa = '$empresa';");?>
				<table id="tabla">
					<thead>
						<tr>
							<th>Fecha</th>
							<th>Usuario</th>
							<th>Nota</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php
						while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
						?>
							<tr>
								<td align="center"> <?php echo substr($row['hsofecha'], 0, 11) ?></td>
								<?php
								$rownombre = $db->query("SELECT * FROM usuarios WHERE usuid = '" . $row['hsousuario'] . "';")->fetch(PDO::FETCH_ASSOC);
								?>
								<td ><?php echo $rownombre['usunombre'] ?></td>
								<td > <?php echo $row['hsonota'] ?> </td>
								<td><a href="historico_solicitudes.php?eliminar=<?php echo $row['hsoid']?>" class="msj_eliminar" title="eliminar"><img src="<?php echo $r ?>imagenes/iconos/basura.png" class="grayscale" /></a></td>
							</tr>
						<?php
						}
						?>
					</tbody>
				</table>

			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<div id="campo" title="Agregar historico" style="display:none">
		<form id="form" name="form" action="historico_solicitudes.php?id=<?php echo $id . '&empresa=' . $empresa; ?>" method="post">
			<input type="hidden" name="empresa" value="<?php echo $empresa ?>">
			<input type="hidden" name="id" value="<?php echo $id ?>">
			<input type="text" name="nota" required title="Digite la nota" />
			<button type="submit" class="btn btn-success btn-block btnmas" name="adinota" value="adinota">Agregar</button>
	</div>
	<div id="modal" style="display:none"></div>
</body>

</html>