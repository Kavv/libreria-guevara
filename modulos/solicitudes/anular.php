<?php
	$r = '../../';
	require($r . 'incluir/session.php');
	require($r . 'incluir/connection.php');
	if (isset($_POST['anular'])) {
		$empresa = $_POST['empresa'];
		$id = $_POST['id'];

		$row = $db->query("SELECT * FROM solicitudes WHERE solid = '$id' AND solempresa = '$empresa' ");
		if($row->rowCount() > 0)
		{
			$aux = $db->query("UPDATE solicitudes SET solestado = 'ANULADA' WHERE solid = '$id' AND solempresa = '$empresa' ");
			if($aux)
			{
				if ($_FILES['imagen']['name']) {
					if ($_FILES['imagen']['size'] < 1600000) {
						move_uploaded_file($_FILES['imagen']['tmp_name'], $r . 'pdf/solicitudes/' . $empresa . '/' . $id . '.pdf');
					} else {
						$error = 'la digitalizacon excede el tamano permitido (max. 1.5MB)';
						header('Location:anular.php?error=' . $error);
						exit();
					}
				}
			}
		}

		$mensaje = 'La solicitud se ha anulado';
		header('Location:anular.php?mensaje=' . $mensaje);
		exit();
	}
?>
<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<script>
			var prueba;
		$(document).ready(function(){
			$("#verificar").click(function(e){
				prueba = $(this);
				if($("#empresa").val() == "" || $("#soli").val() == "")
					e.preventDefault();
				else
				{
					var redireccion = $(this).prop("href");
					redireccion += "?anular=1&id1=" + $("#empresa").val() + "&id2=" + $("#soli").val();
					$(this).prop("href", redireccion);
				}
	
			});

		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a class="current">Solicitudes</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="anular.php" method="post" enctype="multipart/form-data">
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">Anular solicitud</legend>
						<p>
							<label for="empresa">Empresa:</label>
							<select id="empresa" name="empresa" class="validate[required]">
								<option value="">SELECCIONE</option>
								<?php
								$qry = $db->query("SELECT * FROM empresas ORDER BY empnombre");
								while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
									echo '<option value=' . $row['empid'] . '>' . $row['empnombre'] . '</option>';
								}
								?>
							</select>
						</p>
						<p>
							<label for="id">Solicitud: </label>
							<input type="text" id="soli" name="id" class="pedido validate[required, custom[onlyNumberSp]] text-input" title="Digite numero de solicitud" />
						</p>
						<div class="row d-flex justify-content-center">
							<a id="verificar" href="ver.php" target="_blank" class="btn btn-info text-white"> Ver detalle de la solicitud </a>
						</div>
						<p>
							<label for="imagen">Digitalizacion PDF (max. 1.5Mb): </label>
							<input type="file" name="imagen" class="validate[checkFileType[pdf|PDF]]" />
						</p>
						<p class="boton">
							<button type="submit" class="btn btn-primary btnanular" name="anular" value="anular">Anular</button>
						</p>
					</fieldset>
				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<?php
	if (isset($_GET['error'])) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['error'] . '</div>';
	elseif (isset($_GET['mensaje'])) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['mensaje'] . '</div>';
	?>
</body>

</html>