<?php
$r = '../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
$tipo = $_GET['tipo']
?>
<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#fecha').datepicker({
				dateFormat: 'yy-mm-dd',
			}).keypress(function(event) {
				event.preventDefault()
			});
			$("#empresa").change(function(){
				$.get('ajax/solicitud.php?empresa='+$("#empresa").val(), function(res) {
					$("#sol").val(res);
				});
			});
			
		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Solicitudes</a>
				<div class="mapa_div"></div><a class="current">Normales</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="form1.php" method="post">
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">Insertar solicitud</legend>
						<input type="hidden" name="tipo" value="<?php echo $tipo ?>" />
						<input type="hidden" name="insertar" value="insertar" />
						<p>
							<label for="empresa">Empresa:</label>
							<select id="empresa" name="empresa" class="validate[required]">
								<option value="">SELECCIONE</option>
								<?php
								$qry = $db->query("SELECT * FROM empresas ORDER BY empnombre");
								while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
									echo '<option value=' . $row['empid'] . '>' . $row['empnombre'] . '</option>';
								}
								?>
							</select>
						</p>
						<p>
							<label for="id">Solicitud: </label>
							<input id="sol" readonly type="text" name="id" class="pedido validate[required, custom[onlyNumberSp], ajax[Solicitud]] text-input" title="Digite numero de solicitud" />
						</p>
						
						<p id="ide">
							<label for="cliente">Cedula/RUC:</label> 
							<input type="text" name="cliente" class="id validate[required] text-input" title="Digite la cedula del cliente" />
						</p>
						<p>
							<label for="tid">Fecha: </label>
							<input autocomplete="off" type="text" id="fecha" name="fecha" class="fecha validate[required]" title="Seleccione la fecha de la solicitud" /></td>
						</p>
						<div class="boton">
							<button id="envio" type="submit" class="btn btn-primary btninsertar" >Insertar</button>
						</div>
					</fieldset>
				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<?php
	if (isset($_GET['error'])) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['error'] . '</div>';
	elseif (isset($_GET['mensaje'])) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['mensaje'] . '</div>';
	?>
</body>

</html>