<?php
	$r = '../../';
	require($r . 'incluir/session.php');
	require($r . 'incluir/connection.php');
	if (isset($_POST['validar'])) {
		if ($_POST['estado'] == 'RECHAZADO') {
			$qry = $db->query("UPDATE solicitudes SET solestado = 'RECHAZADO SCREEN', solususcreen = " . $_SESSION['id'] . ", soltcascreen = " . $_POST['tcausal'] . ", solcauscreen = " . $_POST['causal'] . ", solfechscreen = NOW() WHERE solempresa = " . $_POST['empresa'] . " AND solid = '" . $_POST['id'] . "'");
			$mensaje = 'La solicitud se rechazo';
		} else {
			$qry = $db->query("UPDATE solicitudes SET solestado = 'APROBADO SCREEN', solususcreen = " . $_SESSION['id'] . ", solfechscreen = NOW() WHERE solempresa = " . $_POST['empresa'] . " AND solid = '" . $_POST['id'] . "'");
			$mensaje = 'La solicitud se aprobo';
		}
	}

	if(isset($_GET['mensaje']))
		$mensaje = $_GET['mensaje'];
	if(isset($_GET['error']))
		$error = $_GET['error'];


	$qry = $db->query(
		"SELECT * FROM (
			(
				(
					(solicitudes INNER JOIN empresas ON solempresa = empid) 
					INNER JOIN clientes ON solcliente = cliid
				) INNER JOIN departamentos ON soldepcobro = depid
			) INNER JOIN ciudades ON soldepcobro = ciudepto AND solciucobro = ciuid
		) INNER JOIN usuarios ON solasesor = usuid WHERE solestado = 'PROCESO' ORDER BY solfechreg"
	);
?>
<!doctype html>
<html>

<head>
	<meta charset="utf-8">
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>

	<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
	<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#tabla').dataTable({
				'bJQueryUI': true,
				'sPaginationType': 'full_numbers',
				'oLanguage': {
					'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
				},
				'bSort': false,
			});
			$('.pdf').click(function(e) {
				e.preventDefault();
				newSrc = $(this).attr('href');
				if(validation_file(newSrc) == 200)
					$('#modal').html("<iframe src='" + newSrc + "' width='100%' height='100%'></iframe>");
				else
					$('#modal').html("<spam>El PDF no fue encontrado</spam>");
					
				$('#modal').dialog({
					modal: true,
					width: '600',
					height: '800',
					title: 'PDF de la solicitud'
				});
			});
		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Solicitudes</a>
				<div class="mapa_div"></div><a class="current">Screens</a>
			</article>
			<article id="contenido">
				<h2>Listado de solicitudes para hacer screen</h2>
				<table id="tabla">
					<thead>
						<tr>
							<th>Empresa</th>
							<th>Solicitud</th>
							<th>Cliente</th>
							<th>Asesor</th>
							<th>F.Sol</th>
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php
						while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
						?>
							<tr>
								<td title="<?php echo $row['empid'] . ' ' . $row['empdigito'] ?>"><?php echo $row['empnombre'] ?></td>
								<td align="center"><?php echo $row['solid'] ?></td>
								<td title="<?php echo $row['cliid'] . ' ' . $row['clidigito'] ?>"><?php echo $row['clinombre'] . ' ' . $row['clinom2'] . ' ' . $row['cliape1'] . ' ' . $row['cliape2'] ?></td>
								<td title="<?php echo $row['usuid'] ?>"><?php echo $row['usunombre'] ?></td>
								<td align="center"><img src="<?php echo $r ?>imagenes/iconos/date.png" title="<?php echo $row['solfecha'] ?>" /></td>
								<td align="center"><a href="<?php echo $r . 'pdf/solicitudes/' . $row['solempresa'] . '/' . $row['solid'] . '.pdf' ?>" class="pdf"><img src="<?php echo $r ?>imagenes/iconos/pdf.png" title="Solicitud en PDF" /></a></td>
								<td align="center"><a href="form1.php?empresa=<?php echo $row['solempresa'] . '&id=' . $row['solid'] . '&screen=1' ?>" onClick="carga()" title="validar"><img src="<?php echo $r ?>imagenes/iconos/tick.png" class="grayscale" /></a></td>
							</tr>
						<?php
						}
						?>
					</tbody>
				</table>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<?php
	if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>';
	elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>';
	?>
	<div id="modal" style="display:none"></div>
</body>

</html>