<?php
	// LA ACCION PRINCIPAL ES ESTA PAGINA ES CONSULTAR UNA SOLICITUD PARA VALIDAR QUE SE RECIBIO Y SE ENCUENTRA LA SOLICITUD EN FISICO
	$r = '../../';
	//INCLUIR SESION Y CONECCION
	require($r . 'incluir/session.php');
	require($r . 'incluir/connection.php');
	?>
<!doctype html>
<html>

<head>

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>

</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
		<?php require($r . 'incluir/src/menu.php') ?>
		<!-- INCLUIMOS MENU PRINCIPAL -->
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Solicitudes</a>
				<div class="mapa_div"></div><a class="current">Fisicos</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="fisicos.php" method="post">
					<!-- ENVIO FORMULARIO POR POST A FISICOS.PHP -->
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">Busqueda de solicitudes fisicas</legend>
						<p>
							<label for="id">Solicitud:</label>
							<!-- CAMPO NUM DE SOLICITUD, SI NO SE DIGITA NADA EN EL PROCESAMIENTO DEL 
							FORMULARIO SE MOSTRARAN TODAS LAS SOLICITUDES QUE ESTAN PENDIENTES POR SER 
							VALIDADAS FISICAMENTE -->
							<input type="text" name="id" class="pedido validate[custom[onlyNumberSp]] text-input" title="Digite numero de solicitud" /> 
						</p>
						<div class="row">
							<div class="col-md-12 col-lg-6">
								<button type="submit" class="btnconsulta btn btn-primary btn-block mt-2" name="consultar" value="Buscar">consultar</button> <!-- BOTON CONSULTAR -->
							</div>
							<div class="col-md-12 col-lg-6">
								<button type="submit" class="btnconsulta btn btn-primary btn-block mt-2" name="show-all" value="Buscar">Mostrar Todo</button> </div>
						</div>
					</fieldset>
				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
		<!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
	</section>
	<?php
	if ($_GET['mensaje']) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['mensaje'] . '</div>'; // MENSAJE ERROR EN LA VENTANA MODAL
	?>
</body>

</html>