<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

$nombre = $_GET['nombre'];
$estado = $_GET['estado'];
$fecha1 = $_GET['fecha1'];
$fecha2 = $_GET['fecha2'];

$filtro = 'nombre=' . $nombre . '&estado=' . $estado . '&fecha1=' . $fecha1 . '&fecha2=' . $fecha2;

if (isset($_GET['rela-menos']) && $_GET['rela_menos']) {

    $relacionista = $_GET['relacionista'];
    $campana = $_GET['campana'];

    $qry = $db->query("DELETE FROM campanarelacionista WHERE idcampana = '$campana' AND idrelacionista = '$relacionista'");

    header('Location:asignargrupo.php?' . $filtro . '&campana=' . $campana);
    die();
} elseif (isset($_POST['agregarrelacionista']) && $_POST['agregarrelacionista']) {

    $campana = $_POST['campana'];
    $grupo = $_POST['grupo'];

    $qrygrupo = $db->query("SELECT * FROM gruposcall WHERE gruponombre = '" . $grupo . "' AND grupointegrantes NOT IN (SELECT idrelacionista FROM campanarelacionista WHERE idcampana = '" . $campana . "')");
    $numgrupo = $qrygrupo->rowCount();
    if ($numgrupo > 0) {
        while ($rowgrupo = $qrygrupo->fetch(PDO::FETCH_ASSOC)) {
            $qry = $db->query("INSERT INTO campanarelacionista (idrelacionista, idcampana) VALUES ('" . $rowgrupo['grupointegrantes'] . "', '" . $campana . "');");
        }
    } else {
        $error = "Actualmente todas las relacionistas de este grupo se encuentran cargadas a esta camapaña!";
        header('Location:asignargrupo.php?' . $filtro . '&error=' . $error . '&campana=' . $campana);
    }


    header('Location:asignargrupo.php?' . $filtro . '&campana=' . $campana);

} else {
    if (isset($_GET['campana'])) {
        $campana = $_GET['campana'];
    } else {

        $error = 'Necesita una campana que editar';
        header('Location:' . $r . 'index2.php?error=' . $error . '');
        die();
    }
}

$row = $db->query("SELECT * FROM campana WHERE campaid = '" . $campana . "'")->fetch(PDO::FETCH_ASSOC);
$row1 = $db->query("SELECT * FROM campanarelacionista WHERE idcampana = '" . $campana . "'")->fetch(PDO::FETCH_ASSOC);
?>
<!doctype html>
<html>
<head>
    < <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>
</head>
<body>
<?php require($r . 'incluir/src/login.php') ?>
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <?php require($r . 'incluir/src/menu.php') ?>
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a href="#">Call Center</a>
            <div class="mapa_div"></div>
            <a class="current">Grupo a la campaña</a>
        </article>
        <article id="contenido">
            <h2 align="center"> Campaña - <?php echo $row['campanombre'] ?> </h2>
            <form id="form" name="form" action="asignargrupo.php?campana=<?php echo $campana . "&" . $filtro ?>"
                  method="post">

                <fieldset id="field" class="ui-widget ui-widget-content ui-corner-all col-md-6">
                    <legend class="ui-widget ui-widget-header ui-corner-all">Añadir grupo a la campaña</legend>
                    <br>
                    <br>
                    <p align="center">
                    <div class="input-group not-w">
                        <select name="grupo" class="custom-select validate[required] text-input not-w">
                            <option value="">SELECCIONE</option>
                            <?php
                            $qry = $db->query("SELECT * FROM gruposcall GROUP BY gruponombre ORDER BY gruponombre");
                            while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
                                echo '<option value="' . $row2['gruponombre'] . '">' . $row2['gruponombre'] . '</option>';
                            }
                            ?>
                        </select>
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-success"
                                          name="agregarrelacionista"
                                          value="agregarrelacionista">+
                            </button>
                        </div>
                    </div>
                    </p>
                    <input type="hidden" name="campana" value="<?php echo $campana ?>"/>
                    <br>
                    <br>
                    <fieldset class="ui-widget ui-widget-content ui-corner-all">
                        <legend class="ui-widget ui-widget-header ui-corner-all">Relacionistas Asignadas</legend>
                        <?php
                        $qry = $db->query("SELECT * FROM campanarelacionista INNER JOIN usuarios ON idrelacionista = usuid WHERE idcampana = '$campana' order by usunombre ");
                        $numresul = $qry->rowCount();
                        while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
                            ?>
                            <p align="center">
                            <div class="input-group ">
                                <input type="text" class="form-control not-w" name="usuarioasig"
                                       value="<?php echo $row2['usunombre'] ?>"
                                       readonly/>

                                <div class="input-group-append">
                                    <button type="button" class="btn btn-danger"
                                            onclick="carga(); location.href = 'asignargrupo.php?<?php echo 'campana=' . $campana . '&relacionista=' . $row2['usuid'] . '&rela_menos=aIUOfHbHjFHoLm48129692jF45n' ?>'">
                                        -
                                    </button>
                                </div>
                            </div>
                            </p>
                            <?php
                        }
                        if ($numresul == 0) {
                            echo "<p>Esta campaña actualmente no posee relacionistas asignadas, por favor revise y asigne relacionistas si es pertinente.</p>";
                        }
                        ?>
                    </fieldset>
                    <br>
                    <p class="boton">
                        <button type="button" class="btn btn-outline-primary btnatras"
                                onClick="carga(); location.href='listar_cam.php?<?php echo $filtro ?>'">
                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-left-square-fill"
                                 fill="currentColor"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                      d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm9.5 8.5a.5.5 0 0 0 0-1H5.707l2.147-2.146a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708-.708L5.707 8.5H11.5z"/>
                            </svg>
                            Atras
                        </button> <!-- BOTON PARA VOLVER A CONSULTAR -->
                    </p>
                </fieldset>

            </form>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php'); ?>
</section>
<?php
if (isset($_GET['error'])) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['error'] . '</div>';
?>
<div id="modal" style="display:none"></div>
<div id="dialog2" style="display:none"></div>
</body>
</html>