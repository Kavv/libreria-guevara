<?php
// LA ACCION PRINCIPAL DE ESTA PAGINA ES MODIFICAR UN PROSPECTO ENVIANDO LA INFORMACION A LISTAR.PHP

$r = '../../../';
//INCLUIR SESION Y CONECCION
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

if (!isset($_GET['id'])) {

    $error = 'Necesita prospecto que editar';
    header('Location:' . $r . 'listar.php?error=' . $error . '');
    die();
}

$row = $db->query("SELECT * FROM prospectos WHERE proid = " . $_GET['id'])->fetch(PDO::FETCH_ASSOC); // CONSULTA A LA BD CON VARUABLES RECIBIDAS POR GET
$filtro = 'nombre=' . $_GET['nombre'] . '&telefonos=' . $_GET['telefonos'] . '&celulares=' . $_GET['celulares'];

?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">

    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>
    <script>
        $(document).ready(function () {
            $('#depresidencia').change(function (event) {
                var id1 = $('#depresidencia').find(':selected').val();
                $('#ciuresidencia').load('<?php echo $r ?>incluir/carga/ciudades.php?id1=' + id1);
            });
        });
    </script>
</head>
<body>
<?php require($r . 'incluir/src/login.php') ?> <!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
    <?php require($r . 'incluir/src/menu.php') ?> <!-- INCLUIMOS MENU PRINCIPAL -->
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a href="#">Call Center</a>
            <div class="mapa_div"></div>
            <a class="current">Modificar</a>
        </article>
        <article id="contenido">
            <div class="ui-widget">
                <form id="form" name="form" action="listar.php?<?php echo $filtro ?>" method="post">
                    <!-- FORMULARIO ENVIADO POR POST A LISTAR.PHP JUNTO CON EL FILTRO GENERAL  -->
                    <h2>Modificar Prospecto</h2>
                    <fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
                        <legend class="ui-widget ui-widget-header ui-corner-all">Datos Personales</legend>
                        <p>
                            <label for="numid">Num ID:</label> <!-- NUM IDENTIFICACION -->
                            <input type="text" name="numid" class="id" value="<?php echo $row['proid'] ?>" readonly/>
                        </p>
                        <p>
                            <label for="nombrecliente">Nombre del Cliente:</label>
                            <input type="text" style="width:65%" class="id validate[required] text-input"
                                   name="nombrecliente" value="<?php echo $row['pronombre'] ?>"/>
                            <!-- CAMPO NOMBRE CLIENTE -->
                        </p>
                        <p>
                            <label for="nombrealternativo">Nombre Alternativo:</label>
                            <input type="text" style="width:65%" class="id" name="nombrealternativo"
                                   value="<?php echo $row['pronombrealternativo'] ?>"/>
                            <!-- CAMPO NOMBRE ALTERNATIVO -->
                        </p>
                        <p>
                            <label>Tipo de Cliente: </label> <!-- CAMPO TIPO DE PROSPECTO -->
                            <select name="tipocli" id="tipocli" class="validate[required] text-input">
                                <?php
                                if ($row['protipo'] == 1) {
                                    echo '<option value="1" selected >PERSONA</option>';
                                    echo '<option value="2">EMPRESA</option>';
                                } else {
                                    echo '<option value="1">PERSONA</option>';
                                    echo '<option value="2" selected>EMPRESA</option>';
                                }
                                ?>
                            </select>
                        </p>
                    </fieldset>

                    </br></br>

                    <fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
                        <legend class="ui-widget ui-widget-header ui-corner-all">Datos de Ubicacion</legend>
                        <!-- DATOS DE UBICACION -->
                        <p>
                            <label>Provincia:</label> <!-- CAMPO DEPARTAMENTO -->
                            <select id="depresidencia" name="depresidencia" class="text-input">
                                <?php
                                if ($row['prodepartamento'] != '') { // SI EL DEPARTAMENTO DEL PROSPECTO ES DISTINTO A VACIO
                                    $row2 = $db->query("SELECT * FROM departamentos WHERE depid = '" . $row['prodepartamento'] . "'")->fetch(PDO::FETCH_ASSOC); // SE BUSCA ESPECIFICAMENTE EL DEPARTAMENTO
                                    echo '<option value=' . $row['prodepartamento'] . '>' . $row2['depnombre'] . '</option>'; // SE MUESTAR LOS RESULTADOS TANTO NOMBRE DEL DEPARTAMENTO COMO SU ID EN UN SELECT
                                } // SI EL DEPARTAMENTO DEL PROSPECTO ESTA VACIO
                                echo '<option value="">SELECCIONE</option>';
                                $qry = $db->query("SELECT * FROM departamentos WHERE depid <> '" . $row['prodepartamento'] . "' ORDER BY depnombre");
                                while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
                                    echo '<option value=' . $row2['depid'] . '>' . $row2['depnombre'] . '</option>'; // APARECEN TODOS LOS DEPARTAMENTOS PARA QUE EL USUARIO ELIGA EL PERTINENTE
                                }
                                ?>
                            </select>
                        </p>
                        <p>
                            <label>Ciudad: </label> <!-- CAMPO CIUDAD -->
                            <select name="ciuresidencia" id="ciuresidencia" class="text-input">
                                <?php
                                if ($row['prociudad'] != '') { // SI EL CAMPO CIUDAD ES DISTINTO A VACIO
                                    $row2 = $db->query("SELECT * FROM ciudades WHERE ciudepto = '" . $row['prodepartamento'] . "' AND ciuid = '" . $row['prociudad'] . "'")->fetch(PDO::FETCH_ASSOC); //SE BUSCA ESPECIFICAMENTE LA CIUDAD
                                    echo '<option value=' . $row['prociudad'] . '>' . $row2['ciunombre'] . '</option>'; // SE MUESTARN LOS RESULTADOS TANTO NOMBRE DE LA CIUDAD
                                } // SI ES DEPARTAMENTO DEL PROSPECTO ESTA VACIO
                                echo '<option value="">SELECCIONE</option>';
                                $qry = $db->query("SELECT * FROM ciudades WHERE ciudepto = '" . $row['prodepartamento'] . "' AND ciuid <> '" . $row['prociudad'] . "' ORDER BY ciunombre");
                                while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
                                    echo '<option value=' . $row2['depid'] . '>' . $row2['depnombre'] . '</option>'; // APARECEN LAS CIUDADES PARA QUE EL PROSPECTO ESCOJA LA QUE CORRESPONDE
                                }
                                ?>
                            </select>
                        </p>
                        <p>
                            <label>Barrio: </label>
                            <input type="text" name="barrio" value="<?php echo $row['probarrio'] ?>"/>
                        </p>
                        <p>
                            <label>Direccion: </label>
                            <input type="text" name="direccion" style="width:76%" class="direccion"
                                   value="<?php echo $row['prodireccion'] ?>"/>
                        </p>
                        <p>
                            <label>Indicaciones: </label>
                            <input type="text" name="indicaciones" style="width:76%"
                                   value="<?php echo $row['proindicaciones'] ?>"/> <!-- CAMPO INDICACIONES -->
                        </p>
                        <p>
                            <label>E-mail 1: </label>
                            <input type="text" name="email1" class="email" value="<?php echo $row['proemail1'] ?>"/>
                        </p>
                        <p>
                            <label>E-mail 2: </label>
                            <input type="text" name="email2" class="email" value="<?php echo $row['proemail2'] ?>"/>
                        </p>
                        <p>
                            <label>Telefono 1: </label>
                            <input type="text" name="telefono1" class="telefono  custom[phone]] text-input"
                                   value="<?php echo $row['protelefono1'] ?>" title="Numero Telefono 1"/>
                            <label>Telefono 2: </label>
                            <input type="text" name="telefono2" class="telefono"
                                   value="<?php echo $row['protelefono2'] ?>" title="Numero Telefono 2"/>
                        </p>
                        <p>
                            <label>Celular 1: </label>
                            <input type="text" name="celular1" class="telefono  custom[phone]] text-input"
                                   value="<?php echo $row['procelular1'] ?>" title="Numero Celular 1"/>
                            <label>Celular 2: </label>
                            <input type="text" name="celular2" class="telefono"
                                   value="<?php echo $row['procelular2'] ?>" title="Numero Celular 2"/>
                        </p>
                        <p>
                            <label>Contacto: </label>
                            <input type="text" name="contacto" style="width:65%;"
                                   value="<?php echo $row['procontacto'] ?>"/>
                        </p>
                        <p>
                            <label>Contacto Alternativo: </label>
                            <input type="text" name="contactoalternativo" style="width:65%;"
                                   value="<?php echo $row['procontactoalternativo'] ?>"/>
                        </p>

                    </fieldset>
                    <br>
                    </p>
                    <p class="boton">
                        <button type="button" class="btn btn-outline-primary btnatras"
                                onClick="carga(); location.href='listar.php'">
                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-left-square-fill"
                                 fill="currentColor"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                      d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm9.5 8.5a.5.5 0 0 0 0-1H5.707l2.147-2.146a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708-.708L5.707 8.5H11.5z"/>
                            </svg>
                            Atras
                        </button>

                        <button type="submit" class="btn btn-primary btnmodificar" name="modificar" value="modificar">
                            Modificar
                        </button>  <!-- BOTON MODIFICAR -->
                    </p>
                </form>
            </div>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php'); ?> <!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
</section>
</body>
</html>