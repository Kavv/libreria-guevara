<?php

$r = '../../../';
//INCLUIR SESION Y CONECCION
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
?>
<!doctype html>
<html>
<head>
    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>
</head>
<body>
<?php require($r . 'incluir/src/login.php') ?> <!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
    <?php require($r . 'incluir/src/menu.php') ?> <!-- INCLUIMOS MENU PRINCIPAL -->
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a href="#">Call Center</a>
            <div class="mapa_div"></div>
            <a class="current">No Clientes</a>
        </article>
        <article id="contenido">


            <div class="alert alert-dark">No hay más clientes para trabajar a su cargo. Puede normalizar prospectos o
                asignar relacionistas a prospectos sin gestionar
            </div>
            <div class="text-center">
                <a class="btn btn-primary" href="<?php echo $r ?>modulos/callcenter/llamadas/llamar.php">Reintentar
                    gestionar llamada</a>
            </div>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php');
    $db = null; ?> <!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
</section>
</body>
</html>