<?php
$r = '../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');

$id_capacitacion = $_GET['capaid'];
$call = $_GET['call'];
$fecha1 = $_GET['fecha1'];
$fecha2 = $_GET['fecha2'];
$consultar = 'consultar';
$asesor = $_GET['asesor'];
$departamento = $_GET['departamento'];

$filtro = "call=".$call."&fecha1=".$fecha1."&fecha2=".$fecha2."&asesor=".$asesor."&departamento=".$departamento."&consultar=".$consultar;


if($_POST['validar_capa']){

$capacitaid = $_POST['capacitaid'];
$realizado = $_POST['realizada'];
$num_realasistentes = $_POST['asistentes_real'];
$pedidos = $_POST['pedidos'];
$comentarios_capa = strtoupper(trim($_POST['comentarios_capacitador']));
	//if($_FILES['imagen']['size'] < 1600000){
		//if(move_uploaded_file($_FILES['imagen']['tmp_name'], $r.'pdf/capacitaciones/'.$capacitaid.'.pdf')){

			if ($realizado == 'REALIZADA'){
			$estado = '5';
			} elseif ($realizado == 'NO REALIZADA') {
			$estado = '6';
			}

			$qry = $db->query("UPDATE capacitaciones SET capaestado='".$estado."', caparealizada='".$realizado."', capanumasistentesreal='".$num_realasistentes."', capapedidos='".$pedidos."', capacomentarioscapaci='".$comentarios_capa."' WHERE capaid='".$capacitaid."' ;");
			if ($qry){
				$mensaje = 'Se ha validado correctamente la capacitacion';
				header('Location:liscapacitacion.php?'.$filtro.'&mensaje='.$mensaje);
				exit();
			} else {
				$error = 'No se pudo validar la capacitacion por favor contactese con el administrador';
				header('Location:liscapacitacion.php?'.$filtro.'&error='.$error);
				exit();
			}
		//}else {
		//$error = "No se pudo cargar el archivo";
		//}
	//} else {
	//$error = 'la digitalizacon excede el tamano permitido (max. 1.5MB)';
	//header('Location:liscapacitacion.php?'.$filtro.'&error='.$error);
	//exit();
	//}

}

$qry1 = $db->query("SELECT * FROM capacitaciones WHERE capaid = '".$id_capacitacion."';");
$row1 = $qry1->fetch(PDO::FETCH_ASSOC);

if ($row1['capaestado'] <> '3')
{
$error = "No se puede validar la capacitacion por que no se encuentra confirmada por el director del call center";
header ('Location:liscapacitacion.php?error='.$error.'&'.$filtro.'');
}

$qrypermisos = $db->query("SELECT * FROM usuarios WHERE usuid = ".$_SESSION['id'].";");
$rowpermisos = $qrypermisos->fetch(PDO::FETCH_ASSOC);

?>
<!doctype html>
<html>
<head>
<title>. : : S o p h y a : : .</title>
<meta charset="utf-8" />
<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/css/normalize.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/jquery/css/smoothness/jquery-ui-1.10.3.custom.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/css/css.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/css/menu.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/validation/css/validationEngine.jquery.css" />
<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/js/jquery-ui-1.10.3.custom.js"></script>
<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>
<script type="text/javascript" src="<?php echo $r ?>incluir/validation/js/languages/jquery.validationEngine-es.js"></script>
<script type="text/javascript" src="<?php echo $r ?>incluir/validation/js/jquery.validationEngine.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#form').validationEngine({
		showOneMessage:true,
		onValidationComplete: function(form, status){
			if(status == true) {
				carga();
				return true;
			}
		}
	});
	$('#fecha2').datepicker({
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		minDate: '0D',
	});
	$('.historico').click(function(){ // ACCION PARA CARASTERISTICO
		newHref = $(this).attr('data-rel');
		$('#dialog2').load(newHref).dialog({ modal:true, width: 800 });
    });

	$('.btnvalidar').button({ icons: { primary: 'ui-icon ui-icon-check' }});
});
</script>
<style type="text/css">
#form form{ width:650px }
#form fieldset{ padding:10px; display:block; width:650px; margin:20px auto }

#form fieldset{ padding:10px; display:block; width:650px; margin:20px auto }
#form legend{ font-weight: bold; margin-left:5px; padding:5px }
#form label{ display:inline-block; text-align:right;  margin:0.3em 2% 0 0 }
#form p{ margin:5px 0 }
</style>
</head>
<body>
<?php require($r.'incluir/src/login.php') ?>
<section id="principal">
<?php require($r.'incluir/src/cabeza.php') ?>
<?php require($r.'incluir/src/menu.php') ?>
<article id="cuerpo">
<article class="mapa">
<a href="#">Principal</a><div class="mapa_div"></div><a href="#">Call Center</a><div class="mapa_div"></div><a class="current">Validar Capacitacion</a>
</article>
<article id="contenido">
<form id="form" name="form" action="valicapacitacion.php?<?php echo $filtro; ?>" method="post">
<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
<legend class="ui-widget ui-widget-header ui-corner-all">Validar Capacitacion <img src="<?php echo $r ?>imagenes/iconos/comments.png" data-rel="notas.php?hisproprospecto=<?php echo $prospecto.'&hisproid='.$prospecto ?>" class="historico" title="Visualizacion Historico" /></legend>
<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
<legend>Datos de la capacitacion</legend>
	<p>
	<label style="width:140px;" for="prospecto"> Identificacion del prospecto: </label>
	<input type="text" name="prospecto" id="prospecto" value="<?php echo $row1['capaprospectoid'] ?>" class="id validate[required]" readonly />
	</p>
	<p>
	<label style="width:140px;" for="capacitaid"> Identificacion de la capacitacion: </label>
	<input type="text" name="capacitaid" id="capacitaid" value="<?php echo $row1['capaid'] ?>" class="id validate[required]" readonly />
	</p>
	<p>
	<?php
	$qrynombre = $db->query("SELECT * FROM usuarios WHERE usuid = '".$row1['capausuario']."';");
	$rownombre = $qrynombre->fetch(PDO::FETCH_ASSOC);
	$nombre_usu = $rownombre['usunombre'];
	?>
	<label style="width:140px;" for="agendadapor"> Capacitacion agendada por: </label>
	<input style="width:300px" type="text" name="agendadapor" id="agendadapor" value="<?php echo $nombre_usu  ?>" readonly />
	</p>
	<p>
	<label style="width:140px;" for="diaagenda"> Capacitacion gestionada el dia: </label>
	<input style="width:300px" type="text" name="diaagenda" id="diaagenda" value="<?php echo $row1['capafechausuario']  ?>" readonly />
	</p>
	<p>
	<label style="width:140px;" for="capacitacion"> Capacitacion: </label>
	<?php
	if($row1['capaidlistado'] != ''){
		$row2 = $db->query("SELECT * FROM liscapacitaciones WHERE liscapaid = '".$row1['capaidlistado']."'")->fetch(PDO::FETCH_ASSOC);
		echo '<input style="width:350px" type="text" name="capacitacion" id="capacitacion" value="'. $row2['liscapadescripcion'] .'" readonly />';
	}else{
		echo '<input style="width:350px" type="text" name="capacitacion" id="capacitacion" value="CAPACITACION NO ASIGNADA" readonly />';
	}
	?>
	</p>
	<p>
	<label style="width:140px;" for="asesor">Asesor:</label>
	<?php
		if($row1['capacapacitador'] != ''){
			$row2 = $db->query("SELECT * FROM usuarios WHERE usuasesor = '1'  AND usuid = '".$row1['capacapacitador']."' ORDER BY usunombre")->fetch(PDO::FETCH_ASSOC);
			echo '<input style="width:300px" type="text" name="asesor" id="asesor" value="'. $row2['usunombre'] .'" readonly />';
		}else{
		echo '<input style="width:300px" type="text" name="asesor" id="asesor" value="ASESOR NO ASIGNADO" readonly />';
		}
	?>
	</p>
	<p>
	<label style="width:140px;" for="comentarios"> Comentarios: </label>
	<textarea name="comentarios" id="comentarios"  rows="4" cols="65" disabled ><?php echo $row1['capacomentarios']?></textarea>
	</p>
	<?php
	$qryhofe = $db->query("SELECT time(capafecha) as horaCapaci , date(capafecha) as fechaCapaci FROM capacitaciones WHERE capaid = '".$id_capacitacion."';");
	$rowhofe = $qryhofe->fetch(PDO::FETCH_ASSOC);
	$horaCapaci = $rowhofe['horaCapaci'];
	$fechaCapaci = $rowhofe['fechaCapaci'];
	?>
	<p>
	<label style="width:140px;" for="fecha2"> Fecha de capacitacion: </label>
	<input type="text" name="fecha2" value="<?php echo $fechaCapaci ?>"  id="fecha2" class="validate[required] text-input fecha" readonly />
	</p>
	<p>
	<label style="width:140px;" for="hora2"> Hora de capacitacion: </label>
	<input type="time" name="hora2" id="hora2" value="<?php echo $horaCapaci ?>" max="23:59:59" min="00:00:01" class="validate[required]" step="1" readonly>
	</p>
	<p>
	<label style="width:140px;" title="Numero posible de asistentes" for="asistentes">N.P de Asistentes: </label>
	<input style="width:60px;" type="number" name="asistentes" value="<?php echo $row1['capanumasistentes'] ?>" class="id validate[required]" id="asistentes" min="1" max="50" readonly>
	</p>
	</br>
</fieldset>

<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
<legend>Datos de Validacion</legend>
<p>
<label style="width:140px;" for="realizada">¿Se realizo? </label>
<select name="realizada" class="validate[required]">
<option value="" > SELECCIONE </option>
<option value="REALIZADA"> REALIZADA </option>
<!--<option value="NO REALIZADA"> NO REALIZADA </option>-->
</select>
</p>
<p>
<label style="width:140px;" title="Numero real de asistentes" for="asistentes_real">N.R de Asistentes: </label>
<input style="width:60px;" type="number" name="asistentes_real"  class="id validate[required]" id="asistentes_real" min="0" max="50">
</p>
<p>
<label style="width:140px;" for="pedidos">Pedidos Logrados </label>
<input style="width:60px;" type="number" name="pedidos"  class="id validate[required]" id="pedidos" min="0" max="50">
</p>
<p>
<label style="width:140px;" for="comentarios_capacitador"> Comentarios de la capacitacion: </label>
<textarea name="comentarios_capacitador" id="comentarios_capacitador"  rows="4" cols="65" ></textarea>
</p>
<!--
<p>
<label style="width:140px;" for="imagen">PDF (max. 1.5 Mb): </label>
<input type="file" id="imagen" name="imagen" class="validate[required, checkFileType[pdf|PDF]]" />
</p>
-->
</fieldset>

<p class="boton">
<button type="button" class="btnatras" onClick="carga(); location.href='liscapacitacion.php?<?php echo $filtro ?>'">Atras</button>  <!-- BOTON ATRAS -->
<button type="submit" class="btnvalidar" name="validar_capa" value="validar_capa">Validar Capacitacion</button> <!-- BOTON VALIDAR -->
</p>
</fieldset>
</form>
</article>
</article>
<?php require($r.'incluir/src/pie.php') ;
 ?>
</section>
<div id="dialog2" title="Historico de Llamadas" style="display:none"></div>
</body>
</html>