<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

if (isset($_GET['error'])) {
    $error = $_GET['error'];
}
if (isset($_GET['mensaje'])) {
    $mensaje = $_GET['mensaje'];
}

$self = $_SERVER['PHP_SELF']; //Obtenemos la página en la que nos encontramos
header("refresh:60; url=$self"); //Refrescamos cada 300 segundos

$hoy = date("Y-m-d");
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>

    <link rel="stylesheet" type="text/css"
          href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css"/>
    <script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#tabla').dataTable({
                'bJQueryUI': true,
                'bStateSave': true,
                'sPaginationType': 'full_numbers',
                'oLanguage': {
                    'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
                },
                'bSort': false,
            });
            $('.btnatras').button({icons: {primary: 'ui-icon ui-icon-arrowthick-1-w'}});
            $('#dialog-message').dialog({
                height: 80,
                width: 'auto',
                modal: true
            });
        });
    </script>
</head>
<body>
<article id="cuerpo">
    <article id="contenido">
        <h2>Semaforo de Gestiones del dia de hoy <?php echo $hoy; ?></h2>
        <table id="tabla">
            <thead>
            <tr>
                <th title="Semaforo de Gestiones"></th>
                <th align="center">Relacionista</th>
                <th align="center">Gestiones</th>
                <th align="center">Activo</th>
                <th align="center">G VS A</th>
                <th align="center">Confirmado</th>
                <th align="center">Aplazado</th>
                <th align="center">Reprogramado</th>
                <th align="center">Cancelado</th>
                <th align="center">Realizado</th>
                <th align="center">Concretadas</th>
                <th align="center">Tiempo en Disponible</th>
                <th align="center">Tiempo Hablando</th>
            </tr>
            </thead>
            <tbody>
            <?php

            $sql = "select u.usunombre                                                                                                as Nombre, " .
                "       (select count(*) " .
                "        from hisprospectos " .
                "        where hisprousuario = u.usuid " .
                "          and date(hisprofechahora) = date(now()))                                                                as Gestiones, " .
                "       count(*)                                                                                                   as Concretadas, " .
                "       sum(if(c.capaestado = 1, 1, 0))                                                                            as Activa, " .
                "       sum(if(c.capaestado = 2, 1, 0))                                                                            as Cancelado, " .
                "       sum(if(c.capaestado = 3, 1, 0))                                                                            as Confirmado, " .
                "       sum(if(c.capaestado = 4, 1, 0))                                                                            as Aplazado, " .
                "       sum(if(c.capaestado = 5, 1, 0))                                                                            as Realizado, " .
                "       sum(if(c.capaestado = 7, 1, 0))                                                                            as Reprogramado " .
                "from usuarios u " .
                "         inner join capacitaciones c on c.capausuario = u.usuid " .
                "where (select count(*) from hisprospectos where hisprousuario = u.usuid) > 0 " .
                "and date(c.capafecha) = date(now()) " .
                "group by u.usunombre; ";

            $qry = $db->query($sql);

            $gestiones_totales = 0;
            $concretadas_totales = 0;
            $activas_totales = 0;
            $cancelado_totales = 0;
            $confirmado_totales = 0;
            $aplazado_totales = 0;
            $realizado_totales = 0;
            $reprogramado_totales = 0;
            while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
                $numgestion = $row['Gestiones'];

                $numconcretadas = $row['Concretadas'];

                $gestiones_totales = $gestiones_totales + $numgestion;
                $concretadas_totales = $concretadas_totales + $numconcretadas;

                $numactiva = $row['Activa'];
                $numcancelado = $row['Cancelado'];
                $numconfirmado = $row['Confirmado'];
                $numaplazado = $row['Aplazado'];
                $numrealizado = $row['Realizado'];
                $numreprogramado = $row['Reprogramado'];

                $activas_totales = $activas_totales + $numactiva;
                $cancelado_totales = $cancelado_totales + $numcancelado;
                $confirmado_totales = $confirmado_totales + $numconfirmado;
                $aplazado_totales = $aplazado_totales + $numaplazado;
                $realizado_totales = $realizado_totales + $numrealizado;
                $reprogramado_totales = $reprogramado_totales + $numreprogramado;

                $ges_vs_con = $numgestion == 0 ? 0 : ($numactiva * 100) / $numgestion;

                if ($numgestion > 0 or $numconcretadas > 0) {
                    ?>
                    <tr>
                        <?php if ($numactiva >= 0 and $numactiva <= 2) {
                            $color = "style='background-color: red;'";
                        } elseif ($numactiva >= 3 and $numactiva <= 4) {
                            $color = "style='background-color: yellow;'";
                        } elseif ($numactiva >= 5) {
                            $color = "style='background-color: green;'";
                        } ?>
                        <td title="Semaforo de Gestiones" <?php echo $color; ?> align="center"></td>
                        <td align="center"><?php echo $row['Nombre']; ?></td>
                        <td align="center"><?php echo $numgestion; ?></td>
                        <td align="center"><?php echo $numactiva; ?></td>
                        <td align="center"><?php echo " % " . number_format($ges_vs_con, 2, ',', '.'); ?></td>
                        <td align="center"><?php echo $numconfirmado; ?></td>
                        <td align="center"><?php echo $numaplazado; ?></td>
                        <td align="center"><?php echo $numreprogramado; ?></td>
                        <td align="center"><?php echo $numcancelado; ?></td>
                        <td align="center"><?php echo $numrealizado; ?></td>
                        <td align="center"><?php echo $numconcretadas; ?></td>
                        <td align="center"></td>
                        <td align="center"></td>
                    </tr>
                    <?php
                }
            }
            ?>
            <tr>
                <td align="center"></td>
                <td align="center"></td>
                <td align="center"><?php echo $gestiones_totales; ?></td>
                <td align="center"><?php echo $activas_totales; ?></td>
                <td align="center"></td>
                <td align="center"><?php echo $confirmado_totales; ?></td>
                <td align="center"><?php echo $aplazado_totales; ?></td>
                <td align="center"><?php echo $reprogramado_totales; ?></td>
                <td align="center"><?php echo $cancelado_totales; ?></td>
                <td align="center"><?php echo $realizado_totales; ?></td>
                <td align="center"><?php echo $concretadas_totales; ?></td>
                <td align="center"></td>
                <td align="center"></td>
            </tr>
            </tbody>
        </table>
    </article>
    <?php
    if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>';
    elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>';
    ?>
    <div id="modal" style="display:none"></div>
    <div id="dialog2" style="display:none"></div>
</body>
</html>