<?php
$r = '../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');

if(isset($_POST['consultar'])){
	$asesor = $_POST['asesor'];
	$fechaida1 = $_POST['fecha1'];
	$fechaida2 = $_POST['fecha2'];
	$fecharegreso1 = $_POST['fecha3'];
	$fecharegreso2 = $_POST['fecha4'];
} else {
	$asesor = $_GET['asesor'];
	$fechaida1 = $_GET['fecha1'];
	$fechaida2 = $_GET['fecha2'];
	$fecharegreso1 = $_GET['fecha3'];
	$fecharegreso2 = $_GET['fecha4'];
}

if(isset($_POST['modificar'])){
	
	$iden = $_POST['iden'];
	$dia_viaje_ida = $_POST['fechaida'];
	$dia_viaje_regreso = $_POST['fecharegreso'];
	$departamento = $_POST['departamento'];
	$ciudad = $_POST['ciudad'];
	$asesor = $_POST['asesor'];
	$empresa_asesor = $_POST['empresa_asesor'];
	$mes_diligenciamiento = $_POST['mesdili'];
	$fechavalor1 = $_POST['fecha2'];
	$valor1 = $_POST['monto2'];
	$fechavalor2 = $_POST['fecha3'];
	$valor2 = $_POST['monto3'];
	$valor_total = $_POST['monto_total'];
	
	$qry = $db->query("UPDATE viaticos SET viadiaviajeida='".$dia_viaje_ida."', viadiaviajeregreso='".$dia_viaje_regreso."', viadepdestino='".$departamento."', viaciudestino='".$ciudad."', viacapacitador='".$asesor."', viaempcapacitador='".$empresa_asesor."', viamesdiligenciamiento='".$mes_diligenciamiento."', viafechavalor1='".$fechavalor1."', viavalor1='".$valor1."', viafechavalor2='".$fechavalor2."', viavalor2='".$valor2."', viavalortotal='".$valor_total."' WHERE viaid='".$iden."';");
	if ($qry) {
	header ('Location:finalizar_viaticos.php?iden='.$iden.'');
	} else {
	$error = 'No se pudo actualizar el prospecto';
	}
}

$error = $_GET['error'];

$filtro = 'asesor='.$asesor.'&fechaida1='.$fechaida1.'&fechaida2='.$fechaida2.'&fecharegreso1='.$fecharegreso1.'&fecharegreso2='.$fecharegreso2;
$con = 'SELECT * FROM viaticos '; 
$ord = ''; // ORDER BY viadiadiligencia DESC Ordenar la Consulta
$group = ''; // Realizar Group By 

if($asesor == ''  && $fechaida1 == '' && $fecharegreso1 == '' ) $sql = "$con  $group $ord";
elseif($asesor != ''  && $fechaida1 == '' && $fecharegreso1 == '' ) $sql = "$con WHERE viacapacitador = '$asesor' $group $ord";
elseif($asesor == ''  && $fechaida1 != '' && $fecharegreso1 == '' ) $sql = "$con WHERE DATE(viadiaviajeida) BETWEEN '$fechaida1' AND '$fechaida2' $group $ord";
elseif($asesor != ''  && $fechaida1 != '' && $fecharegreso1 == '' ) $sql = "$con WHERE viacapacitador = '$asesor' AND DATE(viadiaviajeida) BETWEEN '$fechaida1' AND '$fechaida2' $group $ord";
elseif($asesor == ''  && $fechaida1 == '' && $fecharegreso1 != '' ) $sql = "$con WHERE DATE(viadiaviajeregreso) BETWEEN '$fecharegreso1' AND '$fecharegreso2' $group $ord";
elseif($asesor != ''  && $fechaida1 == '' && $fecharegreso1 != '' ) $sql = "$con WHERE DATE(viadiaviajeregreso) BETWEEN '$fecharegreso1' AND '$fecharegreso2' AND viacapacitador = '$asesor' $group $ord";



$qry = $db->query($sql);
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
	<?php 
		require($r . 'incluir/src/head.php');
		require($r . 'incluir/src/head-form.php');
	?>	

<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#tabla').dataTable({
		'bJQueryUI': true,
		'bStateSave': true,
		'sPaginationType': 'full_numbers',
		'oLanguage': {
			'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
		},
		'bSort': false,
	});
	$('.btnatras').button({ icons: { primary: 'ui-icon ui-icon-arrowthick-1-w' }});
	$('#dialog-message').dialog({
		height: 80,
		width: 'auto',
		modal: true
	});
	$('.pdf').click (function(){
		newSrc = $(this).attr('data-rel');
		$('#modal').html("<iframe src='" + newSrc + "' width='100%' height='100%'></iframe>");
		$('#modal').dialog({ modal: true, width: '800', height: '650', title : 'PDF de la solicitud' });
	});
	$('.msj_anular').click(function(e){
		e.preventDefault();
		var targetUrl = $(this).attr('href');
		var $dialog_link_follow_confirm = $('<div></div>').html("<p><span class='ui-icon ui-icon-alert' style='float:left; margin:2px 7px 7px 0;'></span>Esta seguro que desea anular este documento?</p>").
		dialog({
			title: 'Porfavor confirmar',
			buttons :{
				'Si' : function() { 
					window.location.href = targetUrl; 
				},
				'No' : function() { 
					$(this).dialog("close"); 
				}
			},
			modal: true,
			width: 'auto',
			height: 'auto'
		}
	);
	$dialog_link_follow_confirm.dialog("open");
	});
});
</script>
</head>
<body>
<?php require($r.'incluir/src/login.php') ?>
<section id="principal">
<?php require($r.'incluir/src/cabeza.php') ?>
<?php require($r.'incluir/src/menu.php') ?>
<article id="cuerpo">
<article class="mapa">
<a href="#">Principal</a><div class="mapa_div"></div><a href="#">Call Center</a><div class="mapa_div"></div><a class="current">Consultar Viaticos</a>
</article>
<article id="contenido">
<h2>Listado de Viaticos <?php //echo  $sql ?></h2>
<div class="reporte">
<a href="pdflistarviaticos.php?<?php echo $filtro ?>"><img src="<?php echo $r ?>imagenes/iconos/pdf.png" title="pdf" /></a>
</div>
<table id="tabla">
<thead>
<tr>
<th align="center">Asesor</th>
<th align="center">Empresa</th>
<th align="center">Dia de Viaje Ida</th>
<th align="center">Dia de Viaje Regreso</th>
<th>Departamento</th>
<th>Ciudad</th>
<th align="center" title="Mes Diligenciamiento">M.D</th>
<th>Fecha de pago 1 50%</th>
<th>Monto 1 50%</th>
<th>Fecha de pago 2 50%</th>
<th>Monto 2 50%</th>
<th>Valor Total</th>
<th>Estado</th>
<th></th>
<th></th>
<th></th>
</tr>
</thead>
<tbody>
<?php
while($row = $qry->fetch(PDO::FETCH_ASSOC)){

$qrydeparta = $db->query("SELECT * FROM departamentos where depid = ".$row['viadepdestino']."; ");
$rowdeparta = $qrydeparta->fetch(PDO::FETCH_ASSOC);
$departamento = $rowdeparta['depnombre'];

$qryciudad = $db->query("SELECT * FROM ciudades where ciudepto = ".$row['viadepdestino']." AND ciuid = ".$row['viaciudestino']." ");
$rowciudad = $qryciudad->fetch(PDO::FETCH_ASSOC);
$ciudad = $rowciudad['ciunombre'];

$qryasesor = $db->query("SELECT * FROM usuarios where usuid = ".$row['viacapacitador']."; ");
$rowasesor = $qryasesor->fetch(PDO::FETCH_ASSOC);
$asesor = $rowasesor['usunombre'];

$qryempresa = $db->query("SELECT * FROM empresas where empid = ".$row['viaempcapacitador']."; ");
$rowempresa = $qryempresa->fetch(PDO::FETCH_ASSOC);
$empresa = $rowempresa['empnombre'];

$qrymeses = $db->query("SELECT * FROM meses_ano where mesid = ".$row['viamesdiligenciamiento']."; ");
$rowmeses = $qrymeses->fetch(PDO::FETCH_ASSOC);
$meses = $rowmeses['mesnombre'];

?>
<tr>
<td><?php echo $asesor ?></td>
<td><?php echo $empresa ?></td>
<td align="center"><?php echo $row['viadiaviajeida'] ?></td>
<td align="center"><?php echo $row['viadiaviajeregreso'] ?></td>
<td align="center"><?php echo $departamento ?></td>
<td align="center"><?php echo $ciudad ?></td>
<td><?php echo $meses ?></td>
<td align="center"><?php echo $row['viafechavalor1'] ?></td>
<td align="center"><?php echo number_format($row['viavalor1'],0,',','.') ?></td>
<td align="center"><?php echo $row['viafechavalor2'] ?></td>
<td align="center"><?php echo number_format($row['viavalor2'],0,',','.') ?></td>
<td align="center"><?php echo number_format($row['viavalortotal'],0,',','.') ?></td>
<td align="center"><?php echo $row['viaestado'] ?></td>
<td align="center">
<?php
$qrypermisos = $db->query("SELECT * FROM usuarios WHERE usuid = ".$_SESSION['id'].";");
$rowpermisos = $qrypermisos->fetch(PDO::FETCH_ASSOC);
if($rowpermisos['usuperfil'] == 63 or $rowpermisos['usuperfil'] == 30 or $rowpermisos['usuperfil'] == 5 ){
?>
<a href="modificarviaticos.php?id=<?php echo $row['viaid']."&modificar=1&".$filtro ?>" title="Modificar"><img src="<?php echo $r ?>imagenes/iconos/lapiz.png"  /></a>
<?php
}else echo '<img src="'.$r.'imagenes/iconos/lapiz.png" class="gray" />';
?>	
</td>
<?php if ($row['viaestado'] != 'PROCESO') { ?>
<td align="center"><img style="cursor:pointer;" src="<?php echo $r ?>imagenes/iconos/pdf.png" class="pdf" data-rel="<?php echo $r.'pdf/viaticos/'.$row['viaid'].'.pdf' ?>" title="PDF viaticos" /></td>
<?php } else { ?>
<td align="center"><img src="<?php echo $r ?>imagenes/iconos/pdf.png" title="PDF NO DISPONIBLE" /></td>
<?php } ?>
<?php if ($row['viaestado'] == 'ACTIVA') { ?>
<td><a href="anular_viaticos.php?<?php echo 'viaticosidenti='.$row['viaid']; ?>" class="msj_anular" title="Anular"><img src="<?php echo $r ?>imagenes/iconos/cancelar.png" class="grayscale" /></a></td></tr>
<?php } else { ?>
<td align="center"><img class="gray" src="<?php echo $r ?>imagenes/iconos/cancelar.png" title="NO SE PUEDE ANULAR" /></td>
<?php } ?>
<?php
$vlr1 = $vlr1 + $row['viavalortotal'];
}
?>
</tbody>
<tfoot>
<tr><td colspan="9"></td><td colspan="2" align="center" bgcolor="#D1CFCF">Valor Total del listado</td><td align="right" bgcolor="#D1CFCF"><?php echo number_format($vlr1,0,',','.') ?></td><td colspan="4" ></td></tr>
</tfoot>
</table>
<p class="boton">
<button type="button" class="btnatras" onClick="carga(); location.href='consultarviaticos.php'">Atras</button>
</p>
</article>
</article>
<?php require($r.'incluir/src/pie.php') ?>
</section>
<?php
if(isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>'.$error.'</div>';
elseif(isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>'.$mensaje.'</div>';
?>
<div id="modal" style="display:none"></div>
<div id="dialog2" style="display:none"></div>
</body>
</html>