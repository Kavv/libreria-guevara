<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

$id_capacitacion = $_GET['capaid'];
$fecha1 = $_GET['fecha1'];
$fecha2 = $_GET['fecha2'];
$consultar = $_GET['consultar'];
$call = $_GET['call'];
$asesor = $_GET['asesor'];
$departamento = $_GET['departamento'];
$estado = $_GET['estado'];
$fecha3 = $_GET['fecha3'];
$fecha4 = $_GET['fecha4'];
$idcapaci = $_GET['idcapaci'];
$idpros = $_GET['idpros'];
$jeferela = $_GET['jeferela'];

$filtro = "jeferela=" . $jeferela . "&idpros=" . $idpros . "&idcapaci=" . $idcapaci . "&capaid=" . $id_capacitacion . "&call=" . $call . "&fecha1=" . $fecha1 . "&fecha2=" . $fecha2 . "&asesor=" . $asesor . "&departamento=" . $departamento . "&estado=" . $estado . "&consultar=" . $consultar . "&fecha3=" . $fecha3 . "&fecha4=" . $fecha4;


$filtro1 = "jeferela=" . $jeferela . "&idpros=" . $idpros . "&idcapaci=" . $idcapaci . "&call=" . $call . "&fecha1=" . $fecha1 . "&fecha2=" . $fecha2 . "&asesor=" . $asesor . "&departamento=" . $departamento . "&estado=" . $estado . "&consultar=" . $consultar . "&fecha3=" . $fecha3 . "&fecha4=" . $fecha4;


$qry1 = $db->query("SELECT * FROM capacitaciones INNER JOIN prospectos on proid = capaprospectoid WHERE capaid = '" . $id_capacitacion . "';");
$row1 = $qry1->fetch(PDO::FETCH_ASSOC);


$qrypermisos = $db->query("SELECT * FROM usuarios WHERE usuid = '" . $_SESSION['id'] . "';");
$rowpermisos = $qrypermisos->fetch(PDO::FETCH_ASSOC);

if (isset($_GET['menos']) && $_GET['menos']) {
    $identificador = $_GET['identificador'];
    $qry = $db->query("DELETE FROM asistentescapa WHERE asisid='$identificador';");


    header('Location:asistentescapa.php?' . $filtro . '&asdasqwevy');
} elseif (isset($_POST['mas']) && $_POST['mas']) {
    $id_capacitacion = $_GET['capaid'];
    $fecha1 = $_GET['fecha1'];
    $fecha2 = $_GET['fecha2'];
    $consultar = $_GET['consultar'];
    $call = $_GET['call'];
    $asesor = $_GET['asesor'];
    $departamento = $_GET['departamento'];
    $estado = $_GET['estado'];
    $fecha3 = $_GET['fecha3'];
    $fecha4 = $_GET['fecha4'];
    $idcapaci = $_GET['idcapaci'];
    $idpros = $_GET['idpros'];
    $jeferela = $_GET['jeferela'];

    $nombre = strtoupper(trim($_POST['nombre']));
    $cargo = strtoupper(trim($_POST['cargo']));
    $antiguedad = $_POST['antiguedad'];
    $celular = $_POST['celular'];
    $empresarecomendar = strtoupper(trim($_POST['empresarecomendar']));
    $datosrecomendar = $_POST['datosrecomendar'];

    $filtro1 = "jeferela=" . $jeferela . "&idpros=" . $idpros . "&idcapaci=" . $idcapaci . "&call=" . $call . "&fecha1=" . $fecha1 . "&fecha2=" . $fecha2 . "&asesor=" . $asesor . "&departamento=" . $departamento . "&estado=" . $estado . "&consultar=" . $consultar . "&fecha3=" . $fecha3 . "&fecha4=" . $fecha4;

    $qry = $db->query("INSERT INTO asistentescapa (asiscapa, asisnombre, asiscargo, asisantiguedad, asiscelular, asisempresarecomendar, asisdatosrecomendar) VALUES ('$id_capacitacion', '$nombre', '$cargo', '$antiguedad', '$celular', '$empresarecomendar', '$datosrecomendar');");

    if ($qry) {
        header('Location:asistentescapa.php?' . $filtro . '&asdasqwevy');
    } else {
        $error = 'No se pudo ingresar el asistente por favor intente nuevamente o contactese con el administrador del portal.';
        header('Location:asistentescapa.php?' . $filtro . '&asdasqwevy&error=' . $error);
    }

}


?>
<!doctype html>
<html>
<head>


    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>
    <link rel="stylesheet" type="text/css"
          href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css"/>
    <script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {

            $('.tblW').dataTable({
                'bJQueryUI': true,
                'bStateSave': true,
                'bSort': false,
                'sPaginationType': 'full_numbers',
                'oLanguage': {
                    'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
                },
            });
            $('#fecha2').datepicker({
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true,
                minDate: '0D',
            });

            $('.historico').click(function () { // ACCION PARA CARASTERISTICO
                newHref = $(this).attr('data-rel');
                $('#dialog2').load(newHref).dialog({
                    modal: true,
                    width: 800
                });
            });
        });
    </script>
</head>
<body>
<?php require($r . 'incluir/src/login.php') ?>
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <?php require($r . 'incluir/src/menu.php') ?>
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a href="#">Call Center</a>
            <div class="mapa_div"></div>
            <a class="current">Asistentes a la Capacitacion</a>
        </article>
        <article id="contenido">
            <form id="form" name="form" action="asistentescapa.php?<?php echo $filtro; ?>" method="post">

                <fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
                    <legend class="ui-widget ui-widget-header ui-corner-all col-md-6">Detalle de de la capacitacion <img
                                src="<?php echo $r ?>imagenes/iconos/comments.png"
                                data-rel="notas.php?hisproprospecto=<?php echo $row1['capaprospectoid'] . '&hisproid=' . $row1['capaprospectoid'] ?>"
                                class="historico" title="Visualizacion Historico"/></legend>

                    <p>
                        <label style="width:140px;" for="prospecto"> Identificacion del prospecto: </label>
                        <input type="text" name="prospecto" id="prospecto"
                               value="<?php echo $row1['capaprospectoid'] ?>" class="id validate[required]"/>
                    </p>
                    <p>
                        <label style="width:140px;" for="capaid"> Identificacion de la capacitacion: </label>
                        <input type="text" name="capaid" id="capaid" value="<?php echo $row1['capaid'] ?>"
                               class="id validate[required]"/>
                    </p>
                    <p>
                        <?php
                        $qrynombre = $db->query("SELECT * FROM usuarios WHERE usuid = '" . $row1['capausuario'] . "';");
                        $rownombre = $qrynombre->fetch(PDO::FETCH_ASSOC);
                        $nombre_usu = $rownombre['usunombre'];
                        ?>
                        <label style="width:140px;" for="agendadapor"> Capacitacion agendada por: </label>
                        <input style="width:300px" type="text" name="agendadapor" id="agendadapor"
                               value="<?php echo $nombre_usu ?>"/>
                    </p>
                    <p>
                        <label style="width:140px;" for="nombrepros"> Nombre del prospecto: </label>
                        <input style="width:400px" type="text" name="nombrepros" id="nombrepros"
                               value="<?php echo $row1['pronombre'] ?>"/>
                    </p>
                    <p>
                        <label style="width:140px;" for="nombrecontacto"> Nombre del contacto: </label>
                        <input style="width:400px" type="text" name="nombrecontacto" id="nombrecontacto"
                               value="<?php echo $row1['procontacto'] ?>"/>
                    </p>
                    <br>
                    <hr>
                    <br>

                    <p>
                        <label style="width:140px;">Nombre: </label>
                        <input type="text" name="nombre" style="width:310px;"
                               class="nombre validate[required, custom[onlyLetterSp]] text-input"
                               title="Digite su Nombre"/>
                    </p>
                    <p>
                        <label style="width:140px;">Cargo: </label>
                        <input type="text" name="cargo" class="cargo validate[required] text-input"/>
                    </p>
                    <p>
                        <label style="width:140px;">Antiguedad en meses: </label>
                        <input type="text" name="antiguedad" class="validate[required, custom[onlyNumberSp]] text-input"
                               style="text-align:center; width: 3em"
                               title="Digite la antiguedad en la empresa en meses"/>
                    </p>
                    <p>
                        <label style="width:140px;">Celular: </label>
                        <input type="text" name="celular" class="telefono validate[required, custom[phone]] text-input"
                               title="Digite numero celular"/>
                    </p>
                    <p>
                        <label style="width:140px;">Empresa donde nos recomienda: </label>
                        <input type="text" name="empresarecomendar" style="width:310px;"
                               class="nombre validate[custom[onlyLetterSp]] text-input" title="Digite su Nombre"/>
                    </p>
                    <p>
                        <label style="width:140px;">Contacto con dicha empresa: </label>
                        <input type="text" name="datosrecomendar" style="width:310px;" class="text-input"
                               title="Digite su Nombre"/>
                    </p>
                    <center>
                        <p>
                            <button type="submit" class="btn btn-primary" name="mas" value="mas">Añadir</button>
                        </p>
                    </center>

                    <br>

                    <fieldset class="ui-widget ui-widget-content ui-corner-all ">
                        <legend class="ui-widget ui-widget-header ui-corner-all">Asistentes Agregados</legend>


                        <?php
                        $qry = $db->query("SELECT * FROM asistentescapa WHERE asiscapa = '$id_capacitacion' ");
                        $numresul = $qry->rowCount();


                        if ($numresul == 0) {
                            echo "<p>Actualmente no se han agregado asistentes a esta capacitación.</p>";
                        } else {
                            ?>
                            <table class="tblW">
                            <thead>
                            <tr>
                                <th title="Nombre">Nombre</th>
                                <th>Cargo</th>
                                <th>Antiguedad</th>
                                <th>Celular</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
                                ?>
                                <tr>
                                    <td><?php echo $row2['asisnombre'] ?></td>
                                    <td><?php echo $row2['asiscargo'] ?></td>
                                    <td><?php echo $row2['asisantiguedad'] ?></td>
                                    <td><?php echo $row2['asiscelular'] ?></td>
                                    <td>
                                        <button type="button" class="btn btn-primary"
                                                onclick="carga(); location.href = 'asistentescapa.php?<?php echo 'capaid=' . $id_capacitacion . '&identificador=' . $row2['asisid'] . '&menos=aIUOfHbHjFHoLm48129692jF45n' ?>'">
                                            -
                                        </button>
                                    </td>
                                </tr>
                                </tbody>
                                </table>
                                <?php
                            }
                        }
                        ?>
                    </fieldset>


                    </br>
                    </br>
                    <p class="boton">
                        <button type="button" class="btn btn-outline-primary btnatras"
                                onClick="carga(); location.href='liscapacitacion.php'">
                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-left-square-fill"
                                 fill="currentColor"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                      d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm9.5 8.5a.5.5 0 0 0 0-1H5.707l2.147-2.146a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708-.708L5.707 8.5H11.5z"/>
                            </svg>
                            Atras
                        </button>
                    </p>

                </fieldset>
            </form>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php'); ?>
</section>
<div id="dialog2" title="Historico de Llamadas" style="display:none"></div>
</body>
</html>