<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
require($r . 'incluir/funciones.php');

$call = $_SESSION['id'];


if (isset($_POST['consultar'])) {
    $fecha1 = $_POST['fecha1'];
    $estado = $_POST['estado'];
} else {
    $fecha1 = isset($_GET['fecha1']) ? $_GET['fecha1'] : '';
    $estado = isset($_GET['estado']) ? $_GET['estado'] : '';
}

$filtro = 'fecha1=' . $fecha1 . '&estado=' . $estado;

$finalizar_agendamiento = isset($_GET['finalizar_agendamiento']) ? $_GET['finalizar_agendamiento'] : 0;
$reactivar_agendamiento = isset($_GET['reactivar_agendamiento']) ? $_GET['reactivar_agendamiento'] : 0;

if ($finalizar_agendamiento == 1) {
    $agenid = $_GET['agenid'];
    $qry = $db->query("UPDATE agendadas SET agenestado = 'FINALIZADO' WHERE agenid = " . $agenid . ";"); // update para indicar que se finaliza el agendamiento
    $num = $qry->rowCount();
    if ($num == 1) {
        $mensaje = "Se ha finalizado correctamente el agendamiento";
        header('Location:lisagendado.php?mensaje=' . $mensaje . '&' . $filtro);
    } else {
        $error = "No se ha podido finalizar el agendamiento por favor contactese con el administrador";
        header('Location:lisagendado.php?error=' . $error . '&' . $filtro);
    }
}

if ($reactivar_agendamiento == 1) {
    $agenid = $_GET['agenid'];
    $qry = $db->query("UPDATE agendadas SET agenestado = 'ACTIVO' WHERE agenid = " . $agenid . ";"); // update para indicar que se finaliza el agendamiento
    $num = $qry->rowCount();
    if ($num == 1) {
        $mensaje = "Se ha reactivado correctamente el agendamiento";
        header('Location:lisagendado.php?mensaje=' . $mensaje . '&' . $filtro);
    } else {
        $error = "No se ha podido reactivar el agendamiento por favor contactese con el administrador";
        header('Location:lisagendado.php?error=' . $error . '&' . $filtro);
    }
}

if (isset($_POST['modificar'])) {

    $prospecto = $_POST['prospecto'];
    $agenid = $_POST['agenid'];
    $fecha2 = $_POST['fecha2'];
    $hora2 = $_POST['hora2'];
    $fecha_hora_capacitacion = $fecha2 . " " . $hora2;

    $qry = $db->query("UPDATE agendadas SET agenfechahorallamar='" . $fecha_hora_capacitacion . "' WHERE agenid='" . $agenid . "' AND agenproid = '" . $prospecto . "';");
    if ($qry) {
        $mensaje = 'Se ha actualizado correctamente la fecha de agendamiento';
        header('Location:lisagendado.php?mensaje=' . $mensaje . '&' . $filtro);
    } else {
        $error = 'No se pudo actualizar la fecha de agendamiento';
        header('Location:lisagendado.php?error=' . $error . '&' . $filtro);
    }


}

if (isset($_GET['mensaje'])) {
    $mensaje = $_GET['mensaje'];
}
if (isset($_GET['error'])) {
    $error = $_GET['error'];
}
?>
<!doctype html>
<html>
<head>
    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>

    <link rel="stylesheet" type="text/css"
          href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css"/>
    <script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.tblW').dataTable({
                'bJQueryUI': true,
                'bStateSave': true,
                'bSort': false,
                'sPaginationType': 'full_numbers',
                'oLanguage': {
                    'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
                },
            });

            $('.msj_finalizar').click(function (e) {
                e.preventDefault();
                var targetUrl = $(this).attr('href');
                var $dialog_link_follow_confirm = $('<div></div>').html("<p><span class='ui-icon ui-icon-alert' style='float:left; margin:2px 7px 7px 0;'></span>Esta seguro que desea Finalizar este agendamiento de llamada?</p>").dialog({
                        title: 'Porfavor confirmar',
                        buttons: {
                            'Si': function () {
                                window.location.href = targetUrl;
                            },
                            'No': function () {
                                $(this).dialog("close");
                            }
                        },
                        modal: true,
                        width: 'auto',
                        height: 'auto'
                    }
                );
                $dialog_link_follow_confirm.dialog("open");
            });
            $('.msj_reactivar').click(function (e) {
                e.preventDefault();
                var targetUrl = $(this).attr('href');
                var $dialog_link_follow_confirm = $('<div></div>').html("<p><span class='ui-icon ui-icon-alert' style='float:left; margin:2px 7px 7px 0;'></span>Esta seguro que desea Reactivar este agendamiento de llamada?</p>").dialog({
                        title: 'Porfavor confirmar',
                        buttons: {
                            'Si': function () {
                                window.location.href = targetUrl;
                            },
                            'No': function () {
                                $(this).dialog("close");
                            }
                        },
                        modal: true,
                        width: 'auto',
                        height: 'auto'
                    }
                );
                $dialog_link_follow_confirm.dialog("open");
            });
        });
    </script>
</head>
<body>
<?php require($r . 'incluir/src/login.php') ?>
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <?php require($r . 'incluir/src/menu.php') ?>
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a href="#">Call Center</a>
            <div class="mapa_div"></div>
            <a class="current">Agendamiento</a>
        </article>
        <article id="contenido">
            <h2>Agendamiento detallado del dia <?php echo $fecha1; ?></h2>
            <table class="tblW">
                <thead>
                <tr>
                    <th title="Finalizar Agendamiento"></th>
                    <th>Cliente</th>
                    <th>Usuario</th>
                    <th title="Fecha de Compromiso">Fecha Compromiso de Llamada</th>
                    <th title="Estado">Estado</th>
                    <th title="Email Uno"></th>
                    <th title="Email Dos"></th>
                    <th title="Telefono Uno"></th>
                    <th title="Telefono Dos"></th>
                    <th title="Celular Uno"></th>
                    <th title="Celular Dos"></th>
                    <th title="Modificar"></th>
                    <th title="Gestionar llamada"></th>
                </tr>
                </thead>
                <tbody>
                <?php

                $con = 'SELECT * FROM agendadas INNER JOIN prospectos ON proid = agenproid inner join usuarios on usuid = agenusuario ';

                $ord = 'ORDER BY agenfechahorallamar;';
                $sql = $con . $ord;


                if (!isset($_POST['show-all'])) {
                    $sql = crearConsulta($con, $ord,
                        array($fecha1, "date(agenfechahorallamar) = '$fecha1'"),
                        array($estado, "agenestado = '$estado'")
                    );
                } else {
                    $sql = $con . ' ' . $ord;
                }

                $qry2 = $db->query($sql);

                ?>

                <?php
                while ($row2 = $qry2->fetch(PDO::FETCH_ASSOC)) {
                    ?>
                    <tr>
                        <td>
                            <?php if ($row2['agenestado'] == 'ACTIVO') { ?>
                                <a href="lisagendado.php?finalizar_agendamiento=1&agenid=<?php echo $row2['agenid'] . "&" . $filtro ?>"
                                   class="msj_finalizar" title="Finalizar Agendamiento"><img
                                            src="<?php echo $r ?>imagenes/iconos/cancel_.png"
                                            class='grayscale'/></a>
                            <?php } elseif ($row2['agenestado'] == 'FINALIZADO') { ?>
                                <a href="lisagendado.php?reactivar_agendamiento=1&agenid=<?php echo $row2['agenid'] . "&" . $filtro ?>"
                                   class="msj_reactivar" title="Reactivar Agendamiento"><img
                                            src="<?php echo $r ?>imagenes/iconos/accept.png" class='grayscale'/></a>
                            <?php } ?>
                        </td>

                        <td><?php echo $row2['pronombre'] ?></td>
                        <td>
                            <?php echo $row2['usunombre'] . '-' . $row2['usuid'] ?>
                        </td>
                        <td align="center"><?php echo $row2['agenfechahorallamar'] ?></td>
                        <td align="center"><?php echo $row2['agenestado'] ?></td>
                        <td>
                            <?php
                            if ($row2['proemail1'] != "")
                                echo '<a href="mailto:' . $row2["proemail1"] . '"><img src="' . $r . 'imagenes/iconos/email.png" title="' . $row2["proemail1"] . '" /></a>';
                            else
                                echo "-";
                            ?>
                        </td>
                        <td>
                            <?php
                            if ($row2['proemail2'] != "")
                                echo '<a href="mailto:' . $row2["proemail2"] . '"><img src="' . $r . 'imagenes/iconos/email.png" title="' . $row2["proemail2"] . '" /></a>';
                            else
                                echo "-";
                            ?>
                        </td>
                        <td>
                            <?php
                            if ($row2['protelefono1'] != "")
                                echo '<a href="tel:' . $row2["protelefono1"] . '"><img src="' . $r . 'imagenes/iconos/telefono.png" title="' . $row2["protelefono1"] . '" /></a>';
                            else
                                echo "-";
                            ?>
                        </td>
                        <td>
                            <?php
                            if ($row2['protelefono2'] != "")
                                echo '<a href="tel:' . $row2["protelefono2"] . '"><img src="' . $r . 'imagenes/iconos/telefono.png" title="' . $row2["protelefono2"] . '" /></a>';
                            else
                                echo "-";
                            ?>
                        </td>
                        <td>
                            <?php
                            if ($row2['procelular1'] != "")
                                echo '<a href="tel:' . $row2["procelular1"] . '"><img src="' . $r . 'imagenes/iconos/telefono.png" title="' . $row2["procelular1"] . '" /></a>';
                            else
                                echo "-";
                            ?>
                        </td>
                        <td>
                            <?php
                            if ($row2['procelular2'] != "")
                                echo '<a href="tel:' . $row2["procelular2"] . '"><img src="' . $r . 'imagenes/iconos/telefono.png" title="' . $row2["procelular2"] . '" /></a>';
                            else
                                echo "-";
                            ?>
                        </td>
                        <td align="center"><a
                                    href="modagendado.php?proid=<?php echo $row2['proid'] . '&agendamientoid=' . $row2['agenid'] . '&' . $filtro ?>"
                                    title="Modificar Hora de Llamada"><img
                                        src="<?php echo $r ?>imagenes/iconos/lapiz.png" class="grayscale"/></a></td>
                        <td align="center"><a
                                    href="llamar.php?proid=<?php echo $row2['proid'] . '&hisproprospecto=' . $row2['proid'] . '&nuevo_pro=1' ?>"
                                    class="confirmar" target="_blank" title="Gestionar Llamada"><img
                                        src="<?php echo $r ?>imagenes/iconos/date_go.png" class="grayscale"/></a>
                        </td>
                    </tr>
                    <?php

                    ?>
                    <?php
                }
                ?>

                </tbody>
            </table>
            <p class="boton">
                <button type="button" class="btn btn-outline-primary btnatras"
                        onClick="carga(); location.href='agendado.php'">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-left-square-fill"
                         fill="currentColor"
                         xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                              d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm9.5 8.5a.5.5 0 0 0 0-1H5.707l2.147-2.146a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708-.708L5.707 8.5H11.5z"/>
                    </svg>
                    Atras
                </button> <!-- BOTON PARA VOLVER A CONSULTAR -->
            </p>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php'); ?>
</section>
<?php
if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>';
elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>';
?>
<div id="modal" style="display:none"></div>
<div id="dialog2" style="display:none"></div>
</body>
</html>