<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

if (isset($_GET['error'])) {
    $error = $_GET['error'];
}
if (isset($_GET['mensaje'])) {
    $mensaje = $_GET['mensaje'];
}

$fecha1 = isset($_POST['fecha1']) ? $_POST['fecha1'] : '';
$fecha2 = isset($_POST['fecha2']) ? $_POST['fecha2'] : '';

?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>

    <link rel="stylesheet" type="text/css"
          href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css"/>
    <script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#tabla').dataTable({
                'bJQueryUI': true,
                'bStateSave': true,
                'sPaginationType': 'full_numbers',
                'oLanguage': {
                    'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
                },
                'bSort': false,
            });
            $('.btnatras').button({icons: {primary: 'ui-icon ui-icon-arrowthick-1-w'}});
            $('#dialog-message').dialog({
                height: 80,
                width: 'auto',
                modal: true
            });
        });
    </script>
</head>
<body>
<?php require($r . 'incluir/src/login.php') ?>
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <?php require($r . 'incluir/src/menu.php') ?>
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a href="#">Call Center</a>
            <div class="mapa_div"></div>
            <a class="current">Semaforo Antiguo</a>
        </article>
        <article id="contenido">
            <h2>Semaforo de Gestiones en el intervalo del <?php echo $fecha1 . " - " . $fecha2 ?></h2>
            <table id="tabla">
                <thead>
                <tr>
                    <th title="Semaforo de Gestiones"></th>
                    <th align="center">Relacionista</th>
                    <th align="center">Gestiones</th>
                    <th align="center">Activo</th>
                    <th align="center">G VS A</th>
                    <th align="center">Confirmado</th>
                    <th align="center">Aplazado</th>
                    <th align="center">Reprogramado</th>
                    <th align="center">Cancelado</th>
                    <th align="center">Realizado</th>
                    <th align="center">Concretadas</th>
                    <th align="center">Tiempo en Disponible</th>
                    <th align="center">Tiempo Hablando</th>
                </tr>
                </thead>
                <tbody>
                <?php
                if (isset($_POST['show-all'])) {
                    $sql = "select u.usunombre                                                                                                as Nombre, " .
                        "       (select count(*) " .
                        "        from hisprospectos " .
                        "        where hisprousuario = u.usuid )                                                                           as Gestiones, " .
                        "       count(*)                                                                                                   as Concretadas, " .
                        "       sum(if(c.capaestado = 1, 1, 0))                                                                            as Activa, " .
                        "       sum(if(c.capaestado = 2, 1, 0))                                                                            as Cancelado, " .
                        "       sum(if(c.capaestado = 3, 1, 0))                                                                            as Confirmado, " .
                        "       sum(if(c.capaestado = 4, 1, 0))                                                                            as Aplazado, " .
                        "       sum(if(c.capaestado = 5, 1, 0))                                                                            as Realizado, " .
                        "       sum(if(c.capaestado = 7, 1, 0))                                                                            as Reprogramado " .
                        "from usuarios u " .
                        "         inner join capacitaciones c on c.capausuario = u.usuid " .
                        "where (select count(*) from hisprospectos where hisprousuario = u.usuid) > 0 " .
                        "group by u.usunombre; ";

                } else {
                    $sql = "select u.usunombre                                                                                                as Nombre, " .
                        "       (select count(*) " .
                        "        from hisprospectos " .
                        "        where hisprousuario = u.usuid " .
                        "          and date(hisprofechahora) between date('$fecha1') and date('$fecha2'))                                                                as Gestiones, " .
                        "       count(*)                                                                                                   as Concretadas, " .
                        "       sum(if(c.capaestado = 1, 1, 0))                                                                            as Activa, " .
                        "       sum(if(c.capaestado = 2, 1, 0))                                                                            as Cancelado, " .
                        "       sum(if(c.capaestado = 3, 1, 0))                                                                            as Confirmado, " .
                        "       sum(if(c.capaestado = 4, 1, 0))                                                                            as Aplazado, " .
                        "       sum(if(c.capaestado = 5, 1, 0))                                                                            as Realizado, " .
                        "       sum(if(c.capaestado = 7, 1, 0))                                                                            as Reprogramado " .
                        "from usuarios u " .
                        "         inner join capacitaciones c on c.capausuario = u.usuid " .
                        "where (select count(*) from hisprospectos where hisprousuario = u.usuid) > 0 " .
                        "and date(c.capafecha) between date('$fecha1') and date('$fecha2') " .
                        "group by u.usunombre; ";

                }
                $qry = $db->prepare($sql);

                $qry->execute();

                $gestiones_totales = 0;
                $concretadas_totales = 0;
                $activas_totales = 0;
                $cancelado_totales = 0;
                $confirmado_totales = 0;
                $aplazado_totales = 0;
                $realizado_totales = 0;
                $reprogramado_totales = 0;
                while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
                    $numgestion = $row['Gestiones'];

                    $numconcretadas = $row['Concretadas'];

                    $gestiones_totales = $gestiones_totales + $numgestion;
                    $concretadas_totales = $concretadas_totales + $numconcretadas;

                    $numactiva = $row['Activa'];
                    $numcancelado = $row['Cancelado'];
                    $numconfirmado = $row['Confirmado'];
                    $numaplazado = $row['Aplazado'];
                    $numrealizado = $row['Realizado'];
                    $numreprogramado = $row['Reprogramado'];

                    $activas_totales = $activas_totales + $numactiva;
                    $cancelado_totales = $cancelado_totales + $numcancelado;
                    $confirmado_totales = $confirmado_totales + $numconfirmado;
                    $aplazado_totales = $aplazado_totales + $numaplazado;
                    $realizado_totales = $realizado_totales + $numrealizado;
                    $reprogramado_totales = $reprogramado_totales + $numreprogramado;

                    $ges_vs_con = $numgestion == 0 ? 0 : ($numactiva * 100) / $numgestion;


                    if ($numgestion > 0) {
                        ?>
                        <tr>
                            <?php if ($numactiva >= 0 and $numactiva <= 2) {
                                $color = "style='background-color: red;'";
                            } elseif ($numactiva >= 3 and $numactiva <= 4) {
                                $color = "style='background-color: yellow;'";
                            } elseif ($numactiva >= 5) {
                                $color = "style='background-color: green;'";
                            } ?>
                            <td title="Semaforo de Gestiones" <?php echo $color; ?> align="center"></td>
                            <td align="center"><?php echo $row['Nombre']; ?></td>
                            <td align="center"><?php echo $numgestion; ?></td>
                            <td align="center"><?php echo $numactiva; ?></td>
                            <td align="center"><?php echo " % " . number_format($ges_vs_con, 2, ',', '.'); ?></td>
                            <td align="center"><?php echo $numconfirmado; ?></td>
                            <td align="center"><?php echo $numaplazado; ?></td>
                            <td align="center"><?php echo $numreprogramado; ?></td>
                            <td align="center"><?php echo $numcancelado; ?></td>
                            <td align="center"><?php echo $numrealizado; ?></td>
                            <td align="center"><?php echo $numconcretadas; ?></td>
                            <td align="center"></td>
                            <td align="center"></td>
                        </tr>
                        <?php
                    }
                }
                ?>
                <tr>
                    <td align="center"></td>
                    <td align="center"></td>
                    <td align="center"><?php echo $gestiones_totales; ?></td>
                    <td align="center"><?php echo $activas_totales; ?></td>
                    <td align="center"></td>
                    <td align="center"><?php echo $confirmado_totales; ?></td>
                    <td align="center"><?php echo $aplazado_totales; ?></td>
                    <td align="center"><?php echo $reprogramado_totales; ?></td>
                    <td align="center"><?php echo $cancelado_totales; ?></td>
                    <td align="center"><?php echo $realizado_totales; ?></td>
                    <td align="center"><?php echo $concretadas_totales; ?></td>
                    <td align="center"></td>
                    <td align="center"></td>
                </tr>
                </tbody>
            </table>
            <p class="boton">
                <button type="button" class="btn btn-outline-primary btnatras  mt-1"
                        onClick="carga(); location.href='consemaforofechas.php'">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-left-square-fill"
                         fill="currentColor"
                         xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                              d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm9.5 8.5a.5.5 0 0 0 0-1H5.707l2.147-2.146a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708-.708L5.707 8.5H11.5z"/>
                    </svg>
                    Atras
                </button>
            </p>
            <?php require($r . 'incluir/src/pie.php'); ?>
        </article>
    </article>
</section>
<?php
if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>';
elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>';
?>
<div id="modal" style="display:none"></div>
<div id="dialog2" style="display:none"></div>
</body>
</html>