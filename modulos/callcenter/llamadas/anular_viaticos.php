<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
require($r . 'incluir/funciones.php');

$viaticosidenti = $_GET['viaticosidenti'];

$asesor = isset($_GET['asesor']) ? $_GET['asesor'] : '';
$fechaida1 = isset($_GET['fecha1']) ? $_GET['fecha1'] : '';
$fechaida2 = isset($_GET['fecha2']) ? $_GET['fecha2'] : '';
$fecharegreso1 = isset($_GET['fecha3']) ? $_GET['fecha3'] : '';
$fecharegreso2 = isset($_GET['fecha4']) ? $_GET['fecha4'] : '';

$filtro = 'asesor=' . $asesor . '&fechaida1=' . $fechaida1 . '&fechaida2=' . $fechaida2 . '&fecharegreso1=' . $fecharegreso1 . '$fecharegreso2=' . $fecharegreso2;

$qry = $db->query("UPDATE viaticos SET viaestado = 'ANULADA' WHERE viaid = '$viaticosidenti'");

?>
<!doctype html>
<html>
<head>

    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>
</head>
<body>
<?php require($r . 'incluir/src/login.php') ?>
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <?php require($r . 'incluir/src/menu.php') ?>
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a href="#">Call Center</a>
            <div class="mapa_div"></div>
            <a class="current">Anular Viaticos</a>
        </article>
        <article id="contenido">
            <p align="center">
                <iframe src="pdfanular_viaticos.php?viaticosidenti=<?php echo $viaticosidenti; ?>" width="800"
                        height="900"></iframe>
            </p>
            <p class="boton">
                <button type="button" class="btn btn-outline-primary btnatras"
                        onClick="carga(); location.href = 'listarviaticos.php?<?php echo $filtro ?>'">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-left-square-fill"
                         fill="currentColor"
                         xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                              d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm9.5 8.5a.5.5 0 0 0 0-1H5.707l2.147-2.146a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708-.708L5.707 8.5H11.5z"/>
                    </svg>
                    Atras
                </button>
            </p>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php'); ?>
</section>
</body>
</html>