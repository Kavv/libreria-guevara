<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

$id_capacitacion = $_GET['capaid'];
$call = $_GET['call'];
$fecha1 = $_GET['fecha1'];
$fecha2 = $_GET['fecha2'];
$consultar = 'consultar';
$asesor = $_GET['asesor'];
$departamento = $_GET['departamento'];

$filtro = "call=" . $call . "&fecha1=" . $fecha1 . "&fecha2=" . $fecha2 . "&asesor=" . $asesor . "&departamento=" . $departamento . "&consultar=" . $consultar;


if (isset($_POST['validar_capa']) && $_POST['validar_capa']) {

    $capacitaid = $_POST['capacitaid'];
    $prospecto = $_POST['prospecto'];
    $realizado = $_POST['realizada'];
    $num_realasistentes = $_POST['asistentes_real'];
    $pedidos = $_POST['pedidos'];
    $comentarios_capa = strtoupper(trim($_POST['comentarios_capacitador']));

    if ($realizado == 'REALIZADA') {
        $estado = '5';
    } elseif ($realizado == 'NO REALIZADA') {
        $estado = '6';
    }

    if (!empty($_POST['empresa1']) && !empty($_POST['numpedido1'])) {

        $cont = 1;
        for ($i = 1; $i <= $pedidos; $i++) {
            $qryinser = $db->query("INSERT INTO pedsol (pedsolsolicitud, pedsolempresa, pedsolcapacitacion, pedsolprospecto) VALUES ('" . $_POST['numpedido' . $cont] . "', '" . $_POST['empresa' . $cont] . "', '" . $capacitaid . "', '" . $prospecto . "');");
            $cont = $cont + 1;
        }
    }

    $qry = $db->query("UPDATE capacitaciones SET capaestado='" . $estado . "', caparealizada='" . $realizado . "', capanumasistentesreal='" . $num_realasistentes . "', capapedidos='" . $pedidos . "', capacomentarioscapaci='" . $comentarios_capa . "' WHERE capaid='" . $capacitaid . "' ;");

    if ($qry) {
        $mensaje = 'Se ha validado correctamente la capacitacion';
        header('Location:liscapacitacion.php?' . $filtro . '&mensaje=' . $mensaje);
        exit();
    } else {
        $error = 'No se pudo validar la capacitacion por favor contactese con el administrador';
        header('Location:liscapacitacion.php?' . $filtro . '&error=' . $error);
        exit();
    }

}

$qry1 = $db->query("SELECT * FROM capacitaciones WHERE capaid = '" . $id_capacitacion . "';");
$row1 = $qry1->fetch(PDO::FETCH_ASSOC);

if ($row1['capaestado'] <> '3') {
    $error = "No se puede validar la capacitacion por que no se encuentra confirmada por el director del call center";
    header('Location:liscapacitacion.php?error=' . $error . '&' . $filtro . '');
}

$qrypermisos = $db->query("SELECT * FROM usuarios WHERE usuid = '" . $_SESSION['id'] . "';");
$rowpermisos = $qrypermisos->fetch(PDO::FETCH_ASSOC);

?>
<!doctype html>
<html>
<head>

    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>
    <script type="text/javascript">

        $(document).ready(function () {


            var MaxInputs = 50; //Número Maximo de Campos
            var contenedor = $("#contenedor"); //ID del contenedor
            var AddButton = $("#agregarCampo"); //ID del Botón Agregar

            //var x = número de campos existentes en el contenedor
            var x = $("#contenedor div").length + 1;
            var FieldCount = x - 1; //para el seguimiento de los campos

            $(AddButton).click(function (e) {
                if (x <= MaxInputs) //max input box allowed
                {
                    FieldCount++;
                    //agregar campo
                    $(contenedor).append('<div class="dinamic"><label style="width:140px;" for="empresa' + FieldCount + '">Empresa ' + FieldCount + ':</label><select name="empresa' + FieldCount + '" class="validate[required]"><option value="">SELECCIONE</option><?php $qry = $db->query("SELECT * FROM empresas ORDER BY empnombre");while ($row = $qry->fetch(PDO::FETCH_ASSOC)) echo "<option value=" . $row['empid'] . ">" . $row['empnombre'] . "</option>";?></select> -	<input type="text" class="validate[required]" name="numpedido' + FieldCount + '" id="pedidonum_' + FieldCount + '" placeholder="Pedido Numero ' + FieldCount + '"/> <a href="#" class="eliminar">&times;</a></div>');
                    x++; //text box increment
                }
                return false;
            });


            $("body").on("click", ".eliminar", function (e) { //click en eliminar campo
                if (x > 1) {
                    $(this).parent('div').remove(); //eliminar el campo
                    x--;
                    FieldCount = FieldCount - 1;
                }
                return false;
            });


            $('#form').validationEngine({
                showOneMessage: true,
                onValidationComplete: function (form, status) {
                    if (status == true) {
                        carga();
                        return true;
                    }
                }
            });
            $('#fecha2').datepicker({
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true,
                minDate: '0D',
            });

            $('#pedidos').change(function (event) {
                var npedidos = $('#pedidos').val();
                if (npedidos > 0) {
                    alert("Es de Caracter obligatorio que ingrese detalle de los  " + npedidos + " que ha ingresado!");
                    $('#empresa1').addClass('validate[required]');
                    $('#pedidonum_1').addClass('validate[required]');
                } else {
                    $('#empresa1').removeClass('validate[required]');
                    $('#pedidonum_1').removeClass('validate[required]');
                }
            });

            $('.historico').click(function () { // ACCION PARA CARASTERISTICO
                newHref = $(this).attr('data-rel');
                $('#dialog2').load(newHref).dialog({
                    modal: true,
                    width: 800
                });
            });

        });
    </script>
</head>
<body>
<?php require($r . 'incluir/src/login.php') ?>
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <?php require($r . 'incluir/src/menu.php') ?>
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a href="#">Call Center</a>
            <div class="mapa_div"></div>
            <a class="current">Validar Capacitacion</a>
        </article>
        <article id="contenido">
            <form id="form" name="form" action="valicapacitacion.php?<?php echo $filtro; ?>" method="post">
                <fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
                    <legend class="ui-widget ui-widget-header ui-corner-all">Validar Capacitacion <img
                                src="<?php echo $r ?>imagenes/iconos/comments.png"
                                data-rel="notas.php?hisproprospecto=<?php echo $row1['capaprospectoid'] . '&hisproid=' . $row1['capaprospectoid'] ?>"
                                class="historico" title="Visualizacion Historico"/></legend>
                    <fieldset class="ui-widget ui-widget-content ui-corner-all">
                        <legend>Datos de la capacitacion</legend>
                        <p>
                            <label style="width:140px;" for="prospecto"> Identificacion del prospecto: </label>
                            <input type="text" name="prospecto" id="prospecto"
                                   value="<?php echo $row1['capaprospectoid'] ?>" class="id validate[required]"
                                   readonly/>
                        </p>
                        <p>
                            <label style="width:140px;" for="capacitaid"> Identificacion de la capacitacion: </label>
                            <input type="text" name="capacitaid" id="capacitaid" value="<?php echo $row1['capaid'] ?>"
                                   class="id validate[required]" readonly/>
                        </p>
                        <p>
                            <?php
                            $qrynombre = $db->query("SELECT * FROM usuarios WHERE usuid = '" . $row1['capausuario'] . "';");
                            $rownombre = $qrynombre->fetch(PDO::FETCH_ASSOC);
                            $nombre_usu = $rownombre['usunombre'];
                            ?>
                            <label style="width:140px;" for="agendadapor"> Capacitacion agendada por: </label>
                            <input style="width:300px" type="text" name="agendadapor" id="agendadapor"
                                   value="<?php echo $nombre_usu ?>" readonly/>
                        </p>
                        <p>
                            <label style="width:140px;" for="diaagenda"> Capacitacion gestionada el dia: </label>
                            <input style="width:300px" type="text" name="diaagenda" id="diaagenda"
                                   value="<?php echo $row1['capafechausuario'] ?>" readonly/>
                        </p>
                        <p>
                            <label style="width:140px;" for="capacitacion"> Capacitacion: </label>
                            <?php
                            if ($row1['capaidlistado'] != '') {
                                $row2 = $db->query("SELECT * FROM liscapacitaciones WHERE liscapaid = '" . $row1['capaidlistado'] . "'")->fetch(PDO::FETCH_ASSOC);
                                echo '<input style="width:350px" type="text" name="capacitacion" id="capacitacion" value="' . $row2['liscapadescripcion'] . '" readonly />';
                            } else {
                                echo '<input style="width:350px" type="text" name="capacitacion" id="capacitacion" value="CAPACITACION NO ASIGNADA" readonly />';
                            }
                            ?>
                        </p>
                        <p>
                            <label style="width:140px;" for="asesor">Asesor:</label>
                            <?php
                            if ($row1['capacapacitador'] != '') {
                                $row2 = $db->query("SELECT * FROM usuarios WHERE usuasesor = '1'  AND usuid = '" . $row1['capacapacitador'] . "' ORDER BY usunombre")->fetch(PDO::FETCH_ASSOC);
                                echo '<input style="width:300px" type="text" name="asesor" id="asesor" value="' . $row2['usunombre'] . '" readonly />';
                            } else {
                                echo '<input style="width:300px" type="text" name="asesor" id="asesor" value="ASESOR NO ASIGNADO" readonly />';
                            }
                            ?>
                        </p>
                        <p>
                            <label style="width:140px;" for="comentarios"> Comentarios: </label>
                            <textarea name="comentarios" class="form-control" id="comentarios" rows="4" cols="65"
                                      disabled><?php echo $row1['capacomentarios'] ?></textarea>
                        </p>
                        <?php
                        $qryhofe = $db->query("SELECT time(capafecha) as horaCapaci , date(capafecha) as fechaCapaci FROM capacitaciones WHERE capaid = '" . $id_capacitacion . "';");
                        $rowhofe = $qryhofe->fetch(PDO::FETCH_ASSOC);
                        $horaCapaci = $rowhofe['horaCapaci'];
                        $fechaCapaci = $rowhofe['fechaCapaci'];
                        ?>
                        <p>
                            <label style="width:140px;" for="fecha2"> Fecha de capacitacion: </label>
                            <input type="text" name="fecha2" value="<?php echo $fechaCapaci ?>" id="fecha2"
                                   class="validate[required] text-input fecha" readonly/>
                        </p>
                        <p>
                            <label style="width:140px;" for="hora2"> Hora de capacitacion: </label>
                            <input type="time" name="hora2" id="hora2" value="<?php echo $horaCapaci ?>" max="23:59:59"
                                   min="00:00:01" class="validate[required]" step="1" readonly>
                        </p>
                        <p>
                            <label style="width:140px;" title="Numero posible de asistentes" for="asistentes">N.P de
                                Asistentes: </label>
                            <input style="width:60px;" type="number" name="asistentes"
                                   value="<?php echo $row1['capanumasistentes'] ?>" class="id validate[required]"
                                   id="asistentes" min="1" max="50" readonly>
                        </p>
                        </br>
                    </fieldset>

                    <fieldset class="ui-widget ui-widget-content ui-corner-all ">
                        <legend>Datos de Validacion</legend>
                        <p>
                            <label style="width:140px;" for="realizada">¿Se realizo? </label>
                            <select name="realizada" class="validate[required]">
                                <option value=""> SELECCIONE</option>
                                <option value="REALIZADA"> REALIZADA</option>
                            </select>
                        </p>
                        <p>
                            <label style="width:140px;" title="Numero real de asistentes" for="asistentes_real">N.R de
                                Asistentes: </label>
                            <input style="width:60px;" type="number" name="asistentes_real"
                                   class="id validate[required]" id="asistentes_real" min="0" max="50">
                        </p>
                        <p>
                            <label style="width:140px;" for="pedidos">Pedidos Logrados </label>
                            <input style="width:60px;" type="number" name="pedidos" class="id validate[required]"
                                   id="pedidos" min="0" max="50"> - <a id="agregarCampo" class="btn-info" href="#">Agregar
                                Campo</a>
                        </p>
                        <p>
                        <div id="contenedor">
                            <div class="added">
                                <label style="width:140px;" for="empresa1">Empresa 1:</label>
                                <select name="empresa1" id="empresa1">
                                    <option value="">SELECCIONE</option>
                                    <?php
                                    $qry = $db->query("SELECT * FROM empresas ORDER BY empnombre");
                                    while ($row = $qry->fetch(PDO::FETCH_ASSOC))
                                        echo '<option value=' . $row['empid'] . '>' . $row['empnombre'] . '</option>';
                                    ?>
                                </select> -
                                <input type="text" name="numpedido1" id="pedidonum_1" placeholder="Pedido Numero 1"/> <a
                                        href="#" class="eliminar">&times;</a>
                            </div>
                        </div>
                        </p>
                        <p>
                            <label style="width:140px;" for="comentarios_capacitador"> Comentarios de la
                                capacitacion: </label>
                            <textarea class="form-control" name="comentarios_capacitador" id="comentarios_capacitador"
                                      rows="4"
                                      cols="65"></textarea>
                        </p>
                    </fieldset>

                    <p class="boton">
                        <button type="button" class="btn btn-outline-primary btnatras"
                                onClick="carga(); location.href='liscapacitacion.php'">
                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-left-square-fill"
                                 fill="currentColor"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                      d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm9.5 8.5a.5.5 0 0 0 0-1H5.707l2.147-2.146a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708-.708L5.707 8.5H11.5z"/>
                            </svg>
                            Atras
                        </button>
                        <button type="submit" class="btn btn-primary" name="validar_capa" value="validar_capa">Validar
                            Capacitacion
                        </button> <!-- BOTON VALIDAR -->
                    </p>
                </fieldset>
            </form>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php'); ?>
</section>
<div id="dialog2" title="Historico de Llamadas" style="display:none"></div>
</body>
</html>