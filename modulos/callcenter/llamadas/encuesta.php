<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

$id_capacitacion = $_GET['capaid'];
$fecha1 = $_GET['fecha1'];
$fecha2 = $_GET['fecha2'];
$consultar = $_GET['consultar'];
$call = $_GET['call'];
$asesor = $_GET['asesor'];
$departamento = $_GET['departamento'];
$estado = $_GET['estado'];
$fecha3 = $_GET['fecha3'];
$fecha4 = $_GET['fecha4'];
$idcapaci = $_GET['idcapaci'];
$idpros = $_GET['idpros'];
$jeferela = $_GET['jeferela'];

$filtro = "jeferela=" . $jeferela . "&idpros=" . $idpros . "&idcapaci=" . $idcapaci . "&encuesta=1&capaid=" . $id_capacitacion . "&call=" . $call . "&fecha1=" . $fecha1 . "&fecha2=" . $fecha2 . "&asesor=" . $asesor . "&departamento=" . $departamento . "&estado=" . $estado . "&consultar=" . $consultar . "&fecha3=" . $fecha3 . "&fecha4=" . $fecha4;


$filtro1 = "jeferela=" . $jeferela . "&idpros=" . $idpros . "&idcapaci=" . $idcapaci . "&call=" . $call . "&fecha1=" . $fecha1 . "&fecha2=" . $fecha2 . "&asesor=" . $asesor . "&departamento=" . $departamento . "&estado=" . $estado . "&consultar=" . $consultar . "&fecha3=" . $fecha3 . "&fecha4=" . $fecha4;


$qry1 = $db->query("SELECT * FROM capacitaciones INNER JOIN prospectos on proid = capaprospectoid WHERE capaid = '" . $id_capacitacion . "';");
$row1 = $qry1->fetch(PDO::FETCH_ASSOC);


$qrypermisos = $db->query("SELECT * FROM usuarios WHERE usuid = '" . $_SESSION['id'] . "';");
$rowpermisos = $qrypermisos->fetch(PDO::FETCH_ASSOC);

?>
<!doctype html>
<html>
<head>

    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>
    <script type="text/javascript">
        $(document).ready(function () {

            $('#fecha2').datepicker({
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true,
                minDate: '0D',
            });

        });
    </script>
</head>
<body>
<?php require($r . 'incluir/src/login.php') ?>
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <?php require($r . 'incluir/src/menu.php') ?>
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a href="#">Call Center</a>
            <div class="mapa_div"></div>
            <a class="current">Encuesta Pos Capacitacion</a>
        </article>
        <article id="contenido">
            <form id="form" name="form" action="liscapacitacion.php?<?php echo $filtro; ?>" method="post">
                <fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
                    <legend class="ui-widget ui-widget-header ui-corner-all">Detalle de de la capacitacion <img
                                src="<?php echo $r ?>imagenes/iconos/comments.png"
                                data-rel="notas.php?hisproprospecto=<?php echo $prospecto . '&hisproid=' . $prospecto ?>"
                                class="historico" title="Visualizacion Historico"/></legend>

                    <p>
                        <label style="width:140px;" for="prospecto"> Identificacion del prospecto: </label>
                        <input type="text" name="prospecto" id="prospecto"
                               value="<?php echo $row1['capaprospectoid'] ?>" class="id validate[required]"/>
                    </p>
                    <p>
                        <label style="width:140px;" for="capaid"> Identificacion de la capacitacion: </label>
                        <input type="text" name="capaid" id="capaid" value="<?php echo $row1['capaid'] ?>"
                               class="id validate[required]"/>
                    </p>
                    <p>
                        <?php
                        $qrynombre = $db->query("SELECT * FROM usuarios WHERE usuid = '" . $row1['capausuario'] . "';");
                        $rownombre = $qrynombre->fetch(PDO::FETCH_ASSOC);
                        $nombre_usu = $rownombre['usunombre'];
                        ?>
                        <label style="width:140px;" for="agendadapor"> Capacitacion agendada por: </label>
                        <input style="width:300px" type="text" name="agendadapor" id="agendadapor"
                               value="<?php echo $nombre_usu ?>"/>
                    </p>
                    <p>
                        <label style="width:140px;" for="nombrepros"> Nombre del prospecto: </label>
                        <input type="text" name="nombrepros" id="nombrepros" value="<?php echo $row1['pronombre'] ?>"/>
                    </p>
                    <p>
                        <label style="width:140px;" for="nombrecontacto"> Nombre del contacto: </label>
                        <input type="text" name="nombrecontacto" id="nombrecontacto"
                               value="<?php echo $row1['procontacto'] ?>"/>
                    </p>

                    </br>
                    <p><b>GUION ENCUESTA TELEFÓNICA CALIDAD Y SERVICIO POS CAPACITACIÓN </b></p>
                    <p>Muy Buenos (días / tardes) mi nombre es <?PHP echo $rowpermisos['usunombre']; ?> de Idencorp de
                        la ciudad de Bogotá, ¿con quién tengo el gusto de hablar? </p>
                    <p>Como está señor (a) XXX habla con la persona con quien se coordinó la capacitación en su empresa
                        si lo recuerda? </p>
                    <p>¿Cómo le ha ido?, bien? </p>
                    <p>Me alegra mucho! </p>
                    <p>El motivo de mi llamada se debe a que para Idencorp S.A es muy importante conocer su percepción
                        sobre la calidad del servicio prestado por nuestro capacitador, razón por la cual quisiera
                        realizarle unas breves preguntas que no le tomarán más de dos (2) minutos de su tiempo, de
                        acuerdo?</p>
                    </br>
                    <p>
                    <fieldset>
                        <legend>¿Usted asistió a la totalidad de la capacitación?</legend>
                        <input class="radio" name="asistencia_total" type="radio" value="SI"/>SI</br>
                        <input class="radio" name="asistencia_total" type="radio" value="NO"/>NO</br>
                    </fieldset>
                    </p>
                    <p>
                    <fieldset>
                        <legend>¿El capacitador fue puntual en su llegada a la empresa a la hora acordada para la
                            capacitación?
                        </legend>
                        <input class="radio" name="puntualidad" type="radio" value="SI"/>SI</br>
                        <input class="radio" name="puntualidad" type="radio" value="NO"/>NO</br>
                    </fieldset>
                    </p>
                    <p>
                    <fieldset>
                        <legend>Por favor califique según su percepción entre las siguientes cuatro opciones si el
                            capacitador demostró preparación, fluidez, dinamismo y conocimiento del tema:
                        </legend>
                        <input class="radio" name="percepcion" type="radio" value="EXCELENTE"/>EXCELENTE</br>
                        <input class="radio" name="percepcion" type="radio" value="BUENO"/>BUENO</br>
                        <input class="radio" name="percepcion" type="radio" value="REGULAR"/>REGULAR</br>
                        <input class="radio" name="percepcion" type="radio" value="MALO"/>MALO</br>
                        <input class="radio" name="percepcion" type="radio" value="PESIMO"/>PESIMO</br>
                    </fieldset>
                    </p>
                    <p>
                    <fieldset>
                        <legend>¿Considera usted que la capacitación fue útil y aportó valor para su vida personal y
                            profesional?
                        </legend>
                        <input class="radio" name="utilidad" type="radio" value="SI"/>SI</br>
                        <input class="radio" name="utilidad" type="radio" value="NO"/>NO</br>
                    </fieldset>
                    </p>
                    <p>
                    <fieldset>
                        <legend>¿Recomendaría usted esta capacitación a un conocido, amigo o colega en otra área o
                            empresa?
                        </legend>
                        <input class="radio" name="recomendar" type="radio" value="SI"/>SI</br>
                        <input class="radio" name="recomendar" type="radio" value="NO"/>NO</br>
                    </fieldset>
                    </p>

                    <p>
                        <textarea name="comentarios" rows="5" cols="103"
                                  class="form-control validate[required] text-input"
                                  placeholder="Comentarios..."></textarea>
                        </br></br>
                    </p>

                    <p>Señor (a) XXX gracias por haber compartido su apreciación de la actividad con nosotros, esto
                        contribuirá a que mejoremos cada día más nuestro servicio.
                        Gracias por haber atendido mi llamada, recuerde que habló
                        con <?PHP echo $rowpermisos['usunombre']; ?> de Buen día/Feliz tarde</p>


                    </br>
                    </br>
                    <p class="boton">
                        <button type="submit" class="btn btn-primary" name="btnvalidar" value="btnvalidar">Validar
                        </button>
                        <button type="button" class="btn btn-outline-primary btnatras"
                                onClick="carga(); location.href='liscapacitacion.php'">
                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-left-square-fill"
                                 fill="currentColor"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                      d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm9.5 8.5a.5.5 0 0 0 0-1H5.707l2.147-2.146a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708-.708L5.707 8.5H11.5z"/>
                            </svg>
                            Atras
                        </button>
                    </p>

                </fieldset>
            </form>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php'); ?>
</section>
<div id="dialog2" title="Historico de Llamadas" style="display:none"></div>
</body>
</html>