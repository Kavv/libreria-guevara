<?php

$r = '../../../';


require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
require($r . 'incluir/funciones.php');

if (isset($_GET['error'])) {
    $error = $_GET['error'];
}

if (isset($_GET['mensaje'])) {
    $mensaje = $_GET['mensaje'];
}

// Si a dado en guardar
if (isset($_POST['cargar'])) {

    if (!isset($_POST['relacionista']) || empty($_POST['relacionista'])) {
        $error = " Es Necesario que seleccione los relacionistas que trabajaran esta campaña.";
        header('Location:cargarcampa.php?error=' . $error . '');
        die();
    }


    $relacionista = $_POST['relacionista'];
    $array = array_envia($relacionista);

    if (!isset($_POST['nombrecampana'])) {
        $error = "Por favor agregue el nombre de la campaña";
        header('Location:cargarcampa.php?error=' . $error . '');
        die();
    }

    $nombre = $_POST["nombrecampana"];
    $nom = $nombre; // Se elimina el strupper

    $qrycampana = $db->query("INSERT INTO campana (campanombre, campaestado) VALUES ('" . $nom . "', 'ACTIVA');");
    $rowcampana = $qrycampana->fetch(PDO::FETCH_ASSOC);

    if ($qrycampana->rowCount() > 0) {

        $qrycam = $db->query("SELECT * FROM campana ORDER BY campaid DESC LIMIT 1;");
        $rowcam = $qrycam->fetch(PDO::FETCH_ASSOC);
        $campana = $rowcam['campaid'];


        foreach ($relacionista as $valor) {
            if ($valor == -1) {
                continue;
            } else {
                $qrycampana = $db->query("INSERT INTO campanarelacionista (idrelacionista, idcampana) VALUES ('" . $valor . "', '" . $rowcam['campaid'] . "');");
            }
        }

        $ms = "Se ha ingresado la campaña " . $nombre;

        // Reseteo
        header('Location:cargarcampa.php?mensaje=' . $ms . '');
    } else {
        $error = "No se ha podido ingresar la campaña";
        header('Location:cargarcampa.php?error=' . $error . '');
        die();

    }


}

?>
<!doctype html>
<html>

<head>


    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>

    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css"/>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#checkAll').click(function () {
                $('input:checkbox').prop('checked', this.checked);
            });

            $('select').selectpicker();

            $('#archivo').on('change', function () {
                //get the file name
                var fileName = $(this).val();
                //replace the "Choose a file" label
                $(this).next('.custom-file-label').html(fileName);
            });

            $('#ckComboRelacionista').on('change', function () {
                var thisObj = $(this);
                var isAllSelected = thisObj.find('option[value="-1"]').prop('selected');
                var lastAllSelected = $(this).data('all');
                var selectedOptions = (thisObj.val()) ? thisObj.val() : [];
                var allOptionsLength = thisObj.find('option[value!="-1"]').length;

                var selectedOptionsLength = selectedOptions.length;

                if (isAllSelected == lastAllSelected) {

                    if ($.inArray("-1", selectedOptions) >= 0) {
                        selectedOptionsLength -= 1;
                    }

                    if (allOptionsLength <= selectedOptionsLength) {

                        thisObj.find('option[value="-1"]').prop('selected', true).parent().selectpicker(
                            'refresh');
                        isAllSelected = true;
                    } else {
                        thisObj.find('option[value="-1"]').prop('selected', false).parent().selectpicker(
                            'refresh');
                        isAllSelected = false;
                    }

                } else {
                    thisObj.find('option').prop('selected', isAllSelected).parent().selectpicker('refresh');
                }

                $(this).data('all', isAllSelected);
            }).trigger('change');
        });
    </script>
</head>

<body>
<?php require($r . 'incluir/src/login.php') ?>
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <?php require($r . 'incluir/src/menu.php') ?>

    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a href="#">Call Center</a>
            <div class="mapa_div"></div>
            <a class="current">Cargar Campaña</a>
        </article>
        <article id="contenido">
            <form id="form" name="form" action="cargarcampa.php" method="post"
                  enctype="multipart/form-data">
                <fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">


                    <legend class="ui-widget ui-widget-header ui-corner-all">Crea una nueva campaña</legend>
                    <p>
                        <label for="nombrecampana">Nombre de Campaña</label>
                        <input type="text" name="nombrecampana" class="nombre validate[[required]] text-input"
                               title="Digite el nombre del cliente"/> <!-- ASIGNAR UN NOMBRE A LA CAMPAÑA -->
                    </p>

                    <label for="relacionista">Relacionistas</label>
                    <select name="relacionista[]" id="ckComboRelacionista" multiple="multiple"
                            class="form-control selectpicker" data-live-search="true"
                            title="Seleccione los relacionistas"
                            data-width="100%">
                        <option value="-1">Seleccionar todos</option>
                        <?php
                        $qry = $db->query("SELECT * FROM usuarios WHERE usucallcenter = '1' and usuperfil <> 56 ORDER BY usunombre;");
                        //Todas las relacionistas del call
                        while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
                            echo '<option value="' . $row['usuid'] . '">' . $row['usunombre'] . '</option>';
                        }
                        ?>
                    </select>

                    <p class="boton">
                        <button type="submit" class="btn btn-primary" name="cargar" value="cargar">Cargar</button>
                    </p>
                </fieldset>
            </form>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php');
    $db = null; ?>
</section>
<?php
if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>';
elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>';
?>
<div id="modal" style="display:none"></div>
<div id="dialog2" style="display:none"></div>
</body>

</html>