<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
require($r . 'incluir/phpexcel/Classes/PHPExcel.php');
require($r . 'incluir/funciones.php');

$hoy = date('Y-m-d');

$fecha1 = $_GET['fecha1'];
$fecha2 = $_GET['fecha2'];
$consultar = $_GET['consultar'];
$call = $_GET['call'];
$asesor = $_GET['asesor'];
$departamento = $_GET['departamento'];
$ciudad = $_GET['ciudad'];
$estado = $_GET['estado'];
$fecha3 = $_GET['fecha3'];
$fecha4 = $_GET['fecha4'];
$uninegoci = $_GET['uninegoci'];
$idcapaci = $_GET['idcapaci'];
$idpros = $_GET['idpros'];
$jeferela = $_GET['jeferela'];

$filtro = "jeferela=" . $jeferela . "&idpros=" . $idpros . "&call=" . $call . "&fecha1=" . $fecha1 . "&fecha2=" . $fecha2 . "&asesor=" . $asesor . "&departamento=" . $departamento . "&ciudad=" . $ciudad . "&estado=" . $estado . "&fecha3=" . $fecha3 . "&fecha4=" . $fecha4 . "&uninegoci=" . $uninegoci . "&idcapaci=" . $idcapaci . "&consultar=" . $consultar;


$con = 'SELECT * FROM capacitaciones INNER JOIN prospectos ON proid = capaprospectoid INNER JOIN usuarios ON usuid = capausuario ';


$ord = ' ORDER BY capafecha desc';


$sql = crearConsulta($con, $ord,
    array($idcapaci, "capaid = '$idcapaci'"),
    array($idpros, "proid = '$idpros'"),
    array($call, "capausuario = '$call'"),
    array($fecha1, "DATE(capafecha) BETWEEN '$fecha1' AND '$fecha2'"),
    array($asesor, "capacapacitador = '$asesor'"),
    array($estado, "capaestado = '$estado'"),
    array($departamento, "prodepartamento = '$departamento'"),
    array($ciudad, "prociudad = '$ciudad'"),
    array($fecha3, "DATE(capafechausuario) BETWEEN '$fecha3' AND '$fecha4'")
);

$qry = $db->query($sql);

$men_titu = '';

if ($uninegoci <> '') {
    $men_titu = ", UNICAMENTE VENTA DIRECTA";
}

if ($fecha1 <> '') {
    $titulo = "CAPACITACIONES AGENDADAS PARA EL INTERVALO $fecha1 - $fecha2 $men_titu ";
} else {
    $titulo = "DETALLE DE LAS CAPACITACIONES $men_titu";
}


$objPHPExcel = new PHPExcel();
$styleArray = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN
        )
    )
);
$objPHPExcel->getActiveSheet()->getStyle('A1:AQ2')->applyFromArray($styleArray);
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:AQ1')
    ->setCellValue('A1', $titulo);
$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A2', 'CODIGO PROSPECTO')
    ->setCellValue('B2', 'NOMBRE PROSPECTO')
    ->setCellValue('C2', 'ESTADO CAPACITACION')
    ->setCellValue('D2', 'MOTIVO DE APLAZAMIENTO')
    ->setCellValue('E2', 'MOTIVO DE CANCELACION')
    ->setCellValue('F2', 'TIEMPO')
    ->setCellValue('G2', 'CAPACITACION')
    ->setCellValue('H2', 'FECHA CAPACITACION')
    ->setCellValue('I2', 'AÑO CAPACITACION')
    ->setCellValue('J2', 'MES CAPACITACION')
    ->setCellValue('K2', 'SEMANA CAPACITACION')
    ->setCellValue('L2', 'DIA CAPACITACION')
    ->setCellValue('M2', 'HORA CAPACITACION')
    ->setCellValue('N2', 'USUARIO')
    ->setCellValue('O2', 'ESTADO USUARIO')
    ->setCellValue('P2', 'GERENTE CONTAC CENTER')
    ->setCellValue('Q2', 'NAP - NAR')
    ->setCellValue('R2', 'FECHA GESTION')
    ->setCellValue('S2', 'AÑO GESTION')
    ->setCellValue('T2', 'MES GESTION')
    ->setCellValue('U2', 'SEMANA GESTION')
    ->setCellValue('V2', 'DIA GESTION')
    ->setCellValue('W2', 'HORA GESTION')
    ->setCellValue('X2', 'DEPARTAMENTO')
    ->setCellValue('Y2', 'CIUDAD')
    ->setCellValue('Z2', 'CAPACITADOR')
    ->setCellValue('AA2', 'GERENTE CAPACITADOR')
    ->setCellValue('AB2', 'NUMERO DE PEDIDOS')
    ->setCellValue('AC2', 'ID CAPACITACION')
    ->setCellValue('AD2', 'UNIDAD DE NEGOCIO')
    ->setCellValue('AE2', 'NUMERO DE ASISTENTES')
    ->setCellValue('AF2', 'NUMERO DE ASISTENTES REAL')
    ->setCellValue('AG2', 'EMAIL 1')
    ->setCellValue('AH2', 'TELEFONO 1')
    ->setCellValue('AI2', 'TELEFONO 2')
    ->setCellValue('AJ2', 'CELULAR 1')
    ->setCellValue('AK2', 'CELULAR 2')
    ->setCellValue('AL2', 'CONTACTO')
    ->setCellValue('AM2', 'CONTCTO ALTERNATIVO')
    ->setCellValue('AN2', 'BARRIO')
    ->setCellValue('AO2', 'DIRECCION')
    ->setCellValue('AP2', 'COACH')
    ->setCellValue('AQ2', 'OBSERVADOR');

$i = 3;
while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {

    if (!empty($row['capausuario'])) {
        $qryrela = $db->query("SELECT * FROM usuarios WHERE usuid = '" . $row['capausuario'] . "';");
        $rowrela = $qryrela->fetch(PDO::FETCH_ASSOC);
        $numrela = $qryrela->rowCount();
        if ($numrela == 1) {
            $relacionista = $rowrela['usunombre'];
            $diasactivo = fechaDif($rowrela['usufechaingreso'], $hoy);
            if ($rowrela['usuperfil'] == 56) {
                $estadorelacionista = "INACTIVO";
            } elseif ($diasactivo >= 60) {
                $estadorelacionista = "ANTIGUO";
            } elseif ($diasactivo < 60) {
                $estadorelacionista = "NUEVO";
            } elseif ($rowrela['usufechaingreso'] == '') {
                $estadorelacionista = "SIN FECHA DE INGRESO";
            }
            if (!empty($rowrela['usujefecallcenter'])) {
                $qryjeferela = $db->query("SELECT * FROM usuarios WHERE usuid = '" . $rowrela['usujefecallcenter'] . "';");
                $rowjeferela = $qryjeferela->fetch(PDO::FETCH_ASSOC);
                $numjeferela = $qryjeferela->rowCount();
                if ($numjeferela == 1) {
                    $jeferelacionista = $rowjeferela['usunombre'];
                } else {
                    $jeferelacionista = "NONE";
                }
            } else {
                $jeferelacionista = "NONE";
            }
        } else {
            $relacionista = "NONE";
            $estadorelacionista = "NONE";
            $jeferelacionista = "NONE";
        }
    } else {
        $relacionista = "NONE";
        $estadorelacionista = "NONE";
        $jeferelacionista = "NONE";
    }

    if (isset($row['capacapacitador'])) {
        $qryca = $db->query("SELECT * FROM usuarios WHERE usuid = '" . $row['capacapacitador'] . "';");
        $rowca = $qryca->fetch(PDO::FETCH_ASSOC);
        $capacitador = isset($rowca['usunombre']) ? $rowca['usunombre'] : '';

    } else {
        $capacitador = "";
    }

    if (isset($row['capacoach'])) {
        $qrycoach = $db->query("SELECT * FROM usuarios WHERE usuid = '" . $row['capacoach'] . "';");
        $rowcoach = $qrycoach->fetch(PDO::FETCH_ASSOC);
        $coach = isset($rowcoach['usunombre']) ? $rowcoach['usunombre'] : '';
    } else {
        $coach = "";
    }

    if (isset($row['capaobservador'])) {
        $qryobservador = $db->query("SELECT * FROM usuarios WHERE usuid = '" . $row['capaobservador'] . "';");
        $rowobservador = $qryobservador->fetch(PDO::FETCH_ASSOC);
        $observador = isset($rowobservador['usunombre']) ? $rowobservador['usunombre'] : '';
    } else {
        $observador = "";
    }


    if (isset($row['usujefeasesor'])) {
        $qryjefeasesor = $db->query("SELECT * FROM usuarios WHERE usuid = '" . $row['usujefeasesor'] . "';");
        $rowjefeasesor = $qryjefeasesor->fetch(PDO::FETCH_ASSOC);
        $numjefeasesor = $qryjefeasesor->rowCount();
        if ($numjefeasesor == 1) {
            $jefeasesor = $rowjefeasesor['usunombre'];
        } else {
            $jefeasesor = "NONE";
        }
    } else {
        $jefeasesor = "NONE";
    }

    if (!empty($row['prodepartamento'])) {
        $qrydepar = $db->query("SELECT * FROM departamentos WHERE depid = '" . $row['prodepartamento'] . "';");
        $rowdepar = $qrydepar->fetch(PDO::FETCH_ASSOC);
        $numdepar = $qrydepar->rowCount();
        if ($numdepar == 1) {
            $departamento = $rowdepar['depnombre'];
            $qryciu = $db->query("SELECT * FROM ciudades WHERE ciudepto = '" . $rowdepar['depid'] . "' AND ciuid = '" . $row['prociudad'] . "' ;");
            $rowciu = $qryciu->fetch(PDO::FETCH_ASSOC);
            $numciu = $qryciu->rowCount();
            if ($numciu == 1) {
                $ciudad = $rowciu['ciunombre'];
            } else {
                $ciudad = "NONE";
            }
        } else {
            $departamento = "NONE";
            $ciudad = "NONE";
        }
    } else {
        $departamento = "NONE";
        $ciudad = "NONE";
    }

    if (($row['capaestado']) == 1) {
        $estado = 'ACTIVA';
    } elseif (($row['capaestado']) == 2) {
        $estado = 'CANCELADA';
    } elseif (($row['capaestado']) == 3) {
        $estado = 'CONFIRMADA';
    } elseif (($row['capaestado']) == 4) {
        $estado = 'APLAZADA';
    } elseif (($row['capaestado']) == 5) {
        $estado = 'REALIZADA';
    } elseif (($row['capaestado']) == 6) {
        $estado = 'NO REALIZADA';
    } elseif (($row['capaestado']) == 7) {
        $estado = 'REPROGRAMADA';
    }

    if ($row['capaestado'] == 5) {
        $asis = $row['capanumasistentes'] . "P - " . $row['capanumasistentesreal'] . "R";
    } else {
        $asis = "--";
    }

    if (!empty($row['capaaplarepro'])) {
        $qryapla = $db->query("SELECT * FROM aplarepro WHERE aplareproid = '" . $row['capaaplarepro'] . "';");
        $rowapla = $qryapla->fetch(PDO::FETCH_ASSOC);
        $numapla = $qryapla->rowCount();
        if ($numapla == 1) {
            $aplarepro = $rowapla['aplareprodescripcion'];
            if ($row['capaestado'] == 4) {
                $aplazamiento = $aplarepro;
            } elseif ($row['capaestado'] == 7) {
                $reprogramacion = $aplarepro;
            } else {
                $aplazamiento = '';
                $reprogramacion = '';
            }
        } else {
            $aplarepro = "NONE";
            $aplazamiento = '';
            $reprogramacion = '';
        }
    } else {
        $aplarepro = "NONE";
        $aplazamiento = '';
        $reprogramacion = '';
    }

    if (!empty($row['capacancelacion'])) {
        $qrycanc = $db->query("SELECT * FROM cancelaciones WHERE canceid = '" . $row['capacancelacion'] . "';");
        $rowcanc = $qrycanc->fetch(PDO::FETCH_ASSOC);
        $numcanc = $qrycanc->rowCount();
        if ($numcanc == 1) {
            $cancelacion = $rowcanc['cancedescripcion'];
        } else {
            $cancelacion = "NONE";
        }
    } else {
        $cancelacion = "";
    }

    $fechacapa = substr($row['capafecha'], 0, 10);
    $horacapa = substr($row['capafecha'], 10, 20);

    if ($hoy < $fechacapa) {
        $tiempo = "FUTURO";
    } elseif ($hoy > $fechacapa) {
        $tiempo = "PASADO";
    } elseif ($hoy == $fechacapa) {
        $tiempo = "PRESENTE";
    }

    if (!empty($row['capauninegocio'])) {
        $qryuni = $db->query("SELECT * FROM uninegocio WHERE uniid = '" . $row['capauninegocio'] . "';");
        $rowuni = $qryuni->fetch(PDO::FETCH_ASSOC);
        $numuni = $qryuni->rowCount();
        if ($numuni == 1) {
            $uninegocio = $rowuni['unidescripcion'];
        } else {
            $uninegocio = "NONE";
        }
    } else {
        $uninegocio = "NONE";
    }

    $dateone = date_create($row['capafecha']);
    $semanaCapacitacion = date_format($dateone, 'W');

    $datetwo = date_create($row['capafechausuario']);
    $semanaGestion = date_format($datetwo, 'W');


    $objPHPExcel->getActiveSheet()->getStyle('A' . $i . ':AO' . $i)->applyFromArray($styleArray);
    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A' . $i, $row['proid'])
        ->setCellValue('B' . $i, $row['pronombre'])
        ->setCellValue('C' . $i, $estado)
        ->setCellValue('D' . $i, $aplazamiento)
        ->setCellValue('E' . $i, $cancelacion)
        ->setCellValue('F' . $i, $tiempo)
        ->setCellValue('G' . $i, $row['capanombre'])
        ->setCellValue('H' . $i, $fechacapa)
        ->setCellValue('I' . $i, substr($row['capafecha'], 0, 4))
        ->setCellValue('J' . $i, substr($row['capafecha'], 5, 2))
        ->setCellValue('K' . $i, $semanaCapacitacion)
        ->setCellValue('L' . $i, substr($row['capafecha'], 8, 3))
        ->setCellValue('M' . $i, $horacapa)
        ->setCellValue('N' . $i, $relacionista)
        ->setCellValue('O' . $i, $estadorelacionista)
        ->setCellValue('P' . $i, $jeferelacionista)
        ->setCellValue('Q' . $i, $asis)
        ->setCellValue('R' . $i, substr($row['capafechausuario'], 0, 10))
        ->setCellValue('S' . $i, substr($row['capafechausuario'], 0, 4))
        ->setCellValue('T' . $i, substr($row['capafechausuario'], 5, 2))
        ->setCellValue('U' . $i, $semanaGestion)
        ->setCellValue('V' . $i, substr($row['capafechausuario'], 8, 3))
        ->setCellValue('W' . $i, substr($row['capafechausuario'], 10, 20))
        ->setCellValue('X' . $i, $departamento)
        ->setCellValue('Y' . $i, $ciudad)
        ->setCellValue('Z' . $i, $capacitador)
        ->setCellValue('AA' . $i, $jefeasesor)
        ->setCellValue('AB' . $i, $row['capapedidos'])
        ->setCellValue('AC' . $i, $row['capaid'])
        ->setCellValue('AD' . $i, $uninegocio)
        ->setCellValue('AE' . $i, $row['capanumasistentes'])
        ->setCellValue('AF' . $i, $row['capanumasistentesreal'])
        ->setCellValue('AG' . $i, $row['proemail1'])
        ->setCellValue('AH' . $i, $row['protelefono1'])
        ->setCellValue('AI' . $i, $row['protelefono2'])
        ->setCellValue('AJ' . $i, $row['procelular1'])
        ->setCellValue('AK' . $i, $row['procelular2'])
        ->setCellValue('AL' . $i, $row['procontacto'])
        ->setCellValue('AM' . $i, $row['procontactoalternativo'])
        ->setCellValue('AN' . $i, $row['probarrio'])
        ->setCellValue('AO' . $i, $row['prodireccion'])
        ->setCellValue('AP' . $i, $coach)
        ->setCellValue('AQ' . $i, $observador);
    $i++;
}
// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Listado Capacitaciones.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
?>