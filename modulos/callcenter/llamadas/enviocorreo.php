<?php
$r = '../../../';

require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
require($r . 'incluir/phpmailer/PHPMailer/class.phpmailer.php');
require($r . 'incluir/phpmailer/PHPMailer/class.smtp.php');
require($r . 'incluir/mail/send_email.php');

if (isset($_GET['mensaje'])) {
    $mensaje = $_GET['mensaje'];
}

if (isset($_GET['error'])) {
    $error = $_GET['error'];
}
if (isset($_GET['prospecto'])) {
    $prospecto = $_GET['prospecto'];
}

if (isset($_POST['prospecto'])) {

    $prospectop = $_POST['prospecto'];
}
if ((isset($_POST['validar']) and $_POST['validar']) or (isset($_POST['atras']) and $_POST['atras'])) {

    $prospectop = $_POST['prospecto'];
    $email1 = $_POST['email1'];
    $email2 = $_POST['email2'];

    if (!empty($_POST['capacitacion'])) {
        $qry1 = $db->query("SELECT * FROM liscapacitaciones WHERE liscapaid = " . $_POST['capacitacion'] . ";");
        $row1 = $qry1->fetch(PDO::FETCH_ASSOC);

        $capacitacion_dictar = $row1['liscapadescripcion'];
        $qryusuario = $db->query("SELECT * FROM usuarios INNER JOIN perfiles ON usuperfil = perid WHERE usuid = '" . $_SESSION['id'] . "'"); //verificacion usuario por ID de sesion
        $rowusuario = $qryusuario->fetch(PDO::FETCH_ASSOC);
        if ($rowusuario['usugenero'] == 'M') {
            $genero = 'Coordinador';
        } else {
            $genero = 'Coordinadora';
        }


        $email1 = htmlspecialchars($_POST['email1']);
        $email2 = htmlspecialchars($_POST['email2']);
        $asunto = htmlspecialchars($_POST['asunto']);

        $adjunto = $_FILES['adjunto'];
        $name = $_FILES['adjunto']['name'];
        $tmp_name = $_FILES['adjunto']['tmp_name'];

        $mail = new PHPMailer();

        if ($asunto == "CONFIRMACION DE CAPACITACION") {
            $qryverifi = $db->query("SELECT * FROM capacitaciones WHERE capaprospectoid ='" . $prospectop . "' order by capafecha desc;");
            $numcapacitaciones = $qryverifi->rowCount();
            if ($numcapacitaciones == 1) {
                $rowverifi = $qryverifi->fetch(PDO::FETCH_ASSOC);
                $confirmacion = "<p style='color:green;'>Le recuerdo su capacitacion esta confirmada para el dia " . $rowverifi['capafecha'] . "  y se confirma minimo con " . $rowverifi['capanumasistentes'] . " asistentes.</p>";
            } elseif ($numcapacitaciones > 1) {
                $rowverifi = $qryverifi->fetch(PDO::FETCH_ASSOC);
                $confirmacion = "<p style='color:green;'>Le recuerdo su capacitacion esta confirmada para el dia " . $rowverifi['capafecha'] . "  y se confirma minimo con " . $rowverifi['capanumasistentes'] . " asistentes.</p>";
            }
        } else {
            $confirmacion = '';
        }

        $body = isset($_POST['cuerpo']) ? $_POST['cuerpo'] : '';

        $mail->IsSMTP(); // telling the class to use SMTP
        $mail->SMTPDebug = 0;                     // enables SMTP debug information (for testing)
        // 1 = errors and messages
        // 2 = messages only
        $mail->SMTPAuth = true;                  // enable SMTP authentication
        $mail->SMTPSecure = "ssl";                 // sets the prefix to the servier

        // Se agregaron estas varibales a enviar correo

        $mail->Host = $defaultHost;      // sets GMAIL as the SMTP server
        $mail->Port = $defaultPort;                   // set the SMTP port for the GMAIL server
        $mail->Username = $defaultUsername;
        $mail->Password = $defaultPassword;


        $mail->SetFrom($rowusuario['usuemail'], $rowusuario['usunombre']);
        $mail->AddReplyTo($rowusuario['usuemail'], $rowusuario['usunombre']);
        $mail->Subject = $asunto . " - " . $row1['liscapadescripcion'];
        $mail->AddAttachment($tmp_name, $name);
        $mail->AltBody = "Envío adjunto información sobre la conferencia que lleva el nombre de: "; // optional, comment out and test
        $mail->MsgHTML(utf8_decode($body));
        //$mail->AddEmbeddedImage($r.'imagenes/jackmiller.jpg', 'imagen','IDENCORP','base64','image/jpeg');
        $address = $email1;
        $address_copy = $email2;
        $mail->AddAddress($address);
        $mail->AddCC($address_copy);
        if (!$mail->Send()) {
            $msg = "No se pudo enviar el email por la siguiente causa: " . $mail->ErrorInfo;
            header('Location:enviocorreo.php?prospecto=' . $prospectop . '&error=' . $msg);
        } elseif ($_POST['atras']) {
            header('Location:llamar.php?proid=' . $prospectop . '&hisproprospecto=' . $prospectop . '');
        } else {
            header('Location:llamar.php');
        }

    } //cierre if fecha and capacitacion

} //cierre if POST validar

$qryprin = $db->query("SELECT * FROM prospectos WHERE proid = '" . $prospecto . "';");
$rowprin = $qryprin->fetch(PDO::FETCH_ASSOC);

$qryverifi = $db->query("SELECT * FROM capacitaciones WHERE capaprospectoid ='" . $prospecto . "' order by capaid desc;");
$rowverifi = $qryverifi->fetch(PDO::FETCH_ASSOC);
$numverifi = $qryverifi->rowCount();

?>
<!doctype html>
<html>
<head>

    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>
    <script type="text/javascript">
        $(document).ready(function () {

            $('#fecha2').datepicker({
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true,
                minDate: '0D',
            });
        });
    </script>
</head>
<body>
<?php require($r . 'incluir/src/login.php') ?>
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <?php require($r . 'incluir/src/menu.php') ?>
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a href="#">Call Center</a>
            <div class="mapa_div"></div>
            <a class="current">Envio de Correo</a>
        </article>
        <article id="contenido">
            <form id="form" name="form" enctype="multipart/form-data" action="enviocorreo.php" method="post">
                <fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
                    <legend class="ui-widget ui-widget-header ui-corner-all">Envio de Correo</legend>
                    <?php
                    if ($numverifi >= 1) {
                        $qryusuarioagendo = $db->query("SELECT * FROM usuarios WHERE usuid = '" . $rowverifi['capausuario'] . "';");
                        $rowusuarioagendo = $qryusuarioagendo->fetch(PDO::FETCH_ASSOC);
                        echo "<p style='color:red'> Este prospecto ya tiene agendada una capacitacion con tematica de " . $rowverifi['capanombre'] . " para el dia " . $rowverifi['capafecha'] . " con una posibilidad de asistentes de " . $rowverifi['capanumasistentes'] . " y fue agendada por " . $rowusuarioagendo['usunombre'] . ". </p></br>";
                    }
                    ?>
                    <label style="width:150px;" for="prospecto"> Identificacion de prospectos: </label>
                    <input type="text" name="prospecto" id="prospecto" value="<?php echo $prospecto ?>"
                           class="id validate[required]" readonly/>
                    </p>
                    <p>
                        <label style="width:150px;" for="email1"> Email Destinatario 1: </label>
                        <input type="text" name="email1" id="email1" value="<?php echo $rowprin['proemail1'] ?>"
                               class="email validate[required, custom[email]] text-input"/>
                    </p>
                    <p>
                        <label style="width:150px;" for="email2"> Email Destinatario 2: </label>
                        <input type="text" name="email2" id="email2" value="<?php echo $rowprin['proemail2'] ?>"
                               class="email validate[custom[email]] text-input"/>
                    </p>
                    <p>
                        <label style="width:150px;" for="asunto"> Asunto: </label>
                        <select name="asunto" id="asunto" class="validate[required] text-input"/>
                        <option value=""> SELECCIONE</option>
                        <option value="INVITACION A CAPACITACION"> INVITACION A CAPACITACION</option>
                        <option value="CONFIRMACION DE CAPACITACION"> CONFIRMACION DE CAPACITACION</option>
                        </select>
                    </p>
                    <label for="cuerpo">Cuerpo del correo</label>
                    <textarea name="cuerpo" cols="30" rows="10" class="form-control"></textarea>
                    <p>
                        <label style="width:150px;" for="adjunto"> Adjunto Archivo: </label>
                        <input type="file" name="adjunto" id="adjunto"/>
                    </p>
                    <p>
                        <label style="width:150px;" for="capacitacion"> Capacitacion: </label>
                        <select name="capacitacion" id="capacitacion" class="validate[required] text-input">
                            <option value=""> SELECCIONE</option>
                            <?php
                            $qry = $db->query("SELECT * FROM liscapacitaciones WHERE liscapaestado = 1 ORDER BY liscapadescripcion");
                            while ($row4 = $qry->fetch(PDO::FETCH_ASSOC)) {
                                echo '<option class="validate[required]" value="' . $row4["liscapaid"] . '">' . $row4['liscapadescripcion'] . '</option>';
                            }
                            echo '<option class="validate[required]" value="99">SEMINARIO EXCEL JUNTO CON HABILIDADES</option>';
                            ?>
                        </select>
                    </p>
                    </br>
                    <p class="boton">
                        <button type="submit" class="btn btn-primary" name="validar" value="validar">Enviar y seguir
                            gestionando
                        </button> <!-- BOTON VALIDAR -->
                        <button type="submit" class="btn btn-primary" name="atras" value="atras">Enviar y volver al
                            prospecto
                        </button> <!-- BOTON ATRAS -->
                    </p>


                </fieldset>
            </form>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php'); ?>
</section>
<?php
if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>';
elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>';
?>
<div id="modal" style="display:none"></div>
<div id="dialog2" style="display:none"></div>
</body>
</html>