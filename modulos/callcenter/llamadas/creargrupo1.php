<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

if (isset($_GET['nombregrupo'])) {
    $nombregrupo = $_GET['nombregrupo'];
} else if (isset($_POST['nombregrupo'])) {

    $nombregrupo = $_POST['nombregrupo'];


    $sqlQuery = "select count(*) from gruposcall where gruponombre = '$nombregrupo'";

    $qryCount = $db->query($sqlQuery);

    if ($qryCount->fetchColumn() > 0) {
        $error = 'El nombre del grupo ya está siendo usado, puede  editar un grupo desde consultar grupos';
        header('Location:creargrupo.php?error=' . $error . '');
        die();
    }

} else {
    $error = 'Necesita especificar el nombre del grupo';
    header('Location:creargrupo.php?error=' . $error . '');
    die();
}

$filtro = 'nombregrupo=' . $nombregrupo;


if (isset($_GET['rela_menos']) && $_GET['rela_menos']) {

    $relacionista = $_GET['relacionista'];
    $nombregrupo = trim(strtoupper($_GET['nombregrupo']));

    $qry = $db->query("DELETE FROM gruposcall WHERE gruponombre = '$nombregrupo' AND grupointegrantes = '$relacionista'");


    header('Location:creargrupo1.php?' . $filtro);
} elseif (isset($_POST['agregarrelacionista']) && $_POST['agregarrelacionista']) {

    $nombregrupo = trim(strtoupper($_POST['nombregrupo']));
    $relacionista = $_POST['relacionista'];

    $hoy = date('Y-m-d');
    $qry = $db->query("INSERT INTO gruposcall (gruponombre, grupointegrantes, grupofecha) VALUES ('" . $nombregrupo . "', '" . $relacionista . "', '" . $hoy . "');");

    header('Location:creargrupo1.php?' . $filtro);
}

?>
<!doctype html>
<html>
<head>
    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>

    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css"/>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('select').selectpicker();
        }
    </script>
</head>
<body>
<?php require($r . 'incluir/src/login.php') ?>
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <?php require($r . 'incluir/src/menu.php') ?>
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a href="#">Call Center</a>
            <div class="mapa_div"></div>
            <a class="current">Nuevo Grupo</a>
        </article>
        <article id="contenido">
            <h2 align="center"> Grupo - <?php echo $nombregrupo ?> </h2>
            <form id="form" name="form" action="creargrupo1.php?nombregrupo=<?php echo $nombregrupo ?>" method="post">
                <fieldset id="field" class="ui-widget ui-widget-content ui-corner-all col-md-6">
                    <legend class="ui-widget ui-widget-header ui-corner-all">Nuevo Grupo Form 2</legend>

                    <br>
                    <br>
                    <p align="center">
                    <div class="input-group">
                        <select name="relacionista" class="validate[required] text-input selectpicker form-control"
                                data-live-search="true">
                            <option value="">SELECCIONE</option>
                            <?php
                            $qry = $db->query("SELECT * FROM usuarios WHERE usucallcenter = '1' AND usuperfil <> '56' AND usuid NOT IN (SELECT grupointegrantes FROM gruposcall WHERE gruponombre = '$nombregrupo') ORDER BY usunombre");
                            while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
                                echo '<option value="' . $row2['usuid'] . '">' . $row2['usunombre'] . '</option>';
                            }
                            ?>
                        </select>
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-success"
                                          name="agregarrelacionista"
                                          value="agregarrelacionista">+
                            </button>
                        </div>
                    </div>
                    </p>
                    <input type="hidden" name="nombregrupo" value="<?php echo $nombregrupo ?>"/>
                    <br>
                    <br>
                    <fieldset class="ui-widget ui-widget-content ui-corner-all">
                        <legend class="ui-widget ui-widget-header ui-corner-all">Relacionistas Asignados</legend>
                        <?php
                        $qry = $db->query("SELECT * FROM gruposcall INNER JOIN usuarios ON grupointegrantes = usuid WHERE gruponombre = '$nombregrupo' order by usunombre ");
                        $numresul = $qry->rowCount();
                        while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
                            ?>
                            <p align="center">
                            <div class="input-group">
                                <input type="text" name="usuarioasig" class="form-control not-w"
                                       value="<?php echo $row2['usunombre'] ?>"
                                       readonly/>
                                <div class="input-group-append">
                                    <button type="button" class="btn btn-danger"
                                            onclick="carga(); location.href = 'creargrupo1.php?<?php echo 'nombregrupo=' . $nombregrupo . '&relacionista=' . $row2['usuid'] . '&rela_menos=aIUOfHbHjFHoLm48129692jF45n' ?>'">
                                        -
                                    </button>
                                </div>
                            </div>
                            </p>
                            <?php
                        }
                        if ($numresul == 0) {
                            echo "<p>Este grupo no posee relacionistas asigandas, por favor revise y asigne relacionistas si es pertinente.</p>";
                        }
                        ?>
                    </fieldset>
                    <p class="boton">
                        <button type="button" class="btn btn-outline-primary btnatras"
                                onClick="carga(); location.href='creargrupo.php?<?php echo $filtro ?>'">
                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-left-square-fill"
                                 fill="currentColor"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                      d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm9.5 8.5a.5.5 0 0 0 0-1H5.707l2.147-2.146a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708-.708L5.707 8.5H11.5z"/>
                            </svg>
                            Atras
                        </button> <!-- BOTON PARA VOLVER A CONSULTAR -->
                    </p>
                </fieldset>
            </form>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php'); ?>
</section>
<?php
if (isset($_GET['error'])) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['error'] . '</div>';
?>
<div id="modal" style="display:none"></div>
<div id="dialog2" style="display:none"></div>
</body>
</html>