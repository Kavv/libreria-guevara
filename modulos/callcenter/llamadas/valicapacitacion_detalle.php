<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

$id_capacitacion = $_GET['capaid'];
$fecha1 = $_GET['fecha1'];
$fecha2 = $_GET['fecha2'];
$consultar = $_GET['consultar'];
$call = $_GET['call'];
$asesor = $_GET['asesor'];
$departamento = $_GET['departamento'];
$estado = $_GET['estado'];
$fecha3 = $_GET['fecha3'];

$filtro = "call=" . $call . "&fecha1=" . $fecha1 . "&fecha2=" . $fecha2 . "&asesor=" . $asesor . "&departamento=" . $departamento . "&estado=" . $estado . "&consultar=" . $consultar;


$qry1 = $db->query("SELECT * FROM capacitaciones WHERE capaid = '" . $id_capacitacion . "';");
$row1 = $qry1->fetch(PDO::FETCH_ASSOC);


$qrypermisos = $db->query("SELECT * FROM usuarios WHERE usuid = '" . $_SESSION['id'] . "';");
$rowpermisos = $qrypermisos->fetch(PDO::FETCH_ASSOC);

?>
<!doctype html>
<html>
<head>

    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#fecha2').datepicker({
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true,
                minDate: '0D',
            });
            $('.historico').click(function () { // ACCION PARA CARASTERISTICO
                newHref = $(this).attr('data-rel');
                $('#dialog2').load(newHref).dialog({modal: true, width: 800});
            });

        });
    </script>
</head>
<body>
<?php require($r . 'incluir/src/login.php') ?>
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <?php require($r . 'incluir/src/menu.php') ?>
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a href="#">Call Center</a>
            <div class="mapa_div"></div>
            <a class="current">Detalle de validacion de la capacitacion</a>
        </article>
        <article id="contenido">
            <form id="form" name="form" action="liscapacitacion.php?<?php echo $filtro; ?>" method="post">
                <fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
                    <legend class="ui-widget ui-widget-header ui-corner-all">Detalle de validacion de la capacitacion
                        <img src="<?php echo $r ?>imagenes/iconos/comments.png"
                             data-rel="notas.php?hisproprospecto=<?php echo $prospecto . '&hisproid=' . $prospecto ?>"
                             class="historico" title="Visualizacion Historico"/></legend>

                    <fieldset class="ui-widget ui-widget-content ui-corner-all">
                        <legend>Datos de la capacitacion</legend>
                        <p>
                            <label style="width:140px;" for="prospecto"> Identificacion del prospecto: </label>
                            <input type="text" name="prospecto" id="prospecto"
                                   value="<?php echo $row1['capaprospectoid'] ?>" class="id validate[required]"/>
                        </p>
                        <p>
                            <label style="width:140px;" for="capacitaid"> Identificacion de la capacitacion: </label>
                            <input type="text" name="capacitaid" id="capacitaid" value="<?php echo $row1['capaid'] ?>"
                                   class="id validate[required]"/>
                        </p>
                        <p>
                            <?php
                            $qrynombre = $db->query("SELECT * FROM usuarios WHERE usuid = '" . $row1['capausuario'] . "';");
                            $rownombre = $qrynombre->fetch(PDO::FETCH_ASSOC);
                            $nombre_usu = $rownombre['usunombre'];
                            ?>
                            <label style="width:140px;" for="agendadapor"> Capacitacion agendada por: </label>
                            <input style="width:300px" type="text" name="agendadapor" id="agendadapor"
                                   value="<?php echo $nombre_usu ?>"/>
                        </p>
                        <p>
                            <label style="width:140px;" for="diaagenda"> Capacitacion gestionada el dia: </label>
                            <input style="width:300px" type="text" name="diaagenda" id="diaagenda"
                                   value="<?php echo $row1['capafechausuario'] ?>"/>
                        </p>
                        <p>
                            <label style="width:140px;" for="capacitacion"> Capacitacion: </label>
                            <select name="capacitacion" id="capacitacion" class="validate[required] text-input">
                                <?php
                                if ($row1['capaidlistado'] != '') {
                                    $row2 = $db->query("SELECT * FROM liscapacitaciones WHERE liscapaid = '" . $row1['capaidlistado'] . "'")->fetch(PDO::FETCH_ASSOC);
                                    echo '<option class="validate[required]" value="' . $row2["liscapaid"] . '">' . $row2['liscapadescripcion'] . '</option>';
                                }
                                ?>
                            </select>
                        </p>
                        <p>
                            <label style="width:140px;" for="asesor">Asesor:</label>
                            <select name="asesor">
                                <?php
                                if ($row1['capacapacitador'] != '') {
                                    $row2 = $db->query("SELECT * FROM usuarios WHERE usuasesor = '1'  AND usuid = '" . $row1['capacapacitador'] . "' ORDER BY usunombre")->fetch(PDO::FETCH_ASSOC);
                                    echo '<option class="validate[required]" value="' . $row2["usuid"] . '">' . $row2['usunombre'] . '</option>';
                                }
                                ?>
                            </select>
                        </p>
                        <p>
                            <label style="width:140px;" for="comentarios"> Comentarios: </label>
                            <textarea name="comentarios" class="form-control" id="comentarios" rows="4"
                                      cols="65"><?php echo $row1['capacomentarios'] ?></textarea>
                        </p>
                        <?php
                        $qryhofe = $db->query("SELECT time(capafecha) as horaCapaci , date(capafecha) as fechaCapaci FROM capacitaciones WHERE  capaid = '" . $id_capacitacion . "';");
                        $rowhofe = $qryhofe->fetch(PDO::FETCH_ASSOC);
                        $horaCapaci = $rowhofe['horaCapaci'];
                        $fechaCapaci = $rowhofe['fechaCapaci'];
                        ?>
                        <p>
                            <label style="width:140px;" for="fecha2"> Fecha de capacitacion: </label>
                            <input type="text" name="fecha2" value="<?php echo $fechaCapaci ?>" id="fecha2"
                                   class="validate[required] text-input fecha"/>
                            <input type="hidden" name="fecha2_actual" value="<?php echo $fechaCapaci ?>"/>
                        </p>
                        <p>
                            <label style="width:140px;" for="hora2"> Hora de capacitacion: </label>
                            <input type="time" name="hora2" id="hora2" value="<?php echo $horaCapaci ?>" max="23:59:59"
                                   min="00:00:01" class="validate[required]" step="1">
                        </p>
                        <p>
                            <label style="width:140px;" title="Numero posible de asistentes" for="asistentes">N.P de
                                Asistentes: </label>
                            <input style="width:60px;" type="number" name="asistentes"
                                   value="<?php echo $row1['capanumasistentes'] ?>" class="id validate[required]"
                                   id="asistentes" min="1" max="50">
                        </p>
                    </fieldset>

                    <fieldset class="ui-widget ui-widget-content ui-corner-all">
                        <legend>Datos de Validacion del capacitador</legend>
                        <p>
                            <label style="width:140px;" for="realizada">¿Se realizo? </label>
                            <select name="realizada" class="validate[required]">
                                <option> <?php echo $row1['caparealizada'] ?> </option>
                            </select>
                        </p>
                        <p>
                            <label style="width:140px;" title="Numero real de asistentes" for="asistentes_real">N.R de
                                Asistentes: </label>
                            <input style="width:60px;" type="number" name="asistentes_real"
                                   value="<?php echo $row1['capanumasistentesreal'] ?>" class="id validate[required]"
                                   id="asistentes_real" min="0" max="50">
                        </p>
                        <p>
                            <label style="width:140px;" for="pedidos">Pedidos Logrados </label>
                            <input style="width:60px;" type="number" name="pedidos"
                                   value="<?php echo $row1['capapedidos'] ?>" class="id validate[required]" id="pedidos"
                                   min="0" max="50">
                        </p>
                        <?php
                        if ($row1['capapedidos'] >= 1) {
                            $qrypedidos = $db->query("SELECT * FROM pedsol WHERE pedsolcapacitacion = " . $row1['capaid'] . " AND pedsolprospecto = " . $row1['capaprospectoid'] . ";");
                            $cont = 1;
                            while ($rowpedidos = $qrypedidos->fetch(PDO::FETCH_ASSOC)) {
                                echo "<p><label style='width:140px;' for='numpedido" . $cont . "'>Pedido Numero " . $cont . " </label> <input type='text' name='numpedido" . $cont . "' value ='" . $rowpedidos['pedsolsolicitud'] . "' /> - ";

                                echo "<select name='empresa" . $cont . "'>";
                                $qry = $db->query("SELECT * FROM empresas WHERE empid = " . $rowpedidos['pedsolempresa'] . " ORDER BY empnombre");
                                while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
                                    echo '<option value=' . $row['empid'] . '>' . $row['empnombre'] . '</option>';
                                }
                                echo "</select></p>";
                                $cont = $cont + 1;
                            }
                        }
                        ?>
                        <p>
                            <label style="width:140px;" for="comentarios_capacitador"> Comentarios de la
                                capacitacion: </label>
                            <textarea name="comentarios_capacitador" class="form-control" id="comentarios_capacitador"
                                      rows="4"
                                      cols="65"><?php echo $row1['capacomentarioscapaci'] ?></textarea>
                        </p>
                    </fieldset>
                    </br>
                    <p class="boton">
                        <button type="button" class="btn btn-primary"
                                onClick="carga(); location.href='liscapacitacion.php?<?php echo $filtro ?>'">Atras
                        </button>  <!-- BOTON ATRAS -->

                    </p>

                </fieldset>
            </form>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php'); ?>
</section>
<div id="dialog2" title="Historico de Llamadas" style="display:none"></div>
</body>
</html>