<?php
// LA ACCION PRINCIPAL ES ESTA PAGINA ES EL MANEJO DE LLAMADAS PARA EL CALL CENTER
$r = '../../../';
//INCLUIR SESION Y CONECCION
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

$identificador = $_GET['id'];

$qry = $db->query("SELECT * FROM viaticos WHERE viaid = " . $identificador . ";");

?>
<!doctype html>
<html>
<head>

    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>
    <script type="text/javascript">

        $(document).ready(function () {

            $('#form').validationEngine({ //VALIDACION DE CAMPOS REQUERIDOS EN FORMULARIO
                showOneMessage: true,
                onValidationComplete: function (form, status) {
                    if (status) {
                        carga();
                        return true;
                    }
                }
            });

            $('#fecha2').datepicker({ // VALIDACION DE LA FECHA UNO
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true,
                onClose: function (selectedDate) {
                    $('#fecha3').datepicker('option', 'minDate', selectedDate);
                }
            }).keypress(function (event) {
                event.preventDefault()
            });
            $('#fecha3').datepicker({ // VALIDACION DE LA FECHA DOS
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true,
                onClose: function (selectedDate) {
                    $('#fecha2').datepicker('option', 'maxDate', selectedDate);
                }
            }).keypress(function (event) {
                event.preventDefault()
            });

            $('#departamento').change(function (event) {
                var id1 = $('#departamento').find(':selected').val();
                $('#ciudad').load('<?php echo $r ?>incluir/carga/ciudades.php?id1=' + id1);
            });

            $('#monto_total').change(function (event) {
                var nVal = $('#monto_total').val();
                $('#monto2, #monto3').val(nVal / 2);
            });

            $('.btnvalidar').button({icons: {primary: 'ui-icon ui-icon-check'}});
            $('.btnatras').button({icons: {primary: 'ui-icon ui-icon-arrowthick-1-w'}});
        });
    </script>
</head>
<body>
<?php require($r . 'incluir/src/login.php') ?> <!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
    <?php require($r . 'incluir/src/menu.php') ?> <!-- INCLUIMOS MENU PRINCIPAL -->
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a href="#">Call Center</a>
            <div class="mapa_div"></div>
            <a class="current">Insertar Viaticos</a>
        </article>
        <article id="contenido">
            <form id="form" name="form" action="viaticos_2.php" method="post"> <!-- ENVIO FORMULARIO POR POST -->
                <fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
                    <legend class="ui-widget ui-widget-header ui-corner-all">Insertar Viaticos</legend>
                    </br>
                    <?php while ($rowprin = $qry->fetch(PDO::FETCH_ASSOC)) { ?>
                        <p>
                            <label for="iden"> Identificador: </label>
                            <input type="text" class="id" name="iden" id="iden" value="<?php echo $rowprin['viaid']; ?>"
                                   readonly/>
                        </p>
                        <p>
                            <label for="fechaida"> Fecha Ida y Regreso: </label>
                            <input type="text" name="fechaida" id="fechaida"
                                   value="<?php echo $rowprin['viadiaviajeida'] ?>"
                                   class="validate[required] text-input fecha" disabled/> - <input type="text"
                                                                                                   name="fecharegreso"
                                                                                                   id="fecharegreso"
                                                                                                   value="<?php echo $rowprin['viadiaviajeregreso'] ?>"
                                                                                                   class="validate[required] text-input fecha"
                                                                                                   disabled/>
                        </p>
                        <p>
                            <label>Provincia:</label>
                            <select id="departamento" name="departamento" class="validate[required] text-input"
                                    disabled>
                                <?php
                                $qry = $db->query("SELECT * FROM departamentos WHERE depid = " . $rowprin['viadepdestino'] . " ORDER BY depnombre");
                                while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
                                    echo '<option value=' . $row2['depid'] . '>' . $row2['depnombre'] . '</option>';
                                }
                                ?>
                            </select>
                        </p>
                        <p>
                            <label>Ciudad: </label>
                            <select name="ciudad" id="ciudad" class="validate[required] text-input" disabled>
                                <?php
                                $qry = $db->query("SELECT * FROM ciudades WHERE ciudepto = " . $rowprin['viadepdestino'] . " AND ciuid =  " . $rowprin['viaciudestino'] . " ORDER BY ciunombre");
                                while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
                                    echo '<option value=' . $row2['ciuid'] . '>' . $row2['ciunombre'] . '</option>';
                                }
                                ?>
                            </select>
                        </p>
                        <p>
                            <label for="asesor">Asesor:</label>
                            <select name="asesor" class="validate[required] text-input" disabled>
                                <?php
                                $qry = $db->query("SELECT * FROM usuarios WHERE usuasesor = '1' AND usuperfil <> 56 AND usuid = '" . $rowprin['viacapacitador'] . "' ORDER BY usunombre");
                                while ($row = $qry->fetch(PDO::FETCH_ASSOC))
                                    echo '<option value=' . $row['usuid'] . '>' . $row['usunombre'] . '</option>';
                                ?>
                            </select>
                        </p>
                        <p>
                            <label for="empresa">Empresa del capacitador:</label>
                            <select name="empresa" class="validate[required] text-input" disabled>
                                <?php
                                $qry = $db->query("SELECT * FROM empresas WHERE empid = '" . $rowprin['viaempcapacitador'] . "'");
                                while ($row = $qry->fetch(PDO::FETCH_ASSOC))
                                    echo '<option value=' . $row['empid'] . '>' . $row['empnombre'] . '</option>';
                                ?>
                            </select>
                        </p>
                        <p>
                            <label title='Monto Total'>Monto Total: </label>
                            <?php
                            if (empty($rowprin['viavalortotal'])) {
                                echo "<input type='text' name='monto_total' id='monto_total' class='valor validate[required, custom[onlyNumberSp]] text-input'  disabled />";
                            } elseif (!empty($rowprin['viavalortotal'])) {
                                $montos = ($rowprin['viavalortotal'] / 2);
                                echo "<input type='text' value ='" . $rowprin['viavalortotal'] . "' name='monto_total' id='monto_total' class='valor validate[required, custom[onlyNumberSp]] text-input' disabled  />";
                            }
                            ?>
                        </p>
                        <p>
                            <label title='Fecha y monto del pago del primer 50%'>F. Monto del 1 50%: </label>
                            <?php
                            if (!empty($rowprin['viafechavalor1'])) {
                                echo "<input type='text' name='fecha2' id='fecha2' value='" . $rowprin['viafechavalor1'] . "' class='validate[required] text-input fecha'/>";
                            } elseif (empty($rowprin['viafechavalor1'])) {
                                echo "<input type='text' name='fecha2' id='fecha2' class='validate[required] text-input fecha'/>";
                            }
                            echo "<span> - </span>";
                            if (!empty($rowprin['viavalor1'])) {
                                echo "<input type='text' name='monto2' id='monto2' value='" . $rowprin['viavalor1'] . "' class='valor validate[required, custom[onlyNumberSp]] text-input' readonly/>";
                            } elseif (empty($rowprin['viavalor1'])) {
                                echo "<input type='text' name='monto2' id='monto2' value='" . $montos . "' class='valor validate[required, custom[onlyNumberSp]] text-input' readonly/>";
                            }
                            ?>
                        </p>
                        <p>
                            <label title='Fecha y monto del pago del segundo 50%'>F. Monto del 2 50%: </label>
                            <?php
                            if (!empty($rowprin['viafechavalor2'])) {
                                echo "<input type='text' name='fecha3' id='fecha3' value='" . $rowprin['viafechavalor2'] . "' class='validate[required] text-input fecha'/>";
                            } elseif (empty($rowprin['viafechavalor2'])) {
                                echo "<input type='text' name='fecha3' id='fecha3' class='validate[required] text-input fecha'/>";
                            }
                            echo "<span> - </span>";
                            if (!empty($rowprin['viavalor2'])) {
                                echo "<input type='text' name='monto3' id='monto3' value='" . $rowprin['viavalor2'] . "' class='valor validate[required, custom[onlyNumberSp]] text-input' readonly/>";
                            } elseif (empty($rowprin['viavalor2'])) {
                                echo "<input type='text' name='monto3' id='monto3' value='" . $montos . "' class='valor validate[required, custom[onlyNumberSp]] text-input' readonly/>";
                            }
                            ?>
                        </p>
                        <p>
                            <label for="mesdili">Mes Diligenciamiento:</label>
                            <select name="mesdili" class="validate[required] text-input">
                                <?php
                                if ($rowprin['viamesdiligenciamiento'] <> 0) {
                                    $qry = $db->query("SELECT * FROM meses_ano WHERE mesid = '" . $rowprin['viamesdiligenciamiento'] . "'");
                                    while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
                                        echo '<option value=' . $row['mesid'] . '>' . $row['mesnombre'] . '</option>';
                                    }
                                } elseif ($rowprin['viamesdiligenciamiento'] == 0) {
                                    $qry = $db->query("SELECT * FROM meses_ano ");
                                    echo "<option value=''> SELECCIONE </option>";
                                    while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
                                        echo '<option value=' . $row['mesid'] . '>' . $row['mesnombre'] . '</option>';
                                    }
                                }
                                ?>
                            </select>
                        </p>
                        </br>
                    <?php } ?>
                    <p class="boton">
                        <button type="button" class="btn btn-primary" onClick="carga(); location.href='viaticos_2.php'">
                            Atras
                        </button>
                        <button type="submit" class="btn btn-primary" name="validar" value="validar">Validar</button>
                        <!-- BOTONES VALIDAR Y ATRAS -->
                    </p>
                </fieldset>
            </form>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php'); ?> <!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
</section>
</body>
</html>