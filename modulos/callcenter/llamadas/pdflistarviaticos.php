<?php

$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
include($r . 'incluir/fpdf/fpdf.php');

$asesor = $_GET['asesor'];
$fechaida1 = $_GET['fechaida1'];
$fechaida2 = $_GET['fechaida2'];
$fecharegreso1 = $_GET['fecharegreso1'];
$fecharegreso2 = $_GET['fecharegreso2'];


if (isset($_POST['modificar'])) {

    $iden = $_POST['iden'];
    $dia_viaje_ida = $_POST['fechaida'];
    $dia_viaje_regreso = $_POST['fecharegreso'];
    $departamento = $_POST['departamento'];
    $ciudad = $_POST['ciudad'];
    $asesor = $_POST['asesor'];
    $empresa_asesor = $_POST['empresa_asesor'];
    $mes_diligenciamiento = $_POST['mesdili'];
    $fechavalor1 = $_POST['fecha2'];
    $valor1 = $_POST['monto2'];
    $fechavalor2 = $_POST['fecha3'];
    $valor2 = $_POST['monto3'];
    $valor_total = $_POST['monto_total'];

    $qry = $db->query("UPDATE viaticos SET viadiaviajeida='" . $dia_viaje_ida . "', viadiaviajeregreso='" . $dia_viaje_regreso . "', viadepdestino='" . $departamento . "', viaciudestino='" . $ciudad . "', viacapacitador='" . $asesor . "', viaempcapacitador='" . $empresa_asesor . "', viamesdiligenciamiento='" . $mes_diligenciamiento . "', viafechavalor1='" . $fechavalor1 . "', viavalor1='" . $valor1 . "', viafechavalor2='" . $fechavalor2 . "', viavalor2='" . $valor2 . "', viavalortotal='" . $valor_total . "' WHERE viaid='" . $iden . "';");
    if ($qry) {
        header('Location:finalizar_viaticos.php?iden=' . $iden . '');
    } else {
        $error = 'No se pudo actualizar el prospecto';
    }
}

if (isset($_GET['error'])) {
    $error = $_GET['error'];
}

$filtro = 'asesor=' . $asesor . '&fechaida1=' . $fechaida1 . '&fechaida2=' . $fechaida2 . '&fecharegreso1=' . $fecharegreso1 . '$fecharegreso2=' . $fecharegreso2;
$con = 'SELECT * FROM viaticos ';
$ord = 'ORDER BY viadiadiligencia DESC'; // Ordenar la Consulta
$group = ''; // Realizar Group By 

if ($asesor != '' && $fechaida1 == '' && $fecharegreso1 == '') $sql = "$con WHERE viacapacitador = '$asesor' $group $ord";
elseif ($asesor == '' && $fechaida1 != '' && $fecharegreso1 == '') $sql = "$con WHERE DATE(viadiaviajeida) BETWEEN '$fechaida1' AND '$fechaida2' $group $ord";
elseif ($asesor != '' && $fechaida1 != '' && $fecharegreso1 == '') $sql = "$con WHERE viacapacitador = '$asesor' AND DATE(viadiaviajeida) BETWEEN '$fechaida1' AND '$fechaida2' $group $ord";
elseif ($asesor == '' && $fechaida1 == '' && $fecharegreso1 != '') $sql = "$con WHERE DATE(viadiaviajeregreso) BETWEEN '$fecharegreso1' AND '$fecharegreso2' $group $ord";
elseif ($asesor != '' && $fechaida1 == '' && $fecharegreso1 != '') $sql = "$con WHERE DATE(viadiaviajeregreso) BETWEEN '$fecharegreso1' AND '$fecharegreso2' AND viacapacitador = '$asesor' $group $ord";
elseif ($asesor == '' && $fechaida1 == '' && $fecharegreso1 == '') $sql = "$con  $group $ord";

$qry = $db->query($sql);

$pdf = new FPDF();
$pdf->AddFont('LucidaConsole', '', 'lucon.php');
$pdf->AddPage('L', 'Letter');
$pdf->SetMargins('30', '25', '30');
$pdf->SetFont('LucidaConsole', '', 8);
$pdf->Cell(0, 3, date('Y/m/d H:i:s'), 0, 1);
$pdf->Cell(0, 3, '---------------------------------------------------------------------------------', 0, 1, 'C');
$pdf->SetFont('Arial', 'B', 10);
$pdf->Cell(0, 4, 'LISTADO DE VIATICOS', 0, 1, 'C');
$pdf->SetFont('LucidaConsole', '', 8);
if ($asesor != '') {
    $qryasesor = $db->query("SELECT * FROM usuarios where usuid = " . $asesor . "; ");
    $rowasesor = $qryasesor->fetch(PDO::FETCH_ASSOC);
    $nombreasesor = $rowasesor['usunombre'];
    $pdf->Cell(0, 4, 'DEL ASESOR: ' . $nombreasesor, 0, 1, 'C');
}
if ($fechaida1 != '') $pdf->Cell(0, 4, 'FECHA DE IDA DESDE: ' . $fechaida1, 0, 1, 'C');
if ($fechaida2 != '') $pdf->Cell(0, 4, 'FECHA DE IDA HASTA: ' . $fechaida2, 0, 1, 'C');
if ($fecharegreso1 != '') $pdf->Cell(0, 4, 'FECHA DE REGRESO DESDE: ' . $fecharegreso1, 0, 1, 'C');
if ($fecharegreso2 != '') $pdf->Cell(0, 4, 'FECHA DE REGRESO HASTA: ' . $fecharegreso2, 0, 1, 'C');
$pdf->Cell(0, 3, '---------------------------------------------------------------------------------', 0, 1, 'C');
$pdf->Ln(5);
$pdf->SetFont('LucidaConsole', '', 8);
$pdf->SetFillColor(220, 220, 220);
$pdf->SetX(2);
$pdf->Cell(42, 5, 'ASESOR: ', 1, 0, '', true);
$pdf->Cell(30, 5, 'EMPRESA: ', 1, 0, '', true);
$pdf->Cell(15, 5, 'IDA: ', 1, 0, '', true);
$pdf->Cell(16, 5, 'REGRESO: ', 1, 0, '', true);
$pdf->Cell(26, 5, 'DEPARTAMENTO: ', 1, 0, '', true);
$pdf->Cell(20, 5, 'CIUDAD: ', 1, 0, '', true);
$pdf->Cell(19, 5, 'MES D. : ', 1, 0, '', true);
$pdf->Cell(18, 5, 'PAGO1 50%: ', 1, 0, '', true);
$pdf->Cell(20, 5, 'MONTO1 50%: ', 1, 0, '', true);
$pdf->Cell(18, 5, 'PAGO2 50%: ', 1, 0, '', true);
$pdf->Cell(20, 5, 'MONTO2 50%: ', 1, 0, '', true);
$pdf->Cell(16, 5, 'VLR TOTAL: ', 1, 0, '', true);
$pdf->Cell(16, 5, 'ESTADO: ', 1, 1, '', true);
$i = 1;
$vlr1 = 0;


while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {

    $qrydeparta = $db->query("SELECT * FROM departamentos where depid = " . $row['viadepdestino'] . "; ");
    $rowdeparta = $qrydeparta->fetch(PDO::FETCH_ASSOC);
    $departamento = $rowdeparta['depnombre'];

    $qryciudad = $db->query("SELECT * FROM ciudades where ciudepto = " . $row['viadepdestino'] . " AND ciuid = " . $row['viaciudestino'] . " ");
    $rowciudad = $qryciudad->fetch(PDO::FETCH_ASSOC);
    $ciudad = $rowciudad['ciunombre'];

    $qryasesor = $db->query("SELECT * FROM usuarios where usuid = '" . $row['viacapacitador'] . "'; ");
    $rowasesor = $qryasesor->fetch(PDO::FETCH_ASSOC);
    $asesor = substr($rowasesor['usunombre'], 0, 32);

    $qryempresa = $db->query("SELECT * FROM empresas where empid = '" . $row['viaempcapacitador'] . "'; ");
    $rowempresa = $qryempresa->fetch(PDO::FETCH_ASSOC);
    $empresa = substr($rowempresa['empnombre'], 0, 23);

    if ($row['viamesdiligenciamiento'] == 0) {
        $meses = 'NO DEFINIDO';
    } else {
        $qrymeses = $db->query("SELECT * FROM meses_ano where mesid = " . $row['viamesdiligenciamiento'] . "; ");
        $rowmeses = $qrymeses->fetch(PDO::FETCH_ASSOC);
        $meses = $rowmeses['mesnombre'];
    }
    $pdf->SetFont('LucidaConsole', '', 6);
    if ($i % 2 == 0) $x = 255;
    else $x = 235;
    $pdf->SetFillColor($x, $x, $x);
    $pdf->SetX(2);
    $pdf->Cell(42, 4, $asesor, 1, 0, 'L', true);
    $pdf->Cell(30, 4, $empresa, 1, 0, 'C', true);
    $pdf->Cell(15, 4, $row['viadiaviajeida'], 1, 0, 'C', true);
    $pdf->Cell(16, 4, $row['viadiaviajeregreso'], 1, 0, 'C', true);
    $pdf->Cell(26, 4, $departamento, 1, 0, 'C', true);
    $pdf->Cell(20, 4, $ciudad, 1, 0, 'C', true);
    $pdf->Cell(19, 4, $meses, 1, 0, 'C', true);
    $pdf->Cell(18, 4, $row['viafechavalor1'], 1, 0, 'C', true);
    $pdf->Cell(20, 4, number_format($row['viavalor1'],2), 1, 0, 'C', true);
    $pdf->Cell(18, 4, $row['viafechavalor2'], 1, 0, 'C', true);
    $pdf->Cell(20, 4, number_format($row['viavalor2'],2), 1, 0, 'C', true);
    $pdf->Cell(16, 4, number_format($row['viavalortotal'],2), 1, 0, 'C', true);
    $pdf->Cell(16, 4, $row['viaestado'], 1, 1, 'C', true);
    $i++;
    $vlr1 = $vlr1 + $row['viavalortotal'];
}//END WHILE
$pdf->SetFont('Arial', 'B', 8);
$pdf->SetX(2);
$pdf->Cell(260, 4, 'TOTAL DEL LISTADO:  $' . number_format($vlr1,2), 1, 1, 'R', true);
$pdf->Output('resumen.pdf', 'd');
?>