<?php
// LA ACCION PRINCIPAL DE ESTA PAGINA ES MODIFICAR UN PROSPECTO ENVIANDO LA INFORMACION A LISTAR.PHP

$r = '../../../';
//INCLUIR SESION Y CONECCION
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

if (isset($_GET['error'])) {
    $error = $_GET['error'];
}
if (isset($_GET['mensaje'])) {
    $mensaje = $_GET['mensaje'];
}

if ((isset($_POST['insertar']) && $_POST['insertar']) || (isset($_POST['insertarges']) && $_POST['insertarges'])) {

    $nombrecliente = strtoupper(trim($_POST['nombrecliente']));  // MAYUSCULA
    $nombrealternativo = strtoupper(trim($_POST['nombrealternativo']));  // MAYUSCULA
    $tipocli = $_POST['tipocli'];
    $depresidencia = $_POST['depresidencia'];
    $ciuresidencia = $_POST['ciuresidencia'];
    $barrio = strtoupper(trim($_POST['barrio']));  // MAYUSCULA
    $direccion = strtoupper(trim($_POST['direccion']));  // MAYUSCULA
    $indicaciones = strtoupper(trim($_POST['indicaciones']));  // MAYUSCULA
    $email1 = strtoupper(trim($_POST['email1']));  // MAYUSCULA
    $email2 = strtoupper(trim($_POST['email2']));  // MAYUSCULA
    $telefono1 = $_POST['telefono1'];
    $telefono2 = $_POST['telefono2'];
    $celular1 = $_POST['celular1'];
    $celular2 = $_POST['celular2'];
    $contacto = strtoupper(trim($_POST['contacto']));  // MAYUSCULA
    $contactoalternativo = strtoupper(trim($_POST['contactoalternativo']));  // MAYUSCULA

    $campana_num = $_POST['campana'];

    $qry = $db->query("INSERT INTO prospectos (procampana, protipo, pronombre, pronombrealternativo, prodepartamento, prociudad, probarrio, prodireccion, proindicaciones, proemail1, proemail2, protelefono1, protelefono2, procelular1, procelular2, procontacto, procontactoalternativo, progestion, procargue, proingresado) VALUES ('" . $campana_num . "', '" . $tipocli . "', '" . $nombrecliente . "', '" . $nombrealternativo . "', '" . $depresidencia . "', '" . $ciuresidencia . "', '" . $barrio . "', '" . $direccion . "', '" . $indicaciones . "', '" . $email1 . "', '" . $email2 . "', '" . $telefono1 . "', '" . $telefono2 . "', '" . $celular1 . "', '" . $celular2 . "', '" . $contacto . "', '" . $contactoalternativo . "', '0', '0', '" . $_SESSION['id'] . "');");

    if (isset($_POST['insertarges']) && $_POST['insertarges']) {
        $qrygesti = $db->query("SELECT * FROM prospectos ORDER BY proid DESC LIMIT 1");
        $rowgesti = $qrygesti->fetch(PDO::FETCH_ASSOC);
        header('Location:llamar.php?proid=' . $rowgesti['proid'] . '&hisproprospecto=&nuevo_pro=1');
    } elseif (isset($_POST['insertar']) && $_POST['insertar']) {
        if ($qry) {
            $mensaje = 'Se ha insertado correctamente el prospecto';
            header('Location:insertar_pros.php?mensaje=' . $mensaje);

        } else {
            $error = 'No se pudo insertar el prospecto';
            header('Location:insertar_pros.php?error=' . $error);

        }
    }


}

?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">

    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>
    <script>
        $(document).ready(function () {
            $('#depresidencia').change(function (event) {
                var id1 = $('#depresidencia').find(':selected').val();
                $('#ciuresidencia').load('<?php echo $r ?>incluir/carga/ciudades.php?id1=' + id1);
            });
        });
    </script>
</head>
<body>
<?php require($r . 'incluir/src/login.php') ?> <!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
    <?php require($r . 'incluir/src/menu.php') ?> <!-- INCLUIMOS MENU PRINCIPAL -->
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a href="#">Call Center</a>
            <div class="mapa_div"></div>
            <a class="current">Insertar Prospecto</a>
        </article>
        <article id="contenido">
            <div class="ui-widget">
                <form id="form" name="form" action="insertar_pros.php" method="post">
                    <!-- FORMULARIO ENVIADO POR POST A SI MISMO  -->
                    <h2>Insertar Prospecto</h2>
                    <fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
                        <legend class="ui-widget ui-widget-header ui-corner-all">Datos Personales</legend>

                        <p>
                            <label for="nombrecliente">Nombre del Cliente:</label>
                            <input type="text" style="width:65%" class="id validate[required] text-input"
                                   name="nombrecliente"/> <!-- CAMPO NOMBRE CLIENTE -->
                        </p>
                        <p>
                            <label for="nombrealternativo">Nombre Alternativo:</label>
                            <input type="text" style="width:65%" class="id" name="nombrealternativo"/>
                            <!-- CAMPO NOMBRE ALTERNATIVO -->
                        </p>
                        <p>
                            <label>Tipo de Cliente: </label> <!-- CAMPO TIPO DE PROSPECTO -->
                            <select name="tipocli" id="tipocli" class="validate[required] text-input">
                                <?php
                                echo '<option value="1" selected >PERSONA</option>';
                                echo '<option value="2">EMPRESA</option>';
                                ?>
                            </select>
                        </p>
                        <p>
                            <label>Campaña: </label> <!-- CAMPO TIPO DE PROSPECTO -->
                            <select name="campana" id="campana" class="validate[required] text-input">
                                <?php
                                echo '<option value="">SELECCIONE</option>';

                                $sqlConsult = "select distinct c.campaid, c.campanombre from campana c inner join campanarelacionista cr on c.campaid = cr.idcampana where cr.idrelacionista = '" . $_SESSION['id'] . "'  and c.campaestado = 'ACTIVA';";

                                $qry = $db->query($sqlConsult);

                                echo $sqlConsult;

                                while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
                                    echo '<option value=' . $row2['campaid'] . '>' . $row2['campanombre'] . '</option>'; // APARECEN TODOS LOS DEPARTAMENTOS PARA QUE EL USUARIO ELIGA EL PERTINENTE
                                }
                                ?>
                            </select>
                        </p>
                    </fieldset>

                    </br></br>

                    <fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
                        <legend class="ui-widget ui-widget-header ui-corner-all">Datos de Ubicacion</legend>
                        <!-- DATOS DE UBICACION -->
                        <p>
                            <label>Provincia:</label> <!-- CAMPO DEPARTAMENTO -->
                            <select id="depresidencia" name="depresidencia" class="validate[required] text-input">
                                <?php
                                if ($row['prodepartamento'] != '') { // SI EL DEPARTAMENTO DEL PROSPECTO ES DISTINTO A VACIO
                                    $row2 = $db->query("SELECT * FROM departamentos WHERE depid = '" . $row['prodepartamento'] . "'")->fetch(PDO::FETCH_ASSOC); // SE BUSCA ESPECIFICAMENTE EL DEPARTAMENTO
                                    echo '<option value=' . $row['prodepartamento'] . '>' . $row2['depnombre'] . '</option>'; // SE MUESTAR LOS RESULTADOS TANTO NOMBRE DEL DEPARTAMENTO COMO SU ID EN UN SELECT
                                } // SI EL DEPARTAMENTO DEL PROSPECTO ESTA VACIO
                                echo '<option value="">SELECCIONE</option>';
                                $qry = $db->query("SELECT * FROM departamentos WHERE depid <> '" . $row['prodepartamento'] . "' ORDER BY depnombre");
                                while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
                                    echo '<option value=' . $row2['depid'] . '>' . $row2['depnombre'] . '</option>'; // APARECEN TODOS LOS DEPARTAMENTOS PARA QUE EL USUARIO ELIGA EL PERTINENTE
                                }
                                ?>
                            </select>
                        </p>
                        <p>
                            <label>Ciudad: </label> <!-- CAMPO CIUDAD -->
                            <select name="ciuresidencia" id="ciuresidencia" class="validate[required] text-input">
                                <?php
                                if ($row['prociudad'] != '') { // SI EL CAMPO CIUDAD ES DISTINTO A VACIO
                                    $row2 = $db->query("SELECT * FROM ciudades WHERE ciudepto = '" . $row['prodepartamento'] . "' AND ciuid = '" . $row['prociudad'] . "'")->fetch(PDO::FETCH_ASSOC); //SE BUSCA ESPECIFICAMENTE LA CIUDAD
                                    echo '<option value=' . $row['prociudad'] . '>' . $row2['ciunombre'] . '</option>'; // SE MUESTARN LOS RESULTADOS TANTO NOMBRE DE LA CIUDAD
                                } // SI ES DEPARTAMENTO DEL PROSPECTO ESTA VACIO
                                echo '<option value="">SELECCIONE</option>';
                                $qry = $db->query("SELECT * FROM ciudades WHERE ciudepto = '" . $row['prodepartamento'] . "' AND ciuid <> '" . $row['prociudad'] . "' ORDER BY ciunombre");
                                while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
                                    echo '<option value=' . $row2['depid'] . '>' . $row2['depnombre'] . '</option>'; // APARECEN LAS CIUDADES PARA QUE EL PROSPECTO ESCOJA LA QUE CORRESPONDE
                                }
                                ?>
                            </select>
                        </p>
                        <p>
                            <label>Barrio: </label>
                            <input type="text" name="barrio"/>
                        </p>
                        <p>
                            <label>Direccion: </label>
                            <input type="text" name="direccion" style="width:76%" class="direccion"/>
                        </p>
                        <p>
                            <label>Indicaciones: </label>
                            <input type="text" name="indicaciones" style="width:76%"/> <!-- CAMPO INDICACIONES -->
                        </p>
                        <p>
                            <label>E-mail 1: </label>
                            <input type="text" name="email1" class="email"/>
                        </p>
                        <p>
                            <label>E-mail 2: </label>
                            <input type="text" name="email2" class="email"/>
                        </p>
                        <p>
                            <label>Telefono 1: </label>
                            <input type="text" name="telefono1"
                                   class="telefono validate[required]  custom[phone]] text-input"
                                   title="Numero Telefono 1"/>
                            <label>Telefono 2: </label>
                            <input type="text" name="telefono2" class="telefono" title="Numero Telefono 2"/>
                        </p>
                        <p>
                            <label>Celular 1: </label>
                            <input type="text" name="celular1"
                                   class="telefono validate[required]  custom[phone]] text-input"
                                   title="Numero Celular 1"/>
                            <label>Celular 2: </label>
                            <input type="text" name="celular2" class="telefono" title="Numero Celular 2"/>
                        </p>
                        <p>
                            <label>Contacto: </label>
                            <input type="text" name="contacto" style="width:65%;"/>
                        </p>
                        <p>
                            <label>Contacto Alternativo: </label>
                            <input type="text" name="contactoalternativo" style="width:65%;"/>
                        </p>
                    </fieldset>
                    <br>
                    </p>
                    <p class="boton">
                        <button type="submit" class="btn btn-primary" name="insertar" value="insertar">Insertar y
                            continuar
                            ingresando
                        </button>  <!-- BOTON INSERTAR Y CONTINUAR INSERTANDO -->
                        <button type="submit" class="btn btn-primary" name="insertarges" value="insertarges">Insertar y
                            gestionar llamada
                        </button>  <!-- BOTON INSERTAR Y CONTINUAR INSERTANDO -->
                    </p>
                </form>
            </div>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php'); ?> <!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
</section>
<?php
if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>';
elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>';
?>
<div id="modal" style="display:none"></div>
<div id="dialog2" style="display:none"></div>
</body>
</html>