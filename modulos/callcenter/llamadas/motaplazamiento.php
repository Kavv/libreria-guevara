<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

$id_capacitacion = $_GET['capaid'];
$fecha1 = $_GET['fecha1'];
$fecha2 = $_GET['fecha2'];
$consultar = $_GET['consultar'];
$call = $_GET['call'];
$asesor = $_GET['asesor'];
$departamento = $_GET['departamento'];
$estado = $_GET['estado'];
$fecha3 = $_GET['fecha3'];
$fecha4 = $_GET['fecha4'];
$idcapaci = $_GET['idcapaci'];
$idpros = $_GET['idpros'];
$jeferela = $_GET['jeferela'];

$filtro = "jeferela=" . $jeferela . "&idpros=" . $idpros . "&aplazar=1&capaid=" . $id_capacitacion . "&idcapaci=" . $idcapaci . "&call=" . $call . "&fecha1=" . $fecha1 . "&fecha2=" . $fecha2 . "&asesor=" . $asesor . "&departamento=" . $departamento . "&estado=" . $estado . "&consultar=" . $consultar . "&fecha3=" . $fecha3 . "&fecha4=" . $fecha4;
$filtro1 = "jeferela=" . $jeferela . "&idpros=" . $idpros . "&idcapaci=" . $idcapaci . "&call=" . $call . "&fecha1=" . $fecha1 . "&fecha2=" . $fecha2 . "&asesor=" . $asesor . "&departamento=" . $departamento . "&estado=" . $estado . "&consultar=" . $consultar . "&fecha3=" . $fecha3 . "&fecha4=" . $fecha4;


$qry1 = $db->query("SELECT * FROM capacitaciones WHERE capaid = '" . $id_capacitacion . "';");
$row1 = $qry1->fetch(PDO::FETCH_ASSOC);


$qrypermisos = $db->query("SELECT * FROM usuarios WHERE usuid = '" . $_SESSION['id'] . "';");
$rowpermisos = $qrypermisos->fetch(PDO::FETCH_ASSOC);

?>
<!doctype html>
<html>
<head>

    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>
    <script type="text/javascript">
        $(document).ready(function () {

            $('#fecha2').datepicker({
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true,
                minDate: '0D',
            });

            $('.historico').click(function () { // ACCION PARA CARASTERISTICO
                newHref = $(this).attr('data-rel');
                $('#dialog2').load(newHref).dialog({
                    modal: true,
                    width: 800
                });
            });
        });
    </script>
</head>
<body>
<?php require($r . 'incluir/src/login.php') ?>
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <?php require($r . 'incluir/src/menu.php') ?>
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a href="#">Call Center</a>
            <div class="mapa_div"></div>
            <a class="current">Motivo de aplazamiento de la capacitacion</a>
        </article>
        <article id="contenido">
            <form id="form" name="form" action="liscapacitacion.php?<?php echo $filtro; ?>" method="post">
                <fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
                    <legend class="ui-widget ui-widget-header ui-corner-all">Detalle de de la capacitacion <img
                                src="<?php echo $r ?>imagenes/iconos/comments.png"
                                data-rel="notas.php?hisproprospecto=<?php echo  $row1['capaprospectoid'] . '&hisproid=' .  $row1['capaprospectoid'] ?>"
                                class="historico" title="Visualizacion Historico"/></legend>

                    <fieldset class="ui-widget ui-widget-content ui-corner-all ">
                        <legend>Datos de la capacitacion</legend>
                        <p>
                            <label style="width:140px;" for="prospecto"> Identificacion del prospecto: </label>
                            <input type="text" name="prospecto" id="prospecto"
                                   value="<?php echo $row1['capaprospectoid'] ?>" class="id validate[required]"/>
                        </p>
                        <p>
                            <label style="width:140px;" for="capaid"> Identificacion de la capacitacion: </label>
                            <input type="text" name="capaid" id="capaid" value="<?php echo $row1['capaid'] ?>"
                                   class="id validate[required]"/>
                        </p>
                        <p>
                            <?php
                            $qrynombre = $db->query("SELECT * FROM usuarios WHERE usuid = '" . $row1['capausuario'] . "';");
                            $rownombre = $qrynombre->fetch(PDO::FETCH_ASSOC);
                            $nombre_usu = $rownombre['usunombre'];
                            ?>
                            <label style="width:140px;" for="agendadapor"> Capacitacion agendada por: </label>
                            <input style="width:300px" type="text" name="agendadapor" id="agendadapor"
                                   value="<?php echo $nombre_usu ?>"/>
                        </p>
                        <p>
                            <label style="width:140px;" for="diaagenda"> Capacitacion gestionada el dia: </label>
                            <input style="width:300px" type="text" name="diaagenda" id="diaagenda"
                                   value="<?php echo $row1['capafechausuario'] ?>"/>
                        </p>
                        <p>
                            <label style="width:140px;" for="capacitacion"> Capacitacion: </label>
                            <select name="capacitacion" id="capacitacion" class="validate[required] text-input">
                                <?php
                                if ($row1['capaidlistado'] != '') {
                                    $row2 = $db->query("SELECT * FROM liscapacitaciones WHERE liscapaid = '" . $row1['capaidlistado'] . "'")->fetch(PDO::FETCH_ASSOC);
                                    echo '<option class="validate[required]" value="' . $row2["liscapaid"] . '">' . $row2['liscapadescripcion'] . '</option>';
                                }
                                ?>
                            </select>
                        </p>
                        <p>
                            <label style="width:140px;" for="asesor">Asesor:</label>
                            <select name="asesor">
                                <?php
                                if ($row1['capacapacitador'] != '') {
                                    $row2 = $db->query("SELECT * FROM usuarios WHERE usuasesor = '1'  AND usuid = '" . $row1['capacapacitador'] . "' ORDER BY usunombre")->fetch(PDO::FETCH_ASSOC);
                                    echo '<option class="validate[required]" value="' . $row2["usuid"] . '">' . $row2['usunombre'] . '</option>';
                                }
                                ?>
                            </select>
                        </p>
                        <p>
                            <label style="width:140px;" for="comentarios"> Comentarios: </label>
                            <textarea name="comentarios" class="form-control" id="comentarios" rows="4"
                                      cols="65"><?php echo $row1['capacomentarios'] ?></textarea>
                        </p>
                        <?php
                        $qryhofe = $db->query("SELECT time(capafecha) as horaCapaci , date(capafecha) as fechaCapaci FROM capacitaciones WHERE  capaid = '" . $id_capacitacion . "';");
                        $rowhofe = $qryhofe->fetch(PDO::FETCH_ASSOC);
                        $horaCapaci = $rowhofe['horaCapaci'];
                        $fechaCapaci = $rowhofe['fechaCapaci'];
                        ?>
                        <p>
                            <label style="width:140px;" for="fecha2"> Fecha de capacitacion: </label>
                            <input type="text" name="fecha2" value="<?php echo $fechaCapaci ?>" id="fecha2"
                                   class="validate[required] text-input fecha"/>
                            <input type="hidden" name="fecha2_actual" value="<?php echo $fechaCapaci ?>"/>
                        </p>
                        <p>
                            <label style="width:140px;" for="hora2"> Hora de capacitacion: </label>
                            <input type="time" name="hora2" id="hora2" value="<?php echo $horaCapaci ?>" max="23:59:59"
                                   min="00:00:01" class="validate[required]" step="1">
                        </p>
                        <p>
                            <label style="width:140px;" title="Numero posible de asistentes" for="asistentes">N.P de
                                Asistentes: </label>
                            <input style="width:60px;" type="number" name="asistentes"
                                   value="<?php echo $row1['capanumasistentes'] ?>" class="id validate[required]"
                                   id="asistentes" min="1" max="50">
                        </p>
                    </fieldset>

                    <label for="motaplazar">Motivo de aplazamiento:</label>
                    <select name="motaplazar" class="validate[required]">
                        <option value="">SELECCIONE</option>
                        <?php
                        $qrycal = $db->query("SELECT * FROM aplarepro WHERE aplareproestado = 1 ORDER BY aplareprodescripcion");
                        while ($rowcal = $qrycal->fetch(PDO::FETCH_ASSOC))
                            echo '<option value=' . $rowcal['aplareproid'] . '>' . $rowcal['aplareprodescripcion'] . '</option>';
                        ?>
                    </select>
                    </br></br>
                    </br>
                    <p class="boton">
                        <button type="submit" class="btn btn-primary" name="btncancelar" value="btncancelar">Aplazar
                        </button>
                        <button type="button" class="btn btn-outline-primary btnatras"
                                onClick="carga(); location.href='liscapacitacion.php'">
                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-left-square-fill"
                                 fill="currentColor"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                      d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm9.5 8.5a.5.5 0 0 0 0-1H5.707l2.147-2.146a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708-.708L5.707 8.5H11.5z"/>
                            </svg>
                            Atras
                        </button> <!-- BOTON ATRAS -->
                    </p>

                </fieldset>
            </form>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php'); ?>
</section>
<div id="dialog2" title="Historico de Llamadas" style="display:none"></div>
</body>
</html>