<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

?>
<!doctype html>
<html>
<head>
    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>
</head>
<body>
<?php require($r . 'incluir/src/login.php') ?>
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <?php require($r . 'incluir/src/menu.php') ?>
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a href="#">Call Center</a>
            <div class="mapa_div"></div>
            <a class="current">Nuevo grupo</a>
        </article>
        <article id="contenido">
            <form id="form" name="form" action="creargrupo1.php" method="POST">
                <fieldset id="field" class="ui-widget ui-widget-content ui-corner-all col-md-6">
                    <legend class="ui-widget ui-widget-header ui-corner-all">Nuevo Grupo Form 1</legend>
                    <p>
                    <p>Debe tener en cuenta que el nombre debe ser unico, no se puede repetir.</p>
                    <br>
                    <label for="nombregrupo">Nombre del grupo:</label>
                    <input type="text" style="width:250px" name="nombregrupo"
                           class="nombre validate[[required]] text-input" title="Digite el nombre del grupo"/>
                    </p>
                    <br>
                </fieldset>
                <p class="boton">
                    <button type="submit" class="btn btn-primary" name="siguiente" value="siguiente">Siguiente</button>
                </p>
            </form>
            </fieldset>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php'); ?>
</section>
<?php
if (isset($_GET['error'])) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['error'] . '</div>';
?>
<div id="modal" style="display:none"></div>
<div id="dialog2" style="display:none"></div>
</body>
</html>