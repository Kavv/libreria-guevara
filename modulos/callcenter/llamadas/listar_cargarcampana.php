<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
require($r . 'incluir/funciones.php');

$relacionista = $_POST['relacionista'];
$array = array_envia($relacionista);
if (empty($relacionista)) {
    $error = $relacionista . " Es Necesario que seleccione las relacionistas que trabajaran esta campaña, </br> Por favor cargue el archivo nuevamente seleccionando las relacionistas pertinentes!";
    header('Location:cargarcampa.php?error=' . $error . '');
    die();
}
?>
<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>

    <link rel="stylesheet" type="text/css"
          href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css"/>
    <script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#tabla').dataTable({
                'bJQueryUI': true,
                'bStateSave': true,
                'sPaginationType': 'full_numbers',
                'oLanguage': {
                    'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
                },
                'bSort': false,
            });
            $('.btnatras').button({
                icons: {
                    primary: 'ui-icon ui-icon-arrowthick-1-w'
                }
            });
        });
    </script>
</head>

<body>
<?php require($r . 'incluir/src/login.php') ?>
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <?php require($r . 'incluir/src/menu.php') ?>
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a href="#">Call Center</a>
            <div class="mapa_div"></div>
            <a class="current">Cargue de Campaña</a>
        </article>
        <?php
        $nombre = $_POST["nombrecampana"];
        $nom = strtoupper($nombre);
        ?>
        <article id="contenido">
            <h2>Cargue de Campaña <?php echo $nom; ?></h2>
            <?php

            $qrycampana = $db->query("INSERT INTO campana (campanombre, campaestado) VALUES ('" . $nom . "', 'ACTIVA');");
            $rowcampana = $qrycampana->fetch(PDO::FETCH_ASSOC);

            if ($qrycampana) {
                $qrycam = $db->query("SELECT * FROM campana ORDER BY campaid DESC LIMIT 1;");
                $rowcam = $qrycam->fetch(PDO::FETCH_ASSOC);
                $campana = $rowcam['campaid'];


                foreach ($relacionista as $valor) {
                    if ($valor == -1) {
                        continue;
                    } else {
                        $qrycampana = $db->query("INSERT INTO campanarelacionista (idrelacionista, idcampana) VALUES ('" . $valor . "', '" . $rowcam['campaid'] . "');");
                    }
                }
            }

            ?>
            <br>
            <p class="boton">
                <button type="button" class="btn btn-outline-primary btnatras"
                        onClick="carga(); location.href='cargarcampa.php'">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-left-square-fill"
                         fill="currentColor"
                         xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                              d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm9.5 8.5a.5.5 0 0 0 0-1H5.707l2.147-2.146a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708-.708L5.707 8.5H11.5z"/>
                    </svg>
                    Atras
                </button> <!-- BOTON PARA VOLVER A CONSULTAR -->
            </p>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php');
    ?>

</section>

</body>
</html>