<?php
$r = '../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');

$id_capacitacion = $_GET['capaid'];
$fecha1 = $_GET['fecha1'];
$fecha2 = $_GET['fecha2'];
$consultar = $_GET['consultar'];
$call = $_GET['call'];
$asesor = $_GET['asesor'];
$departamento = $_GET['departamento'];
$estado = $_GET['estado'];
$fecha3 = $_GET['fecha3'];
$fecha4 = $_GET['fecha4'];
$idcapaci = $_GET['idcapaci'];
$idpros = $_GET['idpros'];
$jeferela = $_GET['jeferela'];

$filtro = "jeferela=".$jeferela."&idpros=".$idpros."&idcapaci=".$idcapaci."&reprogramar=1&capaid=".$id_capacitacion."&call=".$call."&fecha1=".$fecha1."&fecha2=".$fecha2."&asesor=".$asesor."&departamento=".$departamento."&estado=".$estado."&consultar=".$consultar."&fecha3=".$fecha3."&fecha4=".$fecha4;

$filtro1 = "jeferela=".$jeferela."&idpros=".$idpros."&idcapaci=".$idcapaci."&call=".$call."&fecha1=".$fecha1."&fecha2=".$fecha2."&asesor=".$asesor."&departamento=".$departamento."&estado=".$estado."&consultar=".$consultar."&fecha3=".$fecha3."&fecha4=".$fecha4;


$qry1 = $db->query("SELECT * FROM capacitaciones WHERE capaid = '".$id_capacitacion."';");
$row1 = $qry1->fetch(PDO::FETCH_ASSOC);


$qrypermisos = $db->query("SELECT * FROM usuarios WHERE usuid = ".$_SESSION['id'].";");
$rowpermisos = $qrypermisos->fetch(PDO::FETCH_ASSOC);

?>
<!doctype html>
<html>
<head>
<title>. : : S o p h y a : : .</title>
<meta charset="utf-8" />
<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/css/normalize.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/jquery/css/smoothness/jquery-ui-1.10.3.custom.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/css/css.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/css/menu.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/validation/css/validationEngine.jquery.css" />
<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/js/jquery-ui-1.10.3.custom.js"></script>
<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>
<script type="text/javascript" src="<?php echo $r ?>incluir/validation/js/languages/jquery.validationEngine-es.js"></script>
<script type="text/javascript" src="<?php echo $r ?>incluir/validation/js/jquery.validationEngine.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#form').validationEngine({
		showOneMessage:true,
		onValidationComplete: function(form, status){
			if(status == true) {
				carga();
				return true;
			}
		}
	});
	$('#fecha2').datepicker({
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		minDate: '0D',
	});
	$('.historico').click(function(){ // ACCION PARA CARASTERISTICO
		newHref = $(this).attr('data-rel');
		$('#dialog2').load(newHref).dialog({ modal:true, width: 800 }); 
    });
	
	$('.btnmodificar_correo').button({ icons: { primary: 'ui-icon ui-icon-pencil' }});
	$('.btnmodificar').button({ icons: { primary: 'ui-icon ui-icon-pencil' }});
	$('.btninsertar').button({ icons: { primary: 'ui-icon ui-icon-check' }});
});
</script>
<style type="text/css">
#form form{ width:890px }
#form fieldset{ padding:10px; display:block; width:890px; margin:20px auto }
#form legend{ font-weight: bold; margin-left:5px; padding:5px }
#form label{ display:inline-block; text-align:right;  margin:0.3em 2% 0 0 }
#form p{ margin:5px 0 }
</style>
</head>
<body>
<?php require($r.'incluir/src/login.php') ?>
<section id="principal">
<?php require($r.'incluir/src/cabeza.php') ?>
<?php require($r.'incluir/src/menu.php') ?>
<article id="cuerpo">
<article class="mapa">
<a href="#">Principal</a><div class="mapa_div"></div><a href="#">Call Center</a><div class="mapa_div"></div><a class="current">Motivo de reprogramacion de la capacitacion</a>
</article>
<article id="contenido">
<form id="form" name="form" action="liscapacitacion.php?<?php echo $filtro; ?>" method="post">
<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
<legend class="ui-widget ui-widget-header ui-corner-all">Detalle de de la capacitacion <img src="<?php echo $r ?>imagenes/iconos/comments.png" data-rel="notas.php?hisproprospecto=<?php echo $prospecto.'&hisproid='.$prospecto ?>" class="historico" title="Visualizacion Historico" /></legend>
	
	<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
	<legend>Datos de la capacitacion</legend>
	<p>
	<label style="width:140px;" for="prospecto"> Identificacion del prospecto: </label>
	<input type="text" name="prospecto" id="prospecto" value="<?php echo $row1['capaprospectoid'] ?>" class="id validate[required]"  />
	</p>
	<p>
	<label style="width:140px;" for="capaid"> Identificacion de la capacitacion: </label>
	<input type="text" name="capaid" id="capaid" value="<?php echo $row1['capaid'] ?>" class="id validate[required]"  />
	</p>
	<p>
	<?php 
	$qrynombre = $db->query("SELECT * FROM usuarios WHERE usuid = '".$row1['capausuario']."';");
	$rownombre = $qrynombre->fetch(PDO::FETCH_ASSOC);
	$nombre_usu = $rownombre['usunombre'];
	?>
	<label style="width:140px;" for="agendadapor"> Capacitacion agendada por: </label>
	<input style="width:300px" type="text" name="agendadapor" id="agendadapor" value="<?php echo $nombre_usu  ?>"  />
	</p>
	<p>
	<label style="width:140px;" for="diaagenda"> Capacitacion gestionada el dia: </label>
	<input style="width:300px" type="text" name="diaagenda" id="diaagenda" value="<?php echo $row1['capafechausuario']  ?>"  />
	</p>
	<p>
	<label style="width:140px;" for="capacitacion"> Capacitacion: </label>
	<select name="capacitacion" id="capacitacion"  class="validate[required] text-input" >
	<?php
	if($row1['capaidlistado'] != ''){
		$row2 = $db->query("SELECT * FROM liscapacitaciones WHERE liscapaid = '".$row1['capaidlistado']."'")->fetch(PDO::FETCH_ASSOC);
		echo '<option class="validate[required]" value="'.$row2["liscapaid"].'">'.$row2['liscapadescripcion'].'</option>';
	}
	?>
	</select>
	</p>
	<p>
	<label style="width:140px;" for="asesor">Asesor:</label>
	<select name="asesor">
	<?php
		if($row1['capacapacitador'] != ''){
			$row2 = $db->query("SELECT * FROM usuarios WHERE usuasesor = '1'  AND usuid = '".$row1['capacapacitador']."' ORDER BY usunombre")->fetch(PDO::FETCH_ASSOC);
			echo '<option class="validate[required]" value="'.$row2["usuid"].'">'.$row2['usunombre'].'</option>';
		}
	?>
	</select>
	</p>
	<p>
	<label style="width:140px;" for="comentarios"> Comentarios: </label>
	<textarea name="comentarios" id="comentarios"  rows="4" cols="65"  ><?php echo $row1['capacomentarios'] ?></textarea>
	</p>
	<?php
	$qryhofe = $db->query("SELECT time(capafecha) as horaCapaci , date(capafecha) as fechaCapaci FROM capacitaciones WHERE  capaid = '".$id_capacitacion."';");
	$rowhofe = $qryhofe->fetch(PDO::FETCH_ASSOC);
	$horaCapaci = $rowhofe['horaCapaci'];
	$fechaCapaci = $rowhofe['fechaCapaci'];
	?>
	<p>
	<label style="width:140px;" for="diaagenda"> Capacitacion agendada el dia: </label>
	<input style="width:300px" type="text" name="diaagenda" id="diaagenda" value="<?php echo $row1['capafecha']  ?>"  />
	</p>
	<p>
	<label style="width:140px;" title="Numero posible de asistentes" for="asistentes">N.P de Asistentes: </label>
	<input style="width:60px;" type="number" name="asistentes" value="<?php echo $row1['capanumasistentes'] ?>" class="id validate[required]" id="asistentes" min="1" max="50" >
	</p>
	</fieldset>
	
	<p>
	<label style="width:150px;" for="motreprogramar">Motivo de reprogramacion:</label>
	<select name="motreprogramar" class="validate[required]">
	<option value="">SELECCIONE </option>
	<?php
	$qrycal = $db->query("SELECT * FROM aplarepro WHERE aplareproestado = 1 ORDER BY aplareprodescripcion");
	while($rowcal = $qrycal->fetch(PDO::FETCH_ASSOC))
		echo '<option value='.$rowcal['aplareproid'].'>'.$rowcal['aplareprodescripcion'].'</option>';
	?>
	</select>
	</p>
	<p>
	<label style="width:150px;" for="fecha2"> Fecha de capacitacion Nueva: </label>
	<input type="text" name="fecha2"  id="fecha2" class="validate[required] text-input fecha"  />
	</p>
	<p>
	<label style="width:150px;" for="hora2"> Hora de capacitacion Nueva: </label>
	<input type="time" name="hora2" id="hora2" max="23:59:59" min="00:00:01" class="validate[required]" step="1" >
	</p>
	</br></br>
	</br>
	<p class="boton">
	<button type="submit" class="btninsertar" name="btncancelar" value="btncancelar">Reprogramar</button>
	<button type="button" class="btnatras" onClick="carga(); location.href='liscapacitacion.php?<?php echo $filtro1 ?>'">Atras</button>  <!-- BOTON ATRAS -->
	</p>

</fieldset>
</form>
</article>
</article>
<?php require($r.'incluir/src/pie.php') ;
?>
</section>
<div id="dialog2" title="Historico de Llamadas" style="display:none"></div>
</body>
</html>