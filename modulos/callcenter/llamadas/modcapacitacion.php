<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

$prospecto = isset($_GET['prospecto']) ? $_GET['prospecto'] : '';
$id_capacitacion = isset($_GET['idcapacitacion']) ? $_GET['idcapacitacion'] : '';
$call = isset($_GET['call']) ? $_GET['call'] : '';
$fecha1 = isset($_GET['fecha1']) ? $_GET['fecha1'] : '';
$fecha2 = isset($_GET['fecha2']) ? $_GET['fecha2'] : '';
$consultar = isset($_GET['consultar']) ? $_GET['consultar'] : '';
$call = isset($_GET['call']) ? $_GET['call'] : '';
$asesor = isset($_GET['asesor']) ? $_GET['asesor'] : '';
$departamento = isset($_GET['departamento']) ? $_GET['departamento'] : '';
$ciudad = isset($_GET['ciudad']) ? $_GET['ciudad'] : $_GET['ciudad'];
$estado = isset($_GET['estado']) ? $_GET['estado'] : '';
$fecha3 = isset($_GET['fecha3']) ? $_GET['fecha3'] : '';
$fecha4 = isset($_GET['fecha4']) ? $_GET['fecha4'] : '';
$uninegoci = isset($_GET['uninegoci']) ? $_GET['uninegoci'] : '';
$idcapaci = isset($_GET['idcapaci']) ? $_GET['idcapaci'] : '';
$idpros = isset($_GET['idpros']) ? $_GET['idpros'] : '';
$jeferela = isset($_GET['jeferela']) ? $_GET['jeferela'] : '';
$modalidad = isset($_GET['modalidad']) ? $_GET['modalidad'] : '';

$filtro = "jeferela=" . $jeferela . "&idpros=" . $idpros . "&call=" . $call . "&fecha1=" . $fecha1 . "&fecha2=" . $fecha2 . "&asesor=" . $asesor . "&departamento=" . $departamento . "&ciudad=" . $ciudad . "&estado=" . $estado . "&fecha3=" . $fecha3 . "&fecha4=" . $fecha4 . "&uninegoci=" . $uninegoci . "&idcapaci=" . $idcapaci . "&consultar=" . $consultar . "&modalidad" . $modalidad;


$qry1 = $db->query("SELECT * FROM capacitaciones WHERE capaprospectoid = '" . $prospecto . "' AND capaid = '" . $id_capacitacion . "';");
$row1 = $qry1->fetch(PDO::FETCH_ASSOC);

if ($row1['capaestado'] == '2') {
    $error = "No se puede modificar la capacitacion por que se encuentra cancelada";
    header('Location:liscapacitacion.php?error=' . $error . '&' . $filtro . '');
}

$qrypermisos = $db->query("SELECT * FROM usuarios WHERE usuid = '" . $_SESSION['id'] . "';");
$rowpermisos = $qrypermisos->fetch(PDO::FETCH_ASSOC);

?>
<!doctype html>
<html>
<head>
    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css"/>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {

            $('#agendamientopor').selectpicker();
            $('#coach').selectpicker();
            $('#observador').selectpicker();

            <?php
            if ($rowpermisos['usuid'] == '1110451657' or $rowpermisos['usuperfil'] == 30){
            ?>


            $('#fecha2').datepicker({
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true,
            });
            <?php } else { ?>
            $('#fecha2').datepicker({
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true,
                minDate: '0D',
            });
            <?php } ?>
            $('.historico').click(function () { // ACCION PARA CARASTERISTICO
                newHref = $(this).attr('data-rel');
                $('#dialog2').load(newHref).dialog({modal: true, width: 800});
            });

        });
    </script>
</head>
<body>
<?php require($r . 'incluir/src/login.php') ?>
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <?php require($r . 'incluir/src/menu.php') ?>
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a href="#">Call Center</a>
            <div class="mapa_div"></div>
            <a class="current">Modificar Capacitacion</a>
        </article>
        <article id="contenido">
            <form id="form" name="form" action="liscapacitacion.php?<?php echo $filtro; ?>" method="post">
                <fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
                    <legend class="ui-widget ui-widget-header ui-corner-all">Modificar Capacitacion <img
                                src="<?php echo $r ?>imagenes/iconos/comments.png"
                                data-rel="notas.php?hisproprospecto=<?php echo $prospecto . '&hisproid=' . $prospecto ?>"
                                class="historico" title="Visualizacion Historico"/></legend>

                    <p>
                        <label style="width:140px;" for="prospecto"> Identificacion del prospecto: </label>
                        <input type="text" name="prospecto" id="prospecto"
                               value="<?php echo $row1['capaprospectoid'] ?>" class="id validate[required]" readonly/>
                    </p>
                    <p>
                        <label style="width:140px;" for="capacitaid"> Identificacion de la capacitacion: </label>
                        <input type="text" name="capacitaid" id="capacitaid" value="<?php echo $row1['capaid'] ?>"
                               class="id validate[required]" readonly/>
                    </p>
                    <?php
                    $qrynombre = $db->query("SELECT * FROM usuarios WHERE usuid = '" . $row1['capausuario'] . "';");
                    $rownombre = $qrynombre->fetch(PDO::FETCH_ASSOC);
                    $nombre_usu = $rownombre['usunombre'];

                    if ($rowpermisos['usuperfil'] == 30 or $rowpermisos['usuperfil'] == 99) {
                        echo "
	<p>
	<label  for='agendamientopor'>Agendado Por:</label>
	<select name='agendamientopor' id='agendamientopor' class='form-control' data-live-search='true'  >
	";
                        $qry = $db->query("SELECT * FROM usuarios WHERE usucallcenter = '1' and usuid = '" . $row1['capausuario'] . "' ORDER BY usunombre asc");
                        while ($row = $qry->fetch(PDO::FETCH_ASSOC))
                            echo '<option value=' . $row['usuid'] . '>' . $row['usunombre'] . '</option>';
                        $qry = $db->query("SELECT * FROM usuarios WHERE usucallcenter = '1' and usuid <> '" . $row1['capausuario'] . "' ORDER BY usunombre asc");
                        while ($row = $qry->fetch(PDO::FETCH_ASSOC))
                            echo '<option value=' . $row['usuid'] . '>' . $row['usunombre'] . '</option>';
                        echo "
	</select>
	</p>
	";
                    } elseif ($rowpermisos['usuperfil'] <> 30) {
                        echo "
	<p>
	<label style='width:140px;' for='agendamientopor'>Agendado Por:</label>
	<select name='agendamientopor' id='agendamientopor'>
	";
                        $qry = $db->query("SELECT * FROM usuarios WHERE usucallcenter = '1' and usuid = '" . $row1['capausuario'] . "' ORDER BY usunombre asc");
                        while ($row = $qry->fetch(PDO::FETCH_ASSOC))
                            echo '<option value=' . $row['usuid'] . '>' . $row['usunombre'] . '</option>';
                        echo "
	</select>
	</p>
	";
                    }
                    ?>
                    <?php
                    if ($rowpermisos['usuid'] == '1110451657' or $rowpermisos['usuperfil'] == 30){
                        ?>
                        <p>
                            <label for="diaagenda"> Capacitacion gestionada el dia: </label>
                            <input type="text" name="diaagenda" id="diaagenda"
                                   value="<?php echo $row1['capafechausuario'] ?>"/>
                        </p>
                        <?php
                    } else {
                    ?>
                    <p>
                        <label for="diaagenda"> Capacitacion gestionada el dia: </label>
                        <input type="text" name="diaagenda" id="diaagenda"
                               value="<?php echo $row1['capafechausuario'] ?>" readonly/>
                    </p>
                    <p>
                        <?php } ?>
                        <label for="capacitacion"> Capacitacion: </label>
                        <select name="capacitacion" id="capacitacion" class="validate[required] text-input">
                            <?php
                            if ($row1['capaidlistado'] != '') {
                                $row2 = $db->query("SELECT * FROM liscapacitaciones WHERE liscapaid = '" . $row1['capaidlistado'] . "'")->fetch(PDO::FETCH_ASSOC);
                                echo '<option class="validate[required]" value="' . $row2["liscapaid"] . '">' . $row2['liscapadescripcion'] . '</option>';
                            }
                            echo '<option value="">SELECCIONE</option>';
                            $qry = $db->query("SELECT * FROM liscapacitaciones WHERE liscapaid <> '" . $row1['capaidlistado'] . "' ");
                            while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
                                echo '<option class="validate[required]" value="' . $row2["liscapaid"] . '">' . $row2['liscapadescripcion'] . '</option>';
                            }
                            ?>
                        </select>
                    </p>

                    <p>
                        <label for="uninegocio"> Unidad de Negocio: </label>
                        <select name="uninegocio" id="uninegocio" class="validate[required] text-input">
                            <?php
                            if ($row1['capauninegocio'] != '') {
                                $rowunio = $db->query("SELECT * FROM uninegocio WHERE uniestado = 1 AND uniid = '" . $row1['capauninegocio'] . "'")->fetch(PDO::FETCH_ASSOC);
                                echo '<option class="validate[required]" value="' . $rowunio["uniid"] . '">' . $rowunio['unidescripcion'] . '</option>';
                            }
                            echo '<option value="">SELECCIONE</option>';
                            $qry = $db->query("SELECT * FROM uninegocio WHERE uniestado = 1 and uniid <> '" . $row1['capauninegocio'] . "' ORDER BY unidescripcion");
                            while ($row4 = $qry->fetch(PDO::FETCH_ASSOC)) {
                                echo '<option value="' . $row4["uniid"] . '">' . $row4['unidescripcion'] . '</option>';
                            }
                            ?>
                        </select>
                    </p>

                    <p>
                        <label for="asesor">Asesor:</label>
                        <select name="asesor" id="asesor" class="form-control" data-live-search="true">
                            <?php
                            if ($row1['capacapacitador'] != '') {
                                $row2 = $db->query("SELECT * FROM usuarios WHERE usuasesor = '1' AND usuperfil <> 56  AND usuid = '" . $row1['capacapacitador'] . "' ORDER BY usunombre")->fetch(PDO::FETCH_ASSOC);
                                echo '<option class="validate[required]" value="' . $row2["usuid"] . '">' . $row2['usunombre'] . '</option>';
                            }
                            echo '<option value="">SELECCIONE</option>';
                            $qry = $db->query("SELECT * FROM usuarios WHERE usuasesor = '1' AND usuperfil <> 56 AND usuid <> '" . $row1['capacapacitador'] . "' ORDER BY usunombre ");
                            while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
                                echo '<option class="validate[required]" value="' . $row2["usuid"] . '">' . $row2['usunombre'] . '</option>';
                            }
                            ?>
                        </select>
                    </p>

                    <p>
                        <label for="coach">Coach:</label>
                        <select name="coach" id="coach" class="form-control" data-live-search="true">
                            <?php
                            if ($row1['capacoach'] != '') {
                                $row2 = $db->query("SELECT * FROM usuarios WHERE usuasesor = '1' AND usuperfil <> 56  AND usuid = '" . $row1['capacoach'] . "' ORDER BY usunombre")->fetch(PDO::FETCH_ASSOC);
                                echo '<option class="validate[required]" value="' . $row2["usuid"] . '">' . $row2['usunombre'] . '</option>';
                            }
                            echo '<option value="">SELECCIONE</option>';
                            $qry = $db->query("SELECT * FROM usuarios WHERE usuasesor = '1' AND usuperfil <> 56 AND usuid <> '" . $row1['capacoach'] . "' ORDER BY usunombre ");
                            while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
                                echo '<option class="validate[required]" value="' . $row2["usuid"] . '">' . $row2['usunombre'] . '</option>';
                            }
                            ?>
                        </select>
                    </p>

                    <p>
                        <label for="observador">Observador:</label>
                        <select name="observador" class="form-control" data-live-search="true" id="observador">
                            <?php
                            if ($row1['capaobservador'] != '') {
                                $row2 = $db->query("SELECT * FROM usuarios WHERE usuasesor = '1' AND usuperfil <> 56  AND usuid = '" . $row1['capaobservador'] . "' ORDER BY usunombre")->fetch(PDO::FETCH_ASSOC);
                                echo '<option class="validate[required]" value="' . $row2["usuid"] . '">' . $row2['usunombre'] . '</option>';
                            }
                            echo '<option value="">SELECCIONE</option>';
                            $qry = $db->query("SELECT * FROM usuarios WHERE usuasesor = '1' AND usuperfil <> 56 AND usuid <> '" . $row1['capaobservador'] . "' ORDER BY usunombre ");
                            while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
                                echo '<option class="validate[required]" value="' . $row2["usuid"] . '">' . $row2['usunombre'] . '</option>';
                            }
                            ?>
                        </select>
                    </p>


                    <p>
                        <label style="width:140px;" for="comentarios"> Comentarios: </label>
                        <textarea class="form-control" name="comentarios" id="comentarios" rows="4"
                                  cols="65"><?php echo $row1['capacomentarios'] ?></textarea>
                    </p>
                    <?php
                    $qryhofe = $db->query("SELECT time(capafecha) as horaCapaci , date(capafecha) as fechaCapaci FROM capacitaciones WHERE capaprospectoid = '" . $prospecto . "' AND capaid = '" . $id_capacitacion . "';");
                    $rowhofe = $qryhofe->fetch(PDO::FETCH_ASSOC);
                    $horaCapaci = $rowhofe['horaCapaci'];
                    $fechaCapaci = $rowhofe['fechaCapaci'];
                    ?>
                    <p>
                        <label style="width:140px;" for="fecha2"> Fecha de capacitacion: </label>
                        <input type="text" name="fecha2" value="<?php echo $fechaCapaci ?>" id="fecha2"
                               class="validate[required] text-input fecha"/>
                        <input type="hidden" name="fecha2_actual" value="<?php echo $fechaCapaci ?>">
                    </p>
                    <p>
                        <label style="width:140px;" for="hora2"> Hora de capacitacion: </label>
                        <input type="time" name="hora2" id="hora2" value="<?php echo $horaCapaci ?>" max="23:59:59"
                               min="00:00:01" class="validate[required]" step="1">
                    </p>
                    <p>
                        <label style="width:140px;" title="Numero posible de asistentes" for="asistentes">N.P de
                            Asistentes: </label>
                        <input style="width:60px;" type="number" name="asistentes"
                               value="<?php echo $row1['capanumasistentes'] ?>" class="id validate[required]"
                               id="asistentes" min="1" max="50">
                    </p>
                    </br>
                    <p class="boton">
                        <button type="button" class="btn btn-outline-primary btnatras"
                                onClick="carga(); location.href='liscapacitacion.php'">
                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-left-square-fill"
                                 fill="currentColor"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                      d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm9.5 8.5a.5.5 0 0 0 0-1H5.707l2.147-2.146a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708-.708L5.707 8.5H11.5z"/>
                            </svg>
                            Atras
                        </button>
                        <button type="submit" class="btn btn-primary btnmodificar" name="modificar" value="modificar">
                            Modificar
                        </button>  <!-- BOTON MODIFICAR -->
                        <?php if ($rowpermisos['usudirectorcall'] == '1') { ?>
                            <?php if ($row1['capaestado'] == '3') { // si la capacitacion no se encuentra confirmada no se permite el envio del correo al capacitador ?>
                                <button type="submit" class="btn btn-primary" name="modificar_correo"
                                        value="modificar_correo">Modificar y Enviar Correo
                                </button>  <!-- BOTON MODIFICAR y CORREO-->
                            <?php } ?>
                        <?php } ?>
                    </p>

                </fieldset>
            </form>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php');
    ?>
</section>
<div id="dialog2" title="Historico de Llamadas" style="display:none"></div>
</body>
</html>