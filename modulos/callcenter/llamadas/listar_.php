<?php
$r = '../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');

if(isset($_POST['consultar'])){
	$nombre = $_POST['nombre'];
	$telefonos = $_POST['telefonos'];
	$celulares = $_POST['celulares'];
	$email = $_POST['email'];
	$idpros = $_POST['idpros'];
} else {
	$nombre = $_GET['nombre'];
	$telefonos = $_GET['telefonos'];
	$celulares = $_GET['celulares'];
	$email = $_GET['email'];
	$idpros = $_GET['idpros'];
}
if(isset($_POST['modificar'])){
	$numid = $_POST['numid'];
	$nombrecliente = strtoupper(trim($_POST['nombrecliente']));  // LO RECIBIDO DE ESTE CAMPO LO CAMBIAMOS A MAYUSCULA
	$nombrealternativo = strtoupper(trim($_POST['nombrealternativo']));  // LO RECIBIDO DE ESTE CAMPO LO CAMBIAMOS A MAYUSCULA
	$tipocli = $_POST['tipocli'];
	$depresidencia = $_POST['depresidencia'];
	$ciuresidencia = $_POST['ciuresidencia'];
	$barrio = strtoupper(trim($_POST['barrio']));  // LO RECIBIDO DE ESTE CAMPO LO CAMBIAMOS A MAYUSCULA
	$direccion = strtoupper(trim($_POST['direccion']));  // LO RECIBIDO DE ESTE CAMPO LO CAMBIAMOS A MAYUSCULA
	$indicaciones = strtoupper(trim($_POST['indicaciones']));  // LO RECIBIDO DE ESTE CAMPO LO CAMBIAMOS A MAYUSCULA
	$email1 = strtoupper(trim($_POST['email1']));  // LO RECIBIDO DE ESTE CAMPO LO CAMBIAMOS A MAYUSCULA
	$email2 = strtoupper(trim($_POST['email2']));  // LO RECIBIDO DE ESTE CAMPO LO CAMBIAMOS A MAYUSCULA
	$telefono1 = $_POST['telefono1'];
	$telefono2 = $_POST['telefono2'];
	$celular1 = $_POST['celular1'];
	$celular2 = $_POST['celular2'];
	$contacto = strtoupper(trim($_POST['contacto']));  // LO RECIBIDO DE ESTE CAMPO LO CAMBIAMOS A MAYUSCULA
	$contactoalternativo = strtoupper(trim($_POST['contactoalternativo']));  // LO RECIBIDO DE ESTE CAMPO LO CAMBIAMOS A MAYUSCULA
	$videobeam = $_POST['videobeam'];
	
$qry = $db->query("UPDATE prospectos SET protipo = '".$tipocli."', pronombre = '".$nombrecliente."', pronombrealternativo ='".$nombrealternativo."', prodepartamento = '".$depresidencia."', prociudad = '".$ciuresidencia."', probarrio = '".$barrio."', prodireccion = '".$direccion."', proindicaciones = '".$indicaciones."', proemail1 = '".$email1."', proemail2 = '".$email2."', protelefono1 = '".$telefono1."', protelefono2 = '".$telefono2."', procelular1 = '".$celular1."', procelular2 = '".$celular2."', procontacto = '".$contacto."', procontactoalternativo = '".$contactoalternativo."', proVideoBeam = '".$videobeam."' WHERE  proid = ".$numid." ;");
	if ($qry) $mensaje = 'Se ha actualizado correctamente el prospecto';
	else $error = 'No se pudo actualizar el prospecto';

}

if($nombre =='' && $telefonos == '' && $celulares == '' && $email == '' && $idpros == ''){
	$error = 'Debe rellenar al menos un campo.';
	header('Location:consultar.php?error='.$error);
	exit();
}


$filtro = 'nombre='.$nombre.'&telefonos='.$telefonos.'&celulares='.$celulares;
$con = 'SELECT * FROM prospectos '; // INNER JOIN departamentos ON prodepartamento = depid LEFT JOIN ciudades ON (prodepartamento = ciudepto AND prociudad = ciuid)
$ord = 'ORDER BY profechaingreso DESC'; // Ordenar la Consulta
$group = ''; // Realizar Group By 

if ($idpros != '' ) $sql = "$con WHERE  proid = $idpros $group $ord";
elseif($nombre != ''  && $telefonos == '' && $celulares == '' ) $sql = "$con WHERE  (pronombre LIKE '%$nombre%' OR pronombrealternativo LIKE '%$nombre%') $group $ord";
elseif($nombre == ''  && $telefonos != '' && $celulares == '' ) $sql = "$con WHERE  (protelefono1 LIKE '%$telefonos%' OR protelefono2 LIKE '%$telefonos%') $group $ord";
elseif($nombre == ''  && $telefonos == '' && $celulares != '' ) $sql = "$con WHERE  (procelular1 LIKE '%$celulares%' OR procelular2 LIKE '%$celulares%') $group $ord";
elseif($nombre != ''  && $telefonos != '' && $celulares == '' ) $sql = "$con WHERE  (pronombre LIKE '%$nombre%' OR protelefono1 LIKE '%$telefonos%' OR protelefono2 LIKE '%$telefonos%') $group $ord";
elseif($nombre != ''  && $telefonos == '' && $celulares != '' ) $sql = "$con WHERE  (pronombre LIKE '%$nombre%' OR procelular1 LIKE '%$celulares%' OR procelular2 LIKE '%$celulares%')  $group $ord";
elseif($nombre == ''  && $telefonos != '' && $celulares != '' ) $sql = "$con WHERE  (procelular1 LIKE '%$celulares%' OR procelular2 LIKE '%$celulares%' OR protelefono1 LIKE '%$telefonos%' OR protelefono2 LIKE '%$telefonos%') $group $ord";
elseif($nombre == ''  && $telefonos == '' && $celulares == '' && $email != '') $sql = "$con WHERE  (proemail1 LIKE '%$email%' OR proemail2 LIKE '%$email%') $group $ord";


$qry = $db->query($sql);
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
	<?php 
		require($r . 'incluir/src/head.php');
		require($r . 'incluir/src/head-form.php');
	?>	

<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#tabla').dataTable({
		'bJQueryUI': true,
		'bStateSave': true,
		'sPaginationType': 'full_numbers',
		'oLanguage': {
			'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
		},
		'bSort': false,
	});
	$('.btnatras').button({ icons: { primary: 'ui-icon ui-icon-arrowthick-1-w' }});
	$('#dialog-message').dialog({
		height: 80,
		width: 'auto',
		modal: true
	});
});
</script>
</head>
<body>
<?php require($r.'incluir/src/login.php') ?>
<section id="principal">
<?php require($r.'incluir/src/cabeza.php') ?>
<?php require($r.'incluir/src/menu.php') ?>
<article id="cuerpo">
<article class="mapa">
<a href="#">Principal</a><div class="mapa_div"></div><a href="#">Call Center</a><div class="mapa_div"></div><a class="current">Consultar</a>
</article>
<article id="contenido">
<h2>Listado de Prospectos</h2>
<table id="tabla">
<thead>
<tr>
<th align="center">Nombre</th>
<th align="center" title="Nombre Alternativo">N.A</th>
<th>Departamento</th>
<th>Ciudad</th>
<th>Barrio</th>
<th>Direccion</th>
<th title="Video Beam">V.B</th>
<th title="Email Uno"></th>
<th title="Email Dos"></th>
<th title="Telefono Uno"></th>
<th title="Telefono Dos"></th>
<th title="Celular Uno"></th>
<th title="Celular Dos"></th>
<th></th>
<th></th>
</tr>
</thead>
<tbody>
<?php
while($row = $qry->fetch(PDO::FETCH_ASSOC)){
?>
<tr>
<td title="<?php echo $row['proid'] ?>"><?php echo $row['pronombre'] ?></td>
<td align="center"><img src="<?php echo $r ?>imagenes/iconos/user_red.png" title="<?php echo $row['pronombrealternativo'] ?>" /></td>
<?php 
if (!empty($row['prodepartamento'])){
$qrydepar = $db->query("SELECT * FROM departamentos WHERE depid = ".$row['prodepartamento'].";");
$rowdepar = $qrydepar->fetch(PDO::FETCH_ASSOC);
$numdepar = $qrydepar->rowCount();
	if ($numdepar == 1){
	$departamento = $rowdepar['depnombre'];
	$qryciu = $db->query("SELECT * FROM ciudades WHERE ciudepto = ".$rowdepar['depid']." AND ciuid = ".$row['prociudad']." ;");
	$rowciu = $qryciu->fetch(PDO::FETCH_ASSOC);
	$numciu = $qryciu->rowCount();
		if ($numciu == 1){
		$ciudad = $rowciu['ciunombre'];
		} else {
		$ciudad = "NONE";
		} 
	} else {
	$departamento = "NONE";
	$ciudad = "NONE";
	}
} else {
$departamento = "NONE";
$ciudad = "NONE";
}
?>
<td align="center"><?php echo $departamento ?></td>
<td align="center"><?php echo $ciudad ?></td>
<td align="center"><?php echo $row['probarrio'] ?></td>
<td align="center"><img src="<?php echo $r ?>imagenes/iconos/casa.png" title="<?php echo $row['prodireccion']." - ".$row['proindicaciones'] ?>" /></td>
<td align="center"><?php  if($row['proVideoBeam'] == 1) {echo "SI";} else {echo "NO";} ?></td>
<td align="center"><img src="<?php echo $r ?>imagenes/iconos/email.png" title="<?php echo $row['proemail1'] ?>" /></td>
<td align="center"><img src="<?php echo $r ?>imagenes/iconos/email.png" title="<?php echo $row['proemail2'] ?>" /></td>
<td align="center"><img src="<?php echo $r ?>imagenes/iconos/telefono.png" title="<?php echo $row['protelefono1'] ?>" /></td>
<td align="center"><img src="<?php echo $r ?>imagenes/iconos/telefono.png" title="<?php echo $row['protelefono2'] ?>" /></td>
<td align="center"><img src="<?php echo $r ?>imagenes/iconos/phone.png" title="<?php echo $row['procelular1'] ?>" /></td>
<td align="center"><img src="<?php echo $r ?>imagenes/iconos/phone.png" title="<?php echo $row['procelular2'] ?>" /></td>
<td align="center">
<?php
$qrypermisos = $db->query("SELECT * FROM usuarios WHERE usuid = ".$_SESSION['id'].";");
$rowpermisos = $qrypermisos->fetch(PDO::FETCH_ASSOC);
if($rowpermisos['usuperfil'] == 98 or $rowpermisos['usudirectorcall'] == 1 ){
?>
<a href="modificar.php?id=<?php echo $row['proid']."&modificar=1&".$filtro ?>" title="Modificar"><img src="<?php echo $r ?>imagenes/iconos/lapiz.png" class="grayscale" /></a>
<?php
}else echo '<img src="'.$r.'imagenes/iconos/lapiz.png" class="gray" />';
?>	
</td>
<td align="center">
<?php
if($rowpermisos['usuperfil'] == 30 || $rowpermisos['usuperfil'] == 98 || $rowpermisos['usudirectorcall'] == 1 || $rowpermisos['usuperfil'] == 25){
$qry1 = $db->query("SELECT * FROM hisprospectos WHERE hisproprospectos = ".$row['proid']." GROUP BY hisproprospectos;");
$row1 = $qry1->fetch(PDO::FETCH_ASSOC);
$num = $qry1->rowCount();
if ($num >= 1) { $hisprospectos_x = $row1['hisproprospectos']; } else { $hisprospectos_x = '';} 
?>
<a href="llamar.php?proid=<?php echo $row['proid'].'&hisproprospecto='.$hisprospectos_x.'&'.$filtro ?>" class="confirmar" title="Gestionar Llamada"><img src="<?php echo $r ?>imagenes/iconos/date_go.png" class="grayscale" /></a>
<?php
}else{ echo '<img src="'.$r.'imagenes/iconos/date_go.png" class="gray" />';}
?>
</td>
</tr>
<?php
}
?>
</tbody>
</table>
<p class="boton">
<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href='consultar.php'">Atras</button>
</p>
</article>
</article>
<?php require($r.'incluir/src/pie.php');
?>
</section>
<?php
if(isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>'.$error.'</div>';
elseif(isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>'.$mensaje.'</div>';
?>
<div id="modal" style="display:none"></div>
<div id="dialog2" style="display:none"></div>
</body>
</html>