<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

$prospecto = $_GET['prospecto'];

if (isset($_POST['validar']) && $_POST['validar']) {
    if (!empty($_POST['fecha2']) and !empty($_POST['capacitacion'])) {
        $qry1 = $db->query("SELECT * FROM liscapacitaciones WHERE liscapaid = " . $_POST['capacitacion'] . ";");
        $row1 = $qry1->fetch(PDO::FETCH_ASSOC);
        $fecha_hora_capacitacion = $_POST['fecha2'] . " " . $_POST['hora2'];
        $qry = $db->query("INSERT INTO capacitaciones (capaprospectoid,capauninegocio,capaidlistado,capanombre, capacomentarios, capanumasistentes, capafecha, capaestado, capausuario) VALUES (" . $_POST['prospecto'] . ",'" . $_POST['uninegocio'] . "','" . $row1['liscapaid'] . "','" . $row1['liscapadescripcion'] . "', '" . $_POST['comentarios'] . "', '" . $_POST['asistentes'] . "', '" . $fecha_hora_capacitacion . "', '1', '" . $_SESSION['id'] . "' );");
        header('Location:llamar.php');
    }
}

?>
<!doctype html>
<html>
<head>

    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#form').validationEngine({
                showOneMessage: true,
                onValidationComplete: function (form, status) {
                    if (status == true) {
                        carga();
                        return true;
                    }
                }
            });
            $('#fecha2').datepicker({
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true,
                minDate: '0D',
            });
            $('.btnvalidar').button({icons: {primary: 'ui-icon ui-icon-check'}});
        });
    </script>
</head>
<body>
<?php require($r . 'incluir/src/login.php') ?>
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <?php require($r . 'incluir/src/menu.php') ?>
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a href="#">Call Center</a>
            <div class="mapa_div"></div>
            <a class="current">Agendar Capacitacion</a>
        </article>
        <article id="contenido">
            <form id="form" name="form" action="agencapacitacion.php" method="post">
                <fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
                    <legend class="ui-widget ui-widget-header ui-corner-all">Agendar Capacitacion</legend>

                    <p>
                        <label style="width:140px;" for="prospecto"> Identificacion de prospectos: </label>
                        <input type="text" name="prospecto" id="prospecto" value="<?php echo $prospecto ?>"
                               class="id validate[required]" readonly/>
                        <input type="hidden" name="agencapacitacion" id="agencapacitacion" value="1"
                               class="id validate[required]"/>
                    </p>
                    <p>
                        <label style="width:140px;" for="capacitacion"> Capacitacion: </label>
                        <select name="capacitacion" id="capacitacion" class="validate[required] text-input">
                            <option value=""> SELECCIONE</option>
                            <?php
                            $qry = $db->query("SELECT * FROM liscapacitaciones WHERE liscapaestado = 1 ORDER BY liscapadescripcion");
                            while ($row4 = $qry->fetch(PDO::FETCH_ASSOC)) {
                                echo '<option class="validate[required]" value="' . $row4["liscapaid"] . '">' . $row4['liscapadescripcion'] . '</option>';
                            }
                            ?>
                        </select>
                    </p>
                    <p>
                        <label style="width:140px;" for="uninegocio"> Unidad de Negocio: </label>
                        <select name="uninegocio" id="uninegocio" class="validate[required] text-input">
                            <option value=""> SELECCIONE</option>
                            <?php
                            $qry = $db->query("SELECT * FROM uninegocio WHERE uniestado = 1 ORDER BY unidescripcion");
                            while ($row4 = $qry->fetch(PDO::FETCH_ASSOC)) {
                                echo '<option value="' . $row4["uniid"] . '">' . $row4['unidescripcion'] . '</option>';
                            }
                            ?>
                        </select>
                    </p>
                    <p>
                        <label for="comentarios"> Comentarios: </label>
                        <textarea name="comentarios" class="form-control" id="comentarios" rows="4"
                                  cols="65"></textarea>
                    </p>
                    <p>
                        <label for="fecha2"> Fecha de capacitacion: </label>
                        <input type="text" name="fecha2" id="fecha2" class="validate[required] text-input fecha"/>
                    </p>
                    <p>
                        <label style="width:140px;" for="hora2"> Hora de capacitacion: </label>
                        <input type="time" name="hora2" id="hora2" value="" max="23:59:59" min="00:00:01"
                               class="validate[required]" step="1">
                    </p>
                    <p>
                        <label style="width:140px;" title="Numero posible de asistentes" for="asistentes">N.P de
                            Asistentes: </label>
                        <input style="width:60px;" type="number" name="asistentes" class="id validate[required]"
                               id="asistentes" min="1" max="50">
                    </p>
                    </br>
                    <p class="boton">
                        <button type="submit" class="btn btn-primary mt-1" name="validar" value="validar">Validar
                        </button>
                        <!-- BOTON VALIDAR -->
                    </p>

                </fieldset>
            </form>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php'); ?>
</section>
</body>
</html>