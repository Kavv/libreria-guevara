<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
require($r . 'incluir/funciones.php');

if (isset($_POST['consultar'])) {
    $nombre = $_POST['nombre'];
    $telefonos = $_POST['telefonos'];
    $celulares = $_POST['celulares'];
    $email = $_POST['email'];
    $idpros = $_POST['idpros'];

} else {

    $nombre = isset($_GET['nombre']) ? $_GET['nombre'] : '';
    $telefonos = isset($_GET['telefonos']) ? $_GET['telefonos'] : '';
    $celulares = isset($_GET['celulares']) ? $_GET['celulares'] : '';
    $email = isset($_GET['email']) ? $_GET['email'] : '';
    $idpros = isset($_GET['idpros']) ? $_GET['idpros'] : '';
}

if (isset($_POST['modificar'])) {
    $numid = $_POST['numid'];
    $nombrecliente = strtoupper(trim($_POST['nombrecliente']));  // LO RECIBIDO DE ESTE CAMPO LO CAMBIAMOS A MAYUSCULA
    $nombrealternativo = strtoupper(trim($_POST['nombrealternativo']));  // LO RECIBIDO DE ESTE CAMPO LO CAMBIAMOS A MAYUSCULA
    $tipocli = $_POST['tipocli'];
    $depresidencia = $_POST['depresidencia'];
    $ciuresidencia = $_POST['ciuresidencia'];
    $barrio = strtoupper(trim($_POST['barrio']));  // LO RECIBIDO DE ESTE CAMPO LO CAMBIAMOS A MAYUSCULA
    $direccion = strtoupper(trim($_POST['direccion']));  // LO RECIBIDO DE ESTE CAMPO LO CAMBIAMOS A MAYUSCULA
    $indicaciones = strtoupper(trim($_POST['indicaciones']));  // LO RECIBIDO DE ESTE CAMPO LO CAMBIAMOS A MAYUSCULA
    $email1 = strtoupper(trim($_POST['email1']));  // LO RECIBIDO DE ESTE CAMPO LO CAMBIAMOS A MAYUSCULA
    $email2 = strtoupper(trim($_POST['email2']));  // LO RECIBIDO DE ESTE CAMPO LO CAMBIAMOS A MAYUSCULA
    $telefono1 = $_POST['telefono1'];
    $telefono2 = $_POST['telefono2'];
    $celular1 = $_POST['celular1'];
    $celular2 = $_POST['celular2'];
    $contacto = strtoupper(trim($_POST['contacto']));  // LO RECIBIDO DE ESTE CAMPO LO CAMBIAMOS A MAYUSCULA
    $contactoalternativo = strtoupper(trim($_POST['contactoalternativo']));  // LO RECIBIDO DE ESTE CAMPO LO CAMBIAMOS A MAYUSCULA


    $qry = $db->query("UPDATE prospectos SET protipo = '" . $tipocli . "', pronombre = '" . $nombrecliente . "', pronombrealternativo ='" . $nombrealternativo . "', prodepartamento = '" . $depresidencia . "', prociudad = '" . $ciuresidencia . "', probarrio = '" . $barrio . "', prodireccion = '" . $direccion . "', proindicaciones = '" . $indicaciones . "', proemail1 = '" . $email1 . "', proemail2 = '" . $email2 . "', protelefono1 = '" . $telefono1 . "', protelefono2 = '" . $telefono2 . "', procelular1 = '" . $celular1 . "', procelular2 = '" . $celular2 . "', procontacto = '" . $contacto . "', procontactoalternativo = '" . $contactoalternativo . "' WHERE proid = " . $numid . " ;");
    if ($qry) $mensaje = 'Se ha actualizado correctamente el prospecto';
    else $error = 'No se pudo actualizar el prospecto';

}


$filtro = 'nombre=' . $nombre . '&telefonos=' . $telefonos . '&celulares=' . $celulares;

$con = 'SELECT * FROM prospectos ';
$ord = ' ORDER BY profechaingreso DESC';

$sql = $con;

// Mostrar todos ignora
if (!isset($_POST['show-all'])) {
    $sql = crearConsulta($sql, $ord,
        array($idpros, "proid = $idpros"),
        array($nombre, "(pronombre like '%$nombre%' or pronombrealternativo like '%$nombre%')"),
        array($telefonos, "(protelefono1 like '%$telefonos%' or protelefono2 like '%$telefonos%')"),
        array($celulares, "(procelular1 like '%$celulares%' or procelular2 like '%$celulares%')")
    );
} else {
    $sql = $con . " " . $ord;
}


$qry = $db->query($sql);
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>

    <link rel="stylesheet" type="text/css"
          href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css"/>
    <script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            $(".btn-dir").click(function () {
                var text = $(this).data().dir;
                var p = "<p>" + text + "</p>";
                console.log(p);
                $("#detalle-body-modal").empty();
                $("#detalle-body-modal").append(p);
            });

            $('#tabla').dataTable({
                'bJQueryUI': true,
                'bStateSave': true,
                'sPaginationType': 'full_numbers',
                'oLanguage': {
                    'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
                },
                'bSort': false,
            });
            $('#dialog-message').dialog({
                height: 80,
                width: 'auto',
                modal: true
            });
        });
    </script>
</head>
<body>
<?php require($r . 'incluir/src/login.php') ?>
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <?php require($r . 'incluir/src/menu.php') ?>
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a href="#">Call Center</a>
            <div class="mapa_div"></div>
            <a class="current">Consultar</a>
        </article>
        <article id="contenido">
            <h2>Listado de Prospectos</h2>
            <table id="tabla">
                <thead>
                <tr>
                    <th align="center">Nombre</th>
                    <th align="center" title="Nombre Alternativo">N.A</th>
                    <th>Departamento</th>
                    <th>Ciudad</th>
                    <th>Barrio</th>
                    <th>Direccion</th>
                    <th title="Email Uno"><img src="<?php echo $r . 'imagenes/iconos/email.png' ?>"/></th>
                    <th title="Email Dos"><img src="<?php echo $r . 'imagenes/iconos/email.png' ?>"/></th>
                    <th title="Telefono Uno"><img src="<?php echo $r . 'imagenes/iconos/telefono.png' ?>"/></th>
                    <th title="Telefono Dos"><img src="<?php echo $r . 'imagenes/iconos/telefono.png' ?>"/></th>
                    <th title="Celular Uno"><img src="<?php echo $r . 'imagenes/iconos/telefono.png' ?>"/></th>
                    <th title="Celular Dos"><img src="<?php echo $r . 'imagenes/iconos/telefono.png' ?>"/></th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php
                while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
                    ?>
                    <tr>
                        <td title="<?php echo $row['proid'] ?>"><?php echo $row['pronombre'] ?></td>
                        <td align="center"><img src="<?php echo $r ?>imagenes/iconos/user_red.png"
                                                title="<?php echo $row['pronombrealternativo'] ?>"/></td>
                        <?php
                        if (!empty($row['prodepartamento'])) {
                            $qrydepar = $db->query("SELECT * FROM departamentos WHERE depid = '" . $row['prodepartamento'] . "';");
                            $rowdepar = $qrydepar->fetch(PDO::FETCH_ASSOC);
                            $numdepar = $qrydepar->rowCount();
                            if ($numdepar == 1) {
                                $departamento = $rowdepar['depnombre'];
                                $qryciu = $db->query("SELECT * FROM ciudades WHERE ciudepto = '" . $rowdepar['depid'] . "' AND ciuid = '" . $row['prociudad'] . "' ;");
                                $rowciu = $qryciu->fetch(PDO::FETCH_ASSOC);
                                $numciu = $qryciu->rowCount();
                                if ($numciu == 1) {
                                    $ciudad = $rowciu['ciunombre'];
                                } else {
                                    $ciudad = "NONE";
                                }
                            } else {
                                $departamento = "NONE";
                                $ciudad = "NONE";
                            }
                        } else {
                            $departamento = "NONE";
                            $ciudad = "NONE";
                        }
                        ?>
                        <td align="center"><?php echo $departamento ?></td>
                        <td align="center"><?php echo $ciudad ?></td>
                        <td align="center"><?php echo $row['probarrio'] ?></td>
                        <td align="center">
                            <?php
                            if ($row['prodireccion'] != "")
                                echo '<button class="btn btn-light btn-dir" data-dir="' . $row['prodireccion'] . " - " . $row['proindicaciones'] . '" data-toggle="modal" data-target="#modaldetalle"><img src="' . $r . 'imagenes/iconos/casa.png" title="' . $row['prodireccion'] . " - " . $row['proindicaciones'] . '" /></button>';
                            else
                                echo "-";
                            ?>
                        </td>
                        <td>
                            <?php
                            if ($row['proemail1'] != "")
                                echo '<a href="mailto:' . $row["proemail1"] . '"><img src="' . $r . 'imagenes/iconos/email.png" title="' . $row["proemail1"] . '" /></a>';
                            else
                                echo "-";
                            ?>
                        </td>
                        <td>
                            <?php
                            if ($row['proemail2'] != "")
                                echo '<a href="mailto:' . $row["proemail2"] . '"><img src="' . $r . 'imagenes/iconos/email.png" title="' . $row["proemail2"] . '" /></a>';
                            else
                                echo "-";
                            ?>
                        </td>
                        <td>
                            <?php
                            if ($row['protelefono1'] != "")
                                echo '<a href="tel:' . $row["protelefono1"] . '"><img src="' . $r . 'imagenes/iconos/telefono.png" title="' . $row["protelefono1"] . '" /></a>';
                            else
                                echo "-";
                            ?>
                        </td>
                        <td>
                            <?php
                            if ($row['protelefono2'] != "")
                                echo '<a href="tel:' . $row["protelefono2"] . '"><img src="' . $r . 'imagenes/iconos/telefono.png" title="' . $row["protelefono2"] . '" /></a>';
                            else
                                echo "-";
                            ?>
                        </td>
                        <td>
                            <?php
                            if ($row['procelular1'] != "")
                                echo '<a href="tel:' . $row["procelular1"] . '"><img src="' . $r . 'imagenes/iconos/telefono.png" title="' . $row["procelular1"] . '" /></a>';
                            else
                                echo "-";
                            ?>
                        </td>
                        <td>
                            <?php
                            if ($row['procelular2'] != "")
                                echo '<a href="tel:' . $row["procelular2"] . '"><img src="' . $r . 'imagenes/iconos/telefono.png" title="' . $row["procelular2"] . '" /></a>';
                            else
                                echo "-";
                            ?>
                        </td>
                        <td align="center">
                            <?php
                            $qrypermisos = $db->query("SELECT * FROM usuarios WHERE usuid = '" . $_SESSION['id'] . "';");
                            $rowpermisos = $qrypermisos->fetch(PDO::FETCH_ASSOC);
                            if ($rowpermisos['usuperfil'] == 98 or $rowpermisos['usudirectorcall'] == 1) {
                                ?>
                                <a href="modificar.php?id=<?php echo $row['proid'] . "&modificar=1&" . $filtro ?>"
                                   title="Modificar"><img src="<?php echo $r ?>imagenes/iconos/lapiz.png"
                                                          class="grayscale"/></a>
                                <?php
                            } else echo '<img src="' . $r . 'imagenes/iconos/lapiz.png" class="gray" />';
                            ?>
                        </td>
                        <td align="center">
                            <?php
                            if ($rowpermisos['usuperfil'] == 30 || $rowpermisos['usuperfil'] == 98 || $rowpermisos['usudirectorcall'] == 1) {
                                $qry1 = $db->query("SELECT * FROM hisprospectos WHERE hisproprospectos = " . $row['proid'] . " GROUP BY hisproprospectos;");
                                $row1 = $qry1->fetch(PDO::FETCH_ASSOC);
                                $num = $qry1->rowCount();
                                if ($num >= 1) {
                                    $hisprospectos_x = $row1['hisproprospectos'];
                                } else {
                                    $hisprospectos_x = '';
                                }
                                ?>
                                <a href="llamar.php?proid=<?php echo $row['proid'] . '&hisproprospecto=' . $hisprospectos_x . '&' . $filtro ?>"
                                   class="confirmar" title="Gestionar Llamada"><img
                                            src="<?php echo $r ?>imagenes/iconos/date_go.png" class="grayscale"/></a>
                                <?php
                            } else {
                                echo '<img src="' . $r . 'imagenes/iconos/date_go.png" class="gray" />';
                            }
                            ?>
                        </td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
            <button type="button" class="btn btn-outline-primary btnatras"
                    onClick="carga(); location.href='consultar.php'">
                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-left-square-fill"
                     fill="currentColor"
                     xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd"
                          d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm9.5 8.5a.5.5 0 0 0 0-1H5.707l2.147-2.146a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708-.708L5.707 8.5H11.5z"/>
                </svg>
                Atras
            </button>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php'); ?>
    <div class="modal fade" id="modaldetalle" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Dirección</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div id="detalle-body-modal" class="modal-body">

                </div>
            </div>
        </div>
    </div>
</section>
<?php
if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>';
elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>';
?>
<div id="modal" style="display:none"></div>
<div id="dialog2" style="display:none"></div>
</body>
</html>