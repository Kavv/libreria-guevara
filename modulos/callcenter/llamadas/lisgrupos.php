<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
require($r . 'incluir/funciones.php');

if (isset($_GET['error'])) {
    $error = $_GET['error'];

}
if (isset($_GET['mensaje'])) {
    $mensaje = $_GET['mensaje'];
}

if (isset($_POST['consultar'])) {
    $fecha1 = $_POST['fecha1'];
    $fecha2 = $_POST['fecha2'];
} else {
    $fecha1 = isset($_GET['fecha1']) ? $_GET['fecha1'] : '';
    $fecha2 = isset($_GET['fecha2']) ? $_GET['fecha2'] : '';
}


$filtro = 'fecha1=' . $fecha1 . '&fecha2=' . $fecha2;

$con = "SELECT gruponombre,grupofecha, COUNT(gruponombre) as Cantidad FROM gruposcall";
$ord = " GROUP BY gruponombre ORDER BY grupofecha,gruponombre";


// Consulta base
$sql = $con;

if (!isset($_POST['show-all'])) {
    $sql = crearConsulta($sql, $ord,
        array($fecha1, "DATE(grupofecha)  between '$fecha1' and '$fecha2'")
    );
} else {
    $sql = $sql . " " . $ord;
}

$qry = $db->query($sql);

?>
<!doctype html>
<html>
<head>
    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>

    <link rel="stylesheet" type="text/css"
          href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css"/>
    <script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#tabla').dataTable({
                'bJQueryUI': true,
                'bStateSave': true,
                'sPaginationType': 'full_numbers',
                'oLanguage': {
                    'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
                },
                'bSort': false,
            });
            $('.btnatras').button({icons: {primary: 'ui-icon ui-icon-arrowthick-1-w'}});
            $('#dialog-message').dialog({
                height: 80,
                width: 'auto',
                modal: true
            });
        });
    </script>
</head>
<body>
<?php require($r . 'incluir/src/login.php') ?>
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <?php require($r . 'incluir/src/menu.php') ?>
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a href="#">Call Center</a>
            <div class="mapa_div"></div>
            <a class="current">Consultar Grupos</a>
        </article>
        <article id="contenido">
            <h2>Listado de Grupos <?php $sql ?></h2>
            <table id="tabla">
                <thead>
                <tr>
                    <th align="center">Nombre Grupo</th>
                    <th align="center">Fecha de Creacion</th>
                    <th align="center">Numero de Integrantes</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php
                while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
                    ?>
                    <tr>
                        <td align="center"><?php echo $row['gruponombre'] ?></td>
                        <td align="center"><?php echo $row['grupofecha'] ?></td>
                        <td align="center"><?php echo $row['Cantidad'] ?></td>
                        <td align="center"><a
                                    href="detallegrupo.php?link=si&nombregrupo=<?php echo $row['gruponombre'] . "&" . $filtro ?>"
                                    title="Detalle Relacionistas"><img
                                        src="<?php echo $r ?>imagenes/iconos/page_white_find.png"/></a></td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
            <p class="boton">
                <button type="button" class="btn btn-outline-primary btnatras"
                        onClick="carga(); location.href='congrupos.php'">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-left-square-fill"
                         fill="currentColor"
                         xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                              d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm9.5 8.5a.5.5 0 0 0 0-1H5.707l2.147-2.146a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708-.708L5.707 8.5H11.5z"/>
                    </svg>
                    Atras
                </button> <!-- BOTON PARA VOLVER A CONSULTAR -->
            </p>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php'); ?>
</section>
<?php
if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>';
elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>';
?>
<div id="modal" style="display:none"></div>
<div id="dialog2" style="display:none"></div>
</body>
</html>