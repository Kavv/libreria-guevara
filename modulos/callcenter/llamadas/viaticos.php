<?php
// LA ACCION PRINCIPAL ES ESTA PAGINA ES EL MANEJO DE LLAMADAS PARA EL CALL CENTER
$r = '../../../';
//INCLUIR SESION Y CONECCION
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');


if (isset($_POST['validar']) && $_POST['validar']) {

    $dia_viaje_ida = $_POST['fechaida'];
    $dia_viaje_regreso = $_POST['fecharegreso'];
    $dep_destino = $_POST['departamento'];
    $ciu_destino = $_POST['ciudad'];
    $capacitador = $_POST['asesor'];
    $empresa_capa = $_POST['empresa'];
    $monto_total = $_POST['monto_total'];
    $usuario = $_SESSION['id'];
    $ip_usuario = $_SERVER['REMOTE_ADDR'];

    $qryviaticos = $db->query("INSERT INTO viaticos (viadiaviajeida, viadiaviajeregreso, viadepdestino, viaciudestino, viacapacitador, viaempcapacitador, viausuario, viaip, viaestado, viavalortotal) VALUES ('" . $dia_viaje_ida . "', '" . $dia_viaje_regreso . "', '" . $dep_destino . "', '" . $ciu_destino . "', '" . $capacitador . "', '" . $empresa_capa . "', '" . $usuario . "', '" . $ip_usuario . "', 'PROCESO', '" . $monto_total . "'); ");

    if ($qryviaticos) {
        $mensaje = "Se ha insertado correctamente.";
        header('Location:viaticos.php?mensaje=' . $mensaje . '');
    } else {
        $error = "No se pudo ingresar por favor intente nuevamente o contacte con el encargado del portal.";
        header('Location:viaticos.php?error=' . $error . '');
    }

}
?>
<!doctype html>
<html>
<head>

    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>

    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css"/>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

    <script type="text/javascript">

        $(document).ready(function () {

            $('#asesor').selectpicker();

            $('#fechaida').datepicker({ // VALIDACION DE LA FECHA UNO
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true,
                maxDate: '+50D',
                onClose: function (selectedDate) {
                    $('#fecharegreso').datepicker('option', 'minDate', selectedDate);
                }
            })
            $('#fecharegreso').datepicker({ // VALIDACION DE LA FECHA DOS
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true,
                maxDate: '+50D',
                onClose: function (selectedDate) {
                    $('#fechaida').datepicker('option', 'maxDate', selectedDate);
                }
            }).keypress(function (event) {
                event.preventDefault()
            });


            $('#departamento').change(function (event) {
                var id1 = $('#departamento').find(':selected').val();
                $('#ciudad').load('<?php echo $r ?>incluir/carga/ciudades.php?id1=' + id1);
            });


        });
    </script>
</head>
<body>
<?php require($r . 'incluir/src/login.php') ?> <!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
    <?php require($r . 'incluir/src/menu.php') ?> <!-- INCLUIMOS MENU PRINCIPAL -->
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a href="#">Call Center</a>
            <div class="mapa_div"></div>
            <a class="current">Insertar Viaticos</a>
        </article>
        <article id="contenido">
            <form id="form" name="form" action="viaticos.php" method="post"> <!-- ENVIO FORMULARIO POR POST -->
                <fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
                    <legend class="ui-widget ui-widget-header ui-corner-all">Insertar Viaticos</legend>
                    </br>

                    <p>
                        <label for="fecha1"> Fecha de Ida y Regreso: </label>
                        <input type="text" name="fechaida" id="fechaida" class="validate[required] text-input fecha"/> -
                        <input type="text" name="fecharegreso" id="fecharegreso"
                               class="validate[required] text-input fecha"/>
                    </p>
                    <p>
                        <label>Provincia:</label>
                        <select id="departamento" name="departamento"
                                class="validate[required] text-input">
                            <?php
                            echo '<option value="">SELECCIONE</option>';
                            $qry = $db->query("SELECT * FROM departamentos ORDER BY depnombre");
                            while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
                                echo '<option value=' . $row2['depid'] . '>' . $row2['depnombre'] . '</option>';
                            }
                            ?>
                        </select>
                    </p>
                    <p>
                        <label>Ciudad: </label>
                        <select name="ciudad" id="ciudad" class="clasvalidate[required] text-input">
                            <?php
                            echo '<option value="">SELECCIONE</option>';
                            $qry = $db->query("SELECT * FROM ciudades  ORDER BY ciunombre");
                            while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
                                echo '<option value=' . $row2['depid'] . '>' . $row2['depnombre'] . '</option>';
                            }
                            ?>
                        </select>
                    </p>
                    <p>
                        <label for="asesor">Asesor:</label>
                        <select name="asesor" id="asesor" class="form-control validate[required] text-input" data-live-search="true">
                            <option value="">SELECCIONE</option>
                            <?php
                            $qry = $db->query("SELECT * FROM usuarios WHERE usuasesor = '1' AND usuperfil <> 56 or usuid = '79619076' ORDER BY usunombre");
                            while ($row = $qry->fetch(PDO::FETCH_ASSOC))
                                echo '<option value=' . $row['usuid'] . '>' . $row['usunombre'] . '</option>';
                            ?>
                        </select>
                    </p>
                    <p>
                        <label for="empresa">Empresa del capacitador:</label>
                        <select name="empresa" class="validate[required] text-input">
                            <option value="">SELECCIONE</option>
                            <?php
                            $qry = $db->query("SELECT * FROM empresas ORDER BY empnombre");
                            while ($row = $qry->fetch(PDO::FETCH_ASSOC))
                                echo '<option value=' . $row['empid'] . '>' . $row['empnombre'] . '</option>';
                            ?>
                        </select>
                    </p>
                    <p>
                        <label title='Monto Total'>Monto Total: </label>
                        <?php
                        if (empty($rowprin['viavalortotal'])) {
                            echo "<input type='text' name='monto_total' id='monto_total' class='valor validate[required, custom[onlyNumberSp]] text-input'  />";
                        } elseif (!empty($rowprin['viavalortotal'])) {
                            echo "<input type='text' value ='" . $rowprin['viavalortotal'] . "' name='monto_total' id='monto_total' class='valor validate[required, custom[onlyNumberSp]] text-input'  />";
                        }
                        ?>
                    </p>
                    </br>
                    <p class="boton">
                        <button type="submit" class="btn btn-primary" name="validar" value="validar">Validar</button>
                        <!-- BOTON VALIDAR -->
                    </p>
                </fieldset>
            </form>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php'); ?> <!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
</section>
<?php
if (isset($_GET['error'])) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['error'] . '</div>'; // MENSAJE MODAL ERROR
if (isset($_GET['mensaje'])) echo '<div id="dialog-message" title="Exitoso"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['mensaje'] . '</div>'; // MENSAJE MODAL EXITOSO
?>
<div id="dialog2" title="Historico de Llamadas" style="display:none"></div>
</body>
</html>