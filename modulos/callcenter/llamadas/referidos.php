<?php
// LA ACCION PRINCIPAL ES ESTA PAGINA ES EL MANEJO DE REFERIDOS
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
require($r . 'incluir/phpmailer/PHPMailer/class.phpmailer.php');
require($r . 'incluir/phpmailer/PHPMailer/class.smtp.php');
require($r . 'incluir/mail/send_email.php');

if (isset($_GET['mensaje'])) {
    $mensaje = $_GET['mensaje'];
}
if (isset($_GET['error'])) {
    $error = $_GET['error'];
}
if (isset($_POST['validar']) && $_POST['validar']) {
    $nombrecliente = strtoupper(trim($_POST['nombre']));  // MAYUSCULA
    $nombreempresa = strtoupper(trim($_POST['empresa']));  // MAYUSCULA
    $email1 = strtoupper(trim($_POST['email1']));  // MAYUSCULA
    $email2 = strtoupper(trim($_POST['email2']));  // MAYUSCULA
    $telefono1 = $_POST['telefono1'];
    $telefono2 = $_POST['telefono2'];

    if (!isset($_POST['campana'])) {
        $error = 'Seleccione la campaña del referido';
        header('Location:referidos.php?error=' . $error);
    }

    $idCampa = $_POST['campana'];
    $qry = $db->query("INSERT INTO prospectos (procampana, protipo, pronombre, pronombrealternativo, proemail1, proemail2, protelefono1, protelefono2, procontacto, procampatipo) VALUES ($idCampa, '2', '$nombreempresa', '$nombrecliente', '$email1', '$email2', '$telefono1', '$telefono2', '$nombrecliente', '1');");

    if ($qry) {

        $body = '
		<h2>Se ha insertado un nuevo referido con la siguiente informacion</h2>
		<p>
		<ul>
		<li>Nombre del Cliente = ' . $nombrecliente . '</li>
		<li>Nombre de la Empresa = ' . $nombreempresa . '</li>
		<li>Email 1 = ' . $email1 . '</li>
		<li>Email 2 = ' . $email2 . '</li>
		<li>Telefono 1 = ' . $telefono1 . '</li>
		<li>Telefono 2 = ' . $telefono2 . '</li>
		</ul>
		</p>
		<p style="font-size:11px;">los signos de puntuacion y caracteres especiales han sido omitidos de manera predeterminada para evitar inconvenientes con los diferentes servidores de correo</p>
		
		';


        $emailDelUsuarioActual = $db->query("select usuemail from usuarios where usuid = '" . $_SESSION['id'] . "';")->fetchColumn();
        $address_copy = '';

        $address = $emailDelUsuarioActual;

        $mail = new PHPMailer();
        $mail->IsSMTP(); // telling the class to use SMTP
        $mail->SMTPDebug = 0;                     // enables SMTP debug information (for testing)
        // 1 = errors and messages
        // 2 = messages only
        $mail->SMTPAuth = true;                  // enable SMTP authentication
        $mail->SMTPSecure = "ssl";                 // sets the prefix to the servier
        $mail->Host = $defaultHost;      // sets GMAIL as the SMTP server
        $mail->Port = $defaultPort;                   // set the SMTP port for the GMAIL server
        $mail->Username = $defaultUsername;  // GMAIL username
        $mail->Password = $defaultPassword;            // GMAIL password
        $mail->SetFrom($address, "REFERIDOS");
        $mail->AddReplyTo($address, "REFERIDOS");
        $mail->Subject = "Referidos ";
        $mail->MsgHTML(utf8_decode($body));
        $mail->AddAddress($email1);
        $mail->AddCC($email2);
        if (!$mail->Send()) {
            $error = "Se inserto el caso pero no se pudo enviar el email por la siguiente causa: " . $mail->ErrorInfo;
            header('Location:' . $r . 'insertarpqrs.php?error=' . $error);
        } else {
            $mensaje = 'Se ha insertado correctamente el Referido';
            header('Location:referidos.php?mensaje=' . $mensaje);
        }
    } else {
        $error = 'No se pudo insertar el referido intente nuevamente o contacte con el administrador';
        header('Location:referidos.php?error=' . $error);
    }

}


?>
<!doctype html>
<html>
<head>

    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>

</head>
<body>
<?php require($r . 'incluir/src/login.php') ?> <!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
    <?php require($r . 'incluir/src/menu.php') ?> <!-- INCLUIMOS MENU PRINCIPAL -->
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a href="#">Call Center</a>
            <div class="mapa_div"></div>
            <a class="current">Gestion de Referidos</a>
        </article>
        <article id="contenido">
            <form id="form" name="form" action="referidos.php" method="post"> <!-- ENVIO FORMULARIO POR POST -->
                <fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
                    <legend class="ui-widget ui-widget-header ui-corner-all">Gestion de referidos</legend>
                    </br>
                    <p>
                        <label for="empresa">Nombre de la Empresa:</label>
                        <input type="text" style="width:80%" class="id" name="empresa"/>
                    </p>
                    <p>
                        <label for="nombre">Nombre del Cliente:</label>
                        <input type="text" style="width:80%" class="id" name="nombre"/>
                    </p>
                    <p>
                        <label>E-mail 1: </label>
                        <input type="text" name="email1" class="email"/>
                        <label>E-mail 2: </label>
                        <input type="text" name="email2" class="email"/>
                    </p>
                    <p>
                        <label>Campaña: </label> <!-- CAMPO TIPO DE PROSPECTO -->
                        <select name="campana" id="campana" class="validate[required] text-input">
                            <?php
                            echo '<option value="">SELECCIONE</option>';
                            $qry = $db->query("select distinct c.campaid, c.campanombre
from campana c
         inner join campanarelacionista cr on c.campaid = cr.idcampana
where cr.idrelacionista = '" . $_SESSION['id'] .
                                "'  and c.campaestado = 'ACTIVA';");
                            while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
                                echo '<option value=' . $row2['campaid'] . '>' . $row2['campanombre'] . '</option>'; // APARECEN TODOS LOS DEPARTAMENTOS PARA QUE EL USUARIO ELIGA EL PERTINENTE
                            }
                            ?>
                        </select>
                    </p>
                    <p>
                        <label>Telefono 1: </label>
                        <input type="text" name="telefono1" class="telefono" title="Numero Telefono 1"/>
                        <label>Telefono 2: </label>
                        <input type="text" name="telefono2" class="telefono" title="Numero Telefono 2"/>
                    </p>
                    <br>
                    <p class="boton">
                        <button type="submit" class="btn btn-primary mt-1" name="validar" value="validar">Validar
                        </button>
                        <!-- BOTON VALIDAR -->
                    </p>
                </fieldset>
            </form>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php'); ?> <!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
</section>
<?php
if (isset($_GET['error'])) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['error'] . '</div>'; // MENSAJE MODAL ERROR
if (isset($_GET['mensaje'])) echo '<div id="dialog-message" title="Exitoso"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['mensaje'] . '</div>'; // MENSAJE MODAL EXITOSO
?>
<div id="dialog2" title="Historico de Llamadas" style="display:none"></div>
</body>
</html>