<?php
$r = '../../../'; 
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
include($r.'incluir/fpdf/fpdf.php');

$iden = $_GET['iden'];
$sql = "SELECT * FROM viaticos WHERE viaid = ".$iden.";"; 
$rowprin = $db->query($sql)->fetch(PDO::FETCH_ASSOC);

$qrydeparta = $db->query("SELECT * FROM departamentos where depid = ".$rowprin['viadepdestino']."; ");
$rowdeparta = $qrydeparta->fetch(PDO::FETCH_ASSOC);
$departamento = $rowdeparta['depnombre'];

$qryciudad = $db->query("SELECT * FROM ciudades where ciudepto = ".$rowprin['viadepdestino']." AND ciuid = ".$rowprin['viaciudestino']." ");
$rowciudad = $qryciudad->fetch(PDO::FETCH_ASSOC);
$ciudad = $rowciudad['ciunombre'];

$qryasesor = $db->query("SELECT * FROM usuarios where usuid = ".$rowprin['viacapacitador']."; ");
$rowasesor = $qryasesor->fetch(PDO::FETCH_ASSOC);
$asesor = $rowasesor['usunombre'];

$qryempresa = $db->query("SELECT * FROM empresas where empid = ".$rowprin['viaempcapacitador']."; ");
$rowempresa = $qryempresa->fetch(PDO::FETCH_ASSOC);
$empresa = $rowempresa['empnombre'];

$qrymeses = $db->query("SELECT * FROM meses_ano where mesid = ".$rowprin['viamesdiligenciamiento']."; ");
$rowmeses = $qrymeses->fetch(PDO::FETCH_ASSOC);
$meses = $rowmeses['mesnombre'];


$pdf = new FPDF();
$pdf->AddFont('LucidaConsole','','lucon.php');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Arial','',8);
$pdf->Cell(0,5,date('Y/m/d H:i:s'),0,1,'R');
$pdf->SetFont('Arial','',10);
$pdf->Cell(0,4,'-----------------------------------------------------------------------------------------------------------------------------------------------------------------',0,1,'C');
$pdf->SetFont('Arial','B',10);
$pdf->Cell(0,4,''.$empresa.'',0,1,'C');
$pdf->Cell(0,4,'NIT '.number_format($rowprin['viaempcapacitador'],0,',','.').'',0,1,'C');
$pdf->Cell(0,4,'COMPROBANTE DE PAGO A LOS TRABAJADORES             NUMERO - '.$iden.'',0,1,'C');
$pdf->Ln(5);
$pdf->SetFont('Arial','',10);
$pdf->Cell(0,4,'NOMBRE:                                          '.$asesor.'',0,1,'');
$pdf->Cell(0,4,'MES DE LIQUIDACION:                   '.$meses.'',0,1,'');
$pdf->Cell(0,4,'FECHA:                                             '.date('d/m/Y').'',0,1,'');
$pdf->Cell(0,4,'-----------------------------------------------------------------------------------------------------------------------------------------------------------------',0,1,'C');
$pdf->Ln(6);

$alojamiento = $rowprin['viavalortotal'] * 0.25;
$manutencion = $rowprin['viavalortotal'] * 0.25;
$representacion = $rowprin['viavalortotal'] * 0.5;

$rtefuente = $representacion * 0.1;
$rteica = $representacion * 0.01;

$pdf->SetFont('Arial','B',10);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(140,6,'CONCEPTO ',1,0,'C',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(50,6,'VALOR',1,1,'C',true);
$pdf->SetFillColor(250,250,250);
$pdf->SetFont('Arial','',10);
$pdf->Cell(140,5,'VIATICOS = '.$departamento.' - '.$ciudad.' ',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(50,5,'$ '.number_format($rowprin['viavalortotal'],0,',','.'),1,1,'R',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(140,5,'    Gastos de Alojamiento ',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(50,5,'$ '.number_format($alojamiento,0,',','.'),1,1,'R',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(140,5,'    Gastos de Manutencion ',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(50,5,'$ '.number_format($manutencion,0,',','.'),1,1,'R',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(140,5,'    Gastos de Representacion ',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(50,5,'$ '.number_format($representacion,0,',','.'),1,1,'R',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(140,5,'          Rte Fuente 10% ',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(50,5,'$ '.number_format($rtefuente,0,',','.'),1,1,'R',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(140,5,'          Rte ICA 1% ',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(50,5,'$ '.number_format($rteica,0,',','.'),1,1,'R',true);
$pdf->SetFillColor(250,250,250);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(140,5,'TOTAL INGRESOS',1,0,'C',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(50,5,'$ '.number_format($rowprin['viavalortotal'],0,',','.'),1,1,'R',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(140,5,'',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(50,5,' ',1,1,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->SetFont('Arial','',10);
$pdf->Cell(140,5,'    Fecha de pago del 1 50% '.$rowprin['viafechavalor1'],1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(50,5,'$ '.number_format($rowprin['viavalor1'],0,',','.'),1,1,'R',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(140,5,'    Fecha de pago del 2 50% '.$rowprin['viafechavalor2'],1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(50,5,'$ '.number_format($rowprin['viavalor2'],0,',','.'),1,1,'R',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(140,5,'',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(50,5,' ',1,1,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(140,5,'TOTAL A PAGAR',1,0,'C',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(50,5,'$ '.number_format($rowprin['viavalortotal'],0,',','.'),1,1,'R',true);
$pdf->SetFont('Arial','',10);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(95,14,'Firma: _________________________________________',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->SetFont('Arial','',8);
$pdf->MultiCell(95,7,'        Se realizara transferencia electronica por lo que se solicita hacer                        llegar el presente documento firmado antes de 20 dias',1,1,'C',true);
$pdf->SetFillColor(250,250,250);
$pdf->SetFont('Arial','',9);
$pdf->Cell(47.5,12,'Elaboro:       Carolina Cortes',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(47.5,12,'Reviso:',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(47.5,12,'Aprobo:',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(47.5,12,'Pago:',1,1,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->SetFont('Arial','',7);
$pdf->Cell(190,4,'Copias: Gestion Humana y Contabilidad',1,1,'R',true);


$pdf->Output($r.'pdf/viaticos/'.$iden.'.pdf');
$pdf->Output();

?>