<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

$iden = $_GET['iden'];
//if (!empty($iden)) {
//    include('pdfviaticos.php?iden=' . $iden);
//}

?>
<!doctype html>
<html>
<head>

    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>

</head>
<body>
<?php require($r . 'incluir/src/login.php') ?>
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <?php require($r . 'incluir/src/menu.php') ?>
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a href="#">Call Center</a>
            <div class="mapa_div"></div>
            <a class="current">PDF Viaticos</a>
        </article>
        <article id="contenido">
            <h2> PDF del comprobante de pago </h2>
            <p align="center">
                <iframe src="pdfviaticos.php?iden=<?php echo $iden ?>" width="800" height="900"></iframe>
            </p>
            <p class="boton">
                <button type="button" class="btn btn-primary" onClick="carga(); location.href = 'viaticos_2.php'">Atras
                </button>
            </p>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php'); ?>
</section>
</body>
</html>