<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');


$qryusu = $db->query("SELECT * FROM usuarios WHERE usuid ='" . $_SESSION['id'] . "';");
$rowusu = $qryusu->fetch(PDO::FETCH_ASSOC);

if ($rowusu['usuperfil'] == 98) {
    $qry2 = $db->query("SELECT * FROM prospectos INNER JOIN hisprospectos ON proid = hisproprospectos WHERE hisprousuario = '" . $_SESSION['id'] . "' AND hisproresultadollamada = 15 AND hisprocontactabilidad = 1 AND hisproefectividad = 3  ORDER BY hisprofechahora desc;");
} elseif ($rowusu['usuperfil'] == 30 || $rowusu['usudirectorcall'] == 1) {
    $qry2 = $db->query("SELECT * 
FROM prospectos INNER JOIN hisprospectos ON proid = hisproprospectos 
WHERE hisproresultadollamada = 15 AND hisprocontactabilidad = 1 AND hisproefectividad = 3  
ORDER BY proid, hisprofechahora DESC");
}

if (isset($_GET['mensaje'])) {
    $mensaje = $_GET['mensaje'];
}
if (isset($_GET['error'])) {
    $error = $_GET['error'];
}

?>
<!doctype html>
<html>
<head>
    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>

    <link rel="stylesheet" type="text/css"
          href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css"/>
    <script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#tabla').dataTable({
                'bJQueryUI': true,
                'bStateSave': true,
                'bSort': false,
                'sPaginationType': 'full_numbers',
                'oLanguage': {
                    'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
                },
            });
            $('.btnatras').button({icons: {primary: 'ui-icon ui-icon-arrowthick-1-w'}});
            $('#dialog-message').dialog({
                height: 80,
                width: 'auto',
                modal: true
            });
        });
    </script>
</head>
<body>
<?php require($r . 'incluir/src/login.php') ?>
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <?php require($r . 'incluir/src/menu.php') ?>
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a href="#">Call Center</a>
            <div class="mapa_div"></div>
            <a class="current">Seguimiento</a>
        </article>
        <article id="contenido">
            <h2>Prospectos en seguimiento por envio de correo</h2>
            <br/><br/>
            <div class="reporte">

            </div>
            <table id="tabla">
                <thead>
                <tr>
                    <th>Prospecto</th>
                    <th>Nombre Alternativo del Prospecto</th>
                    <th title="Usuario">Usuario</th>
                    <th title="Fecha de Gestion">Fecha de Gestion</th>
                    <th title="Telefono Uno"></th>
                    <th title="Telefono Dos"></th>
                    <th title="Celular Uno"></th>
                    <th title="Celular Dos"></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php
                $prospectoSeguimiento = '';
                while ($row2 = $qry2->fetch(PDO::FETCH_ASSOC)) {
                    if ($prospectoSeguimiento <> $row2['proid']) {// se muestra
                        $qryusuario = $db->query("SELECT * FROM usuarios WHERE usuid = " . $row2['hisprousuario'] . "");
                        $rowusuario = $qryusuario->fetch(PDO::FETCH_ASSOC);
                        $usuario = $rowusuario['usunombre'];


                        $filtro = "proid=" . $row2['proid'] . "&hisproprospecto=" . '';
                        ?>
                        <tr>
                            <td title="<?php echo $row2['proid'] ?>"><?php echo $row2['pronombre'] ?></td>
                            <td><?php echo $row2['pronombrealternativo'] ?></td>
                            <td align="center"><?php echo $usuario ?></td>
                            <td align="center"><?php echo $row2['hisprofechahora'] ?></td>
                            <td>
                                <?php
                                if ($row2['protelefono1'] != "")
                                    echo '<a href="tel:' . $row2["protelefono1"] . '"><img src="' . $r . 'imagenes/iconos/telefono.png" title="' . $row2["protelefono1"] . '" /></a>';
                                else
                                    echo "-";
                                ?>
                            </td>
                            <td>
                                <?php
                                if ($row2['protelefono2'] != "")
                                    echo '<a href="tel:' . $row2["protelefono2"] . '"><img src="' . $r . 'imagenes/iconos/telefono.png" title="' . $row2["protelefono2"] . '" /></a>';
                                else
                                    echo "-";
                                ?>
                            </td>
                            <td>
                                <?php
                                if ($row2['procelular1'] != "")
                                    echo '<a href="tel:' . $row2["procelular1"] . '"><img src="' . $r . 'imagenes/iconos/telefono.png" title="' . $row2["procelular1"] . '" /></a>';
                                else
                                    echo "-";
                                ?>
                            </td>
                            <td>
                                <?php
                                if ($row2['procelular2'] != "")
                                    echo '<a href="tel:' . $row2["procelular2"] . '"><img src="' . $r . 'imagenes/iconos/telefono.png" title="' . $row2["procelular2"] . '" /></a>';
                                else
                                    echo "-";
                                ?>
                            </td>
                            <td>
                                <a href="llamar.php?proid=<?php echo $row2['proid'] . '&agendado=1&hisproprospecto=' . $row2['proid'] . '&' . $filtro ?>"
                                   title="Gestionar Llamada"><img src="<?php echo $r ?>imagenes/iconos/date_go.png"
                                                                  class="grayscale"/></a></td>
                        </tr>
                        <?php
                    }
                    $prospectoSeguimiento = $row2['proid'];
                }
                ?>
                </tbody>
            </table>

            <p class="boton">
                <button type="button" class="btn btn-outline-primary btnatras"
                        onClick="carga(); location.href='capacitacion.php'">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-left-square-fill"
                         fill="currentColor"
                         xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                              d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm9.5 8.5a.5.5 0 0 0 0-1H5.707l2.147-2.146a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708-.708L5.707 8.5H11.5z"/>
                    </svg>
                    Atras
                </button> <!-- BOTON PARA VOLVER A CONSULTAR -->
            </p>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php'); ?>
</section>
<?php
if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>';
elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>';
?>
<div id="modal" style="display:none"></div>
<div id="dialog2" style="display:none"></div>
</body>
</html>