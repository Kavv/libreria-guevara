<?php
$r = '../../../';

require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

require($r . 'incluir/phpmailer/PHPMailer/class.phpmailer.php');
require($r . 'incluir/phpmailer/PHPMailer/class.smtp.php');
require($r . 'incluir/mail/send_email.php');
require($r . 'incluir/funciones.php');

$fecha1 = isset($_GET['fecha1']) ? $_GET['fecha1'] : '';
$fecha2 = isset($_GET['fecha2']) ? $_GET['fecha2'] : '';
$consultar = isset($_GET['consultar']) ? $_GET['consultar'] : '';
$call = isset($_GET['call']) ? $_GET['call'] : '';
$asesor = isset($_GET['asesor']) ? $_GET['asesor'] : '';
$departamento = isset($_GET['departamento']) ? $_GET['departamento'] : '';
$ciudad = isset($_GET['ciudad']) ? $_GET['ciudad'] : '';
$estado = isset($_GET['estado']) ? $_GET['estado'] : '';
$fecha3 = isset($_GET['fecha3']) ? $_GET['fecha3'] : '';
$fecha4 = isset($_GET['fecha4']) ? $_GET['fecha4'] : '';
$uninegoci = isset($_GET['uninegoci']) ? $_GET['uninegoci'] : '';
$idcapaci = isset($_GET['idcapaci']) ? $_GET['idcapaci'] : '';
$idpros = isset($_GET['idpros']) ? $_GET['idpros'] : '';
$jeferela = isset($_GET['jeferela']) ? $_GET['jeferela'] : '';
$modalidad = isset($_GET['modalidad']) ? $_GET['modalidad'] : '';


$filtro = "jeferela=" . $jeferela . "&idpros=" . $idpros . "&call=" . $call . "&fecha1=" . $fecha1 . "&fecha2=" . $fecha2 . "&asesor=" . $asesor . "&departamento=" . $departamento . "&ciudad=" . $ciudad . "&estado=" . $estado . "&fecha3=" . $fecha3 . "&fecha4=" . $fecha4 . "&uninegoci=" . $uninegoci . "&idcapaci=" . $idcapaci . "&consultar=" . $consultar . "&modalidad" . $modalidad;

if ((isset($_POST['modificar']) && $_POST['modificar']) || (isset($_POST['modificar_correo']) && $_POST['modificar_correo'])) {

    $prospecto = $_POST['prospecto'];
    $capacitaid = $_POST['capacitaid'];
    $capacitacion = $_POST['capacitacion'];
    $diaagenda = $_POST['diaagenda'];
    $comentarios = strtoupper(trim($_POST['comentarios']));
    $fecha2 = $_POST['fecha2'];
    $fecha2_actual = $_POST['fecha2_actual'];
    $hora2 = $_POST['hora2'];
    $asistentes = $_POST['asistentes'];
    $asesor = $_POST['asesor'];
    $coach = $_POST['coach'];
    $observador = $_POST['observador'];
    $agendamientopor = $_POST['agendamientopor'];
    $fecha_hora_capacitacion = $fecha2 . " " . $hora2;
    $unidadnegocio = $_POST['uninegocio'];

    if (empty($coach)) {
        $coach = '';
    }
    if (empty($observador)) {
        $observador = '';
    }

    $sqlcapaci = "SELECT * FROM liscapacitaciones WHERE liscapaid = '" . $capacitacion . "';";  //
    $qrycapaci = $db->query($sqlcapaci);
    $rowcapaci = $qrycapaci->fetch(PDO::FETCH_ASSOC);
    $nombrecapacitacion = $rowcapaci['liscapadescripcion'];

    if ($fecha2_actual <> $fecha2) { // evaluar si se cambio la fecha de la capacitacion
        $qry = $db->query("UPDATE capacitaciones SET capauninegocio='" . $unidadnegocio . "', capaidlistado='" . $capacitacion . "', capanombre='" . $nombrecapacitacion . "', capacomentarios='" . $comentarios . "', capanumasistentes='" . $asistentes . "', capafecha='" . $fecha_hora_capacitacion . "', capausuario='" . $agendamientopor . "', capacapacitador='', capacorreo='', capaestado='1', capafechausuario='" . $diaagenda . "'  WHERE capaid='" . $capacitaid . "' ;");
    } else {
        $qry = $db->query("UPDATE capacitaciones SET capauninegocio='" . $unidadnegocio . "', capaidlistado='" . $capacitacion . "', capanombre='" . $nombrecapacitacion . "', capacomentarios='" . $comentarios . "', capanumasistentes='" . $asistentes . "', capafecha='" . $fecha_hora_capacitacion . "', capausuario='" . $agendamientopor . "', capacapacitador='" . $asesor . "', capacoach='" . $coach . "', capaobservador='" . $observador . "', capafechausuario='" . $diaagenda . "' WHERE capaid='" . $capacitaid . "' ;");
    }
    if ($qry) {
        $mensaje = 'Se ha actualizado correctamente la capacitacion';
        if ($_POST['modificar_correo']) {
            if (!empty($asesor)) {

                $qryincapacitador = $db->query("SELECT * FROM usuarios WHERE usuid='" . $asesor . "' ;");
                $rowincapacitador = $qryincapacitador->fetch(PDO::FETCH_ASSOC);
                $emailcapacitador = $rowincapacitador['usuemail'];


                $qrycapac = $db->query("SELECT * FROM capacitaciones INNER JOIN prospectos on capaprospectoid = proid WHERE capaid='" . $capacitaid . "' ;");
                $rowcapac = $qrycapac->fetch(PDO::FETCH_ASSOC);

                $qrydepart = $db->query("SELECT * FROM departamentos WHERE depid='" . $rowcapac['prodepartamento'] . "' ;");
                $rowdepart = $qrydepart->fetch(PDO::FETCH_ASSOC);

                $qryciuda = $db->query("SELECT * FROM ciudades WHERE ciudepto='" . $rowcapac['prodepartamento'] . "' AND ciuid = '" . $rowcapac['prociudad'] . "' ;");
                $rowciuda = $qryciuda->fetch(PDO::FETCH_ASSOC);

                $qryusuario1 = $db->query("SELECT * FROM usuarios WHERE usuid = '" . $rowcapac['capausuario'] . "';"); //verificacion usuario por ID de sesion
                $rowusuario1 = $qryusuario1->fetch(PDO::FETCH_ASSOC);

                $qryusuarionom = $db->query("SELECT * FROM usuarios WHERE usuid = '" . $rowcapac['capausuario'] . "';");
                $rowusuarionom = $qryusuarionom->fetch(PDO::FETCH_ASSOC);

                $usuarionom = $rowusuarionom['usunombre'];

                if (!empty($coach)) {
                    $qryincoach = $db->query("SELECT * FROM usuarios WHERE usuid='" . $coach . "' ;");
                    $rowincoach = $qryincoach->fetch(PDO::FETCH_ASSOC);
                    $emailcoach = $rowincoach['usuemail'];
                    $nombrecoach = $rowincoach['usunombre'];
                    $asunto = "Asistira en calidad de COACH - Capacitacion " . $rowcapac['capanombre'] . " " . $rowcapac['capafecha'];
                    $mensaje_calidad = '<p>RECUERDE que usted asistira a esta capacitacion en calidad de COACH</p>';
                } elseif (!empty($observador)) {
                    $qryinobservador = $db->query("SELECT * FROM usuarios WHERE usuid='" . $observador . "' ;");
                    $rowinobservador = $qryinobservador->fetch(PDO::FETCH_ASSOC);
                    $emailobservador = $rowinobservador['usuemail'];
                    $nombreobservador = $rowinobservador['usunombre'];
                    $asunto = "Asistira en calidad de OBSERVADOR - Capacitacion " . $rowcapac['capanombre'] . " " . $rowcapac['capafecha'];
                    $mensaje_calidad = '<p>RECUERDE que usted asistira a esta capacitacion en calidad de OBSERVADOR</p>';
                } else {
                    $emailobservador = '';
                    $nombreobservador = '';
                    $emailcoach = '';
                    $nombrecoach = '';
                    $asunto = $asunto = "Capacitacion " . $rowcapac['capanombre'] . " " . $rowcapac['capafecha'];
                    $mensaje_calidad = '';
                }


                $recordatorio_staff = "<p><b> Recuerde ingresar los pedidos a nombre de " . $usuarionom . "!  </b></p>";


                if ($rowusuarionom['usujefecallcenter'] == '52765484') {
                    $mail = new PHPMailer();
                    $body = '
					<p>Buen Dia!</p>
					' . $mensaje_calidad . '
					<p>A continuacion se envia la informacion de la capacitacion a dictar</p>
					<p>CAPACITACION:  ' . $rowcapac['capanombre'] . '</p>
					<p>FECHA Y HORA:  ' . $rowcapac['capafecha'] . '</p>
					<p>EMPRESA:  ' . $rowcapac['pronombre'] . ' ' . $rowcapac['pronombrealternativo'] . '</p>
					<p>CONTACTO:  ' . $rowcapac['procontacto'] . ' ' . $rowcapac['procontactoalternativo'] . '</p>
					<p>DEPARTAMENTO - CIUDAD: ' . $rowdepart['depnombre'] . ' ' . $rowciuda['ciunombre'] . '</p>
					<p>DIRECCION:  ' . $rowcapac['prodireccion'] . ' ' . $rowcapac['probarrio'] . ' ' . $rowcapac['proindicaciones'] . '</p>
					<p>ASISTENTES:  ' . $rowcapac['capanumasistentes'] . ' posibles</p>
					<p>TELS:    ' . $rowcapac['protelefono1'] . ' ' . $rowcapac['protelefono2'] . ' ' . $rowcapac['procelular1'] . ' ' . $rowcapac['procelular2'] . '</p>
					<p>CORREO:  ' . $rowcapac['proemail1'] . ' ' . $rowcapac['proemail2'] . '</p>
					<p>COMENTARIOS RELACIONISTA:   ' . $rowcapac['capacomentarios'] . '</p>
					<p style="margin-bottom:30px;"><b style="color:red;"> Nota: Favor enviar diariamente los resultados de cada capacitacion, gracias; Exitos..!!! </b></p> 
					<p style="margin-bottom:40px;">Cualquier inquietud con gusto la atendere.</p>
					' . $recordatorio_staff;

                    $mail->IsSMTP(); // telling the class to use SMTP
                    $mail->Host = "smtp.gmail.com"; // SMTP server
                    $mail->SMTPDebug = 0;                     // enables SMTP debug information (for testing)
                    $mail->SMTPAuth = true;                  // enable SMTP authentication
                    $mail->SMTPSecure = "ssl";                 // sets the prefix to the servier


                    $mail->Host = $defaultHost;      // sets GMAIL as the SMTP server
                    $mail->Port = $defaultPort;                   // set the SMTP port for the GMAIL server
                    $mail->Username = $defaultUsername;  // GMAIL username
                    $mail->Password = $defaultPassword;            // GMAIL password

                    $mail->SetFrom($defaultUsername, 'Dpto Coordinacion');
                    $mail->Subject = $asunto;
                    $mail->MsgHTML(utf8_decode($body));
                    $address = $emailcapacitador;
                    $mail->AddAddress($address);
                    $mail->AddCC($emailcoach, $nombrecoach);
                    $mail->AddCC($emailobservador, $nombreobservador);

                    $emailDelUsuarioActual = $db->query("select usuemail from usuarios where usuid = '" . $_SESSION['id'] . "';")->fetchColumn();

                    $mail->AddCC($emailDelUsuarioActual, $emailDelUsuarioActual);

                    if ($mail->Send()) {
                        $msg = ", y se envio correo notificando al capacitador. ";
                        header('Location:liscapacitacion.php?mensaje=' . $mensaje . $msg . '&' . $filtro);
                        $qry = $db->query("UPDATE capacitaciones SET capacorreo='1' WHERE capaid='" . $capacitaid . "' ;");
                    } else {
                        echo $msg = "No se pudo enviar el email por la siguiente causa: " . $mail->ErrorInfo;
                        die;
                    }
                    header('Location:liscapacitacion.php?mensaje=' . $mensaje . $msg . '&' . $filtro);

                }// end if
                else {

                    $mail = new PHPMailer();
                    $body = '
					<p>Buen Dia!</p>
					' . $mensaje_calidad . '
					<p>A continuacion se envia la informacion de la capacitacion a dictar</p>
					<p>CAPACITACION:  ' . $rowcapac['capanombre'] . '</p>
					<p>FECHA Y HORA:  ' . $rowcapac['capafecha'] . '</p>
					<p>EMPRESA:  ' . $rowcapac['pronombre'] . ' ' . $rowcapac['pronombrealternativo'] . '</p>
					<p>CONTACTO:  ' . $rowcapac['procontacto'] . ' ' . $rowcapac['procontactoalternativo'] . '</p>
					<p>DEPARTAMENTO - CIUDAD: ' . $rowdepart['depnombre'] . ' ' . $rowciuda['ciunombre'] . '</p>
					<p>DIRECCION:  ' . $rowcapac['prodireccion'] . ' ' . $rowcapac['probarrio'] . ' ' . $rowcapac['proindicaciones'] . '</p>
					<p>ASISTENTES:  ' . $rowcapac['capanumasistentes'] . ' posibles</p>
					<p>TELS:    ' . $rowcapac['protelefono1'] . ' ' . $rowcapac['protelefono2'] . ' ' . $rowcapac['procelular1'] . ' ' . $rowcapac['procelular2'] . '</p>
					<p>CORREO:  ' . $rowcapac['proemail1'] . ' ' . $rowcapac['proemail2'] . '</p>
					<p>COMENTARIOS RELACIONISTA:   ' . $rowcapac['capacomentarios'] . '</p>
					<p style="margin-bottom:30px;"><b style="color:red;"> Nota: Favor enviar diariamente los resultados de cada capacitacion, gracias; Exitos..!!! </b></p> 
					<p style="margin-bottom:40px;">Cualquier inquietud con gusto la atendere.</p>
					' . $recordatorio_staff;

                    $mail->IsSMTP(); // telling the class to use SMTP
                    $mail->Host = "smtp.gmail.com"; // SMTP server
                    $mail->SMTPDebug = 0;                     // enables SMTP debug information (for testing)
                    $mail->SMTPAuth = true;                  // enable SMTP authentication
                    $mail->SMTPSecure = "ssl";                 // sets the prefix to the servier


                    $mail->Host = $defaultHost;      // sets GMAIL as the SMTP server
                    $mail->Port = $defaultPort;                   // set the SMTP port for the GMAIL server
                    $mail->Username = $defaultUsername;  // GMAIL username
                    $mail->Password = $defaultPassword;            // GMAIL password

                    $mail->SetFrom($defaultUsername, 'Dpto Coordinacion');
                    $mail->Subject = $asunto;
                    $mail->MsgHTML(utf8_decode($body));
                    $address = $emailcapacitador;
                    $mail->AddAddress($address);
                    $mail->AddCC($emailcoach, $nombrecoach);
                    $mail->AddCC($emailobservador, $nombreobservador);
                    $emailDelUsuarioActual = $db->query("select usuemail from usuarios where usuid = '" . $_SESSION['id'] . "';")->fetchColumn();

                    $mail->AddCC($emailDelUsuarioActual, $emailDelUsuarioActual);
                    if ($mail->Send()) {
                        $msg = ", y se envio correo notificando al capacitador. ";
                        header('Location:liscapacitacion.php?mensaje=' . $mensaje . $msg . '&' . $filtro);
                        $qry = $db->query("UPDATE capacitaciones SET capacorreo='1' WHERE capaid='" . $capacitaid . "' ;");
                    } else {
                        $msg = "No se pudo enviar el email por la siguiente causa: " . $mail->ErrorInfo;
                        die;
                    }
                    header('Location:liscapacitacion.php?mensaje=' . $mensaje . $msg . '&' . $filtro);

                }//end else
            } else {
                $error = "Se modifico el prospecto correctamente pero recuerde que para enviar el correo de confirmacion es necesario seleccionar un asesor";
                header('Location:liscapacitacion.php?error=' . $error . '&' . $filtro);
            }

        } else {
            header('Location:liscapacitacion.php?mensaje=' . $mensaje . '&' . $filtro);
        }
    } else {
        $error = "No se pudo actualizar la capacitacion ";
        header('Location:liscapacitacion.php?error=' . $error . '&' . $filtro);
    }

}


$con = 'SELECT * FROM capacitaciones INNER JOIN prospectos ON proid = capaprospectoid INNER JOIN usuarios ON usuid = capausuario ';


$ord = ' ORDER BY capafecha desc';


if (!isset($_POST['show-all'])) {
    $sql = crearConsulta($con, $ord,
        array($idcapaci, "capaid = '$idcapaci'"),
        array($idpros, "proid = '$idpros'"),
        array($call, "capausuario = '$call'"),
        array($fecha1, "DATE(capafecha) BETWEEN '$fecha1' AND '$fecha2'"),
        array($asesor, "capacapacitador = '$asesor'"),
        array($estado, "capaestado = '$estado'"),
        array($departamento, "prodepartamento = '$departamento'"),
        array($ciudad, "prociudad = '$ciudad'"),
        array($modalidad, "usumodalidad = '$modalidad'"),
        array($fecha3, "DATE(capafechausuario) BETWEEN '$fecha3' AND '$fecha4'")
    );
} else {
    $sql = $con . ' ' . $ord;
}

$qry2 = $db->query($sql);


// Acciones de actualización
$desactivar = isset($_GET['desactivar']) ? $_GET['desactivar'] : 0;
$activar = isset($_GET['activar']) ? $_GET['activar'] : 0;
$aplazar = isset($_GET['aplazar']) ? $_GET['aplazar'] : 0;
$confirmar = isset($_GET['confirmar']) ? $_GET['confirmar'] : 0;
$reprogramar = isset($_GET['reprogramar']) ? $_GET['reprogramar'] : 0;
$eliminar = isset($_GET['eliminar_capacitacion']) ? $_GET['eliminar_capacitacion'] : 0;
$encuesta = isset($_GET['encuesta']) ? $_GET['encuesta'] : 0;

if ($activar == 1) {
    $capaid = $_GET['capaid'];
    $qry = $db->query("UPDATE capacitaciones SET capaestado = '1' WHERE capaid = " . $capaid . ";"); // update para indicar que la capacitacion a sido activada nuevamente
    $num = $qry->rowCount();
    if ($num == 1) {
        $mensaje = "Se ha activado correctamente la la capacitacion";
        header('Location:liscapacitacion.php?mensaje=' . $mensaje . '&' . $filtro);
    } else {
        $error = "No se ha podido activar la capacitacion por favor contactese con el administrador";
        header('Location:liscapacitacion.php?error=' . $error . '&' . $filtro);
    }
}

if ($desactivar == 1) {
    $capaid = $_GET['capaid'];
    $motcancelar = $_POST['motcancelar'];
    $qry = $db->query("UPDATE capacitaciones SET capaestado = '2', capacancelacion = " . $motcancelar . " WHERE capaid = " . $capaid . ";"); // update para indicar que la capacitacion a sido desactivada
    $num = $qry->rowCount();
    if ($num == 1) {
        $mensaje = "Se ha desactivado correctamente la la capacitacion";
        header('Location:liscapacitacion.php?capa=' . $capaid . '&mensaje=' . $mensaje . '&' . $filtro);
    } else {
        $error = "No se ha podido desactivar la capacitacion por favor contactese con el administrador";
        header('Location:liscapacitacion.php?capa=' . $capaid . '&error=' . $error . '&' . $filtro);
    }
}

if ($confirmar == 1) {
    $capaid = $_GET['capaid'];
    $qry = $db->query("UPDATE capacitaciones SET capaestado = '3' WHERE capaid = " . $capaid . ";"); // update para indicar que la capacitacion a sido confirmada
    $num = $qry->rowCount();
    if ($num == 1) {
        $mensaje = "Se ha confirmado la capacitacion";
        header('Location:liscapacitacion.php?mensaje=' . $mensaje . '&' . $filtro);
    } else {
        $error = "No se ha podido confirmar la capacitacion por favor contactese con el administrador";
        header('Location:liscapacitacion.php?error=' . $error . '&' . $filtro);
    }
}

if ($aplazar == 1) {
    $capaid = $_GET['capaid'];
    $motaplazar = $_POST['motaplazar'];
    $qry = $db->query("UPDATE capacitaciones SET capaestado = '4', capaaplarepro =" . $motaplazar . "  WHERE capaid = " . $capaid . ";"); // update para indicar que la capacitacion a sido aplazada
    $num = $qry->rowCount();
    if ($num == 1) {
        $mensaje = "Se ha aplazado correctamente la capacitacion, Recuerde modificarla ";
        header('Location:liscapacitacion.php?capa=' . $capaid . '&mensaje=' . $mensaje . '&' . $filtro);
    } else {
        $error = "No se ha podido aplazar la capacitacion por favor contactese con el administrador";
        header('Location:liscapacitacion.php?capa=' . $capaid . '&error=' . $error . '&' . $filtro);
    }
}

if ($encuesta == 1) {
    $capaid = $_GET['capaid'];

    $asistencia_total = $_POST['asistencia_total'];
    $puntualidad = $_POST['puntualidad'];
    $percepcion = $_POST['percepcion'];
    $utilidad = $_POST['utilidad'];
    $recomendar = $_POST['recomendar'];
    $comentarios = $_POST['comentarios'];

    $qry = $db->query("UPDATE capacitaciones SET capaencuesta='1', capaencu1='" . $asistencia_total . "', capaencu2='" . $puntualidad . "', capaencu3='" . $percepcion . "', capaencu4='" . $utilidad . "', capaencu5='" . $recomendar . "', capaencucomentarios='" . $comentarios . "', capaencuusuario='" . $_SESSION['id'] . "'  WHERE capaid = " . $capaid . ";"); // update para indicar que la capacitacion a se le evaluo la encuesta
    $num = $qry->rowCount();
    if ($num == 1) {
        $mensaje = "Se ha aplicado la encuesta a la capacitacion correctamente ";
        header('Location:liscapacitacion.php?capa=' . $capaid . '&mensaje=' . $mensaje . '&' . $filtro);
    } else {
        $error = "No se ha podido aplicar la encuesta a  la capacitacion por favor contactese con el administrador";
        header('Location:liscapacitacion.php?capa=' . $capaid . '&error=' . $error . '&' . $filtro);
    }
}

if ($reprogramar == 1) {
    $capaid = $_GET['capaid'];
    $motreprogramar = $_POST['motreprogramar'];
    $fecha2 = $_POST['fecha2'];
    $hora2 = $_POST['hora2'];
    $fecha_hora_capacitacion = $fecha2 . " " . $hora2;
    $qry = $db->query("UPDATE capacitaciones SET capaestado = '7', capaaplarepro =" . $motreprogramar . ", capafecha = '" . $fecha_hora_capacitacion . "'  WHERE capaid = " . $capaid . ";"); // update para indicar que la capacitacion a sido aplazada
    $num = $qry->rowCount();
    if ($num == 1) {
        $mensaje = "Se ha reprogramado correctamente la capacitacion";
        header('Location:liscapacitacion.php?capa=' . $capaid . '&mensaje=' . $mensaje . '&' . $filtro);
    } else {
        $error = "No se ha podido reprogramar la capacitacion por favor contactese con el administrador";
        header('Location:liscapacitacion.php?capa=' . $capaid . '&error=' . $error . '&' . $filtro);
    }
}

if ($eliminar == 1) {
    $capaid = $_GET['capaid'];
    $qry = $db->query("DELETE FROM capacitaciones WHERE capaid = '" . $capaid . "';"); // DELETE DE LA CAPACITACION
    $num = $qry->rowCount();
    if ($num == 1) {
        $mensaje = "Se ha eliminado exitosamente la capacitacion.";
        header('Location:liscapacitacion.php?mensaje=' . $mensaje . '&' . $filtro);
    } else {
        $error = "No se ha podido eliminar la capacitacion por favor contactese con el administrador";
        header('Location:liscapacitacion.php?error=' . $error . '&' . $filtro);
    }
}


if (isset($_GET['mensaje'])) {
    $mensaje = $_GET['mensaje'];
}
if (isset($_GET['error'])) {
    $error = $_GET['error'];
}

?>
<!doctype html>
<html>
<head>

    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>

    <link rel="stylesheet" type="text/css"
          href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css"/>
    <script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#tabla').dataTable({
                'bJQueryUI': true,
                'bStateSave': true,
                'bSort': false,
                'sPaginationType': 'full_numbers',
                'oLanguage': {
                    'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
                },
            });

            $('.msj_eliminar').click(function (e) {
                e.preventDefault();
                var targetUrl = $(this).attr('href');
                var $dialog_link_follow_confirm = $('<div></div>').html("<p><span class='ui-icon ui-icon-alert' style='float:left; margin:2px 7px 7px 0;'></span>Esta seguro que desea eliminar la capacitacion?</p>").dialog({
                        title: 'Porfavor confirmar',
                        buttons: {
                            'Si': function () {
                                window.location.href = targetUrl;
                            },
                            'No': function () {
                                $(this).dialog("close");
                            }
                        },
                        modal: true,
                        width: 'auto',
                        height: 'auto'
                    }
                );
                $dialog_link_follow_confirm.dialog("open");
            });

        });
    </script>
</head>
<body>
<?php require($r . 'incluir/src/login.php') ?>
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <?php require($r . 'incluir/src/menu.php') ?>
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a href="#">Call Center</a>
            <div class="mapa_div"></div>
            <a class="current">Capacitaciones</a>
        </article>
        <article id="contenido">
            <?php
            if ($uninegoci <> '') {
                $men_titu = ", Unicamente UNIDAD DE NEGOCIO";
            }

            if ($fecha1 <> '') {
                echo "<h2>Capacitaciones Agendadas para el intervalo $fecha1 - $fecha2 " . (isset($men_titu) ? $men_titu : "") . "  </h2>";
            } else {
                echo "<h2>Detalle de las capacitaciones " . (isset($men_titu) ? $men_titu : "") . "</h2>";
            }
            ?>
            <br/><br/>
            <div class="reporte">
                <a href="excelliscapacitacion.php?<?php echo $filtro ?>"><img
                            src="<?php echo $r ?>imagenes/iconos/excel.png" title="csv"/></a>
            </div>
            <table id="tabla">
                <thead>
                <tr>
                    <th title="Envio de correo"></th>
                    <th title="Eliminar Capacitacion"></th>
                    <th>Prospecto</th>
                    <th title="Estado de la Capacitacion"> E.C</th>
                    <th title="Capacitacion">Capacitacion</th>
                    <th>Fecha de Capacitacion</th>
                    <th title="Usuario">Usuario</th>
                    <th title="Numero de asistentes programados - Numero de asistentes real "></th>
                    <th title="Fecha de Gestion">F.G</th>
                    <th title="Departamento">Departamento</th>
                    <th title="Ciudad">Ciudad</th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php

                // Se deja esta varibale para setear si entrar a la opcion que muestra todas las acciones
                // sin importar el permiso
                $banderDejar = 1;

                while ($row2 = $qry2->fetch(PDO::FETCH_ASSOC)) {
                    $qryusuario = $db->query("SELECT * FROM usuarios WHERE usuid = '" . $row2['capausuario'] . "'");
                    $rowusuario = $qryusuario->fetch(PDO::FETCH_ASSOC);

                    $usuario = $rowusuario['usunombre'];


                    if ($row2['capaestado'] == 4) {
                        $qryapla = $db->query("SELECT * FROM aplarepro WHERE aplareproid = " . $row2['capaaplarepro'] . "");
                        $rowapla = $qryapla->fetch(PDO::FETCH_ASSOC);
                        $subestado = $rowapla['aplareprodescripcion'];
                    } elseif ($row2['capaestado'] == 2) {
                        $qrycancela = $db->query("SELECT * FROM cancelaciones WHERE canceid = " . $row2['capacancelacion'] . "");
                        $rowcancela = $qrycancela->fetch(PDO::FETCH_ASSOC);
                        $subestado = $rowcancela['cancedescripcion'];
                    } else {
                        $subestado = '';
                    }


                    if (!empty($row2['prodepartamento'])) {
                        $qrydepar = $db->query("SELECT * FROM departamentos WHERE depid = '" . $row2['prodepartamento'] . "';");
                        $rowdepar = $qrydepar->fetch(PDO::FETCH_ASSOC);
                        $numdepar = $qrydepar->rowCount();
                        if ($numdepar == 1) {
                            $departamento = $rowdepar['depnombre'];
                            $qryciu = $db->query("SELECT * FROM ciudades WHERE ciudepto = '" . $rowdepar['depid'] . "' AND ciuid = '" . $row2['prociudad'] . "' ;");
                            $rowciu = $qryciu->fetch(PDO::FETCH_ASSOC);
                            $numciu = $qryciu->rowCount();
                            if ($numciu == 1) {
                                $ciudad = $rowciu['ciunombre'];
                            } else {
                                $ciudad = "NONE";
                            }
                        } else {
                            $departamento = "NONE";
                            $ciudad = "NONE";
                        }
                    } else {
                        $departamento = "NONE";
                        $ciudad = "NONE";
                    }

                    $qrypermisos = $db->query("SELECT * FROM usuarios WHERE usuid = '" . $_SESSION['id'] . "';");
                    $rowpermisos = $qrypermisos->fetch(PDO::FETCH_ASSOC);
                    ?>
                    <tr>
                        <td <?php if ($row2['capacorreo'] == 1) echo "bgcolor='green'"; ?> ></td>

                        <?php if ($rowpermisos['usuid'] == 52765484 or $rowpermisos['usuid'] == 1110451657 or $rowpermisos['usuid'] == 1024540606 or $rowpermisos['usuid'] == 1018410939) {
                            echo "<td> <a href='liscapacitacion.php?capaid=" . $row2['capaid'] . "&eliminar_capacitacion=1&" . $filtro . "' class='msj_eliminar' title='Eliminar Capacitacion'><img src='" . $r . "imagenes/iconos/bin_closed.png' class='grayscale' /></a> </td>";
                        } else {
                            echo "<td><img src='" . $r . "imagenes/iconos/bin_closed.png' class='gray' title='PARA ELIMINAR CAPACITACIONES DIRIGASE AL DIRECTOR' /></td>";
                        } ?>

                        <td title="<?php echo $row2['proid'] . ' - ' . $row2['capaid'] ?>"><?php echo $row2['pronombre'] ?></td>
                        <td title="<?php echo $subestado ?>" align="center"> <?php if (($row2['capaestado']) == 1) {
                                echo 'ACTIVA';
                            } elseif (($row2['capaestado']) == 2) {
                                echo 'CANCELADA';
                            } elseif (($row2['capaestado']) == 3) {
                                echo 'CONFIRMADA';
                            } elseif (($row2['capaestado']) == 4) {
                                echo 'APLAZADA';
                            } elseif (($row2['capaestado']) == 5) {
                                echo 'REALIZADA';
                            } elseif (($row2['capaestado']) == 6) {
                                echo 'NO REALIZADA';
                            } elseif (($row2['capaestado']) == 7) {
                                echo 'REPROGRAMADA';
                            } ?> </td>
                        <td><?php echo $row2['capanombre'] ?>"</td>
                        <td align="center"><?php if (!empty($row2['capacapacitador'])) {
                                echo "<b>" . $row2['capafecha'] . "</b>";
                            } else {
                                echo $row2['capafecha'];
                            } ?></td>
                        <td align="center"><?php echo $usuario ?></td>
                        <td align="center"> <?php if ($row2['capaestado'] == 5) {
                                echo $row2['capanumasistentes'] . "P - " . $row2['capanumasistentesreal'] . "R";
                            } else {
                                echo "--";
                            } ?></td>
                        <td align="center"><img src="<?php echo $r ?>imagenes/iconos/date.png"
                                                title="<?php echo $row2['capafechausuario'] ?>"/></td>
                        <td align="center"><?php echo $departamento ?></td>
                        <td align="center"><?php echo $ciudad ?></td>
                        <?php
                        if ($rowpermisos['usuperfil'] == 3 and $_SESSION['id'] <> 52522766 and $_SESSION['id'] <> 1020790281 and $_SESSION['id'] <> 1020790281 and $_SESSION['id'] <> 80083610 and $_SESSION['id'] <> 77155629) {
                            echo "<td><img src='" . $r . "imagenes/iconos/lapiz.png' class='gray' /></td>";
                            echo "<td><img src='" . $r . "imagenes/iconos/date_go.png' class='gray' /></td>";
                        } else {
                            ?>
                            <td>
                                <a href="modcapacitacion.php?prospecto=<?php echo $row2['proid'] . '&idcapacitacion=' . $row2['capaid'] . '&' . $filtro ?>"
                                   title="Modificar Capacitacion"><img src="<?php echo $r ?>imagenes/iconos/lapiz.png"
                                                                       class="grayscale"/></a></td>
                            <td>
                                <a href="llamar.php?proid=<?php echo $row2['proid'] . '&agendado=1&hisproprospecto=' . $row2['proid'] . '&' . $filtro ?>"
                                   title="Gestionar Llamada"><img src="<?php echo $r ?>imagenes/iconos/date_go.png"
                                                                  class="grayscale"/></a></td>
                            <?php
                        }
                        if ($banderDejar = 1 or $rowpermisos['usudirectorcall'] == 1 or $_SESSION['id'] == 52522766 or $_SESSION['id'] == 1020790281 or $_SESSION['id'] == 1020790281) {
                            echo "<td><a href='motcancelacion.php?capaid=" . $row2['capaid'] . '&desactivar=1&' . $filtro . "' title='Cancelar Capacitacion'><img src='" . $r . "imagenes/iconos/cancel_.png' class='grayscale' /></a></td>";
                            echo "<td><a href='liscapacitacion.php?capaid=" . $row2['capaid'] . '&activar=1&' . $filtro . "' title='Activar Capacitacion'><img src='" . $r . "imagenes/iconos/accept.png' class='grayscale' /></a></td>";
                            echo "<td><a href='motaplazamiento.php?capaid=" . $row2['capaid'] . '&aplazar=1&' . $filtro . "' title='Aplazar Capacitacion'><img src='" . $r . "imagenes/iconos/error.png' class='grayscale' /></a></td>";
                            if ($row2['capaestado'] == 5 && $row2['capaencuesta'] <> 1) {
                                echo "<td><a href='encuesta.php?capaid=" . $row2['capaid'] . '&encuesta=1&' . $filtro . "' title='Encuesta de Capacitacion'><img src='" . $r . "imagenes/iconos/layout_edit.png' class='grayscale' /></a></td>";
                            } else {
                                echo "<td><img src='" . $r . "imagenes/iconos/layout_edit.png' class='gray' /></td>";
                            }
                            if ($row2['capaestado'] <> '2' && $row2['capaestado'] <> '4') {
                                echo "<td><a href='asistentescapa.php?capaid=" . $row2['capaid'] . '&asistentescapa=1&' . $filtro . "' title='Asistentes a la Capacitacion'><img src='" . $r . "imagenes/iconos/group.png' class='grayscale' /></a></td>";
                            } else {
                                echo "<td><img src='" . $r . "imagenes/iconos/group.png' class='gray' /></td>";
                            }
                            /*echo "<td><a href='motreprogramacion.php?capaid=".$row2['capaid'].'&reprogramar=1&'.$filtro."' title='Reprogramar Capacitacion'><img src='".$r."imagenes/iconos/date.png' class='grayscale' /></a></td>";*/
                            echo "<td><a href='liscapacitacion.php?capaid=" . $row2['capaid'] . '&confirmar=1&' . $filtro . "' title='Confirmar Capacitacion'><img src='" . $r . "imagenes/iconos/thumb_up.png' class='grayscale' /></a></td>";
                            if ($row2['capaestado'] == '3') {
                                echo "<td><a href='valicapacitacion.php?capaid=" . $row2['capaid'] . '&' . $filtro . "' title='Validar Capacitacion'><img src='" . $r . "imagenes/iconos/bullet_go.png' class='grayscale' /></a></td>";
                            } elseif ($row2['capaestado'] == '5' || $row2['capaestado'] == '6') {
                                echo "<td><a href='valicapacitacion_detalle.php?capaid=" . $row2['capaid'] . '&' . $filtro . "' title='Detalle de validacion de la  Capacitacion'><img src='" . $r . "imagenes/iconos/magnifier.png' class='grayscale' /></a></td>";
                            } else {
                                echo "<td><img src='" . $r . "imagenes/iconos/bullet_go.png' class='gray' /></td>";
                            }

                        } elseif ($rowpermisos['usuperfil'] == 3 and $_SESSION['id'] <> 52522766 and $_SESSION['id'] <> 1020790281 and $_SESSION['id'] <> 1020790281 and $_SESSION['id'] <> 80083610 and $_SESSION['id'] <> 77155629) {


                            echo "<td><img src='" . $r . "imagenes/iconos/cancel_.png' class='gray' /></td>";
                            echo "<td><img src='" . $r . "imagenes/iconos/accept.png' class='gray' /></td>";
                            echo "<td><a href='motaplazamiento.php?capaid=" . $row2['capaid'] . '&aplazar=1&' . $filtro . "' title='Aplazar Capacitacion'><img src='" . $r . "imagenes/iconos/error.png' class='grayscale' /></a></td>";
                            echo "<td><img src='" . $r . "imagenes/iconos/layout_edit.png' class='gray' /></td>";
                            if ($row2['capaestado'] <> '2' && $row2['capaestado'] <> '4') {
                                echo "<td><a href='asistentescapa.php?capaid=" . $row2['capaid'] . '&asistentescapa=1&' . $filtro . "' title='Asistentes a la Capacitacion'><img src='" . $r . "imagenes/iconos/group.png' class='grayscale' /></a></td>";
                            } else {
                                echo "<td><img src='" . $r . "imagenes/iconos/group.png' class='gray' /></td>";
                            }
                            echo "<td><img src='" . $r . "imagenes/iconos/thumb_up.png' class='gray' /></td>";
                            if ($row2['capaestado'] == '3') {
                                echo "<td><a href='valicapacitacion.php?capaid=" . $row2['capaid'] . '&' . $filtro . "' title='Validar Capacitacion'><img src='" . $r . "imagenes/iconos/bullet_go.png' class='grayscale' /></a></td>";
                            } elseif ($row2['capaestado'] == '5' || $row2['capaestado'] == '6') {
                                echo "<td><a href='valicapacitacion_detalle.php?capaid=" . $row2['capaid'] . '&' . $filtro . "' title='Detalle de validacion de la  Capacitacion'><img src='" . $r . "imagenes/iconos/magnifier.png' class='grayscale' /></a></td>";
                            } else {
                                echo "<td><img src='" . $r . "imagenes/iconos/bullet_go.png' class='gray' /></td>";
                            }

                        } else {
                            echo "<td><img src='" . $r . "imagenes/iconos/cancel_.png' class='gray' /></td>";
                            echo "<td><img src='" . $r . "imagenes/iconos/accept.png' class='gray' /></td>";
                            echo "<td><img src='" . $r . "imagenes/iconos/error.png' class='gray' /></td>";
                            if ($row2['capaestado'] == 5 && $row2['capaencuesta'] <> 1) {
                                echo "<td><a href='encuesta.php?capaid=" . $row2['capaid'] . '&encuesta=1&' . $filtro . "' title='Encuesta de Capacitacion'><img src='" . $r . "imagenes/iconos/layout_edit.png' class='grayscale' /></a></td>";
                            } else {
                                echo "<td><img src='" . $r . "imagenes/iconos/layout_edit.png' class='gray' /></td>";
                            }
                            echo "<td><img src='" . $r . "imagenes/iconos/group.png' class='gray' /></td>";
                            echo "<td><img src='" . $r . "imagenes/iconos/thumb_up.png' class='gray' /></td>";
                            echo "<td><a href='valicapacitacion_detalle.php?capaid=" . $row2['capaid'] . '&' . $filtro . "' title='Detalle de validacion de la  Capacitacion'><img src='" . $r . "imagenes/iconos/magnifier.png' class='grayscale' /></a></td>";

                        }
                        ?>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
            <p class="boton">
                <button type="button" class="btn btn-outline-primary btnatras"
                        onClick="carga(); location.href='capacitacion.php'">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-left-square-fill"
                         fill="currentColor"
                         xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                              d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm9.5 8.5a.5.5 0 0 0 0-1H5.707l2.147-2.146a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708-.708L5.707 8.5H11.5z"/>
                    </svg>
                    Atras
                </button> <!-- BOTON PARA VOLVER A CONSULTAR -->
            </p>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php'); ?>
</section>
<?php
if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>';
elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>';
?>
<div id="modal" style="display:none"></div>
<div id="dialog2" style="display:none"></div>
</body>
</html>