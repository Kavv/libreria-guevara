<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
?>
<!doctype html>
<html>
<head>

    < <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>
    <script type="text/javascript">

        function validarMostrarTodo() {
            if ($("#fecha1").hasClass('validate[required]')) {
                $("#fecha1").removeClass('validate[required]')
            }
            if ($("#fecha2").hasClass('validate[required]')) {
                $("#fecha2").removeClass('validate[required]')
            }
        }

        $(document).ready(function () {

            $("#fecha1").datepicker({
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true,
                maxDate: "+0D",
                onClose: function (selectedDate) {
                    $("#fecha2").datepicker("option", "minDate", selectedDate);
                }
            }).keypress(function (event) {
                event.preventDefault()
            });
            $("#fecha2").datepicker({
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true,
                maxDate: "+0D",
                onClose: function (selectedDate) {
                    $("#fecha1").datepicker("option", "maxDate", selectedDate);
                }
            }).keypress(function (event) {
                event.preventDefault()
            });
        });
    </script>
</head>
<body>
<?php require($r . 'incluir/src/login.php') ?>
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <?php require($r . 'incluir/src/menu.php') ?>
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a href="#">Call Center</a>
            <div class="mapa_div"></div>
            <a class="current">Consultar Grupos</a>
        </article>
        <article id="contenido">
            <form id="form" name="form" action="lisgrupos.php" method="post">
                <fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
                    <legend class="ui-widget ui-widget-header ui-corner-all">Consulta de Grupos</legend>
                    <p>
                        <label for="fechas">Fecha de creación:</label>
                        <label for="">Desde</label>
                        <input type="text" id="fecha1" name="fecha1"
                               class="fecha validate[required]"/>
                        <label for="">Hasta</label>
                        <input
                                type="text" id="fecha2" name="fecha2" class="fecha validate[required]"/>
                    </p>
                    <div class="row">
                        <div class="col-md-12 col-lg-6">
                            <button type="submit" class="btnconsulta btn btn-primary btn-block mt-2" name="consultar"
                                    value="Buscar">consultar
                            </button> <!-- BOTON CONSULTAR -->
                        </div>
                        <div class="col-md-12 col-lg-6">
                            <button onclick="validarMostrarTodo()" type="submit"
                                    class="btnconsulta btn btn-primary btn-block mt-2" name="show-all"
                                    value="Buscar">Mostrar Todo
                            </button>
                        </div>
                    </div>
                </fieldset>
            </form>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php'); ?>
</section>
</body>
</html>