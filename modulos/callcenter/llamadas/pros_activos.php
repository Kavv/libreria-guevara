<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

$sql = "SELECT * FROM prospectos 
INNER JOIN campana on procampana = campaid 
WHERE progestion = 1";

if (isset($_GET['id'])) {
    $id = $_GET['id'];
}

$normalizar = isset($_GET['normalizar_cliente']) ? $_GET['normalizar_cliente'] : 0;

$normalizar_todos = isset($_GET['normalizar_todos']) ? $_GET['normalizar_todos'] : 0;

if (isset($_GET['error'])) {

    $error = $_GET['error'];
}
if (isset($_GET['mensaje'])) {

    $mensaje = $_GET['mensaje'];
}

if ($normalizar == 1) {
    $qry = $db->query("UPDATE prospectos SET progestion = 0 WHERE proid = " . $id . " ;");
    if ($qry) {
        $mensaje = "Se ha Normalizado el cliente indicado";
        header('Location:pros_activos.php?mensaje=' . $mensaje . '');
    } else {
        $error = "Se presento un error y no se puedo normalizar el cliente";
        header('Location:pros_activos.php?error=' . $error . '');
    }
}

if ($normalizar_todos == 1) {
    $qry = $db->query("UPDATE prospectos SET progestion = 0 WHERE progestion = 1 ;");
    if ($qry) {
        $mensaje = "Se han Normalizado todos los clientes con gestion activa";
        header('Location:pros_activos.php?mensaje=' . $mensaje . '');
    } else {
        $error = "Se presento un error y no se pudo normalizar los clientes";
        header('Location:pros_activos.php?error=' . $error . '');
    }
}


$qry = $db->query($sql);


$qrypermisos = $db->query("SELECT * FROM usuarios WHERE usuid = '" . $_SESSION['id'] . "';");
$rowpermisos = $qrypermisos->fetch(PDO::FETCH_ASSOC);
?>
<!doctype html>
<html>
<head>
    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>

    <link rel="stylesheet" type="text/css"
          href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css"/>
    <script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#tabla').dataTable({
                'bJQueryUI': true,
                'bStateSave': true,
                'sPaginationType': 'full_numbers',
                'oLanguage': {
                    'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
                },
                'bSort': false,
            });
        });
    </script>
</head>
<body>
<?php require($r . 'incluir/src/login.php') ?>
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <?php require($r . 'incluir/src/menu.php') ?>
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a href="#">Call Center</a>
            <div class="mapa_div"></div>
            <a class="current">Prospectos activos</a>
        </article>
        <article id="contenido">
            <h2>Listado de Prospectos con gestion Activa</h2>
            <table id="tabla">
                <thead>
                <tr>
                    <th title="ID"></th>
                    <th align="center" title="Nombre de Prospecto">Nombre Prospecto</th>
                    <th align="center" title="Nombre Alternativo">Nombre Alternativo</th>
                    <th align="center" title="Nombre de Campana">Nombre Campaña</th>
                    <th>Tipo</th>
                    <th>Contacto</th>
                    <th>Contacto Alternativo</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php
                while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
                    ?>
                    <tr>
                        <td><?php echo $row['proid'] ?></td>
                        <td title="<?php echo $row['proid'] ?>"><?php echo $row['pronombre'] ?></td>
                        <td align="center"><?php echo $row['pronombrealternativo'] ?></td>
                        <td align="center"><?php echo $row['campanombre'] ?></td>
                        <td align="center"><?php if ($row['protipo'] == 1) echo "PERSONA"; else echo "EMPRESA"; ?></td>
                        <td align="center"><?php echo $row['procontacto'] ?></td>
                        <td align="center"><?php echo $row['procontactoalternativo'] ?></td>

                        <td align="center">
                            <?php
                            if ($rowpermisos['usuperfil'] == 98 or $rowpermisos['usuperfil'] == 30 or $rowpermisos['usudirectorcall'] == 1) {
                                ?>
                                <a href="pros_activos.php?id=<?php echo $row['proid'] . "&normalizar_cliente=1" ?>"><img
                                            title="Normalizar Cliente" src="<?php echo $r ?>imagenes/iconos/cog_go.png"
                                            class="grayscale"/></a>
                                <?php
                            } else echo '<img src="' . $r . 'imagenes/iconos/cog_go.png" class="gray" />';
                            ?>
                        </td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
            <?php if ($rowpermisos['usuperfil'] == 98 or $rowpermisos['usuperfil'] == 30 or $rowpermisos['usudirectorcall'] == 1) { ?>
                <p class="boton">
                    <a href="pros_activos.php?<?php echo "normalizar_todos=1" ?>">
                        <button type="submit" class="btn btn-primary mt-1" name="normalizar" value="normalizar">
                            Normalizar
                            Todos
                        </button>
                    </a> <!-- BOTON NORMALIZAR -->
                </p>
            <?php } ?>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php'); ?>
</section>
<?php
if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>';
elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>';
?>
<div id="modal" style="display:none"></div>
<div id="dialog2" style="display:none"></div>
</body>
</html>