<?php
// LA ACCION PRINCIPAL ES ESTA PAGINA ES CONSULTAR UNA UNA CAMPANA EN LA TABLA DE CAMPANAS DE LA BD
$r = '../../../';
//INCLUIR SESION Y CONECCION
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
?>
<!doctype html>
<html>
<head>

    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>
    <script type="text/javascript"
            src="<?php echo $r ?>incluir/jquery/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            $('#fecha1').datepicker({ // VALIDACION DE LA FECHA UNO
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true,
                maxDate: '+0D',
                onClose: function (selectedDate) {
                    $('#fecha2').datepicker('option', 'minDate', selectedDate);
                }
            }).keypress(function (event) {
                event.preventDefault()
            });
            $('#fecha2').datepicker({ // VALIDACION DE LA FECHA DOS
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true,
                maxDate: '+0D',
                onClose: function (selectedDate) {
                    $('#fecha1').datepicker('option', 'maxDate', selectedDate);
                }
            }).keypress(function (event) {
                event.preventDefault()
            });
        });
    </script>
</head>
<body>
<?php require($r . 'incluir/src/login.php') ?> <!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
    <?php require($r . 'incluir/src/menu.php') ?> <!-- INCLUIMOS MENU PRINCIPAL -->
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a href="#">Call Center</a>
            <div class="mapa_div"></div>
            <a class="current">Consultar Campaña</a>
        </article>
        <article id="contenido">
            <form id="form" name="form" action="listar_cam.php" method="post">
                <!-- ENVIO FORMULARIO POR POST A LISTAR.PHP -->
                <fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
                    <legend class="ui-widget ui-widget-header ui-corner-all">Busqueda de Campañas</legend>
                    <p>
                        <label for="nombre">Nombre:</label>
                        <input type="text" name="nombre" class="nombre validate[custom[onlyLetterSp]] text-input"
                               title="Digite el nombre del cliente"/>
                        <!-- CONSULTAR CLIENTE POR COINCIDENCIAS EN EL NOMBRE USANDO UN LIKE EN LA BD -->
                    </p>
                    <p>
                        <label for="estado">Estado:</label>
                        <select name="estado">
                            <option value="ACTIVA">ACTIVA</option>
                            <option value="">TODOS</option>
                            <option value="INACTIVA">INACTIVA</option>
                            <option value="FINALIZADA">FINALIZADA</option>
                        </select>
                    </p>
                    <p>
                        <label for="rango">Intervalo de cargue:</label> <!-- CAMPOS FECHA -->
                        <label for="">Desde</label>
                        <input type="text" id="fecha1" name="fecha1" class="fecha"/>
                        <label for="">Hasta</label>
                        <input type="text" id="fecha2"
                               name="fecha2"
                               class="fecha"/>
                    </p>
                    <div class="row">
                        <div class="col-md-12 col-lg-6">
                            <button type="submit" class="btnconsulta btn btn-primary btn-block mt-2" name="consultar"
                                    value="Buscar">consultar
                            </button> <!-- BOTON CONSULTAR -->
                        </div>
                        <div class="col-md-12 col-lg-6">
                            <button type="submit" class="btnconsulta btn btn-primary btn-block mt-2" name="show-all"
                                    value="Buscar">Mostrar Todo
                            </button>
                        </div>
                    </div>
                    </p>
                </fieldset>
            </form>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php'); ?> <!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
</section>
<?php
if (isset($_GET['error'])) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['error'] . '</div>'; // MENSAJE MODAL ERROR
?>
</body>
</html>