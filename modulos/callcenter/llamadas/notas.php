<?php
$r = '../../../';
require($r . 'incluir/connection.php');
$proid = isset($_GET['proid']) ? $_GET['proid'] : '';
$hisproprospecto = isset($_GET['hisproprospecto']) ? $_GET['hisproprospecto'] : '';

?>
<!doctype html>
<html>
<head>
    <link rel="stylesheet" type="text/css"
          href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css"/>
    <script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#tabla2').dataTable({
                'bJQueryUI': true,
                'bStateSave': true,
                'bFilter': false,
                'bLengthChange': false,
                'bSort': false,
                'sPaginationType': 'full_numbers',
                'oLanguage': {
                    'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
                },
            });
        });
    </script>
</head>
<body>
<table id="tabla2">
    <thead>
    <tr>
        <th>Fecha</th>
        <th>Usuario</th>
        <th>Nota</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $qry = $db->query("SELECT h.hisprofechahora, u.usunombre,h.hisproobservacion FROM hisprospectos h inner join usuarios u on u.usuid = h.hisprousuario WHERE h.hisproprospectos = '$hisproprospecto' ORDER BY hisprofechahora DESC");
    while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {

        ?>
        <tr>
            <td align="center"><?php echo $row2['hisprofechahora'] ?></td>
            <td align="center"><?php echo $row2['usunombre'] ?></td>
            <td align="center"><?php echo $row2['hisproobservacion'] ?></td>
        </tr>
        <?php
    }
    ?>
    </tbody>
</table>
</p>
</body>
</html>