<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');


if (isset($_POST['validar']) && $_POST['validar']) {

    $fecha2 = $_POST['fecha2'];
    $monto2 = $_POST['monto2'];
    $fecha3 = $_POST['fecha3'];
    $monto3 = $_POST['monto3'];
    $monto_total = $_POST['monto_total'];
    $mesdili = $_POST['mesdili'];
    $iden = $_POST['iden'];

    $qry = $db->query("UPDATE viaticos SET viaestado='ACTIVA', viamesdiligenciamiento='" . $mesdili . "', viafechavalor1='" . $fecha2 . "', viavalor1='" . $monto2 . "', viafechavalor2='" . $fecha3 . "', viavalor2='" . $monto3 . "' WHERE viaid='" . $iden . "'; ");
    if ($qry) {
        header('Location:finalizar_viaticos.php?iden=' . $iden . '');
    } else {
        $error = 'No se pudo actualizar la informacion intente nuevamente o contacte con el administrador del portal';
    }

}

$sql = "SELECT * FROM viaticos WHERE viafechavalor1 IS NULL or viafechavalor2 IS NULL ORDER BY viadiadiligencia asc"; //WHERE viafechavalor1 IS NULL or viavalor1 IS NULL or viafechavalor2 IS NULL or viavalor2 IS NULL


$qry = $db->query($sql);
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>

    <link rel="stylesheet" type="text/css"
          href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css"/>
    <script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#tabla').dataTable({
                'bJQueryUI': true,
                'bStateSave': true,
                'sPaginationType': 'full_numbers',
                'oLanguage': {
                    'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
                },
                'bSort': false,
            });

        });
    </script>
</head>
<body>
<?php require($r . 'incluir/src/login.php') ?>
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <?php require($r . 'incluir/src/menu.php') ?>
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a href="#">Call Center</a>
            <div class="mapa_div"></div>
            <a class="current">Consultar Viaticos</a>
        </article>
        <article id="contenido">
            <h2>Listado de Viaticos</h2>
            <table id="tabla">
                <thead>
                <tr>
                    <th align="center">Asesor</th>
                    <th align="center">Empresa</th>
                    <th align="center">Dia de Viaje Ida</th>
                    <th align="center">Dia de Viaje Regreso</th>
                    <th>Departamento</th>
                    <th>Ciudad</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php
                while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {

                    $qrydeparta = $db->query("SELECT * FROM departamentos where depid = " . $row['viadepdestino'] . "; ");
                    $rowdeparta = $qrydeparta->fetch(PDO::FETCH_ASSOC);
                    $departamento = $rowdeparta['depnombre'];

                    $qryciudad = $db->query("SELECT * FROM ciudades where ciudepto = " . $row['viadepdestino'] . " AND ciuid = " . $row['viaciudestino'] . " ");
                    $rowciudad = $qryciudad->fetch(PDO::FETCH_ASSOC);
                    $ciudad = $rowciudad['ciunombre'];

                    $qryasesor = $db->query("SELECT * FROM usuarios where usuid = '" . $row['viacapacitador'] . "'; ");
                    $rowasesor = $qryasesor->fetch(PDO::FETCH_ASSOC);
                    $asesor = $rowasesor['usunombre'];

                    $qryempresa = $db->query("SELECT * FROM empresas where empid = " . $row['viaempcapacitador'] . "; ");
                    $rowempresa = $qryempresa->fetch(PDO::FETCH_ASSOC);
                    $empresa = $rowempresa['empnombre'];


                    ?>
                    <tr>
                        <td><?php echo $asesor ?></td>
                        <td><?php echo $empresa ?></td>
                        <td align="center"><?php echo $row['viadiaviajeida'] ?></td>
                        <td align="center"><?php echo $row['viadiaviajeregreso'] ?></td>
                        <td align="center"><?php echo $departamento ?></td>
                        <td align="center"><?php echo $ciudad ?></td>
                        <td align="center">
                            <?php
                            $qrypermisos = $db->query("SELECT * FROM usuarios WHERE usuid = '" . $_SESSION['id'] . "';");
                            $rowpermisos = $qrypermisos->fetch(PDO::FETCH_ASSOC);

                            ?>
                            <a href="viaticos_valores.php?id=<?php echo $row['viaid'] ?>" title="Continuar"><img
                                        src="<?php echo $r ?>imagenes/iconos/tick.png" class="grayscale"/></a>
                            <?php

                            ?>
                        </td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php'); ?>
</section>
<?php
if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>';
elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>';
?>
<div id="modal" style="display:none"></div>
<div id="dialog2" style="display:none"></div>
</body>
</html>