<?php
// LA ACCION PRINCIPAL ES ESTA PAGINA ES EL MANEJO DE LLAMADAS PARA EL CALL CENTER
$r = '../../../';
//INCLUIR SESION Y CONECCION
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');


if (isset($_POST['numid'])) {
    $proid = $_POST['numid'];
}

if (isset($_GET['proid'])) {
    $proid = $_GET['proid'];
}

$hisproprospecto = isset($_GET['hisproprospecto']) ? $_GET['hisproprospecto'] : '';


if (isset($proid) && !empty($proid)) {

    $filtro = "proid=" . $proid . "&hisproprospecto=" . $hisproprospecto;

    $qry = $db->query("UPDATE prospectos SET progestion = '1' WHERE proid = '" . $proid . "';"); // update para indicar que el cliente se encuentra se encuntra activo por alguna relacionista

    if (empty($hisproprospecto)) {
        $sql = "SELECT * FROM prospectos WHERE proid = " . $proid . "; ";
    } else {
        $sql = "SELECT * FROM prospectos INNER JOIN hisprospectos ON proid = hisproprospectos WHERE proid = " . $proid . " AND hisproprospectos = " . $hisproprospecto . " GROUP BY hisproprospectos;";
    }

    $qry = $db->query($sql);

    if (isset($_POST['validar']) && $_POST['validar']) {
        $nombrealternativo = strtoupper(trim($_POST['nombrealternativo']));
        $barrio = strtoupper(trim($_POST['barrio']));
        $direccion = strtoupper(trim($_POST['direccion']));
        $indicaciones = strtoupper(trim($_POST['indicaciones']));
        $email1 = strtoupper(trim($_POST['email1']));
        $email2 = strtoupper(trim($_POST['email2']));
        $contacto = strtoupper(trim($_POST['contacto']));
        $contactoalternativo = strtoupper(trim($_POST['contactoalternativo']));

        $qryupd = $db->query("UPDATE prospectos SET pronombrealternativo = '" . $nombrealternativo . "', probarrio = '" . $barrio . "', prodireccion = '" . $direccion . "', proindicaciones = '" . $indicaciones . "', proemail1 = '" . $email1 . "' , proemail2 = '" . $email2 . "', protelefono2 = '" . $_POST['telefono2'] . "', procelular2 = '" . $_POST['celular2'] . "' , procontacto = '" . $contacto . "' , procontactoalternativo = '" . $contactoalternativo . "' WHERE proid = " . $proid . ";");

        $qryupdciudep = $db->query("UPDATE prospectos SET prodepartamento = '" . $_POST['departamento'] . "', prociudad = '" . $_POST['ciudad'] . "' WHERE proid = " . $proid . ";");

        $observaciones = strtoupper(trim($_POST['observaciones']));

        if (empty($_POST['contactabilidad'])) {
            $contactabilidad = 0;
        } else {
            $contactabilidad = $_POST['contactabilidad'];
        }
        if (empty($_POST['efectividad'])) {
            $efectividad = 0;
        } else {
            $efectividad = $_POST['efectividad'];
        }


        $qryhispro = $db->query("INSERT INTO hisprospectos (hisproprospectos, hisproresultadollamada, hisprocontactabilidad, hisproefectividad, hisproobservacion, hisprousuario, hisproip) VALUES (" . $proid . ", " . $_POST['resllamada'] . ", " . $contactabilidad . ", " . $efectividad . ", '" . $observaciones . "', '" . $_SESSION['id'] . "', '" . $_SERVER['REMOTE_ADDR'] . "');");


        if (!empty($_POST['fecha1'])) {
            $fecha_hora_llamada = $_POST['fecha1'] . " " . $_POST['hora'];
            $qry = $db->query("INSERT INTO agendadas (agenproid, agenfechahorallamar, agenusuario, agenestado) VALUES (" . $proid . ", '" . $fecha_hora_llamada . "', '" . $_SESSION['id'] . "', 'ACTIVO');");
        }

        //$qry = $db->query("UPDATE prospectos SET progestion = '0' WHERE proid = ".$proid.";"); // Update en progestion para identificar que es un cliente fuera de uso
        if ($_POST['efectividad'] == 2 or $_POST['efectividad'] == 16 or $_POST['efectividad'] == 18) {
            header('Location:agencapacitacion.php?prospecto=' . $proid . '');
        } elseif ($_POST['efectividad'] == 12 or $_POST['efectividad'] == 3) {
            header('Location:enviocorreo.php?prospecto=' . $proid . '');
        } else {
            header('Location:llamar.php');
        }
        exit();
    }
} else {
    if (isset($_GET['noClientes'])) {
        header('Location:noclientes.php');
    } else {
        header('Location:cargar_cliente.php'); // si por get no se recibe nada en la variable de $proid redirecciona al php encargado de buscar prospectos
    }
    exit();
}
?>
<!doctype html>
<html>

<head>

    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>
    <script type="text/javascript">
        function resulllama(valor) {
            if (valor == '15') {
                document.getElementById("contactabilidad").disabled = false;
                document.getElementById("divcontactabilidad").style.display = 'block';
            } else {
                $("#contactabilidad").prop("selectedIndex", 0);
                document.getElementById("contactabilidad").disabled = true;
                document.getElementById("divcontactabilidad").style.display = 'none';
                $("#efectividad").prop("selectedIndex", 0);
                document.getElementById("efectividad").disabled = true;
                document.getElementById("divefectividad").style.display = 'none';
                document.getElementById("divagendamiento").style.display = 'none';
                document.getElementById("fecha1").value = '';
                document.getElementById("hora").value();
            }
        }

        function contacta(valor) {
            if (valor == '1') {
                document.getElementById("efectividad").disabled = false;
                document.getElementById("divefectividad").style.display = 'block';
            } else {
                $("#efectividad").prop("selectedIndex", 0);
                document.getElementById("efectividad").disabled = true;
                document.getElementById("divefectividad").style.display = 'none';
                document.getElementById("divagendamiento").style.display = 'none';
                document.getElementById("fecha1").value = '';
                document.getElementById("hora").value();
            }
        }

        function agendamiento(valor) {
            if (valor == '1') {
                document.getElementById("divagendamiento").style.display = 'block';
            } else {
                document.getElementById("divagendamiento").style.display = 'none';
                document.getElementById("fecha1").value = '';
                document.getElementById("hora").value();
            }
        }


        $(document).ready(function () {


            $('#fecha1').datepicker({
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true,
                minDate: '0D',
            });

            $('#departamento').change(function (event) {
                var id1 = $('#departamento').find(':selected').val();
                $('#ciudad').load('<?php echo $r ?>incluir/carga/ciudades.php?id1=' + id1);
            });

            $('.historico').click(function () { // ACCION PARA CARASTERISTICO
                newHref = $(this).attr('data-rel');
                $('#dialog2').load(newHref).dialog({
                    modal: true,
                    width: 800
                });
            });
        });
    </script>
</head>

<body>
<?php require($r . 'incluir/src/login.php') ?>
<!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
    <?php require($r . 'incluir/src/menu.php') ?>
    <!-- INCLUIMOS MENU PRINCIPAL -->
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a href="#">Call Center</a>
            <div class="mapa_div"></div>
            <a class="current">Gestion de Llamadas</a>
        </article>
        <article id="contenido">
            <form id="form" name="form" action="llamar.php" method="post">
                <!-- ENVIO FORMULARIO POR POST -->
                <fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
                    <legend class="ui-widget ui-widget-header ui-corner-all">Gestion de Llamadas <img
                                src="<?php echo $r ?>imagenes/iconos/comments.png"
                                data-rel="notas.php?hisproprospecto=<?php echo $hisproprospecto . '&proid=' . $proid ?>"
                                class="historico" title="Visualizacion Historico"/></legend>
                    </br>
                    <?php
                    while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
                    ?>
                    <h2>Campaña
                        <?php
                        //$qrynombrecampa = $db->query("SELECT * FROM campana WHERE campaid = ".$row['procampana']."");
                        $qrynombrecampa = $db->query("SELECT * FROM campana WHERE campaid = " . $row['procampana']);

                        $rownombrecampa = $qrynombrecampa->fetch(PDO::FETCH_ASSOC);
                        echo isset($rownombrecampa['campanombre']) ? $rownombrecampa['campanombre'] : '';
                        ?>
                    </h2>
                    <fieldset class="ui-widget ui-widget-content ui-corner-all">
                        <legend class="ui-widget ui-widget-header ui-corner-all">Datos Personales del Cliente</legend>
                        <?php
                        $qrycampanas = $db->query("SELECT * FROM capacitaciones WHERE capaprospectoid = '" . $proid . "'");
                        while ($rowcampanas = $qrycampanas->fetch(PDO::FETCH_ASSOC)) {
                            $qryusuariocampana = $db->query("SELECT * FROM usuarios WHERE usuid = '" . $rowcampanas['capausuario'] . "'");
                            $rowusuariocampana = $qryusuariocampana->fetch(PDO::FETCH_ASSOC);
                            $usuarionombre = $rowusuariocampana['usunombre'];
                            echo "<br><p style='color:red;text-align:center;'> Recuerde que ya hay una capacitacion agendada de " . $rowcampanas['capanombre'] . " para el " . $rowcampanas['capafecha'] . " gestionada por " . $usuarionombre . " el dia " . $rowcampanas['capafechausuario'] . ". </p><br>";
                        }
                        ?>
                        <p>
                            <label for="numid">Num ID:</label> <!-- NUM IDENTIFICACION -->
                            <input type="text" name="numid" class="id" value="<?php echo $row['proid'] ?>" readonly/>
                        </p>
                        <p>
                            <label for="nombrecliente">Nombre del Cliente:</label>
                            <input type="text" style="width:76%" class="id" name="nombrecliente"
                                   value="<?php echo $row['pronombre'] ?>" readonly/> <!-- CAMPO NOMBRE CLIENTE -->
                        </p>
                        <p>
                            <label for="nombrealternativo">Nombre Alternativo:</label>
                            <input type="text" style="width:76%" class="id" name="nombrealternativo"
                                   value="<?php echo $row['pronombrealternativo'] ?>"/>
                            <!-- CAMPO NOMBRE ALTERNATIVO -->
                        </p>
                        <p>
                            <label for="tipocli">Tipo de cliente:</label>
                            <input type="text" align="center" name="tipocli" value="<?php if ($row['protipo'] == 1) {
                                echo 'PERSONA';
                            } elseif ($row['protipo'] == 2) {
                                echo 'EMPRESA';
                            } ?>" readonly/> <!-- CAMPO TIPO -->
                        </p>
                    </fieldset>
                    </br>
                    <fieldset class="ui-widget ui-widget-content ui-corner-all">
                        <legend class="ui-widget ui-widget-header ui-corner-all">Datos de Contacto</legend>
                        <p>
                            <label>Provincia:</label>
                            <select id="departamento" name="departamento">
                                <?php
                                if ($row['prodepartamento'] != '') {
                                    $row2 = $db->query("SELECT * FROM departamentos WHERE depid = '" . $row['prodepartamento'] . "'")->fetch(PDO::FETCH_ASSOC);
                                    echo '<option value=' . $row['prodepartamento'] . '>' . $row2['depnombre'] . '</option>';
                                }
                                echo '<option value="">SELECCIONE</option>';
                                $qry = $db->query("SELECT * FROM departamentos WHERE depid <> '" . $row['prodepartamento'] . "' ORDER BY depnombre");
                                while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
                                    echo '<option value=' . $row2['depid'] . '>' . $row2['depnombre'] . '</option>';
                                }
                                ?>
                            </select>
                            <label>Ciudad: </label>
                            <select name="ciudad" id="ciudad">
                                <?php
                                if ($row['prociudad'] != '') {
                                    $row2 = $db->query("SELECT * FROM ciudades WHERE ciudepto = '" . $row['prodepartamento'] . "' AND ciuid = '" . $row['prociudad'] . "'")->fetch(PDO::FETCH_ASSOC);
                                    echo '<option value=' . $row['prociudad'] . '>' . $row2['ciunombre'] . '</option>';
                                }
                                echo '<option value="">SELECCIONE</option>';
                                $qry = $db->query("SELECT * FROM ciudades WHERE ciudepto = '" . $row['prodepartamento'] . "' AND ciuid <> '" . $row['prociudad'] . "' ORDER BY ciunombre");
                                while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
                                    echo '<option value=' . $row2['depid'] . '>' . $row2['depnombre'] . '</option>';
                                }
                                ?>
                            </select>
                        </p>
                        <p>
                            <label>Barrio: </label>
                            <input type="text" name="barrio" value="<?php echo $row['probarrio'] ?>"/>
                        </p>
                        <p>
                            <label>Direccion: </label>
                            <input type="text" name="direccion" style="width:76%" class="direccion"
                                   value="<?php echo $row['prodireccion'] ?>"/>
                        </p>
                        <p>
                            <label>Indicaciones: </label>
                            <textarea name="indicaciones" rows="5" class="form-control text-area"
                                      cols="70"><?php echo $row['proindicaciones'] ?></textarea>
                        </p>
                        <p>
                            <label>E-mail 1: </label>
                            <input type="text" name="email1" class="email" value="<?php echo $row['proemail1'] ?>"/>
                            <label>E-mail 2: </label>
                            <input type="text" name="email2" class="email" value="<?php echo $row['proemail2'] ?>"/>
                        </p>
                        <p>
                            <label>Telefono 1: </label>
                            <input type="text" name="telefono1" class="telefono"
                                   value="<?php echo $row['protelefono1'] ?>" title="Numero Telefono 1" readonly/>
                            <label>Telefono 2: </label>
                            <input type="text" name="telefono2" class="telefono"
                                   value="<?php echo $row['protelefono2'] ?>" title="Numero Telefono 2"/>
                        </p>
                        <p>
                            <label>Celular 1: </label>
                            <input type="text" name="celular1" class="telefono"
                                   value="<?php echo $row['procelular1'] ?>" title="Numero Celular 1" readonly/>
                            <label>Celular 2: </label>
                            <input type="text" name="celular2" class="telefono"
                                   value="<?php echo $row['procelular2'] ?>" title="Numero Celular 2"/>
                        </p>
                        <p>
                            <label>Contacto: </label>
                            <input type="text" name="contacto" style="width:65%;"
                                   value="<?php echo $row['procontacto'] ?>"/>
                        </p>
                        <p>
                            <label>Contacto Alternativo: </label>
                            <input type="text" name="contactoalternativo" style="width:65%;"
                                   value="<?php echo $row['procontactoalternativo'] ?>"/>
                        </p>

                        <?php } ?>
                    </fieldset>
                    </br>
                    <fieldset class="ui-widget ui-widget-content ui-corner-all">
                        <legend class="ui-widget ui-widget-header ui-corner-all">Observaciones</legend>
                        <textarea name="observaciones" rows="5" cols="100"
                                  class="form-control validate[required] text-input"></textarea>
                        </br></br>
                        <center>
                            <div id="divresllamada">
                                <p>
                                    <label style="width:120px;">Resultado de la Llamada:</label>
                                    <select name="resllamada" id="resllamada" onChange="resulllama(this.value);"
                                            class="validate[required] text-input">
                                        <option value=""> SELECCIONE</option>
                                        <?php
                                        $qry = $db->query("SELECT * FROM resultadollamada WHERE resllamestado = 1 ORDER BY resllamdescripcion");
                                        while ($row3 = $qry->fetch(PDO::FETCH_ASSOC)) {
                                            echo '<option value="' . $row3["resllamid"] . '">' . $row3['resllamdescripcion'] . '</option>';
                                        }
                                        ?>
                                    </select>
                                </p>
                            </div>
                            <div id="divcontactabilidad" style="display:none;">
                                <p>
                                    <label style="width:120px;">Contactabilidad:</label>
                                    <select name="contactabilidad" id="contactabilidad" onChange="contacta(this.value);"
                                            class="validate[required] text-input" disabled>
                                        <option value=""> SELECCIONE</option>
                                        <?php
                                        $qry = $db->query("SELECT * FROM contactabilidad WHERE conestado = 1 ORDER BY condescripcion");
                                        while ($row3 = $qry->fetch(PDO::FETCH_ASSOC)) {
                                            echo '<option value="' . $row3["conid"] . '">' . $row3['condescripcion'] . '</option>';
                                        }
                                        ?>
                                    </select>
                                </p>
                            </div>
                            <div id="divefectividad" style="display:none;">
                                <p>
                                    <label style="width:120px;">Efectividad:</label>
                                    <select name="efectividad" id="efectividad" onChange="agendamiento(this.value);"
                                            class="validate[required] text-input" disabled>
                                        <option value=""> SELECCIONE</option>
                                        <?php
                                        $qry = $db->query("SELECT * FROM efectividad WHERE efeestado = 1 ORDER BY efedescripcion");
                                        while ($row3 = $qry->fetch(PDO::FETCH_ASSOC)) {
                                            echo '<option value="' . $row3["efeid"] . '">' . $row3['efedescripcion'] . '</option>';
                                        }
                                        ?>
                                    </select>
                                </p>
                            </div>
                            <div id="divagendamiento" style="display:none;">
                                <p>
                                    <label style="width:250px;" for="fecha1"> Fecha de Llamada Agendada: </label>
                                    <input type="text" name="fecha1" id="fecha1"
                                           class="validate[required] text-input fecha"/>
                                </p>
                                <p>
                                    <label style="width:250px;" for="hora"> Hora de llamada: </label>
                                    <input type="time" name="hora" value="" max="23:59:59" min="00:00:01"
                                           class="validate[required]" step="1">
                                </p>
                            </div>
                        </center>
                    </fieldset>
                    <p class="boton">
                        <button type="submit" class="btn btn-primary mt-1" name="validar" value="validar">Validar
                        </button>
                        <!-- BOTON VALIDAR -->
                    </p>
                </fieldset>
            </form>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php');
    ?>
    <!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
</section>
<?php
if (isset($_GET['error'])) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['error'] . '</div>'; // MENSAJE MODAL ERROR
if (isset($_GET['exitoso'])) echo '<div id="dialog-message" title="Exitoso"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['exitoso'] . '</div>'; // MENSAJE MODAL EXITOSO
?>
<div id="dialog2" title="Historico de Llamadas" style="display:none"></div>
</body>

</html>