<?php
$r = '../../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
require($r . 'incluir/funciones.php');
require($r . 'incluir/phpexcel/Classes/PHPExcel.php');

$fecha1 = $_GET['fecha1'];
$fecha2 = $_GET['fecha2'];
$gerente = $_GET['gerente'];

$ide = $_SESSION['id'];
$rowusuario = $db->query("SELECT * FROM usuarios INNER JOIN perfiles ON usuperfil = perid WHERE usuid = '" . $ide . "';")->fetch(PDO::FETCH_ASSOC);
$perfil = $rowusuario['usuperfil'];


$titulo = "REPORTE INTEGRAL $fecha1 - $fecha2  ";


$objPHPExcel = new PHPExcel();
$styleArray = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN
        )
    )
);
$objPHPExcel->getActiveSheet()->getStyle('A1:M2')->applyFromArray($styleArray);
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:M1')
    ->setCellValue('A1', $titulo);
$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A2', 'IDENTIFICACION DEL USUARIO')
    ->setCellValue('B2', 'USUARIO')
    ->setCellValue('C2', 'ESTADO USUARIO')
    ->setCellValue('D2', 'GERENTE CONTAC CENTER')
    ->setCellValue('E2', 'TIEMPO EN CONEXION')
    ->setCellValue('F2', 'REGISTROS BARRIDOS')
    ->setCellValue('G2', 'AGENDAS NUEVAS')
    ->setCellValue('H2', 'AGENDAS REALIZADAS')
    ->setCellValue('J2', 'CANTIDAD DE PEDIDOS')
    ->setCellValue('K2', 'SUMA DE FACTURAS')
    ->setCellValue('L2', 'CANTIDAD DE FACTURAS');
$i = 3;


$consultaSql = "select u.usuid, " .
    "       u.usunombre, " .
    "       case " .
    "           when uJefe.usuperfil = 56 then 'INACTIVO' " .
    "           when (isnull(uJefe.usufechaingreso) or uJefe.usufechaingreso = '') then 'SIN FECHA DE INGRESO' " .
    "           when datediff(uJefe.usufechaingreso, now()) >= 60 then 'ANTIGUO' " .
    "           else 'NUEVO' end               EstadoRelacionista, " .
    "       ifnull(uJefe.usunombre, 'NONE')    Jefe, " .
    "       (select count(*) " .
    "        from hisprospectos " .
    "        where hisprousuario = u.usuid " .
    (empty($fecha1) ? "" : "and DATE(hisprofechahora) between '$fecha1' and '$fecha2'") .
    ") as Gestiones, " .
    "       sum(if(c.capaestado = 3, 1, 0)) as Confirmado, " .
    "       sum(if(c.capaestado = 5, 1, 0)) as Realizado, " .
    "       (select count(*) " .
    "        from solicitudes " .
    "        where solrelacionista = u.usuid " .
    (empty($fecha1) ? "" : "and DATE(solfecha) between '$fecha1' and '$fecha2'") .
    ")  CantidadDePedidos, " .
    "       (SELECT ifnull(sum(soltotal), 0) " .
    "        FROM solicitudes " .
    "                 INNER JOIN movimientos ON (solempresa = movempresa AND solfactura = movnumero) " .
    "                 INNER JOIN clientes ON solcliente = cliid " .
    "        where movprefijo = 'FV' " .
    "          AND movestado = 'FACTURADO' " .
    "          and solrelacionista = u.usuid " .
    (empty($fecha1) ? "" : "and DATE(solfecha) between '$fecha1' and '$fecha2'") .
    ")  SumaDeFacturas, " .
    "       (SELECT count(*) " .
    "        FROM solicitudes " .
    "                 INNER JOIN movimientos ON (solempresa = movempresa AND solfactura = movnumero) " .
    "                 INNER JOIN clientes ON solcliente = cliid " .
    "        where movprefijo = 'FV' " .
    "          AND movestado = 'FACTURADO' " .
    "          and solrelacionista = u.usuid " .
    (empty($fecha1) ? "" : "and DATE(solfecha) between '$fecha1' and '$fecha2'") .
    ")  CantidadDeFacturas " .
    "from usuarios u " .
    "         inner join capacitaciones c on c.capausuario = u.usuid " .
    "         left join usuarios uJefe on u.usujefecallcenter = uJefe.usuid " .
    "where (select count(*) from hisprospectos where hisprousuario = u.usuid) > 0 " .
    "  and u.usucallcenter = 1 " .
    (empty($gerente) ? "" : "and u.usuid = '$gerente'") .
    (empty($fecha1) ? "" : "and DATE(c.capafechausuario) between '$fecha1' and '$fecha2'") .
    "group by u.usuid, u.usunombre, uJefe.usuperfil, uJefe.usufechaingreso; ";


$reporte = $db->query($consultaSql);

$dias = fechaDif($fecha1, $fecha2);


while ($row = $reporte->fetch(PDO::FETCH_ASSOC)) {


    $objPHPExcel->getActiveSheet()->getStyle('A' . $i . ':M' . $i)->applyFromArray($styleArray);
    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A' . $i, $row['usuid'])
        ->setCellValue('B' . $i, $row['usunombre'])
        ->setCellValue('C' . $i, $row['EstadoRelacionista'])
        ->setCellValue('D' . $i, $row['Jefe'])
        ->setCellValue('E' . $i, $dias)
        ->setCellValue('F' . $i, $row['Gestiones'])
        ->setCellValue('G' . $i, $row['Confirmado'])
        ->setCellValue('H' . $i, $row['Realizado'])
        ->setCellValue('I' . $i, $row['CantidadDePedidos'])
        ->setCellValue('J' . $i, number_format($row['SumaDeFacturas'], 0, ',', '.'))
        ->setCellValue('K' . $i, number_format($row['CantidadDeFacturas'], 0, ',', '.'));
    $i++;


}
// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Reporte Integral.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
?>