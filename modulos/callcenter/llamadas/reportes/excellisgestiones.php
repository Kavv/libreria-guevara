<?php
$r = '../../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
require($r.'incluir/phpexcel/Classes/PHPExcel.php'); 

$fecha1 = $_GET['fecha1'];
$fecha2 = $_GET['fecha2'];
$call = $_GET['call'];


$filtro = 'call='.$call.'&fecha1='.$fecha1.'&fecha2='.$fecha2;
if($call == '') $qry = $db->query("SELECT * FROM usuarios WHERE usucallcenter = '1'");
else $qry = $db->query("SELECT * FROM usuarios WHERE usuid = '$call'");

while($row = $qry->fetch(PDO::FETCH_ASSOC)){
	$qry2 = $db->query("SELECT * FROM hisprospectos 
	INNER JOIN prospectos ON proid =  hisproprospectos
	LEFT JOIN resultadollamada ON resllamid = hisproresultadollamada 
	LEFT JOIN contactabilidad ON conid = hisprocontactabilidad 
	LEFT JOIN efectividad ON efeid = hisproefectividad 	
	LEFT JOIN capacitaciones ON capaprospectoid = proid 
	LEFT JOIN uninegocio ON uniid = capauninegocio
	LEFT JOIN aplarepro ON aplareproid = capaaplarepro
	LEFT JOIN cancelaciones ON canceid = capacancelacion
	LEFT JOIN campana ON campaid = procampana
	WHERE DATE(hisprofechahora) BETWEEN '$fecha1' AND '$fecha2' AND hisprousuario = '".$row['usuid']."' ");
	$num2 = $qry2->rowCount();
	
$titulo = 'LISTADO DE GESTIONES DEL '.$fecha1.' AL '.$fecha2.' '.$row['usunombre'];

$objPHPExcel = new PHPExcel();
$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN
		)
	)
);
$objPHPExcel->getActiveSheet()->getStyle('A1:AM2')->applyFromArray($styleArray);
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:AM1')
			->setCellValue('A1', $titulo);
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A2', 'ID PROSPECTO')
			->setCellValue('B2', 'NOMBRE')	
            ->setCellValue('C2', 'NOMBRE ALTERNATIVO')
            ->setCellValue('D2', 'CAMPAÑA')
            ->setCellValue('E2', 'DEPARTAMENTO')
			->setCellValue('F2', 'CIUDAD')	
			->setCellValue('G2', 'DIRECCION')
			->setCellValue('H2', 'TEL 1')	
			->setCellValue('I2', 'TEL 2')	
			->setCellValue('J2', 'CEL 1')	
			->setCellValue('K2', 'CEL 2')	
            ->setCellValue('L2', 'EMAIL 1')
            ->setCellValue('M2', 'EMAIL 2')
            ->setCellValue('N2', 'CONTACTO')
			->setCellValue('O2', 'CEDULA USUARIO GESTIONO')	
			->setCellValue('P2', 'USUARIO GESTIONO')
			->setCellValue('Q2', 'GERENTE CONTACT CENTER')
			->setCellValue('R2', 'MES Y AÑO EN QUE GESTIONO')
			->setCellValue('S2', 'DIA EN QUE GESTIONO')			
			->setCellValue('T2', 'FECHA GESTION')	
			->setCellValue('U2', 'HORA GESTION')
			->setCellValue('V2', 'RESULTADO LLAMADA')	
            ->setCellValue('W2', 'CONTACTABILIDAD')
            ->setCellValue('X2', 'EFECTIVIDAD')
            ->setCellValue('Y2', 'UNIDAD DE NEGOCIO')
			->setCellValue('Z2', 'NOMBRE DE LA CAPACITACION')	
			->setCellValue('AA2', 'FECHA OBJETIVO CAPACITACION')
			->setCellValue('AB2', 'HORA OBJETIVO CAPACITACION')	
			->setCellValue('AC2', 'NOMBRE CAPACITADOR')	
			->setCellValue('AD2', 'GERENTE CAPACITADOR')
			->setCellValue('AE2', 'ESTADO CAPACITACION')	
            ->setCellValue('AF2', 'MOTIVO APLAZAMIENTO REPROGRAMACION')
            ->setCellValue('AG2', 'MOTIVO CANCELACION')
            ->setCellValue('AH2', 'NUMERO DE ASISTENTES PROGRAMADOS')
			->setCellValue('AI2', 'NUMERO DE ASISTENTES REALES')	
			->setCellValue('AJ2', 'NUMERO DE PEDIDOS')
			->setCellValue('AK2', 'TIPO DE PRODUCTO')	
			->setCellValue('AL2', 'VALOR PRODUCTO')	
			->setCellValue('AM2', 'OBSERVACION');				
$i = 3;
while($row2 = $qry2->fetch(PDO::FETCH_ASSOC)){
	
	if (!empty($row2['prodepartamento'])){
	$qrydepar = $db->query("SELECT * FROM departamentos WHERE depid = '".$row2['prodepartamento']."';");
	$rowdepar = $qrydepar->fetch(PDO::FETCH_ASSOC);
	$numdepar = $qrydepar->rowCount();
		if ($numdepar == 1){
		$departamento = $rowdepar['depnombre'];
		$qryciu = $db->query("SELECT * FROM ciudades WHERE ciudepto = '".$rowdepar['depid']."' AND ciuid = '".$row2['prociudad']."' ;");
		$rowciu = $qryciu->fetch(PDO::FETCH_ASSOC);
		$numciu = $qryciu->rowCount();
			if ($numciu == 1){
			$ciudad = $rowciu['ciunombre'];
			} else {
			$ciudad = "NONE";
			} 
		} else {
		$departamento = "NONE";
		$ciudad = "NONE";
		}
	} else {
	$departamento = "NONE";
	$ciudad = "NONE";
	}
	

	
	
	if (!empty($row2['hisprousuario'])){
		$qryrela = $db->query("SELECT * FROM usuarios WHERE usuid = '".$row2['hisprousuario']."';");
		$rowrela = $qryrela->fetch(PDO::FETCH_ASSOC);
		$numrela = $qryrela->rowCount();
		if ($numrela == 1){
		$relacionista = $rowrela['usunombre'];
			if (!empty($rowrela['usujefecallcenter'])){
				$qryjeferela = $db->query("SELECT * FROM usuarios WHERE usuid = '".$rowrela['usujefecallcenter']."';");
				$rowjeferela = $qryjeferela->fetch(PDO::FETCH_ASSOC);
				$numjeferela = $qryjeferela->rowCount();
				if ($numjeferela == 1){
					$jeferelacionista = $rowjeferela['usunombre'];
				} else {
					$jeferelacionista = "NONE";	
				}
			} else {
			$jeferelacionista = "NONE";	
			}
		} else {
		$relacionista = "NONE";
		$jeferelacionista = "NONE";
		}
	} else {
		$relacionista = "NONE";
		$jeferelacionista = "NONE";
	}
	
	
	
	if (!empty($row2['capacapacitador'])){
		$qryase = $db->query("SELECT * FROM usuarios WHERE usuid = '".$row2['capacapacitador']."';");
		$rowase = $qryase->fetch(PDO::FETCH_ASSOC);
		$numase = $qryase->rowCount();
		if ($numase == 1){
		$asesor = $rowase['usunombre'];
			if (!empty($rowase['usujefeasesor'])){
				$qryjefease = $db->query("SELECT * FROM usuarios WHERE usuid = '".$rowase['usujefeasesor']."';");
				$rowjefease = $qryjefease->fetch(PDO::FETCH_ASSOC);
				$numjefease = $qryjefease->rowCount();
				if ($numjefease == 1){
					$jefeasesor = $rowjefease['usunombre'];
				} else {
					$jefeasesor = "NONE";	
				}
			} else {
			$jefeasesor = "NONE";	
			}
		} else {
		$asesor = "NONE";
		$jefeasesor = "NONE";
		}
	} else {
		$asesor = "NONE";
		$jefeasesor = "NONE";
	}
	
	if (($row2['capaestado']) == 1) {$estado = 'ACTIVA';} elseif (($row2['capaestado']) == 2){$estado = 'CANCELADA';} elseif (($row2['capaestado']) == 3){$estado = 'CONFIRMADA';} elseif (($row2['capaestado']) == 4){$estado = 'APLAZADA';} elseif (($row2['capaestado']) == 5){$estado = 'REALIZADA';} elseif (($row2['capaestado']) == 6){$estado = 'NO REALIZADA';} elseif (($row['capaestado']) == 7){$estado = 'REPROGRAMADA';} else {$estado = 'NONE';}	

	$fecha1dia = date_create($row2['hisprofechahora']);
	$fecha11dia = date_format($fecha1dia,"d");
	
	$fecha1mes = date_create($row2['hisprofechahora']);
	$fecha11mes = date_format($fecha1mes,"m");
	
	$fecha1ano = date_create($row2['hisprofechahora']);
	$fecha11ano = date_format($fecha1ano,"Y");
	
	if ($fecha11mes == '1') { $mesfecha1 = "ENERO"; }
	if ($fecha11mes == '2') { $mesfecha1 = "FEBRERO"; }
	if ($fecha11mes == '3') { $mesfecha1 = "MARZO"; }
	if ($fecha11mes == '4') { $mesfecha1 = "ABRIL"; }
	if ($fecha11mes == '5') { $mesfecha1 = "MAYO"; }
	if ($fecha11mes == '6') { $mesfecha1 = "JUNIO"; }
	if ($fecha11mes == '7') { $mesfecha1 = "JULIO"; }
	if ($fecha11mes == '8') { $mesfecha1 = "AGOSTO"; }
	if ($fecha11mes == '9') { $mesfecha1 = "SEPTIEMBRE"; }
	if ($fecha11mes == '10') { $mesfecha1 = "OCTUBRE"; }
	if ($fecha11mes == '11') { $mesfecha1 = "NOVIEMBRE"; }
	if ($fecha11mes == '12') { $mesfecha1 = "DICIEMBRE"; }
	
	
	$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':AM'.$i)->applyFromArray($styleArray);
	$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('A'.$i, $row2['proid'])
    	->setCellValue('B'.$i, $row2['pronombre'])
    	->setCellValue('C'.$i, $row2['pronombrealternativo'])
		->setCellValue('D'.$i, $row2['campanombre'])
    	->setCellValue('E'.$i, $departamento)
    	->setCellValue('F'.$i, $ciudad)
		->setCellValue('G'.$i, $row2['prodireccion'])
    	->setCellValue('H'.$i, $row2['protelefono1'])
		->setCellValue('I'.$i, $row2['protelefono2'])
		->setCellValue('J'.$i, $row2['procelular1'])
		->setCellValue('K'.$i, $row2['procelular2'])
		->setCellValue('L'.$i, $row2['proemail1'])
		->setCellValue('M'.$i, $row2['proemail2'])
		->setCellValue('N'.$i, $row2['procontacto'])
		->setCellValue('O'.$i, $row2['hisprousuario'])
		->setCellValue('P'.$i, $relacionista)
		->setCellValue('Q'.$i, $jeferelacionista)
		->setCellValue('R'.$i, $mesfecha1.' '.$fecha11ano)
		->setCellValue('S'.$i, $fecha11dia)
		->setCellValue('T'.$i, substr($row2['hisprofechahora'], 0, 10))
		->setCellValue('U'.$i, substr($row2['hisprofechahora'], 10, 20))
		->setCellValue('V'.$i, $row2['resllamdescripcion'])
		->setCellValue('W'.$i, $row2['condescripcion'])
		->setCellValue('X'.$i, $row2['efedescripcion'])
		->setCellValue('Y'.$i, $row2['unidescripcion'])
		->setCellValue('Z'.$i, $row2['capanombre'])
		->setCellValue('AA'.$i, substr($row2['capafecha'], 0, 10))
		->setCellValue('AB'.$i, substr($row2['capafecha'], 10, 20))
		->setCellValue('AC'.$i, $asesor)
		->setCellValue('AD'.$i, $jefeasesor)
		->setCellValue('AE'.$i, $estado)
		->setCellValue('AF'.$i, $row2['aplareprodescripcion'])
		->setCellValue('AG'.$i, $row2['cancedescripcion'])
		->setCellValue('AH'.$i, $row2['capanumasistentes'])
		->setCellValue('AI'.$i, $row2['capanumasistentesreal'])
		->setCellValue('AJ'.$i, $row2['capapedidos'])
		->setCellValue('AK'.$i, $producto)
		->setCellValue('AL'.$i, $valorproducto)
		->setCellValue('AM'.$i, $row2['hisproobservacion']);
	$i++;
}
}
// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Listado Gestiones.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
?>