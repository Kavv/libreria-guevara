<?php
$r = '../../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
require($r . 'incluir/funciones.php');

if (isset($_GET['error'])) {
    $error = $_GET['error'];
}
if (isset($_GET['mensaje'])) {
    $mensaje = $_GET['mensaje'];
}

$fecha1 = $_GET['fecha1'];
$fecha2 = $_GET['fecha2'];
$gerente = $_GET['gerente'];

if (!isset($_GET['show-all'])) {
    if (empty($fecha1) || empty($fecha2) || empty($gerente)) {
        $error = "Use mostrar todos o llene los campos requeridos";
        header('Location:repintegral.php?error=' . $error . '');
        die();
    }
} else {
    $fecha1 = '';
    $fecha2 = '';
    $gerente = '';
}


$filtro = "fecha1=" . $fecha1 . "&fecha2=" . $fecha2 . "&gerente=" . $gerente;

$ide = $_SESSION['id'];
$rowusuario = $db->query("SELECT * FROM usuarios INNER JOIN perfiles ON usuperfil = perid WHERE usuid = '" . $ide . "';")->fetch(PDO::FETCH_ASSOC);
$perfil = $rowusuario['usuperfil'];

?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>

    <link rel="stylesheet" type="text/css"
          href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css"/>
    <script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#tabla').dataTable({
                'bJQueryUI': true,
                'bStateSave': true,
                'sPaginationType': 'full_numbers',
                'oLanguage': {
                    'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
                },
                'bSort': false,
            });
            $('.btnatras').button({icons: {primary: 'ui-icon ui-icon-arrowthick-1-w'}});
            $('#dialog-message').dialog({
                height: 80,
                width: 'auto',
                modal: true
            });
        });
    </script>
</head>
<body>
<?php require($r . 'incluir/src/login.php') ?>
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <?php require($r . 'incluir/src/menu.php') ?>
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a href="#">Call Center</a>
            <div class="mapa_div"></div>
            <a class="current">Reportes</a>
        </article>
        <article id="contenido">
            <h2>Reporte Integral <?php echo $fecha1 . " - " . $fecha2; ?></h2>
            <div class="reporte"><a
                        href="excelrepintegral.php?<?php echo $filtro ?>"><img
                            src="<?php echo $r ?>imagenes/iconos/excel.png" title="csv"/></a>
            </div>
            <table id="tabla">
                <thead>
                <tr>
                    <th align="center">Identificacion</th>
                    <th align="center">Usuario</th>
                    <th align="center">Estado Usuario</th>
                    <th align="center">Gerente Contac Center</th>
                    <th align="center">Tiempo en Conexion</th>
                    <th align="center">Registros Barridos</th>
                    <th align="center">Agendas Nuevas</th>
                    <th align="center">Agendas Realizadas</th>
                    <th align="center">Cantidad de pedidos</th>
                    <th align="center">Suma de Facturas</th>
                    <th align="center">Cantidad de Facturas</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $hoy = date('Y-m-d');
                if (empty($gerente)) {
                    $qry = $db->query("SELECT * FROM usuarios WHERE usucallcenter = '1' ORDER BY usunombre");
                } elseif (!empty($gerente)) {
                    $qry = $db->query("SELECT * FROM usuarios WHERE usucallcenter = '1' AND usujefecallcenter = '$gerente' ORDER BY usunombre");
                }


                $dias = fechaDif($fecha1, $fecha2);

                $consultaSql = "select u.usuid, " .
                    "       u.usunombre, " .
                    "       case " .
                    "           when uJefe.usuperfil = 56 then 'INACTIVO' " .
                    "           when (isnull(uJefe.usufechaingreso) or uJefe.usufechaingreso = '') then 'SIN FECHA DE INGRESO' " .
                    "           when datediff(uJefe.usufechaingreso, now()) >= 60 then 'ANTIGUO' " .
                    "           else 'NUEVO' end               EstadoRelacionista, " .
                    "       ifnull(uJefe.usunombre, 'NONE')    Jefe, " .
                    "       (select count(*) " .
                    "        from hisprospectos " .
                    "        where hisprousuario = u.usuid " .
                    (empty($fecha1) ? "" : "and DATE(hisprofechahora) between '$fecha1' and '$fecha2'") .
                    ") as Gestiones, " .
                    "       sum(if(c.capaestado = 3, 1, 0)) as Confirmado, " .
                    "       sum(if(c.capaestado = 5, 1, 0)) as Realizado, " .
                    "       (select count(*) " .
                    "        from solicitudes " .
                    "        where solrelacionista = u.usuid " .
                    (empty($fecha1) ? "" : "and DATE(solfecha) between '$fecha1' and '$fecha2'") .
                    ")  CantidadDePedidos, " .
                    "       (SELECT ifnull(sum(soltotal), 0) " .
                    "        FROM solicitudes " .
                    "                 INNER JOIN movimientos ON (solempresa = movempresa AND solfactura = movnumero) " .
                    "                 INNER JOIN clientes ON solcliente = cliid " .
                    "        where movprefijo = 'FV' " .
                    "          AND movestado = 'FACTURADO' " .
                    "          and solrelacionista = u.usuid " .
                    (empty($fecha1) ? "" : "and DATE(solfecha) between '$fecha1' and '$fecha2'") .
                    ")  SumaDeFacturas, " .
                    "       (SELECT count(*) " .
                    "        FROM solicitudes " .
                    "                 INNER JOIN movimientos ON (solempresa = movempresa AND solfactura = movnumero) " .
                    "                 INNER JOIN clientes ON solcliente = cliid " .
                    "        where movprefijo = 'FV' " .
                    "          AND movestado = 'FACTURADO' " .
                    "          and solrelacionista = u.usuid " .
                    (empty($fecha1) ? "" : "and DATE(solfecha) between '$fecha1' and '$fecha2'") .
                    ")  CantidadDeFacturas " .
                    "from usuarios u " .
                    "         inner join capacitaciones c on c.capausuario = u.usuid " .
                    "         left join usuarios uJefe on u.usujefecallcenter = uJefe.usuid " .
                    "where (select count(*) from hisprospectos where hisprousuario = u.usuid) > 0 " .
                    "  and u.usucallcenter = 1 " .
                    (empty($gerente) ? "" : "and u.usuid = '$gerente'") .
                    (empty($fecha1) ? "" : "and DATE(c.capafechausuario) between '$fecha1' and '$fecha2'") .
                    "group by u.usuid, u.usunombre, uJefe.usuperfil, uJefe.usufechaingreso; ";


                $reporte = $db->query($consultaSql);
                while ($row = $reporte->fetch(PDO::FETCH_ASSOC)) {


                    ?>
                    <tr>
                        <td align="center"><?php echo $row['usuid']; ?></td>
                        <td align="center"><?php echo $row['usunombre']; ?></td>
                        <td align="center"><?php echo $row['EstadoRelacionista']; ?></td>
                        <td align="center"><?php echo $row['Jefe']; ?></td>
                        <td align="center"><?php echo $dias; ?></td>
                        <td align="center"><?php echo $row['Gestiones']; ?></td>
                        <td align="center"><?php echo $row['Confirmado']; ?></td>
                        <td align="center"><?php echo $row['Realizado']; ?></td>
                        <td align="center"><?php echo $row['CantidadDePedidos']; ?></td>
                        <td align="center"><?php echo number_format($row['SumaDeFacturas'], 2); ?></td>
                        <td align="center"><?php echo number_format($row['CantidadDeFacturas'], 2); ?></td>
                    </tr>
                    <?php
                }
                ?>

                </tbody>
            </table>
            <p class="boton">
                <button type="button" class="btn btn-outline-primary btnatras"
                        onClick="carga(); location.href='repintegral.php'">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-left-square-fill"
                         fill="currentColor"
                         xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                              d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm9.5 8.5a.5.5 0 0 0 0-1H5.707l2.147-2.146a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708-.708L5.707 8.5H11.5z"/>
                    </svg>
                    Atras
                </button>
            </p>
            <?php require($r . 'incluir/src/pie.php') ?>
        </article>
    </article>
</section>

<?php
if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>';
elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>';
?>

<div id="modal" style="display:none"></div>
<div id="dialog2" style="display:none"></div>
</body>
</html>