<?php
$r = '../../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');


if (isset($_GET['error'])) {
    $error = $_GET['error'];
}
if (isset($_GET['mensaje'])) {
    $mensaje = $_GET['mensaje'];
}


?>
<!doctype html>
<html>
<head>
    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>
    <script type="text/javascript">
        $(document).ready(function () {

            $('#fecha1').datepicker({
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true,
                maxDate: '+0D',
                onClose: function (selectedDate) {
                    $('#fecha2').datepicker('option', 'minDate', selectedDate);
                }
            });
            $('#fecha2').datepicker({
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true,
                maxDate: '+0D',
                onClose: function (selectedDate) {
                    $('#fecha1').datepicker('option', 'maxDate', selectedDate);
                }
            });
        });
    </script>
</head>
<body>
<?php require($r . 'incluir/src/login.php') ?>
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <?php require($r . 'incluir/src/menu.php') ?>
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a href="#">Call Center</a>
            <div class="mapa_div"></div>
            <a class="current">Reportes</a>
        </article>
        <article id="contenido">
            <form id="form" name="form" action="lisrepintegral.php" method="get">
                <fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
                    <legend class="ui-widget ui-widget-header ui-corner-all">Reporte Integral</legend>
                    <p>
                        <label for="fechas">Intervalo:</label>
                        <label for="">Desde</label>
                        <input type="text" name="fecha1" id="fecha1" class="text-input fecha"/>
                        <label for="">Hasta</label>
                        <input type="text" name="fecha2" id="fecha2" class="text-input fecha"/>
                    </p>

                    <p>
                        <label for="gerente">Gerente:</label>
                        <select id="gerente" name="gerente">
                            <option value=''>SELECCIONE</option>

                            <?php
                            $qry = $db->query("SELECT * FROM usuarios WHERE usudirjefe = usuid order by usunombre");
                            while ($row4 = $qry->fetch(PDO::FETCH_ASSOC)) {
                                echo '<option value="' . $row4["usuid"] . '">' . $row4['usunombre'] . '</option>';
                            }
                            ?>
                        </select>
                    </p>

                    <div class="row">
                        <div class="col-md-12 col-lg-6">
                            <button type="submit" class="btnconsulta btn btn-primary btn-block mt-2" name="consultar"
                                    value="Buscar">consultar
                            </button> <!-- BOTON CONSULTAR -->
                        </div>
                        <div class="col-md-12 col-lg-6">
                            <button type="submit" class="btnconsulta btn btn-primary btn-block mt-2" name="show-all"
                                    value="Buscar">Mostrar Todo
                            </button>
                        </div>
                    </div>
                </fieldset>
            </form>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php') ?>

</section>
<?php
if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>';
elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>';
?>

<div id="modal" style="display:none"></div>
<div id="dialog2" style="display:none"></div>
</body>
</html>