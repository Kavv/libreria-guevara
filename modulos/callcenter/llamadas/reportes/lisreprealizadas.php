<?php
$r = '../../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
require($r . 'incluir/funciones.php');
$fecha1 = isset($_POST['fecha1']) ? $_POST['fecha1'] : '';
$fecha2 = isset($_POST['fecha2']) ? $_POST['fecha2'] : '';
$filtro = 'fecha1=' . $fecha1 . '&fecha2=' . $fecha2;

$con = "select usunombre,usuid,usuperfil, count(*) Cantidad " .
    "from capacitaciones " .
    "         inner join usuarios on capausuario = usuid ";

$group = "group by usunombre,usuid,usuperfil ";

$siempreVerdaero = true;
if (isset($_POST['show-all'])) {
    $sql = $con . " where capaestado = 5 " . $group;
} else {
    $sql = crearConsulta($con, $group,
        array($fecha1, "capafecha BETWEEN '$fecha1' AND '$fecha2'"),
        array($siempreVerdaero, "capaestado = 5")
    );
}

$qry = $db->query($sql);

?>
<!doctype html>
<html>
<head>
    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>

    <link rel="stylesheet" type="text/css"
          href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css"/>
    <script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>

    <style>
        .dataTables_wrapper {
            overflow-x: auto;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#tabla').dataTable({
                'bJQueryUI': true,
                'bStateSave': true,
                'sPaginationType': 'full_numbers',
                'oLanguage': {
                    'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
                },
            });
        });
    </script>
</head>
<body>
<?php require($r . 'incluir/src/login.php') ?>
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <?php require($r . 'incluir/src/menu.php') ?>
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a href="#">Call Center</a>
            <div class="mapa_div"></div>
            <a class="current">Reportes</a>
        </article>
        <article id="contenido">
            <h2>Reporte Capacitaciones
                Realizadas <?php echo $fecha1 == '' ? '' : ' del' . $fecha1 . ' al ' . $fecha2 ?></h2>
            <table id="tabla">
                <thead>
                <tr>
                    <th>Asesor</th>
                    <th>Estado</th>
                    <th>C. Realizadas</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $fnlsol = 0;

                while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {

                    ?>
                    <tr>
                        <td title="<?php echo $row['usuid'] ?>"><?php echo $row['usunombre'] ?></td>
                        <td align="center"><?php if ($row['usuperfil'] == 56) {
                                echo "DESACTIVADO";
                            } else {
                                echo "ACTIVO";
                            } ?></td>
                        <td align="center"><?php echo number_format($row['Cantidad'], 2) ?></td>
                    </tr>
                    <?php
                    $fnlsol = $fnlsol + $row['Cantidad'];

                }
                ?>

                </tbody>
                <tfoot>
                <tr>
                    <td align="center"></td>
                    <td align="center"></td>
                    <td align="center" bgcolor="#D1CFCF"><?php echo $fnlsol ?></td>
                </tr>
                </tfoot>
            </table>
            <p class="boton">
                <button type="button" class="btn btn-outline-primary btnatras"
                        onClick="carga(); location.href='reprealizadas.php'">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-left-square-fill"
                         fill="currentColor"
                         xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                              d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm9.5 8.5a.5.5 0 0 0 0-1H5.707l2.147-2.146a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708-.708L5.707 8.5H11.5z"/>
                    </svg>
                    Atras
                </button>
            </p>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php') ?>
</section>
</body>
</html>