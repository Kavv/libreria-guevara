<?php
$r = '../../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
require($r . 'incluir/funciones.php');
$call = $_POST['call'];
$fecha1 = $_POST['fecha1'];
$fecha2 = $_POST['fecha2'];

$filtro = 'call=' . $call . '&fecha1=' . $fecha1 . '&fecha2=' . $fecha2;

$con = "SELECT proid, " .
    "       resllamdescripcion                    as ResultadoDeLaLlamada, " .
    "       ifnull(condescripcion, 'No efectivo') as Contactabilidad, " .
    "       ifnull(efedescripcion, 'No efectivo')    Efectividad, " .
    "       hisproobservacion, " .
    "       hisprofechahora, " .
    "       usunombre                             as Usuario " .
    "FROM hisprospectos " .
    "         inner join prospectos on hisproprospectos = proid " .
    "         inner join usuarios on usuid = hisprousuario " .
    "         LEFT JOIN resultadollamada ON resllamid = hisproresultadollamada " .
    "         LEFT JOIN contactabilidad ON conid = hisprocontactabilidad " .
    "         LEFT JOIN efectividad ON efeid = hisproefectividad ";

$group = "order by hisprofechahora desc";

if (!isset($_POST['show-all'])) {
    $sql = crearConsulta($con, $group,
        array($fecha1, "DATE(hisprofechahora) BETWEEN '$fecha1' AND '$fecha2'"),
        array($call, "hisprousuario = '$call'")
    );
} else {
    $sql = $con . " " . $group;
}

$qry = $db->query($sql);

?>
<!doctype html>
<html>
<head>
    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>

    <link rel="stylesheet" type="text/css"
          href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css"/>
    <script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.tabla').dataTable({
                'bJQueryUI': true,
                'bStateSave': true,
                'bSort': false,
                'sPaginationType': 'full_numbers',
                'oLanguage': {
                    'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
                },
            });
        });
    </script>
</head>
<body>
<?php require($r . 'incluir/src/login.php') ?>
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <?php require($r . 'incluir/src/menu.php') ?>
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a href="#">Call Center</a>
            <div class="mapa_div"></div>
            <a class="current">Reportes</a>
        </article>
        <article id="contenido">
            <h2>Estadistico venta detallada por personal del call center
                del <?php echo $fecha1 . ' al ' . $fecha2 ?></h2>
            <table class="tabla">
                <thead>
                <tr>
                    <th>ID Prospecto</th>
                    <th title="Resultado de llamada">Resultado de Llamada</th>
                    <th title="Contactabilidad">Contactabilidad</th>
                    <th title="Efectividad">Efectividad</th>
                    <th>Observacion</th>
                    <th>Fecha</th>
                    <th>Usuario Call</th>
                </tr>
                </thead>
                <tbody>
                <?php if ($call == '') { ?>
                    <div class="reporte"> <a
                                href="excellisgestiones1.php?<?php echo $filtro ?>"><img
                                    src="<?php echo $r ?>imagenes/iconos/excel.png" title="csv"/></a>
                    </div>
                <?php } ?>
                <?php
                while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
                    ?>


                    <tr>
                        <td align="center"><?php echo $row['proid'] ?></td>
                        <td align="center"><?php echo $row['ResultadoDeLaLlamada'] ?></td>
                        <td align="center"><?php echo $row['Contactabilidad'] ?></td>
                        <td align="center"><?php echo $row['Efectividad'] ?></td>
                        <td title="<?php echo $row['proid'] ?>"><?php echo $row['hisproobservacion'] ?></td>
                        <td align="center"><?php echo $row['hisprofechahora'] ?></td>
                        <td align="right"
                            title="<?php echo $row['Usuario'] ?>"><?php echo $row['Usuario'] ?></td>
                    </tr>

                    <?php
                }
                ?>
                </tbody>
            </table>

            <div class="text-center mt-1">
                <button type="button" class="btn btn-outline-primary btnatras"
                        onClick="carga(); location.href='repgestiones.php'">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-left-square-fill"
                         fill="currentColor"
                         xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                              d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm9.5 8.5a.5.5 0 0 0 0-1H5.707l2.147-2.146a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708-.708L5.707 8.5H11.5z"/>
                    </svg>
                    Atras
                </button>
            </div>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php') ?>
</section>
</body>
</html>