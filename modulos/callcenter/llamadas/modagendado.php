<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

$prospecto = $_GET['proid'];
$agendamientoid = $_GET['agendamientoid'];
$fecha1 = $_GET['fecha1'];


$filtro = "fecha1=" . $fecha1;

$qry1 = $db->query("SELECT * FROM agendadas WHERE agenproid = '" . $prospecto . "' AND agenid = '" . $agendamientoid . "';");
$row1 = $qry1->fetch(PDO::FETCH_ASSOC);


?>
<!doctype html>
<html>
<head>

    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>

    <script type="text/javascript">
        $(document).ready(function () {

            $('#fecha2').datepicker({
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true,
                minDate: '0D',
            });

        });
    </script>
</head>
<body>
<?php require($r . 'incluir/src/login.php') ?>
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <?php require($r . 'incluir/src/menu.php') ?>
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a href="#">Call Center</a>
            <div class="mapa_div"></div>
            <a class="current">Modificar Agendamiento</a>
        </article>
        <article id="contenido">
            <form id="form" name="form" action="lisagendado.php?<?php echo $filtro; ?>" method="post">
                <fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
                    <legend class="ui-widget ui-widget-header ui-corner-all">Modificar Agendamiento</legend>

                    <p>
                        <label style="width:140px;" for="prospecto"> Identificacion del prospecto: </label>
                        <input type="text" name="prospecto" id="prospecto" value="<?php echo $row1['agenproid'] ?>"
                               class="id validate[required]" readonly/>
                    </p>
                    <p>
                        <label style="width:140px;" for="agenid"> Identificacion de la Agendada: </label>
                        <input type="text" name="agenid" id="agenid" value="<?php echo $row1['agenid'] ?>"
                               class="id validate[required]" readonly/>
                    </p>
                    <p>
                        <?php
                        $qrynombre = $db->query("SELECT * FROM usuarios WHERE usuid = '" . $row1['agenusuario'] . "';");
                        $rownombre = $qrynombre->fetch(PDO::FETCH_ASSOC);
                        $nombre_usu = $rownombre['usunombre'];
                        ?>
                        <label style="width:140px;" for="agendadapor"> Agendamiento Gestionado por: </label>
                        <input style="width:300px" type="text" name="agendadapor" id="agendadapor"
                               value="<?php echo $nombre_usu ?>" readonly/>
                    </p>
                    <p>
                        <label style="width:140px;" for="diaagenda"> Agendamiento gestionado el dia: </label>
                        <input style="width:300px" type="text" name="diaagenda" id="diaagenda"
                               value="<?php echo $row1['agenfechahora'] ?>" readonly/>
                    </p>

                    <?php
                    $qryhofe = $db->query("SELECT time(agenfechahorallamar) as horaAgen , date(agenfechahorallamar) as fechaAgen FROM agendadas WHERE agenproid = '" . $prospecto . "' AND agenid = '" . $agendamientoid . "';");
                    $rowhofe = $qryhofe->fetch(PDO::FETCH_ASSOC);
                    $horaAgen = $rowhofe['horaAgen'];
                    $fechaAgen = $rowhofe['fechaAgen'];
                    ?>
                    <p>
                        <label style="width:140px;" for="fecha2"> Fecha de Agendamiento: </label>
                        <input type="text" name="fecha2" value="<?php echo $fechaAgen ?>" id="fecha2"
                               class="validate[required] text-input fecha"/>
                    </p>
                    <p>
                        <label style="width:140px;" for="hora2"> Hora de Agendamiento: </label>
                        <input type="time" name="hora2" id="hora2" value="<?php echo $horaAgen ?>" max="23:59:59"
                               min="00:00:01" class="validate[required]" step="1">
                    </p>
                    </br>
                    <p class="boton">
                        <button type="button" class="btn btn-outline-primary btnatras"
                                onClick="carga(); location.href='lisagendado.php?<?php echo $filtro ?>'">
                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-left-square-fill"
                                 fill="currentColor"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                      d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm9.5 8.5a.5.5 0 0 0 0-1H5.707l2.147-2.146a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708-.708L5.707 8.5H11.5z"/>
                            </svg>
                            Atras
                        </button>
                        <button type="submit" class="btn btn-primary btnmodificar" name="modificar" value="modificar">
                            Modificar
                        </button>  <!-- BOTON MODIFICAR -->
                    </p>

                </fieldset>
            </form>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php'); ?>
</section>
</body>
</html>