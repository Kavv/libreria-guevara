<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

if (isset($_GET['mensaje'])) {
    $mensaje = $_GET['mensaje'];
}
if (isset($_GET['error'])) {
    $error = $_GET['error'];
}
?>
<!doctype html>
<html>
<head>

    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>
    <script type="text/javascript">
        $(document).ready(function () {

            $("#fecha1").datepicker({
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true,
                maxDate: "+100D",
                onClose: function (selectedDate) {
                    $("#fecha2").datepicker("option", "minDate", selectedDate);
                }
            }).keypress(function (event) {
                event.preventDefault()
            });
            $("#fecha2").datepicker({
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true,
                maxDate: "+100D",
                onClose: function (selectedDate) {
                    $("#fecha1").datepicker("option", "maxDate", selectedDate);
                }
            }).keypress(function (event) {
                event.preventDefault()
            });

            $("#fecha3").datepicker({
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true,
                maxDate: "+100D",
                onClose: function (selectedDate) {
                    $("#fecha4").datepicker("option", "minDate", selectedDate);
                }
            }).keypress(function (event) {
                event.preventDefault()
            });
            $("#fecha4").datepicker({
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true,
                maxDate: "+100D",
                onClose: function (selectedDate) {
                    $("#fecha3").datepicker("option", "maxDate", selectedDate);
                }
            }).keypress(function (event) {
                event.preventDefault()
            });

            $('#departamento').change(function (event) {
                var id1 = $('#departamento').find(':selected').val();
                $('#ciudad').load('<?php echo $r ?>incluir/carga/ciudades.php?id1=' + id1);
            });
        });
    </script>
</head>
<body>
<?php require($r . 'incluir/src/login.php') ?>
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <?php require($r . 'incluir/src/menu.php') ?>
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a href="#">Call Center</a>
            <div class="mapa_div"></div>
            <a class="current">Capacitaciones</a>
        </article>
        <article id="contenido">
            <form id="form" name="form" action="liscapacitacion.php" method="get">
                <fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
                    <legend class="ui-widget ui-widget-header ui-corner-all">Capacitaciones Agendadas</legend>
                    <p>
                        <label for="idcapaci"> ID Capacitacion: </label>
                        <input type="text" name="idcapaci"/>
                    <p>
                    <p>
                        <label for="idpros"> ID Prospecto: </label>
                        <input type="text" name="idpros"/>
                    <p>
                    <p>
                        <label for="call">Call :</label>
                        <select name="call">
                            <?php
                            $qrypermisos = $db->query("SELECT * FROM usuarios WHERE usuid = '" . $_SESSION['id'] . "';");
                            $rowpermisos = $qrypermisos->fetch(PDO::FETCH_ASSOC);
                            if ($rowpermisos['usuperfil'] <> 98 or $rowpermisos['usustaff'] == 1) {
                                echo "<option value=''>TODOS</option>";
                            }
                            if ($rowpermisos['usuperfil'] == 98 and $rowpermisos['usustaff'] <> 1) {
                                $qry = $db->query("SELECT * FROM usuarios WHERE usuid = " . $_SESSION['id'] . " ORDER BY usunombre asc;");
                                while ($row = $qry->fetch(PDO::FETCH_ASSOC))
                                    echo '<option value=' . $row['usuid'] . '>' . $row['usunombre'] . '</option>';
                            } else {
                                $qry = $db->query("SELECT * FROM usuarios WHERE usucallcenter = '1' and usuperfil <> '56' ORDER BY usunombre asc");

                                while ($row = $qry->fetch(PDO::FETCH_ASSOC))
                                    echo '<option value=' . $row['usuid'] . '>' . $row['usunombre'] . '</option>';
                            }
                            ?>
                        </select>
                    </p>
                    <p>
                        <label for="asesor">Asesor:</label>
                        <select name="asesor">
                            <option value="">TODOS</option>
                            <?php
                            $qry = $db->query("SELECT * FROM usuarios WHERE usuasesor = '1' and usuperfil <> '56'  ORDER BY usunombre");
                            while ($row = $qry->fetch(PDO::FETCH_ASSOC))
                                echo '<option value=' . $row['usuid'] . '>' . $row['usunombre'] . '</option>';
                            ?>
                        </select>
                    </p>
                    <p>
                        <label for="departamento">Provincia:</label>
                        <select name="departamento" id="departamento">
                            <option value="">TODOS</option>
                            <?php
                            $qry = $db->query("SELECT * FROM departamentos ORDER BY depnombre");
                            while ($row = $qry->fetch(PDO::FETCH_ASSOC))
                                echo '<option value=' . $row['depid'] . '>' . $row['depnombre'] . '</option>';
                            ?>
                        </select>
                    </p>
                    <p>
                        <label>Ciudad: </label> <!-- CAMPO CIUDAD -->
                        <select name="ciudad" id="ciudad" class="text-input">
                            <?php
                            echo '<option value="">TODOS</option>';
                            $qry = $db->query("SELECT * FROM ciudades WHERE ciudepto = '" . $row['prodepartamento'] . "' AND ciuid <> '" . $row['prociudad'] . "' ORDER BY ciunombre");
                            while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
                                echo '<option value=' . $row2['depid'] . '>' . $row2['depnombre'] . '</option>'; // APARECEN LAS CIUDADES PARA QUE EL PROSPECTO ESCOJA LA QUE CORRESPONDE
                            }
                            ?>
                        </select>
                    </p>
                    <p>
                        <label for="fechas">Rango fechas de capacitacion:</label>
                        <input type="text" name="fecha1" id="fecha1" class="fecha"/> - <input type="text" id="fecha2"
                                                                                              name="fecha2"
                                                                                              class="fecha"/>
                    </p>
                    <?php if ($rowpermisos['usudirectorcall'] == 1 or $rowpermisos['usuid'] == '1053724602') {
                        echo "
<p>
<label for='fechas_ges'>Rango Fechas de Gestion:</label>
<input type='text' name='fecha3' id='fecha3' class='fecha' />  - <input type='text' name='fecha4' id='fecha4' class='fecha' /> 
</p>
";
                    }
                    ?>

                    <p>
                        <label for="estado">Estado de capacitacion:</label>
                        <select name="estado">
                            <option value="">TODOS</option>
                            <option value="1">ACTIVA</option>
                            <option value="2">CANCELADA</option>
                            <option value="3">CONFIRMADA</option>
                            <option value="4">APLAZADA</option>
                            <option value="7">REPROGRAMADA</option>
                            <option value="5">REALIZADA</option>
                            <option value="6">NO REALIZADA</option>
                        </select>
                    </p>
                    <p>
                        <label for="uninegoci">Unidad de Negocio:</label>
                        <select name="uninegoci">
                            <option value="">TODOS</option>
                            <?php
                            $qry = $db->query("SELECT * FROM uninegocio ORDER BY unidescripcion");
                            while ($row = $qry->fetch(PDO::FETCH_ASSOC))
                                echo '<option value=' . $row['uniid'] . '>' . $row['unidescripcion'] . '</option>';
                            ?>
                        </select>
                    </p>
                    <p>
                        <label for="modalidad">Modalidad:</label>
                        <select name="modalidad">
                            <option value="">TODOS</option>
                            <option value="TELETRABAJO">TELETRABAJO</option>
                            <option value="PRESENCIAL">PRESENCIAL</option>

                        </select>
                    </p>
                    <p>
                        <label for="jeferela">Gerente Relacionistas:</label>
                        <select id="jeferela" name="jeferela">
                            <option value=''>TODOS</option>
                            <option value='52765484'>MARIA LILIANA MANRIQUE DIAZ</option>
                            <option value='1110451657'>JUAN CAMILO ROMERO HOYOS</option>
                            <option value='1018410939'>YONNATAN ANDRES LIZCANO TELLEZ</option>
                        </select>
                    </p>
                    <div class="row">
                        <div class="col-md-12 col-lg-6">
                            <button type="submit" class="btnconsulta btn btn-primary btn-block mt-2" name="consultar"
                                    value="Buscar">consultar
                            </button> <!-- BOTON CONSULTAR -->
                        </div>
                        <div class="col-md-12 col-lg-6">
                            <button type="submit" class="btnconsulta btn btn-primary btn-block mt-2" name="show-all"
                                    value="Buscar">Mostrar Todo
                            </button>
                        </div>
                    </div>
                </fieldset>
            </form>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php'); ?>
</section>
<?php require($r . 'incluir/src/pie.php'); ?>
</section>
<?php
if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>';
elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>';
?>
<div id="modal" style="display:none"></div>
<div id="dialog2" style="display:none"></div>
</body>
</html>