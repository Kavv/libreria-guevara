<?php
// LA ACCION PRINCIPAL DE ESTA PAGINA ES MODIFICAR UN PROSPECTO ENVIANDO LA INFORMACION A LISTAR.PHP

$r = '../../../';
//INCLUIR SESION Y CONECCION
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
$rowprin = $db->query("SELECT * FROM viaticos WHERE viaid = " . $_GET['id'])->fetch(PDO::FETCH_ASSOC); // CONSULTA A LA BD CON VARUABLES RECIBIDAS POR GET
$filtro = 'asesor=' . $_GET['asesor'] . '&fechaida1=' . $_GET['fechaida1'] . '&fechaida2=' . $_GET['fechaida2'] . '&fecharegreso1=' . $_GET['fecharegreso1'] . '$fecharegreso2=' . $_GET['fecharegreso2'];

if ($rowprin['viaestado'] == 'ANULADA') {
    $error = "No es posible modificar esta solicitud por que se encuentra ANULADA";
    header('Location:listarviaticos.php?error=' . $error . '&' . $filtro . '');
}
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">

    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>
    <script>
        $(document).ready(function () {

            $('#fechaida').datepicker({ // VALIDACION DE LA FECHA UNO
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true,
                onClose: function (selectedDate) {
                    $('#fecharegreso').datepicker('option', 'minDate', selectedDate);
                }
            }).keypress(function (event) {
                event.preventDefault()
            });
            $('#fecharegreso').datepicker({ // VALIDACION DE LA FECHA DOS
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true,
                onClose: function (selectedDate) {
                    $('#fechaida').datepicker('option', 'maxDate', selectedDate);
                }
            }).keypress(function (event) {
                event.preventDefault()
            });


            $('#fecha2').datepicker({ // VALIDACION DE LA FECHA UNO
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true,
                onClose: function (selectedDate) {
                    $('#fecha3').datepicker('option', 'minDate', selectedDate);
                }
            }).keypress(function (event) {
                event.preventDefault()
            });
            $('#fecha3').datepicker({ // VALIDACION DE LA FECHA DOS
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true,
                onClose: function (selectedDate) {
                    $('#fecha2').datepicker('option', 'maxDate', selectedDate);
                }
            }).keypress(function (event) {
                event.preventDefault()
            });


            $('#departamento').change(function (event) {
                var id1 = $('#departamento').find(':selected').val();
                $('#ciudad').load('<?php echo $r ?>incluir/carga/ciudades.php?id1=' + id1);
            });

            $('#monto_total').change(function (event) {
                var nVal = $('#monto_total').val();
                $('#monto2, #monto3').val(nVal / 2);
            });

        });
    </script>
</head>
<body>
<?php require($r . 'incluir/src/login.php') ?> <!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
    <?php require($r . 'incluir/src/menu.php') ?> <!-- INCLUIMOS MENU PRINCIPAL -->
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a href="#">Call Center</a>
            <div class="mapa_div"></div>
            <a class="current">Modificar</a>
        </article>
        <article id="contenido">
            <div class="ui-widget">
                <form id="form" name="form" action="listarviaticos.php?<?php echo $filtro ?>" method="post">
                    <!-- FORMULARIO ENVIADO POR POST A LISTAR.PHP JUNTO CON EL FILTRO GENERAL  -->

                    <fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
                        <legend class="ui-widget ui-widget-header ui-corner-all">Modificar Viaticos</legend>
                        </br>
                        <p>
                            <label for="iden"> Identificador: </label>
                            <input type="text" name="iden" id="iden" class="id" value="<?php echo $rowprin['viaid'] ?>"
                                   readonly/>
                        </p>
                        <p>
                            <label for="fechaida"> fecha ida y regreso: </label>
                            <input type="text" value="<?php echo $rowprin['viadiaviajeida']; ?>" name="fechaida"
                                   id="fechaida" class="validate[required] text-input fecha"/> - <input type="text"
                                                                                                        value="<?php echo $rowprin['viadiaviajeregreso']; ?>"
                                                                                                        name="fecharegreso"
                                                                                                        id="fecharegreso"
                                                                                                        class="validate[required] text-input fecha"/>
                        </p>
                        <p>
                            <label>Provincia:</label>
                            <select id="departamento" name="departamento" class="validate[required] text-input">
                                <?php
                                if ($rowprin['viadepdestino'] != '') {
                                    $row2 = $db->query("SELECT * FROM departamentos WHERE depid = '" . $rowprin['viadepdestino'] . "'")->fetch(PDO::FETCH_ASSOC);
                                    echo '<option value=' . $rowprin['viadepdestino'] . '>' . $row2['depnombre'] . '</option>';
                                }
                                echo '<option value="">SELECCIONE</option>';
                                $qry = $db->query("SELECT * FROM departamentos WHERE depid <> '" . $rowprin['viadepdestino'] . "' ORDER BY depnombre");
                                while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
                                    echo '<option value=' . $row2['depid'] . '>' . $row2['depnombre'] . '</option>';
                                }
                                ?>
                            </select>
                        </p>
                        <p>
                            <label>Ciudad: </label>
                            <select name="ciudad" id="ciudad" class="validate[required] text-input">
                                <?php
                                if ($rowprin['viaciudestino'] != '') {
                                    $row2 = $db->query("SELECT * FROM ciudades WHERE ciudepto = '" . $rowprin['viadepdestino'] . "' AND ciuid = '" . $rowprin['viaciudestino'] . "'")->fetch(PDO::FETCH_ASSOC);
                                    echo '<option value=' . $row['viaciudestino'] . '>' . $row2['ciunombre'] . '</option>';
                                }
                                echo '<option value="">SELECCIONE</option>';
                                $qry = $db->query("SELECT * FROM ciudades WHERE ciudepto = '" . $rowprin['viadepdestino'] . "' AND ciuid <> '" . $rowprin['viaciudestino'] . "' ORDER BY ciunombre");
                                while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
                                    echo '<option value=' . $row2['depid'] . '>' . $row2['depnombre'] . '</option>';
                                }
                                ?>
                            </select>
                        </p>
                        <p>
                            <label for="asesor">Asesor:</label>
                            <select name="asesor" class="validate[required] text-input">
                                <?php
                                if ($rowprin['viacapacitador'] != '') {
                                    $qry = $db->query("SELECT * FROM usuarios WHERE usuasesor = '1' AND usuperfil <> 56 AND usuid = '" . $rowprin['viacapacitador'] . "' ORDER BY usunombre");
                                    while ($row = $qry->fetch(PDO::FETCH_ASSOC))
                                        echo '<option value=' . $row['usuid'] . '>' . $row['usunombre'] . '</option>';
                                }
                                echo '<option value="">SELECCIONE</option>';
                                $qry = $db->query("SELECT * FROM usuarios WHERE usuasesor = '1' AND usuperfil <> 56 AND usuid <> '" . $rowprin['viacapacitador'] . "' ORDER BY usunombre");
                                while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
                                    echo '<option value=' . $row['usuid'] . '>' . $row['usunombre'] . '</option>';
                                }
                                ?>
                            </select>
                        </p>
                        <p>
                            <label for="empresa_asesor">Empresa del Asesor:</label>
                            <select name="empresa_asesor" class="validate[required] text-input">
                                <?php
                                if ($rowprin['viaempcapacitador'] != '') {
                                    $qry = $db->query("SELECT * FROM empresas WHERE empid = '" . $rowprin['viaempcapacitador'] . "' ORDER BY empnombre");
                                    while ($row = $qry->fetch(PDO::FETCH_ASSOC))
                                        echo '<option value=' . $row['empid'] . '>' . $row['empnombre'] . '</option>';
                                }
                                echo '<option value="">SELECCIONE</option>';
                                $qry = $db->query("SELECT * FROM empresas WHERE empid <> '" . $rowprin['viacapacitador'] . "' ORDER BY empnombre");
                                while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
                                    echo '<option value=' . $row['empid'] . '>' . $row['empnombre'] . '</option>';
                                }
                                ?>
                            </select>
                        </p>
                        <p>
                            <label title='Monto Total'>Monto Total: </label>
                            <?php
                            if (empty($rowprin['viavalortotal'])) {
                                echo "<input type='text' name='monto_total' id='monto_total' class='valor validate[required, custom[onlyNumberSp]] text-input'  />";
                            } elseif (!empty($rowprin['viavalortotal'])) {
                                echo "<input type='text' value ='" . $rowprin['viavalortotal'] . "' name='monto_total' id='monto_total' class='valor validate[required, custom[onlyNumberSp]] text-input'  />";
                            }
                            ?>
                        </p>
                        <p>
                            <label title='Fecha y monto del pago del primer 50%'>F. Monto del 1 50%: </label>
                            <?php
                            if (!empty($rowprin['viafechavalor1'])) {
                                echo "<input type='text' name='fecha2' id='fecha2' value='" . $rowprin['viafechavalor1'] . "' class='validate[required] text-input fecha'/>";
                            } elseif (empty($rowprin['viafechavalor1'])) {
                                echo "<input type='text' name='fecha2' id='fecha2' class='validate[required] text-input fecha'/>";
                            }
                            echo "<span> - </span>";
                            if (!empty($rowprin['viavalor1'])) {
                                echo "<input type='text' name='monto2' id='monto2' value='" . $rowprin['viavalor1'] . "' class='valor validate[required, custom[onlyNumberSp]] text-input' readonly />";
                            } elseif (empty($rowprin['viavalor1'])) {
                                echo "<input type='text' name='monto2' id='monto2' class='valor validate[required, custom[onlyNumberSp]] text-input' readonly />";
                            }
                            ?>
                        </p>
                        <p>
                            <label title='Fecha y monto del pago del segundo 50%'>F. Monto del 2 50%: </label>
                            <?php
                            if (!empty($rowprin['viafechavalor2'])) {
                                echo "<input type='text' name='fecha3' id='fecha3' value='" . $rowprin['viafechavalor2'] . "' class='validate[required] text-input fecha'/>";
                            } elseif (empty($rowprin['viafechavalor2'])) {
                                echo "<input type='text' name='fecha3' id='fecha3' class='validate[required] text-input fecha'/>";
                            }
                            echo "<span> - </span>";
                            if (!empty($rowprin['viavalor2'])) {
                                echo "<input type='text' name='monto3' id='monto3' value='" . $rowprin['viavalor2'] . "' class='valor validate[required, custom[onlyNumberSp]] text-input' readonly />";
                            } elseif (empty($rowprin['viavalor2'])) {
                                echo "<input type='text' name='monto3' id='monto3' class='valor validate[required, custom[onlyNumberSp]] text-input' readonly />";
                            }
                            ?>
                        </p>
                        <p>
                            <label for="mesdili">Mes Diligenciamiento:</label>
                            <select name="mesdili" class="validate[required] text-input">
                                <?php
                                if ($rowprin['viamesdiligenciamiento'] <> 0) {
                                    $qry = $db->query("SELECT * FROM meses_ano WHERE mesid = '" . $rowprin['viamesdiligenciamiento'] . "'");
                                    while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
                                        echo '<option value=' . $row['mesid'] . '>' . $row['mesnombre'] . '</option>';
                                    }
                                }
                                echo '<option value="">SELECCIONE</option>';
                                $qry = $db->query("SELECT * FROM meses_ano WHERE mesid <> '" . $rowprin['viamesdiligenciamiento'] . "'");
                                while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
                                    echo '<option value=' . $row['mesid'] . '>' . $row['mesnombre'] . '</option>';
                                }
                                ?>
                            </select>
                        </p>
                        </br>
                    </fieldset>

                    <br>
                    </p>
                    <p class="boton">
                        <button type="button" class="btn btn-outline-primary btnatras"
                                onClick="carga(); location.href='listarviaticos.php?<?php echo $filtro ?>'">
                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-left-square-fill"
                                 fill="currentColor"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                      d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm9.5 8.5a.5.5 0 0 0 0-1H5.707l2.147-2.146a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708-.708L5.707 8.5H11.5z"/>
                            </svg>
                            Atras
                        </button>
                        <button type="submit" class="btn btn-primary btnmodificar" name="modificar" value="modificar">
                            Modificar
                        </button>  <!-- BOTON MODIFICAR -->
                    </p>
                </form>
            </div>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php'); ?> <!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
</section>
</body>
</html>