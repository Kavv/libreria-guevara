<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
?>
<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#fecha1').datepicker({
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
				onClose: function(selectedDate) {
					$('#fecha2').datepicker('option', 'minDate', selectedDate);
				}
			});
			$('#fecha2').datepicker({
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
				onClose: function(selectedDate) {
					$('#fecha1').datepicker('option', 'maxDate', selectedDate);
				}
			});
		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a class="current">Comisiones</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="lisdirector.php" method="get">
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">Proceso comisiones detallada directores</legend>
						<p>
							<label for="director">Director:</label>
							<select name="director">
								<option value="">SELECCIONE</option>
								<?php
								$qry = $db->query("SELECT * FROM usuarios WHERE usudirector = '1' ORDER BY usunombre");
								while ($row = $qry->fetch(PDO::FETCH_ASSOC))
									echo '<option value=' . $row['usuid'] . '>' . $row['usunombre'] . '</option>';
								?>
							</select>
						</p>
						<p>
							<label for="fechas">Intervalo:</label>
							<input type="text" name="fecha1" id="fecha1" class="validate[required] text-input fecha" /> 
							-
							<input type="text" name="fecha2" id="fecha2" class="validate[required] text-input fecha" />
						</p>
						<p class="boton">
							<button type="submit" class="btn btn-primary btngenera" name="genera" value="genera">generar</button>
						</p>
					</fieldset>
				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
</body>

</html>