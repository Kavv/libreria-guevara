<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
require($r . 'incluir/funciones.php');
require($r . 'incluir/fpdf/fpdf.php');

$director = $_GET['director'];
$fecha1 = $_GET['fecha1'];
$fecha2 = $_GET['fecha2'];

if ($director == '')
	$qrynom = $db->query("SELECT * FROM usuarios");
else
	$qrynom = $db->query("SELECT * FROM usuarios WHERE usuid = '$director'");



class PDF extends FPDF
{
	function Header()
	{
		global $fecha1;
		global $fecha2;
		global $nombre_director;
		$this->SetFont('LucidaConsole', '', 6);
		$this->Cell(50, 5, date('Y/m/d'), 0, 1);
		$this->SetFont('LucidaConsole', '', 8);
		$this->Cell(280, 5, '--------------------------------------------------------------------------------------------------------------------------------------------------------------------', 0, 1, 'C');
		$this->Cell(280, 5, 'ARRENDAMIENTO DETALLADO ' . $fecha1 . ' AL ' . $fecha2 . ' DEL DIRECTOR ' . $nombre_director, 0, 1, 'C');
		$this->Cell(280, 5, '--------------------------------------------------------------------------------------------------------------------------------------------------------------------', 0, 1, 'C');
		$this->Ln(5);
	}

	function Footer()
	{
		$this->SetY(-15);
		$this->SetFont('LucidaConsole', '', 8);
		$this->Cell(0, 5, '----------------------------------------------------------------------------------------------------------------------------------------------------------------------', 0, 1, 'C');
		$this->SetFont('LucidaConsole', '', 8);
		$this->Cell(0, 5, 'Pagina ' . $this->PageNo() . '/{nb}', 0, 0, 'R');
	}
}

$pdf = new PDF();
while ($rownom = $qrynom->fetch(PDO::FETCH_ASSOC)) {
	$nombre_director = $rownom['usunombre'];

	$sql =
		"SELECT * FROM 
		(
			(
				(
					(
						solicitudes LEFT JOIN carteras ON 
						(
							solempresa = carempresa AND solfactura = carfactura
						)
					) 
					LEFT JOIN detcarteras ON 
					(
						solempresa = dcaempresa AND solfactura = dcafactura
					)
				) 
				INNER JOIN empresas ON solempresa = empid
			) 
			INNER JOIN clientes ON solcliente = cliid
		) 
		INNER JOIN usuarios ON solover1 = usuid WHERE soltipo = 'NORMAL' 
		AND 
		(
			(
				dcacuota = 1 AND dcaestado = 'CANCELADO' AND solover1 = '" . $rownom['usuid'] . "' AND dcafepag BETWEEN '$fecha1' AND '$fecha2'
			) 
			OR 
			(
				solncuota = '0' AND solover1 = '" . $rownom['usuid'] . "' AND solfecha BETWEEN '$fecha1' AND '$fecha2' AND 
				(
					solestado = 'FACTURADO' OR solestado = 'ENVIADO' OR solestado = 'ENTREGADO' OR solestado = 'BANDEJA'
				)
			)
		) ORDER BY solfecha";
	//Fin


	$qry = $db->query($sql);
	$num = $qry->rowCount();
	if ($num > 0) {

		$pdf->AddFont('LucidaConsole', '', 'lucon.php');
		$pdf->AliasNbPages();
		$pdf->AddPage('L');

		$pdf->SetFont('LucidaConsole', '', 7);
		$pdf->SetFillColor(220, 220, 220);
		$pdf->SetX(5);
		// Suma total 260
		$pdf->Cell(50, 5, 'EMPRESA', 1, 0, 'C', true);
		$pdf->Cell(20, 5, 'SOLICITUD', 1, 0, 'C', true);
		$pdf->Cell(20, 5, 'FACTURA', 1, 0, 'C', true);
		$pdf->Cell(20, 5, 'F. FAC', 1, 0, 'C', true);
		$pdf->Cell(15, 5, '# CUOTA', 1, 0, 'C', true);
		$pdf->Cell(20, 5, 'F. CUOTA', 1, 0, 'C', true);
		$pdf->Cell(57, 5, 'CLIENTE', 1, 0, 'C', true);
		$pdf->Cell(15, 5, 'P.C', 1, 0, 'C', true);
		$pdf->Cell(20, 5, 'BASE', 1, 0, 'C', true);
		$pdf->Cell(20, 5, '%', 1, 0, 'C', true);
		$pdf->Cell(20, 5, 'COMISION', 1, 1, 'C', true);
		$i = 1;
		$comision = 0;
		$ttlbase = 0;
		$ttlcomision = 0;

		while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {

			$fechaDif = fechaDif($row['solfecha'], $row['dcafepag']);
			if ($fechaDif <= 0) {
				$fechaDif = 0;
			}

			$porcentaje = $row['solpover1'];
			$comision = $row['solbase'] * ($porcentaje / 100);
			$ttlbase = $ttlbase + $row['solbase'];
			$ttlcomision = $ttlcomision + $comision;

			$pdf->SetFont('LucidaConsole', '', 6);
			if ($i % 2 == 0) $x = 255;
			else $x = 235;
			$pdf->SetFillColor($x, $x, $x);
			$pdf->SetX(5);
			$pdf->Cell(50, 5, $row['empnombre'], 1, 0, 'L', true);
			$pdf->Cell(20, 5, $row['solid'], 1, 0, 'C', true);
			$pdf->Cell(20, 5, $row['solfactura'], 1, 0, 'C', true);
			$pdf->Cell(20, 5, substr($row['solfechafac'], 0, 10), 1, 0, 'C', true);
			$pdf->Cell(15, 5, $row['solncuota'], 1, 0, 'C', true);
			$pdf->Cell(20, 5, $row['dcafepag'], 1, 0, 'C', true);
			$pdf->Cell(57, 5, $row['clinombre'] . ' ' . $row['clinom2'] . ' ' . $row['cliape1'] . ' ' . $row['cliape2'], 1, 0, 'C', true);
			if ($row['solposcontado'] == 1) {
				$pdf->Cell(15, 5, 'SI', 1, 0, 'C', true);
			} else {
				$pdf->Cell(15, 5, 'NO', 1, 0, 'C', true);
			}
			$pdf->Cell(20, 5, number_format($row['solbase'], 2), 1, 0, 'R', true);
			$pdf->Cell(20, 5, $porcentaje, 1, 0, 'C', true);
			$pdf->Cell(20, 5, number_format($comision, 2), 1, 1, 'R', true);
			$i++;
		}
		$pdf->SetX(5);
		$pdf->SetFillColor(255, 255, 255);
		$pdf->Cell(50, 5, '', 0, 0, '', true);
		$pdf->SetFillColor(220, 220, 220);
		$pdf->Cell(20, 5, $num, 1, 0, 'C', true);
		$pdf->SetFillColor(255, 255, 255);
		$pdf->Cell(147, 5, '', 0, 0, '', true);
		$pdf->SetFillColor(220, 220, 220);
		$pdf->Cell(20, 5, number_format($ttlbase, 2), 1, 0, 'R', true);
		$pdf->SetFillColor(255, 255, 255);
		$pdf->Cell(20, 5, '', 0, 0, '', true);
		$pdf->SetFillColor(220, 220, 220);
		$pdf->Cell(20, 5, number_format($ttlcomision, 2), 1, 0, 'R', true);
	}
}
$pdf->Output('Arrendamineto detallado director ' . $fecha1 . ' a ' . $fecha2 . ' , ' . $nombre_director . '.pdf', 'd');
