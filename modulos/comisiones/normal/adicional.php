<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
?>
<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

	<script type="text/javascript">
		$(document).ready(function() {
			$('#fecha1').datepicker({
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
				maxDate: '+0D',
				onClose: function(selectedDate) {
					$('#fecha2').datepicker('option', 'minDate', selectedDate);
				}
			});
			$('#fecha2').datepicker({
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
				maxDate: '+0D',
				onClose: function(selectedDate) {
					$('#fecha1').datepicker('option', 'maxDate', selectedDate);
				}
			});
		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a class="current">Comisiones</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="lisadicional.php" method="get">
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">Proceso comisiones detallada adicional</legend>
						<p>
							<label for="adicional">Adicional:</label>
							<select id="adicional" name="adicional" class="selectpicker" data-live-search="true" title="Seleccione un usuario" data-width="100%">
								<option value="">SELECCIONE</option>
								<?php
								$qry = $db->query("SELECT * FROM usuarios ORDER BY usunombre");
								while ($row = $qry->fetch(PDO::FETCH_ASSOC))
									echo '<option value=' . $row['usuid'] . '>' . $row['usunombre'] . '</option>';
								?>
							</select>
						</p>
						<p>
							<label for="fechas">Intervalo:</label>
							<input type="text" name="fecha1" id="fecha1" class="validate[required] text-input fecha" /> 
							-
							<input type="text" name="fecha2" id="fecha2" class="validate[required] text-input fecha" />
						</p>
						<p class="boton">
							<button type="submit" class="btn btn-primary btngenera" name="genera" value="genera">generar</button>
						</p>
					</fieldset>
				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
</body>

</html>