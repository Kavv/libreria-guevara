<?php
$r = '../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
$director = $_GET['director'];
$fecha1 = $_GET['fecha1'];
$fecha2 = $_GET['fecha2'];
$filtro = 'director='.$director.'&fecha1='.$fecha1.'&fecha2='.$fecha2;
if($director == '') $qrynom = $db->query("SELECT * FROM usuarios WHERE usudirector = '1'");
else $qrynom = $db->query("SELECT * FROM usuarios WHERE usuid = '$director'");
?>
<!doctype html>
<html>
<head>
	<?php 
		require($r . 'incluir/src/head.php');
		require($r . 'incluir/src/head-form.php');
	?>	

<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('.tabla').dataTable({
		'bJQueryUI': true,
		'bStateSave': true,
		'bSort': false,
		'sPaginationType': 'full_numbers',
		'oLanguage': {
			'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
		},
	});
	$('.btnatras').button({ icons: { primary: 'ui-icon ui-icon-arrowthick-1-w' }});
	$('#dialog-message').dialog({
		height: 80,
		width: 'auto',
		modal: true
	});
	$('.pdf').click (function(){
		newSrc = $(this).attr('data-rel');
		$('#modal').html("<iframe src='" + newSrc + "' width='100%' height='100%'></iframe>");
		$('#modal').dialog({ modal: true, width: '600', height: '800', title : 'PDF de la solicitud' });
	});
});
</script>
</head>
<body>
<?php require($r.'incluir/src/login.php') ?>
<section id="principal">
<?php require($r.'incluir/src/cabeza.php') ?>
<?php require($r.'incluir/src/menu.php') ?>
<article id="cuerpo">
<article class="mapa">
<a href="#">Principal</a><div class="mapa_div"></div><a class="current">Comisiones</a>
</article>
<article id="contenido">
<h2>Comisiones detalladas director(es) de <?php echo $fecha1.' al '.$fecha2 ?></h2>
<div class="reporte">
<a href="pdf_director.php?<?php echo $filtro ?>"><img src="<?php echo $r ?>imagenes/iconos/pdf.png" title="pdf" /></a> <a href="#"><img src="<?php echo $r ?>imagenes/iconos/excel.png" title="csv" /></a>
</div>
<?php
while($rownom = $qrynom->fetch(PDO::FETCH_ASSOC)){
?>
<h2><?php echo $rownom['usunombre'] ?></h2>
<table class="tabla">
<thead>
<tr>
<th>Empresa</th>
<th>Solicitud</th>
<th>Factura</th>
<th>F. Solicitud</th>
<th>NC</th>
<th>F. Cuota</th>
<th>Cliente</th>
<th title="Posible Contado">P.C</th>
<th>Base</th>
<th> % </th>
<th>Comision</th>
<th></th>
</tr>
</thead>
<tbody>
<?php
$comision = 0;
$ttlbase = 0;
$ttlcomision = 0;
$sql = "SELECT * FROM ((((solicitudes LEFT JOIN carteras ON (solempresa = carempresa AND solfactura = carfactura)) LEFT JOIN detcarteras ON (solempresa = dcaempresa AND solfactura = dcafactura)) INNER JOIN empresas ON solempresa = empid) INNER JOIN clientes ON solcliente = cliid) INNER JOIN usuarios ON solover1 = usuid WHERE soltipo = 'NORMAL' AND ((dcacuota = '1' AND dcaestado = 'CANCELADO' AND solover1 = '".$rownom['usuid']."' AND dcafepag BETWEEN '$fecha1' AND '$fecha2') OR (solncuota = '0' AND solover1 = '".$rownom['usuid']."' AND solfecha BETWEEN '$fecha1' AND '$fecha2' AND (solestado = 'FACTURADO' OR solestado = 'ENVIADO' OR solestado = 'ENTREGADO'))) ORDER BY solfecha";
$qry = $db->query($sql);
$num = $qry->rowCount();
while($row = $qry->fetch(PDO::FETCH_ASSOC)){
	$porcentaje = $row['solpover1'];
	$comision = $row['solbase'] * ($porcentaje / 100);
	$ttlbase = $ttlbase + $row['solbase'];
	$ttlcomision = $ttlcomision + $comision;
?>
<tr>
<td title="<?php echo $row['empid'].' '.$row['empdigito'] ?>"><?php echo $row['empnombre'] ?></td>
<td align="center"><?php echo $row['solid'] ?></td>
<td align="center"><?php echo $row['solfactura'] ?></td>
<td align="center"><?php echo $row['solfecha'] ?></td>
<td align="center"><?php echo $row['solncuota'] ?></td>
<td align="center"><?php echo $row['dcafepag'] ?></td>
<td title="<?php echo $row['cliid'] ?>"><?php echo $row['clinombre'].' '.$row['clinom2'].' '.$row['cliape1'].' '.$row['cliape2'] ?></td>
<td align="center"> 
<?php if($row['solposcontado'] == 1) {echo "<span style='color:red'> SI </span>";} else { echo "NO";}  ?> 
</td>
<td align="right"><?php echo number_format($row['solbase'],0,',','.') ?></td>
<td align="center">% <?php echo $porcentaje ?></td>
<td align="right"><?php echo number_format($comision,0,',','.') ?></td>
<td align="center"><img src="<?php echo $r ?>imagenes/iconos/pdf.png" class="pdf" data-rel="<?php echo $r.'pdf/solicitudes/'.$row['solempresa'].'/'.$row['solid'].'.pdf' ?>" title="Solicitud" /></td>
</tr>
<?php
}
?>
</tbody>
<tfoot>
<tr>
<td></td>
<td align="center" bgcolor="#D1CFCF"><?php echo number_format($num,0,',','.') ?></td>
<td colspan="6"></td>
<td align="right" bgcolor="#D1CFCF"><?php echo number_format($ttlbase,0,',','.') ?></td>
<td></td>
<td align="right" bgcolor="#D1CFCF"><?php echo number_format($ttlcomision,0,',','.') ?></td>
<td></td>
</tr>
</tfoot>
</table>
<br/><br/>
<?php
}
?>
<p class="boton">
<button type="button" class="btnatras" onClick="carga(); location.href='director.php'">atras</button>
</p>
</article>
</article>
<?php require($r.'incluir/src/pie.php') ?>
</section>
<div id="modal" style="display:none"></div>
</body>
</html>