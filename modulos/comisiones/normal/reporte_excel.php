<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
require($r . 'incluir/funciones.php');
include($r . 'incluir/phpexcel/Classes/PHPExcel.php');


$asesor = $_GET['asesor'];
$fecha1 = $_GET['fecha1'];
$fecha2 = $_GET['fecha2'];

if ($asesor == '')
	$qrynom = $db->query("SELECT * FROM usuarios");
else
	$qrynom = $db->query("SELECT * FROM usuarios WHERE usuid = '$asesor'");


$objPHPExcel = new PHPExcel();

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN
		)
	)
);
$page = 0;
while ($rownom = $qrynom->fetch(PDO::FETCH_ASSOC)) {

	$sql =
		"SELECT * FROM 
			(
				(
					(
						(
							solicitudes LEFT JOIN carteras ON 
							(
								solempresa = carempresa AND solfactura = carfactura
							)
						) 
						LEFT JOIN detcarteras ON 
						(
							solempresa = dcaempresa AND solfactura = dcafactura
						)
					) INNER JOIN empresas ON solempresa = empid
				) INNER JOIN clientes ON solcliente = cliid
			) INNER JOIN usuarios ON solasesor = usuid 
			WHERE soltipo = 'NORMAL' AND 
			(
				(
					dcacuota = 1 AND dcaestado = 'CANCELADO' AND solasesor = '" . $rownom['usuid'] . "' AND dcafepag BETWEEN '$fecha1' AND '$fecha2'
				) 
				OR 
				(
					solncuota = '0' AND solasesor = '" . $rownom['usuid'] . "' AND solfecha BETWEEN '$fecha1' AND '$fecha2' AND (
						solestado = 'FACTURADO' OR solestado = 'ENVIADO' OR solestado = 'ENTREGADO' OR solestado = 'BANDEJA'
					)
				)
			) ORDER BY solfecha";
	//Fin

	$qry = $db->query($sql);
	$num = $qry->rowCount();

	if ($num > 0) {


		$nombre_titulo = $rownom['usunombre'];
		if($page > 0)
		{
			$objPHPExcel->createSheet($page);
			$nombre_titulo = "ASESORES";
		}
		$objPHPExcel->setActiveSheetIndex($page);
		$objPHPExcel->getActiveSheet($page)->setTitle(($page+1)."_".$rownom['usunombre']);
		$objPHPExcel->getActiveSheet($page)->getStyle('A1:L2')->applyFromArray($styleArray);
		$objPHPExcel->setActiveSheetIndex($page)->mergeCells('A1:L1');
		$objPHPExcel->setActiveSheetIndex($page)->setCellValue('A1', 'Arrendamiento detallado de ' . $fecha1 . ' a ' . $fecha2 . ' ' . $rownom['usunombre'] . ' ');




		$objPHPExcel->setActiveSheetIndex($page)
			->setCellValue('A2', 'EMPRESA')
			->setCellValue('B2', 'SOLICITUD')
			->setCellValue('C2', 'FACTURA')
			->setCellValue('D2', 'F. SOLICITUD')
			->setCellValue('E2', '# DE CUOTAS')
			->setCellValue('F2', 'F.CUOTA')
			->setCellValue('G2', 'DD')
			->setCellValue('H2', 'CLIENTE')
			->setCellValue('I2', 'POSIBLE CONTADO')
			->setCellValue('J2', 'BASE')
			->setCellValue('K2', '%')
			->setCellValue('L2', 'COMISION');

		$i = 3;
		$ttlbase = 0;
		$ttlcomision = 0;
		while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {

			$fechaDif = fechaDif($row['solfecha'], $row['dcafepag']);
			if ($fechaDif <= 0) {
				$fechaDif = 0;
			}

			if ($row['solncuota'] < 2 && $fechaDif < 11) {
				$porcentaje = $row['solcomision1'];
			} elseif ($row['solncuota'] < 2 && $fechaDif > 10 && $fechaDif < 31) {
				$porcentaje = $row['solcomision2'];
			} elseif ($row['solncuota'] == 2 && $fechaDif < 11) {
				$porcentaje = $row['solcomision3'];
			} else {
				$porcentaje = $row['solcomision'];
			}
			$comision = $row['solbase'] * ($porcentaje / 100);
			$ttlbase = $ttlbase + $row['solbase'];
			$ttlcomision = $ttlcomision + $comision;

			if ($row['solposcontado'] == 1) {
				$posible_contado = "Sí";
			} else {
				$posible_contado = "No";
			}


			$objPHPExcel->getActiveSheet()->getStyle('A' . $i . ':K' . $i)->applyFromArray($styleArray);

			$objPHPExcel->setActiveSheetIndex($page)
			->setCellValue('A' . $i, $row['empnombre'])
			->setCellValue('B' . $i, $row['solid'])
			->setCellValue('C' . $i, $row['solfactura']);

			$objPHPExcel->setActiveSheetIndex($page)
			->setCellValue('D' . $i, $row['solfechafac'])
			->setCellValue('E' . $i, $row['solncuota'])
			->setCellValue('F' . $i, $row['dcafepag']);

			$objPHPExcel->setActiveSheetIndex($page)
			->setCellValue('G' . $i, $fechaDif)
			->setCellValue('H' . $i, $row['clinombre'] . ' ' . $row['clinom2'] . ' ' . $row['cliape1'] . ' ' . $row['cliape2'])
			->setCellValue('I' . $i, $posible_contado)
			->setCellValue('J' . $i, ($row['solbase']))
			->setCellValue('K' . $i, $porcentaje)
			->setCellValue('L' . $i, ($comision));



			$i++;
		}
		// Totales 
		$objPHPExcel->setActiveSheetIndex($page)
		->setCellValue('A' . $i, '')
		->setCellValue('B' . $i, "Comisiones: ".$num)
		->setCellValue('C' . $i, '');

		$objPHPExcel->setActiveSheetIndex($page)
		->setCellValue('D' . $i, '')
		->setCellValue('E' . $i, '')
		->setCellValue('F' . $i, '');

		$objPHPExcel->setActiveSheetIndex($page)
		->setCellValue('G' . $i, '')
		->setCellValue('H' . $i, '')
		->setCellValue('I' . $i, '')
		->setCellValue('J' . $i, ($ttlbase))
		->setCellValue('K' . $i, '')
		->setCellValue('L' . $i, ($ttlcomision));
		
		$page++;
	}
}

// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Comisiones_normal_' . $nombre_titulo . ' del ' . $fecha1 . ' al ' . $fecha2 . '.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
