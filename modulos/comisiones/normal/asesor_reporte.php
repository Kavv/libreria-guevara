<?php
$r = '../../../';
require($r.'incluir/session.php');
include($r.'incluir/connection.php');
require($r.'incluir/funciones.php');
include($r.'incluir/fpdf/fpdf.php');


class PDF extends FPDF
{

//Cabecera de página
   function Header()
   {
	$asesor =$_GET['asesor'];
	$fecha1 =$_GET['fecha1'];
	$fecha2 =$_GET['fecha2'];
	$nomasesor = $_GET['nomasesor'];
	
	// Logo
	$this->Image('../../../imagenes/logos/logo1.jpg',70,10,160);
	// Arial 11
	$this->SetFont('Arial','',11);
	// Movernos a la derecha
	$this->Cell(130);
	// Título
	$this->Cell(30,85,'Arrendamiento detallado de '.$fecha1.' al '.$fecha2.'. ',0,0,'C');
	$this->Cell(34,95,' '.$nomasesor.' ',0,0,'R');
	// Salto de línea
	$this->Ln(20);
	
   }
 
   //Pie de página
   function Footer()
   {
    //Posición: a 1,5 cm del final
    $this->SetY(-15);
    //Arial italic 8
    $this->SetFont('Arial','I',8);
    //Número de página
    $this->Cell(0,10,'Reporte Generado '.$sql.'  '.date('d').' de '.date(' M ').' del'.date(' Y ').'.',0,0,'C');
   }
  
   //Tabla coloreada
function TablaColores($header)
{

//Colores, ancho de línea y fuente en negrita
$this->SetFillColor(94,93,93);
$this->SetTextColor(255);
$this->SetDrawColor(0,0,0);
$this->SetLineWidth(.2);
$this->SetFont('','B',9);
//Cabecera
$w = array(70,15,20,20,20,20,10,60,15,10,17);
for($i=0;$i<count($header);$i++)
$this->Cell($w[$i],7,$header[$i],1,0,'C',1);
$this->Ln();
//Restauración de colores y fuentes
$this->SetFillColor(224,235,255);
$this->SetTextColor(0);
$this->SetFont('','',6.5);
//Datos
	$asesor =$_GET['asesor'];
	$fecha1 =$_GET['fecha1'];
	$fecha2 =$_GET['fecha2'];
	$nomasesor = $_GET['nomasesor'];
	$qrynom = '';
	
	if(empty($asesor)) 
	{
	$sql = "SELECT * FROM usuarios WHERE usuasesor = '1'";
	$qry = mysql_query($sql);
//		$qrynom = $db->query();
	}
	if(!empty($asesor)) 
	{
	$sql = "SELECT * FROM usuarios WHERE usuasesor = '$asesor'";
	$qry = mysql_query($sql);
//			$qrynom = $db->query("SELECT * FROM usuarios WHERE usuasesor = '1'");
	}
/*		while($rownom = $qrynom->fetch(PDO::FETCH_ASSOC)){*/
		while($rownom = mysql_fetch_assoc($qry)){
		$comision = 0;
		$ttlbase = 0;
		$ttlcomision = 0;
		$sql1 = "SELECT * FROM ((((solicitudes LEFT JOIN carteras ON (solempresa = carempresa AND solfactura = carfactura))
		LEFT JOIN detcarteras ON (solempresa = dcaempresa AND solfactura = dcafactura)) 
		INNER JOIN empresas ON solempresa = empid) INNER JOIN clientes ON solcliente = cliid) 
		INNER JOIN usuarios ON solasesor = usuid WHERE soltipo = 'NORMAL' 
		AND ((dcacuota = '1' AND dcaestado = 'CANCELADO' AND solasesor = '".$rownom['usuid']."' 
		AND dcafepag BETWEEN '$fecha1' AND '$fecha2') OR (solncuota = '0' 
		AND solasesor = '".$rownom['usuid']."' AND solfecha BETWEEN '$fecha1' AND '$fecha2' 
		AND (solestado = 'FACTURADO' OR solestado = 'ENVIADO' OR solestado = 'ENTREGADO'))) 
		ORDER BY solfecha";
		$qry1 = mysql_query($sql1);
		$num = mysql_num_rows($qry1);
		//$num = $qry->rowCount();
		while($row = mysql_fetch_assoc($qry1)){
		//while($row = $qry->fetch(PDO::FETCH_ASSOC)){
		$fechaDif = fechaDif($row['solfecha'], $row['dcafepag']);
		if($fechaDif < 0) $fechaDif = 0;
		if($row['solncuota'] < 2 && $fechaDif < 11) $porcentaje = $row['solcomision1'];
		elseif($row['solncuota'] < 2 && $fechaDif > 10 && $fechaDif < 31) $porcentaje = $row['solcomision2'];
		elseif($row['solncuota'] == 2 && $fechaDif < 11) $porcentaje = $row['solcomision3'];
		else $porcentaje = $row['solcomision'];
		$comision = $row['solbase'] * ($porcentaje / 100);
		$ttlbase = $ttlbase + $row['solbase'];
		$ttlcomision = $ttlcomision + $comision;

		$this->Cell($w[0],6,''.$row['empnombre'].'','LRB',0,'R');
		$this->Cell($w[1],6,''.$row['solid'].'','LRB',0,'R');
		$this->Cell($w[2],6,''.$row['solfactura'].'','LRB',0,'R');
		$this->Cell($w[3],6,''.$row['solfecha'].'','LRB',0,'R');
		$this->Cell($w[4],6,''.$row['solncuota'].'','LRB',0,'R');
		$this->Cell($w[5],6,''.$row['dcafepag'].'','LRB',0,'R');
		$this->Cell($w[6],6,''.$fechaDif.'','LRB',0,'R');
		$this->Cell($w[7],6,''.$row['clinombre'].' '.$row['clinom2'].' '.$row['cliape1'].' '.$row['cliape2'].'','LRB',0,'R');
		$this->Cell($w[8],6,''.number_format($row['solbase'],0,',','.').'','LRB',0,'R');
		$this->Cell($w[9],6,'% '.$porcentaje.'','LRB',0,'R');
		$this->Cell($w[10],6,''.number_format($comision,0,',','.').'','LRB',0,'R');
		$this->Ln();
		
		} //fin de row

	} // fin de rownom
		
		$this->Cell($w[0],6,' ','LRB',0,'R');
		$this->Cell($w[1],6,' '.number_format($num,0,',','.').' ','LRB',0,'R'); // num de solicitudes
		$this->Cell($w[2],6,' ','LRB',0,'R');
		$this->Cell($w[3],6,' ','LRB',0,'R');
		$this->Cell($w[4],6,' ','LRB',0,'R');
		$this->Cell($w[5],6,' ','LRB',0,'R');
		$this->Cell($w[6],6,' ','LRB',0,'R');
		$this->Cell($w[7],6,' ','LRB',0,'R');
		$this->Cell($w[8],6,' '.number_format($ttlbase,0,',','.').' ','LRB',0,'R'); //total base
		$this->Cell($w[9],6,' ','LRB',0,'R');
		$this->Cell($w[10],6,' '.number_format($ttlcomision,0,',','.').' ','LRB',0,'R');
		$this->Cell(array_sum($w),0,'','T');
		$this->Ln();
} // fin funcion tabla colores
} // class PDF extends

$pdf=new PDF('L','mm','A4');
//Títulos de las columnas
$header=array('Empresa','Solicitud','Factura','F. Solicitud','No. Cuotas','F. Cuota','DD','Cliente','Base','%','Comision');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetY(65);
$pdf->TablaColores($header);
$pdf->Output();
?>