<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
require($r . 'incluir/funciones.php');
require($r . 'incluir/fpdf/fpdf.php');

$adicional = $_GET['adicional'];
$fecha1 = $_GET['fecha1'];
$fecha2 = $_GET['fecha2'];

if ($adicional == '')
	$qrynom = $db->query("SELECT * FROM usuarios");
else
	$qrynom = $db->query("SELECT * FROM usuarios WHERE usuid = '$adicional'");

class PDF extends FPDF
{
	function Header()
	{
		global $fecha1;
		global $fecha2;
		global $nombre_adicional;
		$this->SetFont('LucidaConsole', '', 6);
		$this->Cell(50, 5, date('Y/m/d'), 0, 1);
		$this->SetFont('LucidaConsole', '', 8);
		$this->Cell(280, 5, '--------------------------------------------------------------------------------------------------------------------------------------------------------------------', 0, 1, 'C');
		$this->Cell(280, 5, 'COMISIONES DETALLADA ADICIONAL DEL ' . $fecha1 . ' AL ' . $fecha2 . ' = ' . $nombre_adicional, 0, 1, 'C');
		$this->Cell(280, 5, '--------------------------------------------------------------------------------------------------------------------------------------------------------------------', 0, 1, 'C');
		$this->Ln(5);
	}

	function Footer()
	{
		$this->SetY(-15);
		$this->SetFont('LucidaConsole', '', 8);
		$this->Cell(0, 5, '----------------------------------------------------------------------------------------------------------------------------------------------------------------------', 0, 1, 'C');
		$this->SetFont('LucidaConsole', '', 8);
		$this->Cell(0, 5, 'Pagina ' . $this->PageNo() . '/{nb}', 0, 0, 'R');
	}
}

$pdf = new PDF();
$page = 0;
while ($rownom = $qrynom->fetch(PDO::FETCH_ASSOC)) {
	$nombre_adicional = $rownom['usunombre'];

	$sql =
		"SELECT * FROM 
		(
			(
				(
					(
						solicitudes LEFT JOIN carteras ON 
						(
							solempresa = carempresa AND solfactura = carfactura
						)
					) LEFT JOIN detcarteras ON 
					(
						solempresa = dcaempresa AND solfactura = dcafactura
					)
				) INNER JOIN empresas ON solempresa = empid
			) INNER JOIN clientes ON solcliente = cliid
		) INNER JOIN usuarios ON solover4 = usuid WHERE soltipo = 'NORMAL' AND 
		(
			(
				dcacuota = '1' AND dcaestado = 'CANCELADO' AND solover4 = '" . $rownom['usuid'] . "' AND dcafepag BETWEEN '$fecha1' AND '$fecha2'
			) 
			OR 
			(
				solncuota = '0' AND solover4 = '" . $rownom['usuid'] . "' AND solfecha BETWEEN '$fecha1' AND '$fecha2' AND 
				(
					solestado = 'FACTURADO' OR solestado = 'ENVIADO' OR solestado = 'ENTREGADO' OR solestado = 'BANDEJA'
				)
			)
		) ORDER BY solfecha";
	//Fin

	$qry = $db->query($sql);
	$num = $qry->rowCount();
	if ($num > 0) {
		$nombre_titulo = $rownom['usunombre'];
		if ($page > 0) {
			$nombre_titulo = "ADICIONALES";
		}
		$pdf->AddFont('LucidaConsole', '', 'lucon.php');
		$pdf->AliasNbPages();
		$pdf->AddPage('L');

		$pdf->SetFont('LucidaConsole', '', 7);
		$pdf->SetFillColor(220, 220, 220);
		$pdf->SetX(5);
		// Suma total 260
		$pdf->Cell(63, 5, 'EMPRESA', 1, 0, 'C', true);
		$pdf->Cell(20, 5, 'REFERENCIA S.', 1, 0, 'C', true);
		$pdf->Cell(20, 5, 'REFERENCIA F.', 1, 0, 'C', true);
		$pdf->Cell(20, 5, 'FECHA REF. S.', 1, 0, 'C', true);
		$pdf->Cell(15, 5, 'NC', 1, 0, 'C', true);
		$pdf->Cell(20, 5, 'F. CUOTA', 1, 0, 'C', true);

		$pdf->Cell(68, 5, 'CLIENTE', 1, 0, 'C', true);

		$pdf->Cell(20, 5, 'TOTAL', 1, 0, 'C', true);
		$pdf->Cell(20, 5, 'NUM.', 1, 0, 'C', true);
		$pdf->Cell(20, 5, 'VALOR GIRADO', 1, 1, 'C', true);
		$i = 1;
		$comision = 0;
		$ttlbase = 0;
		$ttlcomision = 0;
		while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
			$porcentaje = $row['solpover4'];
			$comision = $row['solbase'] * ($porcentaje / 100);
			$ttlbase = $ttlbase + $row['solbase'];
			$ttlcomision = $ttlcomision + $comision;

			$pdf->SetFont('LucidaConsole', '', 6);
			if ($i % 2 == 0) $x = 255;
			else $x = 235;
			$pdf->SetFillColor($x, $x, $x);
			$pdf->SetX(5);
			$pdf->Cell(63, 5, $row['empnombre'], 1, 0, 'L', true);
			$pdf->Cell(20, 5, $row['solid'], 1, 0, 'C', true);
			$pdf->Cell(20, 5, $row['solfactura'], 1, 0, 'C', true);
			$pdf->Cell(20, 5, substr($row['solfechafac'], 0, 10), 1, 0, 'C', true);
			$pdf->Cell(15, 5, $row['solncuota'], 1, 0, 'C', true);
			$pdf->Cell(20, 5, $row['dcafepag'], 1, 0, 'C', true);
			$pdf->Cell(68, 5, $row['clinombre'] . ' ' . $row['clinom2'] . ' ' . $row['cliape1'] . ' ' . $row['cliape2'], 1, 0, 'C', true);
			$pdf->Cell(20, 5, number_format($row['solbase'], 2), 1, 0, 'R', true);
			$pdf->Cell(20, 5, $porcentaje, 1, 0, 'C', true);
			$pdf->Cell(20, 5, number_format($comision, 2), 1, 1, 'R', true);
			$i++;
		}
		$pdf->SetX(5);
		$pdf->SetFillColor(255, 255, 255);
		$pdf->Cell(63, 5, '', 0, 0, '', true);
		$pdf->SetFillColor(220, 220, 220);
		$pdf->Cell(20, 5, $num, 1, 0, 'C', true);
		$pdf->SetFillColor(255, 255, 255);
		$pdf->Cell(143, 5, '', 0, 0, '', true);
		$pdf->SetFillColor(220, 220, 220);
		$pdf->Cell(20, 5, number_format($ttlbase, 2), 1, 0, 'R', true);
		$pdf->SetFillColor(255, 255, 255);
		$pdf->Cell(20, 5, '', 0, 0, '', true);
		$pdf->SetFillColor(220, 220, 220);
		$pdf->Cell(20, 5, number_format($ttlcomision, 2), 1, 0, 'R', true);
		
		$page++;
	}
}
$pdf->Output('Comisiones detalladas Adicional de ' . $fecha1 . ' a ' . $fecha2 . ' , ' . $nombre_titulo . '.pdf', 'd');
