<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
require($r . 'incluir/phpexcel/Classes/PHPExcel.php');
require($r . 'incluir/funciones.php');

$adicional = $_GET['adicional'];
$fecha1 = $_GET['fecha1'];
$fecha2 = $_GET['fecha2'];
$filtro = 'adicional=' . $adicional . '&fecha1=' . $fecha1 . '&fecha2=' . $fecha2;
if ($adicional == '')
	$qrynom = $db->query("SELECT * FROM usuarios");
else
	$qrynom = $db->query("SELECT * FROM usuarios WHERE usuid = '$adicional'");

$objPHPExcel = new PHPExcel();

$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN
		)
	)
);

$page = 0;
while ($rownom = $qrynom->fetch(PDO::FETCH_ASSOC)) {

	$sql =
		"SELECT * FROM 
		(
			(
				(
					(
						solicitudes LEFT JOIN carteras ON 
						(
							solempresa = carempresa AND solfactura = carfactura
						)
					) LEFT JOIN detcarteras ON 
					(
						solempresa = dcaempresa AND solfactura = dcafactura
					)
				) INNER JOIN empresas ON solempresa = empid
			) INNER JOIN clientes ON solcliente = cliid
		) INNER JOIN usuarios ON solover4 = usuid WHERE soltipo = 'NORMAL' AND 
		(
			(
				dcacuota = '1' AND dcaestado = 'CANCELADO' AND solover4 = '" . $rownom['usuid'] . "' AND dcafepag BETWEEN '$fecha1' AND '$fecha2'
			) 
			OR 
			(
				solncuota = '0' AND solover4 = '" . $rownom['usuid'] . "' AND solfecha BETWEEN '$fecha1' AND '$fecha2' AND 
				(
					solestado = 'FACTURADO' OR solestado = 'ENVIADO' OR solestado = 'ENTREGADO' OR solestado = 'BANDEJA'
				)
			)
		) ORDER BY solfecha";
	//Fin

	$qry = $db->query($sql);
	$num = $qry->rowCount();

	if ($num > 0) {

		$nombre_titulo = $rownom['usunombre'];
		if ($page > 0) {
			$objPHPExcel->createSheet($page);
			$nombre_titulo = "ADICIONALES";
		}

		$titulo = "COMISIONES DETALLADAS ADICIONAL DEL $fecha1 AL $fecha2 "  . ', '. $rownom['usunombre'];

		$objPHPExcel->setActiveSheetIndex($page);
		$objPHPExcel->getActiveSheet($page)->setTitle(($page+1)."_".$rownom['usunombre']);
		$objPHPExcel->getActiveSheet()->getStyle('A1:K2')->applyFromArray($styleArray);
		$objPHPExcel->setActiveSheetIndex($page)->mergeCells('A1:K1')
			->setCellValue('A1', $titulo);
		$objPHPExcel->setActiveSheetIndex($page)
			->setCellValue('A2', 'EMPRESA')
			->setCellValue('B2', 'SOLICITUD')
			->setCellValue('C2', 'FACTURA')
			->setCellValue('D2', 'F. SOLICITUD')
			->setCellValue('E2', '# DE CUOTAS')
			->setCellValue('F2', 'F.CUOTA')
			->setCellValue('G2', 'DD')
			->setCellValue('H2', 'CLIENTE')
			->setCellValue('I2', 'BASE')
			->setCellValue('J2', '%')
			->setCellValue('K2', 'COMISION');

		$i = 3;
		$comision = 0;
		$ttlbase = 0;
		$ttlcomision = 0;
		while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
			$fechaDif = fechaDif($row['solfechafac'], $row['dcafepag']);
			if ($fechaDif <= 0) {
				$fechaDif = 0;
			}

			$porcentaje = $row['solpover4'];
			$comision = $row['solbase'] * ($porcentaje / 100);
			$ttlbase = $ttlbase + $row['solbase'];
			$ttlcomision = $ttlcomision + $comision;


			$objPHPExcel->getActiveSheet()->getStyle('A' . $i . ':K' . $i)->applyFromArray($styleArray);
			$objPHPExcel->setActiveSheetIndex($page)
				->setCellValue('A' . $i, $row['empnombre'])
				->setCellValue('B' . $i, $row['solid'])
				->setCellValue('C' . $i, $row['solfactura'])
				->setCellValue('D' . $i, $row['solfechafac'])
				->setCellValue('E' . $i, $row['solncuota'])
				->setCellValue('F' . $i, $row['dcafepag'])
				->setCellValue('G' . $i, $fechaDif)
				->setCellValue('H' . $i, $row['clinombre'] . ' ' . $row['clinom2'] . ' ' . $row['cliape1'] . ' ' . $row['cliape2'])
				->setCellValue('I' . $i, ($row['solbase']))
				->setCellValue('J' . $i, $porcentaje)
				->setCellValue('K' . $i, ($comision));
			$i++;
		}
		// Totales 
		$objPHPExcel->setActiveSheetIndex($page)
		->setCellValue('A' . $i, '')
		->setCellValue('B' . $i, "Comisiones: ".$num)
		->setCellValue('C' . $i, '');

		$objPHPExcel->setActiveSheetIndex($page)
		->setCellValue('D' . $i, '')
		->setCellValue('E' . $i, '')
		->setCellValue('F' . $i, '');

		$objPHPExcel->setActiveSheetIndex($page)
		->setCellValue('G' . $i, '')
		->setCellValue('H' . $i, '')
		->setCellValue('I' . $i, ($ttlbase))
		->setCellValue('J' . $i, '')
		->setCellValue('K' . $i, ($ttlcomision));
		
		$page++;
	}
}

// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Comisiones detalladas Adicional del ' . $fecha1 . ' al ' . $fecha2 . ', ' . $nombre_titulo .'.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
