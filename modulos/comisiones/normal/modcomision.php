<?php
	$r = '../../../';
	require($r . 'incluir/session.php');
	require($r . 'incluir/connection.php');
	require($r . 'incluir/funciones.php');
	$empresa = $_GET['empresa'];
	$id = $_GET['id'];
	$asesor = $_GET['asesor'];
	$fecha1 = $_GET['fecha1'];
	$fecha2 = $_GET['fecha2'];
	$row = $db->query("SELECT * FROM (solicitudes INNER JOIN empresas ON solempresa = empid) INNER JOIN usuarios ON solasesor = usuid WHERE solempresa = '$empresa' AND solid = '$id'")->fetch(PDO::FETCH_ASSOC);
	$filtro = 'asesor=' . $asesor . '&fecha1=' . $fecha1 . '&fecha2=' . $fecha2;
?>
<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a class="current">Comisiones</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="lisasesor.php?<?php echo $filtro ?>" method="post">
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">Actualizar Comision Asesor</legend>
						<p>
							<label for="asesor">Empresa: </label>
							<input type="hidden" name="empresa" value="<?php echo $row['solempresa'] ?>" /><?php echo $row['empnombre'] ?>
						</p>
						<p>
							<label for="asesor">Solicitud: </label>
							<input type="hidden" name="id" value="<?php echo $row['solid'] ?>" /><?php echo $row['solid'] ?>
						</p>
						<p>
							<label for="asesor">Asesor: </label>
							<input type="hidden" name="asesor" value="<?php echo $row['solasesor'] ?>" /><?php echo $row['usunombre'] ?>
						</p>

						<p>
							<label for="asesor">Base: </label>
							<input type="text" name="base" value="<?php echo $row['solbase'] ?>" class="valor validate[required] text-input" />
						</p>
						<p>
							<label for="comision">Comision: </label>
							<input type="text" name="comision" class="porcentaje validate[required]" value="<?php echo $row['solcomision'] ?>" />
							<label>Comision (< 2c & < 11d):</label>
							<input type="text" name="comision1" class="porcentaje validate[required]" value="<?php echo $row['solcomision1'] ?>" />
						</p>
						<p>
							<label for="comision2">Comision (< 2c &> 10d && < 31d): </label>
							<input type="text" name="comision2" class="porcentaje validate[required]" value="<?php echo $row['solcomision2'] ?>" />
							<label>Comision (= 2c & < 11d):</label>
							<input type="text" name="comision3" class="porcentaje validate[required]" value="<?php echo $row['solcomision3'] ?>" />
						</p>
						<p>
							<label for="over1">Director: </label>
							<select name="over1" class="nombre" class="valor validate[required] text-input">
								<?php
								if ($row['solover1'] != '') {
									$row2 = $db->query("SELECT * FROM usuarios WHERE usuid = '" . $row['solover1'] . "'")->fetch(PDO::FETCH_ASSOC);
									echo '<option value="' . $row2['usuid'] . '">' . $row2['usunombre'] . '</option>';
								}
								echo '<option value="">SELECCIONE</option>';
								$qry = $db->query("SELECT * FROM usuarios WHERE usudirector = '1' AND usuid <> '" . $row['solover1'] . "' ORDER BY usunombre");
								while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
									echo '<option value=' . $row2['usuid'] . '>' . $row2['usunombre'] . '</option>';
								}
								?>
							</select>
							<input type="text" name="pover1" class="porcentaje validate[required] text-input" value="<?php echo $row['solpover1'] ?>" />
						</p>
						<p>
							<label for="over2">Gerente comercial: </label>
							<select name="over2" class="nombre">
								<?php
								if ($row['solover2'] != '') {
									$row2 = $db->query("SELECT * FROM usuarios WHERE usuid = '" . $row['solover2'] . "'")->fetch(PDO::FETCH_ASSOC);
									echo '<option value="' . $row2['usuid'] . '">' . $row2['usunombre'] . '</option>';
								}
								echo '<option value="">SELECCIONE</option>';
								$qry = $db->query("SELECT * FROM usuarios WHERE usuid <> '" . $row['solover2'] . "' ORDER BY usunombre");
								while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
									echo '<option value=' . $row2['usuid'] . '>' . $row2['usunombre'] . '</option>';
								}
								?>
							</select>
							<input type="text" name="pover2" class="porcentaje" value="<?php echo $row['solpover2'] ?>" />
						</p>
						<p>
							<label for="over3">Gerente general: </label>
							<select name="over3" class="nombre">
								<?php
								if ($row['solover3'] != '') {
									$row2 = $db->query("SELECT * FROM usuarios WHERE usuid = '" . $row['solover3'] . "'")->fetch(PDO::FETCH_ASSOC);
									echo '<option value="' . $row2['usuid'] . '">' . $row2['usunombre'] . '</option>';
								}
								echo '<option value="">SELECCIONE</option>';
								$qry = $db->query("SELECT * FROM usuarios WHERE usuid <> '" . $row['solover2'] . "' ORDER BY usunombre");
								while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
									echo '<option value=' . $row2['usuid'] . '>' . $row2['usunombre'] . '</option>';
								}
								?>
							</select>
							<input type="text" name="pover3" class="porcentaje" value="<?php echo $row['solpover3'] ?>" />
						</p>
						<p>
							<label for="over4">Adicional: </label>
							<select name="over4" class="nombre">
								<?php
								if ($row['solover4'] != '') {
									$row2 = $db->query("SELECT * FROM usuarios WHERE usuid = '" . $row['solover4'] . "'")->fetch(PDO::FETCH_ASSOC);
									echo '<option value="' . $row2['usuid'] . '">' . $row2['usunombre'] . '</option>';
								}
								echo '<option value="">SELECCIONE</option>';
								$qry = $db->query("SELECT * FROM usuarios WHERE usuid <> '" . $row['solover4'] . "' ORDER BY usunombre");
								while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
									echo '<option value=' . $row2['usuid'] . '>' . $row2['usunombre'] . '</option>';
								}
								?>
							</select>
							<input type="text" name="pover4" class="porcentaje" value="<?php echo $row['solpover4'] ?>" />
						</p>

						<p><label for="relacionista">Relacionista: </label>
							<select name="relacionista" class="nombre">
								<?php
								if ($row['solrelacionista'] != '') {
									$row2 = $db->query("SELECT * FROM usuarios WHERE usuid = '" . $row['solrelacionista'] . "'")->fetch(PDO::FETCH_ASSOC);
									echo '<option value="' . $row2['usuid'] . '">' . $row2['usunombre'] . '</option>';
								}
								echo '<option value="">SELECCIONE</option>';
								$qry = $db->query("SELECT * FROM usuarios WHERE usuid <> '" . $row['solrelacionista'] . "' AND usurelacionista = '1' ORDER BY usunombre");
								while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
									echo '<option value=' . $row2['usuid'] . '>' . $row2['usunombre'] . '</option>';
								}
								?>
							</select>
							<input type="text" name="prelacionista" class="porcentaje" value="<?php echo $row['solprelacionista'] ?>" />
						</p>

						<p>
							<label for="jeferel">Jefe relacionista: </label>
							<select name="jeferel" class="nombre">
								<?php
								if ($row['soljeferel'] != '') {
									$row2 = $db->query("SELECT * FROM usuarios WHERE usuid = '" . $row['soljeferel'] . "'")->fetch(PDO::FETCH_ASSOC);
									echo '<option value="' . $row2['usuid'] . '">' . $row2['usunombre'] . '</option>';
								}
								echo '<option value="">SELECCIONE</option>';
								$qry = $db->query("SELECT * FROM usuarios WHERE usuid <> '" . $row['soljeferel'] . "' ORDER BY usunombre");
								while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
									echo '<option value=' . $row2['usuid'] . '>' . $row2['usunombre'] . '</option>';
								}
								?>
							</select>
							<input type="text" name="pjeferel" class="porcentaje" value="<?php echo $row['solpjeferel'] ?>" />
						</p>

						<p class="boton">
							<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href='lisasesor.php?<?php echo $filtro ?>'">atras</button>
							<button type="submit" class="btn btn-primary btnmodificar" name="modificar" value="modificar">Modificar</button>
						</p>
					</fieldset>
				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
</body>

</html>