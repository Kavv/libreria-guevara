<?php
$r = '../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
require($r.'incluir/phpexcel/Classes/PHPExcel.php');
require($r.'incluir/funciones.php');

$adicional = $_GET['adicional'];
$fecha1 = $_GET['fecha1'];
$fecha2 = $_GET['fecha2'];
$filtro = 'adicional='.$adicional.'&fecha1='.$fecha1.'&fecha2='.$fecha2;
if($adicional == '') $qrynom = $db->query("SELECT * FROM usuarios");
else $qrynom = $db->query("SELECT * FROM usuarios WHERE usuid = '$adicional'");

$rownom = $qrynom->fetch(PDO::FETCH_ASSOC);

$titulo = "COMISIONES DETALLADAS DE $fecha1 AL $fecha2 ".$rownom['usunombre'];



$objPHPExcel = new PHPExcel();
$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN
		)
	)
);
$objPHPExcel->getActiveSheet()->getStyle('A1:H2')->applyFromArray($styleArray);
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:H1')
			->setCellValue('A1', $titulo);
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A2', 'EMPRESA')
			->setCellValue('B2', 'REFERENCIA SOLICITUD ')	
            ->setCellValue('C2', 'REFERENCIA FACTURA ')
			->setCellValue('D2', 'FECHA REFERENCIA SOLICITUD')
			->setCellValue('E2', 'NUMERO DE CUOTAS')
			->setCellValue('F2', 'FECHA CUOTA')
            ->setCellValue('G2', 'DD')
			->setCellValue('H2', 'CLIENTE');
			
$i = 3;
$comision = 0;
$ttlbase = 0;
$ttlcomision = 0;
$sql = "SELECT * FROM ((((solicitudes LEFT JOIN carteras ON (solempresa = carempresa AND solfactura = carfactura)) LEFT JOIN detcarteras ON (solempresa = dcaempresa AND solfactura = dcafactura)) INNER JOIN empresas ON solempresa = empid) INNER JOIN clientes ON solcliente = cliid) INNER JOIN usuarios ON solover4 = usuid WHERE soltipo = 'NORMAL' AND ((dcacuota = '1' AND dcaestado = 'CANCELADO' AND solover4 = '".$rownom['usuid']."' AND dcafepag BETWEEN '$fecha1' AND '$fecha2') OR (solncuota = '0' AND solover4 = '".$rownom['usuid']."' AND solfecha BETWEEN '$fecha1' AND '$fecha2' AND (solestado = 'FACTURADO' OR solestado = 'ENVIADO' OR solestado = 'ENTREGADO' OR solestado = 'BANDEJA'))) ORDER BY solfecha";
$qry = $db->query($sql);
$num = $qry->rowCount();
while($row = $qry->fetch(PDO::FETCH_ASSOC)){
	$fechaDif = fechaDif($row['solfechafac'], $row['dcafepag']);
	if($fechaDif < 0) {$fechaDif = 0;}
	
	$porcentaje = $row['solpover4'];
	$comision = $row['solbase'] * ($porcentaje / 100);
	$ttlbase = $ttlbase + $row['solbase'];
	$ttlcomision = $ttlcomision + $comision;
	
	
	$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':H'.$i)->applyFromArray($styleArray);
	$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('A'.$i, $row['empnombre'])
    	->setCellValue('B'.$i, $row['solid'])
		->setCellValue('C'.$i, $row['solfactura'])
		->setCellValue('D'.$i, $row['solfecha'] )
		->setCellValue('E'.$i, $row['solncuota'])
		->setCellValue('F'.$i, $row['dcafepag'])
    	->setCellValue('G'.$i, $fechaDif)
		->setCellValue('H'.$i, $row['clinombre'].' '.$row['clinom2'].' '.$row['cliape1'].' '.$row['cliape2'])
		;
	$i++;
	}

// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Comisiones Detalladas del '.$fecha1.' al '.$fecha2.'.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
?>