<?php
	$r = '../../../';
	require($r . 'incluir/session.php');
	require($r . 'incluir/connection.php');
	$director = $_GET['director'];
	$fecha1 = $_GET['fecha1'];
	$fecha2 = $_GET['fecha2'];
	$filtro = 'director=' . $director . '&fecha1=' . $fecha1 . '&fecha2=' . $fecha2;
	if ($director == '') 
		$qrynom = $db->query("SELECT * FROM usuarios WHERE usudirector = '1'");
	else 
		$qrynom = $db->query("SELECT * FROM usuarios WHERE usuid = '$director'");
?>
<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>

	<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
	<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('.tabla').dataTable({
				'bJQueryUI': true,
				'bSort': false,
				'sPaginationType': 'full_numbers',
				'oLanguage': {
					'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
				},
			});
			$('.pdf').click(function() {
				newSrc = $(this).attr('data-rel');
				if (validation_file(newSrc) == 200)
					$('#modal').html("<iframe src='" + newSrc + "' width='100%' height='100%'></iframe>");
				else
					$('#modal').html("<spam>El PDF no fue encontrado</spam>");
				$('#modal').dialog({
					modal: true,
					width: '600',
					height: '800',
					title: 'PDF de la solicitud'
				});
			});
		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a class="current">Comisiones</a>
			</article>
			<article id="contenido">
				<h2>Comisiones detalladas director(es) de <?php echo $fecha1 . ' al ' . $fecha2 ?></h2>
				<div class="reporte">
					<a href="pdf_director.php?<?php echo $filtro ?>">
						<img src="<?php echo $r ?>imagenes/iconos/pdf.png" title="Todos" />
					</a> 
				</div>
				<?php
				while ($rownom = $qrynom->fetch(PDO::FETCH_ASSOC)) {
					
					$sql = 
						"SELECT * FROM 
						(
							(
								(
									(
										solicitudes LEFT JOIN carteras ON 
										(
											solempresa = carempresa AND solfactura = carfactura
										)
									) 
									LEFT JOIN detcarteras ON 
									(
										solempresa = dcaempresa AND solfactura = dcafactura
									)
								) 
								INNER JOIN empresas ON solempresa = empid
							) 
							INNER JOIN clientes ON solcliente = cliid
						) 
						INNER JOIN usuarios ON solover1 = usuid WHERE soltipo = 'NORMAL' 
						AND 
						(
							(
								dcacuota = 1 AND dcaestado = 'CANCELADO' AND solover1 = '" . $rownom['usuid'] . "' AND dcafepag BETWEEN '$fecha1' AND '$fecha2'
							) 
							OR 
							(
								solncuota = '0' AND solover1 = '" . $rownom['usuid'] . "' AND solfecha BETWEEN '$fecha1' AND '$fecha2' AND 
								(
									solestado = 'FACTURADO' OR solestado = 'ENVIADO' OR solestado = 'ENTREGADO' OR solestado = 'BANDEJA'
								)
							)
						) ORDER BY solfecha";
					//Fin consulta

					$qry = $db->query($sql);
					$num = $qry->rowCount();
					if($num > 0)
					{

				?>
						<h2><?php echo $rownom['usunombre'] ?></h2>
						
						<div class="reporte">
							<a href="pdf_director.php?<?php echo '&director=' . $rownom['usuid'] .'&fecha1=' . $fecha1 . '&fecha2='. $fecha2   ?>">
								<img src="<?php echo $r ?>imagenes/iconos/pdf.png" title="Individual" />
							</a> 
						</div>
						<table class="tabla">
							<thead>
								<tr>
									<th>Empresa</th>
									<th>Solicitud</th>
									<th>Factura</th>
									<th title="Fecha de solicitud">F. Fac</th>
									<th># Cuota</th>
									<th title="Fecha de pago cuota">F. Cuota</th>
									<th>Cliente</th>
									<th title="Posible Contado">P.C</th>
									<th>Base</th>
									<th title="Porcentaje de Comision"> % </th>
									<th>Comision</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<?php
								$comision = 0;
								$total_base = 0;
								$total_comision = 0;
								while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
									$porcentaje = $row['solpover1'];
									$comision = $row['solbase'] * ($porcentaje / 100);
									$total_base = $total_base + $row['solbase'];
									$total_comision = $total_comision + $comision;
								?>
									<tr>
										<td title="<?php echo $row['empid'] . ' ' . $row['empdigito'] ?>"><?php echo $row['empnombre'] ?></td>
										<td align="center"><?php echo $row['solid'] ?></td>
										<td align="center"><?php echo $row['solfactura'] ?></td>
										<td align="center"><?php echo substr($row['solfechafac'], 0, 10) ?></td>
										<td align="center"><?php echo $row['solncuota'] ?></td>
										<td align="center"><?php echo $row['dcafepag'] ?></td>
										<td title="<?php echo $row['cliid'] ?>"><?php echo $row['clinombre'] . ' ' . $row['clinom2'] . ' ' . $row['cliape1'] . ' ' . $row['cliape2'] ?></td>
										<td align="center">
											<?php if ($row['solposcontado'] == 1) {
												echo "<span style='color:red'> SI </span>";
											} else {
												echo "NO";
											}  ?>
										</td>
										<td align="right"><?php echo number_format($row['solbase'], 2) ?></td>
										<td align="center"><?php echo $porcentaje ?></td>
										<td align="right"><?php echo number_format($comision, 2) ?></td>
										<td align="center"><img src="<?php echo $r ?>imagenes/iconos/pdf.png" class="pdf" data-rel="<?php echo $r . 'pdf/solicitudes/' . $row['solempresa'] . '/' . $row['solid'] . '.pdf' ?>" title="Solicitud" /></td>
									</tr>
								<?php
								}
								?>
							</tbody>
							<tfoot>
								<tr>
									<td></td>
									<td align="center" bgcolor="#D1CFCF">Total: <?php echo $num ?></td>
									<td colspan="6"></td>
									<td align="right" bgcolor="#D1CFCF"><?php echo number_format($total_base, 2) ?></td>
									<td></td>
									<td align="right" bgcolor="#D1CFCF"><?php echo number_format($total_comision, 2) ?></td>
									<td></td>
								</tr>
							</tfoot>
						</table>
						<br /><br />
				<?php
					} 
				}
				?>
				<p class="boton">
					<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href='director.php'">atras</button>
				</p>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<div id="modal" style="display:none"></div>
</body>

</html>