<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
require($r . 'incluir/funciones.php');

$fecha1 = $_GET['fecha1'];
$fecha2 = $_GET['fecha2'];
$asesor = $_SESSION['id'];

$filtro = 'asesor=' . $asesor . '&fecha1=' . $fecha1 . '&fecha2=' . $fecha2;

$qrynom = $db->query("SELECT * FROM usuarios WHERE usuid = '$asesor'");
?>
<!doctype html>
<html>


<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>

	<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
	<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('.tabla').dataTable({
				'bJQueryUI': true,
				'sPaginationType': 'full_numbers',
				'oLanguage': {
					'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
				},
				'aoColumnDefs': [{
					'bSortable': false,
					'aTargets': [12]
				}],
			});
			$('.modificar').click(function() {
				$('#dialog2').dialog({
					modal: true,
					width: '600',
					height: '800',
					title: 'PDF de la solicitud'
				});
			});
			$('.pdf').click(function() {
				newSrc = $(this).attr('data-rel');
				if (validation_file(newSrc) == 200)
					$('#modal').html("<iframe src='" + newSrc + "' width='100%' height='100%'></iframe>");
				else
					$('#modal').html("<spam>El PDF no fue encontrado</spam>");
				$('#modal').dialog({
					modal: true,
					width: '600',
					height: '800',
					title: 'PDF de la solicitud'
				});
			});
		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a class="current">Comisiones</a>
			</article>
			<article id="contenido">
				<h2>Arrendamiento detallado del <?php echo $fecha1 . ' al ' . $fecha2 ?></h2>
				
				<?php
				while ($rownom = $qrynom->fetch(PDO::FETCH_ASSOC)) {

					// conclucion: mostrara los movimientos de las cartera
					$sql = 
						"SELECT * FROM (
							(
								(
									(
										solicitudes LEFT JOIN carteras ON 
										(
											solempresa = carempresa AND solfactura = carfactura
										)
									) 
									LEFT JOIN detcarteras ON 
									(
										solempresa = dcaempresa AND solfactura = dcafactura
									)
								) INNER JOIN empresas ON solempresa = empid
							) INNER JOIN clientes ON solcliente = cliid
						) INNER JOIN usuarios ON solasesor = usuid 
						WHERE soltipo = 'NORMAL' AND 
						(
							(
								dcacuota = 1 AND dcaestado = 'CANCELADO' AND solasesor = '" . $rownom['usuid'] . "' AND dcafepag BETWEEN '$fecha1' AND '$fecha2'
							) 
							OR 
							(
								solncuota = '0' AND solasesor = '" . $rownom['usuid'] . "' AND solfecha BETWEEN '$fecha1' AND '$fecha2' AND (
									solestado = 'FACTURADO' OR solestado = 'ENVIADO' OR solestado = 'ENTREGADO' OR solestado = 'BANDEJA'
								)
							)
						) ORDER BY solfecha";
					//Fin


					$qry = $db->query($sql);
					$num = $qry->rowCount();
					if($num > 0)
					{

				?>
						<h2><?php echo $rownom['usunombre'];  ?></h2>
						<div class="reporte">
							<a href="pdf_asesores.php<?php echo "?asesor=" . $rownom['usuid'] . "&fecha1=" . $fecha1 . "&fecha2=" . $fecha2  ?>">
								<img src="<?php echo $r ?>imagenes/iconos/pdf.png" title="pdf" />
							</a> 
							<a href="reporte_excel.php<?php echo "?asesor=" . $rownom['usuid'] . "&fecha1=" . $fecha1 . "&fecha2=" . $fecha2 ?>">
								<img src="<?php echo $r ?>imagenes/iconos/excel.png" title="csv" />
							</a>
						</div>
						<table class="tabla">
							<thead>
								<tr>
									<th>Empresa</th>
									<th>Solicitud.</th>
									<th>Factura</th>
									<th title="Fecha de facturación">F. Fac</th>
									<th>Cuota #</th>
									<th>F. Cuota</th>
									<th>DD</th>
									<th>Cliente</th>
									<th title="Posible Contado">P.C</th>
									<th>Base</th>
									<th title="Porcentaje de Comision">%</th>
									<th>Comision</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<?php
								$comision = 0;
								$total_base = 0;
								$total_comision = 0;
								while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
									// Diferencia de dias entre la fecha de facturacion y la fecha de pago
									$fechaDif = fechaDif($row['solfechafac'], $row['dcafepag']);
									if ($fechaDif <= 0) {
										$fechaDif = 0;
									}
									// Estos parametros son establecidos en usuario->comisiones->Asesores
									if ($row['solncuota'] < 2 && $fechaDif < 11) {
										$porcentaje = $row['solcomision1'];
									} elseif ($row['solncuota'] < 2 && $fechaDif > 10 && $fechaDif < 31) {
										$porcentaje = $row['solcomision2'];
									} elseif ($row['solncuota'] == 2 && $fechaDif < 11) {
										$porcentaje = $row['solcomision3'];
									} else {
										$porcentaje = $row['solcomision'];
									}
									// solbase es el valor de la solicitud con el que se aplica la comision del vendedor
									$comision = $row['solbase'] * ($porcentaje / 100);
									$total_base = $row['solbase'] + $total_base;
									$total_comision = $total_comision + $comision;
								?>
									<tr>
										<td title="<?php echo $row['empid'] . ' ' . $row['empdigito'] ?>"><?php echo $row['empnombre'] ?></td>
										<td align="center"><?php echo $row['solid'] ?></td>
										<td align="center"><?php echo $row['solfactura'] ?></td>
										<td align="center"><?php echo substr($row['solfechafac'], 0, 10); ?></td>
										<td align="center"><?php echo $row['solncuota'] ?></td>
										<td align="center"><?php if ($row['solncuota'] == 0) {
																echo "Cuota inicial";
															} else {
																echo $row['dcafepag'];
															} ?></td>
										<td align="center"><?php echo $fechaDif ?></td>
										<td title="<?php echo $row['cliid'] ?>"><?php echo $row['clinombre'] . ' ' . $row['clinom2'] . ' ' . $row['cliape1'] . ' ' . $row['cliape2'] ?></td>
										<td align="center">
											<?php if ($row['solposcontado'] == 1) {
												echo "<span style='color:red'> SI </span>";
											} else {
												echo "NO";
											}  ?>
										</td>
										<td align="right"><?php echo number_format($row['solbase'], 2) ?></td>
										<td align="center"><?php echo $porcentaje ?></td>
										<td align="right"><?php echo number_format($comision, 2) ?></td>
										<td align="center"><img src="<?php echo $r ?>imagenes/iconos/pdf.png" class="pdf" data-rel="<?php echo $r . 'pdf/solicitudes/' . $row['solempresa'] . '/' . $row['solid'] . '.pdf' ?>" title="Solicitud" /></td>
									</tr>
								<?php
								}
								?>
							</tbody>
							<tfoot>
								<tr>
									<td></td>
									<td align="center" bgcolor="#D1CFCF"><?php echo "Total: ".$num ?></td>
									<td colspan="7"></td>
									<td align="right" bgcolor="#D1CFCF"><?php echo number_format($total_base, 2) ?></td>
									<td></td>
									<td align="right" bgcolor="#D1CFCF"><?php echo number_format($total_comision, 2) ?></td>
									<td></td>
								</tr>
							</tfoot>
						</table>
						<br /><br />
				<?php
					} 
				}
				?>
				<p class="boton">
					<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href='asesor_prop.php'">atras</button>
				</p>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<?php
	if (isset($_GET['error'])) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['error'] . '</div>';
	elseif (isset($_GET['mensaje'])) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['mensaje'] . '</div>';
	?>
	<div id="modal" style="display:none"></div>
</body>

</html>