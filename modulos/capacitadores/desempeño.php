<?php
$r = '../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');

$error = $_GET['error'];
$mensaje = $_GET['mensaje'];
$hoy = date("Y-m-d"); 

$sql = "SELECT * FROM usuarios WHERE usucallcenter = '1' AND usuperfil <> 56 ORDER BY usunombre ASC";


$qry = $db->query($sql);


?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>. : : S o p h y a : : .</title>
<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/css/normalize.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/jquery/css/smoothness/jquery-ui-1.10.3.custom.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/css/css.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/css/menu.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/validation/css/validationEngine.jquery.css" />
<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/js/jquery-ui-1.10.3.custom.js"></script>
<script type="text/javascript" src="<?php echo $r ?>incluir/validation/js/languages/jquery.validationEngine-es.js"></script>
<script type="text/javascript" src="<?php echo $r ?>incluir/validation/js/jquery.validationEngine.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#form').validationEngine({
		showOneMessage:true,
		onValidationComplete: function(form, status){
        	if(status) {               
            	carga();
            	return true;
			}
		}
	});
	$('.btnatras').button({ icons: { primary: 'ui-icon ui-icon-arrowthick-1-w' }});
	$('.btnsiguiente').button({ icons: { secondary: 'ui-icon ui-icon-arrowthick-1-e' }});	
});
</script>
<style type="text/css">
#cuerpo {min-width: 520px;}
#form { width: 80%;min-width: 500px; margin:5px auto }
#form fieldset { padding:10px; display:block }
#form legend { font-weight: bold; margin-left:5px; padding:5px }
#form label { display:inline-block; width:15%; text-align:right; margin:0.3em 1em 0 0 }
#form p { margin:4px 0 }
#nombre {width:70%;}
#empresa {width:70%;}
#lista {margin:8px 5%;}
.radio {width:30px;margin:4px;}
</style>
</head>
<body>
<article id="cuerpo">
<article id="contenido">
<form id="form" name="form" action="insertar.php" method="post">
<h2>Evaluacion de desempeño del capacitador <?php echo $hoy; ?></h2>
<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
<legend class="ui-widget ui-widget-header ui-corner-all">Datos Personales</legend>
<p>
<label>Nombre: </label>
<input type="text" id="nombre" name="nombre" class="nombre validate[required, custom[onlyLetterSp]] text-input" title="Digite Su Nombre Completo" />
</p>
<p>
<p>
<label>Empresa: </label>
<input type="text" id="empresa" name="empresa" class="nombre validate[required, custom[onlyLetterSp]] text-input" title="Digite el nombre de la empresa" />
</p>
<p>
<label>Edad: </label>
<input type="text" name="edad" class="edad validate[required, custom[onlyNumberSp]] text-input" title="Digite edad" />
</p>
<p>
<label>E-mail: </label>
<input type="text" name="email" class="email validate[required, custom[email]] text-input" />
</p>
<p>
<label>Celular: </label>
<input type="text" name="celular" class="telefono validate[required, custom[phone]] text-input" title="Digite numero celular" />
</p>
</fieldset>
</br></br>
<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
<legend class="ui-widget ui-widget-header ui-corner-all">Preguntas sobre la capacitacion</legend>
<p>Las preguntas acontinuacion seran evaluadas de 1 a 5 de la siguiente manera.</p>
<ol id="lista">
<li>Satisfecho</li>
<li>Bueno</li>
<li>Aceptable</li>
<li>Insuficiente</li>
<li>Deficiente</li>
</ol>
</br>
<p>
<fieldset>
    <legend>¿Llego puntualmente el capacitador?</legend>
    <input class="radio" name="puntu" type="radio" value="1" />1</br>
    <input class="radio" name="puntu" type="radio" value="2" />2</br>
    <input class="radio" name="puntu" type="radio" value="3" />3</br>
    <input class="radio" name="puntu" type="radio" value="4" />4</br>
    <input class="radio" name="puntu" type="radio" value="5" />5</br>
</fieldset>
</p>
<p>
<fieldset>
    <legend>¿Es buena la presentacion personal del capacitador?</legend>
    <input class="radio" name="presen" type="radio" value="1" />1</br>
    <input class="radio" name="presen" type="radio" value="2" />2</br>
    <input class="radio" name="presen" type="radio" value="3" />3</br>
    <input class="radio" name="presen" type="radio" value="4" />4</br>
    <input class="radio" name="presen" type="radio" value="5" />5</br>
</fieldset>
</p>
<p>
<fieldset>
    <legend>¿Es buena la presentacion personal del capacitador?</legend>
    <input class="radio" name="presen" type="radio" value="1" />1</br>
    <input class="radio" name="presen" type="radio" value="2" />2</br>
    <input class="radio" name="presen" type="radio" value="3" />3</br>
    <input class="radio" name="presen" type="radio" value="4" />4</br>
    <input class="radio" name="presen" type="radio" value="5" />5</br>
</fieldset>
</p>
<p>
<fieldset>
    <legend>¿Demuestra conocimiento del tema?</legend>
    <input class="radio" name="conoci" type="radio" value="1" />1</br>
    <input class="radio" name="conoci" type="radio" value="2" />2</br>
    <input class="radio" name="conoci" type="radio" value="3" />3</br>
    <input class="radio" name="conoci" type="radio" value="4" />4</br>
    <input class="radio" name="conoci" type="radio" value="5" />5</br>
</fieldset>
</p>
<p>
<fieldset>
    <legend>¿Respondio a todas las preguntas propuestas por la audiencia?</legend>
    <input class="radio" name="respreg" type="radio" value="1" />1</br>
    <input class="radio" name="respreg" type="radio" value="2" />2</br>
    <input class="radio" name="respreg" type="radio" value="3" />3</br>
    <input class="radio" name="respreg" type="radio" value="4" />4</br>
    <input class="radio" name="respreg" type="radio" value="5" />5</br>
</fieldset>
</p>
<p>
<fieldset>
    <legend>¿Respondio a todas las preguntas propuestas por la audiencia?</legend>
    <input class="radio" name="respreg" type="radio" value="1" />1</br>
    <input class="radio" name="respreg" type="radio" value="2" />2</br>
    <input class="radio" name="respreg" type="radio" value="3" />3</br>
    <input class="radio" name="respreg" type="radio" value="4" />4</br>
    <input class="radio" name="respreg" type="radio" value="5" />5</br>
</fieldset>
</p>
<p>
<fieldset>
    <legend>¿Los temas revisados son aplicables a su actividad laboral?</legend>
    <input class="radio" name="temaapli" type="radio" value="1" />1</br>
    <input class="radio" name="temaapli" type="radio" value="2" />2</br>
    <input class="radio" name="temaapli" type="radio" value="3" />3</br>
    <input class="radio" name="temaapli" type="radio" value="4" />4</br>
    <input class="radio" name="temaapli" type="radio" value="5" />5</br>
</fieldset>
</p>
<p>
<fieldset>
    <legend>¿Los temas revisados respondieron a sus intereses y expectativas?</legend>
    <input class="radio" name="inteexpe" type="radio" value="1" />1</br>
    <input class="radio" name="inteexpe" type="radio" value="2" />2</br>
    <input class="radio" name="inteexpe" type="radio" value="3" />3</br>
    <input class="radio" name="inteexpe" type="radio" value="4" />4</br>
    <input class="radio" name="inteexpe" type="radio" value="5" />5</br>
</fieldset>
</p>
<p>
<fieldset>
    <legend>¿El desarrollo de los temas se realizó de lo sencillo a lo complejo?</legend>
    <input class="radio" name="desatema" type="radio" value="1" />1</br>
    <input class="radio" name="desatema" type="radio" value="2" />2</br>
    <input class="radio" name="desatema" type="radio" value="3" />3</br>
    <input class="radio" name="desatema" type="radio" value="4" />4</br>
    <input class="radio" name="desatema" type="radio" value="5" />5</br>
</fieldset>
</p>
<p>
<fieldset>
    <legend>¿Las dinamicas de trabajo permitieron a la audiencia estar activa?</legend>
    <input class="radio" name="dinamaudi" type="radio" value="1" />1</br>
    <input class="radio" name="dinamaudi" type="radio" value="2" />2</br>
    <input class="radio" name="dinamaudi" type="radio" value="3" />3</br>
    <input class="radio" name="dinamaudi" type="radio" value="4" />4</br>
    <input class="radio" name="dinamaudi" type="radio" value="5" />5</br>
</fieldset>
</p>
<p>
<fieldset>
    <legend>¿El desarrollo de los temas se realizo de lo sencillo a lo complejo?</legend>
    <input class="radio" name="desatema" type="radio" value="1" />1</br>
    <input class="radio" name="desatema" type="radio" value="2" />2</br>
    <input class="radio" name="desatema" type="radio" value="3" />3</br>
    <input class="radio" name="desatema" type="radio" value="4" />4</br>
    <input class="radio" name="desatema" type="radio" value="5" />5</br>
</fieldset>
</p>
<p>
<fieldset>
    <legend>¿Por parte del capacitador la informacion que proporciono fue clara, completa y correcta?</legend>
    <input class="radio" name="inforcapaci" type="radio" value="1" />1</br>
    <input class="radio" name="inforcapaci" type="radio" value="2" />2</br>
    <input class="radio" name="inforcapaci" type="radio" value="3" />3</br>
    <input class="radio" name="inforcapaci" type="radio" value="4" />4</br>
    <input class="radio" name="inforcapaci" type="radio" value="5" />5</br>
</fieldset>
</p>
<p>
<fieldset>
    <legend>¿El capacitador ayudo a la comprension de los temas con ejemplos, analogías, anecdotas, etc?</legend>
    <input class="radio" name="ayudacapa" type="radio" value="1" />1</br>
    <input class="radio" name="ayudacapa" type="radio" value="2" />2</br>
    <input class="radio" name="ayudacapa" type="radio" value="3" />3</br>
    <input class="radio" name="ayudacapa" type="radio" value="4" />4</br>
    <input class="radio" name="ayudacapa" type="radio" value="5" />5</br>
</fieldset>
</p>
</fieldset>
<p class="boton">
<button type="submit" class="btnsiguiente" name="siguiente" value="siguiente">Siguiente</button>
</p>
</article>
</form>
<?php
if(isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>'.$error.'</div>';
elseif(isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>'.$mensaje.'</div>';
?>
<div id="modal" style="display:none"></div>
<div id="dialog2" style="display:none"></div>
</body>
</html>