<?php
// LA ACCION PRINCIPAL DE ESTA PAGINA ES MOSTRAR LOS RESULTADOS DE LA CONSULTA GENERADA DESDE CONSULTAR.PHP
// LUEGO DE HABER RECIBIDO LAS VARIABLES POR POST DESDE CONSULTAR.PHP PROCEDEMOS A EVALUAR ESAS VARIABLES PARA MOSTRAR RESULTADOS DE LA CONSULTA O MODIFICAR SI ASI CORRESPONDE

$r = '../../../';
//INCLUIR SESION Y CONECCION
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
if(isset($_POST['modificar'])){ // SE VALIDA SI MODIFICAR ES TRUE
	$id = $_POST['id'];
	$nombre = strtoupper(trim($_POST['nombre'])); // LO RECIBIDO EN ESTE CAMPO LO CAMBIAMOS A MAYUSCULA
	$num = $db->query("SELECT * FROM mdespachos WHERE mdeid <> '$id' AND mdenombre = '$nombre'")->rowCount();// REALIZAMOS CONSULTA CON VARIABLES RECIBIDAS Y CONTAMOS LOS RESULTADOS OBTENIDOS
	if($num < 1) {  // SI LOS RESULTADOS DE LA CONSULTA SON MENORES A  1 REALIZAMOS LO SIGUIENTE
		$telefono = trim($_POST['telefono']);
		$direccion = strtoupper(trim($_POST['direccion']));
		$guia = 0;
		if(isset($_POST['guia']))
			$guia = $_POST['guia'];

		$qry = $db->query("UPDATE mdespachos SET mdenombre = '$nombre', mdetelefono = '$telefono', mdedireccion = '$direccion', mdeguia = '$guia' WHERE mdeid = '$id'"); // ACTUALIZAMOS LA INFORMACION CON LOS NUEVOS DATOS RECIBIDOS
		if($qry) $mensaje = 'Se actualizo el medio de despacho'; // MENSAJE EXITOSO
		else $error = 'No se actualizo el medio de despacho'; // MENSAJE ERROR
	}else $error = 'El nombre del medio de despacho ya existe';
}
if(isset($_GET['id1'])){ //VALIDAMOS SI GET ID1 ES TRUE 
	$id1 = $_GET['id1'];
	$num = $db->query("SELECT * FROM solicitudes WHERE solenvio = '$id1'")->rowCount(); // REALIZAMOS CONSULTA EN SOLICITUDES CON VARIABLES RECIBIDAS Y CONTAMOS LOS RESULTADOS
	if($num < 1) { // SI LOS RESULTADOS DE LA CONSULTA SON MENORES A  1 REALIZAMOS LO SIGUIENTE
		$qry = $db->query("DELETE FROM mdespachos WHERE mdeid = '$id1'");   //ELIMINAMOS EL MODO DE DESPACHO
		if($qry) $mensaje = 'Se elimino el medio de despacho'; // MENSAJE EXITOSO
		else $error = 'No se pudo eliminar el medio de despacho'; // MENSAJE ERROR
	}else $error = 'Hay solicitudes que posen este medio de despacho';
}
$id = $nombre = "";
if(isset($_POST['consultar'])){ //VALIDAMOS SI POST CONSULTAR ES TRUE
	$id = $_POST['id'];
	$nombre = $_POST['nombre'];
}else if (isset($_GET['id'])) { // SI LO RECIBIDO POR POST CONSULTAR NO ES TRUE
	$id = $_GET['id'];
	$nombre = $_GET['nombre'];
}
$filtro = 'nombre='.$nombre; // FILTRO GENERAL PARA ENVIAR POR GET
if($id != '' && $nombre == '') $sql = "SELECT * FROM mdespachos WHERE mdeid LIKE '%$id%'"; // VALIDAMOS LOS CAMPOS RECIBIDOS
elseif($nombre != '' && $id == '') $sql = "SELECT * FROM mdespachos WHERE mdenombre LIKE '%$nombre%'";
elseif($nombre != '' && $id != '') $sql = "SELECT * FROM mdespachos WHERE mdenombre LIKE '%$nombre%' AND mdeid LIKE '%$id%'";
else $sql = "SELECT * FROM mdespachos";
$qry = $db->query($sql);
?>
<!doctype html>
<html>
<head>

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
	<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
<script>
$(document).ready(function() {
	$('#tabla').dataTable({ //QRY PARA MANEJO Y VISUALIZACION DE TABLAS
		'bJQueryUI': true,
		'bStateSave': true,
		'sPaginationType': "full_numbers",
		'oLanguage': {
			'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
		},
		'aoColumnDefs': [
			{ 'bSortable': false, 'aTargets': [ 2, 3, 4, 5 ] }
    	],
	});
	
	$('.confirmar').click(function(e){ // VENTANA MODAL PARA CONFIRMAR ACCIONES
		e.preventDefault();
		var targetUrl = $(this).attr("href");
		var $dialog_link_follow_confirm = $('<div></div>').html('<p><span class="ui-icon ui-icon-trash" style="float:left; margin:2px 7px 7px 0;"></span>Esta seguro que desea eliminar el medio de despacho?</p>').
		dialog({
			title: 'Porfavor confirmar',
			buttons :{
				'Si' : function() { 
					carga();
					window.location.href = targetUrl; 
				},
				'No' : function() { 
					$(this).dialog("close"); 
				}
			},
			modal: true,
			width: 'auto',
			height: 'auto'
		}
	);
	$dialog_link_follow_confirm.dialog('open');
	});

});
</script>
</head>
<body>
<?php require($r.'incluir/src/login.php') ?> <!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
<section id="principal">
<?php require($r.'incluir/src/cabeza.php') ?>  <!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
<?php require($r.'incluir/src/menu.php') ?> <!-- INCLUIMOS MENU PRINCIPAL -->
<article id="cuerpo">
<article class="mapa">
<a href="#">Principal</a><div class="mapa_div"></div><a href="#">Administracion</a><div class="mapa_div"></div><a class="current">Medios de despacho</a>
</article>
<article id="contenido">
<h2>Listado de medios de despacho</h2>
<div class="reporte">
  <!-- DESCARGABLES -->
</div>
<!-- INICIO DE TABLA -->
<table id="tabla">
<thead>
<tr>
<th>Identificacion</th>
<th>Nombre</th>
<th>Guia</th>
<th>Telefonol</th>
<th>Direccion</th>
<th></th>
<th></th>
</tr>
</thead>
<tbody>
<?php
while($row = $qry->fetch(PDO::FETCH_ASSOC)){ //INFORMACION DE LA CONSULTA
?>
<tr>
<td><?php echo $row['mdeid'].' '.$row['mdedigito'] ?></td>
<td><?php echo $row['mdenombre'] ?></td>
<td align="center"><?php if($row['mdeguia'] == '1') echo '<img src="'.$r.'imagenes/iconos/accept.png" />'; else echo '<img src="'.$r.'imagenes/iconos/cancel_.png" />' ?></td>
<td align="center"><?php echo $row['mdetelefono'] ?></td>
<td align="center"><?php echo $row['mdedireccion'] ?></td>
<td align="center"><a href="modificar.php?<?php echo 'id1='.$row['mdeid'].'&'.$filtro ?>" onClick="carga()" title="editar"><img src="<?php echo $r ?>imagenes/iconos/lapiz.png" class="grayscale" /></a></td> <!-- ENVIA POR GET LAS VARIABLES NECESARIAS PARA MODIFICAR UN MEDIO DE DESPACHO -->
<td align="center"><a href="listar.php?<?php echo 'id1='.$row['mdeid'].'&'.$filtro ?>" title="eliminar" class="confirmar"><img src="<?php echo $r ?>imagenes/iconos/basura.png" class="grayscale" /></a></td>  <!-- ENVIA LAS VARIABLES NECESARIAS PARA ELIMINAR UN MEDIO DE DESPACHO JUNTO CON VENTANA MODAL PARA CONFIRMAR -->
</tr>
<?php
}
?>
</tbody>
</table>
<p class="boton">
<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href='consultar.php'">Atras</button>  <!-- BOTON PARA VOLVER A CONSULTAR -->
</p>
</article>
</article>
<?php require($r.'incluir/src/pie.php') ?> <!-- BOTON PARA VOLVER A LA PARTE SUPERIOR  -->
</section>
<?php
if(isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>'.$error.'</div>'; // VENTANA MODAL ERROR
elseif(isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>'.$mensaje.'</div>';  // VENTANA MODAL EXITOSO
?>
</body>
</html>