<?php
// LA ACCION PRINCIPAL DE ESTA PAGINA ES MODIFICAR UN MEDIO DE DESPACHO ENVIANDOLO A LISTAR

$r = '../../../';
//INCLUIR SESION Y CONECCION
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
$row = $db->query("SELECT * FROM mdespachos WHERE mdeid = '" . $_GET['id1'] . "'")->fetch(PDO::FETCH_ASSOC); // CONSULTA A LA BD DE MEDIOS DE DESPACHO CON VARUABLES RECIBIDAS POR GET
$filtro = 'nombre=' . $_GET['nombre'];  //FILTRO GENERAL
?>
<!doctype html>
<html>

<head>
	<meta charset="utf-8">

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
		<?php require($r . 'incluir/src/menu.php') ?>
		<!-- INCLUIMOS MENU PRINCIPAL -->
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Administracion</a>
				<div class="mapa_div"></div><a class="current">Medios de despacho</a>
			</article>
			<article id="contenido">
				<div class="ui-widget">
					<form id="form" name="form" action="listar.php?<?php echo $filtro ?>" method="post">
						<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
							<legend class="ui-widget ui-widget-header ui-corner-all">Modificar medio de despacho</legend>
							<p>
								<label for="id">Identificacion:</label> <!-- CAMPO IDENTIFICACION -->
								<input type="text" name="id" class="id" value="<?php echo $row['mdeid'] ?>" readonly />
							</p>
							<p>
								<label for="nombre">Nombre:</label> <!-- CAMPO NOMBRE -->
								<input type="text" name="nombre" class="nombre validate[required] text-input" value="<?php echo $row['mdenombre'] ?>" title="Digite el medio de despacho" />
							</p>
							<p>
								<label for="telefono">Telefono:</label> <!-- CAMPO TELEFONO -->
								<input type="text" name="telefono" class="telefono validate[required, custom[phone]] text-input" value="<?php echo $row['mdetelefono'] ?>" />
							</p>
							<p>
								<label for="direccion">Direccion:</label> <!-- CAMPO DIRECCION -->
								<input type="text" name="direccion" class="direccion validate[required] text-input" value="<?php echo $row['mdedireccion'] ?>" />
							</p>
							<p>
								<label for="guia" class="not-w">Guia:</label> <!-- CAMPO GUIA -->
								<input type="checkbox" name="guia" value="1" <?php if ($row['mdeguia'] == 1) { ?> checked <?php } ?>  />
							</p>
							<p class="boton">
								<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href='listar.php?<?php echo $filtro ?>'">Atras</button> <!--  BOTON PARA VOLVER ATRAS JUNTO CON FILTRO GENERAL-->
								<button type="submit" class="btn btn-primary btnmodificar" name="modificar" value="modificar">Modificar</button> <!-- BOTON MODIFICAR -->
							</p>
						</fieldset>
					</form>
				</div>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
		<!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
	</section>
</body>

</html>