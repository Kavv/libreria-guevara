<?php
// LA ACCION PRINCIPAL DE ESTA PAGINA ES INSERTAR UN NUEVO MODO DE DESPACHO

$r = '../../../';
//INCLUIR SESION Y CONECCION
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
if (isset($_POST['insertar'])) { // SE VALIDA SI LA INFORMACION VIENE DE INSERTAR 
	// SE ASIGNAN VARIABLES A LOS DATOS RECIBIDOS
	$id = strtoupper(trim($_POST['id']));
	$nombre = strtoupper(trim($_POST['nombre']));
	$telefono = trim($_POST['telefono']);
	$direccion = strtoupper(trim($_POST['direccion']));
	$digitador = $_SESSION['id'];
	$guia = 0;

	$sql = "INSERT INTO mdespachos (mdeid, mdenombre, mdetelefono, mdedireccion, mdeguia, digitador) VALUES ('$id', '$nombre', '$telefono', '$direccion', '$guia', '$digitador')"; // SE EVALUAN LOS CAMPOS RECIBIDOS PARA LUEGO EJECUTAR EL SQL
	$qry = $db->query($sql);
	if ($qry) $mensaje = 'Se inserto el medio de despacho'; // MENSAJE MODAL EXITOSO
	else $error = 'No se pudo insertar el medio de despacho'; // MENSAJE MODAL ERROR
}
?>
<!doctype html>
<html>

<head>

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<script type="text/javascript" src="<?php echo $r ?>incluir/funciones.js"></script>
	<script>
		$(document).ready(function() {
			$("#tid").change(function(event) { // VALIDACION DE CAMPOS
				var id = $("#tid").find(':selected').val();
				if (id == 1) {
					$("#ide").load('cedula.php');
				} else if (id == 2) {
					$("#ide").load('nit.php');
				}
			});
		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
		<?php require($r . 'incluir/src/menu.php') ?>
		<!-- INCLUIMOS MENU PRINCIPAL -->
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Administracion</a>
				<div class="mapa_div"></div><a class="current">Medios de despacho</a>
			</article>
			<article id="contenido">
				<div class="ui-widget">
					<form id="form" name="form" action="insertar.php" method="post">
						<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
							<legend class="ui-widget ui-widget-header ui-corner-all">Insertar medio de despacho</legend>
							<p id="ide">
								<label for="id">Código:</label> 
								<input type="text" name="id" class="id validate[required, ajax[ajaxUserCall]] text-input" title="Digite la cedula del proveedor" />
							</p>
							<p>
								<label for="nombre">Nombre:</label> <!-- CAMPO NOMBRE -->
								<input type="text" name="nombre" class="nombre validate[required, ajax[ajaxNameCall]] text-input" title="Digite el nombre del medio de despacho" />
							</p>
							<p>
								<label for="telefono">Telefono:</label> <!-- CAMPO TELEFONO -->
								<input type="text" name="telefono" class="telefono validate[required, custom[phone]] text-input" />
							</p>
							<p>
								<label for="direccion">Direccion:</label> <!-- CAMPO DIRECCION -->
								<input type="text" name="direccion" class="direccion validate[required] text-input" />
							</p>
							<p class="boton">
								<button type="submit" class="btn btn-primary btninsertar" name="insertar" value="insertar">Insertar</button> <!-- BOTON INSERTAR -->
							</p>
						</fieldset>
					</form>
				</div>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<?php
	if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>'; //VENTANA MODAL ERROR
	elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>'; //VENTANA MODAL EXITOSO
	?>
</body>

</html>