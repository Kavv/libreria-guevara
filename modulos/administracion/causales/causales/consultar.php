<?php
// LA ACCION PRINCIPAL ES ESTA PAGINA ES CONSULTAR LAS CAUSALES POR LAS CUALES SE PUEDE CANCELAR UNA SOLICITUD.

$r = '../../../../';
//INCLUIR SESION Y CONECCION
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
?>
<!doctype html>
<html>

<head>

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
		<?php require($r . 'incluir/src/menu.php') ?>
		<!-- INCLUIMOS MENU PRINCIPAL -->
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Administracion</a>
				<div class="mapa_div"></div><a href="#">Causales</a>
				<div class="mapa_div"></div><a class="current">Causales</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="listar.php" method="post">
					<!-- ENVIO FORMULARIO POR POST A LISTAR.PHP -->
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">Consultar causales</legend>
						<p>
							<label for="tcausal">Tipo de causal:</label> <!-- MOSTRAMOS TIPOS DE CAUSALES DESDE LA BD  -->
							<select id="tcausal" name="tcausal">
								<option value="">TODOS</option>
								<?php
								$qry = $db->query("SELECT * FROM tcausales ORDER BY tcanombre"); // QRY TIPO DE CAUSALES 
								while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
									echo '<option value=' . $row['tcaid'] . '>' . $row['tcanombre'] . '</option>'; // ASOCIAMOS POR NOMBRE DE LA CAUSAL Y EL ID QUE LA IDENTIFICA
								}
								?>
							</select>
						</p>
						<p>
							<label for="nombre">Nombre:</label> <!-- BUSCAR POR SIMILITUDES EN EL NOMBRE DEL TIPO DE LA CAUSAL CON UN LIKE EN LA BD -->
							<input type="text" name="nombre" class="nombre" title="Digite el nombre de la causal" />
						</p>
						<div class="row">
							<div class="col-md-12 col-lg-6">
								<button type="submit" class="btnconsulta btn btn-primary btn-block mt-2" name="consultar" value="Buscar">Consultar</button> <!-- BOTON CONSULTAR -->
							</div>
							<div class="col-md-12 col-lg-6">
								<button type="submit" class="btnconsulta btn btn-primary btn-block mt-2" name="show-all" value="Buscar">Mostrar Todo</button>							</div>
						</div>
					</fieldset>
				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
		<!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
	</section>
</body>

</html>