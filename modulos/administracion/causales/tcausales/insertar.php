<?php
// LA ACCION PRINCIPAL DE ESTA PAGINA ES INSERTAR UN NUEVO TIPO DE CAUSAL

$r = '../../../../';
//INCLUIR SESION Y CONECCION
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
if (isset($_POST['insertar'])) { // VALIDAMOS QUE POST INSERTAR SEA TRUE
	$nombre = trim(strtoupper($_POST['nombre']));
	$qry = $db->query("INSERT INTO tcausales (tcanombre) VALUES ('$nombre')"); // REALIZAMOS EL INSERT EN LA BD DEL NUEVO TIPO DE CAUSAL
	if ($qry) $mensaje = 'Se inserto el tipo de causal'; // MENSAJE MODAL EXITOSO
	else $error = 'No se pudo insertar el tipo de causal'; // MENSAJE MODAL ERROR
}
?>
<!doctype html>
<html>

<head>

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<script>
	</script>

</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
		<?php require($r . 'incluir/src/menu.php') ?>
		<!-- INCLUIMOS MENU PRINCIPAL -->
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Administracion</a>
				<div class="mapa_div"></div><a href="#">Causales</a>
				<div class="mapa_div"></div><a class="current">Tipos de causales</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="" method="post">
					<!-- FORMULARIO ENVIADO POR POST A SI MISMO -->
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">Insertar tipo de causal</legend>
						<p>
							<label for="nombre">Nombre:</label>
							<input type="text" name="nombre" class="nombre validate[required, ajax[ajaxNameCall]] text-input" title="Digite el tipo de causal" /> <!-- CAMPO PARA INGRESAR NOMBRE DE LA NUEVA CAUSAL -->
						</p>
						<p class="boton">
							<button type="submit" id="botton" class="btn btn-primary btninsertar" name="insertar" value="insertar">insertar</button> <!-- BOTON INSERTAR  -->
						</p>
					</fieldset>
				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
		<!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
	</section>
	<?php
	if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>'; //VENTANA MODAL ERROR
	elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>'; //VENTANA MODAL EXITOSO
	?>
</body>

</html>