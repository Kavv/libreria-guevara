<?php
// LA ACCION PRINCIPAL DE ESTA PAGINA ES MODIFICAR UN TIPO CAUSAL Y ENVIANDOLO A LISTAR

$r = '../../../../';
//INCLUIR SESION Y CONECCION
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
$filtro = 'nombre=' . $_GET['nombre']; //FILTRO GENERAL 
$qry = $db->query("SELECT * FROM tcausales WHERE tcaid = '" . $_GET['id1'] . "'"); // CONSULTA A LA BD CON VARUABLES RECIBIDAS POR GET
$row = $qry->fetch(PDO::FETCH_ASSOC)  //RESULTADOS DE LA CONSULTA
?>
<!doctype html>
<html>

<head>

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>

</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
		<?php require($r . 'incluir/src/menu.php') ?>
		<!-- INCLUIMOS MENU PRINCIPAL -->
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Administracion</a>
				<div class="mapa_div"></div><a href="#">Causales</a>
				<div class="mapa_div"></div><a class="current">Tipos de causales</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="listar.php?<?php echo $filtro ?>" method="post">
					<!-- FORMULARIO ENVIADO POR POST A LISTAR.PHP JUNTO CON EL FILTRO GENERAL  -->
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">Modificar tipo de causal</legend>
						<p>
							<input type="hidden" name="id" value="<?php echo $row['tcaid'] ?>" /> <!-- INPUT OCULTO ALMACENA INFORMACION /-->
						</p>
						<p>
							<label for="nombre">Nombre:</label>
							<input type="text" name="nombre" class="nombre validate[required] text-input" value="<?php echo $row['tcanombre'] ?>" title="Digite el tipo de causal" />
						</p>
						<p class="boton">
							<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href='listar.php?<?php echo $filtro ?>'">Atras</button> <!-- VOLVER A LISTAR.PHP -->
							<button type="submit" class="btn btn-primary btnmodificar" name="modificar" value="modificar">Modificar</button> <!-- BOTON MODIFICAR -->
						</p>
					</fieldset>
				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
		<!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
	</section>
</body>

</html>