<?php
// LA ACCION PRINCIPAL DE ESTA PAGINA ES MODIFICAR EL NUMERO DE CEDULA DE UN CLIENTE EN TODAS LAS TABLAS DE LA BD QUE SE VEN AFECTADAS

$r = '../../../';
//INCLUIR SESION Y CONECCION
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

$cedinco = $cedco = $modificar = null;
if (isset($_POST['cedinco']))
	$cedinco = strtoupper(trim($_POST['cedinco'])); //CEDULA INCORRECTA
if (isset($_POST['cedco']))
	$cedco = strtoupper(trim($_POST['cedco'])); //CEDULA CORRECTA
if (isset($_POST['modificar']))
	$modificar = $_POST['modificar']; // VALIDAR MODIFICAR

if ($modificar == "modificar" && $cedinco != null && $cedco != null) { // SE AVALUA QUE ESTEN LOS CAMPOS REQUERIDOS PARA REALIZAR EL CAMBIO

	
	$sql = " SELECT * FROM clientes where cliid <> '$cedinco' AND cliid = '$cedco' "; //BUSCAR AL CLIENTE AL QUE SE LE VA A CAMBIAR EL NUM
	$qry = $db->query($sql); // SE EJECUTA LA CONSULTA

	if($qry->rowCount() == 0)
	{
		$sql = " SELECT * FROM clientes where cliid = '$cedinco' "; //BUSCAR AL CLIENTE AL QUE SE LE VA A CAMBIAR EL NUM
		$qry = $db->query($sql); // SE EJECUTA LA CONSULTA

		if ($qry->rowCount()) { //SE EVALUA QUE HAYAN RESULTADOS
			// SE REALIZAN ACONTINUACION LOS UPDATES NECESARIOS PARA REALIZAR EL CAMBIO EN LAS TABLAS QUE SE VEN AFECTADAS
			$qry = $db->query("update carteras set carcliente = '$cedco' where carcliente = '$cedinco';");
			$qry = $db->query("update clientes set cliid = '$cedco' where cliid = '$cedinco';");
			$qry = $db->query("update movimientos set movtercero = '$cedco' where movtercero = '$cedinco';");
			$qry = $db->query("update solicitudes set solcliente = '$cedco' where solcliente = '$cedinco';");
			$mensaje = "Se ha modificado la cedula exitosamente"; // MENSAJE EXITOSO
			
			// LOG DE ACCIONES REALIZADAS POR EL USUARIO
			$qrylogsregister = $db->query("SELECT * FROM usuarios INNER JOIN perfiles ON usuperfil = perid WHERE usuid = " . $_SESSION['id']); // VERIFICACION USUARIO POR ID DE SESION
			$rowlogsregister = $qrylogsregister->fetch(PDO::FETCH_ASSOC);
			$qrylogsregister = $db->query("INSERT INTO logs ( usuario, id_usuario , ip_usuario , accion_usuario, fecha_accion) VALUES ( '" . $rowlogsregister['usunombre'] . "', '" . $_SESSION['id'] . "', '" . $_SERVER['REMOTE_ADDR'] . "' , 'CAMBIO CEDULA CLIENTE " . $cedinco . " POR EL " . $cedco . " '  , '" . date("Y-m-d H:i:s") . "' );"); //ANEXO DE INFORMACION DE ACCION A LA BD TABLA LOGS
		}
		else 
			$error = "No se encontro ninguna coincidencia con " . $cedinco . " revise e intente nuevamente."; // MENSAJE ERROR
	}
	else
		$error = "No se actualizo. <br/> La nueva cedula ingresada ya se encuentra en uso por otro cliente.";
}

?>
<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<script type="text/javascript" src="<?php echo $r ?>incluir/funciones.js"></script>

</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
		<?php require($r . 'incluir/src/menu.php') ?>
		<!-- INCLUIMOS MENU PRINCIPAL -->
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a class="current">Mantenimiento</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="cambiarid.php" method="post">
					<!-- FORMULARIO ENVIADO POR POST A SI MISMO -->
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">Modificación de cedula del cliente</legend>
						<p>
							<label>Cedula registrada:</label> <!-- CAMPO IDENTIFICACION INCORRECTA -->
							<input name="cedinco" type="text" class="validate[required]" />
						</p>
						<p>
							<label>Reemplazar cedula por:</label> <!-- CAMPO IDENTIFICACION CORRECTA -->
							<input name="cedco" type="text" class="validate[required]" />
						</p>
						<p align="center">
							<button type="submit" class="btn btn-primary btnmodificar" name="modificar" value="modificar">Modificar</button> <!-- BOTON MODIFICAR -->
						</p>
					</fieldset>
				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
		<!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
	</section>
	<?php
	if (isset($error)) echo '<div id="dialog-message" title="Error" class="text-center"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>';
	elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>';
	?>
</body>

</html>