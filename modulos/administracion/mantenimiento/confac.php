<?php
// LA ACCION PRINCIPAL DE ESTA PAGINA ES ENVIAR DESDE UN FORMULARIO LOS DATOS NECESARIOS A lisfac.php PARA PODER LISTAR LA FACTURA QUE SE VA A REGENERAR

$r = '../../../';
//INCLUIR SESION Y CONECCION
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
?>
<!doctype html>
<html>

<head>

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>

</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
		<?php require($r . 'incluir/src/menu.php') ?>
		<!-- INCLUIMOS MENU PRINCIPAL -->
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a class="current">Mantenimiento</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="lisfac.php" method="post">
					<!-- FORMULARIO ENVIADO A lisfac.php POR POST PARA MOSTRAR DATOS DE LA FACTURA A REGENERAR -->
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">Consulta de factura a regenerar</legend>
						<p>
							<label for="empresa">Empresa:</label> <!-- SELECT EMPRESA -->
							<select name="empresa">
								<option value="">TODAS</option>
								<?php
								$qry = $db->query("SELECT * FROM empresas ORDER BY empnombre");
								while ($row = $qry->fetch(PDO::FETCH_ASSOC))
									echo '<option value=' . $row['empid'] . '>' . $row['empnombre'] . '</option>';
								?>
							</select>
						</p>
						<p>
							<label for="factura">Factura:</label> <!-- CAMPO FACTURA -->
							<input type="text" class="documento" name="prefijo" value="FV" readonly />
							<input type="text" name="numero" class="consecutivo" />
						</p>
						<p>
							<label for="cedula">Cliente:</label> <!-- CAMPO CLIENTE -->
							<input type="text" name="cedula" class="usuario" />
						</p>
						<div class="row">
							<div class="col-md-12 col-lg-6">
								<button type="submit" class="btnconsulta btn btn-primary btn-block mt-2" name="consultar" value="Buscar">consultar</button> <!-- BOTON CONSULTAR -->
							</div>
							<div class="col-md-12 col-lg-6">
								<button type="submit" class="btnconsulta btn btn-primary btn-block mt-2" name="show-all" value="Buscar">Mostrar Todo</button> </div>
						</div>
					</fieldset>
				</form>
			</article>
		</article>
</body>

</html>