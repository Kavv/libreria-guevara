<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');


if (isset($_POST['modificar'])) {
	$empresa = $_POST['empresa'];
	$id = $_POST['id'];
	if ($_FILES['imagen']['size'] < 1600000) {
		if (move_uploaded_file($_FILES['imagen']['tmp_name'], $r . 'pdf/solicitudes/' . $empresa . '/' . $id . '.pdf')) {
			$mensaje = 'Cambio realizaado con exito.';
		} else {
			$error = 'No se pudo cargar el archivo, intente nuevamente o contacte con el encargado.';
		}
	} else {
		$error = 'la digitalizacon excede el tamano permitido (max. 1.5MB)';
	}
}

?>
<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<script type="text/javascript" src="<?php echo $r ?>incluir/funciones.js"></script>

</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a class="current">Mantenimiento</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="pdfs.php" method="post" enctype="multipart/form-data">
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">Cambio de PDF</legend>
						<input type="hidden" name="tipo" value="<?php echo $tipo ?>" />
						<p>
							<label for="empresa">Empresa:</label>
							<select id="empresa" name="empresa" class="validate[required]">
								<option value="">SELECCIONE</option>
								<?php
								$qry = $db->query("SELECT * FROM empresas ORDER BY empnombre");
								while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
									echo '<option value=' . $row['empid'] . '>' . $row['empnombre'] . '</option>';
								}
								?>
							</select>
						</p>
						<p>
							<label for="id">Solicitud: </label>
							<input type="text" name="id" class="pedido validate[required, custom[onlyNumberSp]] text-input" title="Digite numero de solicitud" />
						</p>
						<p>
							<label>PDF (max. 1.5 Mb): </label>
							<input type="file" name="imagen" class="validate[required, checkFileType[pdf|PDF]]" />
						</p>
						<p class="boton">
							<button type="submit" class="btn btn-primary btnmodificar" name="modificar" value="modificar">Modificar</button>
						</p>
					</fieldset>
				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<?php
	if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>';
	elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>';
	?>
</body>

</html>