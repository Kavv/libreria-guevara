<?php
//ESTA PAGINA ESTA DESHABILITADA 03/10/2014 SEBASTIAN SALCEDO  
$r = "../../";
include($r."include/session.class.php");
include($r."include/mysql.class.php");
$session = new Session;
$mysql = new MySql;
$session->iniciar($r);
$mysql->conectar();
$mysql->base();
if($_POST["modificar"]){
	$nit = $_POST["empresa"];
	$solicitud = $_POST["solicitud"];
	$ide = $_POST["idee"];
	$num1 = mysql_num_rows(mysql_query("SELECT * FROM solicitudes WHERE solempresa = '".$nit."' AND solCodigo = '".$solicitud."'"));
	if($num1 > 0){
		$num2 = mysql_num_rows(mysql_query("SELECT * FROM solicitudes WHERE solempresa = '".$nit."' AND solCodigo = '".$solicitud."' AND (solestado = 'FACTURADO' OR solestado = 'ENVIADO' OR solestado = 'ENTREGADO')"));
		if($num2 < 1){
			$num3 = mysql_num_rows(mysql_query("SELECT * FROM clientes WHERE cliCedula = '".$ide."'"));
			if($num3 > 0){
				mysql_query("UPDATE solicitudes SET solcliente = '".$ide."' WHERE solempresa = '".$nit."' AND solCodigo = '".$solicitud."'");
				$mensaje = 'Se cambio el numero de identificacion correctamente.';
			}else $error = 'El numero de identificacion no existe.';
		}else $error = 'La solicitud no debe estar facturada.';
	}else $error = 'La solicitud no existe.';
}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>.:: IdenCorp ::.</title>
<link type="text/css" rel="stylesheet" href="<?php echo $r ?>estilo/estilo.css" />
<link type="text/css" rel="stylesheet" href="<?php echo $r ?>jquery/css/base/jquery-ui-1.10.2.custom.css">
<script type="text/javascript" src="<?php echo $r ?>jquery/js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="<?php echo $r ?>jquery/js/jquery-ui-1.10.2.custom.js"></script>
<script type="text/javascript" src="<?php echo $r ?>include/digito.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$(".btnmodificar").button({ icons: { primary: "ui-icon ui-icon-pencil" }});
	$( "#dialog-message" ).dialog({
		height: 80,
		width: 'auto',
		modal: true
	});
	$("#ti").change(function(event){
		var id = $("#ti").find(':selected').val();
		if (id == 1) {
			$("#datos").load('cedulae.php');
		} else if(id == 2) {
			$("#datos").load('nite.php');
		}
	});
});
</script>
<style type="text/css">
#form form { width:550px }
#form fieldset { padding:10px; display:block; width:550px; margin:20px auto }
#form legend { font-weight: bold; margin-left:5px; padding:5px }
#form label { display:inline-block; width:100px; text-align:right; margin:0.3em 2% 0 0 }
#form p { margin:5px 0 }
</style>
</head>
<body>
<?php include($r.'include/encabezado.php') ?>
<div class="contenedor">
<?php include($r.'include/menu.php') ?>
<div class="mapa">
<a href="<?php echo $r ?>index.php">Principal</a><div class="mapa_div"></div><a href="#">Administracion</a><div class="mapa_div"></div><a href="#">Mantenimiento</a><div class="mapa_div"></div><a class="current">Modificar iden. a solicitud</a>
</div>
<div class="ui-widget">
<form id="form" name="form" action="idensoli.php" method="post">
<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
<legend class="ui-widget ui-widget-header ui-corner-all">MODIFICAR IDENTIFICACION EN SOLICITUD</legend>
<p>
<label for="empresa">EMPRESA:</label>
<select name="empresa" required>
<?php
$mysql->lista("empresas", "empid", "empnombre", "SELECCIONE");
?>
</select>
</p>
<p>
<label for="solicitud">SOLICITUD:</label>
<input type="number" name="solicitud" class="solicitud" required>
</p>
<p>
<label for="ti">TIPO DE IDE.:</label>
<select name="ti" id="ti" required>
<option value=""> -- SELECCIONE -- </option>
<option value="1">CEDULA DE CIUDADANIA</option>
<option value="2">NIT</option>
</select>
</p>
<div id="datos"></div>
<p align="center">
<button type="submit" class="btnmodificar" name="modificar" value="modificar" style="margin-top:5px">modificar</button>
</p>
</fieldset>
</form>
<?php
if($error)
	echo "<div id='dialog-message' title='Error'><span class='ui-icon ui-icon-circle-close' style='float:left; margin:3px 7px 7px 0;'></span>".$error."</div>";
elseif($mensaje)
	echo "<div id='dialog-message' title='Correcto'><span class='ui-icon ui-icon-circle-check' style='float:left; margin:3px 7px 7px 0;'></span>".$mensaje."</div>";
?>
</div>

</div>
</body>
</html>