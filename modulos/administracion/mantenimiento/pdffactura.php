<?php
$r = '../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
require($r.'incluir/fpdf/rotation.php');
require($r.'incluir/letras.class.php');
$empresa = $_GET['empresa'];
$id = $_GET['id'];
$row = $db->query("SELECT * FROM solicitudes INNER JOIN empresas ON solempresa = empid WHERE solempresa = '$empresa' AND solid = '$id'")->fetch(PDO::FETCH_ASSOC);
$numero = $row['solfactura'];
$row2 =$db->query("SELECT * FROM movimientos WHERE movempresa = '$empresa' AND movprefijo = 'FV' AND movnumero = $numero")->fetch(PDO::FETCH_ASSOC);
$cliente = $row['solcliente'];
$vendedor = $row['solasesor'];
$ncuotas = $row['solncuota'];
$fecha = $row2['movfecha'];
$valor = $row2['movvalor'];
$financiacion = $row2['movfinan'];
$total = $valor + $financiacion;
class PDF extends PDF_Rotate{
	function Header(){
		global $db;
		global $empresa;
		global $id;
		global $cliente;
		global $vendedor;
		global $numero;
		global $fecha;
		global $ncuotas;
		$row3 = $db->query("SELECT * FROM empresas WHERE empid = $empresa")->fetch(PDO::FETCH_ASSOC);
		$row4 = $db->query("SELECT * FROM usuarios WHERE usuid = $vendedor")->fetch(PDO::FETCH_ASSOC);
		$row5 = $db->query("SELECT * FROM (clientes INNER JOIN solicitudes ON cliid = solcliente) INNER JOIN ciudades ON (soldepentrega = ciudepto AND solciuentrega = ciuid) WHERE cliid = $cliente")->fetch(PDO::FETCH_ASSOC);
    	$this->SetFont('Arial','B',13);
    	$this->MultiCell(100,5,$row3['empnombre'],0,'C');
		$this->SetFont('Arial','',9);
		$this->Cell(95,6,'RUC 20601541786',0,0,'C');
		$this->Cell(95,4,'',0,1,'C');
		$this->Cell(95,6,$row3['empemail'],0,1,'C');
		$this->Cell(95,4,$row3['empdireccion'],0,1,'C');
		$this->Cell(95,6,'',0,0,'C');
		$this->Cell(95,4,'',0,1,'C');
		$this->Cell(95,6,'',0,0,'C');
		$this->Cell(95,4,'',0,1,'C');
		$this->Ln(10);
		$this->SetFillColor(255,255,255	);
		$this->SetFont('Arial','B',7);
		$this->Cell(30,5,'FECHA',1,0,'C');
		$this->Cell(20,5,'SOLICITUD',1,0,'C');
		if($ncuotas > 0) $enc = 'FACTURA';
		else $enc = 'FACTURA';
		$this->Cell(50,5,$enc,1,0,'C');
		$this->Cell(90,5,'ASESOR',1,1,'C');
		$this->SetFont('Arial','',7);
		$this->Cell(30,5,$fecha,1,0,'C');
		$this->Cell(20,5,$id,1,0,'C');
		$this->Cell(50,5,$row3['empresprefijo'].$numero,1,0,'C');
		$this->Cell(90,5,$row4['usuid'].' | '.$row4['usunombre'],1,1,'',true);
		$this->SetFont('Arial','B',7);
		$this->Cell(190,5,'CLIENTE',1,1,'C');
		$this->Cell(75,5,'NOMBRE',1,0,'C');
		$this->Cell(18,5,'TELEFONO',1,0,'C');
		$this->Cell(30,5,'CIUDAD',1,0,'C');
		$this->Cell(67,5,'DIRECCION',1,1,'C');
		$this->SetFont('Arial','',7);
		$this->Cell(75,5,$row5['cliid'].'-'.$row5['clidigito'].' | '.$row5['clinombre'].' '.$row5['clinom2'].' '.$row5['cliape1'].' '.$row5['cliape2'],1,0,'',true);
		$this->Cell(18,5,$row5['soltelentrega'],1,0,'',true);
		$this->Cell(30,5,$row5['ciunombre'],1,0,'',true);
		$this->Cell(67,5,substr($row5['solbarentrega'].' - '.$row5['solentrega'],0,45),1,1,'',true);		
    	$this->Ln(5);
	}
	function Footer(){
    	$this->SetY(-15);
    	$this->SetFont('Arial','I',8);
    	$this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'C');
	}
}
$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Arial','B',60);
$pdf->SetTextColor(240,240,240);
$pdf->Rotate(40,40,210);
$pdf->Text(40,210,'O  R  I  G  I  N  A  L');
$pdf->Rotate(0);
$pdf->SetTextColor(1,1,1);
$pdf->SetFont('Arial','B',7);
$pdf->Cell(20,5,'CANTIDAD',1,0,'C');
$pdf->Cell(30,5,'CODIGO',1,0,'C');
$pdf->Cell(140,5,'PRODUCTO',1,1,'C');
$pdf->SetFont('Arial','',7);
$qry = $db->query("SELECT * FROM detsolicitudes INNER JOIN productos ON detproducto = proid WHERE detempresa = $empresa AND detsolicitud = '$id'");
while($row3 = $qry->fetch(PDO::FETCH_ASSOC)){
	$pdf->Cell(20,5,$row3['detcantidad'],1,0,'C');
	$pdf->Cell(30,5,$row3['proid'],1,0,'C');
	$pdf->Cell(140,5,$row3['pronombre'],1,1,'L');
}
$pdf->Ln(5);
if($row['solncuota'] > 0){
	$i = 1;
	$fechacom = $row['solcompromiso'];
	$fechapago = $row['solcompromiso'];
	$saldo = $total - $row['solcuota'];
	$cuota = $saldo / $ncuotas;
	while($i <= $ncuotas){
		$qry = $db->query("INSERT INTO detcarteras (dcaempresa, dcafactura, dcacuota, dcafecha, dcaestado) VALUES ($empresa, '$numero', $i, '$fechacom', 'ACTIVA')");
		$saldo = $saldo - $cuota;
		$i++;
		$fechacom = date('Y-m-d', strtotime($fechacom.' next month'));
	}
	
}

	$pdf->SetTextColor(1,1,1);
	$pdf->SetFont('Arial','B',7);
	$pdf->Cell(40,5,'FECHA DE PAGO',1,0,'C');
	$pdf->Cell(40,5,'NUMERO DE CUOTAS',1,0,'C');
	$pdf->Cell(40,5,'VALOR CUOTA',1,0,'C');
	$pdf->Cell(30,5,'CUOTA INICIAL',1,0,'C');
	$pdf->Cell(40,5,'VALOR TOTAL',1,1,'C');
	$pdf->SetFont('Arial','',7);
	$pdf->Cell(40,5,$fechapago,1,0,'C');
	$pdf->Cell(40,5,$ncuotas,1,0,'C');
	$pdf->Cell(40,5,number_format($cuota,0,',','.'),1,0,'C');
	$pdf->Cell(30,5,$row['solcuota'],1,0,'C');
	$pdf->Cell(40,5,number_format($row['soltotal'],0,',','.'),1,1,'C');	


$pdf->Ln(10);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(190,5,'',0,1,'L');
$pdf->Cell(190,5,'Mail: '.$row['empemail'],0,1,'L');
$pdf->SetFont('Arial','',7);
$pdf->Cell(150,5,'',0,0,'L');
$pdf->Cell(20,5,'SUBTOTAL:',1,0,'L');
$pdf->Cell(20,5,number_format($valor,0,',','.'),1,1,'R');
$pdf->Cell(150,5,'',0,0,'L');
$pdf->Cell(20,5,'DESCUENTO:',1,0,'L');
$pdf->Cell(20,5,'0%',1,1,'R');
$pdf->Cell(150,5,'',0,0,'L');
$pdf->Cell(20,5,'FINANCIACION:',1,0,'L');
$pdf->Cell(20,5,number_format($financiacion,0,',','.'),1,1,'R');
$pdf->Cell(150,5,'',0,0,'L');
$pdf->Cell(20,5,'TOTAL NETO:',1,0,'L');
$pdf->Cell(20,5,number_format($total,0,',','.'),1,1,'R');
$pdf->Cell(190,5,'___________________________________________________________________________________________________________________________________________',0,1,'C');

$V = new EnLetras();
$totalletras = strtoupper($V->ValorEnLetras($total," SOLES"));
$pdf->SetFont('Arial','',9);
$pdf->MultiCell(190,5,$totalletras,1,'L');
$pdf->MultiCell(190,5,'RECUERDE QUE SU PAGO SE DEBE REALIZAR A TRAVES DE BCP cuenta Corriente No. 194-2378728-0-53 y INTERBANK cuenta Corriente No. 200-300127142-1',1,'L');

$pdf->Cell(190,20,'',0,1,'L');
$pdf->Cell(60,5,'___________________________________________',0,0,'C');
$pdf->Cell(65,5,'___________________________________________',0,0,'C');
$pdf->Cell(65,5,'___________________________________________',0,1,'C');
$pdf->SetFont('Arial','',7);
$pdf->Cell(60,5,'FIRMA Y SELLO',0,0,'C');
$pdf->Cell(65,5,'CLIENTE FIRMA Y C.C. No            ',0,0,'C');
$pdf->Cell(65,5,'FECHA DE RECIBIDO DE LA FACTURA Y MERCANCIA',0,1,'C');

//------------------------------------------------------------------------------------------------------------------//
$pdf->AddPage();
$pdf->SetFont('Arial','B',60);
$pdf->SetTextColor(240,240,240);
$pdf->Rotate(40,40,210);
$pdf->Text(80,210,'C  O  P  I  A');
$pdf->Rotate(0);
$pdf->SetTextColor(1,1,1);
$pdf->SetFont('Arial','B',7);
$pdf->Cell(20,5,'CANTIDAD',1,0,'C');
$pdf->Cell(30,5,'CODIGO',1,0,'C');
$pdf->Cell(140,5,'PRODUCTO',1,1,'C');
$pdf->SetFont('Arial','',7);
$qry = $db->query("SELECT * FROM detsolicitudes INNER JOIN productos ON detproducto = proid WHERE detempresa = $empresa AND detsolicitud = '$id'");
while($row3 = $qry->fetch(PDO::FETCH_ASSOC)){
	$pdf->Cell(20,5,$row3['detcantidad'],1,0,'C');
	$pdf->Cell(30,5,$row3['proid'],1,0,'C');
	$pdf->Cell(140,5,$row3['pronombre'],1,1,'L');
}
$pdf->Ln(5);
if($row['solncuota'] > 0){
	$i = 1;
	$fechacom = $row['solcompromiso'];
	$fechapago = $row['solcompromiso'];
	$saldo = $total - $row['solcuota'];
	$cuota = $saldo / $ncuotas;
	while($i <= $ncuotas){
		$qry = $db->query("INSERT INTO detcarteras (dcaempresa, dcafactura, dcacuota, dcafecha, dcaestado) VALUES ($empresa, '$numero', $i, '$fechacom', 'ACTIVA')");
		$saldo = $saldo - $cuota;
		$i++;
		$fechacom = date('Y-m-d', strtotime($fechacom.' next month'));
	}
	
}

	$pdf->SetTextColor(1,1,1);
	$pdf->SetFont('Arial','B',7);
	$pdf->Cell(40,5,'FECHA DE PAGO',1,0,'C');
	$pdf->Cell(40,5,'NUMERO DE CUOTAS',1,0,'C');
	$pdf->Cell(40,5,'VALOR CUOTA',1,0,'C');
	$pdf->Cell(30,5,'CUOTA INICIAL',1,0,'C');
	$pdf->Cell(40,5,'VALOR TOTAL',1,1,'C');
	$pdf->SetFont('Arial','',7);
	$pdf->Cell(40,5,$fechapago,1,0,'C');
	$pdf->Cell(40,5,$ncuotas,1,0,'C');
	$pdf->Cell(40,5,number_format($cuota,0,',','.'),1,0,'C');
	$pdf->Cell(30,5,$row['solcuota'],1,0,'C');
	$pdf->Cell(40,5,number_format($row['soltotal'],0,',','.'),1,1,'C');	


$pdf->Ln(10);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(190,5,'',0,1,'L');
$pdf->Cell(190,5,'Mail: '.$row['empemail'],0,1,'L');
$pdf->SetFont('Arial','',7);
$pdf->Cell(150,5,'',0,0,'L');
$pdf->Cell(20,5,'SUBTOTAL:',1,0,'L');
$pdf->Cell(20,5,number_format($valor,0,',','.'),1,1,'R');
$pdf->Cell(150,5,'',0,0,'L');
$pdf->Cell(20,5,'DESCUENTO:',1,0,'L');
$pdf->Cell(20,5,'0%',1,1,'R');
$pdf->Cell(150,5,'',0,0,'L');
$pdf->Cell(20,5,'FINANCIACION:',1,0,'L');
$pdf->Cell(20,5,number_format($financiacion,0,',','.'),1,1,'R');
$pdf->Cell(150,5,'',0,0,'L');
$pdf->Cell(20,5,'TOTAL NETO:',1,0,'L');
$pdf->Cell(20,5,number_format($total,0,',','.'),1,1,'R');
$pdf->Cell(190,5,'___________________________________________________________________________________________________________________________________________',0,1,'C');

$V = new EnLetras();
$totalletras = strtoupper($V->ValorEnLetras($total,' SOLES'));
$pdf->SetFont('Arial','',9);
$pdf->MultiCell(190,5,$totalletras,1,'L');
$pdf->MultiCell(190,5,'RECUERDE QUE SU PAGO SE DEBE REALIZAR A TRAVES DE BCP cuenta Corriente No. 194-2378728-0-53 y INTERBANK cuenta Corriente No. 200-300127142-1',1,'L');

$pdf->Cell(190,20,'',0,1,'L');
$pdf->Cell(60,5,'___________________________________________',0,0,'C');
$pdf->Cell(65,5,'___________________________________________',0,0,'C');
$pdf->Cell(65,5,'___________________________________________',0,1,'C');
$pdf->SetFont('Arial','',7);
$pdf->Cell(60,5,'FIRMA Y SELLO',0,0,'C');
$pdf->Cell(65,5,'CLIENTE FIRMA Y C.C. No            ',0,0,'C');
$pdf->Cell(65,5,'FECHA DE RECIBIDO DE LA FACTURA Y MERCANCIA',0,1,'C');

$pdf->Output($r.'pdf/facturas/'.$empresa.'/'.$numero.'.pdf'); // GUARDAR FACTURA EN CARPETA RESPECTIVA A LA EMPRESA Y CON EL NUMERO DE LA SOLICITUD
$pdf->Output();
?>