<?php
// LA ACCION PRINCIPAL DE ESTA PAGINA ES EN UNA TABLA MOSTRAR LOS RESULTADOS OBTENIDOS REALIZANDO LA CONSULTA CON LOS DATOS RECIBIDOS DE confac.php 

$r = '../../../';
//INCLUIR SESION Y CONECCION
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

$empresa = $numero = $cliente = "";

if (isset($_POST['consultar'])) { // SE VALIDA QUE SE RECIBA INFORMACION POR CONSULTAR Y LE ASIGNAMOS UNA VARIABLE A LOS CAMPOS QUE SE RECIBEN
	$empresa = $_POST['empresa'];
	$numero = $_POST['numero'];
	$cliente = $_POST['cliente'];
} else if (isset($_GET['empresa'])) {
	$empresa = $_GET['empresa'];
	$numero = $_GET['numero'];
	$cliente = $_GET['cliente'];
}
// VALIDAMOS LOS CAMPOS RECIBIDOS PARA LUEGO EJECTUAR LA CONSULTA MYSQL
if ($empresa != '' && $numero == '' && $cliente == '') $sql = "SELECT * FROM (solicitudes INNER JOIN empresas ON solempresa = empid) INNER JOIN clientes ON solcliente = cliid WHERE solempresa = '$empresa' ORDER BY solempresa, solfactura";
elseif ($empresa == '' && $numero != '' && $cliente == '') $sql = "SELECT * FROM (solicitudes INNER JOIN empresas ON solempresa = empid) INNER JOIN clientes ON solcliente = cliid WHERE solfactura LIKE '%$numero%' ORDER BY solempresa, solfactura";
elseif ($empresa == '' && $numero == '' && $cliente != '') $sql = "SELECT * FROM (solicitudes INNER JOIN empresas ON solempresa = empid) INNER JOIN clientes ON solcliente = cliid WHERE cliid = '$cliente' ORDER BY solempresa, solfactura";
elseif ($empresa != '' && $numero == '' && $cliente != '') $sql = "SELECT * FROM (solicitudes INNER JOIN empresas ON solempresa = empid) INNER JOIN clientes ON solcliente = cliid WHERE solempresa = '$empresa' AND solfactura = '$numero' ORDER BY solempresa, solfactura";
else $sql = "SELECT * FROM (solicitudes INNER JOIN empresas ON solempresa = empid) INNER JOIN clientes ON solcliente = cliid ORDER BY solempresa, solfactura";
$qry = $db->query($sql);
?>
<!doctype html>
<html>

<head>
	<meta charset="utf-8">

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
	<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('.pdf').click(function() { // VISUALIZACION EN UNA VENTANA MODAL LA FACTURA DE VENTA ACTUAL 
				newSrc = $(this).attr('data-rel');
				$('#modal').html("<iframe src='" + newSrc + "' width='100%' height='100%'></iframe>");
				$('#modal').dialog({
					modal: true,
					width: '600',
					height: '800',
					title: 'PDF de la factura'
				});
			});

			$('#tabla').dataTable({ // CARACTERISTICAS DE LA TABLA
				'bJQueryUI': true,
				'sPaginationType': 'full_numbers',
				'oLanguage': {
					'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
				},
				'bSort': false,
			});
		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
		<?php require($r . 'incluir/src/menu.php') ?>
		<!-- INCLUIMOS MENU PRINCIPAL -->
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a class="current">Mantenimiento</a>
			</article>
			<article id="contenido">
				<h2>Listado de factura a regenerar</h2>
				<table id="tabla">
					<thead>
						<tr>
							<th>Empresa</th>
							<th>Solicitud</th>
							<th>Factura</th>
							<th>Fecha</th>
							<th>Cliente</th>
							<th>Estado</th>
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php
						while ($row = $qry->fetch(PDO::FETCH_ASSOC)) { // INFORMACION DE LA CONSULTA
						?>
							<tr>
								<td title="<?php echo $row['empid'] ?>"><?php echo $row['empnombre'] ?></td>
								<td align="center"><?php echo $row['solid'] ?></td>
								<td align="center"><?php echo $row['solfactura'] ?></td>
								<td align="center"><img src="<?php echo $r ?>imagenes/iconos/date.png" title="<?php echo $row['solfechafac'] ?>" /></td>
								<td title="<?php echo $row['carId'] ?>"><?php echo $row['clinombre'] . ' ' . $row['clinom2'] . ' ' . $row['cliape1'] . ' ' . $row['cliape2'] ?></td>
								<td><?php echo $row['solestado'] ?></td>
								<td align="center"><img src="<?php echo $r ?>imagenes/iconos/pdf.png" class="pdf" data-rel="<?php echo $r . 'pdf/facturas/' . $row['empid'] . '/' . $row['solfactura'] . '.pdf' ?>" title="Factura" /></td>
								<td align="center"><a href="factura.php?<?php echo 'empresa=' . $row['empid'] . '&id=' . $row['solid'] ?>" onClick="carga()" title="regenerar"><img src="<?php echo $r ?>imagenes/iconos/bullet_wrench.png" /></a></td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
				<p class="boton">
					<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href = 'confac.php'">atras</button> <!-- BOTON PARA VOLVER A CONSULTAR -->
				</p>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
		<!-- BOTON PARA VOLVER A LA PARTE SUPERIOR  -->
	</section>
	<div id="modal" style="display:none"></div> <!-- VENTANA MODAL POR DEFECTO DISPLAY NONE  -->
</body>

</html>