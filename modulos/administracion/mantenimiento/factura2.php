<?php
//LA ACCION PRINCIPAL DE ESTA PAGINA ES MOSTRAR EN UN FRAME LA FACTURA QUE SE HA REGENERADO SEGUN LA EMPRESA Y LA SOLICITUD RECIBIDAS POR GET 

$r = '../../../';
//INCLUIR SESION Y CONECCION
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
$empresa = $_GET['empresa']; // EMPRESA
$id = $_GET['id']; // SOLICITUD
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>. : : S o p h y a : : .</title>
<!-- INSERTAR JQUERYS Y ESTILOS NECESARIAS PARA CORRECTO FUNCIONAMIENTO Y VISUALIZACION -->
<meta charset="utf-8" />
<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/css/normalize.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/jquery/css/smoothness/jquery-ui-1.10.3.custom.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/css/css.css" type="text/css" />
<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/css/menu.css" type="text/css" />
<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/js/jquery-ui-1.10.3.custom.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('.btnatras').button({ icons: { primary: 'ui-icon ui-icon-arrowthick-1-w' }});
});
</script>
</head>
<body>
<?php require($r.'incluir/src/login.php') ?> <!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
<section id="principal">
<?php require($r.'incluir/src/cabeza.php') ?>  <!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
<?php require($r.'incluir/src/menu.php') ?> <!-- INCLUIMOS MENU PRINCIPAL -->
<article id="cuerpo">
<article class="mapa">
<a href="#">Principal</a><div class="mapa_div"></div><a class="current">Mantenimiento</a>
</article>
<article id="contenido">
<p align="center">
<iframe src="pdffactura2.php?empresa=<?php echo $empresa.'&id='.$id ?>" width="800" height="900"></iframe><br /> <!-- FRAME QUE MUESTRA L AFACTURA REGENERADA  -->
</p>
<p class="boton">
<button type="button" class="btnatras" onClick="carga(); location.href = 'confac.php'">atras</button>  <!-- BOTON PARA VOLVER A CONSULTAR FACTURA -->
</p>
</article>
</article>
<?php require($r.'incluir/src/pie.php') ?> <!-- BOTON PARA VOLVER A LA PARTE SUPERIOR  -->
</section>
</body>
</html>