<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');


if (isset($_POST['modificar'])) {
	$empresa = $_POST['empresa'];
	$id = $_POST['id'];
	$identi = $_POST['identi'];

	$sqlconsul = "SELECT * FROM solicitudes WHERE solid = '$id' AND solcliente = '$identi'";
	$qryconsul = $db->query($sqlconsul);
	$rowconsul = $qryconsul->fetch(PDO::FETCH_ASSOC);

	if(isset($rowconsul['solestado']))
	{
		if ($rowconsul['solestado'] == 'PROCESO') {
			$qry = $db->query("UPDATE solicitudes SET solempresa = '$empresa' WHERE solid = '$id' AND solcliente = $identi");
			if ($qry) {
				$mensaje = 'Se Modifico Correctamente le empresa en la solicitud';
			} else {
				$error = 'No se pudo modificar le empresa en la solicitud porfavor intente nuevamente o conctactese con el encargado';
			}
		} else {
			$error = 'No se modifico <br>Recuerde que el cambio solo aplica para solicitudes en PROCESO.';
		}
	}
	else
		$error = 'No se identificado la solicitud numero '. $id .' del cliente con cedula '. $identi ;
}

?>
<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<script type="text/javascript" src="<?php echo $r ?>incluir/funciones.js"></script>

</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a class="current">Mantenimiento</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="camempresa.php" method="post">
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">Cambio de Empresa en Solicitud</legend>
						<input type="hidden" name="tipo" value="<?php echo $tipo ?>" />
						<p>
							<label for="empresa">Empresa Nueva:</label>
							<select id="empresa" name="empresa" class="validate[required]">
								<option value="">SELECCIONE</option>
								<?php
								$qry = $db->query("SELECT * FROM empresas ORDER BY empnombre");
								while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
									echo '<option value=' . $row['empid'] . '>' . $row['empnombre'] . '</option>';
								}
								?>
							</select>
						</p>
						<p>
							<label for="id">Solicitud Actual: </label>
							<input type="text" name="id" class="pedido validate[required, custom[onlyNumberSp]] text-input" title="Digite numero de solicitud" />
						</p>
						<p>
							<label for="identi">Cedula del cliente:</label>
							<input type="text" name="identi" class="id validate[required] text-input" title="Digite la identificacion" />
						</p>
						<p class="boton">
							<button type="submit" class="btn btn-primary btnmodificar" name="modificar" value="modificar">Modificar</button>
						</p>
					</fieldset>
				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<?php
	if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>'; //VENTANA MODAL ERROR
	elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>'; //VENTANA MODAL EXITOSO
	?>
</body>

</html>