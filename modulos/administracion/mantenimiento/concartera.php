<?php
// ESTA PAGINA ESTA DESHABILITADA 25/09/2014 SEBASTIAN SALCEDO 
$r = "../../";
include($r."include/session.class.php");
include($r."include/mysql.class.php");
$session = new Session;
$mysql = new MySql;
$session->iniciar($r);
$mysql->conectar();
$mysql->base();
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>.:: IdenCorp ::.</title>
<link type="text/css" rel="stylesheet" href="<?php echo $r ?>estilo/estilo.css" />
<link type="text/css" rel="stylesheet" href="<?php echo $r ?>jquery/css/base/jquery-ui-1.10.2.custom.css">
<script type="text/javascript" src="<?php echo $r ?>jquery/js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="<?php echo $r ?>jquery/js/jquery-ui-1.10.2.custom.js"></script>
<script type="text/javascript" src="<?php echo $r ?>jquery/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$(".btnconsulta").button({ icons: { primary: "ui-icon ui-icon-search" }});
	$("#fecha").datepicker({
		dateFormat: 'yy-mm-dd'
	});
	$("#dialog-message" ).dialog({
		height: 80,
		width: 'auto',
		modal: true
	});
});
</script>
</head>
<body>
<?php include($r.'include/encabezado.php') ?>
<div class="contenedor">
<?php include($r.'include/menu.php') ?>
<div class="mapa">
<a href="<?php echo $r ?>index.php">Principal</a><div class="mapa_div"></div><a href="#">Administracion</a><div class="mapa_div"></div><a href="#">Mantenimiento</a><div class="mapa_div"></div><a class="current">Modificar cartera</a>
</div>
<div class="cuerpo centrado">
<form name="form" action="datos.php" method="post">
<table>	
<thead>
<tr><th colspan="2"><h1>BUSQUEDA DE CARTERA</h1></th></tr>
</thead>
<tbody>
<tr>
<td class="alt">EMPRESA: </td><td>
<select name="nit" required>
<option value="" > -- SELECCIONE -- </option>
<?php
$qempresa = mysql_query("SELECT * FROM empresas ORDER BY empnombre");
while($rempresa = mysql_fetch_assoc($qempresa))
	echo "<option value='".$rempresa["empid"]."'>".$rempresa["empnombre"]."</option>";
?>
</select></td>
</tr>
<tr>
<td class="alt">cuenta: </td><td><input type="text" name="numero" class="consecutivo" pattern="^[0-9]*$" title="solo numeros" required /></td>
</tr>
<tr>
</tbody>
</table>
<button type="submit" class="btnconsulta" name="consultar" value="consultar" style="margin-top:5px">consultar</button>
</form>
<?php
if($_GET["error"])
	echo "<div id='dialog-message' title='Error'><span class='ui-icon ui-icon-circle-close' style='float:left; margin:3px 7px 7px 0;'></span>".$_GET["error"]."</div>";
elseif($_GET["mensaje"])
	echo "<div id='dialog-message' title='Correcto'><span class='ui-icon ui-icon-circle-check' style='float:left; margin:3px 7px 7px 0;'></span>".$_GET["mensaje"]."</div>";
?>
</div>
</div>
</body>
</html>