<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');


?>


<!doctype html>
<html>

<head>
    <title>AUDITORIA POR CONTRATO</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">
	<?php
		require($r . 'incluir/src/head.php');
		require($r . 'incluir/src/head-not-form.php');
	?>
	<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>
	
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

    <script src="<?php echo $r.'incluir/librerias/fontawesome/js/all.js'?>"></script>

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css" />
    <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>   
    
    <script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>

    <script src="<?php echo $r.'incluir/kavv_js/kavvdt.js'?>" ></script>


	<!-- ESTILOS EMBEBIDOS PARA MANEJAR FORMULARIO INDEPENDIENTEMENTE -->
	<style type="text/css">

		#form label {
			display: inline-block;
			width: 100px;
			text-align: right;
			margin: 0.3em 2% 0 0
		}

		#form p {
			margin: 5px 0
		}
        .divisor {
            margin-left: 5px;
            margin-right: 5px;
            margin-top: 15px;
            margin-bottom: 15px;
            padding-top: 10px;
            padding-bottom: 2px;
            background-color: #004593;
            color: #fff;
            border-radius: 8px;
        }
        .divisor h6, .divisor h3 {
            font-weight: bold; 
        }
        .input-disabled {
            background :#e0e0e0;
            pointer-events: none;
        }
        .sub-rayado{
            border-bottom: 1px #000 solid;
        }
        .bold{
            font-weight: bold;
        }
	</style>
    
    <style>
    </style>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
		<?php require($r . 'incluir/src/menu.php') ?>
		<!-- INCLUIMOS MENU PRINCIPAL -->
        

        <section>
            <h3 class="text-center my-4">DETALLE DE ACCIONES EN EL SISTEMA</h3>
            <div class="" id="msj">

            </div>
            <div class="row d-flex justify-content-center">
                <div class="col-md-5 text-center">
                    <label for="contrato" class="default text-center bold">Ingrese el contrato</label> 
                    <div class="input-group mb-3">
                        <input autocomplete='off' style="border: 3px solid #006dad !important;" type="text" name="contrato" id="contrato" 
                        class="form-control contrato not-w uppercase" title="Digite # de contrato" />
                        <div class="input-group-append">
                            <button type="button" id="btnbuscar" class="btn btn-primary" name="consultar">BUSCAR</button> <!-- BOTON CONSULTAR -->
                        </div>
                    </div>
                </div>
            </div>

            
            
            
            
            
            
        </section>
        <section id="show-logs" style="min-height:20em">
            

        </section>


		<?php require($r . 'incluir/src/pie.php') ?>
		<!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
	</section>
	
</body>
<script>
    var dt;
    $("#btnbuscar").click(function(){
        var contrato = $("#contrato").val();
        if(contrato != "")
        {
            carga();
            var ruta = "ajax/info_logs.php?contrato="+contrato;
            $.get(ruta, function(res){
                $("#show-logs").empty();
                $("#show-logs").append(res);
                
                $("#carga").dialog("close");

                dt = createdt($("#tabla-logs"),{buttons: bootstrapButtons, dom: bootstrapDesingSlimButtons, com:'desc'});;

            });
        }
        else
        {
            msj("danger", "Debe especificar el contrato a buscar");
        }
    });

    function msj(tipo, texto)
    {
        var html = 
            '<div class="alert alert-' + tipo + ' alert-dismissible fade show" role="alert">'+
                texto +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
                    '<span aria-hidden="true">&times;</span>'+
               ' </button>'+
           ' </div>';
        
        
        
        '<div class="alert alert-' + tipo + '" role="alert">' + texto + '</div>';
        $("#msj").empty();
        $("#msj").append(html);
    }

    $("#contrato").keypress(function(e) {
        if (e.which == 13) {
            $("#btnbuscar").click();
            return false;
        }
    });
</script>
</html>