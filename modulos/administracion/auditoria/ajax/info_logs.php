<?php

$r = '../../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

$contrato = strupperEsp($_GET['contrato']);

if($contrato != "NULL" && $contrato != "")
{
    $consulta = "SELECT usunombre as nombre, logs.* FROM logs 
    LEFT JOIN usuarios on usuid = id_usuario 
    WHERE accion_usuario LIKE '%$contrato%'
    ORDER BY fecha_accion desc;";

    $data = $db->query($consulta);
}
?>


    <div class="row">
        <div class="col-md-12">
            <div class="row divisor">
                <div class="col-md-12 text-center">
                    <h6>INFORMACIÓN DE ACCIONES</h6>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12" style="overflow-y:auto;">
                    <table id="tabla-logs" class="table table-hover" style="width: 100%;">
                        <thead>
                            <tr>
                                <th style="width:20px; text-align:center;">#ID</th>
                                <th style="width:40px;">Usuario</th>
                                <th style="width:40px;">Nombre</th>
                                <th style="width:30px;">F.Procesado</th>
                                <th style="width:40px;">Acción</th>
                                <th style="width:40px;">IP</th>
                            </tr>
                        </thead>
                        <tbody id="tb-recibo">
                            <!-- AGREGAR LOS RECIBOS PAGADOS -->
                            <?php 
                                while($row = $data->fetch(PDO::FETCH_ASSOC))
                                {
                                    $idlogs = $row['idlogs'];
                                    $id_usuario = $row['id_usuario'];
                                    $nombre = $row['nombre'];
                                    $ip_usuario = $row['ip_usuario'];
                                    $accion_usuario = $row['accion_usuario'];
                                    $fecha_accion = $row['fecha_accion'];
                                    $fecha_accion = date("Y-m-d h:i:s A", strtotime($fecha_accion));
                                    echo 
                                    "<tr class='recibo-item'>" .
                                        "<td> <label>$idlogs</label></td>" .
                                        "<td> <label>$id_usuario</label></td>" .
                                        "<td> <label>$nombre</label></td>" .
                                        "<td> <label>$fecha_accion</label></td>" .
                                        "<td> <label style='min-width:200px;'>$accion_usuario</label></td>" .
                                        "<td> <label>$ip_usuario</label></td>" .
                                    "</tr>";
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>