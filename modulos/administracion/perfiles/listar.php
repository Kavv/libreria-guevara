<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
if (isset($_POST['modificar'])) {
	$id = $_POST['id'];
	$nombre = strtoupper(trim($_POST['nombre']));
	$num = $db->query("SELECT * FROM perfiles WHERE perid <> $id AND pernombre = '$nombre'")->rowCount();
	if ($num < 1) {
		$_POST['admin'] = $_POST['callcenter'] = $_POST['solicitud'] 
		= $_POST['factura'] = $_POST['bodega'] = $_POST['cartera'] 
		= $_POST['comision'] = $_POST['estadistico'] 
		= $_POST['contrato'] = $_POST['recibo'] = 0;

		/* Admin */
		if(isset($_POST['admpar']) || isset($_POST['admemp']) || isset($_POST['admege']) || isset($_POST['admper']) 
		|| isset($_POST['admusu']) || isset($_POST['admcli']) || isset($_POST['admprv']) || isset($_POST['admcau']) 
		|| isset($_POST['admpro']) || isset($_POST['admfpa']) || isset($_POST['admmde']) || isset($_POST['admman'])
		|| isset($_POST['admaut']) || isset($_POST['admgesdoc']))
			$_POST['admin'] = 1;

		if (!isset($_POST['admpar']))
			$_POST['admpar'] = 0;
		if (!isset($_POST['admemp']))
			$_POST['admemp'] = 0;
		if (!isset($_POST['admege']))
			$_POST['admege'] = 0;
		if (!isset($_POST['admper']))
			$_POST['admper'] = 0;
		if (!isset($_POST['admusu']))
			$_POST['admusu'] = 0;
		if (!isset($_POST['admcli']))
			$_POST['admcli'] = 0;
		if (!isset($_POST['admprv']))
			$_POST['admprv'] = 0;
		if (!isset($_POST['admcau']))
			$_POST['admcau'] = 0;
		if (!isset($_POST['admpro']))
			$_POST['admpro'] = 0;
		if (!isset($_POST['admfpa']))
			$_POST['admfpa'] = 0;
		if (!isset($_POST['admmde']))
			$_POST['admmde'] = 0;
		if (!isset($_POST['admman']))
			$_POST['admman'] = 0;
		if (!isset($_POST['admaut']))
			$_POST['admaut'] = 0;
		if (!isset($_POST['admgesdoc']))
			$_POST['admgesdoc'] = 0;

		
		/* Call Center */
		if(isset($_POST['callLlamar']) || isset($_POST['CallCampana']) || isset($_POST['CallGeneral']) || isset($_POST['CallCapaci']) 
		|| isset($_POST['CallSemaforo']) || isset($_POST['CallRepo']) || isset($_POST['CallViaInser']) || isset($_POST['Callviavalor']) 
		|| isset($_POST['CallViaCons']))
			$_POST['callcenter'] = 1;

		if (!isset($_POST['callLlamar']))
			$_POST['callLlamar'] = 0;
		if (!isset($_POST['CallCampana']))
			$_POST['CallCampana'] = 0;
		if (!isset($_POST['CallGeneral']))
			$_POST['CallGeneral'] = 0;
		if (!isset($_POST['CallCapaci']))
			$_POST['CallCapaci'] = 0;
		if (!isset($_POST['CallSemaforo']))
			$_POST['CallSemaforo'] = 0;
		if (!isset($_POST['CallRepo']))
			$_POST['CallRepo'] = 0;
		if (!isset($_POST['CallViaInser']))
			$_POST['CallViaInser'] = 0;
		if (!isset($_POST['Callviavalor']))
			$_POST['Callviavalor'] = 0;
		if (!isset($_POST['CallViaCons']))
			$_POST['CallViaCons'] = 0;


		
		/* Solicitudes */
		if(isset($_POST['solnor']) || isset($_POST['sollib']) || isset($_POST['solest']) || isset($_POST['Solmod'])
		|| isset($_POST['Solrea']) || isset($_POST['Solcan']) || isset($_POST['solanu']) || isset($_POST['Soleli'])
		|| isset($_POST['solcon']) || isset($_POST['solscr']) || isset($_POST['solche']) || isset($_POST['Solsopor'])
		|| isset($_POST['Solrepor']) || isset($_POST['Solprop']) )
			$_POST['solicitud'] = 1;
		if (!isset($_POST['solnor']))
			$_POST['solnor'] = 0;
		if (!isset($_POST['sollib']))
			$_POST['sollib'] = 0;
		if (!isset($_POST['solest']))
			$_POST['solest'] = 0;
		if (!isset($_POST['Solmod']))
			$_POST['Solmod'] = 0;
		if (!isset($_POST['Solrea']))
			$_POST['Solrea'] = 0;
		if (!isset($_POST['Solcan']))
			$_POST['Solcan'] = 0;
		if (!isset($_POST['solanu']))
			$_POST['solanu'] = 0;
		if (!isset($_POST['Soleli']))
			$_POST['Soleli'] = 0;
		if (!isset($_POST['solcon']))
			$_POST['solcon'] = 0;
		if (!isset($_POST['solscr']))
			$_POST['solscr'] = 0;
		if (!isset($_POST['solche']))
			$_POST['solche'] = 0;
		if (!isset($_POST['Solsopor']))
			$_POST['Solsopor'] = 0;
		if (!isset($_POST['Solrepor']))
			$_POST['Solrepor'] = 0;
		if (!isset($_POST['Solprop']))
			$_POST['Solprop'] = 0;

		
		/* Facturacion */
		if(isset($_POST['faccon']) || isset($_POST['facfac']) || isset($_POST['facrep']) || isset($_POST['Facrepcall']) )
			$_POST['factura'] = 1;
		if (!isset($_POST['facfac']))
			$_POST['facfac'] = 0;
		if (!isset($_POST['faccon']))
			$_POST['faccon'] = 0;
		if (!isset($_POST['facrep']))
			$_POST['facrep'] = 0;
		if (!isset($_POST['Facrepcall']))
			$_POST['Facrepcall'] = 0;


		/* Bodega */
		if(isset($_POST['bodkar']) || isset($_POST['bodpla']) || isset($_POST['boddoc']) 
		|| isset($_POST['bodrep']) || isset($_POST['boddes']) || isset($_POST['bofactura']) )
			$_POST['bodega'] = 1;
		if (!isset($_POST['bodkar']))
			$_POST['bodkar'] = 0;
		if (!isset($_POST['bodpla']))
			$_POST['bodpla'] = 0;
		if (!isset($_POST['boddoc']))
			$_POST['boddoc'] = 0;
		if (!isset($_POST['bodrep']))
			$_POST['bodrep'] = 0;	
		if (!isset($_POST['boddes']))
			$_POST['boddes'] = 0;
		if (!isset($_POST['bofactura']))
			$_POST['bofactura'] = 0;


		/* Tesoreria */
		if (!isset($_POST['tesoreria']))
			$_POST['tesoreria'] = 0;


		/* Cartera */
		if(isset($_POST['carpag']) || isset($_POST['carrep']) || isset($_POST['carnot']) || isset($_POST['carmas']) 
		|| isset($_POST['carpaz']) || isset($_POST['carcon']) || isset($_POST['carfec']) || isset($_POST['carpro']) 
		|| isset($_POST['carcas']) || isset($_POST['cardat']))
			$_POST['cartera'] = 1;

		if (!isset($_POST['carpag']))
			$_POST['carpag'] = 0;
		if (!isset($_POST['carrep']))
			$_POST['carrep'] = 0;
		if (!isset($_POST['carnot']))
			$_POST['carnot'] = 0;
		if (!isset($_POST['carmas']))
			$_POST['carmas'] = 0;
		if (!isset($_POST['carpaz']))
			$_POST['carpaz'] = 0;
		if (!isset($_POST['carcon']))
			$_POST['carcon'] = 0;
		if (!isset($_POST['carfec']))
			$_POST['carfec'] = 0;
		if (!isset($_POST['carpro']))
			$_POST['carpro'] = 0;
		if (!isset($_POST['carcas']))
			$_POST['carcas'] = 0;
		if (!isset($_POST['cardat']))
			$_POST['cardat'] = 0;
		
		/* Caja */
		if (!isset($_POST['caja']))
			$_POST['caja'] = 0;

		/* Comision */
		if(isset($_POST['Cominormal']) || isset($_POST['Comilibranza']) || isset($_POST['Comiestimulacion']) || isset($_POST['Comiprop']) )
			$_POST['comision'] = 1;
		if (!isset($_POST['Cominormal']))
			$_POST['Cominormal'] = 0;
		if (!isset($_POST['Comilibranza']))
			$_POST['Comilibranza'] = 0;
		if (!isset($_POST['Comiestimulacion']))
			$_POST['Comiestimulacion'] = 0;
		if (!isset($_POST['Comiprop']))
			$_POST['Comiprop'] = 0;


		/* Estadistico */

		if(isset($_POST['EstaVent']) || isset($_POST['EstaRepor']))
			$_POST['estadistico'] = 1;

		if (!isset($_POST['EstaVent']))
			$_POST['EstaVent'] = 0;
		if (!isset($_POST['EstaRepor']))
			$_POST['EstaRepor'] = 0;

		/* Contrato */
		if(isset($_POST['contrato-agregar']) || isset($_POST['contrato-editar']) || isset($_POST['contrato-ver']))
			$_POST['contrato'] = 1;

		if (!isset($_POST['contrato_agregar']))
			$_POST['contrato_agregar'] = 0;
		if (!isset($_POST['contrato_editar']))
			$_POST['contrato_editar'] = 0;

			
		/* Recibos */
		if(isset($_POST['r_cobrarse_add']) || isset($_POST['r_cobrarse_edit']) 
		|| isset($_POST['r_pagados_add']) || isset($_POST['r_pagados_edit']) 
		|| isset($_POST['recibo-ver']) || isset($_POST['r_imprimir']) )
			$_POST['recibo'] = 1;

		if (!isset($_POST['r_cobrarse_add']))
			$_POST['r_cobrarse_add'] = 0;
		if (!isset($_POST['r_cobrarse_edit']))
			$_POST['r_cobrarse_edit'] = 0;
		if (!isset($_POST['r_pagados_add']))
			$_POST['r_pagados_add'] = 0;
		if (!isset($_POST['r_pagados_edit']))
			$_POST['r_pagados_edit'] = 0;
		if (!isset($_POST['r_imprimir']))
			$_POST['r_imprimir'] = 0;
			
		$qry = $db->query("UPDATE perfiles SET pernombre = '$nombre', peradmpar = '" 
		. $_POST['admpar'] . "', peradmemp = '" . $_POST['admemp'] . "', peradmege = '" 
		. $_POST['admege'] . "', peradmper = '" . $_POST['admper'] . "', peradmusu = '" 
		. $_POST['admusu'] . "', peradmcli = '" . $_POST['admcli'] . "', peradmprv = '" 
		. $_POST['admprv'] . "', peradmcau = '" . $_POST['admcau'] . "', peradmpro = '" 
		. $_POST['admpro'] . "', peradmfpa = '" . $_POST['admfpa'] . "', peradmmde = '" 
		. $_POST['admmde'] . "', peradmman = '" . $_POST['admman'] . "', peradmaut = '" 
		. $_POST['admaut'] . "', peradmgesdoc = '" . $_POST['admgesdoc'] . "', peradmin = '" 
		. $_POST['admin'] .

		"', percallllamar = '" . $_POST['callLlamar'] . "', percallcampana = '" . $_POST['CallCampana'] . 
		"', percallgeneral = '" . $_POST['CallGeneral'] . "', percallcapaci = '" . $_POST['CallCapaci'] . 
		"', percallsemaforo = '" . $_POST['CallSemaforo'] . "', percallrepo = '" . $_POST['CallRepo'] . 
		"', percallviainser = '" . $_POST['CallViaInser'] . "', percallviavalor = '" . $_POST['Callviavalor'] . 
		"', percallviacons = '" . $_POST['CallViaCons'] . "', percallcenter = '" . $_POST['callcenter'] .
		
		
		"', persolnor = '" . $_POST['solnor'] . "', persollib = '" . $_POST['sollib'] . 
		"', persolest = '" . $_POST['solest'] . "', persolmod = '" . $_POST['Solmod'] . 
		"', persolrea = '" . $_POST['Solrea'] . "', persolcan = '" . $_POST['Solcan'] . 
		"', persolanu = '" . $_POST['solanu'] . "', persoleli = '" . $_POST['Soleli'] . 
		"', persolcon = '" . $_POST['solcon'] . "', persolscr = '" . $_POST['solscr'] . 
		"', persolche = '" . $_POST['solche'] . "', persolsopor = '" . $_POST['Solsopor'] . 
		"', persolrepor = '" . $_POST['Solrepor'] . "', persolprop = '" . $_POST['Solprop'] . 
		"', persolicitud = '" . $_POST['solicitud'] . 
		
		"', perfacfac = '" . $_POST['facfac'] . "', perfaccon = '" . $_POST['faccon'] . 
		"', perfacrep = '" . $_POST['facrep'] . "', perfacrepcall = '" . $_POST['Facrepcall'] . 
		"', perfactura = '" . $_POST['factura'] . 
		
		"', perbodkar = '" . $_POST['bodkar'] . "', perbodpla = '" . $_POST['bodpla'] . 
		"', perboddoc = '" . $_POST['boddoc'] . "', perbodrep = '" . $_POST['bodrep'] . 
		"', perboddes = '" . $_POST['boddes'] . "', perbodega = '" . $_POST['bodega'] . 
		"', percontratofac = '" . $_POST['bofactura'] . 
		
		"', pertesoreria = '" . $_POST['tesoreria'] . 

		"', percarpag = '" . $_POST['carpag'] . "', percarrep = '" . $_POST['carrep'] . 
		"', percarnot = '" . $_POST['carnot'] . "', percarmas = '" . $_POST['carmas'] . 
		"', percarpaz = '" . $_POST['carpaz'] . "', percarcon = '" . $_POST['carcon'] . 
		"', percarfec = '" . $_POST['carfec'] . "', percarpro = '" . $_POST['carpro'] . 
		"', percarcas = '" . $_POST['carcas'] . "', percardat = '" . $_POST['cardat'] . 
		"', percartera = '" . $_POST['cartera'] . 
		
		"', percaja = '" . $_POST['caja'] . 

		"', percominormal = '" . $_POST['Cominormal'] . "', percomilibranza = '" . $_POST['Comilibranza'] . 
		"', percomiestimulacion = '" . $_POST['Comiestimulacion'] . "', percomiprop = '" . $_POST['Comiprop'] . 
		"', percomision = '" . $_POST['comision'] . 
		
		"', perestavent = '" . $_POST['EstaVent'] . "', perestarepor = '" . $_POST['EstaRepor'] . "', perestadistico = '" . $_POST['estadistico'] .
		
		"', percontrato = '" . $_POST['contrato'] . "', percontratoadd = '" . $_POST['contrato_agregar'] . "', percontratoedit = '" . $_POST['contrato_editar'] .
		
		"', perrecibo = '" . $_POST['recibo'] . "', percobrarseadd = '" . $_POST['r_cobrarse_add'] . "', percobrarseedit = '" . $_POST['r_cobrarse_edit'] . "', perpagadoadd = '" . $_POST['r_pagados_add'] . "', perpagadoedit = '" . $_POST['r_pagados_edit'] .
		"', perimprimirrecibos = '" . $_POST['r_imprimir'] . 
		
		"' WHERE perid = $id");


		$consultas = $informes = [];
		$perfil_id = $id;
		if(isset($_POST['consultas']))
		{
			$consultas = $_POST['consultas'];
			$cant = count($consultas);
			$db->query("DELETE FROM perfil_reporte WHERE perfil = '$perfil_id' AND tipo = 1");
			for($i = 0; $i < $cant; $i++)
			{
				$reporte = $consultas[$i];
				$db->query("INSERT INTO perfil_reporte (perfil, reporte, tipo) VALUES ($perfil_id, $reporte, 1)");
			}
		}
		else
		{
			$db->query("DELETE FROM perfil_reporte WHERE perfil = '$perfil_id' AND tipo = 1");
		}
		if(isset($_POST['informes']))
		{
			$informes = $_POST['informes'];
			$cant = count($informes);
			$db->query("DELETE FROM perfil_reporte WHERE perfil = '$perfil_id' AND tipo = 2");
			for($i = 0; $i < $cant; $i++)
			{
				$reporte = $informes[$i];
				$db->query("INSERT INTO perfil_reporte (perfil, reporte, tipo) VALUES ($perfil_id, $reporte, 2)");
			}
		}
		else
		{
			$db->query("DELETE FROM perfil_reporte WHERE perfil = '$perfil_id' AND tipo = 2");

		}


		if ($qry) $mensaje = 'Se actualizo el perfil';
		else $error = 'No se actualizo el perfil';
	} else $error = 'El nombre del perfil ya existe';
}
if (isset($_GET['id1'])) {
	$id1 = $_GET['id1'];
	$num = $db->query("SELECT * FROM usuarios WHERE usuperfil = $id1")->rowCount();
	if ($num < 1) {
		$qry = $db->query("DELETE FROM perfiles WHERE perid = $id1");
		if ($qry) $mensaje = 'Se elimino el perfil';
		else $error = 'No se pudo eliminar el perfil';
	} else $error = 'Este perfil esta asignado a un usuario';
}
$nombre = "";
if (isset($_POST['consultar'])) {
	$nombre = strtoupper($_POST['nombre']);
} else if (isset($_GET['nombre'])) {
	$nombre = strtoupper($_GET['nombre']);
}

$filtro = '&nombre=' . $nombre;
if ($nombre != '') $sql = "SELECT * FROM perfiles WHERE pernombre LIKE '%$nombre%'";
else $sql = "SELECT * FROM perfiles";

?>
<!doctype html>
<html lang="es">

<head>
    <title>LISTA DE PERFILES</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>

	<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
	<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
	<script>
		$(document).ready(function() {
			$('#tabs').tabs();
			$('.tabla').dataTable({
				'bJQueryUI': true,
				'bAutoWidth': false,
				'aoColumnDefs': [{
						'bSortable': true,
						'aTargets': [0]
					},
					{
						'bSortable': false,
						'aTargets': ['_all']
					},
					{
						'sWidth': '40%',
						'aTargets': [0]
					}
				],
				'sPaginationType': 'full_numbers',
				'oLanguage': {
					'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
				}
			});

			$('.confirmar').click(function(e) {
				e.preventDefault();
				var targetUrl = $(this).attr('href');
				var $dialog_link_follow_confirm = $('<div></div>').html('<p><span class="ui-icon ui-icon-trash" style="float:left; margin:2px 7px 7px 0;"></span>Esta seguro que desea eliminar el perfil?</p>').
				dialog({
					title: 'Porfavor confirmar',
					buttons: {
						'Si': function() {
							window.location.href = targetUrl;
						},
						'No': function() {
							$(this).dialog("close");
						}
					},
					modal: true,
					width: 'auto',
					height: 'auto'
				});
				$dialog_link_follow_confirm.dialog('open');
			});
		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Administracion</a>
				<div class="mapa_div"></div><a class="current">Perfiles</a>
			</article>
			<article id="contenido">
				<h2>Listado de perfiles</h2>
				<div id="tabs">
					<div id="tabs-1">
						<table class="tabla">
							<thead>
								<tr>
									<th style="width: 80%;">Nombre</th>
									<th  style="width: 10%;"></th>
									<th  style="width: 10%;"></th>
								</tr>
							</thead>
							<tbody>
								<?php
								$qry = $db->query($sql);
								while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
								?>
									<tr>
										<td><?php echo $row['pernombre'] ?></td>
										<td align="center"><a href="modificar.php?<?php echo 'id1=' . $row['perid'] . $filtro ?>" title="editar"><img src="<?php echo $r ?>imagenes/iconos/lapiz.png" class="grayscale" /></a></td>
										<td align="center"><a href="listar.php?<?php echo 'id1=' . $row['perid'] . $filtro ?>" title="eliminar" class="confirmar"><img src="<?php echo $r ?>imagenes/iconos/basura.png" class="grayscale" /></a></td>
									</tr>
								<?php
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
				<p class="boton">
					<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href='consultar.php'">Atras</button>
				</p>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<?php
	if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>';
	elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>';
	?>
</body>

</html>