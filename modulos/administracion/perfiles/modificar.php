<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
$filtro = '&nombre=' . $_GET['nombre'];
$perfil_id = $_GET['id1'];
$row = $db->query("SELECT * FROM perfiles WHERE perid = '$perfil_id'")->fetch(PDO::FETCH_ASSOC);
?>
<!doctype html>
<html lang="es">

<head>
    <title>MODIFICAR PERFILES</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<script>
		$(document).ready(function() {
			$("input:checkbox").click(function() {
				var element = $(this).attr("id");
				if ($(this).is(":checked")) {
					$("." + element).removeAttr("disabled");
				} else {
					$("." + element).attr("disabled", "disabled");
				}
			});
			$('#acordeon').accordion({
				heightStyle: "content"
			});
		});
	</script>
	<style type="text/css">
		#form input {
			width: auto !important;
		}

		.permission-list {
			background: #e6e6e6;
			border-radius: 15px;
			padding-top: 10px;
			padding-bottom: 10px;
		}
	</style>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Administracion</a>
				<div class="mapa_div"></div><a class="current">Perfiles</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="listar.php?filtro=<?php echo $filtro ?>" method="post">
					<input type="hidden" name="id" value="<?php echo $row['perid'] ?>" readonly />
					<p>
						<label for="nombre" class="tabx">Nombre del perfil:</label> <!-- CAMPO NOMBRE -->
						<input type="text" name="nombre" value="<?php echo $row['pernombre'] ?>" class="nombre validate[required, text-input" title="Digite el nombre del perfil" style="width: 49%!important;"/>
					</p>
					<div class="row">
						<div class="col-6">
							<div class="list-group" id="list-tab" role="tablist">
								<a class="list-group-item list-group-item-action active" id="list-Administracion-list" data-toggle="list" href="#list-Administracion" role="tab" aria-controls="Administracion">Administración</a>
								<a class="list-group-item list-group-item-action" id="list-contratos-list" data-toggle="list" href="#list-contratos" role="tab" aria-controls="Contratos">Contratos</a>
								<a class="list-group-item list-group-item-action" id="list-recibos-list" data-toggle="list" href="#list-recibos" role="tab" aria-controls="Recibos">Recibos</a>
								<a class="list-group-item list-group-item-action" id="list-Bodega-list" data-toggle="list" href="#list-Bodega" role="tab" aria-controls="Bodega">Bodega</a>
								<a class="list-group-item list-group-item-action" id="list-Cartera-list" data-toggle="list" href="#list-Tesoreria" role="tab" aria-controls="Tesoreria">Tesoreria</a>
								<a class="list-group-item list-group-item-action" id="list-consultas-list" data-toggle="list" href="#list-consultas" role="tab" aria-controls="Consultas">Reportes Consultas</a>
								<a class="list-group-item list-group-item-action" id="list-informes-list" data-toggle="list" href="#list-informes" role="tab" aria-controls="Informes">Reportes Informes</a>
							</div>
						</div>
						<div class="col-6 permission-list">
							<div class="tab-content" id="nav-tabContent" style="height:25em; overflow-y:auto; overflow-x:hidden;">
								<!-- Administracion -->
								<div class="tab-pane fade show active" id="list-Administracion" role="tabpanel" aria-labelledby="list-Administracion-list">
									<input type="checkbox" name="admege" class="admin" <?php if($row['peradmege']){?> checked <?php } ?> value="1" /> Estructura geografica<br />
									<input type="checkbox" name="admper" class="admin" <?php if($row['peradmper']){?> checked <?php } ?> value="1" /> Perfiles<br />
									<input type="checkbox" name="admusu" class="admin" <?php if($row['peradmusu']){?> checked <?php } ?> value="1" /> Usuarios<br />
									<input type="checkbox" name="admcli" class="admin" <?php if($row['peradmcli']){?> checked <?php } ?> value="1" /> Clientes<br />
								</div>
								
								<!-- Contratos -->
								<div class="tab-pane fade" id="list-contratos" role="tabpanel" aria-labelledby="list-contratos-list">
									<input type="checkbox" <?php if($row['percontratoadd']){?> checked <?php } ?> name="contrato_agregar" value="1" /> Agregar contratos<br />
									<input type="checkbox" <?php if($row['percontratoedit']){?> checked <?php } ?> name="contrato_editar" value="1" /> Editar contratos<br />
									<input type="checkbox" <?php if($row['percontrato']){?> checked <?php } ?> name="contrato-ver" value="1" /> Ver contratos<br />
								</div>

								<!-- Recibos -->
								<div class="tab-pane fade" id="list-recibos" role="tabpanel" aria-labelledby="list-recibos-list">
									<input type="checkbox" <?php if($row['percobrarseadd']){?> checked <?php } ?> name="r_cobrarse_add" value="1" /> Agregar recibos a cobrarse<br />
									<input type="checkbox" <?php if($row['percobrarseedit']){?> checked <?php } ?> name="r_cobrarse_edit" value="1" /> Editar recibos a cobrarse<br />
									<input type="checkbox" <?php if($row['perpagadoadd']){?> checked <?php } ?> name="r_pagados_add" value="1" /> Agregar recibos cobrados<br />
									<input type="checkbox" <?php if($row['perpagadoedit']){?> checked <?php } ?> name="r_pagados_edit" value="1" /> Editar recibos cobrados<br />
									<input type="checkbox" <?php if($row['perimprimirrecibos']){?> checked <?php } ?> name="r_imprimir" value="1" /> Imprimir recibos<br />
								</div>
								
								<!-- Consultas -->
								<div class="tab-pane fade" id="list-consultas" role="tabpanel" aria-labelledby="list-consultas-list">
								
									<button type="button" class="btn btn-info" onclick="seleccionar_todo('.check-consulta');">Seleccionar Todo</button>
									<button type="button" class="btn btn-warning" onclick="deseleccionar('.check-consulta');">Quitar Todo</button>
									<br>
									<br>
									<?php
										$consultas = $db->query("SELECT * FROM consultasdinamicas where cdiactiva = true ORDER BY cdinombre ASC");
										$aux_consulta = $db->query("SELECT reporte FROM perfil_reporte 
										inner join consultasdinamicas ON cdiid=reporte 
										WHERE tipo = 1 and perfil = $perfil_id ORDER BY cdinombre ASC")->fetchAll(PDO::FETCH_ASSOC);
										$report_exist = 0;
										$cant_exist = count($aux_consulta);
										while($row_consulta = $consultas->fetch(PDO::FETCH_ASSOC))
										{
											$con_nombre = $row_consulta['cdinombre'];
											$con_id = $row_consulta['cdiid'];
											
											if(isset($aux_consulta[$report_exist]['reporte']))
											{
												if($aux_consulta[$report_exist]['reporte'] == $con_id)
												{
													echo "<input checked class='check-consulta' type='checkbox' name='consultas[]' value='$con_id' /> $con_nombre<br />";
													if($cant_exist != $report_exist + 1)
													$report_exist++;
												}
												else
												echo "<input class='check-consulta' type='checkbox' name='consultas[]' value='$con_id' /> $con_nombre<br />";
											}
											else
											echo "<input class='check-consulta' type='checkbox' name='consultas[]' value='$con_id' /> $con_nombre<br />";
										}
									?>
								</div>
								
								<!-- Informes -->
								<div class="tab-pane fade" id="list-informes" role="tabpanel" aria-labelledby="list-informes-list">
									
									<button type="button" class="btn btn-info" onclick="seleccionar_todo('.check-informe');">Seleccionar Todo</button>
									<button type="button" class="btn btn-warning" onclick="deseleccionar('.check-informe');">Quitar Todo</button>
									<br>
									<br>
									<?php
										$consultas = $db->query("SELECT * FROM informesdinamicos WHERE hide is false ORDER BY idinombre ASC");
										$aux_consulta = $db->query("SELECT reporte FROM perfil_reporte 
										inner join informesdinamicos on idiid = reporte 
										WHERE tipo = 2 and perfil = $perfil_id ORDER BY idinombre ASC")->fetchAll(PDO::FETCH_ASSOC);
										$report_exist = 0;
										$cant_exist = count($aux_consulta);
										while($row_consulta = $consultas->fetch(PDO::FETCH_ASSOC))
										{
											$inf_nombre = $row_consulta['idinombre'];
											$inf_id = $row_consulta['idiid'];
											
											if(isset($aux_consulta[$report_exist]['reporte']))
											{
												if($aux_consulta[$report_exist]['reporte'] == $inf_id)
												{
													echo "<input checked class='check-informe' type='checkbox' name='informes[]' value='$inf_id' /> $inf_nombre<br />";
													if($cant_exist != $report_exist + 1)
													$report_exist++;
												}
												else
												echo "<input class='check-informe' type='checkbox' name='informes[]' value='$inf_id' /> $inf_nombre<br />";
											}
											else
											echo "<input class='check-informe' type='checkbox' name='informes[]' value='$inf_id' /> $inf_nombre<br />";
										}
									?>
								</div>

								<!-- CallCenter -->
								<div class="tab-pane fade" id="list-CallCenter" role="tabpanel" aria-labelledby="list-CallCenter-list">
									<input type="checkbox" <?php if($row['percallllamar']){?> checked <?php } ?> name="callLlamar" class="admin" value="1" /> Gestion Llamadas<br />
									<input type="checkbox" <?php if($row['percallcampana']){?> checked <?php } ?> name="CallCampana" class="admin" value="1" /> Campañas<br />
									<input type="checkbox" <?php if($row['percallgeneral']){?> checked <?php } ?> name="CallGeneral" class="admin" value="1" /> General<br />
									<input type="checkbox" <?php if($row['percallcapaci']){?> checked <?php } ?> name="CallCapaci" class="admin" value="1" /> Capacitaciones<br />
									<input type="checkbox" <?php if($row['percallsemaforo']){?> checked <?php } ?> name="CallSemaforo" class="admin" value="1" /> Semaforo<br />
									<input type="checkbox" <?php if($row['percallrepo']){?> checked <?php } ?> name="CallRepo" class="admin" value="1" /> Reportes<br />
									<input type="checkbox" <?php if($row['percallviainser']){?> checked <?php } ?> name="CallViaInser" class="admin" value="1" /> Insertar Viaticos<br />
									<input type="checkbox" <?php if($row['percallviavalor']){?> checked <?php } ?> name="Callviavalor" class="admin" value="1" /> Valor Viaticos<br />
									<input type="checkbox" <?php if($row['percallviacons']){?> checked <?php } ?> name="CallViaCons" class="admin" value="1" /> Consultar Viaticos<br />
								</div>

								<!-- Solicitudes -->
								<div class="tab-pane fade" id="list-Solicitudes" role="tabpanel" aria-labelledby="list-Solicitudes-list">
									<input type="checkbox" <?php if($row['persolnor']){?> checked <?php } ?> name="solnor" class="solicitud" value="1" /> Solicitud normal<br />
									<input type="checkbox" <?php if($row['persollib']){?> checked <?php } ?> name="sollib" class="solicitud" value="1" /> Solicitud libranza<br />
									<input type="checkbox" <?php if($row['persolest']){?> checked <?php } ?> name="solest" class="solicitud" value="1" /> Solicitud estimulacion<br />
									<input type="checkbox" <?php if($row['persolmod']){?> checked <?php } ?> name="Solmod" class="solicitud" value="1" /> Modificar solicitud<br />
									<input type="checkbox" <?php if($row['persolrea']){?> checked <?php } ?> name="Solrea" class="solicitud" value="1" /> Cancelar solicitud<br />
									<input type="checkbox" <?php if($row['persolcan']){?> checked <?php } ?> name="Solcan" class="solicitud" value="1" /> Reactivar solicitud<br />
									<input type="checkbox" <?php if($row['persolanu']){?> checked <?php } ?> name="solanu" class="solicitud" value="1" /> Anular solicitud<br />
									<input type="checkbox" <?php if($row['persoleli']){?> checked <?php } ?> name="Soleli" class="solicitud" value="1" /> Eliminar solicitud<br />
									<input type="checkbox" <?php if($row['persolcon']){?> checked <?php } ?> name="solcon" class="solicitud" value="1" /> Consultar solicitud<br />
									<input type="checkbox" <?php if($row['persolscr']){?> checked <?php } ?> name="solscr" class="solicitud" value="1" /> Screen<br />
									<input type="checkbox" <?php if($row['persolche']){?> checked <?php } ?> name="solche" class="solicitud" value="1" /> Check fisico<br />
									<input type="checkbox" <?php if($row['persolsopor']){?> checked <?php } ?> name="Solsopor" class="solicitud" value="1" /> Soporte<br />
									<input type="checkbox" <?php if($row['persolrepor']){?> checked <?php } ?> name="Solrepor" class="solicitud" value="1" /> Reporte<br />
									<input type="checkbox" <?php if($row['persolprop']){?> checked <?php } ?> name="Solprop" class="solicitud" value="1" /> Mis Solicitudes<br />
								</div>

								<!-- Facturacion -->
								<div class="tab-pane fade" id="list-Facturacion" role="tabpanel" aria-labelledby="list-Facturacion-list">
									<input type="checkbox" <?php if($row['perfacfac']){?> checked <?php } ?> name="facfac" class="factura" value="1" /> Facturar<br />
									<input type="checkbox" <?php if($row['perfaccon']){?> checked <?php } ?> name="faccon" class="factura" value="1" /> Consultar<br />
									<input type="checkbox" <?php if($row['perfacrep']){?> checked <?php } ?> name="facrep" class="factura" value="1" /> Reportes<br />
									<input type="checkbox" <?php if($row['perfacrepcall']){?> checked <?php } ?> name="Facrepcall" class="factura" value="1" /> Reportes Call<br />
								</div>

								<!-- Bodega -->
								<div class="tab-pane fade" id="list-Bodega" role="tabpanel" aria-labelledby="list-Bodega-list">
									<input type="checkbox" <?php if($row['perboddoc']){?> checked <?php } ?> name="boddoc" class="bodega" value="1" /> Documentos<br />
									<input type="checkbox" <?php if($row['percontratofac']){?> checked <?php } ?> name="bofactura" class="bodega" value="1" /> Facturar<br />
									<input type="checkbox" <?php if($row['perbodkar']){?> checked <?php } ?> name="bodkar" class="bodega" value="1" /> Kardex<br />
								</div>

								<!-- tesoreria -->
								<div class="tab-pane fade" id="list-Tesoreria" role="tabpanel" aria-labelledby="list-Tesoreria-list">
									<input type="checkbox" <?php if($row['pertesoreria']){?> checked <?php } ?> name="tesoreria" class="tesoreria" value="1" /> Tesoreria<br />
								</div>
								
								<!-- Cartera -->
								<div class="tab-pane fade" id="list-Cartera" role="tabpanel" aria-labelledby="list-Cartera-list">
									<input type="checkbox" <?php if($row['percarnot']){?> checked <?php } ?> name="carnot" class="cartera" value="1" /> Notas<br />
									<input type="checkbox" <?php if($row['percarcon']){?> checked <?php } ?> name="carcon" class="cartera" value="1" /> Consultar cartera<br />
									<input type="checkbox" <?php if($row['percarpro']){?> checked <?php } ?> name="carpro" class="cartera" value="1" /> Promotores<br />
									<input type="checkbox" <?php if($row['percarpag']){?> checked <?php } ?> name="carpag" class="cartera" value="1" /> Aplicar pago<br />
									<input type="checkbox" <?php if($row['percarmas']){?> checked <?php } ?> name="carmas" class="cartera" value="1" /> Consulta Masiva<br />
									<input type="checkbox" <?php if($row['percarfec']){?> checked <?php } ?> name="carfec" class="cartera" value="1" /> Cambiar fecha<br />
									<input type="checkbox" <?php if($row['percarcas']){?> checked <?php } ?> name="carcas" class="cartera" value="1" /> Cartera castigada<br />
									<input type="checkbox" <?php if($row['percardat']){?> checked <?php } ?> name="cardat" class="cartera" value="1" /> Datacredito<br />
									<input type="checkbox" <?php if($row['percarrep']){?> checked <?php } ?> name="carrep" class="cartera" value="1" /> Reportes<br />
									<input type="checkbox" <?php if($row['percarpaz']){?> checked <?php } ?> name="carpaz" class="cartera" value="1" /> Paz y Salvo<br />
								</div>

								<!-- Caja -->
								<div class="tab-pane fade" id="list-Caja" role="tabpanel" aria-labelledby="list-Caja-list">
									<input type="checkbox" <?php if($row['percaja']){?> checked <?php } ?> name="caja" value="1" /> Caja<br />
								</div>

								<!-- Comisiones -->
								<div class="tab-pane fade" id="list-Comisiones" role="tabpanel" aria-labelledby="list-Comisiones-list">
									<input type="checkbox" <?php if($row['percominormal']){?> checked <?php } ?> name="Cominormal" value="1" /> Comizion normal<br />
									<input type="checkbox" <?php if($row['percomilibranza']){?> checked <?php } ?> name="Comilibranza" value="1" /> Comizion libranza<br />
									<input type="checkbox" <?php if($row['percomiestimulacion']){?> checked <?php } ?> name="Comiestimulacion" value="1" /> Comision estimulacion<br />
									<input type="checkbox" <?php if($row['percomiprop']){?> checked <?php } ?> name="Comiprop" value="1" /> Mi comision<br />
								</div>

								<!-- Estadisticos -->
								<div class="tab-pane fade" id="list-Estadisticos" role="tabpanel" aria-labelledby="list-Estadisticos-list">
									<input type="checkbox" <?php if($row['perestavent']){?> checked <?php } ?> name="EstaVent" value="1" /> Ventas<br />
									<input type="checkbox" <?php if($row['perestarepor']){?> checked <?php } ?> name="EstaRepor" value="1" /> Reportes<br />
								</div>

							</div>
						</div>
					</div>
					<p class="boton">
						<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href='listar.php?<?php echo $filtro ?>'">Atras</button>
						<button type="submit" class="btn btn-primary btnmodificar" name="modificar" value="modificar">Modificar</button>
					</p>


				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
</body>

<script>
	function seleccionar_todo(element)
	{
		$(element).prop('checked', true);
	}
	function deseleccionar(element)
	{
		$(element).prop('checked', false);
	}
</script>

</html>