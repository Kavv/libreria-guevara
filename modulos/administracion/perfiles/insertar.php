<?php
// LA ACCION PRINCIPAL DE ESTA PAGINA ES INSERTAR UN NUEVO PERFIL

$r = '../../../';
//INCLUIR SESION Y CONECCION
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

if (isset($_POST['insertar'])) { // VALIDAMOS QUE POST INSERTAR SEA TRUE
	$nombre = trim(strtoupper($_POST['nombre']));
	$_POST['admin'] = $_POST['callcenter'] = $_POST['solicitud'] 
	= $_POST['factura'] = $_POST['bodega'] = $_POST['cartera'] 
	= $_POST['comision'] = $_POST['estadistico'] 
	= $_POST['contrato'] = $_POST['recibo'] = 0;
	
	/* Admin */
	if(isset($_POST['admpar']) || isset($_POST['admemp']) || isset($_POST['admege']) || isset($_POST['admper']) 
	|| isset($_POST['admusu']) || isset($_POST['admcli']) || isset($_POST['admprv']) || isset($_POST['admcau']) 
	|| isset($_POST['admpro']) || isset($_POST['admfpa']) || isset($_POST['admmde']) || isset($_POST['admman'])
	|| isset($_POST['admaut']) || isset($_POST['admgesdoc']))
		$_POST['admin'] = 1;

	if (!isset($_POST['admpar']))
		$_POST['admpar'] = 0;
	if (!isset($_POST['admemp']))
		$_POST['admemp'] = 0;
	if (!isset($_POST['admege']))
		$_POST['admege'] = 0;
	if (!isset($_POST['admper']))
		$_POST['admper'] = 0;
	if (!isset($_POST['admusu']))
		$_POST['admusu'] = 0;
	if (!isset($_POST['admcli']))
		$_POST['admcli'] = 0;
	if (!isset($_POST['admprv']))
		$_POST['admprv'] = 0;
	if (!isset($_POST['admcau']))
		$_POST['admcau'] = 0;
	if (!isset($_POST['admpro']))
		$_POST['admpro'] = 0;
	if (!isset($_POST['admfpa']))
		$_POST['admfpa'] = 0;
	if (!isset($_POST['admmde']))
		$_POST['admmde'] = 0;
	if (!isset($_POST['admman']))
		$_POST['admman'] = 0;
	if (!isset($_POST['admaut']))
		$_POST['admaut'] = 0;
	if (!isset($_POST['admgesdoc']))
		$_POST['admgesdoc'] = 0;

	
	/* Call Center */
	if(isset($_POST['callLlamar']) || isset($_POST['CallCampana']) || isset($_POST['CallGeneral']) || isset($_POST['CallCapaci']) 
	|| isset($_POST['CallSemaforo']) || isset($_POST['CallRepo']) || isset($_POST['CallViaInser']) || isset($_POST['Callviavalor']) 
	|| isset($_POST['CallViaCons']))
		$_POST['callcenter'] = 1;

	if (!isset($_POST['callLlamar']))
		$_POST['callLlamar'] = 0;
	if (!isset($_POST['CallCampana']))
		$_POST['CallCampana'] = 0;
	if (!isset($_POST['CallGeneral']))
		$_POST['CallGeneral'] = 0;
	if (!isset($_POST['CallCapaci']))
		$_POST['CallCapaci'] = 0;
	if (!isset($_POST['CallSemaforo']))
		$_POST['CallSemaforo'] = 0;
	if (!isset($_POST['CallRepo']))
		$_POST['CallRepo'] = 0;
	if (!isset($_POST['CallViaInser']))
		$_POST['CallViaInser'] = 0;
	if (!isset($_POST['Callviavalor']))
		$_POST['Callviavalor'] = 0;
	if (!isset($_POST['CallViaCons']))
		$_POST['CallViaCons'] = 0;


	/* Solicitudes */
	if(isset($_POST['solnor']) || isset($_POST['sollib']) || isset($_POST['solest']) || isset($_POST['Solmod'])
	|| isset($_POST['Solrea']) || isset($_POST['Solcan']) || isset($_POST['solanu']) || isset($_POST['Soleli'])
	|| isset($_POST['solcon']) || isset($_POST['solscr']) || isset($_POST['solche']) || isset($_POST['Solsopor'])
	|| isset($_POST['Solrepor']) || isset($_POST['Solprop']) )
		$_POST['solicitud'] = 1;
	if (!isset($_POST['solnor']))
		$_POST['solnor'] = 0;
	if (!isset($_POST['sollib']))
		$_POST['sollib'] = 0;
	if (!isset($_POST['solest']))
		$_POST['solest'] = 0;
	if (!isset($_POST['Solmod']))
		$_POST['Solmod'] = 0;
	if (!isset($_POST['Solrea']))
		$_POST['Solrea'] = 0;
	if (!isset($_POST['Solcan']))
		$_POST['Solcan'] = 0;
	if (!isset($_POST['solanu']))
		$_POST['solanu'] = 0;
	if (!isset($_POST['Soleli']))
		$_POST['Soleli'] = 0;
	if (!isset($_POST['solcon']))
		$_POST['solcon'] = 0;
	if (!isset($_POST['solscr']))
		$_POST['solscr'] = 0;
	if (!isset($_POST['solche']))
		$_POST['solche'] = 0;
	if (!isset($_POST['Solsopor']))
		$_POST['Solsopor'] = 0;
	if (!isset($_POST['Solrepor']))
		$_POST['Solrepor'] = 0;
	if (!isset($_POST['Solprop']))
		$_POST['Solprop'] = 0;

	
	/* Facturacion */
	if(isset($_POST['faccon']) || isset($_POST['facfac']) || isset($_POST['facrep']) || isset($_POST['Facrepcall']) )
		$_POST['factura'] = 1;
	if (!isset($_POST['facfac']))
		$_POST['facfac'] = 0;
	if (!isset($_POST['faccon']))
		$_POST['faccon'] = 0;
	if (!isset($_POST['facrep']))
		$_POST['facrep'] = 0;
	if (!isset($_POST['Facrepcall']))
		$_POST['Facrepcall'] = 0;


	/* Bodega */
	if(isset($_POST['bodkar']) || isset($_POST['bodpla']) || isset($_POST['boddoc']) 
	|| isset($_POST['bodrep']) || isset($_POST['boddes']) || isset($_POST['bofactura']) )
		$_POST['bodega'] = 1;
	if (!isset($_POST['bodkar']))
		$_POST['bodkar'] = 0;
	if (!isset($_POST['bodpla']))
		$_POST['bodpla'] = 0;
	if (!isset($_POST['boddoc']))
		$_POST['boddoc'] = 0;
	if (!isset($_POST['bodrep']))
		$_POST['bodrep'] = 0;	
	if (!isset($_POST['boddes']))
		$_POST['boddes'] = 0;
	if (!isset($_POST['bofactura']))
		$_POST['bofactura'] = 0;

	/* Tesoreria */
	if (!isset($_POST['tesoreria']))
		$_POST['tesoreria'] = 0;

	/* Cartera */
	if(isset($_POST['carpag']) || isset($_POST['carrep']) || isset($_POST['carnot']) || isset($_POST['carmas']) 
	|| isset($_POST['carpaz']) || isset($_POST['carcon']) || isset($_POST['carfec']) || isset($_POST['carpro']) 
	|| isset($_POST['carcas']) || isset($_POST['cardat']))
		$_POST['cartera'] = 1;

	if (!isset($_POST['carpag']))
		$_POST['carpag'] = 0;
	if (!isset($_POST['carrep']))
		$_POST['carrep'] = 0;
	if (!isset($_POST['carnot']))
		$_POST['carnot'] = 0;
	if (!isset($_POST['carmas']))
		$_POST['carmas'] = 0;
	if (!isset($_POST['carpaz']))
		$_POST['carpaz'] = 0;
	if (!isset($_POST['carcon']))
		$_POST['carcon'] = 0;
	if (!isset($_POST['carfec']))
		$_POST['carfec'] = 0;
	if (!isset($_POST['carpro']))
		$_POST['carpro'] = 0;
	if (!isset($_POST['carcas']))
		$_POST['carcas'] = 0;
	if (!isset($_POST['cardat']))
		$_POST['cardat'] = 0;
	
	/* Caja */
	if (!isset($_POST['caja']))
		$_POST['caja'] = 0;

	/* Comision */
	if(isset($_POST['Cominormal']) || isset($_POST['Comilibranza']) || isset($_POST['Comiestimulacion']) || isset($_POST['Comiprop']) )
		$_POST['comision'] = 1;
	if (!isset($_POST['Cominormal']))
		$_POST['Cominormal'] = 0;
	if (!isset($_POST['Comilibranza']))
		$_POST['Comilibranza'] = 0;
	if (!isset($_POST['Comiestimulacion']))
		$_POST['Comiestimulacion'] = 0;
	if (!isset($_POST['Comiprop']))
		$_POST['Comiprop'] = 0;


	/* Estadistico */

	if(isset($_POST['EstaVent']) || isset($_POST['EstaRepor']))
		$_POST['estadistico'] = 1;

	if (!isset($_POST['EstaVent']))
		$_POST['EstaVent'] = 0;
	if (!isset($_POST['EstaRepor']))
		$_POST['EstaRepor'] = 0;



	/* Contrato */
	if(isset($_POST['contrato_agregar']) || isset($_POST['contrato_editar']) || isset($_POST['contrato-ver']))
		$_POST['contrato'] = 1;

	if (!isset($_POST['contrato_agregar']))
		$_POST['contrato_agregar'] = 0;
	if (!isset($_POST['contrato_editar']))
		$_POST['contrato_editar'] = 0;

		
	/* Recibos */
	if(isset($_POST['r_cobrarse_add']) || isset($_POST['r_cobrarse_edit']) 
	|| isset($_POST['r_pagados_add']) || isset($_POST['r_pagados_edit']) 
	|| isset($_POST['recibo-ver']) || isset($_POST['r_imprimir']) )
		$_POST['recibo'] = 1;

	if (!isset($_POST['r_cobrarse_add']))
		$_POST['r_cobrarse_add'] = 0;
	if (!isset($_POST['r_cobrarse_edit']))
		$_POST['r_cobrarse_edit'] = 0;
	if (!isset($_POST['r_pagados_add']))
		$_POST['r_pagados_add'] = 0;
	if (!isset($_POST['r_pagados_edit']))
		$_POST['r_pagados_edit'] = 0;
	if (!isset($_POST['r_imprimir']))
		$_POST['r_imprimir'] = 0;


	$qry = $db->query("INSERT INTO perfiles ( pernombre,
		peradmpar, peradmemp, peradmege, peradmper, peradmusu, peradmcli, peradmprv, peradmcau, peradmpro, peradmfpa, peradmmde, peradmman, peradmaut, peradmgesdoc, peradmin,
		percallllamar, percallcampana, percallgeneral, percallcapaci, percallsemaforo, percallrepo, percallviainser, percallviavalor, percallviacons, percallcenter,
		persolnor, persollib, persolest, persolmod, persolrea, persolcan, persolanu, persoleli, persolcon, persolscr, persolche, persolsopor, persolrepor, persolprop, persolicitud,
		perfacfac, perfaccon, perfacrep, perfacrepcall, perfactura,
		perbodkar, perbodpla, perboddoc, perbodrep, perboddes, perbodega, percontratofac, 
		pertesoreria,
		percarpag, percarrep, percarnot, percarmas, percarpaz, percarcon, percarfec, percarpro, percarcas, percardat, percartera,
		percaja, 
		percominormal, percomilibranza, percomiestimulacion, percomiprop, percomision,
		perestavent, perestarepor, perestadistico,
		percontrato, percontratoadd, percontratoedit,
		perrecibo, percobrarseadd, percobrarseedit, perpagadoadd, perpagadoedit, perimprimirrecibos
		) 
	VALUES ('$nombre', 
	'" . $_POST['admpar'] . "', '" . $_POST['admemp'] . "', '" . $_POST['admege'] . "', '" . $_POST['admper'] . "', 
	'" . $_POST['admusu'] . "', '" . $_POST['admcli'] . "', '" . $_POST['admprv'] . "', '" . $_POST['admcau'] . "', 
	'" . $_POST['admpro'] . "', '" . $_POST['admfpa'] . "', '" . $_POST['admmde'] . "', '" . $_POST['admman'] . "', 
	'" . $_POST['admaut'] . "', '" . $_POST['admgesdoc'] . "', '" . $_POST['admin'] . "', 

	'" . $_POST['callLlamar'] . "', '" . $_POST['CallCampana'] . "', '" . $_POST['CallGeneral'] . "', '" . $_POST['CallCapaci'] . "', 
	'" . $_POST['CallSemaforo'] . "', '" . $_POST['CallRepo'] . "', '" . $_POST['CallViaInser'] . "', '" . $_POST['Callviavalor'] . "', 
	'" . $_POST['CallViaCons'] . "', '" . $_POST['callcenter'] . "', 
	
	'" . $_POST['solnor'] . "', '" . $_POST['sollib'] . "', '" . $_POST['solest'] . "', '" . $_POST['Solmod'] . "', 
	'" . $_POST['Solrea'] . "', '" . $_POST['Solcan'] . "', '" . $_POST['solanu'] . "', '" . $_POST['Soleli'] . "', 
	'" . $_POST['solcon'] . "', '" . $_POST['solscr'] . "', '" . $_POST['solche'] . "', '" . $_POST['Solsopor'] . "', 
	'" . $_POST['Solrepor'] . "', '" . $_POST['Solprop'] . "', '" . $_POST['solicitud'] . "',  

	'" . $_POST['facfac'] . "', '" . $_POST['faccon'] . "', '" . $_POST['facrep'] . "', '" . $_POST['Facrepcall'] . "', 
	'" . $_POST['factura'] . "', 

	'" . $_POST['bodkar'] . "', '" . $_POST['bodpla'] . "', '" . $_POST['boddoc'] . "', '" . $_POST['bodrep'] . "', 
	'" . $_POST['boddes'] . "', '" . $_POST['bodega'] . "', '" . $_POST['bofactura'] . "',   
	
	'" . $_POST['tesoreria'] . "',

	'" . $_POST['carpag'] . "', '" . $_POST['carrep'] . "', '" . $_POST['carnot'] . "', 
	'" . $_POST['carmas'] . "', '" . $_POST['carpaz'] . "', '" . $_POST['carcon'] . "', '" . $_POST['carfec'] . "', 
	'" . $_POST['carpro'] . "', '" . $_POST['carcas'] . "', '" . $_POST['cardat'] . "', '" . $_POST['cartera'] . "', 

	'" . $_POST['caja'] . "',
	
	'" . $_POST['Cominormal'] . "', '" . $_POST['Comilibranza'] . "', '" . $_POST['Comiestimulacion'] . "', '" . $_POST['Comiprop'] . "', 
	'" . $_POST['comision'] . "', 
	
	'" . $_POST['EstaVent'] . "', '" . $_POST['EstaRepor'] . "', '" . $_POST['estadistico'] . "', 
	
	
	'" . $_POST['contrato'] . "', '" . $_POST['contrato_agregar'] . "', '" . $_POST['contrato_editar'] . "', 

	'" . $_POST['recibo'] . "', '" . $_POST['r_cobrarse_add'] . "', '" . $_POST['r_cobrarse_edit'] . "', '" . $_POST['r_pagados_add'] . "', '" . $_POST['r_pagados_edit'] . "', '" . $_POST['r_imprimir'] . "')"); 
	
	$consultas = $informes = [];
	$perfil_id = $db->lastInsertId();
	if(isset($_POST['consultas']))
	{
		$consultas = $_POST['consultas'];
		$cant = count($consultas);
		for($i = 0; $i < $cant; $i++)
		{
			$reporte = $consultas[$i];
			$db->query("INSERT INTO perfil_reporte (perfil, reporte, tipo) VALUES ($perfil_id, $reporte, 1)");
		}
	}
	if(isset($_POST['informes']))
	{
		$informes = $_POST['informes'];
		$cant = count($informes);
		for($i = 0; $i < $cant; $i++)
		{
			$reporte = $informes[$i];
			$db->query("INSERT INTO perfil_reporte (perfil, reporte, tipo) VALUES ($perfil_id, $reporte, 2)");
		}
	}

	if ($qry) $mensaje = 'Se inserto el perfil'; // MENSAJE MODAL EXITOSO
	else $error = "No se inserto el perfil";  // MENSAJE MODAL ERROR
}
?>
<!doctype html>
<html lang="es">

<head>
    <title>AGREGAR PERFIL</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<script>
		$(document).ready(function() {
			$('#acordeon').accordion({
				heightStyle: "content"
			});
			$('input:checkbox').click(function() {
				var element = $(this).attr('id');
				if ($(this).is(":checked")) {
					$('.' + element).removeAttr('disabled');
				} else {
					$('.' + element).attr('disabled', 'disabled');
				}
			});
		});
	</script>
	<!-- ESTILOS EMBEBIDOS PARA MANEJAR FORMULARIO INDEPENDIENTEMENTE -->
	<style type="text/css">
		#form input{
			width: auto!important;
		}
		.permission-list{
			background: #e6e6e6;
			border-radius: 15px;
			padding-top: 10px;
			padding-bottom: 10px;
		}
	</style>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
		<?php require($r . 'incluir/src/menu.php') ?>
		<!-- INCLUIMOS MENU PRINCIPAL -->
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Administracion</a>
				<div class="mapa_div"></div><a class="current">Perfiles</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="insertar.php" method="post">
					<p>
						<label for="nombre" class="tabx">Nombre del perfil:</label> <!-- CAMPO NOMBRE -->
						<input type="text" name="nombre" class="nombre validate[required, ajax[ajaxNameCall]] text-input" title="Digite el nombre del perfil" style="width: 49%!important;"/>
					</p>
					<div class="row">
						<div class="col-md-6">
							<div class="list-group" id="list-tab" role="tablist">
								<a class="list-group-item list-group-item-action active" id="list-Administracion-list" data-toggle="list" href="#list-Administracion" role="tab" aria-controls="Administracion">Administración</a>
								<a class="list-group-item list-group-item-action" id="list-contratos-list" data-toggle="list" href="#list-contratos" role="tab" aria-controls="Contratos">Contratos</a>
								<a class="list-group-item list-group-item-action" id="list-recibos-list" data-toggle="list" href="#list-recibos" role="tab" aria-controls="Recibos">Recibos</a>
								<a class="list-group-item list-group-item-action" id="list-Bodega-list" data-toggle="list" href="#list-Bodega" role="tab" aria-controls="Bodega">Bodega</a>
								<a class="list-group-item list-group-item-action" id="list-Cartera-list" data-toggle="list" href="#list-Tesoreria" role="tab" aria-controls="Tesoreria">Tesoreria</a>
								<a class="list-group-item list-group-item-action" id="list-consultas-list" data-toggle="list" href="#list-consultas" role="tab" aria-controls="Consultas">Reportes Consultas</a>
								<a class="list-group-item list-group-item-action" id="list-informes-list" data-toggle="list" href="#list-informes" role="tab" aria-controls="Informes">Reportes Informes</a>
							</div>
						</div>
						<div class="col-md-6 permission-list">
							<div class="tab-content" id="nav-tabContent" style="height:25em; overflow-y:auto; overflow-x:hidden;">
								<!-- Administracion -->
								<div class="tab-pane fade show active" id="list-Administracion" role="tabpanel" aria-labelledby="list-Administracion-list">
									<input type="checkbox" name="admege" class="admin" value="1" /> Estructura geografica<br />
									<input type="checkbox" name="admper" class="admin" value="1" /> Perfiles<br />
									<input type="checkbox" name="admusu" class="admin" value="1" /> Usuarios<br />
									<input type="checkbox" name="admcli" class="admin" value="1" /> Clientes<br />
								</div>

								<!-- Contratos -->
								<div class="tab-pane fade" id="list-contratos" role="tabpanel" aria-labelledby="list-contratos-list">
									<input type="checkbox" name="contrato_agregar" value="1" /> Agregar contratos<br />
									<input type="checkbox" name="contrato_editar" value="1" /> Editar contratos<br />
									<input type="checkbox" name="contrato-ver" value="1" /> Ver contratos<br />
								</div>

								<!-- Recibos -->
								<div class="tab-pane fade" id="list-recibos" role="tabpanel" aria-labelledby="list-recibos-list">
									<input type="checkbox" name="r_cobrarse_add" value="1" /> Agregar recibos a cobrarse<br />
									<input type="checkbox" name="r_cobrarse_edit" value="1" /> Editar recibos a cobrarse<br />
									<input type="checkbox" name="r_pagados_add" value="1" /> Agregar recibos cobrados<br />
									<input type="checkbox" name="r_pagados_edit" value="1" /> Editar recibos cobrados<br />
									<input type="checkbox" name="r_imprimir" value="1" /> Imprimir Recibos<br />
								</div>
								
								<!-- Consultas -->
								<div class="tab-pane fade" id="list-consultas" role="tabpanel" aria-labelledby="list-consultas-list">
									
									<button type="button" class="btn btn-info" onclick="seleccionar_todo('.check-consulta');">Seleccionar Todo</button>
									<button type="button" class="btn btn-warning" onclick="deseleccionar('.check-consulta');">Quitar Todo</button>
									<br>
									<?php
										$consultas = $db->query("SELECT * FROM consultasdinamicas where cdiactiva = true ORDER BY cdinombre ASC");
										while($row_consulta = $consultas->fetch(PDO::FETCH_ASSOC))
										{
											$con_nombre = $row_consulta['cdinombre'];
											$con_id = $row_consulta['cdiid'];
											echo "<input class='check-consulta' type='checkbox' name='consultas[]' value='$con_id' /> $con_nombre<br />";
										}
									?>
								</div>
								
								<!-- Informes -->
								<div class="tab-pane fade" id="list-informes" role="tabpanel" aria-labelledby="list-informes-list">
									<button type="button" class="btn btn-info" onclick="seleccionar_todo('.check-informe');">Seleccionar Todo</button>
									<button type="button" class="btn btn-warning" onclick="deseleccionar('.check-informe');">Quitar Todo</button>
									<br>
									<?php
										$consultas = $db->query("SELECT * FROM informesdinamicos WHERE hide is false ORDER BY idinombre ASC");
										while($row_consulta = $consultas->fetch(PDO::FETCH_ASSOC))
										{
											$inf_nombre = $row_consulta['idinombre'];
											$inf_id = $row_consulta['idiid'];
											echo "<input class='check-informe' type='checkbox' name='informes[]' value='$inf_id' /> $inf_nombre<br />";
										}
									?>
								</div>

								<!-- Bodega -->
								<div class="tab-pane fade" id="list-Bodega" role="tabpanel" aria-labelledby="list-Bodega-list">
									<input type="checkbox" name="admpro" class="admin" value="1" /> Agregar Productos<br />
									<input type="checkbox" name="boddoc" class="bodega" value="1" /> Documentos<br />
									<input type="checkbox" name="bofactura" class="bodega" value="1" /> Facturación<br />
									<input type="checkbox" name="bodkar" class="bodega" value="1" /> Kardex<br />
								</div>

								<!-- tesoreria -->
								<div class="tab-pane fade" id="list-Tesoreria" role="tabpanel" aria-labelledby="list-Tesoreria-list">
									<input type="checkbox" name="tesoreria" class="tesoreria" value="1" /> Tesoreria<br />
								</div>
								
								
							</div>
						</div>
					</div>
					<p class="boton">
						<button type="submit" class="btn btn-primary btninsertar" name="insertar" value="insertar">Insertar</button> <!-- BOTON INSERTAR -->
					</p>
				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
		<!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
	</section>
	<?php
	if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>'; // VENTANA MODAL ERROR
	elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>'; // VENTANA MODAL EXITOSO
	?>
</body>

<script>
	function seleccionar_todo(element)
	{
		$(element).prop('checked', true);
	}
	function deseleccionar(element)
	{
		$(element).prop('checked', false);
	}
	$("#form").keypress(function(e) {
		if (e.which == 13) {
			e.preventDefault();
		}
	});
</script>
</html>