<?php
// LA ACCION PRINCIPAL ES ESTA PAGINA ES CONSULTAR LAS CIUDADES 
$r = '../../../../';
//INCLUIR SESION Y CONECCION
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
?>
<!doctype html>
<html lang="es">

<head>
    <title>CONSUTAR CIUDADES</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<script>
		$(document).ready(function() {
			$('#form').validationEngine({ //VALIDACION DE CAMPOS REQUERIDOS EN FORMULARIO
				showOneMessage: true,
				onValidationComplete: function(form, status) {
					if (status) {
						carga();
						return true;
					}
				}
			});
			$('.btnconsulta').button({
				icons: {
					primary: 'ui-icon ui-icon-search'
				}
			}); //ACCION DE LA CLASE EN EL BOTON DE CONSULTA
		});
	</script>
	<!-- ESTILOS EMBEBIDOS PARA MANEJAR FORMULARIO INDEPENDIENTEMENTE -->
	<style type="text/css">
		#form fieldset {
			padding: 10px;
			display: block;
			width: 410px;
			margin: 5px auto
		}

		#form legend {
			font-weight: bold;
			margin-left: 5px;
			padding: 5px
		}

		#form label {
			display: inline-block;
			width: 100px;
			text-align: right;
			margin: 0.3em 2% 0 0
		}

		#form p {
			margin: 5px 0
		}
	</style>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
		<?php require($r . 'incluir/src/menu.php') ?>
		<!-- INCLUIMOS MENU PRINCIPAL -->
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Administracion</a>
				<div class="mapa_div"></div><a href="#">E. geografica</a>
				<div class="mapa_div"></div><a class="current">Ciudades</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="listar.php" method="post">
					<!-- ENVIO FORMULARIO POR POST A LISTAR.PHP -->
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-5">
						<legend class="ui-widget ui-widget-header ui-corner-all">Consultar ciudades</legend>
						<p>
							<label class=" col-md-12" for="depto">Provincia:</label> <!-- MOSTRAMOS DEPARTAMENTOS DESDE LA BD  -->
							<select class="form-control  col-md-12" id="depto" name="depto">
								<option value="">TODOS</option>
								<?php
								$qry = $db->query("SELECT * FROM departamentos ORDER BY depnombre"); // QRY DEPARTAMENTOS
								while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
									echo '<option value=' . $row['depid'] . '>' . $row['depnombre'] . '</option>';	// ASOCIAMOS POR NOMBRE DE DEPARTAMENTO Y EL ID QUE LA IDENTIFICA
								}
								?>
							</select>
						</p>
						<p>
							<label class=" col-md-12" for="id">Codigo:</label> <!-- CAMPO CODIGO -->
							<input type="text" name="id" class="form-control codigo validate[custom[onlyNumberSp]] text-input col-md-12" title="Digite el codigo del departamento" />
						</p>
						<p>
							<label class=" col-md-12" for="nombre">Nombre:</label> <!-- BUSCAR POR SIMILITUDES EN EL NOMBRE DEL TIPO DE LA CAUSAL CON UN LIKE EN LA BD -->
							<input type="text" name="nombre" class="form-control nombre col-md-12" title="Digite el nombre del departamento" />
						</p>
						<div class="row">
							<div class="col-md-12 col-lg-6">
								<button type="submit" class="btnconsulta btn btn-primary btn-block mt-2" name="consultar" value="Buscar">consultar</button> <!-- BOTON CONSULTAR -->
							</div>
							<div class="col-md-12 col-lg-6">
								<button type="submit" class="btnconsulta btn btn-primary btn-block mt-2" name="show-all" value="Buscar">Mostrar Todo</button>							</div>
						</div>
					</fieldset>
				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
		<!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
	</section>
</body>

</html>