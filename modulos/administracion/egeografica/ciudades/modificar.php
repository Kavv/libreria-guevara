<?php
// LA ACCION PRINCIPAL DE ESTA PAGINA ES MODIFICA UNA CIUDAD ENVIANDOLO A LISTAR

$r = '../../../../';
//INCLUIR SESION Y CONECCION
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
$filtro = 'depto=' . $_GET['depto'] . '&id=' . $_GET['id'] . '&nombre=' . $_GET['nombre']; //FILTRO GENERAL 
$qry = $db->query("SELECT * FROM ciudades INNER JOIN departamentos ON ciudepto = depid WHERE ciudepto = '" . $_GET['id1'] . "' AND ciuid = '" . $_GET['id2'] . "'"); // CONSULTA A LA BD CON VARUABLES RECIBIDAS POR GET
$row = $qry->fetch(PDO::FETCH_ASSOC)
?>
<!doctype html>
<html lang="es">

<head>
    <title>MODIFICAR CIUDAD</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>

	<!-- ESTILOS EMBEBIDOS PARA MANEJAR FORMULARIO INDEPENDIENTEMENTE -->
	<style type="text/css">
		#form fieldset {
			padding: 10px;
			display: block;
			width: 410px;
			margin: 5px auto
		}

		#form legend {
			font-weight: bold;
			margin-left: 5px;
			padding: 5px
		}

		#form label {
			display: inline-block;
			width: 100px;
			text-align: right;
			margin: 0.3em 2% 0 0
		}

		#form p {
			margin: 5px 0
		}
	</style>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
		<?php require($r . 'incluir/src/menu.php') ?>
		<!-- INCLUIMOS MENU PRINCIPAL -->
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Administracion</a>
				<div class="mapa_div"></div><a href="#">E. geografica</a>
				<div class="mapa_div"></div><a class="current">Ciudades</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="listar.php?<?php echo $filtro ?>" method="post">
					<!-- FORMULARIO ENVIADO POR POST A LISTAR.PHP JUNTO CON EL FILTRO GENERAL  -->
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">Modificar ciudad</legend>
						<p>
							<label for="depto">Provincia:</label> <!-- CAMPO DEPARTAMENTO -->
							<select id="depto" name="depto" class="validate[required] text-input">
								<?php
								echo '<option value=' . $row['ciudepto'] . '>' . $row['depnombre'] . '</option>'; // DEPARTAMENTOS AGRUPADAS EN UN SELECT
								?>
							</select>
						</p>
						<p>
							<label for="id">Codigo:</label>
							<input type="text" name="id" class="codigo" value="<?php echo $row['ciuid'] ?>" readonly /> <!--  CAMPO DE CODIGO DE LA CIUDAD -->
						</p>
						<p>
							<label for="nombre">Nombre:</label>
							<input type="text" name="nombre" class="nombre validate[required] text-input" value="<?php echo $row['ciunombre'] ?>" title="Digite el nombre del departamento" /> <!--  CAMPO NOMBRE -->
						</p>
						<p class="boton">
							<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href='listar.php?<?php echo $filtro ?>'">atras</button> <!--  BOTON PARA VOLVER ATRAS JUNTO CON FILTRO GENERAL-->
							<button type="submit" class="btn btn-primary btnmodificar" name="modificar" value="modificar">modificar</button> <!-- BOTON MODIFICAR -->
						</p>
					</fieldset>
				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
		<!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
	</section>
</body>

</html>