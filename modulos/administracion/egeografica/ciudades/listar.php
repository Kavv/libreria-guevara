<?php
// LA ACCION PRINCIPAL DE ESTA PAGINA ES MOSTRAR LOS RESULTADOS DE LA CONSULTA GENERADA DESDE CONSULTAR.PHP
// LUEGO DE HABER RECIBIDO LAS VARIABLES POR POST DESDE CONSULTAR.PHP PROCEDEMOS A EVALUAR ESAS VARIABLES PARA MOSTRAR RESULTADOS DE LA CONSULTA O MODIFICAR SI ASI CORRESPONDE

//INCLUIR SESION Y CONECCION
$r = '../../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
if (isset($_POST['modificar'])) { //VALIDAMOS SI POST MODIFICAR ES TRUE
	$depto = $_POST['depto'];
	$id = $_POST['id'];
	$nombre = strtoupper(trim($_POST['nombre'])); // LO RECIBIDO DE ESTE CAMPO LO CAMBIAMOS A MAYUSCULA
	$num = $db->query("SELECT * FROM ciudades WHERE ciudepto = '$depto' AND ciuid <> '$id' AND ciunombre = '$nombre'")->rowCount(); // REALIZAMOS CONSULTA CON VARIABLES RECIBIDAS Y CONTAMOS LOS RESULTADOS OBTENIDOS
	if ($num < 1) { // SI LOS RESULTADOS DE LA CONSULTA SON MENORES A  1 REALIZAMOS LO SIGUIENTE
		$qry = $db->query("UPDATE ciudades SET ciunombre = '$nombre' WHERE ciudepto = '$depto' AND ciuid = '$id'"); // ACTUALIZAMOS LA INFORMACION CON LOS NUEVOS DATOS RECIBIDOS
		if ($qry) $mensaje = 'Se actualizo la ciudad'; // MENSAJE EXITOSO
		else $error = 'No se modifico la ciudad'; // MENSAJE ERROR
	} else $error = 'Ya existe una ciudad, en este departamento, con este nombre';
}
if (isset($_GET['id1'])) { //VALIDAMOS SI GET ID1 ES TRUE 
	$id1 = $_GET['id1'];
	$id2 = $_GET['id2'];
	$num = $db->query("SELECT * FROM clientes WHERE (clidepresidencia = '$id1' AND cliciuresidencia = '$id2') OR (clidepcomercio = '$id1' AND cliciucomercio = '$id2') OR (clidepadicional = '$id1' AND cliciuadicional = '$id2')")->rowCount(); // REALIZAMOS CONSULTA CON VARIABLES RECIBIDAS Y CONTAMOS LOS RESULTADOS OBTENIDOS
	if ($num < 1) { // SI LOS RESULTADOS DE LA CONSULTA SON MENORES A  1 REALIZAMOS LO SIGUIENTE
		$num = $db->query("SELECT * FROM solicitudes WHERE (soldepentrega = '$id1' AND solciuentrega = '$id2') OR (soldepcobro = '$id1' AND solciucobro = '$id2')")->rowCount();
		if ($num < 1) {
			$qry = $db->query("DELETE FROM ciudades WHERE ciudepto = '$id1' AND ciuid = '$id2'"); //ELIMINAMOS LA CIUDAD
			if ($qry) $mensaje = 'Se elimino la ciudad';
			else $error = 'No se pudo eliminar la ciudad';
		} else $error = 'Existen solicitudes asociadas a la ciudad';
	} else $error = 'Existen clientes asociadas a la ciudad';
}
$depto = $id = $nombre = "";
if (isset($_POST['consultar'])) { //VALIDAMOS SI POST CONSULTAR ES TRUE
	$depto = $_POST['depto'];
	$id = $_POST['id'];
	$nombre = $_POST['nombre'];
} else if (isset($_GET['id'])) { // SI LO RECIBIDO POR POST CONSULTAR NO ES TRUE
	$depto = $_GET['depto'];
	$id = $_GET['id'];
	$nombre = $_GET['nombre'];
}
$filtro = 'depto=' . $depto . '&id=' . $id . '&nombre=' . $nombre;  // FILTRO GENERAL PARA ENVIAR POR GET
if ($depto != '' && $id == '' && $nombre == '') $qry = $db->query("SELECT * FROM ciudades INNER JOIN departamentos ON ciudepto = depid WHERE ciudepto = '$depto'");  // VALIDAMOS LOS CAMPOS RECIBIDOS
elseif ($depto == '' && $id != '' && $nombre == '') $qry = $db->query("SELECT * FROM ciudades INNER JOIN departamentos ON ciudepto = depid WHERE ciuid = '$id'");
elseif ($depto == '' && $id == '' && $nombre != '') $qry = $db->query("SELECT * FROM ciudades INNER JOIN departamentos ON ciudepto = depid WHERE ciunombre LIKE '%$nombre%'");
else $qry = $db->query("SELECT * FROM ciudades INNER JOIN departamentos ON ciudepto = depid");
?>
<!doctype html>
<html lang="es">

<head>
    <title>LISTA DE LAS CIUDADES</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
	<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
	<script>
		$(document).ready(function() {
			$('#tabla').dataTable({ //QRY PARA MANEJO Y VISUALIZACION DE TABLAS
				'bJQueryUI': true,
				'sPaginationType': 'full_numbers',
				'oLanguage': {
					'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
				},
				'aoColumnDefs': [{
					'bSortable': false,
					'aTargets': [3, 4]
				}],
			});

			$('.confirmar').click(function(e) { // VENTANA MODAL PARA CONFIRMAR ACCIONES
				e.preventDefault();
				var targetUrl = $(this).attr('href');
				var $dialog_link_follow_confirm = $('<div></div>').html('<p><span class="ui-icon ui-icon-trash" style="float:left; margin:2px 7px 7px 0;"></span>Esta seguro que desea eliminar la ciudad?</p>').
				dialog({
					title: 'Porfavor confirmar',
					buttons: {
						'Si': function() {
							carga();
							window.location.href = targetUrl;
						},
						'No': function() {
							$(this).dialog('close');
						}
					},
					modal: true,
					width: 'auto',
					height: 'auto'
				});
				$dialog_link_follow_confirm.dialog('open');
			});

		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
		<?php require($r . 'incluir/src/menu.php') ?>
		<!-- INCLUIMOS MENU PRINCIPAL -->
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Administracion</a>
				<div class="mapa_div"></div><a href="#">E. geografica</a>
				<div class="mapa_div"></div><a class="current">Ciudades</a>
			</article>
			<article id="contenido">
				<h2>Listado de ciudades</h2>
				<div class="reporte">

				</div>
				<!-- INICIO DE TABLA -->
				<table id="tabla">
					<thead>
						<tr>
							<th>dep. Código</th>
							<th>Departamento</th>
							<th>Ciu. Código</th>
							<th>Nombre</th>
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php
						while ($row = $qry->fetch(PDO::FETCH_ASSOC)) { //INFORMACION DE LA CONSULTA
						?>
							<tr>
								<td><?php echo $row['ciudepto'] ?></td>
								<td><?php echo $row['depnombre'] ?></td>
								<td align="center"><?php echo $row['ciuid'] ?></td>
								<td><?php echo $row['ciunombre'] ?></td>
								<td align="center"><a href="modificar.php?<?php echo 'id1=' . $row['ciudepto'] . '&id2=' . $row['ciuid'] . '&' . $filtro ?>" onClick="carga()" title="editar"><img src="<?php echo $r ?>imagenes/iconos/lapiz.png" class="grayscale" /></a></td> <!-- ENVIA POR GET LAS VARIABLES NECESARIAS PARA MODIFICAR UNA CIUDAD -->
								<td align="center"><a href="listar.php?<?php echo 'id1=' . $row['ciudepto'] . '&id2=' . $row['ciuid'] . '&' . $filtro ?>" title="eliminar" class="confirmar"><img src="<?php echo $r ?>imagenes/iconos/basura.png" class="grayscale" /></a></td> <!-- ENVIA LAS VARIABLES NECESARIAS PARA ELIMINAR UNA CIUDAD JUNTO CON VENTANA MODAL PARA CONFIRMAR -->
							</tr>
						<?php
						}
						?>
					</tbody>
				</table>
				<p class="boton">
					<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href='consultar.php'">Atras</button> <!-- BOTON PARA VOLVER A CONSULTAR -->
				</p>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
		<!-- BOTON PARA VOLVER A LA PARTE SUPERIOR  -->
	</section>
	<?php
	if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>'; // VENTANA MODAL ERROR
	elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>'; // VENTANA MODAL EXITOSO
	?>
</body>

</html>