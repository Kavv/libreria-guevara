<?php
// LA ACCION PRINCIPAL DE ESTA PAGINA ES INSERTAR UNA NUEVA CIUDAD

$r = '../../../../';
//INCLUIR SESION Y CONECCION
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
if (isset($_POST['insertar'])) 
{ // VALIDAMOS QUE POST INSERTAR SEA TRUE
	$depto = $_POST['depto'];
	$id = trim($_POST['id']);
	$nombre = trim(strtoupper($_POST['nombre']));
	$qry = $db->query("INSERT INTO ciudades (ciudepto, ciuid, ciunombre) VALUES ('$depto', '$id', '$nombre')"); // REALIZAMOS EL INSERT EN LA BD SEGUN LOS DATOS RECIBIDOS
	if ($qry) $mensaje = 'Se inserto la ciudad';  // MENSAJE MODAL EXITOSO
	else $error = 'No se pudo insertar la ciudad'; // MENSAJE MODAL ERROR
}
?>
<!doctype html>
<html lang="es">

<head>
    <title>AGREGAR CIUDAD</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>

</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
		<?php require($r . 'incluir/src/menu.php') ?>
		<!-- INCLUIMOS MENU PRINCIPAL -->
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Administracion</a>
				<div class="mapa_div"></div><a href="#">E. geografica</a>
				<div class="mapa_div"></div><a class="current">Ciudades</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="insertar.php" method="post">
					<!-- FORMULARIO ENVIADO POR POST A SI MISMO -->
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">Insertar ciudad</legend>
						<p>
							<label class="col-md-12" for="depto">Departamento:</label>
							<select id="depto" name="depto" class="col-md-12 validate[required] text-input">
								<option value="">SELECCIONE</option>
								<?php
								$qry = $db->query("SELECT * FROM departamentos ORDER BY depnombre"); // CONSULTA DE DEPARTAMENTOS ORDENADAS POR NOMBRE
								while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
									echo '<option value=' . $row['depid'] . '>' . $row['depnombre'] . '</option>'; //AGRUPAMOS TODOS LOS DEPARTAMENTOS JUNTO CON SU ID EN UN SELECT
								}
								?>
							</select>
						</p>
						<p>
							<label class="col-md-12" for="id">Codigo:</label> <!-- CAMPO CODIGO -->
							<input type="text" name="id" class="col-md-12 codigo validate[required, minSize[2], maxSize[2], custom[onlyNumberSp], ajax[IdCity]] text-input" title="Digite el codigo de la ciudad" />
						</p>
						<p>
							<label class="col-md-12" for="nombre">Nombre:</label>
							<input type="text" name="nombre" class="col-md-12 nombre validate[required, ajax[NameCity]] text-input" title="Digite el nombre de la ciudad" />
						</p>
						<p class="boton">
								<input type="hidden" name="insertar">
							<button type="submit" id="botton" class="btn btn-primary btninsertar" name="insertar" value="insertar">insertar</button> <!-- BOTON INSERTAR -->
						</p>
					</fieldset>
				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
		<!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
	</section>
	<?php
	if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>'; //VENTANA MODAL ERROR
	elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>'; //VENTANA MODAL EXITOSO
	?>
</body>

</html>