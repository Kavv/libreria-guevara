<?php
		// LA ACCION PRINCIPAL DE ESTA PAGINA ES MOSTRAR LOS RESULTADOS DE LA CONSULTA GENERADA DESDE CONSULTAR.PHP
		// LUEGO DE HABER RECIBIDO LAS VARIABLES POR POST DESDE CONSULTAR.PHP PROCEDEMOS A EVALUAR ESAS VARIABLES PARA MOSTRAR RESULTADOS DE LA CONSULTA O MODIFICAR SI ASI CORRESPONDE

		//INCLUIR SESION Y CONECCION
		$r = '../../../../';
		require($r . 'incluir/session.php');
		require($r . 'incluir/connection.php');
		if (isset($_POST['modificar'])) { //VALIDAMOS SI POST MODIFICAR ES TRUE
			$id = $_POST['id'];
			$nombre = strtoupper(trim($_POST['nombre'])); // LO RECIBIDO DE ESTE CAMPO LO CAMBIAMOS A MAYUSCULA
			$num = $db->query("SELECT * FROM departamentos WHERE depid <> '$id' AND depnombre = '$nombre'")->rowCount(); // REALIZAMOS CONSULTA CON VARIABLES RECIBIDAS Y CONTAMOS LOS RESULTADOS OBTENIDOS
			if ($num < 1) { // SI LOS RESULTADOS DE LA CONSULTA SON MENORES A  1 REALIZAMOS LO SIGUIENTE
				$qry = $db->query("UPDATE departamentos SET depnombre = '$nombre' WHERE depid = '$id'");  // ACTUALIZAMOS LA INFORMACION CON LOS NUEVOS DATOS RECIBIDOS
				if ($qry) $mensaje = 'Se actualizo el departamento'; // MENSAJE EXITOSO
				else $error = 'No se modifico el departamento'; // MENSAJE ERROR
			} else $error = 'Ya existe un departamento con este nombre';
		}
		if (isset($_GET['id1'])) { //VALIDAMOS SI GET ID1 ES TRUE 
			$id1 = $_GET['id1'];
			$num = $db->query("SELECT * FROM ciudades WHERE ciudepto = '$id1'")->rowCount(); // REALIZAMOS CONSULTA CON VARIABLES RECIBIDAS Y CONTAMOS LOS RESULTADOS OBTENIDOS
			if ($num < 1) {
				$qry = $db->query("DELETE FROM departamentos WHERE depid = '$id1'"); //ELIMINAMOS EL DEPARTAMENTO
				if ($qry) $mensaje = 'Se elimino el departamento';
				else $error = 'No se pudo eliminar el departamento';
			} else $error = 'Tiene ciudades asociadas al departamento';
		}
		$id = $nombre = "";

		if (isset($_POST['consultar'])) { //VALIDAMOS SI POST CONSULTAR ES TRUE
			$id = $_POST['id'];
			$nombre = $_POST['nombre'];
		} else if (isset($_GET['id'])) { // SI LO RECIBIDO POR POST CONSULTAR NO ES TRUE
			$id = $_GET['id'];
			$nombre = $_GET['nombre'];
		}

		$filtro = 'id=' . $id . '&nombre=' . $nombre;  // FILTRO GENERAL PARA ENVIAR POR GET
		if ($id != '' && $nombre == '') 
			$qry = $db->query("SELECT * FROM departamentos WHERE depid = '$id'"); // VALIDAMOS LOS CAMPOS RECIBIDOS
		elseif ($id == '' && $nombre != '') 
			$qry = $db->query("SELECT * FROM departamentos WHERE depnombre LIKE '%$nombre%'");
		else 
			$qry = $db->query("SELECT * FROM departamentos");
?>
<!doctype html>
<html lang="es">

<head>
    <title>LISTAR DEPARTAMENTO</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
	<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
	<script>
		$(document).ready(function() {
			$('#tabla').dataTable({ //QRY PARA MANEJO Y VISUALIZACION DE TABLAS
				'bJQueryUI': true,
				'sPaginationType': 'full_numbers',
				'oLanguage': {
					'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
				},
				'aoColumnDefs': [{
					'bSortable': false,
					'aTargets': [2, 3]
				}],
			});
			$('.confirmar').click(function(e) { // VENTANA MODAL PARA CONFIRMAR ACCIONES
				e.preventDefault();
				var targetUrl = $(this).attr('href');
				var $dialog_link_follow_confirm = $('<div></div>').html('<p><span class="ui-icon ui-icon-trash" style="float:left; margin:2px 7px 7px 0;"></span>Esta seguro que desea eliminar el departamento?</p>').
				dialog({
					title: 'Porfavor confirmar',
					buttons: {
						'Si': function() {
							carga();
							window.location.href = targetUrl;
						},
						'No': function() {
							$(this).dialog('close');
						}
					},
					modal: true,
					width: 'auto',
					height: "auto"
				});
				$dialog_link_follow_confirm.dialog('open');
			});
		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
		<?php require($r . 'incluir/src/menu.php') ?>
		<!-- INCLUIMOS MENU PRINCIPAL -->
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Administracion</a>
				<div class="mapa_div"></div><a href="#">E. geografica</a>
				<div class="mapa_div"></div><a class="current">Departamentos</a>
			</article>
			<article id="contenido">
				<h2>Listado de departamentos</h2>
				<div class="reporte">
					
				</div>
				<!-- INICIO DE TABLA -->
				<table id="tabla">
					<thead>
						<tr>
							<th>Codigo</th>
							<th>Nombre</th>
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php
						while ($row = $qry->fetch(PDO::FETCH_ASSOC)) { //INFORMACION DE LA CONSULTA
						?>
							<tr>
								<td align="center"><?php echo $row['depid'] ?></td>
								<td><?php echo $row['depnombre'] ?></td>
								<td align="center"><a href="modificar.php?<?php echo 'id1=' . $row['depid'] . '&' . $filtro ?>" onClick="carga()" title="editar"><img src="<?php echo $r ?>imagenes/iconos/lapiz.png" class="grayscale" /></a></td> <!-- ENVIA POR GET LAS VARIABLES NECESARIAS PARA MODIFICAR UNA CIUDAD -->
								<td align="center"><a href="listar.php?<?php echo 'id1=' . $row['depid'] . '&' . $filtro ?>" title="eliminar" class="confirmar"><img src="<?php echo $r ?>imagenes/iconos/basura.png" class="grayscale" /></a></td> <!-- ENVIA LAS VARIABLES NECESARIAS PARA ELIMINAR UNA CIUDAD JUNTO CON VENTANA MODAL PARA CONFIRMAR -->
							</tr>
						<?php
						}
						?>
					</tbody>
				</table>
				<p class="boton">
					<button type="button" class="btn btn-outline-primary btnatras" onClick="carga(); location.href='consultar.php'">
						<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-left-square-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
							<path fill-rule="evenodd" d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm9.5 8.5a.5.5 0 0 0 0-1H5.707l2.147-2.146a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708-.708L5.707 8.5H11.5z"/>
						</svg>
						Atras
					</button> <!-- BOTON PARA VOLVER A CONSULTAR -->
				</p>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
		<!-- BOTON PARA VOLVER A LA PARTE SUPERIOR  -->
	</section>
	<?php
	if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>'; // VENTANA MODAL ERROR
	elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>'; // VENTANA MODAL EXITOSO
	?>
</body>

</html>