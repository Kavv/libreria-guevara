<?php
// LA ACCION PRINCIPAL DE ESTA PAGINA ES INSERTAR UN NUEVO DEPARTAMENTO

$r = '../../../../';
//INCLUIR SESION Y CONECCION
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
if (isset($_POST['insertar'])) { // VALIDAMOS QUE POST INSERTAR SEA TRUE
	$id = trim($_POST['id']);
	$nombre = trim(strtoupper($_POST['nombre']));
	$qry = $db->query("INSERT INTO departamentos (depid, depnombre) VALUES ('$id', '$nombre')") or die($db->errorInfo()[2]); // REALIZAMOS EL INSERT EN LA BD
	
	if ($qry) 
	{
		$mensaje = 'Se inserto el departamento'; // MENSAJE MODAL EXITOSO
		header("Location:insertar.php?msj=$mensaje");
		exit();
	}
	else 
	{
		$error = 'No se pudo insertar el departamento'; // MENSAJE MODAL ERROR
		header("Location:insertar.php?error=$error");
		exit();
	}


}
?>
<!doctype html>
<html lang="es">

<head>
    <title>AGREGAR DEPARTAMENTO</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<!-- ESTILOS EMBEBIDOS PARA MANEJAR FORMULARIO INDEPENDIENTEMENTE -->
	<style type="text/css">

		#form label {
			display: inline-block;
			text-align: left;
			margin: 0.3em 2% 0 0
		}

		#form p {
			margin: 5px 0
		}
	</style>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
		<?php require($r . 'incluir/src/menu.php') ?>
		<!-- INCLUIMOS MENU PRINCIPAL -->
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Administracion</a>
				<div class="mapa_div"></div><a href="#">E. geografica</a>
				<div class="mapa_div"></div><a class="current">Departamentos</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="insertar.php" method="post">
					<!-- FORMULARIO ENVIADO POR POST A SI MISMO -->
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-5">
						<legend class="ui-widget ui-widget-header ui-corner-all">Insertar departamento</legend>
						<p>
							<label class="col-md-12"for="id">Codigo:</label> <!-- CAMPO CODIGO -->
							<input type="text" name="id" class="col-md-12 codigo validate[required, custom[onlyNumberSp], minSize[0], maxSize[2], ajax[Id]] text-input" title="Digite el codigo del departamento" />
						</p>
						<p>
							<label class="col-md-12" for="nombre">Nombre:</label> <!-- CAMPO NOMBRE -->
							<input type="text" name="nombre" class="col-md-12 nombre validate[required, ajax[ajaxNameCall]] text-input" title="Digite el nombre del departamento" />
						</p>
						<p class="boton">
							<input type="hidden" name='insertar'>
							<button type="submit" id="botton" class="btn btn-primary btninsertar" name="insertar">Insertar</button>
						</p>
					</fieldset>
				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
		<!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
	</section>
	<?php
	if(isset($_GET['error'])) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['error'] . '</div>'; //VENTANA MODAL ERROR
	elseif(isset($_GET['msj'])) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['msj'] . '</div>'; //VENTANA MODAL EXITOSO
	?>
</body>

</html>