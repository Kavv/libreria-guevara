<?php
// LA ACCION PRINCIPAL DE ESTA PAGINA ES MODIFICA UNA CAUSAL ENVIANDOLO A LISTAR

$r = '../../../../';
//INCLUIR SESION Y CONECCION
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
$filtro = 'id='.$_GET['id'].'&nombre='.$_GET['nombre']; //FILTRO GENERAL 
$qry = $db->query("SELECT * FROM departamentos WHERE depid = '".$_GET['id1']."'"); // CONSULTA A LA BD CON VARUABLES RECIBIDAS POR GET
$row = $qry->fetch(PDO::FETCH_ASSOC)
?>
<!doctype html>
<html lang="es">

<head>
    <title>MODIFICAR DEPARTAMENTO</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>


</head>
<body>
<?php require($r.'incluir/src/login.php') ?> <!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
<section id="principal">
<?php require($r.'incluir/src/cabeza.php') ?> <!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
<?php require($r.'incluir/src/menu.php') ?> <!-- INCLUIMOS MENU PRINCIPAL -->
<article id="cuerpo">
<article class="mapa">
<a href="#">Principal</a><div class="mapa_div"></div><a href="#">Administracion</a><div class="mapa_div"></div><a href="#">E. geografica</a><div class="mapa_div"></div><a class="current">Departamentos</a>
</article>
<article id="contenido">
<form id="form" name="form" action="listar.php?<?php echo $filtro ?>" method="post"> <!-- FORMULARIO ENVIADO POR POST A LISTAR.PHP JUNTO CON EL FILTRO GENERAL  -->
<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
<legend class="ui-widget ui-widget-header ui-corner-all">Modificar departamento</legend>
<p>
<label class="col-md-12" for="id">Codigo:</label> <!-- CAMPO CODIGO -->
<input type="text" name="id" class="col-md-12 codigo" value="<?php echo $row['depid'] ?>" readonly />
</p>
<p>
<label class="col-md-12" for="nombre">Nombre:</label> <!-- CAMPO NOMBRE -->
<input type="text" name="nombre" class="col-md-12 nombre validate[required] text-input" value="<?php echo $row['depnombre'] ?>" title="Digite el nombre del departamento" />
</p>
<p class="boton">
<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href='listar.php?<?php echo $filtro ?>'">atras</button>  <!--  BOTON PARA VOLVER ATRAS JUNTO CON FILTRO GENERAL-->
<button type="submit" class="btn btn-primary btnmodificar" name="modificar" value="modificar">modificar</button> <!-- BOTON MODIFICAR -->
</p>
</fieldset>
</form>
</article>
</article>
<?php require($r.'incluir/src/pie.php') ?>  <!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
</section>
</body>
</html>