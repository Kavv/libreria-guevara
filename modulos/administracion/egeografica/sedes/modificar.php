<?php
// LA ACCION PRINCIPAL DE ESTA PAGINA ES MODIFICA UNA CAUSAL ENVIANDOLO A LISTAR

$r = '../../../../';
//INCLUIR SESION Y CONECCION
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
$filtro = 'depto='.$_GET['depto'].'&ciudad='.$_GET['ciudad'].'&nombre='.$_GET['nombre']; //FILTRO GENERAL 
$qry = $db->query("SELECT * FROM (sedes INNER JOIN ciudades ON (seddepto = ciudepto AND sedciudad = ciuid)) INNER JOIN departamentos ON seddepto = depid WHERE seddepto = '".$_GET['id1']."' AND sedciudad = '".$_GET['id2']."' AND sedid = ".$_GET['id3']); // CONSULTA A LA BD CON VARUABLES RECIBIDAS POR GET
$row = $qry->fetch(PDO::FETCH_ASSOC)
?>
<!doctype html>
<html>
<head>

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>

</head>
<body>
<?php require($r.'incluir/src/login.php') ?> <!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
<section id="principal">
<?php require($r.'incluir/src/cabeza.php') ?> <!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
<?php require($r.'incluir/src/menu.php') ?>  <!-- INCLUIMOS MENU PRINCIPAL -->
<article id="cuerpo">
<article class="mapa">
<a href="#">Principal</a><div class="mapa_div"></div><a href="#">Administracion</a><div class="mapa_div"></div><a href="#">E. geografica</a><div class="mapa_div"></div><a class="current">Sedes</a>
</article>
<article id="contenido">
<form id="form" name="form" action="listar.php?<?php echo $filtro ?>" method="post"> <!-- FORMULARIO ENVIADO POR POST A LISTAR.PHP JUNTO CON EL FILTRO GENERAL  -->
<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
<legend class="ui-widget ui-widget-header ui-corner-all">Modificar sede</legend>
<p>
<label for="depto">Provincia:</label>  <!-- CAMPO DEPARTAMENTO -->
<select id="depto" name="depto" class="validate[required] text-input">
<?php
echo '<option value='.$row['seddepto'].'>'.$row['depnombre'].'</option>';  // DEPARTAMENTOS AGRUPADAS EN UN SELECT
?>
</select>
</p>
<p>
<label for="ciudad">Ciudad:</label> <!-- CAMPO CIUDAD -->
<select id="ciudad" name="ciudad" class="validate[required] text-input">
<?php
echo '<option value='.$row['sedciudad'].'>'.$row['ciunombre'].'</option>'; // CIUDADES AGRUPADAS EN UN SELECT 
?>
</select>
</p>
<p>
<input type="hidden" name="id" value="<?php echo $row['sedid'] ?>" /> <!-- INPUT OCULTA ALMACENA ID -->
</p>
<p>
<label for="nombre">Nombre:</label> <!-- CAMPO NOMBRE -->
<input type="text" name="nombre" class="nombre validate[required] text-input" value="<?php echo $row['sednombre'] ?>" title="Digite el nombre del departamento" />
</p>
<p class="boton">
<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href='listar.php?<?php echo $filtro ?>'">Atras</button>  <!--  BOTON PARA VOLVER ATRAS JUNTO CON FILTRO GENERAL-->
<button type="submit" class="btn btn-primary btnmodificar" name="modificar" value="modificar">Modificar</button> <!-- BOTON MODIFICAR -->
</p>
</fieldset>
</form>
</article>
</article>
<?php require($r.'incluir/src/pie.php') ?>  <!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
</section>
</body>
</html>