<?php
// LA ACCION PRINCIPAL ES ESTA PAGINA ES CONSULTAR LAS SEDES
$r = '../../../../';
//INCLUIR SESION Y CONECCION
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
?>
<!doctype html>
<html>

<head>

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<script>
		$(document).ready(function() { //VALIDACION DE CAMPOS REQUERIDOS EN FORMULARIO
			$("#depto").change(function(event) {
				var id1 = $("#depto").find(':selected').val();
				$("#ciudad").load('<?php echo $r ?>incluir/carga/ciudades.php?id1=' + id1);
			});
		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
		<?php require($r . 'incluir/src/menu.php') ?>
		<!-- INCLUIMOS MENU PRINCIPAL -->
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Administracion</a>
				<div class="mapa_div"></div><a href="#">E. geografica</a>
				<div class="mapa_div"></div><a class="current">Sedes</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="listar.php" method="post">
					<!-- ENVIO FORMULARIO POR POST A LISTAR.PHP -->
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">Consultar sedes</legend>
						<p>
							<label for="depto">Provincia:</label> <!-- MOSTRAMOS DEPARTAMENTOS DESDE LA BD  -->
							<select id="depto" name="depto">
								<option value="">TODOS</option>
								<?php
								$qry = $db->query("SELECT * FROM departamentos ORDER BY depnombre"); // QRY DEPARTAMENTOS
								while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
									echo '<option value=' . $row['depid'] . '>' . $row['depnombre'] . '</option>'; // ASOCIAMOS POR NOMBRE DE DEPARTAMENTO Y EL ID QUE LA IDENTIFICA
								}
								?>
							</select>
						</p>
						<p>
							<label for="ciudad">Ciudad:</label> <!-- CAMPO CIUDAD -->
							<select id="ciudad" name="ciudad">
								<option value="">TODOS</option>
							</select>
						</p>
						<p>
							<label for="nombre">Nombre:</label> <!-- CAMPO NOMBRE -->
							<input type="text" name="nombre" class="nombre" title="Digite el nombre de la ciudad" />
						</p>
						<div class="row">
							<div class="col-md-12 col-lg-6">
								<button type="submit" class="btnconsulta btn btn-primary btn-block mt-2" name="consultar" value="Buscar">consultar</button> <!-- BOTON CONSULTAR -->
							</div>
							<div class="col-md-12 col-lg-6">
								<button type="submit" class="btnconsulta btn btn-primary btn-block mt-2" name="show-all" value="Buscar">Mostrar Todo</button>							</div>
						</div>
					</fieldset>
				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
		<!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
	</section>
</body>

</html>