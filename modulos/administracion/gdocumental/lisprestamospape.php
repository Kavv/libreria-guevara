<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
require($r . 'incluir/funciones.php');

if (isset($_GET['validar'])) {
	$identificador = $_GET['identificador'];
	$inicio = $_GET['inicio'];
	$final = $_GET['final'];
	$comentarios = $_GET['comentarios'];

	if (empty($inicio)) {
		$inicio = 'NONE';
	}
	if (empty($final)) {
		$final = 'NONE';
	}


	$qry = $db->query("UPDATE solicitudespape SET solpapeesatdo='1', solpapeconsecutivoinicio='$inicio', solpapeconsecutivofinal='$final', solpapecomentarios='$comentarios' WHERE solpapeid='$identificador';");
	if ($qry) {
		$mensaje = "Se ha validado exitosamente el documento.";
		header('Location:lisprestamospape.php?mensaje=' . $mensaje . '&' . $filtro);
	} else {
		$error = "No se ha podido validar el documento por favor contactese con el administrador";
		header('Location:lisprestamospape.php?error=' . $error . '&' . $filtro);
	}
}

$sql = 'SELECT * FROM solicitudespape  INNER JOIN usuarios ON usuid = solpapeusuario LEFT JOIN empresas ON empid = solpapeempresa  LEFT JOIN pyatipodocu ON pyatdid = solpapetipodocu WHERE solpapeesatdo = 0 ORDER BY solpapefechahorausuario desc';


if (isset($_GET['mensaje']))
	$mensaje = $_GET['mensaje'];
if (isset($_GET['error']))
	$error = $_GET['error'];

$qry = $db->query($sql);
?>
<!doctype html>
<html>

<head>
	<meta charset="utf-8">
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>

	<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
	<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#tabla').dataTable({
				'bJQueryUI': true,
				'sPaginationType': 'full_numbers',
				'oLanguage': {
					'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
				},
				'bSort': false,
			});
			$('.pdf').click(function() {
				newSrc = $(this).attr('data-rel');
				$('#modal').html("<iframe src='" + newSrc + "' width='100%' height='100%'></iframe>");
				$('#modal').dialog({
					modal: true,
					width: '800',
					height: '900',
					title: 'PDF de la factura'
				});
			});
			$('.historico').click(function() {
				newHref = $(this).attr('data-rel');
				$('#modal').load(newHref).dialog({
					modal: true,
					width: 800
				});
			});
			$('.msj_validar').click(function(e) {
				e.preventDefault();
				var targetUrl = $(this).attr('href');
				var $dialog_link_follow_confirm = $('<div></div>').html("<p><span class='ui-icon ui-icon-alert' style='float:left; margin:2px 7px 7px 0;'></span>Esta seguro que desea validar la entrega?</p>").
				dialog({
					title: 'Porfavor confirmar',
					buttons: {
						'Si': function() {
							window.location.href = targetUrl;
						},
						'No': function() {
							$(this).dialog("close");
						}
					},
					modal: true,
					width: 'auto',
					height: 'auto'
				});
				$dialog_link_follow_confirm.dialog("open");
			});
		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a class="current">Papeleria y aseo</a>
			</article>
			<article id="contenido">
				<h2>Listado solicitudes de entrega </h2>
				<table id="tabla">
					<thead>
						<tr>
							<th>Id</th>
							<th>Fecha Creacion</th>
							<th>Usuario Creacion</th>
							<th>Empresa</th>
							<th>T. Documento</th>
							<th>Consecutivo Solicitado</th>
							<th>Cantidad</th>
							<th>Asignado</th>

							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php
						$i = 0;
						while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {

						?>
							<tr>
								<td><?php echo $row['solpapeid'] ?></td>
								<td><?php echo $row['solpapefechahorausuario'] ?></td>
								<td><?php echo $row['usunombre']  ?></td>
								<td><?php echo $row['empnombre']  ?></td>
								<td><?php echo $row['pyatdnombre']  ?></td>
								<td align="center"><?php echo $row['solpapeconsecutivo'] ?></td>
								<td align="center"><?php echo $row['solpapecantidad'] ?></td>
								<td align="center">
									<?php
										$consulta = $db->query("SELECT usunombre FROM usuarios WHERE usuid = '" . $row['solpapeasignado'] . "'");
										if($consulta->rowCount() > 0){
											$consulta = $consulta->fetch(PDO::FETCH_ASSOC);
											echo $consulta["usunombre"];
										}
									?>
								</td>


								<td align="center"><a href="validarpape.php?validar=1&identificador=<?php echo $row['solpapeid'] ?>" class="msj_validar" title="Validar"><img src="<?php echo $r ?>imagenes/iconos/thumb_up.png" /></a></td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<?php
	if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>';
	elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>';
	?>
	<div id="modal" style="display:none"></div>
	<div id="dialog2" style="display:none"></div>
</body>

</html>