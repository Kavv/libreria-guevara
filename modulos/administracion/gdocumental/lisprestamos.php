<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
require($r . 'incluir/funciones.php');
require($r . 'incluir/phpmailer/PHPMailer/class.phpmailer.php');
require($r . 'incluir/phpmailer/PHPMailer/class.smtp.php');
require($r . 'incluir/mail/send_email.php');

/* Quedo pendiente hacer dinamico el envio de este email */
if (isset($_GET['validar'])) {
	$identificador = $_GET['identificador'];

	$qrydocumen = $db->query("SELECT * FROM solicitudesdocu  INNER JOIN usuarios ON usuid = soldocusuario LEFT JOIN empresas ON empid = soldocempresa  LEFT JOIN gdtipodocumento ON tdid = soldoctipodocumento WHERE soldocid = " . $identificador . ";");
	$rowdocumen = $qrydocumen->fetch(PDO::FETCH_ASSOC);
	$tipodocumento = $rowdocumen['tdnombre'];
	$empresa = $rowdocumen['empnombre'];
	$empresa_id = $rowdocumen['empid'];
	$consecutivo = $rowdocumen['soldocconsecutivo'];
	$detalle = $rowdocumen['soldocdetalle'];
	$correo_usurio = $rowdocumen['usuemail'];

	$qry = $db->query("UPDATE solicitudesdocu SET soldocestado='1' WHERE soldocid='$identificador';");
	$num = $qry->rowCount();
	if ($num > 0) {
		$qryusuario = $db->query("SELECT * FROM usuarios  WHERE usuid = '" . $rowdocumen['soldocusuario'] . "';");
		$rowusuario = $qryusuario->fetch(PDO::FETCH_ASSOC);
	
		$qry_solicitado = $db->query("SELECT * FROM usuarios  WHERE usuid = '" . $_SESSION['id'] . "';");;
		$row_solicitado = $qry_solicitado->fetch(PDO::FETCH_ASSOC);
	
		$body = '
			<h2>Buen dia </h2>
			<h3>El presente con el fin de notificar que ya esta disponible el documento que usted solicito</h3>
			<p>
			<ul>
			<li>Empresa = ' . $empresa . '</li>
			<li>Identificacion empresa: = ' . $empresa_id . '</li>
			<li>Tipo de Documento = ' . $tipodocumento . '</li>
			<li>Consecutivo = ' . $consecutivo . '</li>
			<li>Detalle = ' . $detalle . '</li>
			</ul>
			</p>
			
			<p style="font-size:11px;">los signos de puntuacion y caracteres especiales han sido omitidos de manera predeterminada para evitar inconvenientes con los diferentes servidores de correo</p>
			
			';
		
		$from = $reply_to = $rowusuario["usuemail"];
		$name = $rowusuario['usunombre'];
		$subject = "Respuesta de solicitud de documento";
		$to = $row_solicitado["usuemail"];
		$cc = "";
		if (send_email($from, $name, $reply_to, $subject, $body, $to, $cc)) {
			$mensaje = "Se ha validado exitosamente el documento.";
			header('Location:lisprestamos.php?mensaje=' . $mensaje . '&' . $filtro);
		} else {
			$error = "No se pudo realizar el envio de la solicitud por: " . $mail->ErrorInfo;
			header('Location:lisprestamos.php?error=' . $error);
		}
	} else {
		$error = "No se ha podido validar el documento por favor contactese con el administrador";
		header('Location:lisprestamos.php?error=' . $error . '&' . $filtro);
	}
	exit();
}

$sql = 'SELECT * FROM solicitudesdocu  INNER JOIN usuarios ON usuid = soldocusuario LEFT JOIN empresas ON empid = soldocempresa  LEFT JOIN gdtipodocumento ON tdid = soldoctipodocumento WHERE soldocestado = 0 ORDER BY soldocfechahorausuario desc';

if (isset($_GET['error']))
	$error = $_GET['error'];
if (isset($_GET['mensaje']))
	$mensaje = $_GET['mensaje'];

$qry = $db->query($sql);
?>
<!doctype html>
<html>

<head>
	<meta charset="utf-8">
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>

	<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
	<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#tabla').dataTable({
				'bJQueryUI': true,
				'sPaginationType': 'full_numbers',
				'oLanguage': {
					'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
				},
				'bSort': false,
			});
			$('.pdf').click(function() {
				newSrc = $(this).attr('data-rel');
				$('#modal').html("<iframe src='" + newSrc + "' width='100%' height='100%'></iframe>");
				$('#modal').dialog({
					modal: true,
					width: '800',
					height: '900',
					title: 'PDF de la factura'
				});
			});
			$('.historico').click(function() {
				newHref = $(this).attr('data-rel');
				$('#modal').load(newHref).dialog({
					modal: true,
					width: 800
				});
			});
			$('.msj_validar').click(function(e) {
				e.preventDefault();
				var targetUrl = $(this).attr('href');
				var $dialog_link_follow_confirm = $('<div></div>').html("<p><span class='ui-icon ui-icon-alert' style='float:left; margin:2px 7px 7px 0;'></span>Esta seguro que desea validar el proceso?</p>").
				dialog({
					title: 'Porfavor confirmar',
					buttons: {
						'Si': function() {
							carga();
							window.location.href = targetUrl;
						},
						'No': function() {
							$(this).dialog("close");
						}
					},
					modal: true,
					width: 'auto',
					height: 'auto'
				});
				$dialog_link_follow_confirm.dialog("open");
			});
		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a class="current">Gestion Documental</a>
			</article>
			<article id="contenido">
				<h2>Listado Prestamos </h2>
				<table id="tabla">
					<thead>
						<tr>
							<th>Id</th>
							<th>Fecha Creacion</th>
							<th>Usuario Creacion</th>
							<th>Empresa</th>
							<th>T. Documento</th>
							<th>Consecutivo</th>
							<th>Asignado</th>
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php
						$i = 0;
						while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {

						?>
							<tr>
								<td><?php echo $row['soldocid'] ?></td>
								<td align="center"><?php echo $row['soldocfechahorausuario'] ?></td>
								<td align="center"><?php echo $row['usunombre']  ?></td>
								<td><?php echo $row['empnombre']  ?></td>
								<td align="center"><?php echo $row['tdnombre']  ?></td>
								<td align="center"><?php echo $row['soldocconsecutivo'] ?></td>
								<td align="center">
									<?php
										$consulta = $db->query("SELECT usunombre FROM usuarios WHERE usuid = '" . $row['soldocasignado'] . "'");
										if($consulta != null){
											$consulta = $consulta->fetch(PDO::FETCH_ASSOC);
											echo $consulta["usunombre"];
										}
									?>
								</td>


								<td align="center"><a href="lisprestamos.php?validar=1&identificador=<?php echo $row['soldocid'] ?>" class="msj_validar" title="Validar"><img src="<?php echo $r ?>imagenes/iconos/thumb_up.png" /></a></td>
								<td align="center"><a href="insertar.php?prestamos=1&soldoc=<?php echo $row['soldocid']?> " title="Insertar"><img src="<?php echo $r ?>imagenes/iconos/page_add.png" /></a></td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<?php
	if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>';
	elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>';
	?>
	<div id="modal" style="display:none"></div>
	<div id="dialog2" style="display:none"></div>
</body>

</html>