<?php

$r = '../../../';
//INCLUIR SESION Y CONECCION
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');


$redireccion = 'Location:insertar.php?';
$solicitud = $sol_empresa = $sol_consecutivo = $sol_tipo = "";
//En caso de ser una solicitud de documento (No se porque las variables estan orientadas a prestamos)
if(isset($_GET['prestamos']))
{
	if($_GET['prestamos'] == 1)
	{
		//Es necesaro cambiar la redireccion y asignar los valores correspondientes segun la solicitud
		$redireccion = 'Location:lisprestamos.php?';
		$solicitud = $db->query("SELECT * FROM solicitudesdocu WHERE soldocid = '" .$_GET['soldoc']. "'");
		if($solicitud != null)
		{
			$solicitud = $solicitud->fetch(PDO::FETCH_ASSOC);
			$sol_empresa = $solicitud["soldocempresa"];
			$sol_consecutivo = $solicitud["soldocconsecutivo"];
			$sol_tipo = $solicitud["soldoctipodocumento"];
		} 
	}
}

if (isset($_POST['insertar'])) { // SE VALIDA SI LA INFORMACION VIENE DE INSERTAR 

	$empresa = $_POST['empresa'];
	$tdocumento = $_POST['tdocumento'];
	$consecutivo = $_POST['consecutivo'];
	$caja = $_POST['caja'];
	$carpeta = $_POST['carpeta'];
	$imagen = $_POST['imagen'];
	$prestamos = $_POST['prestamos'];
	//Es necesaro cambiar la redireccion y asignar los valores correspondientes segun la solicitud
	if($prestamos == 1)
		$redireccion = 'Location:lisprestamos.php?';



	$qry = $db->query("INSERT INTO gestiondocumental (gdempresa, gdtipodocumento, gdconsecutivo, gdcaja, gdcarpeta, gdusuario) VALUES ('$empresa', '$tdocumento', '$consecutivo', '$caja', '$carpeta', '" . $_SESSION['id'] . "');	");
	if ($qry) 
	{
		$mensaje = 'Se ha ingresado correctamente';
		$new_id =  $db->lastInsertId();
		if ($_FILES['imagen']['name']) {
			if ($_FILES['imagen']['size'] < 1600000) {
				$ruta = $r . 'pdf/gdocumental/' . $empresa . '_' . $tdocumento . '_' . $new_id . '.pdf';
				if (move_uploaded_file($_FILES['imagen']['tmp_name'], $ruta)) {
					$mensaje .= ' junto con el PDF.'; 
				}
				else 
				{
					$error = 'Se guardo el registro <br/> Pero no se pudo cargar el PDF intente nuevamente o contacte con el encargado';
					header($redireccion.'error=' . $error);
					exit();
				}
			} else {
				$error = 'El PDF excede el tamano permitido (max. 1.5MB)';
				header($redireccion.'error=' . $error);
				exit();
			}
		}

		header($redireccion.'mensaje=' . $mensaje);
		exit();
	} 
	else 
	{
		$error = 'Error al insertar, intenta nuevamente o contacte con el encargado';
		header($redireccion.'error=' . $error);
		exit();
	}
}

if (isset($_GET['mensaje']))
	$mensaje = $_GET['mensaje'];

if (isset($_GET['error']))
	$error = $_GET['error'];
?>
<!doctype html>
<html>

<head>

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<script type="text/javascript" src="<?php echo $r ?>incluir/funciones.js"></script>
	<script>
		$(document).ready(function() {

			$('#viaje').datepicker({
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
				minDate: '+0D',
				maxDate: '+50D',
			}).keypress(function(event) {
				event.preventDefault()
			});
			$('#regreso').datepicker({
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
				minDate: '+0D',
				maxDate: '+50D',
			}).keypress(function(event) {
				event.preventDefault()
			});
		});
	</script>

</head>

<body>
	<?php require($r . 'incluir/src/login.php');   ?>
	<!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
		<?php require($r . 'incluir/src/menu.php') ?>
		<!-- INCLUIMOS MENU PRINCIPAL -->
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Administracion</a>
				<div class="mapa_div"></div><a class="current">Gestion Documental</a>
			</article>
			<article id="contenido">
				<div class="ui-widget">
					<form id="form" name="form" action="insertar.php" method="post" enctype="multipart/form-data">
						<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
							<legend class="ui-widget ui-widget-header ui-corner-all">Insertar Gestion Documental</legend>

							<?php if (isset($_GET['prestamos'])) {
								echo "<input type='hidden' name='prestamos' value='1'/>";
							} else {
								echo "<input type='hidden' name='prestamos' value='NONE'/>";
							} ?>
							<p>
								<label for="empresa">Empresa:</label> <!-- CAMPO EMPRESA -->
								<select name="empresa" class="validate[required]">
									<option value="">SELECCIONE</option>
									<?php
									$qry = $db->query("SELECT * FROM empresas ORDER BY empnombre");
									while ($row = $qry->fetch(PDO::FETCH_ASSOC))
										if($sol_empresa != $row['empid'] )
											echo '<option value=' . $row['empid'] . '>' . $row['empnombre'] . '</option>';
										else
											echo '<option value=' . $row['empid'] . ' selected>' . $row['empnombre'] . '</option>';
									?>
								</select>
							</p>

							<p>
								<label for="tdocumento">T. Documento:</label> <!-- CAMPO T DOCUMENTO -->
								<select name="tdocumento" class="validate[required]">
									<option value="">SELECCIONE</option>
									<?php
									$qry = $db->query("SELECT * FROM gdtipodocumento ORDER BY tdnombre");
									while ($row = $qry->fetch(PDO::FETCH_ASSOC))
										if($sol_tipo != $row['tdid'] )
											echo '<option value=' . $row['tdid'] . '>' . $row['tdnombre'] . '</option>';
										else
											echo '<option value=' . $row['tdid'] . ' selected>' . $row['tdnombre'] . '</option>';
									?>
								</select>
							</p>

							<p>
								<label for="consecutivo">Consecutivo:</label> <!-- CAMPO CONSECUTIVO -->
								<input type="text" name="consecutivo" class="validate[required, custom[onlyNumberSp]] text-input" value="<?php echo $sol_consecutivo?>"/>
							</p>

							<p>
								<label for="caja">Caja:</label> <!-- CAMPO CAJA -->
								<input type="text" name="caja" class="validate[required, custom[onlyNumberSp]] text-input" />
							</p>

							<p>
								<label for="carpeta">Carpeta:</label> <!-- CAMPO CARPETA-->
								<input type="text" name="carpeta" class="validate[required, custom[onlyNumberSp]] text-input" />
							</p>


							<p>
								<label>PDF (max. 1.5 Mb): </label><input type="file" name="imagen" class="validate[checkFileType[pdf|PDF]]" />
							</p>

							<p class="boton">
								<button type="submit" class="btn btn-primary btninsertar" name="insertar" value="insertar">Insertar</button> <!-- BOTON INSERTAR -->
							</p>
						</fieldset>
					</form>
				</div>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<?php
	if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>'; //VENTANA MODAL ERROR
	elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>'; //VENTANA MODAL EXITOSO
	?>
	<div id="modal" style="display:none"></div>
	<div id="dialog2" title="Historico" style="display:none"></div>
</body>

</html>