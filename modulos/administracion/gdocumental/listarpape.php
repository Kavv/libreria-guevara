<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
require($r . 'incluir/funciones.php');

$tipo = $empresa = $tipodocu = $entregado = $fecha1 = $fecha2 = "";
if (isset($_POST['consultar'])) {
	$tipo = $_POST['tipo'];
	$empresa = $_POST['empresa'];
	$tipodocu = $_POST['tipodocu'];
	$entregado = $_POST['entregado'];
	$fecha1 = $_POST['fecha1'];
	$fecha2 = $_POST['fecha2'];
} else if (isset($_GET['tipo'])) {
	$tipo = $_GET['tipo'];
	$empresa = $_GET['empresa'];
	$tipodocu = $_GET['tipodocu'];
	$entregado = $_GET['entregado'];
	$fecha1 = $_GET['fecha1'];
	$fecha2 = $_GET['fecha2'];
}

if (isset($_GET['validar'])) {
	$identificador = $_GET['identificador'];
	$usuario = $_GET['usuario'];
	$tipo = $_GET['tipo'];
	$empresa = $_GET['empresa'];
	$tipodocu = $_GET['tipodocu'];
	$entregado = $_GET['entregado'];
	$fecha1 = $_GET['fecha1'];
	$fecha2 = $_GET['fecha2'];
	$filtro = 'tipo=' . $tipo . '&empresa=' . $empresa . '&tipodocu=' . $tipodocu . '&entregado=' . $entregado . '&fecha1=' . $fecha1 . '&fecha2=' . $fecha2;

	$qry = $db->query("UPDATE papeyaseo SET pyaentregado='$usuario', pyafechaentrega='" . date("Y-m-d H:i:s") . "', pyaestado='ENTREGADO' WHERE pyaid='$identificador';");
	
	if ($qry) {
		$mensaje = "Se ha actulizado corractamente.";
		header('Location:listarpape.php?mensaje=' . $mensaje . '&' . $filtro);
	} else {
		$error = "No se ha podido actualizar por favor contactese con el administrador";
		header('Location:listarpape.php?error=' . $error . '&' . $filtro);
	}
}


$filtro = 'tipo=' . $tipo . '&empresa=' . $empresa . '&tipodocu=' . $tipodocu . '&entregado=' . $entregado . '&fecha1=' . $fecha1 . '&fecha2=' . $fecha2;
$con = 'SELECT * FROM papeyaseo LEFT JOIN empresas ON empid = pyaempresa  LEFT JOIN usuarios ON usuid = pyaentregado INNER JOIN pyatipodocu ON pyatipodocu = pyatdid ';
$ord = 'ORDER BY pyaid desc';

if ($tipo == 'unidad') {
	$tipodetalle = " pyaunidad = '1' ";
} elseif ($tipo == 'consecutivo') {
	$tipodetalle = " pyaconsecutivo = '1' ";
}

if ($tipo == '' && $empresa == '' && $tipodocu == '' && $entregado == '' && $fecha1 == '') $sql = "$con  $ord";
elseif ($tipo != '' && $empresa == '' && $tipodocu == '' && $entregado == '' && $fecha1 == '') $sql = "$con WHERE  $tipodetalle $ord";
elseif ($tipo == '' && $empresa != '' && $tipodocu == '' && $entregado == '' && $fecha1 == '') $sql = "$con WHERE  pyaempresa = '$empresa' $ord";
elseif ($tipo == '' && $empresa == '' && $tipodocu != '' && $entregado == '' && $fecha1 == '') $sql = "$con WHERE  pyatipodocu = '$tipodocu' $ord";
elseif ($tipo == '' && $empresa == '' && $tipodocu == '' && $entregado != '' && $fecha1 == '') $sql = "$con WHERE  pyaentregado = '$entregado' $ord";
elseif ($tipo != '' && $empresa != '' && $tipodocu == '' && $entregado == '' && $fecha1 == '') $sql = "$con WHERE  $tipodetalle AND pyaempresa = '$empresa' $ord";
elseif ($tipo != '' && $empresa != '' && $tipodocu != '' && $entregado == '' && $fecha1 == '') $sql = "$con WHERE  $tipodetalle AND pyaempresa = '$empresa' AND pyatipodocu = '$tipodocu' $ord";
elseif ($tipo != '' && $empresa != '' && $tipodocu != '' && $entregado != '' && $fecha1 == '') $sql = "$con WHERE  $tipodetalle AND pyaempresa = '$empresa' AND pyatipodocu = '$tipodocu' pyaentregado = '$entregado' $ord";
elseif ($tipo == '' && $empresa != '' && $tipodocu != '' && $entregado == '' && $fecha1 == '') $sql = "$con WHERE   pyaempresa = '$empresa' AND pyatipodocu = '$tipodocu' $ord";
elseif ($tipo != '' && $empresa == '' && $tipodocu != '' && $entregado == '' && $fecha1 == '') $sql = "$con WHERE  $tipodetalle  AND pyatipodocu = '$tipodocu' $ord";
elseif ($tipo != '' && $empresa == '' && $tipodocu == '' && $entregado != '' && $fecha1 == '') $sql = "$con WHERE  $tipodetalle AND pyaentregado = '$entregado' $ord";
elseif ($tipo == '' && $empresa == '' && $tipodocu != '' && $entregado != '' && $fecha1 == '') $sql = "$con WHERE  pyatipodocu = '$tipodocu' AND pyaentregado = '$entregado' $ord";
elseif ($tipo == '' && $empresa != '' && $tipodocu == '' && $entregado != '' && $fecha1 == '') $sql = "$con WHERE  pyaempresa = '$empresa' AND pyaentregado = '$entregado' $ord";

elseif ($tipo != '' && $empresa == '' && $tipodocu == '' && $entregado == '' && $fecha1 != '') $sql = "$con WHERE  $tipodetalle AND DATE(pyafechaentrega) BETWEEN '$fecha1' AND '$fecha2' $ord";
elseif ($tipo == '' && $empresa != '' && $tipodocu == '' && $entregado == '' && $fecha1 != '') $sql = "$con WHERE  pyaempresa = '$empresa' AND DATE(pyafechaentrega) BETWEEN '$fecha1' AND '$fecha2' $ord";
elseif ($tipo == '' && $empresa == '' && $tipodocu != '' && $entregado == '' && $fecha1 != '') $sql = "$con WHERE  pyatipodocu = '$tipodocu' AND DATE(pyafechaentrega) BETWEEN '$fecha1' AND '$fecha2' $ord";
elseif ($tipo == '' && $empresa == '' && $tipodocu == '' && $entregado != '' && $fecha1 != '') $sql = "$con WHERE  pyaentregado = '$entregado' AND DATE(pyafechaentrega) BETWEEN '$fecha1' AND '$fecha2' $ord";
elseif ($tipo != '' && $empresa != '' && $tipodocu == '' && $entregado == '' && $fecha1 != '') $sql = "$con WHERE  $tipodetalle AND pyaempresa = '$empresa' AND DATE(pyafechaentrega) BETWEEN '$fecha1' AND '$fecha2' $ord";
elseif ($tipo != '' && $empresa != '' && $tipodocu != '' && $entregado == '' && $fecha1 != '') $sql = "$con WHERE  $tipodetalle AND pyaempresa = '$empresa' AND pyatipodocu = '$tipodocu' AND DATE(pyafechaentrega) BETWEEN '$fecha1' AND '$fecha2' $ord";
elseif ($tipo != '' && $empresa != '' && $tipodocu != '' && $entregado != '' && $fecha1 != '') $sql = "$con WHERE  $tipodetalle AND pyaempresa = '$empresa' AND pyatipodocu = '$tipodocu' pyaentregado = '$entregado' AND DATE(pyafechaentrega) BETWEEN '$fecha1' AND '$fecha2' $ord";
elseif ($tipo == '' && $empresa != '' && $tipodocu != '' && $entregado == '' && $fecha1 != '') $sql = "$con WHERE   pyaempresa = '$empresa' AND pyatipodocu = '$tipodocu' AND DATE(pyafechaentrega) BETWEEN '$fecha1' AND '$fecha2' $ord";
elseif ($tipo != '' && $empresa == '' && $tipodocu != '' && $entregado == '' && $fecha1 != '') $sql = "$con WHERE  $tipodetalle  AND pyatipodocu = '$tipodocu' AND DATE(pyafechaentrega) BETWEEN '$fecha1' AND '$fecha2' $ord";
elseif ($tipo != '' && $empresa == '' && $tipodocu == '' && $entregado != '' && $fecha1 != '') $sql = "$con WHERE  $tipodetalle AND pyaentregado = '$entregado' AND DATE(pyafechaentrega) BETWEEN '$fecha1' AND '$fecha2' $ord";
elseif ($tipo == '' && $empresa == '' && $tipodocu != '' && $entregado != '' && $fecha1 != '') $sql = "$con WHERE  pyatipodocu = '$tipodocu' AND pyaentregado = '$entregado' AND DATE(pyafechaentrega) BETWEEN '$fecha1' AND '$fecha2' $ord";
elseif ($tipo == '' && $empresa != '' && $tipodocu == '' && $entregado != '' && $fecha1 != '') $sql = "$con WHERE  pyaempresa = '$empresa' AND pyaentregado = '$entregado' AND DATE(pyafechaentrega) BETWEEN '$fecha1' AND '$fecha2' $ord";


if (isset($_GET['mensaje']))
	$mensaje = $_GET['mensaje'];
if (isset($_GET['error']))
	$error = $_GET['error'];

$qry = $db->query($sql);
?>
<!doctype html>
<html>

<head>
	<meta charset="utf-8">
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>

	<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
	<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#tabla').dataTable({
				'bJQueryUI': true,
				'sPaginationType': 'full_numbers',
				'oLanguage': {
					'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
				},
				'bSort': false,
			});

			$('.msj_validar').click(function(e) {
				e.preventDefault();
				var targetUrl = $(this).attr('href');
				var $dialog_link_follow_confirm = $('<div></div>').html("<p><span class='ui-icon ui-icon-alert' style='float:left; margin:2px 7px 7px 0;'></span>Esta seguro que desea VALIDAR la entrega?</p>").
				dialog({
					title: 'Porfavor confirmar',
					buttons: {
						'Si': function() {
							window.location.href = targetUrl;
						},
						'No': function() {
							$(this).dialog("close");
						}
					},
					modal: true,
					width: 'auto',
					height: 'auto'
				});
				$dialog_link_follow_confirm.dialog("open");
			});
		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a class="current">Papeleria y aseo</a>
			</article>
			<article id="contenido">
				<h2>Listado Papeleria y aseo </h2>
				<table id="tabla">
					<thead>
						<tr>
							<th>Id</th>
							<th>Fecha Creacion</th>
							<th>Empresa</th>
							<th>T. Documento</th>
							<th>Factura</th>
							<th>Tipo</th>
							<th>Inicio</th>
							<th>Final</th>
							<th>Cantidad</th>
							<th>Entregado a</th>
							<th>Fecha de Entrega</th>
							<th>Estado</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php
						$i = 0;
						while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {

						?>
							<tr>
								<td><?php echo $row['pyaid'] ?></td>
								<td align="center"><?php echo $row['pyafechausuarioingreso'] ?></td>
								<td><?php echo $row['empnombre']; ?></td>
								<td align="center"><?php echo $row['pyatdnombre']  ?></td>
								<td align="center"><?php echo $row['pyafactura']  ?></td>
								<td align="center">
									<?php 
										if ($row['pyaunidad'] == 1) {
											echo "UNIDAD";
										} elseif ($row['pyaconsecutivo'] == 1) {
											echo "CONSECUTIVO";
										} 
									?>
								</td>
								<td align="center"><?php echo $row['pyainicio'] ?></td>
								<td align="center"><?php echo $row['pyafinal'] ?></td>
								<td align="center"><?php echo $row['pyacantidad'] ?></td>
								<td><?php echo $row['usunombre'] ?></td>
								<td align="center"><?php echo $row['pyafechaentrega'] ?></td>
								<td align="center"><?php echo $row['pyaestado'] ?></td>
								<?php if ($row['usunombre'] == '') { ?>
									<td align="center"><a href="validar.php?validar=1&identificador=<?php echo $row['pyaid'] . "&" . $filtro ?>" class="msj_validar" title="Validar"><img src="<?php echo $r ?>imagenes/iconos/thumb_up.png" /></a></td>
								<?php } else { ?>
									<td align="center"><img class="gray" src="<?php echo $r ?>imagenes/iconos/thumb_up.png" /></td>
								<?php } ?>
							</tr>
						<?php } ?>
					</tbody>
				</table>
				<p class="boton">
					<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href='consultarpape.php'">Atras</button>
				</p>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<?php
	if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>';
	elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>';
	?>
	<div id="modal" style="display:none"></div>
	<div id="dialog2" style="display:none"></div>
</body>

</html>