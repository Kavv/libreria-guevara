<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
require($r . 'incluir/funciones.php');

$empresa = $tdocumento = $consecutivo = "";
if (isset($_POST['consultar'])) {
	$empresa = $_POST['empresa'];
	$tdocumento = $_POST['tdocumento'];
	$consecutivo = $_POST['consecutivo'];
} 
if (isset($_GET['empresa'])) {
	$empresa = $_GET['empresa'];
	$tdocumento = $_GET['tdocumento'];
	$consecutivo = $_GET['consecutivo'];
}

if (isset($_GET['eliminar'])) {

	$identificador = $_GET['identificador'];
	$qryiden = $db->query("SELECT * FROM gestiondocumental WHERE gdid = " . $identificador);
	$rowiden = $qryiden->fetch(PDO::FETCH_ASSOC);
	$empresadet = $rowiden['gdempresa'];
	$tdocumento = $rowiden['gdtipodocumento'];

	$fichero = $r . 'pdf/gdocumental/' . $empresadet . '_' . $tdocumento . '_' . $identificador . '.pdf';
	$papelera = $r . 'pdf/gdocumental/papelera/' . $empresadet . '_' . $tdocumento . '_' . $identificador . '_' . date('d-m-Y_H-i', time()) . '.pdf';
	

	$qry = $db->query("DELETE FROM gestiondocumental WHERE gdid='$identificador';");
	if ($qry) {
		if (file_exists($fichero)) 
			rename($fichero, $papelera);

		$mensaje = "Se ha eliminado exitosamente el registro.";
		header('Location:listar.php?mensaje=' . $mensaje . '&' . $filtro);
	} else {
		$error = "No se ha podido eliminar el documento por favor contactese con el administrador";
		header('Location:listar.php?error=' . $error . '&' . $filtro);
	}
}

if (isset($_POST['modificar'])) {
	$identificador = $_POST['identificador'];
	$empresa = $_POST['empresa'];
	$tdocumento = $_POST['tdocumento'];
	$consecutivo = $_POST['consecutivo'];
	$caja = $_POST['caja'];
	$carpeta = $_POST['carpeta'];

	$qryiden = $db->query("SELECT * FROM gestiondocumental WHERE gdid = " . $identificador);
	$rowiden = $qryiden->fetch(PDO::FETCH_ASSOC);
	$empresadet = $rowiden['gdempresa'];
	$tipo_documento = $rowiden['gdtipodocumento'];
	
	
	$nombre_fichero_viejo = $r . 'pdf/gdocumental/' . $empresadet . '_' . $tipo_documento . '_' . $identificador . '.pdf';
	$nombre_fichero_nuevo = $r . 'pdf/gdocumental/' . $empresa . '_' . $tdocumento . '_' . $identificador . '.pdf';
	if (file_exists($nombre_fichero_viejo)) 
	{
		if($nombre_fichero_nuevo != $nombre_fichero_viejo)
		rename($nombre_fichero_viejo, $nombre_fichero_nuevo);
	}

	$qry = $db->query("UPDATE gestiondocumental SET gdempresa='$empresa', gdtipodocumento='$tdocumento', gdconsecutivo='$consecutivo', gdcaja='$caja', gdcarpeta='$carpeta' WHERE gdid='$identificador';");
	if ($qry) {
		$mensaje = "Se ha actualizado correctamente, junto con su respectivo PDF.";
		header('Location:listar.php?mensaje=' . $mensaje . '&' . $filtro);
	} else {
		$error = "No se ha podido actulizar intente nuevamente o contacte con el administrador";
		header('Location:listar.php?error=' . $error . '&' . $filtro);
	}
}

$filtro = 'empresa=' . $empresa . '&tdocumento=' . $tdocumento . '&consecutivo=' . $consecutivo;
$con = 'SELECT * FROM gestiondocumental INNER JOIN empresas ON empid = gdempresa  INNER JOIN usuarios ON usuid = gdusuario INNER JOIN gdtipodocumento ON tdid = gdtipodocumento ';
$ord = 'ORDER BY gdid desc';
if ($empresa == '' && $tdocumento == '' && $consecutivo == '') $sql = "$con  $ord";
elseif ($empresa != '' && $tdocumento == '' && $consecutivo == '') $sql = "$con WHERE gdempresa ='$empresa'  $ord";
elseif ($empresa == '' && $tdocumento != '' && $consecutivo == '') $sql = "$con WHERE gdtipodocumento ='$tdocumento' $ord";
elseif ($empresa == '' && $tdocumento == '' && $consecutivo != '') $sql = "$con WHERE gdconsecutivo ='$consecutivo' $ord";
elseif ($empresa != '' && $tdocumento != '' && $consecutivo == '') $sql = "$con WHERE gdtipodocumento ='$tdocumento' AND gdempresa ='$empresa' $ord";
elseif ($empresa == '' && $tdocumento != '' && $consecutivo != '') $sql = "$con WHERE gdtipodocumento ='$tdocumento' AND gdconsecutivo ='$consecutivo' $ord";
elseif ($empresa != '' && $tdocumento == '' && $consecutivo != '') $sql = "$con WHERE gdempresa ='$empresa' AND gdconsecutivo ='$consecutivo' $ord";
elseif ($empresa != '' && $tdocumento != '' && $consecutivo != '') $sql = "$con WHERE gdempresa ='$empresa' AND gdconsecutivo ='$consecutivo' AND gdtipodocumento ='$tdocumento' $ord";


if (isset($_GET['mensaje']))
	$mensaje = $_GET['mensaje'];

if (isset($_GET['error']))
	$error = $_GET['error'];

$qry = $db->query($sql);
?>
<!doctype html>
<html>

<head>
	<meta charset="utf-8">
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>

	<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
	<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#tabla').dataTable({
				'bJQueryUI': true,
				'sPaginationType': 'full_numbers',
				'oLanguage': {
					'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
				},
				'bSort': false,
			});
			$('.pdf').click(function() {
				newSrc = $(this).attr('data-rel');
				$('#modal').html("<iframe src='" + newSrc + "' width='100%' height='100%'></iframe>");
				$('#modal').dialog({
					modal: true,
					width: '800',
					height: '900',
					title: 'PDF de la factura'
				});
			});
			$('.historico').click(function() {
				newHref = $(this).attr('data-rel');
				$('#modal').load(newHref).dialog({
					modal: true,
					width: 800
				});
			});
			$('.msj_eliminar').click(function(e) {
				e.preventDefault();
				var targetUrl = $(this).attr('href');
				var $dialog_link_follow_confirm = $('<div></div>').html("<p><span class='ui-icon ui-icon-alert' style='float:left; margin:2px 7px 7px 0;'></span>Esta seguro que desea eliminar el Documento?</p>").
				dialog({
					title: 'Porfavor confirmar',
					buttons: {
						'Si': function() {
							window.location.href = targetUrl;
						},
						'No': function() {
							$(this).dialog("close");
						}
					},
					modal: true,
					width: 'auto',
					height: 'auto'
				});
				$dialog_link_follow_confirm.dialog("open");
			});
		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php'); ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a class="current">Gestion Documental</a>
			</article>
			<article id="contenido">
				<h2>Listado Documentos <?php $sql ?></h2>
				<table id="tabla">
					<thead>
						<tr>
							<th>Id</th>
							<th>Fecha Creacion</th>
							<th>Usuario Creacion</th>
							<th>Empresa</th>
							<th>T. Documento</th>
							<th>Consecutivo</th>
							<th>Caja</th>
							<th>Carpeta</th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>

						</tr>
					</thead>
					<tbody>
						<?php
						$i = 0;
						while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
							$id = $row['gdid'];
						?>
							<tr>
								<td><?php echo $row['gdid'] ?></td>
								<td align="center"><?php echo $row['gdfechahorausuario'] ?></td>
								<td align="center"><?php echo $row['usunombre']  ?></td>
								<td><?php echo $row['empnombre']  ?></td>
								<td align="center"><?php echo $row['tdnombre']  ?></td>
								<td align="center"><?php echo $row['gdconsecutivo'] ?></td>
								<td align="center"><?php echo $row['gdcaja'] ?></td>
								<td align="center"><?php echo $row['gdcarpeta'] ?></td>

								<td align="center"><img style="cursor:pointer;" src="<?php echo $r ?>imagenes/iconos/comments.png" class="historico" data-rel="notas.php?<?php echo 'id=' . $id ?>" title="Historia" /></td>

								<td align="center"><img src="<?php echo $r ?>imagenes/iconos/pdf.png" class="pdf" data-rel="<?php echo $r . 'pdf/gdocumental/' . $row['empid'] . '_' . $row['gdtipodocumento'] . '_' . $row['gdid'] . '.pdf' ?>" title="Comprobante" /></td>

								<td align="center"><a href="anexpdf.php?identificador=<?php echo $row['gdid'] . "&" . $filtro ?>" title="Anexar PDF"><img src="<?php echo $r ?>imagenes/iconos/pdf_add.png" /></a></td>

								<td align="center"><a href="observaciones.php?identificador=<?php echo $row['gdid'] . "&" . $filtro ?>" title="observaciones"><img src="<?php echo $r ?>imagenes/iconos/comment.png" /></a></td>

								<td align="center"><a href="listar.php?eliminar=1&identificador=<?php echo $row['gdid'] . "&" . $filtro ?>" class="msj_eliminar" title="Eliminar"><img src="<?php echo $r ?>imagenes/iconos/cancelar.png" /></a></td>

								<td align="center"><a href="modificar.php?identificador=<?php echo $row['gdid'] . "&" . $filtro ?>" title="Modificar"><img src="<?php echo $r ?>imagenes/iconos/lapiz.png" /></a></td>

							</tr>
						<?php } ?>
					</tbody>
				</table>
				<p class="boton">
					<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href='consultar.php'">Atras</button>
				</p>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<?php
	if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>';
	elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>';
	?>
	<div id="modal" title="Historia" style="display:none"></div>
	<div id="dialog2" title="Comentarios" style="display:none"></div>
</body>

</html>