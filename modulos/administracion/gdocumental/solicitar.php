<?php
$r = '../../../';
//INCLUIR SESION Y CONECCION
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
require($r . 'incluir/phpmailer/PHPMailer/class.phpmailer.php');
require($r . 'incluir/phpmailer/PHPMailer/class.smtp.php');
require($r . 'incluir/mail/send_email.php');

if (isset($_POST['solicitar'])) { // SE VALIDA SI LA INFORMACION 

	$empresa = $_POST['empresa'];
	$tdocumento = $_POST['tdocumento'];
	$consecutivo = $_POST['consecutivo'];
	$detalle = $_POST['detalle'];
	$asignado_id = $_POST['asignado'];

	if (empty($detalle)) {
		$detalle = 'NONE';
	}
	if (empty($consecutivo)) {
		$consecutivo = "";
	}

	$qryusuario = $db->query("SELECT * FROM usuarios  WHERE usuid = '" . $_SESSION['id'] . "';");
	$rowusuario = $qryusuario->fetch(PDO::FETCH_ASSOC);

	$qry_asignado = $db->query("SELECT * FROM usuarios  WHERE usuid = '" . $asignado_id . "';");
	$row_asignado = $qry_asignado->fetch(PDO::FETCH_ASSOC);


	if (!empty($tdocumento)) {
		$qrydocumen = $db->query("SELECT * FROM gdtipodocumento  WHERE tdid = " . $tdocumento . ";");
		$rowdocumen = $qrydocumen->fetch(PDO::FETCH_ASSOC);
		$tipodocumento = $rowdocumen['tdnombre'];
	} else {
		$tipodocumento = "";
	}

	$qry = $db->query("INSERT INTO solicitudesdocu (soldocempresa, soldocconsecutivo, soldoctipodocumento, soldocdetalle, soldocusuario, soldocasignado) VALUES ('$empresa', '$consecutivo', '$tdocumento', '$detalle', '" . $_SESSION['id'] . "', '$asignado_id');");
	if ($qry) 
	{
		
		$body = '
			<h2>Buen dia acontinuacion se detalla la solicitud de documento realizada por</h2>
			<h3>' . $rowusuario['usunombre'] . '</h3>
			<p>
			<ul>
			<li>Empresa: ' . $empresa . '</li>
			<li>Tipo de Documento: ' . $tipodocumento . '</li>
			<li>Consecutivo: ' . $consecutivo . '</li>
			<li>Solicitud asignada a: ' . $row_asignado["usunombre"] . '</li>
			<li>Detalle: ' . $detalle . '</li>
			</ul>
			</p>
			
			<p style="font-size:11px;">los signos de puntuacion y caracteres especiales han sido omitidos de manera predeterminada para evitar inconvenientes con los diferentes servidores de correo</p>
			
			';
	
		$from = $reply_to = $rowusuario["usuemail"];
		$name = $rowusuario['usunombre'];
		$subject = "Solicitud de documento";
		$to = $row_asignado["usuemail"];
		$cc = "";
		if (send_email($from, $name, $reply_to, $subject, $body, $to, $cc)) {
			$mensaje = 'Se ha realizado la solicitud de manera correcta, se le dara respuesta pronto.';
			header('Location:solicitar.php?mensaje=' . $mensaje);
		} 
		else
		{
			$error = "Se ha guardado el registro <br/> Pero no se pudo enviar el correo de la solicitud por: " . $mail->ErrorInfo;
			header('Location:solicitar.php?error=' . $error);
		}
	} else {
		$error = 'Error al realizar la solicitud intente nuevamente o contacte con el encargado';
		header('Location:solicitar.php?error=' . $error);
		exit();
	}

}

if (isset($_GET['error']))
	$error = $_GET['error'];
if (isset($_GET['mensaje']))
	$mensaje = $_GET['mensaje'];
?>
<!doctype html>
<html>

<head>

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<script type="text/javascript" src="<?php echo $r ?>incluir/funciones.js"></script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
		<?php require($r . 'incluir/src/menu.php') ?>
		<!-- INCLUIMOS MENU PRINCIPAL -->
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Administracion</a>
				<div class="mapa_div"></div><a class="current">Gestion Documental</a>
			</article>
			<article id="contenido">
				<div class="ui-widget">
					<form id="form" name="form" action="solicitar.php" method="post">
						<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
							<legend class="ui-widget ui-widget-header ui-corner-all">Solicitud de documentos</legend>

							<p>
								<label for="empresa">Empresa:</label> <!-- CAMPO EMPRESA -->
								<select name="empresa" class="validate[required]">
									<option value="">SELECCIONE</option>
									<?php
									$qry = $db->query("SELECT * FROM empresas ORDER BY empnombre");
									while ($row = $qry->fetch(PDO::FETCH_ASSOC))
										echo '<option value=' . $row['empid'] . '>' . $row['empnombre'] . '</option>';
									?>
								</select>
							</p>

							<p>
								<label for="tdocumento">T. Docuemnto:</label> <!-- CAMPO T DOCUMENTO -->
								<select name="tdocumento" class="validate[required]">
									<option value="">SELECCIONE</option>
									<?php
									$qry = $db->query("SELECT * FROM gdtipodocumento ORDER BY tdnombre");
									while ($row = $qry->fetch(PDO::FETCH_ASSOC))
										echo '<option value=' . $row['tdid'] . '>' . $row['tdnombre'] . '</option>';
									?>
								</select>
							</p>

							<p>
								<label for="consecutivo">Consecutivo:</label> <!-- CAMPO CONSECUTIVO -->
								<input type="text" name="consecutivo" class="validate[custom[onlyNumberSp]] text-input" />
							</p>
							<p>
								<label for="asignado">Asignar al usuario:</label> <!-- CAMPO T DOCUMENTO -->
								<select name="asignado" class="validate[required]">
									<option value="">SELECCIONE</option>
									<?php
									$qry = $db->query("SELECT * FROM usuarios INNER JOIN perfiles ON usuperfil = perid WHERE peradmgesdoc = 1");
									while ($row = $qry->fetch(PDO::FETCH_ASSOC))
										echo '<option value=' . $row['usuid'] . '>' . $row['usunombre'] . '</option>';
									?>
								</select>
							</p>

							<label for="detalle">Detalle: </label>
							<textarea name="detalle" rows="5" class="form-control"></textarea>
							</p>


							<p class="boton">
								<button type="submit" class="btn btn-primary btninsertar" name="solicitar" value="solicitar">Solicitar</button> <!-- BOTON  -->
							</p>
						</fieldset>
					</form>
				</div>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<?php
	if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>'; //VENTANA MODAL ERROR
	elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>'; //VENTANA MODAL EXITOSO
	?>
</body>

</html>