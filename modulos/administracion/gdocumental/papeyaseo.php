<?php

$r = '../../../';
//INCLUIR SESION Y CONECCION
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

if (isset($_POST['insertar'])) { // SE VALIDA SI LA INFORMACION VIENE DE INSERTAR 

	$empresa = $_POST['empresa'];
	$tipo = $_POST['tipo'];
	$proveedor = $_POST['proveedor'];
	$factura = $_POST['factura'];
	$tipodocu = $_POST['tipodocu'];
	$cantidad = $_POST['cantidad'];

	if ($tipo == 'unidad') {
		$unidad = 1;
		$consecutivo = 0;
		$inicio = 'NONE';
		$final = 'NONE';
	} elseif ($tipo == 'consecutivo') {
		$unidad = 0;
		$consecutivo = 1;
		$inicio = $_POST['inicio'];
		$final = $_POST['final'];
	}

	$qry = $db->query("INSERT INTO papeyaseo (pyaempresa, pyaunidad, pyaconsecutivo, pyainicio, pyafinal, pyaproveedor, pyafactura, pyatipodocu,  pyacantidad, pyaestado, pyausuarioingreso) VALUES ('$empresa', '$unidad', '$consecutivo', '$inicio', '$final', '$proveedor', '$factura', '$tipodocu', '$cantidad', 'CREADO', '" . $_SESSION['id'] . "');	");
	if ($qry) {
		$mensaje = 'Se ha ingresado correctamente .';
		header('Location:papeyaseo.php?mensaje=' . $mensaje);
		exit();
	} else {
		$error = 'Error al insertar intente nuevamente o contacte con el encargado';
		header('Location:papeyaseo.php?error=' . $error);
		exit();
	}
}
if (isset($_GET['mensaje']))
	$mensaje = $_GET['mensaje'];
if (isset($_GET['error']))
	$error = $_GET['error'];
?>
<!doctype html>
<html>

<head>

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>
	<script>
		function tipodetalle(valor) {
			if (valor == 'unidad') {
				document.getElementById("inicio").disabled = true;
				document.getElementById("final").disabled = true;
				document.getElementById("inicio").value = '';
				document.getElementById("final").value = '';
				document.getElementById("inicio").placeholder = '';
				document.getElementById("final").placeholder = '';
			} else if (valor == 'consecutivo') {
				document.getElementById("inicio").disabled = false;
				document.getElementById("final").disabled = false;
				document.getElementById("inicio").placeholder = 'Inicio Consecutivo';
				document.getElementById("final").placeholder = 'Final Consecutivo';
			}
		}
	</script>

</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
		<?php require($r . 'incluir/src/menu.php') ?>
		<!-- INCLUIMOS MENU PRINCIPAL -->
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Administracion</a>
				<div class="mapa_div"></div><a class="current">Papeleria y aseo</a>
			</article>
			<article id="contenido">
				<div class="ui-widget">
					<form id="form" name="form" action="papeyaseo.php" method="post">
						<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
							<legend class="ui-widget ui-widget-header ui-corner-all">Insertar Papeleria y aseo </legend>

							<p>
								<label for="empresa">Empresa:</label> <!-- CAMPO EMPRESA -->
								<select name="empresa" class="validate[required]">
									<option value="">SELECCIONE</option>
									<?php
									$qry = $db->query("SELECT * FROM empresas ORDER BY empnombre");
									while ($row = $qry->fetch(PDO::FETCH_ASSOC))
										echo '<option value=' . $row['empid'] . '>' . $row['empnombre'] . '</option>';
									?>
								</select>
							</p>

							<p>
								<label for="tipo">Tipo:</label> <!-- CAMPO -->
								<select name="tipo" class="validate[required]" onChange="tipodetalle(this.value);">
									<option value="">SELECCIONE</option>
									<option value='unidad'>Unidad</option>
									<option value='consecutivo'>Consecutivo</option>
								</select>
							</p>


							<p>
								<label for="inicio">Consecutivo:</label> <!-- CAMPO CONSECUTIVO -->
								<input type="text" name="inicio" id="inicio" class="validate[required, custom[onlyNumberSp]] text-input" disabled /> 
								<span> - </span> 
								<input id="final" type="text" name="final" class="validate[required, custom[onlyNumberSp]] text-input" disabled />
							</p>


							<p>
								<label for="proveedor">Proveedor:</label> <!-- CAMPO -->
								<select name="proveedor" class="validate[required]">
									<option value="">SELECCIONE</option>
									<?php
									$qry = $db->query("SELECT * FROM proveedores ORDER BY pvdnombre");
									while ($row = $qry->fetch(PDO::FETCH_ASSOC))
										echo '<option value=' . $row['pvdid'] . '>' . $row['pvdnombre'] . '</option>';
									?>
								</select>
							</p>

							<p>
								<label for="factura">Factura:</label> <!-- CAMPO CONSECUTIVO -->
								<input type="text" name="factura" class="validate[required, custom[onlyNumberSp]] text-input" />
							</p>


							<p>
								<label for="tipodocu">Producto:</label> <!-- CAMPO -->
								<select name="tipodocu" class="validate[required]">
									<option value="">SELECCIONE</option>
									<?php
									/* Retorna el inventario referente a papeleria y aseo */
									$qry = $db->query("SELECT * FROM pyatipodocu ORDER BY pyatdnombre");
									while ($row = $qry->fetch(PDO::FETCH_ASSOC))
										echo '<option value=' . $row['pyatdid'] . '>' . $row['pyatdnombre'] . '</option>';
									?>
								</select>
							</p>

							<p>
								<label for="cantidad">Cantidad:</label> <!-- CAMPO CANTIDAD -->
								<input type="text" name="cantidad" class="validate[required, custom[onlyNumberSp]] text-input" />
							</p>


							<p class="boton">
								<button type="submit" class="btn btn-primary btninsertar" name="insertar" value="insertar">Insertar</button> <!-- BOTON INSERTAR -->
							</p>
						</fieldset>
					</form>
				</div>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<?php
	if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>'; //VENTANA MODAL ERROR
	elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>'; //VENTANA MODAL EXITOSO
	?>
</body>

</html>