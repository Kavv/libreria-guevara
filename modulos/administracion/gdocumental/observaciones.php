<?php
$r = '../../../';
//INCLUIR SESION Y CONECCION
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

$empresa = $tdocumento = $consecutivo = $identificador = "";

if(isset($_GET['empresa']))
	$empresa = $_GET['empresa'];
if(isset($_GET['tdocumento']))
	$tdocumento = $_GET['tdocumento'];
if(isset($_GET['consecutivo']))
	$consecutivo = $_GET['consecutivo'];

if(isset($_GET['identificador']))
{
	$identificador = $_GET['identificador'];

	$qryiden = $db->query("SELECT * FROM gestiondocumental WHERE gdid = '" . $identificador . "';");
	$rowiden = $qryiden->fetch(PDO::FETCH_ASSOC);
	$empresadet = $rowiden['gdempresa'];
	$consecutivodet = $rowiden['gdempresa'];
	$cajadet = $rowiden['gdcaja'];
	$carpetadet = $rowiden['gdcarpeta'];
	
}



if (isset($_POST['insertar'])) { // SE VALIDA SI LA INFORMACION VIENE DE INSERTAR 

	$identificador1 = $_POST['identificador1'];
	$observaciones = trim($_POST['observaciones']);


	$qryiden = $db->query("INSERT INTO histogestiondocu (histoiddocu, histousuario, histonota) VALUES ('" . $identificador1 . "', '" . $_SESSION['id'] . "', '" . $observaciones . "')");

	if ($qryiden) {
		$mensaje = 'Se ha ingresado correctamente el comentario.';
		header('Location:listar.php?mensaje=' . $mensaje . '&' . $filtro);
	} else {
		$error = 'No se pudo ingresar el comentario intente nuevamente o contacte con el encargado';
		header('Location:listar.php?error=' . $error . '&' . $filtro);
	}
}

$filtro = 'empresa=' . $empresa . '&tdocumento=' . $tdocumento . '&consecutivo=' . $consecutivo;
if (isset($_GET['mensaje']))
	$mensaje = $_GET['mensaje'];

if (isset($_GET['error']))
	$error = $_GET['error'];
?>
<!doctype html>
<html>

<head>

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<script type="text/javascript" src="<?php echo $r ?>incluir/funciones.js"></script>
	<script>
		$(document).ready(function() {
			$('#viaje').datepicker({
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
				minDate: '+0D',
				maxDate: '+50D',
			}).keypress(function(event) {
				event.preventDefault()
			});
			$('#regreso').datepicker({
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
				minDate: '+0D',
				maxDate: '+50D',
			}).keypress(function(event) {
				event.preventDefault()
			});
		});
	</script>

</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
		<?php require($r . 'incluir/src/menu.php') ?>
		<!-- INCLUIMOS MENU PRINCIPAL -->
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Administracion</a>
				<div class="mapa_div"></div><a class="current">Gestion Documental</a>
			</article>
			<article id="contenido">
				<div class="ui-widget">
					<form id="form" name="form" action="observaciones.php?<?php echo $filtro ?>" method="post" enctype="multipart/form-data">
						<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
							<legend class="ui-widget ui-widget-header ui-corner-all">Agregar Comentario</legend>

							<input type="hidden" name="identificador1" value="<?php echo $identificador ?>" />

							<p>
								<label>Observaciones: </label>
								<textarea name="observaciones" rows="4" class="form-control"></textarea>
							</p>

							<p class="boton">
								<button type="submit" class="btn btn-primary btninsertar" name="insertar" value="insertar">Agregar</button> <!-- BOTON INSERTAR -->
							</p>
						</fieldset>
					</form>
				</div>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<?php
	if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>'; //VENTANA MODAL ERROR
	elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>'; //VENTANA MODAL EXITOSO
	?>
</body>

</html>