<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
?>
<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a class="current">Gestion Documental</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="listar.php" method="post">
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">Consulta de Documentos</legend>


						<p>
							<label for="empresa">Empresa:</label> <!-- CAMPO EMPRESA -->
							<select name="empresa">
								<option value="">SELECCIONE</option>
								<?php
								$qry = $db->query("SELECT * FROM empresas ORDER BY empnombre");
								while ($row = $qry->fetch(PDO::FETCH_ASSOC))
									echo '<option value=' . $row['empid'] . '>' . $row['empnombre'] . '</option>';
								?>
							</select>
						</p>

						<p>
							<label for="tdocumento">T. Docuemnto:</label> <!-- CAMPO T DOCUMENTO -->
							<select name="tdocumento">
								<option value="">SELECCIONE</option>
								<?php
								$qry = $db->query("SELECT * FROM gdtipodocumento ORDER BY tdnombre");
								while ($row = $qry->fetch(PDO::FETCH_ASSOC))
									echo '<option value=' . $row['tdid'] . '>' . $row['tdnombre'] . '</option>';
								?>
							</select>
						</p>

						<p>
							<label for="consecutivo">Consecutivo:</label> <!-- CAMPO CONSECUTIVO -->
							<input type="text" name="consecutivo" class="validate[custom[onlyNumberSp]] text-input" />
						</p>

						<div class="row">
							<div class="col-md-12 col-lg-6">
								<button type="submit" class="btnconsulta btn btn-primary btn-block mt-2" name="consultar" value="Buscar">Consultar</button> <!-- BOTON CONSULTAR -->
							</div>
							<div class="col-md-12 col-lg-6">
								<button type="submit" class="btnconsulta btn btn-primary btn-block mt-2" name="show-all" value="Buscar">Mostrar Todo</button> </div>
						</div>
					</fieldset>
				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<?php
	if (isset($_GET['error'])) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['error'] . '</div>';
	?>
</body>

</html>