<?php
$r = '../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
require($r.'incluir/funciones.php');

?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
	<?php 
		require($r . 'incluir/src/head.php');
		require($r . 'incluir/src/head-form.php');
	?>	

<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#tabla').dataTable({
		'bJQueryUI': true,
		'bStateSave': true,
		'sPaginationType': 'full_numbers',
		'oLanguage': {
			'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
		},
		'bSort': false,
	});
	$('.btnatras').button({ icons: { primary: 'ui-icon ui-icon-arrowthick-1-w' }});
});
</script>
</head>
<body>
<?php require($r.'incluir/src/login.php') ?>
<section id="principal">
<?php require($r.'incluir/src/cabeza.php') ?>
<?php require($r.'incluir/src/menu.php') ?>
<article id="cuerpo">
<article class="mapa">
<a href="#">Principal</a><div class="mapa_div"></div><a href="#">Administracion</a><div class="mapa_div"></div><a class="current">Cargue Masivo Gestion Documental</a>
</article>

<article id="contenido">
<h2>Cargue de Gestion Documental </h2>
<?php 



	$uploaddir = 'carguemasivo/';
	$uploadfile = $uploaddir . "carguemasivogd.csv";
	if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
		echo " <p>El archivo fue cargado exitosamente </p>";
		$unr = "\r";
		$unn = "\n";
		$qry = $db->query('LOAD DATA INFILE "C://BitNami/wampstack-5.4.24-0/apache2/htdocs/usa/modulos/administracion/gdocumental/'.$uploadfile.'" INTO TABLE gestiondocumental FIELDS TERMINATED BY ";" LINES TERMINATED BY "\r\n" IGNORE 1 LINES;
		');
		$num = $qry->rowCount();
		if ($qry) {
			echo "<p>Se han añadido ".$num." registros nuevos! </p>";
		} else {
		echo "<p>No se Pudo Cargar El archivo</p>";
		}
		
	} else {
		echo " <p>¡Hubo un error por favor intenta nuevamente! </p>";
		echo "<p class='boton'>
		<button type='button' class='btnatras' onClick=' carga(); location.href = 'cargue_masivogd.php''>Atras</button>
		</p>";
	}



?>
</article>
</article>
<?php require($r.'incluir/src/pie.php');

?>
</section>
</body>
</html>