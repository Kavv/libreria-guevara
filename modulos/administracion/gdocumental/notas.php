<?php
$r = '../../../';
require($r . 'incluir/connection.php');
$id = "";
if($_GET['id'])
	$id = $_GET['id'];
?>
<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	?>
	<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
	<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#tabla2').dataTable({
				'bJQueryUI': true,
				'bFilter': false,
				'bLengthChange': false,
				'bSort': false,
				'sPaginationType': 'full_numbers',
				'oLanguage': {
					'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
				},
			});
		});
	</script>
</head>

<body>
	<table id="tabla2">
		<thead>
			<tr>
				<th>Fecha</th>
				<th>Usuario</th>
				<th>Nota</th>
			</tr>
		</thead>
		<tbody>
			<?php
			$qry = $db->query("SELECT * FROM histogestiondocu INNER JOIN usuarios ON histousuario = usuid WHERE  histoiddocu = '$id' ORDER BY histofecha DESC");
			if($qry != null){
			while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
			?>
				<tr>
					<td align="center"><?php echo $row2['histofecha'] ?></td>
					<td align="center"><?php echo $row2['usunombre'] ?></td>
					<td><?php echo $row2['histonota'] ?></td>
				</tr>
			<?php
			}}
			?>
		</tbody>
	</table>
	</p>
</body>

</html>