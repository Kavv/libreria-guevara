<?php
$r = '../../../';
//INCLUIR SESION Y CONECCION
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

$identificador = $_GET['identificador'];
$tipo = $_GET['tipo'];
$empresa = $_GET['empresa'];
$tipodocu = $_GET['tipodocu'];
$entregado = $_GET['entregado'];
$fecha1 = $_GET['fecha1'];
$fecha2 = $_GET['fecha2'];
$filtro = 'tipo=' . $tipo . '&empresa=' . $empresa . '&tipodocu=' . $tipodocu . '&entregado=' . $entregado . '&fecha1=' . $fecha1 . '&fecha2=' . $fecha2;
?>
<!doctype html>
<html>

<head>

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<script type="text/javascript" src="<?php echo $r ?>incluir/funciones.js"></script>

</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
		<?php require($r . 'incluir/src/menu.php') ?>
		<!-- INCLUIMOS MENU PRINCIPAL -->
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Administracion</a>
				<div class="mapa_div"></div><a class="current">Gestion Documental</a>
			</article>
			<article id="contenido">
				<div class="ui-widget">
					<form id="form" name="form" action="listarpape.php?<?php echo $filtro; ?>" method="get">
						<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
							<legend class="ui-widget ui-widget-header ui-corner-all">Validar entrega</legend>

							<input type="hidden" name="identificador" value="<?php echo $identificador ?>" />

							<p>
								<label for="usurio">Entregado a:</label>
								<select name="usuario" class="validate[required]">
									<option value="">SELECCIONE</option>
									<?php
									$qry = $db->query("SELECT * FROM usuarios WHERE usuperfil <> 56 ORDER BY usunombre");
									while ($row = $qry->fetch(PDO::FETCH_ASSOC))
										echo '<option value=' . $row['usuid'] . '>' . $row['usunombre'] . '</option>';
									?>
								</select>
							</p>

							<p class="boton">
								<button type="submit" class="btn btn-primary btninsertar" name="validar" value="validar">Validar</button> <!-- BOTON INSERTAR -->
							</p>
						</fieldset>
					</form>
				</div>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<?php
	if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>'; //VENTANA MODAL ERROR
	elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>'; //VENTANA MODAL EXITOSO
	?>
</body>

</html>