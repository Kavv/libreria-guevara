<?php

$r = '../../../';
//INCLUIR SESION Y CONECCION
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

$empresa = $tdocumento = $consecutivo = $identificador = "";

if (isset($_GET['empresa']))
	$empresa = $_GET['empresa'];
if (isset($_GET['tdocumento']))
	$tdocumento = $_GET['tdocumento'];
if (isset($_GET['consecutivo']))
	$consecutivo = $_GET['consecutivo'];

if (isset($_GET['identificador'])) {
	$identificador = $_GET['identificador'];

	$sqldet = 'SELECT * FROM gestiondocumental INNER JOIN empresas ON empid = gdempresa  INNER JOIN usuarios ON usuid = gdusuario INNER JOIN gdtipodocumento ON tdid = gdtipodocumento WHERE gdid = ' . $identificador . ';';
	$qrydet = $db->query($sqldet);
	$rowdet = $qrydet->fetch(PDO::FETCH_ASSOC);

}

$filtro = 'empresa=' . $empresa . '&tdocumento=' . $tdocumento . '&consecutivo=' . $consecutivo;

if (isset($_GET['mensaje']))
	$mensaje = $_GET['mensaje'];

if (isset($_GET['error']))
	$error = $_GET['error'];
?>
<!doctype html>
<html>

<head>

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<script type="text/javascript" src="<?php echo $r ?>incluir/funciones.js"></script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
		<?php require($r . 'incluir/src/menu.php') ?>
		<!-- INCLUIMOS MENU PRINCIPAL -->
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Administracion</a>
				<div class="mapa_div"></div><a class="current">Gestion Documental</a>
			</article>
			<article id="contenido">
				<div class="ui-widget">
					<form id="form" name="form" action="listar.php?<?php echo $filtro; ?>" method="post" enctype="multipart/form-data">
						<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
							<legend class="ui-widget ui-widget-header ui-corner-all">Modificar Gestion Documental</legend>

							<p>
								<label for="empresa">Empresa:</label> <!-- CAMPO EMPRESA -->
								<select name="empresa" class="validate[required]">
									<?php
									if ($rowdet['gdempresa'] != '') {
										$qry = $db->query("SELECT * FROM empresas where empid = '" . $rowdet['gdempresa'] . "' ORDER BY empnombre");
										while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
											echo '<option value=' . $row['empid'] . '>' . $row['empnombre'] . '</option>';
										}
									}
									
									$qry = $db->query("SELECT * FROM empresas where empid <> '" . $rowdet['gdempresa'] . "' ORDER BY empnombre");
									while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
										echo '<option value=' . $row['empid'] . '>' . $row['empnombre'] . '</option>';
									}
									?>
								</select>
							</p>

							<p>
								<label for="tdocumento">T. Documento:</label> <!-- CAMPO T DOCUMENTO -->
								<select name="tdocumento" class="validate[required]">
									<?php
									if ($rowdet['gdtipodocumento'] <> '') {
										$qry = $db->query("SELECT * FROM gdtipodocumento where tdid = " . $rowdet['gdtipodocumento'] . " ORDER BY tdnombre");
										if($qry != null)
										while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
											echo '<option value=' . $row['tdid'] . '>' . $row['tdnombre'] . '</option>';
										}
									}
									
									$qry = $db->query("SELECT * FROM gdtipodocumento where tdid <> " . $rowdet['gdtipodocumento'] . " ORDER BY tdnombre");
									if($qry != null)
									while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
										echo '<option value=' . $row['tdid'] . '>' . $row['tdnombre'] . '</option>';
									}
									?>
								</select>
							</p>

							<input type="hidden" name="identificador" value="<?php echo $rowdet['gdid']; ?>" />

							<p>
								<label for="consecutivo">Consecutivo:</label> <!-- CAMPO CONSECUTIVO -->
								<input type="text" name="consecutivo" class="validate[required, custom[onlyNumberSp]] text-input" value="<?php echo $rowdet['gdconsecutivo']; ?>" />
							</p>

							<p>
								<label for="caja">Caja:</label> <!-- CAMPO CAJA -->
								<input type="text" name="caja" class="validate[required, custom[onlyNumberSp]] text-input" value="<?php echo $rowdet['gdcaja']; ?>" />
							</p>

							<p>
								<label for="carpeta">Carpeta:</label> <!-- CAMPO CARPETA-->
								<input type="text" name="carpeta" class="validate[required, custom[onlyNumberSp]] text-input" value="<?php echo $rowdet['gdcarpeta']; ?>" />
							</p>

							<p class="boton">
								<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href='listar.php?<?php echo $filtro ?>'">Atras</button>
								<button type="submit" class="btn btn-primary btnmodificar" name="modificar" value="modificar">Modificar</button>
							</p>
						</fieldset>
					</form>
				</div>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<?php
	if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>'; //VENTANA MODAL ERROR
	elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>'; //VENTANA MODAL EXITOSO
	?>
	<div id="modal" style="display:none"></div>
	<div id="dialog2" title="Historico" style="display:none"></div>
</body>

</html>