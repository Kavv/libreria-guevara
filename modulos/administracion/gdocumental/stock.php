<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
require($r . 'incluir/funciones.php');

$sql = "SELECT * FROM pyatipodocu order by pyatdnombre";


if (isset($_GET['mensaje']))
	$mensaje = $_GET['mensaje'];
if (isset($_GET['error']))
	$error = $_GET['error'];

$qry = $db->query($sql);
?>
<!doctype html>
<html>

<head>
	<meta charset="utf-8">
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>

	<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
	<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#tabla').dataTable({
				'bJQueryUI': true,
				'sPaginationType': 'full_numbers',
				'oLanguage': {
					'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
				},
				'bSort': false,
			});

			$('.msj_validar').click(function(e) {
				e.preventDefault();
				var targetUrl = $(this).attr('href');
				var $dialog_link_follow_confirm = $('<div></div>').html("<p><span class='ui-icon ui-icon-alert' style='float:left; margin:2px 7px 7px 0;'></span>Esta seguro que desea VALIDAR la entrega?</p>").
				dialog({
					title: 'Porfavor confirmar',
					buttons: {
						'Si': function() {
							window.location.href = targetUrl;
						},
						'No': function() {
							$(this).dialog("close");
						}
					},
					modal: true,
					width: 'auto',
					height: 'auto'
				});
				$dialog_link_follow_confirm.dialog("open");
			});
		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a class="current">Papeleria y aseo</a>
			</article>
			<article id="contenido">
				<h2>Stock Actual</h2>
				<table id="tabla">
					<thead>
						<tr>
							<th>Id</th>
							<th>Producto</th>
							<th>Ingresados</th>
							<th>Entregados</th>
							<th>Total</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$i = 0;
						while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
							$ingresados = 0;
							$entregados = 0;
							$total = 0;
							$sql1 = "SELECT * FROM papeyaseo where pyatipodocu = " . $row['pyatdid'] . ";";
							$qry1 = $db->query($sql1);
							while ($row1 = $qry1->fetch(PDO::FETCH_ASSOC)) {
								$ingresados += $row1['pyacantidad'];
							}

							$sql2 = "SELECT * FROM solicitudespape where solpapetipodocu = " . $row['pyatdid'] . " AND solpapeesatdo = '1';";
							$qry2 = $db->query($sql2);
							while ($row2 = $qry2->fetch(PDO::FETCH_ASSOC)) {
								$entregados += $row2['solpapecantidad'];
							}

							$total = $ingresados - $entregados;

						?>
							<tr>
								<td><?php echo $row['pyatdid'] ?></td>
								<td><?php echo $row['pyatdnombre']  ?></td>
								<td align="center"><?php echo number_format($ingresados, 0, ',', '.') ?></td>
								<td align="center"><?php echo number_format($entregados, 0, ',', '.') ?></td>
								<td align="center"><?php echo number_format($total, 0, ',', '.') ?></td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
				<p class="boton">
					<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href='consultarpape.php'">Atras</button>
				</p>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<?php
	if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>';
	elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>';
	?>
	<div id="modal" style="display:none"></div>
	<div id="dialog2" style="display:none"></div>
</body>

</html>