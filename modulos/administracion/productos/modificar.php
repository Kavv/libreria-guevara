<?php
// LA ACCION PRINCIPAL DE ESTA PAGINA ES MODIFICA UNA CIUDAD ENVIANDOLO A LISTAR

$r = '../../../';
//INCLUIR SESION Y CONECCION
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
$row = $db->query("SELECT * FROM productos WHERE proid = '" . $_GET['id1'] . "'")->fetch(PDO::FETCH_ASSOC); // CONSULTA A LA BD CON VARUABLES RECIBIDAS POR GET
$filtro = 'id=' . $_GET['id'] . '&nombre=' . $_GET['nombre']. '&categoria=' . $_GET['id']. '&editorial=' . $_GET['editorial']. '&tipo=' . $_GET['tipo']. '&activo=' . $_GET['activo']; // FILTRO GENERAL PARA ENVIAR POR GET

?>
<!doctype html>

<html lang="es">

<head>
    <title>MODIFICAR PRODUCTO <?php echo $_GET['id1']?></title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">
	<meta charset="utf-8">

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>

</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
		<?php require($r . 'incluir/src/menu.php') ?>
		<!-- INCLUIMOS MENU PRINCIPAL -->
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Administracion</a>
				<div class="mapa_div"></div><a class="current">Productos</a>
			</article>
			<article id="contenido">
				<div class="ui-widget">
					<form id="form" name="form" action="listar.php?<?php echo $filtro ?>" method="post">
						<!-- FORMULARIO ENVIADO POR POST A LISTAR.PHP JUNTO CON EL FILTRO GENERAL  -->
						<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
							<legend class="ui-widget ui-widget-header ui-corner-all">Modificar producto</legend>
							<p>
								<label for="id">Codigo:</label> <!-- CAMPO CODIGO -->
								<input type="text" name="id" class="referen" value="<?php echo $row['proid'] ?>" readonly />
							</p>
							<p>
								<label for="nombre">Nombre:</label> <!-- CAMPO NOMBRE -->
								<input type="text" name="nombre" class="nombre validate[required] text-input" value="<?php echo $row['pronombre'] ?>" title="Digite el nombre del producto" />
							</p>
							<p>
								<label for="desc">Descripcion:</label> <!-- CAMPO DESCRIPCION -->
								<input type="text" name="desc" value="<?php echo $row['prodescr'] ?>" />
							</p>
							<p>
								<label for="precio">Precio:</label> <!-- CAMPO PRECIO -->
								<input type="text" name="precio" class="valor validate[required, min[0], custom[onlyNumberSp]] text-input" value="<?php echo $row['proprecio'] ?>" title="Digite el precio del producto" />
							</p>
							<p style="visibility: hidden;height: 0;">
								<label for="tipo">Categoria</label>
								<select class="validate[required]" name="categoria">
									<option value="-1">SIN CATEGORIA</option>
									<?php 
									$categorias = $db->query("SELECT * FROM categorias WHERE id > 0 ORDER BY nombre asc");
									while ($categoria = $categorias->fetch(PDO::FETCH_ASSOC)) {
										if($row['categoria'] == $categoria['id'])
											echo '<option selected value="'.$categoria['id'].'">'.$categoria['nombre'].'</option>';
										else
											echo '<option value="'.$categoria['id'].'">'.$categoria['nombre'].'</option>';
									}
									?>
								</select>
							</p>
							<p>
								<label for="editorial">Editorial</label>
								<input type="text" name="editorial" class="validate[maxSize[40]]" style="text-transform: uppercase" title="Digite la editorial" value="<?php echo $row['editorial'] ?>" />
							</p>
							<p style="visibility: hidden;height: 0;">
								<label for="tipo">Tipo</label>
								<select class="validate[required]" name="tipo">
									<?php
									if ($row['protipo'] == "MP")
										echo '<option value="MP">MATERIA PRIMA</option><option value="PT">PRODUCTO TERMINADO</option>';
									else if ($row['protipo'] == "PT")
										echo '<option value="PT">PRODUCTO TERMINADO</option><option value="MP">MATERIA PRIMA</option>';
									else
										echo '<option value="">SELECCIONE</option><option value="MP">MATERIA PRIMA</option><option value="PT">PRODUCTO TERMINADO</option>';


									?>
								</select>
							</p>
							<p>
								<label for="desactivar" style="width:auto!important;">Desactivar:</label> <!-- CAMPO DESACTIVAR -->
								<input type="checkbox" name="desactivar" value="1" <?php if ($row['prodesactivado'] == 1) { ?> checked <?php } ?> style="width:auto!important;" />
							</p>
							<p class="boton">
								<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href='listar.php?<?php echo $filtro ?>'">Atras</button> <!--  BOTON PARA VOLVER ATRAS JUNTO CON FILTRO GENERAL-->
								<button type="submit" class="btn btn-primary btnmodificar" name="modificar" value="modificar">Modificar</button> <!-- BOTON MODIFICAR -->
							</p>
						</fieldset>
					</form>
				</div>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
		<!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
	</section>
</body>

</html>