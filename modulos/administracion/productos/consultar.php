<?php
// LA ACCION PRINCIPAL ES ESTA PAGINA ES CONSULTAR LAS CIUDADES 

$r = '../../../';
//INCLUIR SESION Y CONECCION
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
?>
<!doctype html>

<html lang="es">

<head>
    <title>CONSULTAR PRODUCTOS</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">

<head>

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>

</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
		<?php require($r . 'incluir/src/menu.php') ?>
		<!-- INCLUIMOS MENU PRINCIPAL -->
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Administracion</a>
				<div class="mapa_div"></div><a class="current">Productos</a>
			</article>
			<article id="contenido">
				<div class="ui-widget">
					<form id="form" name="form" action="listar.php" method="post">
						<!-- ENVIO FORMULARIO POR POST A LISTAR.PHP -->
						<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
							<legend class="ui-widget ui-widget-header ui-corner-all">Consultar productos</legend>
							<p>
								<label for="id">Codigo:</label> <!-- CAMPO CODIGO -->
								<input type="text" name="id" class="referen" title="Digite el codigo" />
							</p>
							<p>
								<label for="nombre">Nombre:</label> <!-- CAMPO  NOMBRE -->
								<input type="text" name="nombre" class="nombre" title="Digite el nombre del producto" />
							</p>
							<p>
								<label for="categoria">Categoria</label>
								<select class="" name="categoria">
									<option value="">TODOS</option>
									<option value="-1">SIN CATEGORIA</option>
									<?php 
									$categorias = $db->query("SELECT * FROM categorias WHERE id > 0 ORDER BY nombre asc");
									while ($categoria = $categorias->fetch(PDO::FETCH_ASSOC)) {
										echo '<option value="'.$categoria['id'].'">'.$categoria['nombre'].'</option>';
									}
									?>
								</select>
							</p>
							<p>
								<label for="editorial">Editorial</label>
								<input type="text" name="editorial" style="text-transform: uppercase" title="Digite la editorial" />
							</p>
							<p>
								<label for="tipo">Tipo</label>
								<select class="" name="tipo">
									<option value="">TODOS</option>
									<option value="PT">PRODUCTO TERMINADO</option>
									<option value="MP">MATERIA PRIMA</option>
								</select>
							</p>
							<p>
								<label for="activo">Activo:</label> <!-- CAMPO  NOMBRE -->

								<select class="" name="activo">
									<option value="">TODOS</option>
									<option value="0">Sí</option>
									<option value="1">No</option>
								</select>							
							</p>
							<div class="row">
								<div class="col-md-12 col-lg-12">
									<button type="submit" class="btnconsulta btn btn-primary btn-block mt-2" name="consultar" value="Buscar">Consultar</button> <!-- BOTON CONSULTAR -->
								</div>
							</div>
						</fieldset>
					</form>
				</div>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
		<!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
	</section>
</body>

</html>