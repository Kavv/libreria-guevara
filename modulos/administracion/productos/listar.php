<?php
// LA ACCION PRINCIPAL DE ESTA PAGINA ES MOSTRAR LOS RESULTADOS DE LA CONSULTA GENERADA DESDE CONSULTAR.PHP
// LUEGO DE HABER RECIBIDO LAS VARIABLES POR POST DESDE CONSULTAR.PHP PROCEDEMOS A EVALUAR ESAS VARIABLES PARA MOSTRAR RESULTADOS DE LA CONSULTA O MODIFICAR SI ASI CORRESPONDE

//INCLUIR SESION Y CONECCION
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
require($r . 'incluir/funciones.php');
if (isset($_POST['modificar'])) { //VALIDAMOS SI POST MODIFICAR ES TRUE
	$id = $_POST['id'];
	$nombre = strtoupper(trim($_POST['nombre']));  // LO RECIBIDO DE ESTE CAMPO LO CAMBIAMOS A MAYUSCULA
	$num = $db->query("SELECT * FROM productos WHERE proid <> '$id' AND pronombre = '$nombre'")->rowCount(); // REALIZAMOS CONSULTA CON VARIABLES RECIBIDAS Y CONTAMOS LOS RESULTADOS OBTENIDOS
	if ($num < 1) { // SI LOS RESULTADOS DE LA CONSULTA SON MENORES A  1 REALIZAMOS LO SIGUIENTE
		$desc = trim(strtoupper($_POST['desc']));
		$precio = $_POST['precio'];
		$tipo = $_POST['tipo'];
		$desactivar = 0;
		$categoria = $_POST['categoria'];
		$editorial = $_POST['editorial'];
		if(isset($_POST['desactivar']))
			$desactivar = $_POST['desactivar'];
		$qry = $db->query("UPDATE productos SET 
		pronombre = '$nombre', 
		prodescr = '$desc', 
		proprecio = $precio, 
		protipo = '$tipo', 
		prodesactivado = '$desactivar', 
		categoria = $categoria,
		editorial = '$editorial'		
		WHERE proid = '$id'"); // ACTUALIZAMOS LA INFORMACION CON LOS NUEVOS DATOS RECIBIDOS
		if ($qry) $mensaje = 'Se actualizo el producto';  // MENSAJE EXITOSO
		else $error = 'No se actualizo el producto'; // MENSAJE ERROR
	} else $error = 'El nombre del producto ya existe';
	$filtro = 'id=' . $_GET['id'] . '&nombre=' . $_GET['nombre']. '&categoria=' . $_GET['id']. '&editorial=' . $_GET['editorial']. '&tipo=' . $_GET['tipo']. '&activo=' . $_GET['activo']; // FILTRO GENERAL PARA ENVIAR POR GET
	if(isset($mensaje))
		$url = 'listar.php?msj='.$mensaje.'&'.$filtro;
	else
		$url = 'listar.php?error='.$error.'&'.$filtro;
	header("location:$url");

}
// Eliminar 
if (isset($_GET['id1'])) { //VALIDAMOS SI GET ID1 ES TRUE 
	$id1 = $_GET['id1'];
	$num = $db->query("SELECT * FROM detsolicitudes WHERE detproducto = '$id1'")->rowCount();
	if ($num < 1) { // SI LOS RESULTADOS DE LA CONSULTA SON MENORES A  1 REALIZAMOS LO SIGUIENTE
		$num = $db->query("SELECT * FROM detmovimientos WHERE dmoproducto = '$id1'")->rowCount();
		if ($num < 1) {
			$qry = $db->query("DELETE FROM productos WHERE proid = '$id1'"); //ELIMINAMOS EL PRODUCTO
			if ($qry) $mensaje = 'Se elimino el producto';
			else $error = 'No se pudo eliminar el producto';
		} else $error = 'El producto tiene movimiento';
	} else $error = 'El producto esta asociado a solicitudes';
	
	$filtro = 'id=' . $_GET['id'] . '&nombre=' . $_GET['nombre']. '&categoria=' . $_GET['id']. '&editorial=' . $_GET['editorial']. '&tipo=' . $_GET['tipo']. '&activo=' . $_GET['activo']; // FILTRO GENERAL PARA ENVIAR POR GET
	if(isset($mensaje))
		$url = 'listar.php?msj='.$mensaje.'&'.$filtro;
	else
		$url = 'listar.php?error='.$error.'&'.$filtro;
	header("location:$url");
}
$id = $nombre = "";
if (isset($_POST['consultar'])) { //VALIDAMOS SI POST CONSULTAR ES TRUE
	$id = $_POST['id'];
	$nombre = strtoupper(trim($_POST['nombre']));
	$categoria = $_POST['categoria'];
	$editorial = strtoupper(trim($_POST['editorial']));
	$tipo = $_POST['tipo'];
	$activo = $_POST['activo'];
} else if (isset($_GET['id'])) { // SI LO RECIBIDO POR POST CONSULTAR NO ES TRUE
	$id = $_GET['id'];
	$nombre = strtoupper(trim($_GET['nombre']));
	$categoria = $_GET['categoria'];
	$editorial = strtoupper(trim($_GET['editorial']));
	$tipo = $_GET['tipo'];
	$activo = $_GET['activo'];
}
$filtro = 'id=' . $id . '&nombre=' . $nombre. '&categoria=' . $categoria. '&editorial=' . $editorial. '&tipo=' . $tipo. '&activo=' . $activo; // FILTRO GENERAL PARA ENVIAR POR GET

$con = "SELECT *
	FROM productos
	LEFT JOIN categorias ON productos.categoria = categorias.id";
$ord = 'ORDER BY pronombre ASC';
$sql = crearConsulta($con, $ord,
	array($id, "proid = '$id'"),
	array($nombre, "pronombre like '%$nombre%'"),
	array($categoria, "categoria = '$categoria'  "),
	array($editorial, "editorial = '$editorial'"),
	array($tipo, "protipo = '$tipo'"),
	array($activo, "prodesactivado = '$activo'"),
);

$qry = $db->query($sql);
?>
<!doctype html>

<html lang="es">

<head>
    <title>LISTA DE PRODUCTOS</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
	<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
	<script>
		$(document).ready(function() {
			$('#tabla').dataTable({ //QRY PARA MANEJO Y VISUALIZACION DE TABLAS
				'bJQueryUI': true,
				'sPaginationType': "full_numbers",
				'oLanguage': {
					'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
				},
				'aoColumnDefs': [{
					'bSortable': false,
					'aTargets': [6, 7, 8]
				}],
			});

			$('.confirmar').click(function(e) { // VENTANA MODAL PARA CONFIRMAR ACCIONES
				e.preventDefault();
				var targetUrl = $(this).attr("href");
				var $dialog_link_follow_confirm = $('<div></div>').html('<p><span class="ui-icon ui-icon-trash" style="float:left; margin:2px 7px 7px 0;"></span>Esta seguro que desea eliminar el producto?</p>').
				dialog({
					title: 'Porfavor confirmar',
					buttons: {
						'Si': function() {
							carga();
							window.location.href = targetUrl;
						},
						'No': function() {
							$(this).dialog("close");
						}
					},
					modal: true,
					width: 'auto',
					height: 'auto'
				});
				$dialog_link_follow_confirm.dialog('open');
			});
		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
		<?php require($r . 'incluir/src/menu.php') ?>
		<!-- INCLUIMOS MENU PRINCIPAL -->
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Administracion</a>
				<div class="mapa_div"></div><a class="current">Productos</a>
			</article>
			<article id="contenido">
				<h2>Listado de productos</h2>
				<div class="reporte">

				</div>
				<table id="tabla">
					<thead>
						<tr>
							<th>Codigo</th>
							<th>Nombre</th>
							<th>Descripcion</th>
							<th>Editorial</th>
							<th>Cantidad</th>
							<th>Precio</th>
							<th>Activo</th>
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php
						while ($row = $qry->fetch(PDO::FETCH_ASSOC)) { //INFORMACION DE LA CONSULTA
						?>
							<tr>
								<td><?php echo $row['proid'] ?></td>
								<td><?php echo $row['pronombre'] ?></td>
								<td><?php echo $row['prodescr'] ?></td>
								<td><?php echo $row['editorial'] ?></td>
								<td align="center"><?php echo $row['procantidad'] ?></td>
								<td align="right"><?php echo number_format($row['proprecio'], 0, '.', ',') ?></td>
								<td align="center"><?php if ($row['prodesactivado'] == '0') echo '<img src="' . $r . 'imagenes/iconos/accept.png" />';
													else echo '<img src="' . $r . 'imagenes/iconos/cancel_.png" />' ?></td>
								<td align="center"><a target="_blank" href="modificar.php?<?php echo 'id1=' . $row['proid'] . '&' . $filtro ?>"  title="editar"><img src="<?php echo $r ?>imagenes/iconos/lapiz.png" class="grayscale" /></a></td> <!-- ENVIA POR GET LAS VARIABLES NECESARIAS PARA MODIFICAR UN PRODUCTO -->
								<td align="center"><a href="listar.php?<?php echo 'id1=' . $row['proid'] . '&' . $filtro ?>" title="eliminar" class="confirmar"><img src="<?php echo $r ?>imagenes/iconos/basura.png" class="grayscale" /></a></td> <!-- ENVIA LAS VARIABLES NECESARIAS PARA ELIMINAR UN PRODUCTO JUNTO CON VENTANA MODAL PARA CONFIRMAR -->
							</tr>
						<?php
						}
						?>
					</tbody>
				</table>
				<p class="boton">
					<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href='consultar.php'">Atras</button> <!-- BOTON PARA VOLVER A CONSULTAR -->
				</p>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
		<!-- BOTON PARA VOLVER A LA PARTE SUPERIOR  -->
	</section>
	<?php
	if (isset($_GET['error'])) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['error'] . '</div>';  // VENTANA MODAL ERROR
	elseif (isset($_GET['msj'])) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['msj'] . '</div>'; // VENTANA MODAL EXITOSO
	?>
</body>

</html>