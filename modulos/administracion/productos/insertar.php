<?php
// LA ACCION PRINCIPAL DE ESTA PAGINA ES INSERTAR UN NUEVO PRODUCTO 

$r = '../../../';
//INCLUIR SESION Y CONECCION
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
if (isset($_POST['insertar'])) { 
	// VALIDAMOS QUE POST INSERTAR SEA TRUE
	$id = trim(strtoupper($_POST['id']));
	$nombre = trim(strtoupper($_POST['nombre']));
	$desc = trim(strtoupper($_POST['desc']));
	$precio = $_POST['precio'];
	$tipo = $_POST['tipo'];
	$categoria = $_POST['categoria'];
	$editorial = trim(strtoupper($_POST['editorial']));
	// REALIZAMOS EL INSERT EN LA BD SEGUN LOS DATOS RECIBIDOS
	$qry = $db->query("INSERT INTO productos 
	(proid, pronombre, prodescr, proprecio, proestado, protipo, categoria, editorial) 
	VALUES ('$id','$nombre', '$desc', $precio, 'OK', '$tipo', '$categoria', '$editorial')") or die($db->errorInfo()[2]);	
	if ($qry) $mensaje = 'Se inserto el producto';  // MENSAJE MODAL EXITOSO
	else $error = 'No se pudo insertar el producto';  // MENSAJE MODAL ERROR
}
?>
<!doctype html>

<html lang="es">

<head>
    <title>AGREGAR PRODUCTO</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
		<?php require($r . 'incluir/src/menu.php') ?>
		<!-- INCLUIMOS MENU PRINCIPAL -->
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Administracion</a>
				<div class="mapa_div"></div><a class="current">Productos</a>
			</article>
			<article id="contenido">
				<div class="ui-widget">
					<form id="form" name="form" action="insertar.php" method="post">
						<!-- FORMULARIO ENVIADO POR POST A SI MISMO -->
						<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
							<legend class="ui-widget ui-widget-header ui-corner-all">Insertar producto</legend>
							<p>
								<label for="id">Codigo:</label><!-- CAMPO CODIGO -->
								<input type="text" name="id" class="referen validate[required, ajax[Id]] text-input" title="Digite el codigo" />
							</p>
							<p>
								<label for="nombre">Nombre:</label> <!-- CAMPO NOMBRE -->
								<input type="text" name="nombre" class="nombre validate[required, maxSize[50], ajax[ajaxNameCall]] text-input" title="Digite el nombre del producto" />
							</p>
							<p>
								<label for="desc">Descripcion:</label> <!-- CAMPO DESCRIPCION -->
								<input type="text" name="desc" class="validate[maxSize[100]]" style="text-transform: uppercase" title="Digite descripcion del producto" />
							</p>
							<p>
								<label for="precio">precio:</label> <!-- CAMPO PRECIO -->
								<input type="text" value="0" name="precio" class="valor validate[required, min[0], custom[onlyNumberSp]] text-input" title="Digite el precio del producto" style="text-align:left!important; padding-left:2.5em;" />
							</p>
							<p style="visibility: hidden;height: 0;">
								<label for="categoria">Categoria</label>
								<select class="" name="categoria">
									<option value="-1">SIN CATEGORIA</option>
									<?php 
									$categorias = $db->query("SELECT * FROM categorias WHERE id > 0 ORDER BY nombre asc");
									while ($categoria = $categorias->fetch(PDO::FETCH_ASSOC)) {
										echo '<option value="'.$categoria['id'].'">'.$categoria['nombre'].'</option>';
									}
									?>
								</select>
							</p>
							<p>
								<label for="editorial">Editorial</label>
								<input type="text" name="editorial" class="validate[maxSize[40]]" style="text-transform: uppercase" title="Digite la editorial" />
							</p>
							<p style="visibility: hidden;height: 0;">
								<label for="tipo">Tipo</label>
								<select class="validate[required]" name="tipo">
									<option value="PT">PRODUCTO TERMINADO</option>
									<option value="MP">MATERIA PRIMA</option>
								</select>
							</p>
							<p class="boton">
								<!-- BOTON INSERTAR -->
								<button type="submit" class="btn btn-primary btninsertar" name="insertar" value="insertar">Insertar</button>
							</p>
						</fieldset>
					</form>
				</div>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
		<!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
	</section>
	<?php
	if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>'; //VENTANA MODAL ERROR
	elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>'; //VENTANA MODAL EXITOSO
	?>
</body>

</html>