<?php
// LA ACCION PRINCIPAL DE ESTA PAGINA ES LISTAR O MODIFICAR LOS PARAMETROS

$r = '../../../';
//INCLUIR SESION Y CONECCION
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
if (isset($_POST["modificar"])) { // SE VALIDA SI MODIFICAR ES TRUE
	$ncuota = $_POST['ncuota']; // VARIABLE NUMERO DE CUOTAS
	$vcuota = $_POST['vcuota']; // VARIABLE VALOR DE CUOTA
	$financiacion = $_POST['financiacion'] / 100; // VARIABLE FINANCIACION

	$qry = $db->query("SELECT * FROM parametros");
	if($qry->rowCount() > 0)
		$qry = $db->query("UPDATE parametros SET parncuota = $ncuota, parvcuota = $vcuota, parfinanciacion = $financiacion"); // ACTUALIZAMOS LA INFORMACION CON LOS NUEVOS DATOS RECIBIDOS
	else
		$qry = $db->query("INSERT INTO parametros(parncuota, parvcuota, parfinanciacion) VALUES ($ncuota, $vcuota, $financiacion)"); // ACTUALIZAMOS LA INFORMACION CON LOS NUEVOS DATOS RECIBIDOS

	if ($qry) 
	{
		$mensaje = 'Se actualizaron los parametros'; //MENSAJE EXITOSO
		header('location:parametros.php?mensaje='.$mensaje);
	}
	else 
	{
		$error = 'No se actualizaron los parametros'; //MENSAJE ERROR
		header('location:parametros.php?error='.$error);
	}
}
$parametros = $db->query("SELECT * FROM parametros")->fetch(PDO::FETCH_ASSOC);
?>
<!doctype html>
<html>

<head>
	<meta charset="utf-8">

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>

</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
		<?php require($r . 'incluir/src/menu.php') ?>
		<!-- INCLUIMOS MENU PRINCIPAL -->
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Administracion</a>
				<div class="mapa_div"></div><a class="current">Parametros</a>
			</article>
			<article id="contenido">
				<div class="ui-widget">
					<form id="form" name="form" action="" method="post">
						<!-- FORMULARIO ENVIADO POR POST A SI MISMO  -->
						<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
							<legend class="ui-widget ui-widget-header ui-corner-all">Parametros Generales</legend>
							<p>
								<label for="ncuota">Numero de Cuotas Maximas:</label> <!-- CAMPO NUEMRO DE CUOTAS -->
								<input type="text" name="ncuota" class="cantidad validate[required, custom[onlyNumberSp]]" value="<?php echo @$parametros['parncuota']; ?>" />
							</p>
							<p>
								<label for="vcuota">Valor Cuota Minima:</label> <!-- CAMPO DE VALOR DE CUOTA MINIMA -->
								<input type="text" name="vcuota" class="valor validate[required, custom[onlyNumberSp]]" value="<?php echo @$parametros['parvcuota']; ?>" />
							</p>
							<p>
								<label for="financiacion">% de Financiación:</label> <!-- CAMPO % DE FINANCIACION -->
								<input type="text" name="financiacion" class="porcentaje validate[required, custom[number]]" value="<?php echo @$parametros['parfinanciacion'] * 100; ?>" />
							</p>
							<p class="boton">
								<button type="submit" class="btn btn-primary btnmodificar" name="modificar" value="modificar">Modificar</button> <!-- BOTON MODIFICAR  -->
							</p>
						</fieldset>
					</form>
				</div>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
		<!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
	</section>
	<?php
	if (isset($_GET['error'])) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0"></span>' . $_GET['error'] . '</div>'; // VENTANA MODAL ERROR
	elseif (isset($_GET['mensaje'])) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0"></span>' . $_GET['mensaje'] . '</div>'; // VENTANA MODAL EXITOSO
	?>
</body>

</html>