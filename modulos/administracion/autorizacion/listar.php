<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
require($r . 'incluir/funciones.php');

$trabajador = $autoriza = $fecha1 = $fecha2 = "";
if (isset($_POST['consultar'])) {
	$trabajador = $_POST['trabajador'];
	$autoriza = $_POST['autoriza'];
	$fecha1 = $_POST['fecha1'];
	$fecha2 = $_POST['fecha2'];
} else if(isset($_GET['trabajador'])){
	$trabajador = $_GET['trabajador'];
	$autoriza = $_GET['autoriza'];
	$fecha1 = $_GET['fecha1'];
	$fecha2 = $_GET['fecha2'];
}

$filtro = 'trabajador=' . $trabajador . '&autoriza=' . $autoriza . '&fecha1=' . $fecha1 . '&fecha2=' . $fecha2;
$con = 'SELECT * FROM autorizaciones ';
$ord = 'ORDER BY autoid desc';
if ($trabajador != '' && $autoriza == '' && $fecha1 == '') $sql = "$con WHERE autotrabajador ='$trabajador'  $ord";
elseif ($trabajador == '' && $autoriza != '' && $fecha1 == '') $sql = "$con WHERE autoautoriza ='$autoriza' $ord";
elseif ($trabajador == '' && $autoriza == '' && $fecha1 != '') $sql = "$con WHERE DATE(autoviaje) BETWEEN '$fecha1' AND '$fecha2'  $ord";
elseif ($trabajador != '' && $autoriza != '' && $fecha1 == '') $sql = "$con WHERE autotrabajador ='$trabajador' AND autoautoriza ='$autoriza' $ord";
elseif ($trabajador == '' && $autoriza != '' && $fecha1 != '') $sql = "$con WHERE autoautoriza ='$autoriza' AND DATE(autoviaje) BETWEEN '$fecha1' AND '$fecha2' $ord";
elseif ($trabajador != '' && $autoriza == '' && $fecha1 != '') $sql = "$con WHERE autotrabajador ='$trabajador' AND DATE(autoviaje) BETWEEN '$fecha1' AND '$fecha2' $ord";
elseif ($trabajador != '' && $autoriza != '' && $fecha1 != '') $sql = "$con WHERE autotrabajador ='$trabajador' AND autoautoriza ='$autoriza' AND DATE(autoviaje) BETWEEN '$fecha1' AND '$fecha2' $ord";
else $sql = "$con $ord";

$qry = $db->query($sql);

?>
<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>

	<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
	<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#tabla').dataTable({
				'bJQueryUI': true,
				'sPaginationType': 'full_numbers',
				'oLanguage': {
					'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
				},
				'bSort': false,
			});
			$('.pdf').click(function() {
				newSrc = $(this).attr('data-rel');
				$('#modal').html("<iframe src='" + newSrc + "' width='100%' height='100%'></iframe>");
				$('#modal').dialog({
					modal: true,
					width: '800',
					height: '900',
					title: 'PDF de la factura'
				});
			});
			$('.historico').click(function() {
				newHref = $(this).attr('data-rel');
				$('#modal').load(newHref).dialog({
					modal: true,
					width: 800
				});
			});
			$('.msj_anular').click(function(e) {
				e.preventDefault();
				var targetUrl = $(this).attr('href');
				var $dialog_link_follow_confirm = $('<div></div>').html("<p><span class='ui-icon ui-icon-alert' style='float:left; margin:2px 7px 7px 0;'></span>Esta seguro que desea anular la autorizacion?</p>").
				dialog({
					title: 'Porfavor confirmar',
					buttons: {
						'Si': function() {
							window.location.href = targetUrl;
						},
						'No': function() {
							$(this).dialog("close");
						}
					},
					modal: true,
					width: 'auto',
					height: 'auto'
				});
				$dialog_link_follow_confirm.dialog("open");
			});
		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a class="current">Solicitudes</a>
			</article>
			<article id="contenido">
				<h2>Listado de autorizaciones <?php if ($fecha1 != "") echo " desde " . $fecha1 . " al " . $fecha2 ?></h2>
				<table id="tabla">
					<thead>
						<tr>
							<th>Id</th>
							<th>Fecha</th>
							<th>Trabajador</th>
							<th>Empresa</th>
							<th>Autoriza</th>
							<th>Destino</th>
							<th>Viaja</th>
							<th>Regreso</th>
							<th>Transporte</th>
							<th>Motivo</th>
							<th>Observaciones</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php
						$i = 0;
						while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
							$autoriza = $trabajador = $empresa = "";

							$qryautoriza = $db->query("SELECT * FROM usuarios where usuid = '" . $row['autoautoriza'] . "'; ");
							$rowautoriza = $qryautoriza->fetch(PDO::FETCH_ASSOC);
							if($rowautoriza != null)
							$autoriza = $rowautoriza['usunombre'];

							$qrytrabajador = $db->query("SELECT * FROM usuarios where usuid = '" . $row['autotrabajador'] . "'; ");
							$rowtrabajador = $qrytrabajador->fetch(PDO::FETCH_ASSOC);
							if($rowtrabajador != null)
							$trabajador = $rowtrabajador['usunombre'];

							$qryempresa = $db->query("SELECT * FROM empresas where empid = '" . $row['autoempresa'] . "'; ");
							$rowempresa = $qryempresa->fetch(PDO::FETCH_ASSOC);
							if($rowempresa != null)
							$empresa = $rowempresa['empnombre'];


						?>
							<tr>
								<td><?php echo $row['autoid'] ?></td>
								<td align="center"><?php echo $row['autofecha'] ?></td>
								<td align="center"><?php echo $trabajador ?></td>
								<td><?php echo $empresa ?></td>
								<td align="center"><?php echo $autoriza ?></td>
								<td align="center"><?php echo $row['autodestino'] ?></td>
								<td align="center"><?php echo $row['autoviaje'] ?></td>
								<td align="center"><?php echo $row['autoregreso'] ?></td>
								<td align="center"><?php echo $row['autotransporte'] ?></td>
								<td align="center"><?php echo $row['automotivo'] ?></td>
								<td align="center"><?php echo $row['autoobservaciones'] ?></td>

								<td align="center"><img src="<?php echo $r ?>imagenes/iconos/pdf.png" class="pdf" data-rel="<?php echo $r . 'pdf/autorizaciones/' . $row['autoid'] . '.pdf' ?>" title="Comprobante" /></td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
				<p class="boton">
					<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href='consultar.php'">Atras</button>
				</p>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<div id="modal" style="display:none"></div>
</body>

</html>