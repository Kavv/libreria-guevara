<?php
$r = '../../../';
require($r . 'incluir/connection.php');
require($r . 'incluir/session.php');
include($r . 'incluir/fpdf/fpdf.php');

//$row = $db->query("SELECT * FROM autorizaciones LEFT JOIN usuarios ON usuid = autotrabajador LEFT JOIN empresas ON empid = autoempresa  ORDER BY autoid DESC LIMIT 1")->fetch(PDO::FETCH_ASSOC);
//Datos de la autorización
$row = $db->query("SELECT * FROM autorizaciones LEFT JOIN usuarios ON usuid = autotrabajador LEFT JOIN empresas ON empid = autoempresa  WHERE autoid = " . $_GET['id'])->fetch(PDO::FETCH_ASSOC);
//Nombre de quien autoriza
$autoriza = $db->query("SELECT usunombre FROM usuarios WHERE usuid = '". $row['autoautoriza']. "'")->fetch(PDO::FETCH_ASSOC);

$iden = $row['autoid'];
$pdf = new FPDF('P', 'mm', array(140, 216));
$pdf->AddFont('Calibri', '', 'calibri.php');
$pdf->AliasNbPages();
$pdf->AddPage();

if ($row['autoempresa'] == '900051528') {
	$nombre_fichero = $r . 'pdf/autorizaciones/900051528.jpg';
	if (file_exists($nombre_fichero)) {
		$pdf->Image($r . 'pdf/autorizaciones/900051528.jpg', 0, 0, 140, 216);
	}
} else if ($row['autoempresa'] == '900813686') {
	$nombre_fichero = $r . 'pdf/autorizaciones/900813686.jpg';
	if (file_exists($nombre_fichero)) {
		$pdf->Image($r . 'pdf/autorizaciones/900813686.jpg', 0, 0, 140, 218);
	}
}

$pdf->Ln(28);
$pdf->SetFont('Calibri', '', 11);
$pdf->Cell(0, 5, 'AUTORIZACION DE TRASLADO Y/O VISITA LABORAL', 0, 1, 'C');
$pdf->Ln(7);
$pdf->SetFont('Calibri', '', 9);
$pdf->Cell(30, 6, 'FECHA', 1, 0, 'C');
$pdf->Cell(60, 6, '', 0, 0, 'C');
$pdf->Cell(30, 6, 'CONSECUTIVO', 1, 1, 'C');

$pdf->Cell(30, 4, $row['autofecha'], 1, 0, 'C');
$pdf->Cell(60, 4, '', 0, 0, 'C');
$pdf->Cell(30, 4, 'No.' . $row['autoid'], 1, 1, 'C');

$pdf->Ln(7);

$pdf->Cell(35, 6, 'TRABAJADOR:', 0, 0, 'L');
$pdf->Cell(45, 6, utf8_decode($row['usunombre']), 0, 1, 'L');
$pdf->Cell(35, 6, 'EMPRESA VINCULACION:', 0, 0, 'L');
$pdf->Cell(65, 6, utf8_decode($row['empnombre']), 0, 1, 'L');
$pdf->Cell(35, 6, 'AUTORIZADO POR:', 0, 0, 'L');
$pdf->Cell(65, 6, $autoriza['usunombre'], 0, 1, 'L');

$pdf->Ln(4);

$pdf->SetFont('Calibri', '', 8);
$pdf->Cell(35, 6, 'Lugar Destino', 1, 0, 'C');
$pdf->Cell(25, 6, 'Fecha Viaje', 1, 0, 'C');
$pdf->Cell(25, 6, 'Fecha Regreso', 1, 0, 'C');
$pdf->Cell(35, 6, 'Medio Transporte', 1, 0, 'C');

$pdf->Ln(6);
$pdf->Cell(35, 4, utf8_decode($row['autodestino']), 1, 0, 'C');
$pdf->Cell(25, 4, $row['autoviaje'], 1, 0, 'C');
$pdf->Cell(25, 4, $row['autoregreso'], 1, 0, 'C');
$pdf->Cell(35, 4, utf8_decode($row['autotransporte']), 1, 0, 'C');


$pdf->SetFont('Calibri', '', 11);
$pdf->Ln(8);
$pdf->Cell(0, 5, 'MOTIVO DEL VIAJE', 0, 1, 'C');
$pdf->Ln(2);
$pdf->SetFont('Calibri', '', 9);
$pdf->MultiCell(122, 4, utf8_decode($row['automotivo']), 1, 'J');

$pdf->SetFont('Calibri', '', 11);
$pdf->Ln(1);
$pdf->Cell(0, 5, 'OBSERVACIONES ADICIONALES', 0, 1, 'C');
$pdf->Ln(2);
$pdf->SetFont('Calibri', '', 9);
$pdf->MultiCell(122, 4, utf8_decode($row['autoobservaciones']), 1, 'J');

$pdf->Ln(10);
$pdf->SetFont('Calibri', '', 9);
$pdf->Cell(30, 6, 'Firma Trabajador', 0, 0, 'C');
$pdf->Cell(10, 6, '', 0, 0, 'C');
$pdf->Cell(30, 6, 'Firma Quien Autoriza', 0, 0, 'C');
$pdf->Cell(10, 6, '', 0, 0, 'C');
$pdf->Cell(30, 6, 'Firma Quien Revisa', 0, 1, 'C');

$pdf->Ln(12);

$nombre_fichero = $r . 'imagenes/firmas/' . $row['autotrabajador'] . '.png';
if (file_exists($nombre_fichero)) {
	$pdf->Image($r . 'imagenes/firmas/' . $row['autotrabajador'] . '.png', 7, 141, 35, 15);
}
$pdf->Cell(30, 6, '__________________', 0, 0, 'C');
$pdf->Cell(10, 6, '', 0, 0, 'C');
$nombre_fichero1 = $r . 'imagenes/firmas/' . $row['autoautoriza'] . '.png';
if (file_exists($nombre_fichero1)) {
	$pdf->Image($r . 'imagenes/firmas/' . $row['autoautoriza'] . '.png', 47, 141, 35, 15);
}
$pdf->Cell(30, 6, '__________________', 0, 0, 'C');
$pdf->Cell(10, 6, '', 0, 0, 'C');

$nombre_fichero2 = $r . 'imagenes/firmas/' . $_SESSION['id'] . '.png';
if (file_exists($nombre_fichero2)) {
	$pdf->Image($r . 'imagenes/firmas/' . $_SESSION['id'] . '.png', 87, 141, 35, 15);
}
$pdf->Cell(30, 6, '__________________', 0, 1, 'C');


$pdf->Ln(3);
$pdf->SetFont('Calibri', '', 11);
$pdf->Output($r . 'pdf/autorizaciones/' . $iden . '.pdf');
$pdf->Output();
