<?php
// LA ACCION PRINCIPAL DE ESTA PAGINA ES INSERTAR UN NUEVO MODO DE DESPACHO

$r = '../../../';
//INCLUIR SESION Y CONECCION
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

if (isset($_POST['insertar'])) { // SE VALIDA SI LA INFORMACION VIENE DE INSERTAR 

	$trabajador = $_POST['trabajador'];
	$autoriza = $_POST['autoriza'];
	$destino = strtoupper(trim($_POST['destino']));
	$viaje = $_POST['viaje'];
	$regreso = $_POST['regreso'];
	$transporte = strtoupper(trim($_POST['transporte']));
	$motivo = strtoupper(trim($_POST['motivo']));
	$observaciones = strtoupper(trim($_POST['observaciones']));

	/* Que es directorio?que simboliza? */
	$qry1 = $db->query("SELECT * FROM directorio WHERE dircedula = '" . $trabajador . "';");
	$row1 = $qry1->fetch(PDO::FETCH_ASSOC);
	$empresa = $row1['dirempresa'];


	$qry = $db->query("INSERT INTO autorizaciones (autotrabajador, autoempresa, autoautoriza, autodestino, autoviaje, autoregreso, autotransporte, automotivo, autoobservaciones) VALUES ('$trabajador', '$empresa', '$autoriza', '$destino', '$viaje', '$regreso', '$transporte', '$motivo', '$observaciones');	"); // INSERTAMOS NUEVO USUARIO
	if ($qry) {
		$new_id = $db->lastInsertId();
		header('Location:finalizar.php?id='.$new_id);
	} else {
		$error = 'No se pudo insertar la autorizacion';
	}
}
?>
<!doctype html>
<html>

<head>

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<script type="text/javascript" src="<?php echo $r ?>incluir/funciones.js"></script>
	<script>
		$(document).ready(function() {
			$('#viaje').datepicker({
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
				minDate: '+0D',
				maxDate: '+50D',
			}).keypress(function(event) {
				event.preventDefault()
			});
			$('#regreso').datepicker({
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
				minDate: '+0D',
				maxDate: '+50D',
			}).keypress(function(event) {
				event.preventDefault()
			});
		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
		<?php require($r . 'incluir/src/menu.php') ?>
		<!-- INCLUIMOS MENU PRINCIPAL -->
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Administracion</a>
				<div class="mapa_div"></div><a class="current">Autorizaciones</a>
			</article>
			<article id="contenido">
				<div class="ui-widget">
					<form id="form" name="form" action="aut_salida.php" method="post">
						<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
							<legend class="ui-widget ui-widget-header ui-corner-all">Crear Nueva Autorizacion de Viaje</legend>

							<p>
								<label for="trabajador">Trabajador:</label> <!-- CAMPO TRABAJADOR -->
								<select name="trabajador" class="validate[required]">
									<option value="">SELECCIONE</option>
									<?php
									/* Los usuarios representan a los trabajadores */
									$qry = $db->query("SELECT * FROM usuarios ORDER BY usunombre");
									while ($row = $qry->fetch(PDO::FETCH_ASSOC))
										echo '<option value=' . $row['usuid'] . '>' . $row['usunombre'] . '</option>';
									?>
								</select>
							</p>

							<p>
								<label for="autoriza">Autorizado por:</label> <!-- CAMPO AUTORIZA -->
								<select name="autoriza" class="validate[required]">
									<option value="">SELECCIONE</option>
									<?php
									$qry = $db->query("SELECT * FROM usuarios  ORDER BY usunombre");
									while ($row = $qry->fetch(PDO::FETCH_ASSOC))
										echo '<option value=' . $row['usuid'] . '>' . $row['usunombre'] . '</option>';
									?>
								</select>
							</p>

							<p>
								<label for="destino">Destino del trabajador:</label> <!-- CAMPO DESTINO-->
								<input type="text" name="destino" class="validate[required] text-input" />
							</p>

							<p>
								<label for="Viaje">Fecha Viaje:</label>
								<input type="text" class="fecha validate[required]" id="viaje" name="viaje" />
							</p>
							<p>
								<label for="regreso">Fecha Regreso:</label>
								<input type="text" id="regreso" class="fecha" name="regreso" />
							</p>

							<p>
								<label for="transporte">Transporte:</label> <!-- CAMPO TRANSPORTE-->
								<input type="text" name="transporte" class="text-input" />
							</p>

							<p>
								<label for="motivo">Motivo del Viaje:</label> <!-- CAMPO MOTIVO-->
								<input type="text" name="motivo" class="text-input" />
							</p>

							<p>
								<label for="observaciones">Observaciones</label>
								<textarea name="observaciones" rows="5" class="validate[required] text-input form-control"></textarea>
							</p>

							<p class="boton">
								<button type="submit" class="btn btn-primary btninsertar" name="insertar" value="insertar">Generar PDF</button> <!-- BOTON INSERTAR -->
							</p>
						</fieldset>
					</form>
				</div>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<?php
	if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>'; //VENTANA MODAL ERROR
	?>
</body>

</html>