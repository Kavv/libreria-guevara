<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

?>
<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	?>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a class="current">Administracion</a>
			</article>
			<article id="contenido">
				<p align="center">
					<iframe src="pdf_autorizacion.php?id=<?php echo $_GET['id']?>" width="800" height="900"></iframe>
				</p>
				<p class="boton">
					<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href = 'aut_salida.php'">Atras</button>
				</p>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
</body>

</html>