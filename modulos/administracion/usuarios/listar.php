<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
if (isset($_POST['modificar'])) {
	$id = trim(strtoupper($_POST['id']));
	$nombre = trim(strtoupper($_POST['nombre']));
	$email = trim(strtolower($_POST['email']));
	$telefono = trim($_POST['telefono']);
	$direccion = trim($_POST['direccion']);

	$fechaingreso = $_POST['fechaingreso'];
	$fecharetiro = $_POST['fecharetiro'];
	$salario = $_POST['salario'];
	$auxilio = $_POST['auxilio'];
	$topeauxilio = $_POST['topeauxilio'];
	$empresa = $_POST['empresa'];
	$cargo = strtoupper(trim($_POST['cargo']));
	$modalidad = $_POST['modalidad'];

	$perfil = $_POST['perfil'];
	$depto = $_POST['depto'];
	$ciudad = $_POST['ciudad'];
	$sede = 1;
	$director = $directorcall = $asesor = $promotor = $callcenter = $relacionista = 0;
	if (isset($_POST['director']))
		$director = $_POST['director'];
	if (isset($_POST['directorcall']))
		$directorcall = $_POST['directorcall'];
	if (isset($_POST['asesor']))
		$asesor = $_POST['asesor'];
	if (isset($_POST['promotor']))
		$promotor = $_POST['promotor'];
	if (isset($_POST['callcenter']))
		$callcenter = $_POST['callcenter'];
	if (isset($_POST['relacionista']))
		$relacionista = $_POST['relacionista'];
	if(!isset($_POST['grupo'])) 
		$_POST['grupo'] = "";

	/* ESTO REALMENTE IMPORTA? */
	$grupo = $_POST['grupo'];
	$jefecall = 0; //$_POST['jefecall'];
	$jefeasesor = 0; //$_POST['jefeasesor'];

	if (!isset($_POST['password']))
		$qry = $db->query("UPDATE usuarios SET usunombre = '$nombre', usuperfil = $perfil, usudepto = '$depto', usuciudad = '$ciudad', ususede = $sede, usuemail = '$email', usutelefono = '$telefono', usudireccion = '$direccion', usudirector = '$director', usudirectorcall = '$directorcall', usuasesor = '$asesor', usupromotor = '$promotor', usuGrupo = '$grupo', usucallcenter = '$callcenter', usujefecallcenter = '$jefecall', usurelacionista = '$relacionista', usufechaingreso = '$fechaingreso', usufecharetiro = '$fecharetiro', ususalariocotizacion = '$salario', usuauxiliotransporte = '$auxilio', usutopeauxiliotransporte = '$topeauxilio', usuempresa = '$empresa', usucargo = '$cargo', usumodalidad = '$modalidad', usujefeasesor = '$jefeasesor' WHERE usuid = '$id'") or die($db->errorInfo()[2]);
	else {
		$password = sha1($_POST['password']);
		$qry = $db->query("UPDATE usuarios SET usupassword = '$password', usunombre = '$nombre', usuperfil = $perfil, usudepto = '$depto', usuciudad = '$ciudad', ususede = $sede, usuemail = '$email', usutelefono = '$telefono', usudireccion = '$direccion', usudirector = '$director',  usudirectorcall = '$directorcall', usuasesor = '$asesor', usupromotor = '$promotor', usuGrupo = '$grupo', usucallcenter = '$callcenter', usujefecallcenter = '$jefecall',  usurelacionista = '$relacionista', usufechaingreso = '$fechaingreso', usufecharetiro = '$fecharetiro', ususalariocotizacion = '$salario', usuauxiliotransporte = '$auxilio', usutopeauxiliotransporte = '$topeauxilio', usuempresa = '$empresa', usucargo = '$cargo', usumodalidad = '$modalidad', usujefeasesor = '$jefeasesor' WHERE usuid = '$id'");
	}
	if ($qry) $mensaje = 'Se actualizo el usuario';
	else $error = 'No se actualizo el usuario';
}
if (isset($_GET['id1'])) {
	$id1 = $_GET['id1'];
	$num = $db->query("SELECT * FROM hiscarteras WHERE hisusuario = '$id1'")->rowCount();
	if ($num < 1) {
		$num = $db->query("SELECT * FROM solicitudes WHERE solasesor like '$id1'")->rowCount();
		if ($num < 1) {
			$num = $db->query("SELECT * FROM carteras WHERE carpromotor = '$id1'")->rowCount();
			if ($num < 1) {
				$qry = $db->query("DELETE FROM usuarios WHERE usuid = '$id1'");
				if ($qry) $mensaje = 'Se elimino el usuario';
				else $error = 'No se pudo eliminar el usuario';
			} else $error = 'El usuario tiene cartera asignada';
		} else $error = 'El usuario tiene movimiento en historial de cartera';
	} else $error = 'El usuario posee solicitudes';
}
$id = $nombre = $perfil = "";
if (isset($_POST['consultar'])) {
	$id = strtoupper($_POST['id']);
	$nombre = strtoupper($_POST['nombre']);
	$perfil = $_POST['perfil'];
} else if (isset($_GET['id'])) {
	$id = strtoupper($_GET['id']);
	$nombre = strtoupper($_GET['nombre']);
	$perfil = $_GET['perfil'];
}

	$con = "SELECT * FROM ((usuarios LEFT JOIN perfiles ON usuperfil = perid) LEFT JOIN departamentos ON usudepto = depid) LEFT JOIN ciudades ON (usudepto = ciudepto AND usuciudad = ciuid)";

    /* Los parametros de la consulta sql se genera dinamicamente 
    en base a los datos recibidos para delimitar los resultados */
    $parameters = [];
    if($id != "")
        array_push($parameters, "usuid LIKE '%$id%'" );
    if($nombre != "")
        array_push($parameters, "usunombre LIKE '%$nombre%'" );
    if($perfil != "")
        array_push($parameters, "usuperfil = '$perfil'" );


    // Consulta base
    $sql = $con;
    foreach ($parameters as $index => $parameter) {
        // Se agregan los parametros del WHERE
        if($index == 0)
            $sql .= " WHERE " . $parameter;
        else
            $sql .= " AND " . $parameter;
	}
	

$filtro = 'id=' . $id . '&nombre=' . $nombre . '&perfil=' . $perfil;

$qry = $db->query($sql);
?>
<!doctype html>
<html lang="es">

<head>
    <title>LISTA DE USUARIOS</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>

	<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
	<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
	<script>
		$(document).ready(function() {
			$('#tabla').dataTable({
				'bJQueryUI': true,
				'sPaginationType': "full_numbers",
				'oLanguage': {
					'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
				},
				'aoColumnDefs': [{
					'bSortable': false,
					'aTargets': [6, 7, 8, 9, 10]
				}],
			});

			$('.confirmar').click(function(e) {
				e.preventDefault();
				var targetUrl = $(this).attr("href");
				var $dialog_link_follow_confirm = $('<div></div>').html('<p><span class="ui-icon ui-icon-trash" style="float:left; margin:2px 7px 7px 0;"></span>Esta seguro que desea eliminar el usuario?</p>').
				dialog({
					title: 'Porfavor confirmar',
					buttons: {
						'Si': function() {
							carga();
							window.location.href = targetUrl;
						},
						'No': function() {
							$(this).dialog("close");
						}
					},
					modal: true,
					width: 'auto',
					height: 'auto'
				});
				$dialog_link_follow_confirm.dialog('open');
			});
		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Administracion</a>
				<div class="mapa_div"></div><a class="current">Usuarios</a>
			</article>
			<article id="contenido">
				<h2>Listado de usuarios</h2>
				<div class="reporte">

				</div>
				<table id="tabla">
					<thead>
						<tr>
							<th>Identificacion</th>
							<th>Nombre</th>
							<th>Perfil</th>
							<th>Departamento</th>
							<th>Ciudad</th>
							<th>Sede</th>
							<th>Email</th>
							<th>Tel</th>
							<th>Dir</th>
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php
						while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
						?>
							<tr>
								<td><?php echo $row['usuid'] ?></td>
								<td><?php echo $row['usunombre'] ?></td>
								<td><?php echo $row['pernombre'] ?></td>
								<td><?php echo $row['depnombre'] ?></td>
								<td><?php echo $row['ciunombre'] ?></td>
								<td><?php echo 'NONE'; ?></td>
								<td align="center"><a href="mailto:<?php echo $row['usuemail'] ?>"><img src="<?php echo $r ?>imagenes/iconos/email.png" title="<?php echo $row['usuemail'] ?>" /></a></td>
								<td align="center"><a href="tel:<?php echo $row['usutelefono'] ?>"><img src="<?php echo $r ?>imagenes/iconos/telefono.png" title="<?php echo $row['usutelefono'] ?>" /></a></td>
								<td align="center"><img src="<?php echo $r ?>imagenes/iconos/casa.png" title="<?php echo $row['usudireccion'] ?>" /></td>
								<td align="center"><a href="modificar.php?<?php echo 'id1=' . $row['usuid'] . '&' . $filtro ?>" onClick="carga()" title="editar"><img src="<?php echo $r ?>imagenes/iconos/lapiz.png" class="grayscale" /></a></td>
								<td align="center"><a href="listar.php?<?php echo 'id1=' . $row['usuid'] . '&' . $filtro ?>" title="eliminar" class="confirmar"><img src="<?php echo $r ?>imagenes/iconos/basura.png" class="grayscale" /></a></td>
							</tr>
						<?php
						}
						?>
					</tbody>
				</table>
				<p class="boton">
					<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href='consultar.php'">Atras</button>
				</p>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<?php
	if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>';
	elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>';
	?>
</body>

</html>