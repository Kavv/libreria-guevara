<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

$codigo = $_GET['id1'];
$row = $db->query("SELECT * FROM  usuarios 
LEFT JOIN grupos ON usugrupo = grunombre WHERE usuid = '$codigo'")->fetch(PDO::FETCH_ASSOC);
$filtro = 'id=' . $_GET['id'] . '&nombre=' . $_GET['nombre'] . '&grupo=' . $_GET['grupo'];
?>
<!doctype html>
<html lang="es">

<head>
    <title>MODIFICAR COBRADOR</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">
	<meta charset="utf-8">
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Administracion</a>
				<div class="mapa_div"></div><a class="current">Cobrador</a>
			</article>
			<article id="contenido">
				<div class="ui-widget">
					<form id="form" name="form" action="listar_cobrador.php?<?php echo $filtro ?>" method="post">
						<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
							<legend class="ui-widget ui-widget-header ui-corner-all">Modificar Cobrador</legend>
							<p>
								<label for="id">Código:</label>
								<input type="text" name="id" class="id" value="<?php echo $row['usuid'] ?>" readonly />
							</p>
							<p>
								<label for="nombre">Nombre Completo:</label>
								<input type="text" name="nombre" class="nombre validate[required] text-input" value="<?php echo $row['usunombre'] ?>" title="Digite el nombre del usuario" />
							</p>

							<p>
								<label for="grupo">Asignar un grupo:</label>
								<select id="grupo" name="grupo" class="selectpicker" data-live-search="true" title="Seleccione un grupo" data-width="100%">
									<?php
									$qry = $db->query('SELECT * FROM grupos ORDER BY grunombre');
									while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
										if ($row['usugrupo'] == $row2['grunombre'])
											echo '<option selected value="' . $row2['grunombre'] . '">' . $row2['grunombre'] . '</option>';
										else
											echo '<option value="' . $row2['grunombre'] . '">' . $row2['grunombre'] . '</option>';
									}
									?>
								</select>
							</p>

							<p class="boton">
								<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href='listar_cobrador.php?<?php echo $filtro ?>'">atras</button>
								<button type="submit" class="btn btn-primary btnmodificar" name="modificar" value="modificar">Modificar</button>
							</p>
						</fieldset>
					</form>
				</div>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
</body>

</html>