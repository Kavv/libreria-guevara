<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
?>
<!doctype html>
<html lang="es">

<head>
    <title>CONSULAR ASESOR</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>

</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
		<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Administracion</a>
				<div class="mapa_div"></div><a class="current">Consultar Asesor</a>
			</article>
			<article id="contenido">
				<div class="ui-widget">
					<form id="form" name="form" action="listar_asesor.php" method="post">
						<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
							<legend class="ui-widget ui-widget-header ui-corner-all">Consultar Asesor</legend>
							<p>
								<label for="id">Usuario:</label>
								<input type="text" name="id" class="id text-input" title="Digite la identificacion" />
							</p>
							<p>
								<label for="nombre">Nombre:</label>
								<input type="text" name="nombre" class="nombre validate[custom[onlyLetterSp]] text-input" title="Digite el nombre del usuario" />
							</p>
							<p>
								<label for="nombre">Grupo:</label>
								<select id="grupo" name="grupo" class="selectpicker" data-live-search="true" title="Seleccione un grupo" data-width="100%">
									<option value="">SELECCIONE</option>
									<?php
										$qry = $db->query("SELECT * FROM grupos ORDER BY grunombre ASC");
										while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
											echo '<option value=' . $row['grunombre'] . '>' . $row['grunombre'] . '</option>';
										}
									?>
								</select>
							</p>
							<div class="row">
								<div class="col-md-12 col-lg-6">
									<button type="submit" class="btnconsulta btn btn-primary btn-block mt-2" name="consultar" value="Buscar">consultar</button> <!-- BOTON CONSULTAR -->
								</div>
								<div class="col-md-12 col-lg-6">
									<button type="submit" class="btnconsulta btn btn-primary btn-block mt-2" name="show-all" value="Buscar">Mostrar Todo</button>
								</div>
							</div>
						</fieldset>
					</form>
				</div>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
</body>

</html>