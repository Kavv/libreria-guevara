<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
if (isset($_POST['insertar'])) 
{
	$id = trim(strtoupper($_POST['id']));
	$password = sha1('Emundo_123456$#');
	$nombre = trim(strtoupper($_POST['nombre']));
	$empresa = '16027020274133';
	$cargo = 'COBRADOR DE CARTERAS';
	$perfil = -1;
	$grupo = strtoupper($_POST['grupo']);
    $digitador = $_SESSION['id'];
	
	$existe = $db->query("SELECT * FROM usuarios WHERE usuid = '$id'");
	if($existe->rowCount() > 0)
		$qry = $db->query("UPDATE usuarios SET usurelacionista = '1' WHERE usuid = '$id')"); 
	else
	{
		$qry = $db->query("INSERT INTO usuarios (usuid, usupassword, usunombre, usuempresa, usuperfil, usucargo, usugrupo, usurelacionista, digitador) 
		VALUES ('$id', '$password', '$nombre', '$empresa', '$perfil', '$cargo', '$grupo', '1', '$digitador')") or die( $db->errorInfo()[2]);
	}

	if ($qry) {
        $mensaje = 'Se inserto el cobrador';
        header("Location:agregar_cobrador.php?msj=$mensaje");
        exit();
    } else {
        $error = 'No se pudo insertar el cobrador';
        header("Location:agregar_cobrador.php?error=$error");
        exit();
    }
}
?>
<!doctype html>
<html lang="es">

<head>
    <title>AGREGAR COBRADOR</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">
	<?php 
		require($r . 'incluir/src/head.php');
		require($r . 'incluir/src/head-form.php');
	?>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

	
	</style>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Administracion</a>
				<div class="mapa_div"></div><a class="current">Cobrador</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="agregar_cobrador.php" method="post">
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">Insertar Cobrador</legend>
						<p>
							<label for="id"><spam style="color:red">*</spam>Código:</label>
							<input type="text" name="id" class="id validate[required, ajax[ajaxUserCall]] text-input" title="Digite la identificacion" />
						</p>
						<p>
							<label for="nombre"><spam style="color:red">*</spam>Nombre Completo:</label>
							<input type="text" style="width:400px" name="nombre" class="nombre validate[required] text-input" title="Digite el nombre del usuario" />
						</p>

						<p>
							<label for="promotor">Asignar un grupo:</label>
							<select id="grupo" name="grupo" class="selectpicker" data-live-search="true" title="Seleccione un grupo" data-width="100%">
								<option value="">SELECCIONE</option>
								<?php
									$qry = $db->query("SELECT * FROM grupos ORDER BY grunombre ASC");
									while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
										echo '<option value=' . $row['grunombre'] . '>' . $row['grunombre'] . '</option>';
									}
								?>
							</select>
						</p>
						<p class="boton">
							<button type="submit" class="btn btn-primary btninsertar" name="insertar" value="insertar">Insertar</button>
						</p>
					</fieldset>
				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<?php
	if(isset($_GET['error'])) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['error'] . '</div>';
	elseif(isset($_GET['msj'])) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['msj'] . '</div>';
	?>
</body>

</html>