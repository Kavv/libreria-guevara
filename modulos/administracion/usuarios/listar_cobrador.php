<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
if (isset($_POST['modificar'])) {
	$id = trim(strtoupper($_POST['id']));
	$nombre = trim(strtoupper($_POST['nombre']));
	$grupo = strtoupper($_POST['grupo']);
    $digitador = $_SESSION['id'];
	
	$filtro = 'id=' . $_GET['id'] . '&nombre=' . $_GET['nombre'] . '&grupo=' . $_GET['grupo'];
	
	$qry = $db->query("UPDATE usuarios SET usunombre = '$nombre', usugrupo = '$grupo' WHERE usuid = '$id'");

	if ($qry) {
        $mensaje = 'Se edito el cobrador';
        header("Location:listar_cobrador.php?msj=$mensaje&$filtro");
        exit();
    } else {
        $error = 'No se pudo editar el cobrador';
        header("Location:listar_cobrador.php?error=$error&$filtro");
        exit();
    }
}
if (isset($_GET['id1'])) {
	$id1 = $_GET['id1'];
	
	$num = $db->query("SELECT * FROM solicitudes WHERE solrelacionista = '$id1'")->rowCount();
	if ($num < 1) {
		$num = $db->query("SELECT * FROM movimientos WHERE movprefijo='RC' AND movcobrador = '$id1'")->rowCount();
		if ($num < 1) {
			
			$es_asesor = $db->query("SELECT * FROM usuarios WHERE usuid = '$id1' AND usuasesor != 0");
			if($es_asesor->rowCount() > 0)
				$qry = $db->query("UPDATE usuarios SET usurelacionista = 0 WHERE usuid = '$id1'");
			else
				$qry = $db->query("DELETE FROM usuarios WHERE usuid = '$id1'");
			if ($qry) $mensaje = 'Se elimino el cobrador';
			else $error = 'No se pudo eliminar el cobrador';
		} else $error = 'El cobrador tiene recibos cobrados a su nombre';
	} else $error = 'El cobrador posee solicitudes';

	$filtro = 'id=' . $_GET['id'] . '&nombre=' . $_GET['nombre'] . '&grupo=' . $_GET['grupo'];
	if(isset($error))
		header("Location:listar_cobrador.php?error=$error&$filtro");
	if(isset($mensaje))
		header("Location:listar_cobrador.php?msj=$mensaje&$filtro");

	exit();
}
$id = $nombre = $grupo = "";
if (isset($_POST['consultar'])) {
	$id = strtoupper($_POST['id']);
	$nombre = strtoupper($_POST['nombre']);
	$grupo = $_POST['grupo'];
} else if (isset($_GET['id'])) {
	$id = strtoupper($_GET['id']);
	$nombre = strtoupper($_GET['nombre']);
	$grupo = $_GET['grupo'];
}

	$con = "SELECT * FROM usuarios 
	LEFT JOIN departamentos ON usudepto = depid 
	LEFT JOIN ciudades ON (usudepto = ciudepto AND usuciudad = ciuid)";

    /* Los parametros de la consulta sql se genera dinamicamente 
    en base a los datos recibidos para delimitar los resultados */
    $parameters = [];
	array_push($parameters, "usurelacionista = '1'" );
    if($id != "")
        array_push($parameters, "usuid LIKE '%$id%'" );
    if($nombre != "")
        array_push($parameters, "usunombre LIKE '%$nombre%'" );
    if($grupo != "")
        array_push($parameters, "usugrupo = '$grupo'" );


    // Consulta base
    $sql = $con;
    foreach ($parameters as $index => $parameter) {
        // Se agregan los parametros del WHERE
        if($index == 0)
            $sql .= " WHERE " . $parameter;
        else
            $sql .= " AND " . $parameter;
	}
	

$filtro = 'id=' . $id . '&nombre=' . $nombre . '&grupo=' . $grupo;

$qry = $db->query($sql);
?>
<!doctype html>
<html lang="es">

<head>
    <title>LISTA DE COBRADORES</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>

	<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
	<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
	<script>
		$(document).ready(function() {
			$('#tabla').dataTable({
				'bJQueryUI': true,
				'sPaginationType': "full_numbers",
				'oLanguage': {
					'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
				}
			});

			$('.confirmar').click(function(e) {
				e.preventDefault();
				var targetUrl = $(this).attr("href");
				var $dialog_link_follow_confirm = $('<div></div>').html('<p><span class="ui-icon ui-icon-trash" style="float:left; margin:2px 7px 7px 0;"></span>Esta seguro que desea eliminar el cobrador?</p>').
				dialog({
					title: 'Porfavor confirmar',
					buttons: {
						'Si': function() {
							carga();
							window.location.href = targetUrl;
						},
						'No': function() {
							$(this).dialog("close");
						}
					},
					modal: true,
					width: 'auto',
					height: 'auto'
				});
				$dialog_link_follow_confirm.dialog('open');
			});
		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Administracion</a>
				<div class="mapa_div"></div><a class="current">Listado de Cobradores</a>
			</article>
			<article id="contenido">
				<h2>Listado de Cobradores</h2>
				<div class="reporte">

				</div>
				<table id="tabla">
					<thead>
						<tr>
							<th>Identificacion</th>
							<th>Nombre</th>
							<th>Grupo</th>
							<th>F.Digitado</th>
							<th>Digitador</th>
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php
						while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
							$originalDate = $row['created_at'];
							$newDate = date("Y-m-d h:i:s A", strtotime($originalDate));
						?>
							<tr>
								<td><?php echo $row['usuid'] ?></td>
								<td><?php echo $row['usunombre'] ?></td>
								<td><?php echo $row['usugrupo'] ?></td>
								<td><?php echo $newDate; ?></td>
								<td><?php echo $row['digitador'] ?></td>
								<td align="center"><a href="modificar_cobrador.php?<?php echo 'id1=' . $row['usuid'] . '&' . $filtro ?>" onClick="carga()" title="editar"><img src="<?php echo $r ?>imagenes/iconos/lapiz.png" class="grayscale" /></a></td>
								<td align="center"><a href="listar_cobrador.php?<?php echo 'id1=' . $row['usuid'] . '&' . $filtro ?>" title="eliminar" class="confirmar"><img src="<?php echo $r ?>imagenes/iconos/basura.png" class="grayscale" /></a></td>
							</tr>
						<?php
						}
						?>
					</tbody>
				</table>
				<p class="boton">
					<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href='consultar_cobrador.php'">Atras</button>
				</p>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<?php
		if(isset($_GET['error'])) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['error'] . '</div>';
		elseif(isset($_GET['msj'])) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['msj'] . '</div>';
	?>
</body>

</html>