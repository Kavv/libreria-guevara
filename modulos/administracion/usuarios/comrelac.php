<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
if (isset($_POST['actualizar'])) {
	$id = $_POST['relacionista'];
	$comision = $_POST['comision'];
	$jeferel = $_POST['jeferel'];
	$pjeferel = $_POST['pjeferel'];
	$qry = $db->query("UPDATE usuarios SET usucomision = '$comision', usujeferel = '$jeferel', usupjeferel = '$pjeferel' WHERE usuid = '$id'");
	if ($qry) $mensaje = 'Se actualizaron las comisiones';
	else $error = 'No se actualizar las comisiones';
}
?>
<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<script>
		$(document).ready(function() {
			$('#relacionista').change(function(event) {
				var id1 = $('#relacionista').find(':selected').val();
				$("#datos").load('relacionista.php?id1=' + id1);
			});
		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Administracion</a>
				<div class="mapa_div"></div><a class="current">Usuarios</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="comrelac.php" method="post">
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">Actualizar comisiones relacionista</legend>
						<p>
							<label for="relacionista">Relacionista: </label>
							<select id="relacionista" name="relacionista" class="nombre validate[required] text-input">
								<option value="">SELECCIONE</option>
								<?php
								$qry = $db->query("SELECT * FROM usuarios WHERE usurelacionista = '1' ORDER BY usunombre");
								while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
									echo '<option value=' . $row['usuid'] . '>' . $row['usunombre'] . '</option>';
								}
								?>
							</select>
						</p>
						<div id="datos"></div>
						<p class="boton">
							<button type="submit" class="btn btn-primary btninsertar" name="actualizar" value="actualizar">Actualizar</button>
						</p>
					</fieldset>
				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<?php
	if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>';
	elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>';
	?>
</body>

</html>