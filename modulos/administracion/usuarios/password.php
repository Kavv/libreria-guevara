<?php
$r = '../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
if($_POST['modificar']) {

	$numcaracteres = strlen($_POST['passnue']);
	if($numcaracteres > 8) {
	$passnue = sha1($_POST['passnue']);
	$qry = $db->query("UPDATE usuarios SET usupassword = '$passnue' WHERE usuid = '".$_SESSION['id']."'");
	if($qry) $mensaje = 'Se modifico el password ';
	else $error = 'No se modifico el password';
	
	//Log de informacion que se le envia a American Logistic 
	$qrylogsregister = $db->query("SELECT * FROM usuarios INNER JOIN perfiles ON usuperfil = perid WHERE usuid = ".$_SESSION['id']); //verificacion usuario por ID de sesion
	$rowlogsregister = $qrylogsregister->fetch(PDO::FETCH_ASSOC);	
	$qrylogsregister = $db->query("INSERT INTO logs ( usuario, id_usuario , ip_usuario , accion_usuario, fecha_accion) VALUES ( '".$rowlogsregister['usunombre']."', '".$_SESSION['id']."', '".$_SERVER['REMOTE_ADDR']."' , 'CAMBIO LA CONTRASEÑA DE ".$_POST['passant']."  A  ".$_POST['passnue']." ', '".date("Y-m-d H:i:s")."' );"); //anexo de informacion de accion a la BD tabla LOGS
	} else {
	$error = 'La contraseña nueva debe de tener minimo 9 caracteres entre numeros y letras';
	}

}
?>
<!doctype html>
<html>
<head>
	<?php 
		require($r . 'incluir/src/head.php');
		require($r . 'incluir/src/head-form.php');
	?>
<script>
function notEqual(field){
    if(field.val() == $('#passant').val()){
        return '* Los passwords deben ser diferentes';
    }
}
$(document).ready(function() {
	$('.btnmodificar').button({ icons: { primary: 'ui-icon ui-icon-pencil' }});
	$('#form').validationEngine({
		showOneMessage:true,
		onValidationComplete: function(form, status){
        	if(status) {               
            	carga();
            	return true;
			}
		}
	});
	$('#dialog-message').dialog({
		height: 80,
		width: 'auto',
		modal: true
	});
});
</script>
<style type="text/css">
#form { width:340px; margin:5px auto }
#form fieldset { padding:10px; display:block }
#form legend { font-weight: bold; margin-left:5px; padding:5px }
#form label { display:inline-block; width:150px; text-align:right; margin:0.3em 2% 0 0 }
#form p { margin:5px 0 }
</style>
</head>
<body>
<?php require($r.'incluir/src/login.php') ?>
<section id="principal">
<?php require($r.'incluir/src/cabeza.php') ?>
<?php require($r.'incluir/src/menu.php') ?>
<article id="cuerpo">
<article class="mapa">
<a href="#">Principal</a><div class="mapa_div"></div><a href="#">Administracion</a><div class="mapa_div"></div><a class="current">Usuarios</a>
</article>
<article id="contenido">
<form id="form" name="form" action="" method="post">
<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
<legend class="ui-widget ui-widget-header ui-corner-all">Modificar password</legend>
<p>
<label for="passant">Password actual:</label>
<input type="password" id="passant" name="passant" class="password validate[required, ajax[ajaxPassCall]] text-input" title="Digite el password actual" />
</p>

<p>
<label for="passnue">Password nuevo:</label>
<input type="password" id="passnue" name="passnue" class="password validate[required, funcCall[notEqual]] text-input" title="Digite el password nuevo" />
</p>
<p>
<label for="passconf">Confirmar password:</label>
<input type="password" id="passconf" name="passconf" class="password validate[required, equals[passnue]] text-input" title="Confirme el password" />
</p>
<p class="boton">
<button type="submit" class="btn btn-primary btnmodificar" name="modificar" value="modificar">Modificar</button>
</p>
</fieldset>
</form>
</article>
</article>
<?php require($r.'incluir/src/pie.php') ?>
</section>
<?php
if(isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>'.$error.'</div>';
elseif(isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>'.$mensaje.'</div>';
?>
</body>
</html>