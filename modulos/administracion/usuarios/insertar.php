<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
if (isset($_POST['insertar'])) 
{
	$id = trim(strtoupper($_POST['id']));
	$password = sha1($_POST['password']);
	$nombre = trim(strtoupper($_POST['nombre']));
	$email = trim(strtolower($_POST['email']));
	$telefono = trim($_POST['telefono']);
	$direccion = trim($_POST['direccion']);

	$fechaingreso = $_POST['fechaingreso'];
	$fecharetiro = $_POST['fecharetiro'];
	$salario = $_POST['salario'];
	$auxilio = $_POST['auxilio'];
	$topeauxilio = $_POST['topeauxilio'];
	$empresa = $_POST['empresa'];
	$cargo = strtoupper(trim($_POST['cargo']));
	$modalidad = $_POST['modalidad'];
	
	$perfil = $_POST['perfil'];
	$depto = $_POST['depto'];
	$ciudad = $_POST['ciudad'];
	$sede = $_POST['sede'];
	$director = $directorcall = $asesor = $promotor = $callcenter = $relacionista = 0;
	if(isset($_POST['director'])) 
		$director = $_POST['director'];
	if(isset($_POST['directorcall'])) 
		$directorcall = $_POST['directorcall'];
	if(isset($_POST['asesor'])) 
		$asesor = $_POST['asesor'];
	if(isset($_POST['promotor'])) 
		$promotor = $_POST['promotor'];
	if(isset($_POST['callcenter'])) 
		$callcenter = $_POST['callcenter'];
	if(isset($_POST['relacionista'])) 
		$relacionista = $_POST['relacionista'];
	if(!isset($_POST['grupo'])) 
		$_POST['grupo'] = "";
	if(!isset($_POST['jefecall'])) 
		$_POST['jefecall'] = "";
	if(!isset($_POST['jefeasesor'])) 
		$_POST['jefeasesor'] = "";

	$grupo = $_POST['grupo'];
	$jefecall = $_POST['jefecall'];
	$jefeasesor = $_POST['jefeasesor'];
	
	$qry = $db->query("INSERT INTO usuarios (usuid, usupassword, usunombre, usuperfil, usudepto, usuciudad, ususede, usuemail, usutelefono, usudireccion, usudirector, usudirectorcall, usuasesor, usupromotor, usuGrupo, usurelacionista, usucallcenter, usujefecallcenter, usufechaingreso, usufecharetiro, ususalariocotizacion, usuauxiliotransporte, usutopeauxiliotransporte, usuempresa, usucargo, usumodalidad, usujefeasesor) VALUES ('$id', '$password', '$nombre', $perfil, '$depto', '$ciudad', '$sede', '$email', '$telefono', '$direccion', '$director', '$directorcall', '$asesor', '$promotor', '$grupo', '$relacionista', '$callcenter', '$jefecall', '$fechaingreso', '$fecharetiro', '$salario', '$auxilio', '$topeauxilio', '$empresa', '$cargo', '$modalidad', '$jefeasesor')");
	
	if ($qry) $mensaje = 'Se inserto el usuario';
	else $error = 'No se pudo insertar el usuario';
}
?>
<!doctype html>
<html lang="es">

<head>
    <title>AGREGAR USUARIO</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">
	<?php 
		require($r . 'incluir/src/head.php');
		require($r . 'incluir/src/head-form.php');
	?>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

	<script>
		$(document).ready(function() {

			$('#fechaingreso').datepicker({
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
				maxDate: '+0D',
				onClose: function(selectedDate) {
					$('#fecharetiro').datepicker('option', 'minDate', selectedDate);
				}
			});
			$('#fecharetiro').datepicker({
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
				maxDate: '+0D',
				onClose: function(selectedDate) {
					$('#fechaingreso').datepicker('option', 'maxDate', selectedDate);
				}
			});
			$('#depto').change(function(event) {
				var id1 = $('#depto').find(':selected').val();
				$('#ciudad').load('<?php echo $r ?>incluir/carga/ciudades.php?id1=' + id1);
				$('#sede').load('<?php echo $r ?>incluir/carga/sedes.php');
			});
			$('#ciudad').change(function(event) {
				var id1 = $('#depto').find(':selected').val();
				var id2 = $('#ciudad').find(':selected').val();
				$('#sede').load('<?php echo $r ?>incluir/carga/sedes.php?id1=' + id1 + '&id2=' + id2);
			});
			$('#asesor').click(function() {
				if ($('#asesor').is(':checked'))
					$('#grupo').prop('disabled', false)
				else $('#grupo').prop('disabled', true)
			});
			$('#callcenter').click(function() {
				if ($('#callcenter').is(':checked'))
					$('#jefecall').prop('disabled', false)
				else $('#jefecall').prop('disabled', true)
			});
			/* $('#asesor').click(function() {
				if ($('#asesor').is(':checked'))
					$('#jefeasesor').prop('disabled', false)
				else $('#jefeasesor').prop('disabled', true)
			}); */
		});
	</script>
	<style type="text/css">
	
	</style>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Administracion</a>
				<div class="mapa_div"></div><a class="current">Usuarios</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="insertar.php" method="post">
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">Insertar usuario</legend>
						<p>
							<label for="id"><spam style="color:red">*</spam>Código:</label>
							<input autocomplete="off" type="text" name="id" class="id validate[required, ajax[ajaxUserCall]] text-input" title="Digite la identificacion" />
						</p>
						<p>
							<label for="password"><spam style="color:red">*</spam>Contraseña:</label>
							<input autocomplete="off" type="password" name="password" class="password validate[required] text-input" title="Digite el password" />
						</p>
						<p>
							<label for="nombre"><spam style="color:red">*</spam>Nombre Completo:</label>
							<input type="text" style="width:400px" name="nombre" class="nombre validate[required] text-input" title="Digite el nombre del usuario" />
						</p>
						<p>
							<label for="email">E-mail:</label>
							<input type="text" name="email" class="email validate[custom[email]] text-input" />
						</p>
						<p>
							<label for="telefono">Telefono:</label>
							<input type="text" name="telefono" class="telefono text-input" />
						</p>
						<p>
							<label for="direccion">Direccion:</label>
							<input type="text" name="direccion" class="direccion  text-input" />
						</p>
						
						<p>
							<label for="depto">Departamentos:</label>
							<select id="depto" name="depto" class="text-input">
								<option value="">SELECCIONE</option>
								<?php
								$qry = $db->query("SELECT * FROM departamentos ORDER BY depnombre");
								while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
									echo '<option value=' . $row['depid'] . '>' . $row['depnombre'] . '</option>';
								}
								?>
							</select>
						</p>
						<p>
							<label for="ciudad">Ciudad:</label>
							<select id="ciudad" name="ciudad" class="text-input">
								<option value="">SELECCIONE</option>
								<?php
								$qry = $db->query("SELECT * FROM ciudades ORDER BY ciunombre");
								while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
									echo '<option value=' . $row['ciuid'] . '>' . $row['ciunombre'] . '</option>';
								}
								?>
							</select>
						</p>
						<div style="visibility: hidden;height: 0;">
							<select type='hidden' id="sede" name="sede" class="text-input">
								<option value="">SELECCIONE</option>
								<?php
								$qry = $db->query("SELECT * FROM sedes ORDER BY sednombre");
								while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
									echo '<option value=' . $row['sedid'] . '>' . $row['sednombre'] . '</option>';
								}
								?>
							</select>
						</div>
						<div style="visibility: hidden;height: 0;">
							<select name="empresa" class="text-input" type='hidden'>
								<?php
								$qryem = $db->query("SELECT * FROM empresas ORDER BY empnombre");
								while ($rowem = $qryem->fetch(PDO::FETCH_ASSOC)) {
									echo '<option value=' . $rowem['empid'] . '>' . $rowem['empnombre'] . '</option>';
								}
								?>
							</select>
						</div>
						<p>
							<label for="cargo">Cargo:</label> <!-- CAMPO CARGO-->
							<input type="text" name="cargo" class="text-input" />
						</p>
						<p>
							<label for="perfil"><spam style="color:red">*</spam>Perfil:</label>
							<select id="perfil" name="perfil" class="validate[required] selectpicker" data-live-search="true" title="Seleccione el perfil" data-width="100%">
								<?php
								$qry = $db->query("SELECT * FROM perfiles ORDER BY pernombre");
								while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
									echo '<option value=' . $row['perid'] . '>' . $row['pernombre'] . '</option>';
								}
								?>
							</select>
						</p>
						
						<p>
							<label for="fechaingreso">Fecha de Ingreso:</label>
							<input type="text" name="fechaingreso" id="fechaingreso" class="text-input fecha" />
						</p>
							<input type="hidden" name="fecharetiro" id="fecharetiro" class="text-input fecha" />
							<input type='hidden' name='salario' id='salario' class='valor validate[custom[onlyNumberSp], min[0], required] text-input' style="text-align:left!important; padding-left:2.5em;" value="0"/>
							<input type='hidden' name='auxilio' id='auxilio' class='valor validate[custom[onlyNumberSp], min[0], required] text-input' style="text-align:left!important; padding-left:2.5em;" value="0"/>
							<input type='hidden' name='topeauxilio' id='topeauxilio' class='valor validate[custom[onlyNumberSp], min[0], required] text-input' style="text-align:left!important; padding-left:2.5em;" value="0"/>

							<div style="visibility: hidden;height: 0;">
							<select type='hidden' id="modalidad" name="modalidad" class="text-input">
								<option value=''>SELECCIONE</option>
								<option value='PRESENCIAL'>PRESENCIAL</option>
								<option value='TELETRABAJO'>TELETRABAJO</option>
							</select>
							</div>
							<input type="hidden" name="director" value="1" style="width:auto!important;"/>
							<input type="hidden" name="directorcall" value="1" style="width:auto!important;" />
							<input type="hidden" id="promotor" name="promotor" value="1" style="width:auto!important;" />
							<input type="hidden" id="callcenter" name="callcenter" value="1" style="width:auto!important;" />
							


						<p>
							<label for="relacionista" style="width:auto!important;">Cobrador:</label>
							<input type="checkbox" id="relacionista" name="relacionista" value="1" style="width:auto!important;" />
							
						</p>
						<p>
							<label for="asesor" style="width:auto!important;">Asesor de ventas:</label>
							<input type="checkbox" id="asesor" name="asesor" value="1" style="width:auto!important;" />
							<label for="promotor">Asignar un grupo:</label>
							<select id="grupo" name="grupo" class="selectpicker validate[required]" data-live-search="true" title="Seleccione un grupo" data-width="100%">
								<option value="">SELECCIONE</option>
								<?php
									$qry = $db->query("SELECT * FROM grupos ORDER BY grunombre ASC");
									while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
										echo '<option value=' . $row['grunombre'] . '>' . $row['grunombre'] . '</option>';
									}
								?>
							</select>
							<em style="font-size:13px">Debe seleccionar la opcion cobrador para asignar un grupo</em>
						</p>
						<p class="boton">
							<button type="submit" class="btn btn-primary btninsertar" name="insertar" value="insertar">Insertar</button>
						</p>
					</fieldset>
				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<?php
	if(isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>';
	elseif(isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>';
	?>
</body>

</html>