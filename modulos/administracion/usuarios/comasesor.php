<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
if (isset($_POST['actualizar'])) {
	$id = $_POST['asesor'];
	$comision = $_POST['comision'];
	$comision1 = $_POST['comision1'];
	$comision2 = $_POST['comision2'];
	$comision3 = $_POST['comision3'];
	$over1 = $_POST['over1'];
	$pover1 = $_POST['pover1'];
	$over2 = $_POST['over2'];
	$pover2 = $_POST['pover2'];
	$over3 = $_POST['over3'];
	$pover3 = $_POST['pover3'];
	$over4 = $_POST['over4'];
	$pover4 = $_POST['pover4'];
	$qry = $db->query("UPDATE usuarios SET usucomision = '$comision', usucomision1 = '$comision1', usucomision2 = '$comision2', usucomision3 = '$comision3', usuover1 = '$over1', usupover1 = '$pover1', usuover2 = '$over2', usupover2 = '$pover2', usuover3 = '$over3', usupover3 = '$pover3', usuover4 = '$over4', usupover4 = '$pover4' WHERE usuid = '$id'");
	if ($qry) $mensaje = 'Se actualizaron las comisiones';
	else $error = 'No se actualizar las comisiones';
}
?>
<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<script>
		$(document).ready(function() {
			$('#asesor').change(function(event) {
				var id1 = $('#asesor').find(':selected').val();
				$("#datos").load('asesor.php?id1=' + id1);
			});
		});
	</script>
	<style type="text/css">
	</style>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Administracion</a>
				<div class="mapa_div"></div><a class="current">Usuarios</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="comasesor.php" method="post">
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">Actualizar comisiones asesor</legend>
						<p>
							<label for="asesor">Asesor: </label>
							<select id="asesor" name="asesor" class="nombre validate[required] text-input">
								<option value="">SELECCIONE</option>
								<?php
								$qry = $db->query("SELECT * FROM usuarios WHERE usuasesor = '1' ORDER BY usunombre");
								while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
									echo '<option value=' . $row['usuid'] . '>' . $row['usunombre'] . '</option>';
								}
								?>
							</select>
						</p>
						<div id="datos"></div>
						<p class="boton">
							<button type="submit" class="btn btn-primary btninsertar" name="actualizar" value="actualizar">Actualizar</button>
						</p>
					</fieldset>
				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<?php
	if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>';
	elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>';
	?>
</body>

</html>