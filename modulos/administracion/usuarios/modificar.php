<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

$codigo = $_GET['id1'];
$row = $db->query("SELECT * FROM 
(
	(
		(
			(
				usuarios LEFT JOIN perfiles ON usuperfil = perid
			) 
			LEFT JOIN departamentos ON usudepto = depid
		) 
		LEFT JOIN ciudades ON 
		(
			usudepto = ciudepto AND usuciudad = ciuid
		)
	) 
) LEFT JOIN grupos ON usuGrupo = grunombre WHERE usuid = '$codigo'")->fetch(PDO::FETCH_ASSOC);
$filtro = 'id=' . $_GET['id'] . '&nombre=' . $_GET['nombre'] . '&perfil=' . $_GET['perfil'];
?>
<!doctype html>
<html lang="es">

<head>
    <title>MODIFICAR USUARIOS</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">
	<meta charset="utf-8">
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

	<script>
		$(document).ready(function() {

			$('#fechaingreso').datepicker({
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
				maxDate: '+0D',
				onClose: function(selectedDate) {
					$('#fecharetiro').datepicker('option', 'minDate', selectedDate);
				}
			});
			$('#fecharetiro').datepicker({
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
				maxDate: '+0D',
				onClose: function(selectedDate) {
					$('#fechaingreso').datepicker('option', 'maxDate', selectedDate);
				}
			});
			$('#depto').change(function(event) {
				var id = $('#depto').find(':selected').val();
				$('#ciudad').load('<?php echo $r ?>incluir/carga/ciudades.php?id1=' + id);
				$('#sede').load('<?php echo $r ?>incluir/carga/sedes.php');
			});
			$('#ciudad').change(function(event) {
				var id = $('#depto').find(':selected').val();
				var id2 = $('#ciudad').find(':selected').val();
				$('#sede').load('<?php echo $r ?>incluir/carga/sedes.php?id=' + id + '&id2=' + id2);
			});
			$('#asesor').click(function() {
				if ($('#asesor').is(':checked'))
					$('#grupo').prop('disabled', false)
				else $('#grupo').prop('disabled', true)
			});
			$('#callcenter').click(function() {
				if ($('#callcenter').is(':checked'))
					$('#jefecall').prop('disabled', false)
				else $('#jefecall').prop('disabled', true)
			});
			/* $('#asesor').click(function() {
				if ($('#asesor').is(':checked'))
					$('#jefeasesor').prop('disabled', false)
				else $('#jefeasesor').prop('disabled', true)
			}); */

		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Administracion</a>
				<div class="mapa_div"></div><a class="current">Usuarios</a>
			</article>
			<article id="contenido">
				<div class="ui-widget">
					<form id="form" name="form" action="listar.php?<?php echo $filtro ?>" method="post">
						<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
							<legend class="ui-widget ui-widget-header ui-corner-all">Modificar usuario</legend>
							<p>
								<label for="id">Código:</label>
								<input type="text" name="id" class="id" value="<?php echo $row['usuid'] ?>" readonly />
							</p>
							<p>
								<label for="password">Contraseña:</label>
								<input type="password" name="password" id="password" class="password validate[required] text-input" title="Digite el password" disabled />
								<label class="not-w" for="password">Cambiar contraseña:</label>
								<input type="checkbox" value="1" onclick="document.getElementById('password').disabled = !this.checked" />
							</p>
							<p>
								<label for="nombre">Nombre Completo:</label>
								<input type="text" name="nombre" class="nombre validate[required] text-input" value="<?php echo $row['usunombre'] ?>" title="Digite el nombre del usuario" />
							</p>
							<p>
								<label for="email">E-mail:</label>
								<input type="text" name="email" class="email validate[custom[email]] text-input" value="<?php echo $row['usuemail'] ?>" />
							</p>
							<p>
								<label for="telefono">Telefono:</label>
								<input type="text" name="telefono" class="telefono validate[custom[phone]] text-input" value="<?php echo $row['usutelefono'] ?>" />
							</p>
							<p>
								<label for="direccion">Direccion:</label>
								<input type="text" name="direccion" class="direccion text-input" value="<?php echo $row['usudireccion'] ?>" />
							</p>
							
							<p>
								<label for="depto">Departamento:</label>
								<select id="depto" name="depto" class=" text-input">
									<option value="<?php echo $row['usudepto'] ?>"><?php echo $row['depnombre'] ?></option>
									<?php
									$qry = $db->query("SELECT * FROM departamentos WHERE depid <> '" . $row['usudepto'] . "' ORDER BY depnombre");
									while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
										echo '<option value=' . $row2['depid'] . '>' . $row2['depnombre'] . '</option>';
									}
									?>
								</select>
							</p>
							<p>
								<label for="ciudad">Ciudad:</label>
								<select id="ciudad" name="ciudad" class=" text-input">
									<option value="<?php echo $row['usuciudad'] ?>"><?php echo $row['ciunombre'] ?></option>
									<?php
									$depto = $row['usudepto'] ;
									$ciu = $row['usuciudad'] ;
									$qry = $db->query("SELECT * FROM ciudades WHERE ciudepto = '$depto' AND ciuid <> '$ciu' ");
									
									while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
										echo '<option value="' . $row2['ciuid'] . '">' . $row2['ciunombre'] . '</option>';
									}
									?>
								</select>
							</p>
							
							<div style="visibility: hidden;height: 0;">
								<label for="sedes">Sede:</label>
								<select id="sede" name="sede" class="text-input">
									<option value="">SELECCIONE</option>
									<?php
									
									$sede = $row['ususede'] ;

									$qry = $db->query("SELECT * FROM sedes ORDER BY sednombre");
									while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
										if($sede == $row['sede'])
										echo '<option active value=' . $row2['sedid'] . '>' . $row2['sednombre'] . '</option>';
										else
										echo '<option value=' . $row2['sedid'] . '>' . $row2['sednombre'] . '</option>';
									}
									?>
								</select>
							</div>

							<div style="visibility: hidden;height: 0;">
								<label for="empresa">Empresa:</label>
								<select name="empresa" class="text-input">
									<?php
									if ($row['usuempresa'] != '') {
										$qryemp = $db->query("SELECT * FROM empresas WHERE empid = '" . $row['usuempresa'] . "' ORDER BY empnombre");
										while ($rowemp = $qryemp->fetch(PDO::FETCH_ASSOC))
											echo '<option value=' . $rowemp['empid'] . '>' . $rowemp['empnombre'] . '</option>';
									}
									$qryem = $db->query("SELECT * FROM empresas WHERE empid <> '" . $row['usuempresa'] . "' ORDER BY empnombre");
									while ($rowem = $qryem->fetch(PDO::FETCH_ASSOC)) {
										echo '<option value=' . $rowem['empid'] . '>' . $rowem['empnombre'] . '</option>';
									}
									?>
								</select>
								</div>
							<p>
								<label for="cargo">Cargo:</label> <!-- CAMPO CARGO-->
								<input type="text" name="cargo" value="<?php echo $row['usucargo'] ?>" class="text-input" />
							</p>

							<p>
								<label for="perfil">Perfil:</label>
								<select id="perfil" name="perfil" class="validate[required] selectpicker" data-live-search="true" title="Seleccione el perfil" data-width="100%">
									<?php
										$qry = $db->query("SELECT * FROM perfiles ORDER BY pernombre");
										while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
											if($row2['perid'] == $row['usuperfil'])
												echo '<option selected value=' . $row2['perid'] . '>' . $row2['pernombre'] . '</option>';
											else
												echo '<option value=' . $row2['perid'] . '>' . $row2['pernombre'] . '</option>';
										}
									?>
								</select>
							</p>

							<p>
								<label for="fechaingreso">Fecha de Ingreso:</label>
								<input type="text" name="fechaingreso" id="fechaingreso" value="<?php echo $row['usufechaingreso'] ?>" class="text-input fecha" />
							</p>
								<input type="hidden" name="fecharetiro" id="fecharetiro" value="<?php echo $row['usufecharetiro'] ?>" class="text-input fecha" />
								<input type='hidden' name='salario' id='salario' style="text-align:left!important; padding-left:2.5em;" value="<?php if($row['ususalariocotizacion'] != '') { echo $row['ususalariocotizacion']; } else { echo "0";} ?>" class='valor validate[custom[onlyNumberSp], min[0], required] text-input' />
								<input type='hidden' name='auxilio' id='auxilio' style="text-align:left!important; padding-left:2.5em;" value="<?php if($row['usuauxiliotransporte'] != '') { echo $row['usuauxiliotransporte']; } else { echo "0";} ?>" class='valor validate[custom[onlyNumberSp], min[0], required] text-input' />
								<input type='hidden' name='topeauxilio' id='topeauxilio' style="text-align:left!important; padding-left:2.5em;" value="<?php if($row['usutopeauxiliotransporte'] != '') { echo $row['usutopeauxiliotransporte']; } else { echo "0";} ?>" class='valor validate[custom[onlyNumberSp], min[0], required] text-input' />

							<div style="visibility: hidden;height: 0;">
								<label for="modalidad">Modalidad:</label>
								<select id="modalidad" name="modalidad" class="text-input">
									<?php if ($row['usumodalidad'] == 'PRESENCIAL') { ?>
										<option value='PRESENCIAL'>PRESENCIAL</option>
										<option value='TELETRABAJO'>TELETRABAJO</option>
									<?php } else if ($row['usumodalidad'] == 'TELETRABAJO') { ?>
										<option value='TELETRABAJO'>TELETRABAJO</option>
										<option value='PRESENCIAL'>PRESENCIAL</option>
									<?php } else { ?>
										<option value=''>SELECCIONE</option>
										<option value='PRESENCIAL'>PRESENCIAL</option>
										<option value='TELETRABAJO'>TELETRABAJO</option>
									<?php } ?>
								</select>
							</div>
							
								<?php if ($row['usudirector'] == 0) echo '<input type="hidden" name="director" value="1" />';
								else echo '<input type="hidden" name="director" value="1" checked />' ?>
								<?php if ($row['usudirectorcall'] == 0) echo '<input type="hidden" name="directorcall" value="1" />';
								else echo '<input type="hidden" name="directorcall" value="1" checked />' ?>
								<?php if ($row['usupromotor'] == 0) echo '<input type="hidden" id="promotor" name="promotor" value="1" />';
								else echo '<input type="hidden" id="promotor" name="promotor" value="1" checked />' ?>
								<?php if ($row['usucallcenter'] == 0) echo '<input type="hidden" id="callcenter" name="callcenter" value="1" />';
								else echo '<input type="hidden" id="callcenter" name="callcenter" value="1" checked />' ?>


							<p>
								<label class="not-w" for="relacionista">Cobrador:</label>
								<?php if ($row['usurelacionista'] == 0) echo '<input type="checkbox" name="relacionista" value="1" />';
								else echo '<input type="checkbox" name="relacionista" value="1" checked />' ?>
							</p>

							<p>
								<label class="not-w" for="asesor">Asesor de ventas:</label>
								<?php if ($row['usuasesor'] == 0) echo '<input type="checkbox" id="asesor" name="asesor" value="1" />';
								else echo '<input type="checkbox" id="asesor" name="asesor" value="1" checked />' ?>
								
								<label for="grupo">Asignar un grupo:</label>
								<select id="grupo" name="grupo" class="selectpicker" data-live-search="true" title="Seleccione un grupo" data-width="100%">
									<?php
									$qry = $db->query('SELECT * FROM grupos ORDER BY grunombre');
									while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
										if($row['usugrupo'] == $row2['grunombre'])
										echo '<option selected value="' . $row2['grunombre'] . '">' . $row2['grunombre'] . '</option>';
										else
										echo '<option value="' . $row2['grunombre'] . '">' . $row2['grunombre'] . '</option>';
									}
									?>
								</select>
							</p>

							<p class="boton">
								<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href='listar.php?<?php echo $filtro ?>'">atras</button>
								<button type="submit" class="btn btn-primary btnmodificar" name="modificar" value="modificar">Modificar</button>
							</p>
						</fieldset>
					</form>
				</div>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
</body>

</html>