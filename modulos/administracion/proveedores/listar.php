<?php
// LA ACCION PRINCIPAL DE ESTA PAGINA ES MOSTRAR LOS RESULTADOS DE LA CONSULTA GENERADA DESDE CONSULTAR.PHP
// LUEGO DE HABER RECIBIDO LAS VARIABLES POR POST DESDE CONSULTAR.PHP PROCEDEMOS A EVALUAR ESAS VARIABLES PARA MOSTRAR RESULTADOS DE LA CONSULTA O MODIFICAR SI ASI CORRESPONDE

//INCLUIR SESION Y CONECCION
$r = '../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
if(isset($_POST['modificar'])){ //VALIDAMOS SI POST MODIFICAR ES TRUE
	$id = strtoupper(trim($_POST['id']));
	$nombre = strtoupper(trim($_POST['nombre']));  // LO RECIBIDO DE ESTE CAMPO LO CAMBIAMOS A MAYUSCULA
	$num = $db->query("SELECT * FROM proveedores WHERE pvdid <> '$id' AND pvdnombre = '$nombre'")->rowCount();  // REALIZAMOS CONSULTA CON VARIABLES RECIBIDAS Y CONTAMOS LOS RESULTADOS OBTENIDOS
	if($num < 1) {  // SI LOS RESULTADOS DE LA CONSULTA SON MENORES A  1 REALIZAMOS LO SIGUIENTE
		$web = strtolower(trim($_POST['web']));
		$email = strtolower(trim($_POST['email']));
		$telefono = trim($_POST['telefono']);
		$direccion = strtoupper(trim($_POST['direccion']));
		$qry = $db->query("UPDATE proveedores SET pvdnombre = '$nombre', pvdweb = '$web', pvdemail = '$email', pvdtelefono = '$telefono', pvddireccion = '$direccion' WHERE pvdid = '$id'"); // ACTUALIZAMOS LA INFORMACION CON LOS NUEVOS DATOS RECIBIDOS
		if($qry) $mensaje = 'Se actualizo el proveedor'; // MENSAJE EXITOSO
		else $error = 'No se actualizo el proveedor';  // MENSAJE ERROR
	}else $error = 'El nombre del proveedor ya existe';
}
if(isset($_GET['id1'])){ //VALIDAMOS SI GET ID1 ES TRUE 
	$id1 = $_GET['id1'];
	//$num = $db->query("SELECT * FROM movimientos WHERE movtercero = '$id1' and")->rowCount(); // REALIZAMOS CONSULTA CON VARIABLES RECIBIDAS Y CONTAMOS LOS RESULTADOS OBTENIDOS
	//f($num < 1) { // SI LOS RESULTADOS DE LA CONSULTA SON MENORES A  1 REALIZAMOS LO SIGUIENTE
		$qry = $db->query("DELETE FROM proveedores WHERE pvdid = '$id1'"); //ELIMINAMOS EL PROEVEDOR
		if($qry) $mensaje = 'Se elimino el proveedor';
		else $error = 'No se pudo eliminar el proveedor';
	//}else $error = 'El proveedor posee movimiento';
}
$id = $nombre = 0;

if(isset($_POST['consultar'])){ //VALIDAMOS SI POST CONSULTAR ES TRUE
	$id = $_POST['id'];
	$nombre = $_POST['nombre'];
}else if (isset($_GET['id'])) { // SI LO RECIBIDO POR POST CONSULTAR NO ES TRUE
	$id = $_GET['id'];
	$nombre = $_GET['nombre'];
}
$filtro = 'id='.$id.'&nombre='.$nombre;  // FILTRO GENERAL PARA ENVIAR POR GET
if($id != '' && $nombre == '') $sql = "SELECT * FROM proveedores WHERE pvdid LIKE '%$id%'"; // VALIDAMOS LOS CAMPOS RECIBIDOS
elseif($id == '' && $nombre != '') $sql = "SELECT * FROM proveedores WHERE pvdnombre LIKE '%$nombre%'";
else $sql = "SELECT * FROM proveedores";
$qry = $db->query($sql);
?>
<!doctype html>
<html>
<head>

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
	<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
<script>
$(document).ready(function() {
	$('#tabla').dataTable({  //QRY PARA MANEJO Y VISUALIZACION DE TABLAS
		'bJQueryUI': true,
		'bStateSave': true,
		'sPaginationType': "full_numbers",
		'oLanguage': {
			'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
		},
		'aoColumnDefs': [
			{ 'bSortable': false, 'aTargets': [ 2, 3, 4, 5, 6, 7 ] }
    	],
	});
	
	$('.confirmar').click(function(e){ // VENTANA MODAL PARA CONFIRMAR ACCIONES
		e.preventDefault();
		var targetUrl = $(this).attr("href");
		var $dialog_link_follow_confirm = $('<div></div>').html('<p><span class="ui-icon ui-icon-trash" style="float:left; margin:2px 7px 7px 0;"></span>Esta seguro que desea eliminar el proveedor?</p>').
		dialog({
			title: 'Porfavor confirmar',
			buttons :{
				'Si' : function() { 
					carga();
					window.location.href = targetUrl; 
				},
				'No' : function() { 
					$(this).dialog("close"); 
				}
			},
			modal: true,
			width: 'auto',
			height: 'auto'
		}
	);
	$dialog_link_follow_confirm.dialog('open');
	});

});
</script>
</head>
<body>
<?php require($r.'incluir/src/login.php') ?>  <!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
<section id="principal">
<?php require($r.'incluir/src/cabeza.php') ?> <!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
<?php require($r.'incluir/src/menu.php') ?>  <!-- INCLUIMOS MENU PRINCIPAL -->
<article id="cuerpo">
<article class="mapa">
<a href="#">Principal</a><div class="mapa_div"></div><a href="#">Administracion</a><div class="mapa_div"></div><a class="current">Proveedores</a>
</article>
<article id="contenido">
<h2>Listado de proveedores</h2>
<div class="reporte">

</div>
<table id="tabla">
<thead>
<tr>
<th>Identificacion</th>
<th>Nombre</th>
<th>Sitio Web</th>
<th>Email</th>
<th>Telefonol</th>
<th>Direccion</th>
<th></th>
<th></th>
</tr>
</thead>
<tbody>
<?php
while($row = $qry->fetch(PDO::FETCH_ASSOC)){ //INFORMACION DE LA CONSULTA
?>
<tr>
<td><?php echo $row['pvdid'].' '.$row['pvddigito'] ?></td>
<td><?php echo $row['pvdnombre'] ?></td>
<td align="center"><a href="<?php echo $row['pvdweb'] ?>" target="_blank"><img src="<?php echo $r ?>imagenes/iconos/web.png" title="<?php echo $row['pvdweb'] ?>" /></a></td>
<td align="center"><a href="mailto:<?php echo $row['pvdemail'] ?>"><img src="<?php echo $r ?>imagenes/iconos/email.png" title="<?php echo $row['pvdemail'] ?>" /></td>
<td align="center"><img src="<?php echo $r ?>imagenes/iconos/telefono.png" title="<?php echo $row['pvdtelefono'] ?>" /></td>
<td align="center"><img src="<?php echo $r ?>imagenes/iconos/casa.png" title="<?php echo $row['pvddireccion'] ?>" /></td>
<td align="center"><a href="modificar.php?<?php echo 'id1='.$row['pvdid'].'&'.$filtro ?>" onClick="carga()" title="editar"><img src="<?php echo $r ?>imagenes/iconos/lapiz.png" class="grayscale" /></a></td>  <!-- ENVIA POR GET LAS VARIABLES NECESARIAS PARA MODIFICAR UN PROEVEDOR -->
<td align="center"><a href="listar.php?<?php echo 'id1='.$row['pvdid'].'&'.$filtro ?>" title="eliminar" class="confirmar"><img src="<?php echo $r ?>imagenes/iconos/basura.png" class="grayscale" /></a></td> <!-- ENVIA LAS VARIABLES NECESARIAS PARA ELIMINAR UN PROEVEDOR JUNTO CON VENTANA MODAL PARA CONFIRMAR -->
</tr>
<?php
}
?>
</tbody>
</table>
<p class="boton">
<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href='consultar.php'">Atras</button> <!-- BOTON PARA VOLVER A CONSULTAR -->
</p>
</article>
</article>
<?php require($r.'incluir/src/pie.php') ?> <!-- BOTON PARA VOLVER A LA PARTE SUPERIOR  -->
</section>
<?php
if(isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>'.$error.'</div>'; // VENTANA MODAL ERROR
elseif(isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>'.$mensaje.'</div>';  // VENTANA MODAL EXITOSO
?>
</body>
</html>