<?php
// LA ACCION PRINCIPAL DE ESTA PAGINA ES INSERTAR UN NUEVO PROEEVEDOR

$r = '../../../';
//INCLUIR SESION Y CONECCION
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
if (isset($_POST['insertar'])) { // VALIDAMOS QUE POST INSERTAR SEA TRUE
	$id = strtoupper(trim($_POST['id']));
	$nombre = strtoupper(trim($_POST['nombre']));
	$web = strtolower(trim($_POST['web']));
	$email = strtolower(trim($_POST['email']));
	$telefono = trim($_POST['telefono']);
	$direccion = strtoupper(trim($_POST['direccion']));
	// REALIZAMOS EL INSERT EN LA BD SEGUN LOS DATOS RECIBIDOS
	$sql = "INSERT INTO proveedores (pvdid, pvdnombre, pvdweb, pvdemail, pvdtelefono, pvddireccion) VALUES ('$id', '$nombre', '$web', '$email', '$telefono', '$direccion')";
	$qry = $db->query($sql);
	if ($qry) $mensaje = 'Se inserto el proveedor'; // MENSAJE MODAL EXITOSO
	else $error = 'No se pudo insertar el proveedor'; // MENSAJE MODAL ERROR
}
?>
<!doctype html>
<html>

<head>

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<script type="text/javascript" src="<?php echo $r ?>incluir/funciones.js"></script>
	<!-- ESTILOS EMBEBIDOS PARA MANEJAR FORMULARIO INDEPENDIENTEMENTE -->
	<style type="text/css">
	</style>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
		<?php require($r . 'incluir/src/menu.php') ?>
		<!-- INCLUIMOS MENU PRINCIPAL -->
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Administracion</a>
				<div class="mapa_div"></div><a class="current">Proveedores</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="insertar.php" method="post">
					<!-- FORMULARIO ENVIADO POR POST A SI MISMO -->
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">Insertar proveedor</legend>
						
						<p id="ide">
							<label for="id">Cedula/RUC:</label> 
							<input type="text" name="id" class="id validate[required, ajax[ajaxUserCall]] text-input" title="Digite la cedula del proveedor" />
						</p>
						<p>
							<label for="nombre">Nombre:</label> <!-- CAMPO NOMBRE  -->
							<input type="text" name="nombre" class="nombre validate[required, ajax[ajaxNameCall]] text-input" title="Digite el nombre del proveedor" />
						</p>
						<p>
							<label for="direccion">Sitio web:</label> <!-- CAMPO SITIO WEB -->
							<input type="text" name="web" class="web validate[custom[url]] text-input" />
						</p>
						<p>
							<label for="email">E-mail:</label> <!-- CAMPO EMAIL -->
							<input type="text" name="email" class="email validate[required, custom[email]] text-input" />
						</p>
						<p>
							<label for="telefono">Telefono:</label> <!-- CAMPO TELEFONO -->
							<input type="text" name="telefono" class="telefono validate[required, custom[phone]] text-input" />
						</p>
						<p>
							<label for="direccion">Direccion:</label> <!-- CAMPO DIRECCION -->
							<input type="text" name="direccion" class="direccion validate[required] text-input" />
						</p>
						<p class="boton">
							<button type="submit" class="btn btn-primary btninsertar" name="insertar" value="insertar">Insertar</button> <!-- BOTON INSERTAR -->
						</p>
					</fieldset>
				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
		<!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
	</section>
	<?php
	if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>'; //VENTANA MODAL ERROR
	elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>'; //VENTANA MODAL EXITOSO
	?>
</body>

</html>