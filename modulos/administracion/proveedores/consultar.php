<?php
// LA ACCION PRINCIPAL ES ESTA PAGINA ES CONSULTAR LOS PROVEEDORES
$r = '../../../';
//INCLUIR SESION Y CONECCION
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
?>
<!doctype html>
<html>

<head>

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>

	<!-- ESTILOS EMBEBIDOS PARA MANEJAR FORMULARIO INDEPENDIENTEMENTE -->
	<style type="text/css">

	</style>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
		<?php require($r . 'incluir/src/menu.php') ?>
		<!-- INCLUIMOS MENU PRINCIPAL -->
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Administracion</a>
				<div class="mapa_div"></div><a class="current">Proveedores</a>
			</article>
			<article id="contenido">
				<div class="ui-widget">
					<form id="form" name="form" action="listar.php" method="post">
						<!-- ENVIO FORMULARIO POR POST A LISTAR.PHP -->
						<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
							<legend class="ui-widget ui-widget-header ui-corner-all">Consultar proveedor</legend>
							<p>
								<label for="id">Identificacion:</label> <!-- CAMPO IDENTIFICACION -->
								<input type="text" name="id" class="id  text-input" title="Digite la identificacion" />
							</p>
							<p>
								<label for="nombre">Nombre:</label> <!-- CAMPO NOMBRE -->
								<input type="text" name="nombre" class="nombre validate[custom[onlyLetterSp]] text-input" title="Digite el nombre del proveedor" />
							</p>
							<div class="row">
								<div class="col-md-12 col-lg-6">
									<button type="submit" class="btnconsulta btn btn-primary btn-block mt-2" name="consultar" value="Buscar">consultar</button> <!-- BOTON CONSULTAR -->
								</div>
								<div class="col-md-12 col-lg-6">
									<button type="submit" class="btnconsulta btn btn-primary btn-block mt-2" name="show-all" value="Buscar">Mostrar Todo</button>								</div>
							</div>
						</fieldset>
					</form>
				</div>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
		<!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
	</section>
</body>

</html>