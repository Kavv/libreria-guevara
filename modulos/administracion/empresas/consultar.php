<?php
// LA ACCION PRINCIPAL ES ESTA PAGINA ES CONSULTAR LAS EMPRESAS
$r = '../../../';
//INCLUIR SESION Y CONECCION
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
?>
<!doctype html>
<html>

<head>
	<?php
		require($r . 'incluir/src/head.php');
		require($r . 'incluir/src/head-form.php');
	?>
	<!-- ESTILOS EMBEBIDOS PARA MANEJAR FORMULARIO INDEPENDIENTEMENTE -->
	<style type="text/css">

		#form label {
			display: inline-block;
			width: 100px;
			text-align: right;
			float: left;
			margin: 0.3em 2% 0 0
		}

		#form p {
			margin: 5px 0
		}
	</style>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
		<?php require($r . 'incluir/src/menu.php') ?>
		<!-- INCLUIMOS MENU PRINCIPAL -->
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Administracion</a>
				<div class="mapa_div"></div><a class="current">Empresas</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="listar.php" method="post">
					<!-- ENVIO FORMULARIO POR POST A LISTAR.PHP -->
					<fieldset class="ui-widget ui-widget-content ui-corner-all  col-md-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">Consultar empresas</legend>
						<p>
							<label for="id">Identificación/RUC:</label> <!-- CAMPO NIT -->
							<input type="text" name="id" id="id" class="form-control id" title="Digite el nit de la empresa" />
						</p>
						<p>
							<label for="nombre">Razon social:</label> <!-- CAMPO RAZON SOCIAL -->
							<input type="text" name="nombre" class="nombre form-control" title="Digite el nombre de la empresa" />
						</p>
						<div class="row">
							<div class="col-md-12 col-lg-6">
								<button type="submit" class="btnconsulta btn btn-primary btn-block mt-2" name="consultar" value="Buscar">consultar</button> <!-- BOTON CONSULTAR -->
							</div>
							<div class="col-md-12 col-lg-6">
								<button type="submit" class="btnconsulta btn btn-primary btn-block mt-2" name="show-all" value="Buscar">Mostrar Todo</button>							</div>
						</div>
					</fieldset>
				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
		<!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
	</section>
</body>

</html>