<?php
// LA ACCION PRINCIPAL DE ESTA PAGINA ES INSERTAR UNA NUEVA EMPRESA

$r = '../../../';
//INCLUIR SESION Y CONECCION
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
//Verificamos que se envio el formulario
if (isset($_POST['insertar'])) {

	$id = $_POST['id'];
	$nombre = strtoupper(trim($_POST['nombre'])); 
	$web = strtolower(trim($_POST['web'])); 
	$email = strtolower(trim($_POST['email'])); 
	$telefono = $_POST['telefono'];
	$direccion = strtoupper(trim($_POST['direccion'])); 
	$cuenta = strtoupper(trim($_POST['cuenta'])); 
	

	//Existe la empresa?
	$qry = $db->query("SELECT * FROM empresas WHERE empid  = '$id' "); 
	$count = $qry->rowCount();
	//Si es menor a 1 es porque no existe en la BD la empresa
	if($count < 1)
	{
		//Se ejecuta la consulta
		$qry = $db->query("INSERT INTO empresas (empid, empnombre, empweb, empemail, emptelefono, empdireccion, empcuenta) VALUES ('$id', '$nombre', '$web', '$email', '$telefono', '$direccion', '$cuenta')"); 
		//Si la consulta se realizo exitosamente
		if ($qry)
		{
			if (!file_exists($r . 'pdf/facturas/' . $id . '/') || !file_exists($r . 'pdf/solicitudes/' . $id . '/')) 
			{ 
				//Si las carpetas no existen se crearan
				if (mkdir($r . 'pdf/facturas/' . $id . '/') && mkdir($r . 'pdf/solicitudes/' . $id . '/') && mkdir($r . 'imagenes/logos/' . $id . '/')) 
				{			
					//Se indico un logo?
					if($_FILES['logo']['name'] != "")
					{
						$ext = explode("/",$_FILES['logo']["type"]);
						$new_name = $id . "." . $ext[1];
						//Se guarda el logo
						move_uploaded_file($_FILES['logo']['tmp_name'], $r . 'imagenes/logos/' . $id . '/'. $new_name);
						$db->query("UPDATE empresas SET logo ='$new_name' WHERE empid = '$id'"); 
					}
					$mensaje = 'Se agrego la nueva empresa';
					header("location:insertar.php?msj=$mensaje");
					exit();
				} 
				else $error = 'No se pudo crear el repositorio para la empresa';
			} 
			else $error = 'El repositorio para la empresa ya existe';
		} 
		else
			$error = "Error, no se pudo guardar el registro";
	}
	else
		$error = "No se guardo el registro. <br> La identificación se encuentra en uso";
	
	header("location:insertar.php?error=$error");
	exit();
}
if(isset($_GET['msj']))
	$mensaje = $_GET['msj'];
if(isset($_GET['error']))
	$error = $_GET['error'];

?>
<!doctype html>
<html>

<head>
	<?php 
		require($r . 'incluir/src/head.php');
		require($r . 'incluir/src/head-form.php');
	?>	
	<!-- ESTILOS EMBEBIDOS PARA MANEJAR FORMULARIO INDEPENDIENTEMENTE -->
	<style>
		#form {
			margin: 20px auto
		}

		#form fieldset {
			padding: 10px;
			display: block
		}

		#form legend {
			font-weight: bold;
			padding: 5px;
			text-align: center;
		}

		#form label {
			display: inline-block;
			text-align: right;
			margin: 0.3em 2% 0 0
		}

		#form p {
			margin: 5px 0
		}
		#cuenta {
			padding: 5px;
		}
		
	</style>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
		<?php require($r . 'incluir/src/menu.php') ?>
		<!-- INCLUIMOS MENU PRINCIPAL -->
		
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Administracion</a>
				<div class="mapa_div"></div><a class="current">Empresas</a>
			</article>
			<article id="contenido">
				<form id="form" class="" name="form" enctype="multipart/form-data" action="" method="post">
					<!-- FORMULARIO ENVIADO POR POST A SI MISMO -->
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">Insertar empresa</legend>
						<div class="col-md-12">
							<label for="id">Identificación/RUC:</label> <!-- CAMPO NIT SOLO NUMEROS-->
						</div>
						<div class="col-md-12">
							<input type="text" name="id" id="id" class="form-control id validate[required, ajax[ajaxUserCall]] text-input">
						</div>
						<div class="col-md-12">
							<label for="razon">Razon social:</label> <!-- CAMPO RAZON SOCIAL -->
						</div>
						<div class="col-md-12">
							<input type="text" id="nombre" name="nombre" class="form-control nombre validate[required, ajax[ajaxNameCall]] text-input" title="Digite el nombre de la empresa" >
						</div>
						<div class="col-md-12">
							<label for="web">Sitio Web:</label> <!-- CAMPO SITIO WEB -->
						</div>
						<div class="col-md-12">
							<input type="text" name="web" class="form-control web validate[custom[url]]" value="" >
						</div>
						<div class="col-md-12">
							<label for="email">E-mail:</label> <!-- CAMPO EMAIL -->
						</div>
						<div class="col-md-12">
							<input type="text" name="email" class="form-control email validate[custom[email]]" >
						</div>
						<div class="col-md-12">
							<label for="telefono">Telefono:</label> <!-- CAMPO TELEFONO -->
						</div>
						<div class="col-md-12">
							<input type="text" name="telefono" class="form-control telefono validate[required, custom[phone]] text-input" >
						</div>
						<div class="col-md-12">
							<label for="direccion">Direccion:</label> <!-- CAMPO DIRECCION -->
						</div>
						<div class="col-md-12">
							<input type="text" name="direccion" class="form-control direccion validate[required] text-input" >
						</div>
						<div class="col-md-12">
							<label for="logo">Logo:</label> <!-- CAMPO LOGO -->
						</div>
						<div class="col-md-12">
							<input type="file" id="logo" name="logo" class="form-control validate[checkFileType[jpg|JPG|png|PNG] text-input">
						</div>
						<!-- CAMPO RESOLUCION POR LA DIAN -->
						<!-- <p>
							<label for="dian">Resolucion DIAN:</label>
							por computador No. <input type="text" name="resnumero" class="validate[required, custom[onlyNumberSp]]" style="text-align:center" /> de
							<input type="text" class="fecha validate[required]" id="fecha" name="resfecha" />
						</p>
						<p>
							<label for="dian"></label>
							del no. <input type="text" name="resnum1" size="10px" class="validate[required, custom[onlyNumberSp]]" style="text-align:center" /> al
							<input type="text" name="resnum2" size="10px" class="validate[required, custom[onlyNumberSp]]" style="text-align:center" />, con el prefijo
							<input type="text" name="resprefijo" class="documento validate[custom[onlyLetterSp]]" /></p>
						</p> -->
						<div class="col-md-12">
							<label for="cuenta">Informacion adicional:</label>
						</div>
						<div class="col-md-12">
							<textarea class="col-md-12 form-control" id="cuenta" name="cuenta" rows="3" ></textarea>
						</div>
						<p class="boton">
							<button type="submit" class="btn btn-primary btninsertar" name="insertar" value="insertar">insertar</button> <!-- BOTON INSERTAR -->
						</p>
					</fieldset>
				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
		<!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
	</section>
	<?php
	if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>'; //VENTANA MODAL ERROR
	elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>'; //VENTANA MODAL EXITOSO
	?>
</body>

</html>