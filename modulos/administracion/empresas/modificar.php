<?php
// LA ACCION PRINCIPAL DE ESTA PAGINA ES MODIFICA UNA CAUSAL ENVIANDOLO A LISTAR

$r = '../../../';
//INCLUIR SESION Y CONECCION
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
$success = false;
// CONSULTA A LA BD CON VARUABLES RECIBIDAS POR GET
if(isset($_GET['id1']))
{
	$temp_id = $_GET['id1'];
	$row_empresa = $db->query("SELECT * FROM empresas WHERE empid = '$temp_id'")->fetch(PDO::FETCH_ASSOC); 
	$success = true;
}
else 
	$_GET['id'] = "";
if(!isset($_GET['nombre']))
	$_GET['nombre'] = "";
if(!isset($_GET['tipo']))
	$_GET['tipo'] = "";

$filtro = 'id='.$_GET['id'].'&nombre='.$_GET['nombre'].'&tipo='.$_GET['tipo']; //FILTRO GENERAL

?>

<!doctype html>
<html>

<head>	
	<?php 
		require($r . 'incluir/src/head.php');
		require($r . 'incluir/src/head-form.php');
	?>	
	<!-- ESTILOS EMBEBIDOS PARA MANEJAR FORMULARIO INDEPENDIENTEMENTE -->
	<style>
		#form label {
			display: inline-block;
			text-align: right;
			margin: 0.3em 2% 0 0
		}

		#form p {
			margin: 5px 0
		}
	</style>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
		<?php require($r . 'incluir/src/menu.php') ?>
		<!-- INCLUIMOS MENU PRINCIPAL -->
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="index.php">Administracion</a>
				<div class="mapa_div"></div><a class="current">Empresas</a>
			</article>
			<article id="contenido">
				<div class="ui-widget">
					<form id="form" name="form" enctype="multipart/form-data" action="listar.php?<?php echo $filtro ?>" method="post">
						<!-- FORMULARIO ENVIADO POR POST A LISTAR.PHP JUNTO CON EL FILTRO GENERAL  -->
						<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
							<legend class="ui-widget ui-widget-header ui-corner-all">Modificar empresa</legend>
							<?php if($success) { ?>
								<div class="col-md-12">
									<label for="id">Identificación/RUC:</label> <!-- CAMPO NIT SOLO NUMEROS-->
								</div>
								<div class="col-md-12">
									<input value="<?php echo $row_empresa['empid'] ?>" readonly type="text" name="id" id="id" class="form-control id">
								</div>
								<div class="col-md-12">
									<label for="razon">Razon social:</label> <!-- CAMPO RAZON SOCIAL -->
								</div>
								<div class="col-md-12">
									<input value="<?php echo $row_empresa['empnombre'] ?>" type="text" id="nombre" name="nombre" class="form-control nombre validate[required] text-input" title="Digite el nombre de la empresa" >
								</div>
								<div class="col-md-12">
									<label for="web">Sitio Web:</label> <!-- CAMPO SITIO WEB -->
								</div>
								<div class="col-md-12">
									<input value="<?php echo $row_empresa['empweb'] ?>" type="text" name="web" class="form-control web validate[custom[url]]" value="" >
								</div>
								<div class="col-md-12">
									<label for="email">E-mail:</label> <!-- CAMPO EMAIL -->
								</div>
								<div class="col-md-12">
									<input value="<?php echo $row_empresa['empemail'] ?>" type="text" name="email" class="form-control email validate[custom[email]]" >
								</div>
								<div class="col-md-12">
									<label for="telefono">Telefono:</label> <!-- CAMPO TELEFONO -->
								</div>
								<div class="col-md-12">
									<input value="<?php echo $row_empresa['emptelefono'] ?>" type="text" name="telefono" class="form-control telefono validate[required, custom[phone]] text-input" >
								</div>
								<div class="col-md-12">
									<label for="direccion">Direccion:</label> <!-- CAMPO DIRECCION -->
								</div>
								<div class="col-md-12">
									<input value="<?php echo $row_empresa['empdireccion'] ?>" type="text" name="direccion" class="form-control direccion validate[required] text-input" >
								</div>
								<div class="col-md-12">
									<label for="logo">Logo Actual:</label>
									<img src="<?php echo $r . 'imagenes/logos/'. $row_empresa['empid'] . "/" .$row_empresa['logo'] ?>" alt="logo empresa" class="img-fluid">
								</div>
								<div class="col-md-12">
									<label for="logo">Cambiar Logo:</label> <!-- CAMPO LOGO -->
								</div>
								<div class="col-md-12">
									<input type="file" id="logo" name="logo" class="form-control validate[checkFileType[jpg|JPG|png|PNG] text-input">
								</div>
								
								<div class="col-md-12">
									<label for="cuenta">Informacion adicional:</label>
								</div>
								<div class="col-md-12">
									<textarea class="col-md-12 form-control" id="cuenta" name="cuenta" rows="3" ><?php echo $row_empresa['empcuenta'] ?></textarea>
								</div>
							
							<?php } ?>
							<p class="boton">
								<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href='listar.php?<?php echo $filtro ?>'">atras</button> <!--  BOTON PARA VOLVER ATRAS JUNTO CON FILTRO GENERAL-->
								<button type="submit" class="btn btn-primary btnmodificar" name="modificar" value="modificar">modificar</button> <!-- BOTON MODIFICAR -->
							</p>
						</fieldset>
					</form>
				</div>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
		<!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
	</section>
</body>

</html>