<?php
	// LA ACCION PRINCIPAL DE ESTA PAGINA ES MOSTRAR LOS RESULTADOS DE LA CONSULTA GENERADA DESDE CONSULTAR.PHP
	// LUEGO DE HABER RECIBIDO LAS VARIABLES POR POST DESDE CONSULTAR.PHP PROCEDEMOS A EVALUAR ESAS VARIABLES PARA MOSTRAR RESULTADOS DE LA CONSULTA O MODIFICAR SI ASI CORRESPONDE

	//INCLUIR SESION Y CONECCION
	$r = '../../../';
	require($r . 'incluir/session.php');
	require($r . 'incluir/connection.php');
	if (isset($_POST['modificar'])) { //VALIDAMOS SI POST MODIFICAR ES TRUE
		$id = $_POST['id'];
		$nombre = strtoupper(trim($_POST['nombre'])); // LO RECIBIDO DE ESTE CAMPO LO CAMBIAMOS A MAYUSCULA
		// REALIZAMOS CONSULTA CON VARIABLES RECIBIDAS Y CONTAMOS LOS RESULTADOS OBTENIDOS
		$num = $db->query("SELECT * FROM empresas WHERE empid <> '$id' AND empnombre = '$nombre'")->rowCount(); 
		// SI LOS RESULTADOS DE LA CONSULTA SON MENORES A  1 REALIZAMOS LO SIGUIENTE
		if ($num < 1) {  
			$web = strtolower(trim($_POST['web']));
			$email = trim($_POST['email']);
			$telefono = $_POST['telefono'];
			$direccion = strtoupper(trim($_POST['direccion'])); // LO RECIBIDO DE ESTE CAMPO LO CAMBIAMOS A MAYUSCULA
			$cuenta = strtoupper(trim($_POST['cuenta'])); // LO RECIBIDO DE ESTE CAMPO LO CAMBIAMOS A MAYUSCULA

			$qry = $db->query("UPDATE empresas SET empnombre = '$nombre', empweb = '$web', empemail = '$email', emptelefono = '$telefono', empdireccion = '$direccion', empcuenta = '$cuenta' WHERE empid = '$id'");
			
			//Se indico un logo?
			if($_FILES['logo']['name'] != "")
			{
				$ext = explode("/",$_FILES['logo']["type"]);
				$new_name = $id . "." . $ext[1];

				// Se obtienen nombre del logo previamente establecido
				$res = $db->query("SELECT logo FROM empresas WHERE empid = '$id'");
				$data = $res->fetch(PDO::FETCH_ASSOC);

				if(!file_exists($r . 'imagenes/logos/' . $id . '/'))
					mkdir($r . 'imagenes/logos/' . $id . '/');
				
				// En caso de existir se eliminara el archivo
				if(file_exists($r . 'imagenes/logos/' . $id . '/'. $data['logo']) && $data['logo'] != "" )
					unlink($r . 'imagenes/logos/' . $id . '/'. $data['logo']);

				// Se guarda el logo nuevo
				move_uploaded_file($_FILES['logo']['tmp_name'], $r . 'imagenes/logos/' . $id . '/'. $new_name);
				$db->query("UPDATE empresas SET logo ='$new_name' WHERE empid = '$id'"); 
			}
			
			if ($qry) $mensaje = 'Se actualizo la empresa'; // MENSAJE EXITOSO
			else $error = 'Ocurrio un error :( <br/>No se actualizo la empresa'; // MENSAJE ERROR
		} else $error = 'No se actualizo el registro. <br/>El nombre de la empresa ya existe';
	}
	if (isset($_GET['id1'])) { //VALIDAMOS SI GET ID1 ES TRUE 
		$id1 = $_GET['id1'];
		// Verificamos si la empresa a eliminar no posee ninguna solicitud
		$num = $db->query("SELECT * FROM solicitudes WHERE solempresa = '$id1'")->rowCount(); 
		if ($num < 1) { // SI LOS RESULTADOS DE LA CONSULTA SON MENORES A  1 REALIZAMOS LO SIGUIENTE
			if (file_exists($r . 'pdf/facturas/' . $id1 . '/') && file_exists($r . 'pdf/solicitudes/' . $id1 . '/')) { // SI LAS CARPETAS EXISTEN 
				if (rmdir($r . 'pdf/facturas/' . $id1 . '/') && rmdir($r . 'pdf/solicitudes/' . $id1 . '/')) {
						
					// Se obtienen nombre del logo previamente establecido
					$res = $db->query("SELECT logo FROM empresas WHERE empid = '$id1'");
					$data = $res->fetch(PDO::FETCH_ASSOC);
					// En caso de existir se eliminara el archivo
					if(file_exists($r . 'imagenes/logos/' . $id1 . '/'. $data['logo']))
						unlink($r . 'imagenes/logos/' . $id1 . '/'. $data['logo']);
					
					if(file_exists($r . 'imagenes/logos/' . $id1 . '/'))
						rmdir($r . 'imagenes/logos/' . $id1 . '/');
					
					
					$qry = $db->query("DELETE FROM empresas WHERE empid = '$id1'"); // ELIMINAR LA EMPRESA
					if ($qry) $mensaje = 'Se elimino la empresa'; // MENSAJE EXITOSO
					else $error = 'Ocurrio un error. <br/>No se pudo eliminar la empresa';
							
				} else $error = 'No se elimino el repositorio de la empresa';
			} else $error = 'Falta al menos un repositorio de la empresa';
		} else $error = 'La empresa tiene solicitudes asociadas';
	}

	$id = $nombre = "";

	if (isset($_POST['consultar'])) { //VALIDAMOS SI POST CONSULTAR ES TRUE
		$id = $_POST['id'];
		$nombre = $_POST['nombre'];
	} else if(isset( $_GET['id'])){ // SI LO RECIBIDO POR POST CONSULTAR NO ES TRUE
		$id = $_GET['id'];
		$nombre = $_GET['nombre'];
	}

	$filtro = 'id=' . $id . '&nombre=' . $nombre; // FILTRO GENERAL PARA ENVIAR POR GET
	if ($id != "" && $nombre == '') 
		$sql = "SELECT * FROM empresas WHERE empid LIKE '%$id%'"; // VALIDAMOS LOS CAMPOS RECIBIDOS
	elseif ($id == "" && $nombre != '') 
		$sql = "SELECT * FROM empresas WHERE empnombre LIKE '%$nombre%'";
	else 
		$sql = "SELECT * FROM empresas";
		
	$qry = $db->query($sql);

?>

<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
	<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#tabla').dataTable({ //QRY PARA MANEJO Y VISUALIZACION DE TABLAS
				'bJQueryUI': true,
				'sPaginationType': 'full_numbers',
				'oLanguage': {
					'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
				},
				'aoColumnDefs': [{
					'bSortable': false,
					'aTargets': [2, 3, 4, 5, 6, 7]
				}],
			});
			
			$('.confirmar').click(function(e) { // VENTANA MODAL PARA CONFIRMAR ACCIONES
				e.preventDefault();
				var targetUrl = $(this).attr('href');
				var $dialog_link_follow_confirm = $('<div></div>').html('<p><span class="ui-icon ui-icon-trash" style="float:left; margin:2px 7px 7px 0"></span>Esta seguro que desea eliminar la empresa?</p>').
				dialog({
					title: 'Porfavor confirmar',
					buttons: {
						'Si': function() {
							carga();
							window.location.href = targetUrl;
						},
						'No': function() {
							$(this).dialog('close');
						}
					},
					modal: true,
					width: 'auto',
					height: 'auto'
				});
				$dialog_link_follow_confirm.dialog("open");
			});
		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
		<?php require($r . 'incluir/src/menu.php') ?>
		<!-- INCLUIMOS MENU PRINCIPAL -->
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Administracion</a>
				<div class="mapa_div"></div><a class="current">Empresas</a>
			</article>
			<article id="contenido">
				<h2>Listado de empresas</h2>
				<!-- INICIO DE TABLA -->
				<table id="tabla">
					<thead>
						<tr>
							<th>Nit</th>
							<th>Razon Social</th>
							<th>Web</th>
							<th>E-mail</th>
							<th>Telefono</th>
							<th>Direccion</th>
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php
						while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {  //INFORMACION DE LA CONSULTA
						?>
							<tr class="text-center">
								<!-- DNI -->
								<td class="text-left">
									<?php echo $row['empid'] . ' ' . $row['empdigito'] ?>
								</td>
								<!-- NOMBRE -->
								<td class="text-left">
									<?php echo $row['empnombre'] ?>
								</td>
								<!-- WEB -->
								<td>
									<?php
										if($row['empweb'] != "") 
										echo '<a href="'.$row["empweb"].'" target="_blank"><img src="'.$r.'imagenes/iconos/web.png" title="'.$row["empweb"].'" /></a>'; 
										else
										echo "-";

									?>
								</td> 
								<!-- EMAIL -->
								<td>
									<?php
										if($row['empemail'] != "") 
											echo '<a href="mailto:'.$row["empemail"].'"><img src="'.$r.'imagenes/iconos/email.png" title="'.$row["empemail"].'" /></a>';
										else
											echo "-";
									?>
								</td> 
								<!-- TELEFONO -->
								<td>
									<?php
										if($row['emptelefono'] != "") 
											echo '<a href="tel:'.$row["emptelefono"].'"><img src="'. $r .'imagenes/iconos/telefono.png" title="'.$row["emptelefono"].'" /></a>';
										else
											echo "-";
									?>
								</td> 
								<!-- DIRECCION -->
								<td>
									<?php
										if($row['empdireccion'] != "") 
											echo '<button class="btn btn-light btn-dir" data-dir="'.$row["empdireccion"]  .'" data-toggle="modal" data-target="#modaldetalle"><img src="'.$r  .'imagenes/iconos/casa.png" title="'.$row["empdireccion"] .'" /></button>';
										else
											echo "-";
									?>
								</td> 
								<!-- ENVIA POR GET LAS VARIABLES NECESARIAS PARA MODIFICAR UNA CIUDAD -->
								<td>
									<a href="modificar.php?<?php echo 'id1=' . $row['empid'] . '&' . $filtro ?>" onClick="carga()" title="editar"><img src="<?php echo $r ?>imagenes/iconos/lapiz.png" class="grayscale" /></a>
								</td> 
								 <!-- ENVIA LAS VARIABLES NECESARIAS PARA ELIMINAR UNA CIUDAD JUNTO CON VENTANA MODAL PARA CONFIRMAR -->
								<td>
									<a href="listar.php?<?php echo 'id1=' . $row['empid'] . '&' . $filtro ?>" title="eliminar" class="confirmar"><img src="<?php echo $r ?>imagenes/iconos/basura.png" class="grayscale" /></a>
								</td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
				<p class="boton">
					<button type="button" class="btn btn-outline-primary btnatras" onClick="carga(); location.href='consultar.php'">
						<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-left-square-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
							<path fill-rule="evenodd" d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm9.5 8.5a.5.5 0 0 0 0-1H5.707l2.147-2.146a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708-.708L5.707 8.5H11.5z"/>
						</svg>
						Atras
					</button> <!-- BOTON PARA VOLVER A CONSULTAR -->
				</p>
			</article>
		</article>
		
		<!-- BOTON PARA VOLVER A LA PARTE SUPERIOR  -->
		<?php require($r . 'incluir/src/pie.php') ?>
		

		<!-- Modal -->
		<div class="modal fade" id="modaldetalle" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Dirección</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div id="detalle-body-modal" class="modal-body">
						
					</div>
				</div>
			</div>
		</div>
		
	</section>
	<script>
		$(".btn-dir").click(function(){
			var text = $(this).data().dir;
			var p = "<p>" + text + "</p>"; 
			console.log(p);
			$("#detalle-body-modal").empty();
			$("#detalle-body-modal").append(p);
		});
	</script>
	<?php
	if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>'; // VENTANA MODAL ERROR
	elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>'; // VENTANA MODAL EXITOSO
	?>
</body>

</html>