<?php
// LA ACCION PRINCIPAL DE ESTA PAGINA ES INSERTAR UNA NUEVA FORMA DE PAGO

$r = '../../../';
//INCLUIR SESION Y CONECCION
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
if (isset($_POST['insertar'])) {  // SE VALIDA SI LA INFORMACION VIENE DE INSERTAR 
	$nombre = trim(strtoupper($_POST['nombre']));
	$qry = $db->query("INSERT INTO fpagos (fpanombre) VALUES ('$nombre')") or die($db->errorInfo()[2]); // REALIZAMOS EL INSERT EN LA BD
	if ($qry) 
	{
		$mensaje = 'Se inserto la forma de pago'; // MENSAJE MODAL EXITOSO
		header('location:insertar.php?msj='.$mensaje);
		exit();
	}
	else 
	{
		$error = 'Se inserto la forma de pago'; // MENSAJE MODAL ERROR
		header('location:insertar.php?error='.$error);
		exit();

	}
}
?>
<!doctype html>
<html>

<head>

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-not-form.php');
	?>

</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
		<?php require($r . 'incluir/src/menu.php') ?>
		<!-- INCLUIMOS MENU PRINCIPAL -->
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Administracion</a>
				<div class="mapa_div"></div><a class="current">Formas de pago</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="" method="POST">
					<!-- FORMULARIO ENVIADO POR POST A SI MISMO -->
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">Insertar forma de pago</legend>
						<p>
							<label for="nombre">Nombre:</label> <!-- CAMPO NOMBRE -->
							<input type="text" name="nombre" class="nombre validate[required, ajax[ajaxNameCall]] text-input" title="Digite la forma de pago" />
						</p>
						<input type="hidden" name="insertar">
						<p class="boton">
							<button id="btninsertar" onclick="carga();" class="btn btn-primary btninsertar">insertar</button> <!-- BOTON INSERTAR -->
						</p>
					</fieldset>
				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
		<!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
	</section>
	<?php
	if (isset($_GET['error'])) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['error'] . '</div>'; //VENTANA MODAL ERROR
	elseif (isset($_GET['msj'])) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['msj'] . '</div>'; //VENTANA MODAL EXITOSO
	?>
</body>

</html>