<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

$codigo = $_GET['id1'];
$row = $db->query("SELECT * FROM grupos  WHERE gruid = '$codigo'")->fetch(PDO::FETCH_ASSOC);
$filtro = 'id=' . $_GET['id'] . '&nombre=' . $_GET['nombre'];
?>
<!doctype html>
<html lang="es">

<head>
    <title>MODIFICAR GRUPO</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">
	<meta charset="utf-8">
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Administracion</a>
				<div class="mapa_div"></div><a class="current">Asesor</a>
			</article>
			<article id="contenido">
				<div class="ui-widget">
					<form id="form" name="form" action="listar.php?<?php echo $filtro ?>" method="post">
						<input type="hidden" name="id" id="idgrupo" value="<?php echo $row['gruid']?>"/>

						<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
							<legend class="ui-widget ui-widget-header ui-corner-all">Insertar Grupo</legend>
							<p>
								<label for="id"><spam style="color:red">*</spam>Código del grupo:</label>
								<input type="text" name="codigo" class="uppercase validate[required, maxSize[45], ajax[ajaxGrupo]] text-input" title="Digite el codigo del grupo" value="<?php echo $row['grunombre']?>"/>
							</p>
							<p>
								<label for="nombre"><spam style="color:red">*</spam>Responsable del grupo:</label>
								<input type="text" name="nombre" class="nombre validate[required, maxSize[50]] text-input" title="Digite el nombre del responsable" value="<?php echo $row['supervisor']?>"/>
							</p>
							<p class="boton">
								<input type="hidden" name="modificar" value=""/>
								<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href='listar.php?<?php echo $filtro ?>'">atras</button>
								<button type="submit" class="btn btn-primary btninsertar">Modificar</button>
							</p>
						</fieldset>
					</form>
				</div>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
</body>

</html>