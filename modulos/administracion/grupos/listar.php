<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
if (isset($_POST['modificar'])) {
	$id = $_POST['id'];
	$codigo = trim(strtoupper($_POST['codigo']));
	$responsable = trim(strtoupper($_POST['nombre']));
    $digitador = $_SESSION['id'];
	
	$filtro = 'id=' . $_GET['id'] . '&nombre=' . $_GET['nombre'];
	$qry = $db->query("UPDATE grupos SET grunombre = '$codigo', supervisor = '$responsable' WHERE gruid = '$id'");

	if ($qry) {
        $mensaje = 'Se edito el grupo';
        header("Location:listar.php?msj=$mensaje&$filtro");
        exit();
    } else {
        $error = 'No se pudo editar el grupos';
        header("Location:listar.php?error=$error&$filtro");
        exit();
    }
}
if (isset($_GET['id1'])) {
	$id1 = $_GET['id1'];
	$grupo = $db->query("SELECT * FROM grupos WHERE gruid = '$id1'")->fetch(PDO::FETCH_ASSOC);
	$grupo_nombre = $grupo['grunombre'];
	$num = $db->query("SELECT * FROM usuarios WHERE usugrupo = '$grupo_nombre'")->rowCount();
	if ($num < 1) 
	{
		$db->query("DELETE FROM grupos WHERE gruid = '$id1'");
		$mensaje = 'Se elimino el grupo';
	}
	else 
		$error = 'No se elimino el grupo, existen usuarios asignados al mismo.';

	$filtro = 'id=' . $_GET['id'] . '&nombre=' . $_GET['nombre'];
	if(isset($error))
		header("Location:listar.php?error=$error&$filtro");
	if(isset($mensaje))
		header("Location:listar.php?msj=$mensaje&$filtro");

	exit();
}
$id = $nombre = $grupo = "";
if (isset($_POST['consultar'])) {
	$id = strtoupper($_POST['id']);
	$nombre = strtoupper($_POST['nombre']);
} else if (isset($_GET['id'])) {
	$id = strtoupper($_GET['id']);
	$nombre = strtoupper($_GET['nombre']);
}

	$con = "SELECT * FROM grupos";

    /* Los parametros de la consulta sql se genera dinamicamente 
    en base a los datos recibidos para delimitar los resultados */
    $parameters = [];
    if($id != "")
        array_push($parameters, "grunombre LIKE '%$id%'" );
    if($nombre != "")
        array_push($parameters, "supervisor LIKE '%$nombre%'" );


    // Consulta base
    $sql = $con;
    foreach ($parameters as $index => $parameter) {
        // Se agregan los parametros del WHERE
        if($index == 0)
            $sql .= " WHERE " . $parameter;
        else
            $sql .= " AND " . $parameter;
	}
	

$filtro = 'id=' . $id . '&nombre=' . $nombre;

$qry = $db->query($sql);
?>
<!doctype html>
<html lang="es">

<head>
    <title>MOSTRAR GRUPOS</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>

	<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
	<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
	<script>
		$(document).ready(function() {
			$('#tabla').dataTable({
				'bJQueryUI': true,
				'sPaginationType': "full_numbers",
				'oLanguage': {
					'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
				}
			});

			$('.confirmar').click(function(e) {
				e.preventDefault();
				var targetUrl = $(this).attr("href");
				var $dialog_link_follow_confirm = $('<div></div>').html('<p><span class="ui-icon ui-icon-trash" style="float:left; margin:2px 7px 7px 0;"></span>Esta seguro que desea eliminar el grupo?</p>').
				dialog({
					title: 'Porfavor confirmar',
					buttons: {
						'Si': function() {
							carga();
							window.location.href = targetUrl;
						},
						'No': function() {
							$(this).dialog("close");
						}
					},
					modal: true,
					width: 'auto',
					height: 'auto'
				});
				$dialog_link_follow_confirm.dialog('open');
			});
		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Administracion</a>
				<div class="mapa_div"></div><a class="current">Listado de Asesores</a>
			</article>
			<article id="contenido">
				<h2>Listado de Asesores</h2>
				<div class="reporte">

				</div>
				<table id="tabla">
					<thead>
						<tr>
							<th>Nombre del Grupo</th>
							<th>Responsable</th>
							<th>Digitador</th>
							<th>F.Digitado</th>
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php
						while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
							$originalDate = $row['created_at'];
							$newDate = date("Y-m-d h:i:s A", strtotime($originalDate));
						?>
							<tr>
								<td><?php echo $row['grunombre'] ?></td>
								<td><?php echo $row['supervisor'] ?></td>
								<td><?php echo $row['digitador'] ?></td>
								<td><?php echo $newDate; ?></td>
								<td align="center"><a href="modificar.php?<?php echo 'id1=' . $row['gruid'] . '&' . $filtro ?>" onClick="carga()" title="editar"><img src="<?php echo $r ?>imagenes/iconos/lapiz.png" class="grayscale" /></a></td>
								<td align="center"><a href="listar.php?<?php echo 'id1=' . $row['gruid'] . '&' . $filtro ?>" title="eliminar" class="confirmar"><img src="<?php echo $r ?>imagenes/iconos/basura.png" class="grayscale" /></a></td>
							</tr>
						<?php
						}
						?>
					</tbody>
				</table>
				<p class="boton">
					<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href='consultar.php'">Atras</button>
				</p>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<?php
		if(isset($_GET['error'])) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['error'] . '</div>';
		elseif(isset($_GET['msj'])) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['msj'] . '</div>';
	?>
</body>

</html>