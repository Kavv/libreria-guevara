<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
?>
<!doctype html>
<html lang="es">

<head>
    <title>CONSULTA GRUPOS</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>

</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
		<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Administracion</a>
				<div class="mapa_div"></div><a class="current">Consultar Grupo</a>
			</article>
			<article id="contenido">
				<div class="ui-widget">
					<form id="form" name="form" action="listar.php" method="post">
						<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
							<legend class="ui-widget ui-widget-header ui-corner-all">Consultar Grupo</legend>
							<p>
								<label for="id">Nombre del grupo:</label>
								<input type="text" name="id" class="id text-input" title="Digite el nombre del grupo" />
							</p>
							<p>
								<label for="nombre">Responsable del grupo:</label>
								<input type="text" name="nombre" class="nombre text-input" title="Digite el nombre del responsable" />
							</p>
							<div class="row">
								<div class="col-md-12 col-lg-6">
									<button type="submit" class="btnconsulta btn btn-primary btn-block mt-2" name="consultar" value="Buscar">consultar</button> <!-- BOTON CONSULTAR -->
								</div>
								<div class="col-md-12 col-lg-6">
									<button type="submit" class="btnconsulta btn btn-primary btn-block mt-2" name="show-all" value="Buscar">Mostrar Todo</button>
								</div>
							</div>
						</fieldset>
					</form>
				</div>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
</body>

</html>