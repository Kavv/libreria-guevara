<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
if (isset($_POST['insertar'])) 
{
	$grupo = trim(strtoupper($_POST['id']));
	$responsable = trim(strtoupper($_POST['nombre']));
    $digitador = $_SESSION['id'];
	
	$qry = $db->query("INSERT INTO grupos (grunombre, supervisor, digitador)
    VALUES ('$grupo', '$responsable', '$digitador')") or die( $db->errorInfo()[2]);
	
	if ($qry) {
        $mensaje = 'Se inserto el grupo';
        header("Location:insertar.php?msj=$mensaje");
        exit();
    } else {
        $error = 'No se pudo insertar el grupo';
        header("Location:insertar.php?error=$error");
        exit();
    }
}
?>
<!doctype html>
<html lang="es">

<head>
    <title>AGREGAR GRUPO</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">
	<?php 
		require($r . 'incluir/src/head.php');
		require($r . 'incluir/src/head-form.php');
	?>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

	
	</style>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Administracion</a>
				<div class="mapa_div"></div><a class="current">Grupos</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="insertar.php" method="post">
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">Insertar Grupo</legend>
						<p>
							<label for="id"><spam style="color:red">*</spam>Código del grupo:</label>
							<input type="text" name="id" class="uppercase validate[required, maxSize[45], ajax[ajaxUserCall]] text-input" title="Digite el codigo del grupo" />
						</p>
						<p>
							<label for="nombre"><spam style="color:red">*</spam>Responsable del grupo:</label>
							<input type="text" name="nombre" class="nombre validate[required, maxSize[50]] text-input" title="Digite el nombre del responsable" />
						</p>
						<p class="boton">
							<button type="submit" class="btn btn-primary btninsertar" name="insertar" value="insertar">Insertar</button>
						</p>
					</fieldset>
				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<?php
	if(isset($_GET['error'])) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['error'] . '</div>';
	elseif(isset($_GET['msj'])) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['msj'] . '</div>';
	?>
</body>

</html>