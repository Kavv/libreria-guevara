<?php
$r = '../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
?>
<!doctype html>
<html>
<head>
	<?php 
		require($r . 'incluir/src/head.php');
		require($r . 'incluir/src/head-form.php');
	?>
<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#form').validationEngine({
		showOneMessage:true,
		onValidationComplete: function(form, status){
        	if(status) {               
            	carga();
            	return true;
			}
		}
	});
	$('#fecha1').datepicker({
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		maxDate: '+0D',
		onClose: function(selectedDate) {
			$('#fecha2').datepicker('option', 'minDate', selectedDate);
		}
	}).keypress(function(event) { event.preventDefault() });
	$('#fecha2').datepicker({
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		maxDate: '+0D',
		onClose: function(selectedDate) {
			$('#fecha1').datepicker('option', 'maxDate', selectedDate);
		}
	}).keypress(function(event) { event.preventDefault() });
	$('.btnconsulta').button({ icons: { primary: 'ui-icon ui-icon-search' }});
	$('#dialog-message').dialog({
		height: 80,
		width: 'auto',
		modal: true
	});
});
</script>
<style type="text/css">
#form form{ width:800px }
#form fieldset{ padding:10px; display:block; width:800px; margin:20px auto }
#form legend{ font-weight: bold; margin-left:5px; padding:5px }
#form label{ display:inline-block; width:150px; text-align:right; float:left; margin:0.3em 2% 0 0 }
#form p{ margin:5px 0 }
</style>
</head>
<body>
<?php require($r.'incluir/src/login.php') ?>
<section id="principal">
<?php require($r.'incluir/src/cabeza.php') ?>
<?php require($r.'incluir/src/menu.php') ?>
<article id="cuerpo">
<article class="mapa">
<a href="#">Principal</a><div class="mapa_div"></div><a class="current">Gestion Documental</a>
</article>
<article id="contenido">
<form id="form" name="form" action="listarpape.php" method="post">
<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
<legend class="ui-widget ui-widget-header ui-corner-all">Consulta de Papeleria</legend>

<p>
<label for="tipo">Tipo:</label> <!-- CAMPO -->
<select name="tipo">
<option value="">SELECCIONE</option>
<option value='unidad'>Unidad</option>
<option value='consecutivo'>Consecutivo</option>
</select>
</p>

<p>
<label for="empresa">Empresa:</label> <!-- CAMPO EMPRESA -->
<select name="empresa" >
<option value="TODAS">TODAS</option>
<?php
$qry = $db->query("SELECT * FROM empresas ORDER BY empnombre");
while($row = $qry->fetch(PDO::FETCH_ASSOC))
    echo '<option value='.$row['empid'].'>'.$row['empnombre'].'</option>';
?>
</select>
</p>

<p>
<label for="tipodocu">Tipo Documento:</label> <!-- CAMPO -->
<select name="tipodocu" >
<option value="">SELECCIONE</option>
<?php
$qry = $db->query("SELECT * FROM pyatipodocu ORDER BY pyatdnombre");
while($row = $qry->fetch(PDO::FETCH_ASSOC))
    echo '<option value='.$row['pyatdid'].'>'.$row['pyatdnombre'].'</option>';
?>
</select>
</p>

<p>
<label for="entregado">Entregado A:</label>
<select name="entregado">
<option value="">SELECCIONE</option>
<?php
$qry = $db->query("SELECT * FROM usuarios WHERE usuperfil <> 56 ORDER BY usunombre");
while($row = $qry->fetch(PDO::FETCH_ASSOC))
    echo '<option value='.$row['usuid'].'>'.$row['usunombre'].'</option>';
?>
</select>
</p>

						<div class="row">
							<div class="col-md-12 col-lg-6">
								<button type="submit" class="btnconsulta btn btn-primary btn-block mt-2" name="consultar" value="Buscar">consultar</button> <!-- BOTON CONSULTAR -->
							</div>
							<div class="col-md-12 col-lg-6">
								<button type="submit" class="btnconsulta btn btn-primary btn-block mt-2" name="show-all" value="Buscar">Mostrar Todo</button>							</div>
						</div>
</fieldset>
</form>
</article>
</article>
<?php require($r.'incluir/src/pie.php') ?>
</section>
<?php
if($_GET['error']) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>'.$_GET['error'].'</div>';
?>
</body>
</html>