<?php
$r = '../../../';
//INCLUIR SESION Y CONECCION
require($r.'incluir/session.php');
require($r.'incluir/connection.php');


	$empresa = $_GET['empresa'];
	$tdocumento = $_GET['tdocumento'];
	$consecutivo = $_GET['consecutivo'];
	$identificador = $_GET['identificador'];

	$qryiden = $db->query("SELECT * FROM gestiondocumental WHERE gdid = '".$identificador1."';");
	$rowiden = $qryiden->fetch(PDO::FETCH_ASSOC);
	$empresadet = $rowiden['gdempresa'];
	$consecutivodet = $rowiden['gdempresa'];
	$cajadet = $rowiden['gdcaja'];
	$carpetadet = $rowiden['gdcarpeta'];

$filtro = 'empresa='.$empresa.'&tdocumento='.$tdocumento.'&consecutivo='.$consecutivo;

if($_POST['insertar']){ // SE VALIDA SI LA INFORMACION VIENE DE INSERTAR 

	$identificador1 = $_POST['identificador1'];

	$qryiden = $db->query("SELECT * FROM gestiondocumental WHERE gdid = ".$identificador1."");
	$rowiden = $qryiden->fetch(PDO::FETCH_ASSOC);
	$empresadet = $rowiden['gdempresa'];
	$consecutivodet = $rowiden['gdconsecutivo'];
	$cajadet = $rowiden['gdcaja'];
	$carpetadet = $rowiden['gdcarpeta'];

	if ($_FILES['imagen']['name']){
		if($_FILES['imagen']['size'] < 1600000){
		
			if(move_uploaded_file($_FILES['imagen']['tmp_name'], $r.'pdf/gdocumental/'.$empresadet.'_'.$consecutivodet.'_'.$cajadet.'_'.$carpetadet.'.pdf')){

				$mensaje = 'Se ha ingresado correctamente el PDF.';
				header('Location:listar.php?mensaje='.$mensaje.'&'.$filtro);
				
			} else {
				$error = 'No se pudo cargar el PDF intente nuevamente o contacte con el encargado';
				header('Location:listar.php?error='.$error.'&'.$filtro);
			}
		} else {
			$error = 'la digitalizacon excede el tamano permitido (max. 1.5MB)';
			header('Location:listar.php?error='.$error.'&'.$filtro);
			exit();	
		}
	} 

}
$mensaje = $_GET['mensaje'];
$error = $_GET['error'];
?>
<!doctype html>
<html>
<head>

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
<script type="text/javascript" src="<?php echo $r ?>incluir/funciones.js"></script>
<script>
$(document).ready(function() {
	$('#form').validationEngine({ // VALIDACION DE CAMPOS REQUERIDOS
		showOneMessage:true,
		onValidationComplete: function(form, status){
        	if(status) {               
            	carga();
            	return true;
			}
		}
	});
	$('.btninsertar').button({ icons: { primary: 'ui-icon ui-icon-plusthick' }});
	$('#dialog-message').dialog({ // CARACTERISTICAS DE LA VENTANA MODAL
		height: 80,
		width: 'auto',
		modal: true
	});
	$('#viaje').datepicker({
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		minDate: '+0D',
		maxDate: '+50D',
	}).keypress(function(event) { event.preventDefault() });
	$('#regreso').datepicker({
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		minDate: '+0D',
		maxDate: '+50D',
	}).keypress(function(event) { event.preventDefault() });
});
</script>
<!-- ESTILOS EMBEBIDOS PARA MANEJAR FORMULARIO INDEPENDIENTEMENTE -->
<style type="text/css">
#form{ width:500px; margin:5px auto }
#form fieldset{ padding:10px; display:block }
#form legend{ font-weight: bold; margin-left:5px; padding:5px }
#form label{ display:inline-block; width:150px; text-align:right; margin:0.3em 2% 0 0 }
#form p{ margin:5px 0 }
</style>
</head>
<body>
<?php require($r.'incluir/src/login.php') ?>  <!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
<section id="principal">
<?php require($r.'incluir/src/cabeza.php') ?> <!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
<?php require($r.'incluir/src/menu.php') ?> <!-- INCLUIMOS MENU PRINCIPAL -->
<article id="cuerpo">
<article class="mapa">
<a href="#">Principal</a><div class="mapa_div"></div><a href="#">Administracion</a><div class="mapa_div"></div><a class="current">Gestion Documental</a>
</article>
<article id="contenido">
<div class="ui-widget">
<form id="form" name="form" action="anexpdf.php" method="post" enctype="multipart/form-data">
<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
<legend class="ui-widget ui-widget-header ui-corner-all">Anexo de PDF</legend>

<input type="hidden" name="identificador1" value="<?php echo $identificador ?>"  />

<p>
<label>PDF (max. 1.5 Mb): </label><input type="file" name="imagen" class="validate[checkFileType[pdf|PDF]]" />
</p>

<p class="boton">
<button type="submit" class="btn btn-primary btninsertar" name="insertar" value="insertar">Insertar</button> <!-- BOTON INSERTAR -->
</p>
</fieldset>
</form>
</div>
</article>
</article>
<?php require($r.'incluir/src/pie.php') ?>
</section>
<?php
if(isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>'.$error.'</div>'; //VENTANA MODAL ERROR
elseif(isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>'.$mensaje.'</div>'; //VENTANA MODAL EXITOSO
?>
</body>
</html>