<?php
$r = '../../../';
//INCLUIR SESION Y CONECCION
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
require ($r.'incluir/phpmailer/PHPMailer/class.phpmailer.php');
require ($r.'incluir/phpmailer/PHPMailer/class.smtp.php');

if($_POST['solicitar']){ // SE VALIDA SI LA INFORMACION 

$tipodocu = $_POST['tipodocu'];
$empresa = $_POST['empresa'];
$consecutivo = $_POST['consecutivo'];
$detalle = $_POST['detalle'];

	$qryusuario = $db->query("SELECT * FROM usuarios  WHERE usuid = ".$_SESSION['id'].";");
	$rowusuario = $qryusuario->fetch(PDO::FETCH_ASSOC);
	
	if (!empty($tipodocu)){
		$qrydocumen = $db->query("SELECT * FROM pyatipodocu  WHERE pyatdid = ".$tipodocu.";");
		$rowdocumen = $qrydocumen->fetch(PDO::FETCH_ASSOC);
		$tipodocumento = $rowdocumen['pyatdnombre'];
	}else {
		$tipodocumento = "";
	}

	$body = '
		<h2>Buen dia acontinuacion se detalla la solicitud de papeleria realizada por</h2>
		<h3>'.$rowusuario['usunombre'].'</h3>
		<p>
		<ul>
		<li>Empresa = '.$empresa.'</li>
		<li>Tipo de Documento = '.$tipodocumento.'</li>
		<li>Consecutivo = '.$consecutivo.'</li>
		<li>Detalle = '.$detalle.'</li>
		</ul>
		</p>
		
		<p style="font-size:11px;">los signos de puntuacion y caracteres especiales han sido omitidos de manera predeterminada para evitar inconvenientes con los diferentes servidores de correo</p>
		
		';
		
		$mail = new PHPMailer();
		$mail->IsSMTP(); // telling the class to use SMTP
		$mail->Host       = "smtp.gmail.com"; // SMTP server
		$mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
		// 1 = errors and messages
		// 2 = messages only
		$mail->SMTPAuth   = true;                  // enable SMTP authentication
		$mail->SMTPSecure = "ssl";                 // sets the prefix to the servier
		$mail->Host       = "smtp.gmail.com";      // sets GMAIL as the SMTP server
		$mail->Port       = 465;                   // set the SMTP port for the GMAIL server	
		$mail->Username   = "no-reply@idencorp.co";  // GMAIL username
		$mail->Password   = "IDT*2015!";            // GMAIL password
		$mail->SetFrom("no-reply@idencorp.co", "SOLICITUD PAPELERIA O ASEO");
		$mail->AddReplyTo("no-reply@idencorp.co", "SOLICITUD PAPELERIA O ASEO");
		$mail->Subject    = "Solicitud de Papaleria o aseo ";
		$mail->MsgHTML($body);
		$address = "gestiondocumentalybodega@idencorp.co";
		$address_copy = "";
		$mail->AddAddress($address);
		$mail->AddCC($address_copy);
		if(!$mail->Send()) {
		$error = "No se pudo realizar el envio de la solicitud por: " . $mail->ErrorInfo;
		header('Location:solicitar.php?error='.$error);
		} else {
			$qry = $db->query("INSERT INTO solicitudespape (solpapeempresa, solpapeconsecutivo, solpapetipodocu, solpapedetalle, solpapeesatdo, solpapeusuario) VALUES ('$empresa', '$consecutivo', '$tipodocu', '$detalle', '0', '".$_SESSION['id']."');"); 
			if($qry) {
				$mensaje = 'Se ha realizado la solicitud de manera correcta, se le dara respuesta pronto.';
				header('Location:solicitarpape.php?mensaje='.$mensaje);
			} else {
				$error = "Error al realizar la solicitud intente nuevamente o contacte con el encargado";
				header("Location:solicitarpape.php?error=".$error);
				exit();	
			}	
		}
		

}


$mensaje = $_GET['mensaje'];
$error = $_GET['error'];
?>
<!doctype html>
<html>
<head>

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
<script type="text/javascript" src="<?php echo $r ?>incluir/funciones.js"></script>
<script>
$(document).ready(function() {
	$('#form').validationEngine({ // VALIDACION DE CAMPOS REQUERIDOS
		showOneMessage:true,
		onValidationComplete: function(form, status){
        	if(status) {               
            	carga();
            	return true;
			}
		}
	});
	$('.btninsertar').button({ icons: { primary: 'ui-icon ui-icon-plusthick' }});
	$('#dialog-message').dialog({ // CARACTERISTICAS DE LA VENTANA MODAL
		height: 80,
		width: 'auto',
		modal: true
	});
	$('#viaje').datepicker({
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		minDate: '+0D',
		maxDate: '+50D',
	}).keypress(function(event) { event.preventDefault() });
	$('#regreso').datepicker({
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		minDate: '+0D',
		maxDate: '+50D',
	}).keypress(function(event) { event.preventDefault() });
});
</script>
<!-- ESTILOS EMBEBIDOS PARA MANEJAR FORMULARIO INDEPENDIENTEMENTE -->
<style type="text/css">
#form{ width:800px; margin:5px auto }
#form fieldset{ padding:10px; display:block }
#form legend{ font-weight: bold; margin-left:5px; padding:5px }
#form label{ display:inline-block; width:150px; text-align:right; margin:0.3em 2% 0 0 }
#form p{ margin:5px 0 }
</style>
</head>
<body>
<?php require($r.'incluir/src/login.php') ?>  <!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
<section id="principal">
<?php require($r.'incluir/src/cabeza.php') ?> <!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
<?php require($r.'incluir/src/menu.php') ?> <!-- INCLUIMOS MENU PRINCIPAL -->
<article id="cuerpo">
<article class="mapa">
<a href="#">Principal</a><div class="mapa_div"></div><a href="#">Administracion</a><div class="mapa_div"></div><a class="current">Papeleria y aseo</a>
</article>
<article id="contenido">
<div class="ui-widget">
<form id="form" name="form" action="solicitarpape.php" method="post">
<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
<legend class="ui-widget ui-widget-header ui-corner-all">Solicitud de papeleria</legend>

<p>
<label for="empresa">Empresa:</label> <!-- CAMPO EMPRESA -->
<select name="empresa" class="validate[required]">
<option value="TODAS">TODAS</option>
<?php
$qry = $db->query("SELECT * FROM empresas ORDER BY empnombre");
while($row = $qry->fetch(PDO::FETCH_ASSOC))
    echo '<option value='.$row['empid'].'>'.$row['empnombre'].'</option>';
?>
</select>
</p>

<p>
<label for="tipodocu">Tipo Documento:</label> <!-- CAMPO -->
<select name="tipodocu" class="validate[required]">
<option value="">SELECCIONE</option>
<?php
$qry = $db->query("SELECT * FROM pyatipodocu ORDER BY pyatdnombre");
while($row = $qry->fetch(PDO::FETCH_ASSOC))
    echo '<option value='.$row['pyatdid'].'>'.$row['pyatdnombre'].'</option>';
?>
</select>
</p>

<p>
<label for="consecutivo">Consecutivo:</label> <!-- CAMPO CONSECUTIVO -->
<input type="text" name="consecutivo" class="validate[required, custom[onlyNumberSp]] text-input" />
</p>

<label for="detalle" >Detalle: </label>
<textarea name="detalle" rows="5" class="validate[required]" cols="60" ></textarea>
</p>


<p class="boton">
<button type="submit" class="btninsertar" name="solicitar" value="solicitar">Solicitar</button> <!-- BOTON  -->
</p>
</fieldset>
</form>
</div>
</article>
</article>
<?php require($r.'incluir/src/pie.php') ?>
</section>
<?php
if(isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>'.$error.'</div>'; //VENTANA MODAL ERROR
elseif(isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>'.$mensaje.'</div>'; //VENTANA MODAL EXITOSO
?>
</body>
</html>