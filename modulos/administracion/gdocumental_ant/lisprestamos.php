<?php
$r = '../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
require($r.'incluir/funciones.php');

if($_GET['validar']){
	$identificador = $_GET['identificador'];
	
	$qry = $db->query("UPDATE solicitudesdocu SET soldocestado='1' WHERE soldocid='$identificador';"); 
	$num = $qry->rowCount();
	if ($num == 1){ 
		$mensaje = "Se ha validado exitosamente el documento.";
		header('Location:lisprestamos.php?mensaje='.$mensaje.'&'.$filtro);
	} else {
		$error ="No se ha podido validar el documento por favor contactese con el administrador";
		header('Location:lisprestamos.php?error='.$error.'&'.$filtro);
	} 

}

$sql = 'SELECT * FROM solicitudesdocu  INNER JOIN usuarios ON usuid = soldocusuario LEFT JOIN empresas ON empid = soldocempresa  LEFT JOIN gdtipodocumento ON tdid = soldoctipodocumento WHERE soldocestado = 0 ORDER BY soldocfechahorausuario desc';


$error = $_GET['error'];
$mensaje = $_GET['mensaje'];

$qry = $db->query($sql);
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
	<?php 
		require($r . 'incluir/src/head.php');
		require($r . 'incluir/src/head-form.php');
	?>	

<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#tabla').dataTable({
		'bJQueryUI': true,
		'bStateSave': true,
		'sPaginationType': 'full_numbers',
		'oLanguage': {
			'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
		},
		'bSort': false,
	});
	$('.btnatras').button({ icons: { primary: 'ui-icon ui-icon-arrowthick-1-w' }});
	$('#dialog-message').dialog({
		height: 80,
		width: 'auto',
		modal: true
	});
	$('.pdf').click (function(){
		newSrc = $(this).attr('data-rel');
		$('#modal').html("<iframe src='" + newSrc + "' width='100%' height='100%'></iframe>");
		$('#modal').dialog({ modal: true, width: '800', height: '900', title : 'PDF de la factura' });
	});
	$('.historico').click(function(){
		newHref = $(this).attr('data-rel');
		$('#modal').load(newHref).dialog({ modal:true, width: 800 }); 
    });
	$('.msj_validar').click(function(e){
		e.preventDefault();
		var targetUrl = $(this).attr('href');
		var $dialog_link_follow_confirm = $('<div></div>').html("<p><span class='ui-icon ui-icon-alert' style='float:left; margin:2px 7px 7px 0;'></span>Esta seguro que desea validar la devolucion?</p>").
		dialog({
			title: 'Porfavor confirmar',
			buttons :{
				'Si' : function() { 
					window.location.href = targetUrl; 
				},
				'No' : function() { 
					$(this).dialog("close"); 
				}
			},
			modal: true,
			width: 'auto',
			height: 'auto'
		}
	);
	$dialog_link_follow_confirm.dialog("open");
	});
});
</script>
</head>
<body>
<?php require($r.'incluir/src/login.php') ?>
<section id="principal">
<?php require($r.'incluir/src/cabeza.php') ?>
<?php require($r.'incluir/src/menu.php') ?>
<article id="cuerpo">
<article class="mapa">
<a href="#">Principal</a><div class="mapa_div"></div><a class="current">Gestion Documental</a>
</article>
<article id="contenido">
<h2>Listado Prestamos <?php  $sql ?></h2>
<table id="tabla">
<thead>
<tr>
<th>Id</th>
<th>Fecha Creacion</th>
<th>Usuario Creacion</th>
<th>Empresa</th>
<th>T. Documento</th>
<th>Consecutivo</th>

<th></th>
</tr>
</thead>
<tbody>
<?php
$i = 0;
while($row = $qry->fetch(PDO::FETCH_ASSOC)){
	
?>
<tr>
<td><?php echo $row['soldocid'] ?></td>
<td align="center"><?php echo $row['soldocfechahorausuario'] ?></td>
<td align="center"><?php echo $row['usunombre']  ?></td>
<td><?php echo $row['empnombre']  ?></td>
<td align="center"><?php echo $row['tdnombre']  ?></td>
<td align="center"><?php echo $row['soldocconsecutivo'] ?></td>


<td align="center"><a href="lisprestamos.php?validar=1&identificador=<?php echo $row['soldocid']."&".$filtro ?>" class="msj_validar" title="Validar"><img src="<?php echo $r ?>imagenes/iconos/thumb_up.png" /></a></td>
</tr>
<?php } ?>
</tbody>
</table>
</article>
</article>
<?php require($r.'incluir/src/pie.php') ?>
</section>
<?php
if(isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>'.$error.'</div>';
elseif(isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>'.$mensaje.'</div>';
?>
<div id="modal" style="display:none"></div>
<div id="dialog2" style="display:none"></div>
</body>
</html>