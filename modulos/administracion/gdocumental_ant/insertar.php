<?php

$r = '../../../';
//INCLUIR SESION Y CONECCION
require($r.'incluir/session.php');
require($r.'incluir/connection.php');

if($_POST['insertar']){ // SE VALIDA SI LA INFORMACION VIENE DE INSERTAR 

$empresa = $_POST['empresa'];
$tdocumento = $_POST['tdocumento'];
$consecutivo = $_POST['consecutivo'];
$caja = $_POST['caja'];
$carpeta = $_POST['carpeta'];
$imagen = $_POST['imagen'];


	if ($_FILES['imagen']['name']){
		if($_FILES['imagen']['size'] < 1600000){
		
			if(move_uploaded_file($_FILES['imagen']['tmp_name'], $r.'pdf/gdocumental/'.$empresa.'_'.$consecutivo.'_'.$caja.'_'.$carpeta.'.pdf')){
				$qry = $db->query("INSERT INTO gestiondocumental (gdempresa, gdtipodocumento, gdconsecutivo, gdcaja, gdcarpeta, gdusuario) VALUES ('$empresa', '$tdocumento', '$consecutivo', '$caja', '$carpeta', '".$_SESSION['id']."');	"); 
				if($qry) {
					$mensaje = 'Se ha ingresado correctamente junto con el PDF.';
					header('Location:insertar.php?mensaje='.$mensaje);
					exit();
				} else {
					$error = 'Error al insertar junto con imagen intente nuevamente o contacte con el encargado';
					header('Location:insertar.php?error='.$error);
					exit();	
				}
				
			} else {
				$error = 'No se pudo cargar el PDF intente nuevamente o contacte con el encargado';
				header('Location:insertar.php?error='.$error);
			}
		} else {
			$error = 'la digitalizacon excede el tamano permitido (max. 1.5MB)';
			header('Location:insertar.php?error='.$error);
			exit();	
		}
	} else {
		$qry = $db->query("INSERT INTO gestiondocumental (gdempresa, gdtipodocumento, gdconsecutivo, gdcaja, gdcarpeta, gdusuario) VALUES ('$empresa', '$tdocumento', '$consecutivo', '$caja', '$carpeta', '".$_SESSION['id']."');	"); 
		if($qry) {
			$mensaje = 'Se ha ingresado correctamente recuerde que anexar el PDF es importante.';
			header('Location:insertar.php?mensaje='.$mensaje);
			exit();
		} else {
			$error = 'Error al insertar intente nuevamente o contacte con el encargado';
			header('Location:insertar.php?error='.$error);
			exit();	
		}	
	}

}
$mensaje = $_GET['mensaje'];
$error = $_GET['error'];
?>
<!doctype html>
<html>
<head>

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
<script type="text/javascript" src="<?php echo $r ?>incluir/funciones.js"></script>
<script>
$(document).ready(function() {
	$('#form').validationEngine({ // VALIDACION DE CAMPOS REQUERIDOS
		showOneMessage:true,
		onValidationComplete: function(form, status){
        	if(status) {               
            	carga();
            	return true;
			}
		}
	});
	$('.btninsertar').button({ icons: { primary: 'ui-icon ui-icon-plusthick' }});
	$('#dialog-message').dialog({ // CARACTERISTICAS DE LA VENTANA MODAL
		height: 80,
		width: 'auto',
		modal: true
	});
	$('#viaje').datepicker({
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		minDate: '+0D',
		maxDate: '+50D',
	}).keypress(function(event) { event.preventDefault() });
	$('#regreso').datepicker({
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		minDate: '+0D',
		maxDate: '+50D',
	}).keypress(function(event) { event.preventDefault() });
});
</script>
<!-- ESTILOS EMBEBIDOS PARA MANEJAR FORMULARIO INDEPENDIENTEMENTE -->
<style type="text/css">
#form{ width:800px; margin:5px auto }
#form fieldset{ padding:10px; display:block }
#form legend{ font-weight: bold; margin-left:5px; padding:5px }
#form label{ display:inline-block; width:150px; text-align:right; margin:0.3em 2% 0 0 }
#form p{ margin:5px 0 }
</style>
</head>
<body>
<?php require($r.'incluir/src/login.php') ?>  <!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
<section id="principal">
<?php require($r.'incluir/src/cabeza.php') ?> <!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
<?php require($r.'incluir/src/menu.php') ?> <!-- INCLUIMOS MENU PRINCIPAL -->
<article id="cuerpo">
<article class="mapa">
<a href="#">Principal</a><div class="mapa_div"></div><a href="#">Administracion</a><div class="mapa_div"></div><a class="current">Gestion Documental</a>
</article>
<article id="contenido">
<div class="ui-widget">
<form id="form" name="form" action="insertar.php" method="post" enctype="multipart/form-data">
<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
<legend class="ui-widget ui-widget-header ui-corner-all">Insertar Gestion Documental</legend>

<p>
<label for="empresa">Empresa:</label> <!-- CAMPO EMPRESA -->
<select name="empresa" class="validate[required]">
<option value="">SELECCIONE</option>
<?php
$qry = $db->query("SELECT * FROM empresas ORDER BY empnombre");
while($row = $qry->fetch(PDO::FETCH_ASSOC))
    echo '<option value='.$row['empid'].'>'.$row['empnombre'].'</option>';
?>
</select>
</p>

<p>
<label for="tdocumento">T. Docuemnto:</label> <!-- CAMPO T DOCUMENTO -->
<select name="tdocumento" class="validate[required]">
<option value="">SELECCIONE</option>
<?php
$qry = $db->query("SELECT * FROM gdtipodocumento ORDER BY tdnombre");
while($row = $qry->fetch(PDO::FETCH_ASSOC))
    echo '<option value='.$row['tdid'].'>'.$row['tdnombre'].'</option>';
?>
</select>
</p>

<p>
<label for="consecutivo">Consecutivo:</label> <!-- CAMPO CONSECUTIVO -->
<input type="text" name="consecutivo" class="validate[required, custom[onlyNumberSp]] text-input" />
</p>

<p>
<label for="caja">Caja:</label> <!-- CAMPO CAJA -->
<input type="text" name="caja" class="validate[required, custom[onlyNumberSp]] text-input" />
</p>

<p>
<label for="carpeta">Carpeta:</label> <!-- CAMPO CARPETA-->
<input type="text" name="carpeta" class="validate[required, custom[onlyNumberSp]] text-input" />
</p>

<p>
<label>PDF (max. 1.5 Mb): </label><input type="file" name="imagen" class="validate[checkFileType[pdf|PDF]]" />
</p>

<p class="boton">
<button type="submit" class="btn btn-primary btninsertar" name="insertar" value="insertar">Insertar</button> <!-- BOTON INSERTAR -->
</p>
</fieldset>
</form>
</div>
</article>
</article>
<?php require($r.'incluir/src/pie.php') ?>
</section>
<?php
if(isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>'.$error.'</div>'; //VENTANA MODAL ERROR
elseif(isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>'.$mensaje.'</div>'; //VENTANA MODAL EXITOSO
?>
</body>
</html>