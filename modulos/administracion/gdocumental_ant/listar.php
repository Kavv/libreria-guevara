<?php
$r = '../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
require($r.'incluir/funciones.php');

if(isset($_POST['consultar'])){
	$empresa = $_POST['empresa'];
	$tdocumento = $_POST['tdocumento'];
	$consecutivo = $_POST['consecutivo'];
}else{
	$empresa = $_GET['empresa'];
	$tdocumento = $_GET['tdocumento'];
	$consecutivo = $_GET['consecutivo'];
}

if($_GET['eliminar']){
	$identificador = $_GET['identificador'];
	$empresa = $_GET['empresa'];
	$tdocumento = $_GET['tdocumento'];
	$consecutivo = $_GET['consecutivo'];
	$filtro = 'empresa='.$empresa.'&tdocumento='.$tdocumento.'&consecutivo='.$consecutivo;
	
	$qry = $db->query("DELETE FROM gestiondocumental WHERE gdid='$identificador';"); 
	$num = $qry->rowCount();
	if ($num == 1){ 
		$mensaje = "Se ha eliminado exitosamente el documento.";
		header('Location:listar.php?mensaje='.$mensaje.'&'.$filtro);
	} else {
		$error ="No se ha podido eliminar el documento por favor contactese con el administrador";
		header('Location:listar.php?error='.$error.'&'.$filtro);
	} 

}

$filtro = 'empresa='.$empresa.'&tdocumento='.$tdocumento.'&consecutivo='.$consecutivo;
$con = 'SELECT * FROM gestiondocumental INNER JOIN empresas ON empid = gdempresa  INNER JOIN usuarios ON usuid = gdusuario INNER JOIN gdtipodocumento ON tdid = gdtipodocumento ';
$ord = 'ORDER BY gdid desc';
if($empresa == '' && $tdocumento == '' && $consecutivo == '') $sql = "$con  $ord";
elseif($empresa != '' && $tdocumento == '' && $consecutivo == '') $sql = "$con WHERE gdempresa ='$empresa'  $ord";
elseif($empresa == '' && $tdocumento != '' && $consecutivo == '') $sql = "$con WHERE gdtipodocumento ='$tdocumento' $ord";
elseif($empresa == '' && $tdocumento == '' && $consecutivo != '') $sql = "$con WHERE gdconsecutivo ='$consecutivo' $ord";
elseif($empresa != '' && $tdocumento != '' && $consecutivo == '') $sql = "$con WHERE gdtipodocumento ='$tdocumento' AND gdempresa ='$empresa' $ord";
elseif($empresa == '' && $tdocumento != '' && $consecutivo != '') $sql = "$con WHERE gdtipodocumento ='$tdocumento' AND gdconsecutivo ='$consecutivo' $ord";
elseif($empresa != '' && $tdocumento == '' && $consecutivo != '') $sql = "$con WHERE gdempresa ='$empresa' AND gdconsecutivo ='$consecutivo' $ord";
elseif($empresa != '' && $tdocumento != '' && $consecutivo != '') $sql = "$con WHERE gdempresa ='$empresa' AND gdconsecutivo ='$consecutivo' AND gdtipodocumento ='$tdocumento' $ord";

$error = $_GET['error'];
$mensaje = $_GET['mensaje'];

$qry = $db->query($sql);
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
	<?php 
		require($r . 'incluir/src/head.php');
		require($r . 'incluir/src/head-form.php');
	?>	

<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#tabla').dataTable({
		'bJQueryUI': true,
		'bStateSave': true,
		'sPaginationType': 'full_numbers',
		'oLanguage': {
			'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
		},
		'bSort': false,
	});
	$('.btnatras').button({ icons: { primary: 'ui-icon ui-icon-arrowthick-1-w' }});
	$('#dialog-message').dialog({
		height: 80,
		width: 'auto',
		modal: true
	});
	$('.pdf').click (function(){
		newSrc = $(this).attr('data-rel');
		$('#modal').html("<iframe src='" + newSrc + "' width='100%' height='100%'></iframe>");
		$('#modal').dialog({ modal: true, width: '800', height: '900', title : 'PDF de la factura' });
	});
	$('.historico').click(function(){
		newHref = $(this).attr('data-rel');
		$('#modal').load(newHref).dialog({ modal:true, width: 800 }); 
    });
	$('.msj_eliminar').click(function(e){
		e.preventDefault();
		var targetUrl = $(this).attr('href');
		var $dialog_link_follow_confirm = $('<div></div>').html("<p><span class='ui-icon ui-icon-alert' style='float:left; margin:2px 7px 7px 0;'></span>Esta seguro que desea eliminar el Documento?</p>").
		dialog({
			title: 'Porfavor confirmar',
			buttons :{
				'Si' : function() { 
					window.location.href = targetUrl; 
				},
				'No' : function() { 
					$(this).dialog("close"); 
				}
			},
			modal: true,
			width: 'auto',
			height: 'auto'
		}
	);
	$dialog_link_follow_confirm.dialog("open");
	});
});
</script>
</head>
<body>
<?php require($r.'incluir/src/login.php') ?>
<section id="principal">
<?php require($r.'incluir/src/cabeza.php') ?>
<?php require($r.'incluir/src/menu.php') ?>
<article id="cuerpo">
<article class="mapa">
<a href="#">Principal</a><div class="mapa_div"></div><a class="current">Gestion Documental</a>
</article>
<article id="contenido">
<h2>Listado Documentos <?php  $sql ?></h2>
<table id="tabla">
<thead>
<tr>
<th>Id</th>
<th>Fecha Creacion</th>
<th>Usuario Creacion</th>
<th>Empresa</th>
<th>T. Documento</th>
<th>Consecutivo</th>
<th>Caja</th>
<th>Carpeta</th>
<th></th>
<th></th>
<th></th>
</tr>
</thead>
<tbody>
<?php
$i = 0;
while($row = $qry->fetch(PDO::FETCH_ASSOC)){
	
?>
<tr>
<td><?php echo $row['gdid'] ?></td>
<td align="center"><?php echo $row['gdfechahorausuario'] ?></td>
<td align="center"><?php echo $row['usunombre']  ?></td>
<td><?php echo $row['empnombre']  ?></td>
<td align="center"><?php echo $row['tdnombre']  ?></td>
<td align="center"><?php echo $row['gdconsecutivo'] ?></td>
<td align="center"><?php echo $row['gdcaja'] ?></td>
<td align="center"><?php echo $row['gdcarpeta'] ?></td>

<td align="center"><img src="<?php echo $r ?>imagenes/iconos/pdf.png" class="pdf" data-rel="<?php echo $r.'pdf/gdocumental/'.$row['empid'].'_'.$row['gdconsecutivo'].'_'.$row['gdcaja'].'_'.$row['gdcarpeta'].'.pdf' ?>" title="Comprobante" /></td>

<td align="center"><a href="anexpdf.php?identificador=<?php echo $row['gdid']."&".$filtro ?>" title="Anexar PDF"><img src="<?php echo $r ?>imagenes/iconos/pdf_add.png" /></a></td>

<td align="center"><a href="listar.php?eliminar=1&identificador=<?php echo $row['gdid']."&".$filtro ?>" class="msj_eliminar" title="Eliminar"><img src="<?php echo $r ?>imagenes/iconos/cancelar.png" /></a></td>
</tr>
<?php } ?>
</tbody>
</table>
<p class="boton">
<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href='consultar.php'">Atras</button>
</p>
</article>
</article>
<?php require($r.'incluir/src/pie.php') ?>
</section>
<?php
if(isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>'.$error.'</div>';
elseif(isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>'.$mensaje.'</div>';
?>
<div id="modal" style="display:none"></div>
<div id="dialog2" style="display:none"></div>
</body>
</html>