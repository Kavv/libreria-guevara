<?php
$r = '../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
	<?php 
		require($r . 'incluir/src/head.php');
		require($r . 'incluir/src/head-form.php');
	?>	

<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#tabla').dataTable({
		'bJQueryUI': true,
		'bStateSave': true,
		'sPaginationType': 'full_numbers',
		'oLanguage': {
			'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
		},
		'bSort': false,
	});
	$('.btnatras').button({ icons: { primary: 'ui-icon ui-icon-arrowthick-1-w' }});
});
</script>
</head>
<body>
<?php require($r.'incluir/src/login.php') ?>
<section id="principal">
<?php require($r.'incluir/src/cabeza.php') ?>
<?php require($r.'incluir/src/menu.php') ?>
<article id="cuerpo">
<article class="mapa">
<a href="#">Principal</a><div class="mapa_div"></div><a href="#">Administracion</a><div class="mapa_div"></div><a class="current">Resultados Consulta Masiva</a>
</article>
<article id="contenido">
<?php 
$uploaddir = 'archivos_csv/';
$uploadfile = $uploaddir . "consulta_masiva.csv";


if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
    echo " <h2>Consulta Masiva, El archivo fue cargado exitosamente. </h2>";
} else {
    echo " <h2>Consulta Masiva, ¡Hubo un error por favor intenta nuevamente! </h2>";
}

$file = file('archivos_csv/consulta_masiva.csv');

?>
<div class="reporte">
<a href="#"><img src="<?php echo $r ?>imagenes/iconos/pdf.png" title="PDF" /></a><a href="#"><img src="<?php echo $r ?>imagenes/iconos/excel.png" title="CSV" /></a>
</div>
<table id="tabla">
<thead>
<tr>
<th>Cliente</th>
<th>Nombre 1</th>
<th>Nombre 2</th>
<th>Apellido 1 </th>
<th>Apellido 2</th>
<th>Emial</th>
</tr>
</thead>
<tbody>
<?php

foreach ($file as $num => $fila) 
{
$sql = "SELECT * FROM  clientes  WHERE cliid = ".$fila." ;";

$qry = $db->query($sql);
$num = $qry->rowCount();

if ($num == 1) {

while($row = $qry->fetch(PDO::FETCH_ASSOC)){
	echo "<tr>";
	echo "<td align='center'>". $row['cliid'] ."</td>";
	echo "<td align='center'>". $row['clinombre'] ."</td>";
	echo "<td align='center'>". $row['clinom2'] ."</td>";
	echo "<td align='center'>". $row['cliape1'] ."</td>";
	echo "<td align='center'>". $row['cliape2'] ."</td>";
	echo "<td align='center'>". $row['cliemail'] ."</td>";
	echo "</tr>";
} // Fin While

} else {

	echo "<tr>";
	echo "<td align='center'>NONE</td>";
	echo "<td align='center'>NONE</td>";
	echo "<td style='color:red' align='center'>". $fila ."</td>";
	echo "<td align='center'> NO SE ENCONTRARON RESULTADOS CON ESTA IDENTIFICACION</td>";
	echo "<td align='center'> NONE </td>";
	echo "<td align='center'> NONE </td>";
	echo "</tr>";

}



} // Fin Foreach
?>
</tbody>
</table>
<p class="boton">
<button type="button" class="btnatras" onClick=" carga(); location.href = 'exportar.php'">Atras</button>
</p>
</article>
</article>
<?php require($r.'incluir/src/pie.php') ?>
</section>
</body>
</html>