<?php
$r = '../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
?>
<!doctype html>
<html>
<head>
	<?php 
		require($r . 'incluir/src/head.php');
		require($r . 'incluir/src/head-form.php');
	?>
<script type="text/javascript">
$(document).ready(function(){
	$('#form').validationEngine({
		showOneMessage:true,
		onValidationComplete: function(form, status){
			if(status == true) {
				carga();
				return true;
			}
		}
	});
	$('.btncargar').button({ icons: { primary: 'ui-icon ui-icon-document' }});
	$( "#dialog-message" ).dialog({
		height: 80,
		width: 'auto',
		modal: true
	});
});
</script>
<style type="text/css">
#form form{ width:280px }
#form fieldset{ padding:10px; display:block; width:280px; margin:20px auto }
#form legend{ font-weight: bold; margin-left:5px; padding:5px }
#form label{ display:inline-block; width:75px; text-align:right; margin:0.3em 2% 0 0 }
#form p{ margin:5px 0 }
</style>
</head>
<body>
<?php require($r.'incluir/src/login.php') ?>
<section id="principal">
<?php require($r.'incluir/src/cabeza.php') ?>
<?php require($r.'incluir/src/menu.php') ?>
<article id="cuerpo">
<article class="mapa">
<a href="#">Principal</a><div class="mapa_div"></div><a href="#">Administracion</a><div class="mapa_div"></div><a class="current">Clientes</a>
</article>
<article id="contenido">
<form id="form" name="form" action="cargar_consulta_masiva.php" method="post" enctype="multipart/form-data">
<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
<legend class="ui-widget ui-widget-header ui-corner-all">Cargar archivo para Consulta Masiva</legend>
<p>Recuerde que el archivo a subir debe ser .CSV delimitado por comas.</p>
<input type="file" id="archivo" name="userfile" type="file" class="validate[required], checkFileType[csv|CSV]" />
<!-- MAX_FILE_SIZE debe preceder el campo de entrada de archivo -->
<input type="hidden" name="MAX_FILE_SIZE" value="30000" />
<p class="boton">
<button type="submit" class="btncargar" name="cargar" value="cargar">Cargar</button>
</p>
</fieldset>
</form>
</article>
</article>
<?php require($r.'incluir/src/pie.php') ?>
</section>
</body>
</html>