<?php
// LA ACCION PRINCIPAL DE ESTA PAGINA ES MODIFICAR UN CLIENTE ENVIANDO LA INFORMACION A LISTAR.PHP

$r = '../../../';
//INCLUIR SESION Y CONECCION
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

if (isset($_POST['insertar'])) { //VALIDAMOS SI POST MODIFICAR ES TRUE
    $id = strtoupper(trim($_POST['id']));
    $nombre = strtoupper(trim($_POST['nom1']));  // LO RECIBIDO DE ESTE CAMPO LO CAMBIAMOS A MAYUSCULA
    $nom2 = strtoupper(trim($_POST['nom2']));  // LO RECIBIDO DE ESTE CAMPO LO CAMBIAMOS A MAYUSCULA
    $ape1 = strtoupper(trim($_POST['ape1']));  // LO RECIBIDO DE ESTE CAMPO LO CAMBIAMOS A MAYUSCULA
    $ape2 = strtoupper(trim($_POST['ape2']));  // LO RECIBIDO DE ESTE CAMPO LO CAMBIAMOS A MAYUSCULA
    $email = strtolower(trim($_POST['email']));  // LO RECIBIDO DE ESTE CAMPO LO CAMBIAMOS A MAYUSCULA
    $barresidencia = trim(strtoupper($_POST['barresidencia']));  // LO RECIBIDO DE ESTE CAMPO LO CAMBIAMOS A MAYUSCULA
    $dirresidencia = trim(strtoupper($_POST['dirresidencia']));  // LO RECIBIDO DE ESTE CAMPO LO CAMBIAMOS A MAYUSCULA
    $barcomercio = trim(strtoupper($_POST['barcomercio']));  // LO RECIBIDO DE ESTE CAMPO LO CAMBIAMOS A MAYUSCULA
    $dircomercio = trim(strtoupper($_POST['dircomercio']));  // LO RECIBIDO DE ESTE CAMPO LO CAMBIAMOS A MAYUSCULA

    $qry = $db->query("INSERT INTO clientes (
        cliid, clinombre, clinom2, cliape1, cliape2, cliemail, clicelular, clidepresidencia, 
        cliciuresidencia, clibarresidencia, clidirresidencia, clitelresidencia, clidepcomercio, 
        cliciucomercio, clibarcomercio, clidircomercio, clitelcomercio
    )
    VALUES ( 
        '$id', '$nombre', '$nom2', '$ape1', '$ape2', '$email', '" . $_POST['celular'] . "', 
        '" . $_POST['depresidencia'] . "', '" . $_POST['ciuresidencia'] . "','$barresidencia', '$dirresidencia',
        '" . $_POST['telresidencia'] . "', '" . $_POST['depcomercio'] . "', '" . $_POST['ciucomercio'] . "',
        '$barcomercio', '$dircomercio', '" . $_POST['telcomercio'] . "'
    )");

    if ($qry) {
        $mensaje = 'Se inserto el cliente'; //MENSAJE EXITOSO
        header("location:insertar.php?mensaje=".$mensaje);
    }
    else {
        $error = 'No se inserto el cliente'; //MENSAJE ERROR
        header("location:insertar.php?error=".$error);
    }
}
?>
<!doctype html>
<html lang="es">

<head>
    <title>AGREGAR CLIENTE</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">

    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

    <script>
        $(document).ready(function() {
			var die_depa_res = -1;
			var dir_ciudad_res = -1;

			var die_depa_co = -1;
			var dir_ciudad_co = -1;

            $('#depresidencia').change(function(event) {
                var id1 = $('#depresidencia').find(':selected').val();
                $("#ciuresidencia").load('ciudades-re.php?depa=' + id1 + '&die_depa_res=' + die_depa_res + '&dir_ciudad_res=' + dir_ciudad_res);
            });
            $('#depcomercio').change(function(event) {
                var id1 = $('#depcomercio').find(':selected').val();
                $("#ciucomercio").load('ciudades-co.php?depa=' + id1 + '&die_depa_co=' + die_depa_co + '&dir_ciudad_co=' + dir_ciudad_co);
            });
            $("#depresidencia").selectpicker();
            
        });
    </script>
    <!-- ESTILOS EMBEBIDOS PARA MANEJAR FORMULARIO INDEPENDIENTEMENTE -->
    <style type="text/css">
    </style>
</head>

<body>
    <?php require($r . 'incluir/src/login.php') ?>
    <!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
    <section id="principal">
        <?php require($r . 'incluir/src/cabeza.php') ?>
        <!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
        <?php require($r . 'incluir/src/menu.php') ?>
        <!-- INCLUIMOS MENU PRINCIPAL -->
        <article id="cuerpo">
            <article class="mapa">
                <a href="#">Principal</a>
                <div class="mapa_div"></div><a href="#">Administracion</a>
                <div class="mapa_div"></div><a class="current">Clientes</a>
            </article>
            <article id="contenido">
                <div class="ui-widget">
                    <form id="form" name="form" action="insertar.php?" method="post">
                        <!-- FORMULARIO ENVIADO POR POST A LISTAR.PHP JUNTO CON EL FILTRO GENERAL  -->
                        <fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
                            <legend class="ui-widget ui-widget-header ui-corner-all">Insertar cliente</legend>
                            <p> 
                                <label for="id">Identificacion:</label> <!-- NUM IDENTIFICACION -->
                                <input type="text" name="id" class="id validate[required, ajax[ajaxUserCall]]" />
                                
                            </p>
                            <p>
                                <label for="nom1">Nombre 1:</label> <!-- CAMPO PRIMER NOMBRE  -->
                                <input type="text" name="nom1" style="text-transform: uppercase" size="10"  required />
                                <label for="nom2">Nombre 2:</label> <!-- CAMPO SEGUNDO NOMBRE -->
                                <input type="text" name="nom2" style="text-transform: uppercase" size="10"  />
                            </p>
                            <p>
                                <label for="ape1">Apellido 1:</label> <!-- CAMPO PRIMER APELLIDO -->
                                <input type="text" name="ape1" style="text-transform: uppercase" size="10"  required />
                                <label for="ape2">Apellido 2:</label> <!-- CAMPO SEGUNDO APELLIDO -->
                                <input type="text" name="ape2" style="text-transform: uppercase" size="10"  />
                            </p>
                            <p>
                                <label for="ape1">Email:</label> <!-- CAMPO EMAIL -->
                                <input type="text" class="email" name="email" />
                            </p>
                            <p>
                                <label>Celular: </label>
                                <input type="text" name="celular" class="telefono validate[required, custom[phone]] text-input" title="Digite numero celular" /> <!-- CAMPO NUM DE CELULAR -->
                            </p>
                        

                            <fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12">
                                <legend class="ui-widget ui-widget-header ui-corner-all">Datos ubicacion residencia</legend> <!-- DATOS DE UBICACION 1 -->
                                <p>
                                    <label>Departamento:</label> <!-- CAMPO DEPARTAMENTO -->
                                    <select id="depresidencia" name="depresidencia" class="selectpicker" required 
                                    data-live-search="true" title="Seleccione un departamento" data-width="100%">
                                        <option value="">SELECCIONE</option>
                                        <?php
                                            $qry = $db->query("SELECT * FROM departamentos ORDER BY depnombre");
                                            while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
                                                echo '<option value=' . $row2['depid'] . '>' . $row2['depnombre'] . '</option>'; // APARECEN TODOS LOS DEPARTAMENTOS PARA QUE EL USUARIO ELIGA EL PERTINENTE
                                            }
                                        ?>
                                    </select>
                                </p>
                                <p>
                                    <label>Ciudad: </label> <!-- CAMPO CIUDAD -->
                                    <select name="ciuresidencia" id="ciuresidencia" class="validate[required] text-input">

                                    </select>
                                </p>
							    <div style="visibility: hidden;height: 0;">
                                    <label>Barrio: </label>
                                    <input type="text" name="barresidencia" class="text-input" /> <!-- CAMPO BARRIO -->
                                </div>
                                <p>
                                    <label>Direccion: </label>
                                    <input type="text" name="dirresidencia" class="direccion validate[required] text-input" /> <!-- CAMPO DIRECCION -->
                                </p>
                                <p>
                                    <label>Telefono: </label>
                                    <input type="text" name="telresidencia" class="telefono validate[required, custom[phone]] text-input" /> <!-- CAMPO TELEFONO -->
                                </p>
                            </fieldset>
                            
                            <br>
                            <fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12">
                                <legend class="ui-widget ui-widget-header ui-corner-all">Datos ubicacion Comercio</legend> <!-- DATOS DE UBICACION 2 -->
                                <p>
                                    <label>Departamento:</label> <!-- CAMPO DEPARTAMENTO 2 -->
                                    
                                    <select id="depcomercio" name="depcomercio" class="selectpicker"  
                                    data-live-search="true" title="Seleccione un departamento" data-width="100%">
                                        <option value="">SELECCIONE</option>'
                                        <?php
                                            $qry = $db->query("SELECT * FROM departamentos ORDER BY depnombre");
                                            while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
                                                echo '<option value=' . $row2['depid'] . '>' . $row2['depnombre'] . '</option>'; // APARECEN TODOS LOS DEPARTAMENTOS PARA QUE EL USUARIO ELIGA EL PERTINENTE
                                            }
                                        ?>
                                    </select>
                                </p>
                                <p>
                                    <label>Ciudad:</label> <!-- CAMPO CIUDAD 2 -->
                                    <select name="ciucomercio" id="ciucomercio" class="text-input">
                                    </select>
                                </p>
							    <div style="visibility: hidden;height: 0;">
                                    <label>Barrio: </label>
                                    <input type="text" name="barcomercio" class="barrio  text-input" /> <!-- CAMPO BARRIO -->
                                        </div>
                                <p>
                                    <label>Direccion: </label>
                                    <input type="text" name="dircomercio" class="direccion  text-input" /> <!-- CAMPO DIRECCION -->
                                </p>
                                <p>
                                    <label>Telefono: </label>
                                    <input type="text" name="telcomercio" class="telefono validate[custom[phone]] text-input" /> <!-- CAMPO TELEFONO -->
                                </p>
                            </fieldset>

                            <p class="boton">
                                <button type="submit" class="btn btn-primary btninsertar" name="insertar" value="insertar">Insertar</button> <!-- BOTON MODIFICAR -->
                            </p>
                        </fieldset>
                    </form>
                </div>
            </article>
        </article>
        <?php require($r . 'incluir/src/pie.php') ?>
        <!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
    </section>
	<?php
	if (isset($_GET['error'])) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['error'] . '</div>'; //VENTANA MODAL ERROR
	elseif (isset($_GET['mensaje'])) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['mensaje'] . '</div>'; //VENTANA MODAL EXITOSO
	?>
</body>

</html>