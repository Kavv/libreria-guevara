<?php
// LA ACCION PRINCIPAL ES ESTA PAGINA ES CONSULTAR LOS CLIENTES REGISTRADOS

$r = '../../../';
//INCLUIR SESION Y CONECCION
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
?>
<!doctype html>
<html lang="es">

<head>
    <title>CONSULTAR CLIENTE</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>

	<!-- ESTILOS EMBEBIDOS PARA MANEJAR FORMULARIO INDEPENDIENTEMENTE -->
	<style type="text/css">

	</style>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
		<?php require($r . 'incluir/src/menu.php') ?>
		<!-- INCLUIMOS MENU PRINCIPAL -->
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Administracion</a>
				<div class="mapa_div"></div><a class="current">Clientes</a>
			</article>
			<article id="contenido">
				<div class="ui-widget">
					<form id="form" name="form" action="listar.php" method="post">
						<!-- ENVIO FORMULARIO POR POST A LISTAR.PHP -->
						<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
							<legend class="ui-widget ui-widget-header ui-corner-all">Consultar clientes</legend>
							<p>
								<label for="id">Identificacion:</label>
								<input type="text" name="id" class="id text-input" title="Digite la identificacion del cliente" /> <!-- CONSULTAR CLIENTE POR NUMERO DE IDENTIFICACION -->
							</p>
							<p>
								<label for="nombre">Primer Nombre:</label>
								<input type="text" name="nombre" class="nombre text-input" title="Digite el primer nombre del cliente" /> 
							</p>
							<p>
								<label for="nombre">Segundo Nombre:</label>
								<input type="text" name="nom2" class="nombre  text-input" title="Digite el segundo nombre del cliente" /> 
							</p>
							<p>
								<label for="nombre">Primer Apellido:</label>
								<input type="text" name="ape1" class="nombre  text-input" title="Digite el primer apellido del cliente" /> 
							</p>
							<p>
								<label for="nombre">Segundo Apellido:</label>
								<input type="text" name="ape2" class="nombre text-input" title="Digite el segundo apellido del cliente" /> 
							</p>
							<p>
								<label for="nombre">Teléfono:</label>
								<input type="text" name="telefono" class="nombre text-input" title="Digite el numero de teléfono" /> 
							</p>
							<div class="row  d-flex justify-content-center">
								<div class="col-md-12 col-lg-6">
									<button type="submit" class="btnconsulta btn btn-primary btn-block mt-2" name="consultar" value="Buscar">consultar</button> <!-- BOTON CONSULTAR -->
								</div>
							</div>
						</fieldset>
					</form>
				</div>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
		<!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
	</section>
	<?php
	if (isset($_GET['error'])) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['error'] . '</div>'; // VENTANA MODAL ERROR
	?>
</body>

</html>