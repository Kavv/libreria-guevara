<?php
// LA ACCION PRINCIPAL DE ESTA PAGINA ES MOSTRAR LOS RESULTADOS DE LA CONSULTA GENERADA DESDE CONSULTAR.PHP
// LUEGO DE HABER RECIBIDO LAS VARIABLES POR POST DESDE CONSULTAR.PHP PROCEDEMOS A EVALUAR ESAS VARIABLES PARA MOSTRAR RESULTADOS DE LA CONSULTA O MODIFICAR SI ASI CORRESPONDE

//INCLUIR SESION Y CONECCION
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
if (isset($_POST['modificar'])) { //VALIDAMOS SI POST MODIFICAR ES TRUE
	$id = $_POST['id'];
	$nombre = strtoupper(trim($_POST['nom1']));  // LO RECIBIDO DE ESTE CAMPO LO CAMBIAMOS A MAYUSCULA
	$nom2 = strtoupper(trim($_POST['nom2']));  // LO RECIBIDO DE ESTE CAMPO LO CAMBIAMOS A MAYUSCULA
	$ape1 = strtoupper(trim($_POST['ape1']));  // LO RECIBIDO DE ESTE CAMPO LO CAMBIAMOS A MAYUSCULA
	$ape2 = strtoupper(trim($_POST['ape2']));  // LO RECIBIDO DE ESTE CAMPO LO CAMBIAMOS A MAYUSCULA
	$email = strtolower(trim($_POST['email']));  // LO RECIBIDO DE ESTE CAMPO LO CAMBIAMOS A MAYUSCULA
	$barresidencia = trim(strtoupper($_POST['barresidencia']));  // LO RECIBIDO DE ESTE CAMPO LO CAMBIAMOS A MAYUSCULA
	$dirresidencia = trim(strtoupper($_POST['dirresidencia']));  // LO RECIBIDO DE ESTE CAMPO LO CAMBIAMOS A MAYUSCULA
	$barcomercio = trim(strtoupper($_POST['barcomercio']));  // LO RECIBIDO DE ESTE CAMPO LO CAMBIAMOS A MAYUSCULA
	$dircomercio = trim(strtoupper($_POST['dircomercio']));  // LO RECIBIDO DE ESTE CAMPO LO CAMBIAMOS A MAYUSCULA
	if(!isset($_POST['ciucomercio']))
		$_POST['ciucomercio'] = "";

	$qry = $db->query("UPDATE clientes SET clinombre = '$nombre', clinom2 = '$nom2', 
	cliape1 = '$ape1', cliape2 = '$ape2', cliemail = '$email', clicelular = '" . $_POST['celular'] . "'
	, clidepresidencia = '" . $_POST['depresidencia'] . "', cliciuresidencia = '" . $_POST['ciuresidencia'] . "'
	, clibarresidencia = '$barresidencia', clidirresidencia = '$dirresidencia'
	, clitelresidencia = '" . $_POST['telresidencia'] . "', clidepcomercio = '" . $_POST['depcomercio'] . "'
	, cliciucomercio = '" . $_POST['ciucomercio'] . "', clibarcomercio = '$barcomercio'
	, clidircomercio = '$dircomercio', clitelcomercio = '" . $_POST['telcomercio'] . "' WHERE cliid = '$id'");  // REALIZAMOS CONSULTA CON VARIABLES RECIBIDAS
	if ($qry) $mensaje = 'Se actualizo el cliente'; //MENSAJE EXITOSO
	else $error = 'No se actualizo el cliente'; //MENSAJE ERROR
}
if (isset($_GET['id1'])) { //VALIDAMOS SI GET ID1 ES TRUE 
	$id1 = $_GET['id1'];
	$num = $db->query("SELECT * FROM solicitudes WHERE solcliente = '$id1'")->rowCount();  // REALIZAMOS CONSULTA CON VARIABLES RECIBIDAS Y CONTAMOS LOS RESULTADOS OBTENIDOS
	if ($num < 1) { // SI LOS RESULTADOS DE LA CONSULTA SON MENORES A  1 REALIZAMOS LO SIGUIENTE
		$qry = $db->query("DELETE FROM clientes WHERE cliid = '$id1'"); //ELIMINAMOS LA CAUSAL
		if ($qry) $mensaje = 'Se elimino el cliente';
		else $error = 'No se pudo eliminar el cliente';
	} else $error = 'El cliente tiene solictudes asociadas';
}
$id = $nombre = 0;
if (isset($_POST['consultar'])) { //VALIDAMOS SI POST CONSULTAR ES TRUE
	$id = strupperEsp($_POST['id']);
	$nombre = strtoupper($_POST['nombre']);
	$nom2 = strtoupper($_POST['nom2']);
	$ape1 = strtoupper($_POST['ape1']);
	$ape2 = strtoupper($_POST['ape2']);
	$tel = $_POST['telefono'];
} else if (isset($_GET['id'])) { // SI LO RECIBIDO POR POST CONSULTAR NO ES TRUE
	$id = $_GET['id'];
	$nombre = strtoupper($_GET['nombre']);
	$nom2 = strtoupper($_GET['nom2']);
	$ape1 = strtoupper($_GET['ape1']);
	$ape2 = strtoupper($_GET['ape2']);
	$tel = $_GET['telefono'];
}

if($id == "" &&  $nombre == "" &&   $nom2 == "" &&   $ape1 == "" &&  $ape2 == "" && $tel == "")
{
	$error = "Es importante que especifique almenos un valor para poder hacer una busqueda";
	header("location:consultar.php?error=".$error);
}

$con = "SELECT  cliid,  clidigito,  clinombre,  clinom2,  cliape1, cliape2,  depnombre,  ciunombre,  clibarresidencia,  clidirresidencia,  clitelresidencia,  clitelcomercio,  clicelular,  cliemail FROM (clientes LEFT JOIN departamentos ON clidepresidencia = depid) LEFT JOIN ciudades ON (clidepresidencia = ciudepto AND cliciuresidencia = ciuid)";

/* Los parametros de la consulta sql se genera dinamicamente 
en base a los datos recibidos para delimitar los resultados */
$parameters = [];
if($id != "")
	array_push($parameters, "cliid LIKE '$id%'");
if($nombre != "")
	array_push($parameters, "clinombre LIKE '%$nombre%'" );
if($nom2 != "")
	array_push($parameters, "clinom2 LIKE '%$nom2%'" );
if($ape1 != "")
	array_push($parameters, "cliape1 LIKE '%$ape1%'" );
if($ape2 != "")
	array_push($parameters, "cliape2 LIKE '%$ape2%'" );
if($tel != "")
	array_push($parameters, "clicelular LIKE '%$tel%' OR clitelresidencia LIKE '%$tel%' OR clitelcomercio LIKE '%$tel%' OR clitelfamiliar LIKE '%$tel%' OR cliteladicional LIKE '%$tel%'" );

// Consulta base
$sql = $con;
foreach ($parameters as $index => $parameter) {
	// Se agregan los parametros del WHERE
	if($index == 0)
		$sql .= " WHERE " . $parameter;
	else
		$sql .= " AND " . $parameter;
}


$filtro = 'id=' . $id . '&nombre=' . $nombre. '&nom2=' . $nom2. '&ape1=' . $ape1. '&ape2=' . $ape2. '&telefono=' . $tel;

$qry = $db->query($sql);
$cantidad_clientes = $qry->rowCount();
?>
<!doctype html>
<html lang="es">

<head>
    <title>MOSTRAR CLIENTES</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
	<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
	<script>
		
		$(document).ready(function() {

			var table;
			
			table = $('#tabla').dataTable({ //QRY PARA MANEJO Y VISUALIZACION DE TABLAS
				'bJQueryUI': true,
				'sPaginationType': "full_numbers",
				'oLanguage': {
					'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
				},
				'aoColumnDefs': [{
					'bSortable': false,
					'aTargets': [4, 5, 6, 7]
				}],
			});

			$('.confirmar').click(function(e) { // VENTANA MODAL PARA CONFIRMAR ACCIONES
				e.preventDefault();
				var targetUrl = $(this).attr("href");
				var $dialog_link_follow_confirm = $('<div></div>').html('<p><span class="ui-icon ui-icon-trash" style="float:left; margin:2px 7px 7px 0;"></span>Esta seguro que desea eliminar el cliente?</p>').
				dialog({
					title: 'Porfavor confirmar',
					buttons: {
						'Si': function() {
							carga();
							window.location.href = targetUrl;
						},
						'No': function() {
							$(this).dialog("close");
						}
					},
					modal: true,
					width: 'auto',
					height: 'auto'
				});
				$dialog_link_follow_confirm.dialog('open');
			});
			table.on("draw", function(){
				console.log("hola");
            	$('#loading').dialog('close');
			});
		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
		<?php require($r . 'incluir/src/menu.php') ?>


		<div id="loading" title="Procesando..."><span class="" style="float:left; margin:3px 7px 7px 0;"></span>Este proceso puede demorar <br>Se estan cargando <span style="color:red"><?php echo $cantidad_clientes?></span> resgistros.</div>
		<script>
			$('#loading').dialog({
					autoOpen:false,
					modal:true,
					draggable: false,
					resizable:false
				}).parent('.ui-dialog').find('.ui-dialog-titlebar-close').remove();
			$('#loading').dialog('open');
		</script>


		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Administracion</a>
				<div class="mapa_div"></div><a class="current">Clientes</a>
			</article>
			<article id="contenido">
				<h2>Listado de clientes</h2>
				<div class="reporte">

				</div>
				<!-- INICIO DE TABLA -->
				<table id="tabla">
					<thead>
						<tr>
							<th>Identificacion</th>
							<th>Nombre</th>
							<th>Departamento</th>
							<th>Municipio</th>
							<th>Direccion</th>
							<th>Telefonos</th>
							<th>Email</th>
							<?php if ($rowlog['peradmcli'] == '1') { ?>
								<th></th>
								<th></th>
							<?php } ?>
						</tr>
					</thead>
					<tbody>
						<?php
						while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {  //INFORMACION DE LA CONSULTA
						?>
							<tr>
								<td><?php echo $row['cliid'] . ' ' . $row['clidigito'] ?></td>
								<td><?php echo $row['clinombre'] . ' ' . $row['clinom2'] . ' ' . $row['cliape1'] . ' ' . $row['cliape2'] ?></td>

								<td><?php echo $row['depnombre'] ?></td>
								<td><?php echo $row['ciunombre'] ?></td>
								<td align="center"><img src="<?php echo $r ?>imagenes/iconos/casa.png" title="<?php echo $row['clidirresidencia'] ?>" /></td>
								<td align="center"><?php echo $row['clicelular'] . ' / ' . $row['clitelresidencia'] . ' / ' . $row['clitelcomercio'] ?></td>
								<td align="center"><a href="mailto:<?php echo $row['cliemail'] ?>"><img src="<?php echo $r ?>imagenes/iconos/email.png" title="<?php echo $row['cliemail'] ?>" /></a></td>
								
								<?php if ($rowlog['peradmcli'] == '1') { ?>
								<td align="center"><a href="modificar.php?<?php echo 'id1=' . $row['cliid'] . '&' . $filtro ?>" onClick="carga()" title="editar"><img src="<?php echo $r ?>imagenes/iconos/lapiz.png" class="grayscale" /></a></td><!-- ENVIA POR GET LAS VARIABLES NECESARIAS PARA MODIFICAR UN CLIENTE -->
								<td align="center"><a href="listar.php?<?php echo 'id1=' . $row['cliid'] . '&' . $filtro ?>" title="eliminar" class="confirmar"><img src="<?php echo $r ?>imagenes/iconos/basura.png" class="grayscale" /></a></td> <!-- ENVIA LAS VARIABLES NECESARIAS PARA ELIMINAR UN CLIENTE JUNTO CON VENTANA MODAL PARA CONFIRMAR -->
								<?php } ?>
							</tr>
						<?php
						}
						?>
					</tbody>
				</table> <!-- FIN DE LA TABLA -->
				<p class="boton">
					<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href='consultar.php'">Atras</button> <!-- BOTON PARA VOLVER A CONSULTAR -->
				</p>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
		<!-- BOTON PARA VOLVER A LA PARTE SUPERIOR  -->
	</section>
	
	<?php
	if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>'; // VENTANA MODAL ERROR
	elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>'; // VENTANA MODAL EXITOSO
	?>
</body>

</html>