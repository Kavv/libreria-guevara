<?php
// LA ACCION PRINCIPAL DE ESTA PAGINA ES MODIFICAR UN CLIENTE ENVIANDO LA INFORMACION A LISTAR.PHP

$r = '../../../';
//INCLUIR SESION Y CONECCION
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
$row = $db->query("SELECT clientes.*, residencia.ciunombre as ciuresidencia, comercio.ciunombre as ciucomercio FROM clientes
LEFT JOIN ciudades AS residencia ON (residencia.ciudepto = clidepresidencia AND residencia.ciuid = cliciuresidencia)
LEFT JOIN ciudades AS comercio ON (comercio.ciudepto = clidepcomercio AND comercio.ciuid = cliciucomercio)
WHERE cliid = '" . $_GET['id1'] . "'")->fetch(PDO::FETCH_ASSOC); // CONSULTA A LA BD CON VARUABLES RECIBIDAS POR GET

$filtro = 'id=' . $_GET['id'] . '&nombre=' . $_GET['nombre']. '&nom2=' . $_GET['nom2']. '&ape1=' . $_GET['ape1']. '&ape2=' . $_GET['ape2'] . '&telefono=' . $_GET['telefono'];
?>
<!doctype html>
<html lang="es">

<head>
    <title>MODIFICAR CLIENTE</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">
	<meta charset="utf-8">

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<script>
		var $ciudad = <?php if($row['cliciucomercio'] != "") {echo $row['cliciuresidencia'];} else{ echo "0";}?>;
		$(document).ready(function() {
			var die_depa_res = <?php if ($row['clidepresidencia'] != "") echo $row['clidepresidencia'];
								else echo '-1'; ?>;
			var dir_ciudad_res = <?php if ($row['cliciuresidencia'] != "") echo $row['cliciuresidencia'];
									else echo '-1'; ?>;

			var die_depa_co = <?php if ($row['clidepcomercio'] != "") echo $row['clidepcomercio'];
								else echo '-1'; ?>;
			var dir_ciudad_co = <?php if ($row['cliciucomercio'] != "") echo $row['cliciucomercio'];
								else echo '-1'; ?>;

			$('#depresidencia').change(function(event) {
				var id1 = $('#depresidencia').find(':selected').val();
				$("#ciuresidencia").load('ciudades-re.php?depa=' + id1 + '&die_depa_res=' + die_depa_res + '&dir_ciudad_res=' + dir_ciudad_res);
			});
			$('#depcomercio').change(function(event) {
				var id1 = $('#depcomercio').find(':selected').val();
				$("#ciucomercio").load('ciudades-co.php?depa=' + id1 + '&die_depa_co=' + die_depa_co + '&dir_ciudad_co=' + dir_ciudad_co);
			});
			//$('#depresidencia').change();
			//$('#depcomercio').change();
		});
	</script>
	<!-- ESTILOS EMBEBIDOS PARA MANEJAR FORMULARIO INDEPENDIENTEMENTE -->
	<style type="text/css">
	</style>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
		<?php require($r . 'incluir/src/menu.php') ?>
		<!-- INCLUIMOS MENU PRINCIPAL -->
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Administracion</a>
				<div class="mapa_div"></div><a class="current">Clientes</a>
			</article>
			<article id="contenido">
				<div class="ui-widget">
					<form id="form" name="form" action="listar.php?<?php echo $filtro ?>" method="post">
						<!-- FORMULARIO ENVIADO POR POST A LISTAR.PHP JUNTO CON EL FILTRO GENERAL  -->
						<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
							<legend class="ui-widget ui-widget-header ui-corner-all">Modificar cliente</legend>
							<p>
								<label for="id">Identificacion:</label> <!-- NUM IDENTIFICACION -->
								<input type="text" name="id" class="id" value="<?php echo $row['cliid'] ?>" readonly />
								<?php
								if ($row['clitide'] == 2) { // SI EL TIPO DE ID ES 2 (HACE REFERENCIA A UNA EMPRESA)
								?>
									<input type="text" name="div" value="<?php echo $row['clidigito'] ?>" size="1" readonly />
							</p>
							<p>
								<label for="nom1">Razon social:</label>
								<input type="text" name="nom1" class="nombre" value="<?php echo $row['clinombre'] ?>" required />
							</p>
						<?php
								} else { // DE LO CONTRARIO SI clitide ES DISTINTO A 2
						?>
							</p>
							<p>
								<label for="nom1">Nombre 1:</label> <!-- CAMPO PRIMER NOMBRE  -->
								<input type="text" name="nom1" style="text-transform: uppercase" size="10" value="<?php echo $row['clinombre'] ?>" required />
								<label for="nom2">Nombre 2:</label> <!-- CAMPO SEGUNDO NOMBRE -->
								<input type="text" name="nom2" style="text-transform: uppercase" size="10" value="<?php echo $row['clinom2'] ?>" />
							</p>
							<p>
								<label for="ape1">Apellido 1:</label> <!-- CAMPO PRIMER APELLIDO -->
								<input type="text" name="ape1" style="text-transform: uppercase" size="10" value="<?php echo $row['cliape1'] ?>" required />
								<label for="ape2">Apellido 2:</label> <!-- CAMPO SEGUNDO APELLIDO -->
								<input type="text" name="ape2" style="text-transform: uppercase" size="10" value="<?php echo $row['cliape2'] ?>" />
							</p>
						<?php
								}
						?>
						<p>
							<label for="ape1">Email:</label> <!-- CAMPO EMAIL -->
							<input type="text" class="email" name="email" value="<?php echo $row['cliemail'] ?>" />
						</p>
						<p>
							<label>Celular: </label><input type="text" name="celular" class="telefono validate[custom[phone]] text-input" value="<?php echo $row['clicelular'] ?>" title="Digite numero celular" /> <!-- CAMPO NUM DE CELULAR -->
						</p>
						<p>

							<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12">
								<legend class="ui-widget ui-widget-header ui-corner-all">Datos ubicacion residencia</legend> <!-- DATOS DE UBICACION 1 -->
								<p>
									<label>Departamento:</label> <!-- CAMPO DEPARTAMENTO -->
									<select id="depresidencia" name="depresidencia" class=" text-input">
										<?php
										if ($row['clidepresidencia'] != '') { // SI EL DEPARTAMENTO DEL CLIENTE ES DISTINTO A VACIO 
											$row2 = $db->query("SELECT * FROM departamentos WHERE depid = '" . $row['clidepresidencia'] . "'")->fetch(PDO::FETCH_ASSOC); // SE BUSCA ESPECIFICAMENTE EL DEPARTAMENTO 
											echo '<option value=' . $row['clidepresidencia'] . '>' . $row2['depnombre'] . '</option>'; // SE MUESTAR LOS RESULTADOS TANTO NOMBRE DEL DEPARTAMENTO COMO SU ID EN UN SELECT 
										} // SI EL DEPARTAMENTO DEL CLIENTE ESTA VACIO
										echo '<option value="">SELECCIONE</option>';
										$qry = $db->query("SELECT * FROM departamentos WHERE depid <> '" . $row['clidepresidencia'] . "' ORDER BY depnombre");
										while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
											echo '<option value=' . $row2['depid'] . '>' . $row2['depnombre'] . '</option>'; // APARECEN TODOS LOS DEPARTAMENTOS PARA QUE EL USUARIO ELIGA EL PERTINENTE
										}
										?>
									</select>
								</p>
								<p>
									<label>Ciudad: </label> <!-- CAMPO CIUDAD -->
									<select name="ciuresidencia" id="ciuresidencia" class=" text-input">
										<?php 
											if ($row['cliciuresidencia'] != '') 
											{
												echo '<option value='.$row['cliciuresidencia'].'>'.$row['ciuresidencia'].'</option>';
											}
										?>
									</select>
								</p>
							    <div style="visibility: hidden;height: 0;">
									<label>Barrio: </label><input type="text" name="barresidencia" class="barrio  text-input" value="<?php echo $row['clibarresidencia'] ?>" /> <!-- CAMPO BARRIO -->
								</div>
								<label>Direccion: </label><input type="text" name="dirresidencia" class="direccion  text-input" value="<?php echo $row['clidirresidencia'] ?>" /> <!-- CAMPO DIRECCION -->
						</p>
						<p>
							<label>Telefono: </label><input type="text" name="telresidencia" class="telefono validate[custom[phone]] text-input" value="<?php echo $row['clitelresidencia'] ?>" /> <!-- CAMPO TELEFONO -->
						</p>
						</fieldset>
						<br>
						<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12">
							<legend class="ui-widget ui-widget-header ui-corner-all">Datos ubicacion Comercio</legend> <!-- DATOS DE UBICACION 2 -->
							<p>
								<label>Departamento:</label> <!-- CAMPO DEPARTAMENTO 2 -->
								<select id="depcomercio" name="depcomercio" class=" text-input">
									<?php
									if ($row['clidepcomercio'] != '') {  // SI EL DEPARTAMENTO ( EN DIRECCION COMERCIO ) DEL CLIENTE ES DISTINTO A VACIO 
										$row2 = $db->query("SELECT * FROM departamentos WHERE depid = '" . $row['clidepcomercio'] . "'")->fetch(PDO::FETCH_ASSOC); // SE BUSCA ESPECIFICAMENTE EL DEPARTAMENTO
										echo '<option value=' . $row['clidepcomercio'] . '>' . $row2['depnombre'] . '</option>'; // SE MUESTRA LOS RESULTADOS TANTO NOMBRE DEL DEPARTAMENTO COMO SU ID EN UN SELECT 
									} // SI EL DEPARTAMENTO ( EN DIRECCION DE COMERCIO ) DEL CLIENTE ESTA VACIO
									echo '<option value="">SELECCIONE</option>';
									$qry = $db->query("SELECT * FROM departamentos WHERE depid <> '" . $row['clidepcomercio'] . "' ORDER BY depnombre");
									while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
										echo '<option value=' . $row2['depid'] . '>' . $row2['depnombre'] . '</option>'; // APARECEN TODOS LOS DEPARTAMENTOS PARA QUE EL USUARIO ELIGA EL PERTINENTE
									}
									?>
								</select>
							</p>
							<p>
								<label>Ciudad:</label> <!-- CAMPO CIUDAD 2 -->
								<select name="ciucomercio" id="ciucomercio" class=" text-input">
									<?php 
										if ($row['cliciucomercio'] != '') 
										{
											echo '<option value='.$row['cliciucomercio'].'>'.$row['ciucomercio'].'</option>';
										}
									?>
								</select>
							</p>
							<div style="visibility: hidden;height: 0;">
								<label>Barrio: </label><input type="text" name="barcomercio" class="barrio text-input" value="<?php echo $row['clibarcomercio'] ?>" size="20" /> <!-- CAMPO BARRIO -->
							</div>
							<p>
								<label>Direccion: </label><input type="text" name="dircomercio" class="direccion  text-input" value="<?php echo $row['clidircomercio'] ?>" /> <!-- CAMPO DIRECCION -->
							</p>
							<p>
								<label>Telefono: </label><input type="text" name="telcomercio" class="telefono validate[custom[phone]] text-input" value="<?php echo $row['clitelcomercio'] ?>" /> <!-- CAMPO TELEFONO -->
							</p>
						</fieldset>

						</p>
						<p class="boton">
							<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href='listar.php?<?php echo $filtro ?>'">Atras</button> <!-- BOTON ATRAS -->
							<button type="submit" class="btn btn-primary btnmodificar" name="modificar" value="modificar">Modificar</button> <!-- BOTON MODIFICAR -->
						</p>
						</fieldset>
					</form>
				</div>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
		<!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
	</section>
</body>

</html>