<?php
	// LA ACCION PRINCIPAL DE ESTA PAGINA ES MOSTRAR LOS RESULTADOS DE LA CONSULTA GENERADA DESDE CONSULTAR.PHP
	// LUEGO DE HABER RECIBIDO LAS VARIABLES POR POST DESDE CONSULTAR.PHP PROCEDEMOS A EVALUAR ESAS VARIABLES PARA MOSTRAR RESULTADOS DE LA CONSULTA O MODIFICAR SI ASI CORRESPONDE

	//INCLUIR SESION Y CONECCION
	$r = '../../../';
	require($r . 'incluir/session.php');
	require($r . 'incluir/connection.php');


	if (isset($_GET['id'])) {
		$id = $_GET['id'];
		$nombre = $_GET['nombre'];
	}
	// Proceso de edicion
	if (isset($_POST['modificar'])) { 
		// El formulario por defecto cuenta con los valores del filtro
		$filtro = 'id=' . $nit . '&nombre=' . $nombre;

		$id = $_POST['id'];
		$nombre = (trim($_POST['nombre']));  
		$nit = strtoupper(trim($_POST['nit']));

		// Verificamos si ya existe un registro con este nombre
		$num = $db->query("SELECT * FROM bancos WHERE bancoid <> $id AND banconombre = '$nombre'")->rowCount(); 
		if ($num < 1) {  
			$email = strtolower(trim($_POST['email']));
			$telefono = trim($_POST['telefono']);
			$direccion = (trim($_POST['direccion']));
			$qry = $db->query("UPDATE bancos SET banconombre = '$nombre', banconit = '$nit', bancoemail = '$email', bancotelefono = '$telefono', bancodireccion = '$direccion' WHERE bancoid = $id"); // ACTUALIZAMOS LA INFORMACION CON LOS NUEVOS DATOS RECIBIDOS

			//Log de acciones realizadas por usuario en la BD	
			$usuario = $_SESSION['id'];
			$msj = "MODIFICO LOS DATOS DEL BANCO CON EL ID $id";
			$ip =  $_SERVER['REMOTE_ADDR'];

			$qrylogsregister = $db->query("INSERT INTO logs (id_usuario , ip_usuario , accion_usuario, fecha_accion) VALUES ( 
			'$usuario', '$ip' , '$msj'  , NOW() );");

			if ($qry) 
			{
				$mensaje = 'Se actualizo el banco'; // MENSAJE EXITOSO
				header("location:listar.php?".$filtro."&mensaje=".$mensaje);
			}
			else 
			{
				$error = 'No se actualizo el banco';  // MENSAJE ERROR
				header("location:listar.php?".$filtro."&error=".$error);
			}
		} 
		else 
		{
			$error = 'No se actualizo. El nombre del banco ya existe';
			header("location:listar.php?".$filtro."&error=".$error);
		}
		exit();
	}
	// Proceso de eliminacion
	if (isset($_GET['id1'])) {
		$id1 = $_GET['id1'];

		$nit = $_GET['id'];
		$nombre = $_GET['nombre'];

		$filtro = 'id=' . $nit . '&nombre=' . $nombre;
		
		// Consultamos si existen registros de tesoreria relacionados al banco a eliminar
		$num = $db->query("SELECT * FROM tesoreria WHERE tesobanco = $id1")->rowCount(); 
		if ($num < 1) {
			$qry = $db->query("DELETE FROM bancos WHERE bancoid = $id1"); //ELIMINAMOS EL BANCO
			if ($qry) {
				$mensaje = 'Se elimino el banco';
				header("location:listar.php?".$filtro."&mensaje=".$mensaje);
			}
			else {
				$error = 'No se pudo eliminar el banco';
				header("location:listar.php?".$filtro."&error=".$error);
			}
		} else {
			$error = 'El banco posee Tesoreria no puede eliminarse';
			header("location:listar.php?".$filtro."&error=".$error);
		}
	}
	if (isset($_POST['consultar'])) { //VALIDAMOS SI POST CONSULTAR ES TRUE
		$id = $_POST['id'];
		$nombre = $_POST['nombre'];
	}
	$filtro = 'id=' . $id . '&nombre=' . $nombre;

	$con = 'SELECT * FROM bancos';

	/* Los parametros de la consulta sql se genera dinamicamente 
	en base a los datos recibidos para delimitar los resultados */
	$parameters = [];
	if ($id != "")
		array_push($parameters, "banconit LIKE '%$id%'");
	if ($nombre != "")
		array_push($parameters, "banconombre LIKE '%$nombre%'");

	// Consulta base
	$sql = $con;
	foreach ($parameters as $index => $parameter) {
		// Se agregan los parametros del WHERE
		if ($index == 0)
			$sql .= " WHERE " . $parameter;
		else
			$sql .= " AND " . $parameter;
	}

	$qry = $db->query($sql);
?>
<!doctype html>

<html lang="es">

<head>
    <title>LISTAR BANCOS</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
	<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
	<script>
		$(document).ready(function() {
			$('#tabla').dataTable({ //QRY PARA MANEJO Y VISUALIZACION DE TABLAS
				'bJQueryUI': true,
				'sPaginationType': "full_numbers",
				'oLanguage': {
					'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
				},
				'aoColumnDefs': [{
					'bSortable': false,
					'aTargets': [2, 3, 4, 5, 6, 7]
				}],
			});

			$('.confirmar').click(function(e) { // VENTANA MODAL PARA CONFIRMAR ACCIONES
				e.preventDefault();
				var targetUrl = $(this).attr("href");
				var $dialog_link_follow_confirm = $('<div></div>').html('<p><span class="ui-icon ui-icon-trash" style="float:left; margin:2px 7px 7px 0;"></span>Esta seguro que desea eliminar el banco?</p>').
				dialog({
					title: 'Porfavor confirmar',
					buttons: {
						'Si': function() {
							carga();
							window.location.href = targetUrl;
						},
						'No': function() {
							$(this).dialog("close");
						}
					},
					modal: true,
					width: 'auto',
					height: 'auto'
				});
				$dialog_link_follow_confirm.dialog('open');
			});

		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
		<?php require($r . 'incluir/src/menu.php') ?>
		<!-- INCLUIMOS MENU PRINCIPAL -->
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Costos y Gastos</a>
				<div class="mapa_div"></div><a class="current">Bancos</a>
			</article>
			<article id="contenido">
				<h2>Listado de bancos</h2>
				<table id="tabla">
					<thead>
						<tr>
							<th>ID</th>
							<th>Nombre</th>
							<th>RUC/Identificacion</th>
							<th>Telefono</th>
							<th>Email</th>
							<th>Dirección</th>
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php
						while ($row = $qry->fetch(PDO::FETCH_ASSOC)) { //INFORMACION DE LA CONSULTA
						?>
							<tr>
								<td><?php echo $row['bancoid'] ?></td>
								<td><?php echo $row['banconombre'] ?></td>
								<td><?php echo $row['banconit'] ?></td>
								<td><?php echo $row['bancotelefono'] ?></td>

								<td align="center"><a href="mailto:<?php echo $row['bancoemail'] ?>"><img src="<?php echo $r ?>imagenes/iconos/email.png" title="<?php echo $row['bancoemail'] ?>" /></td>
								<td><?php echo $row['bancodireccion'] ?> </td>
								<td align="center"><a href="modificar.php?<?php echo 'id1=' . $row['bancoid'] . '&' . $filtro ?>" onClick="carga()" title="editar"><img src="<?php echo $r ?>imagenes/iconos/lapiz.png" class="grayscale" /></a></td> 
								<td align="center"><a href="listar.php?<?php echo 'id1=' . $row['bancoid'] . '&' . $filtro ?>" title="eliminar" class="confirmar"><img src="<?php echo $r ?>imagenes/iconos/basura.png" class="grayscale" /></a></td> 
							</tr>
						<?php
						}
						?>
					</tbody>
				</table>
				<p class="boton">
					<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href='consultar.php'">Atras</button> <!-- BOTON PARA VOLVER A CONSULTAR -->
				</p>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
		<!-- BOTON PARA VOLVER A LA PARTE SUPERIOR  -->
	</section>
	<?php
	if (isset($_GET['error'])) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['error'] . '</div>'; // VENTANA MODAL ERROR
	elseif (isset($_GET['mensaje'])) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['mensaje'] . '</div>';  // VENTANA MODAL EXITOSO
	?>
</body>

</html>