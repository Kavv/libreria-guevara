<?php
// LA ACCION PRINCIPAL DE ESTA PAGINA ES INSERTAR UN NUEVO PROEEVEDOR

$r = '../../../';
//INCLUIR SESION Y CONECCION
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
if (isset($_POST['insertar'])) { // VALIDAMOS QUE POST INSERTAR SEA TRUE
	$nombre = strtoupper(trim($_POST['nombre']));
	$nit = strtoupper(trim($_POST['nit']));
	$email = strtolower(trim($_POST['email']));
	$telefono = trim($_POST['telefono']);
	$direccion = strtoupper(trim($_POST['direccion']));
	// Verificamos si el RUC o Cedula ya estan registrados con otro proveedor
	$num = $db->query("SELECT * FROM bancos WHERE banconombre = '$nombre'")->rowCount();  
	if ($num < 1) {  
		// REALIZAMOS EL INSERT EN LA BD SEGUN LOS DATOS RECIBIDOS
		$sql = "INSERT INTO bancos (banconombre, bancodireccion, bancotelefono, banconit, bancoemail) VALUES ('$nombre', '$direccion', '$telefono', '$nit', '$email')";
		$qry = $db->query($sql);
		if ($qry) 
		{
			$usuario = $_SESSION['id'];
			$msj = "INSERTO EL BANCO CON NOMBRE $nombre, NIT $nit, EMAIL $email, TELEFONO $telefono, DIRECION $direccion ";
			$ip =  $_SERVER['REMOTE_ADDR'];

			$qrylogsregister = $db->query("INSERT INTO logs (id_usuario , ip_usuario , accion_usuario, fecha_accion) VALUES ( 
			'$usuario', '$ip' , '$msj'  , NOW() );");

			$mensaje = 'Se inserto el banco'; 
			header("location:insertar.php?mensaje=".$mensaje);
		}
		else
		{
			$error = 'No se pudo insertar el banco'; 
			header("location:insertar.php?error=".$error);
		}

	}
	else
	{
		$error = 'No se inserto<br/>El nombre del banco ya existe';
		header("location:insertar.php?error=".$error);
	}
	exit();
}
?>
<!doctype html>
<html lang="es">

<head>
    <title>AGREGAR BANCOS</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<script type="text/javascript" src="<?php echo $r ?>incluir/funciones.js"></script>

</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
		<?php require($r . 'incluir/src/menu.php') ?>
		<!-- INCLUIMOS MENU PRINCIPAL -->
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Costos y Gastos</a>
				<div class="mapa_div"></div><a class="current">Bancos</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="insertar.php" method="post">
					<!-- FORMULARIO ENVIADO POR POST A SI MISMO -->
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">Insertar Banco </legend>
						<p>
							<label for="nombre">Nombre:</label> <!-- CAMPO NOMBRE  -->
							<input type="text" name="nombre" class="nombre validate[required] text-input" title="Digite el nombre del banco" />
						</p>
						<p>
							<label for="nit">RUC/Identificación:</label> <!-- CAMPO nit -->
							<input type="text" name="nit" class="nit validate[required] text-input uppercase" />
						</p>
						<p>
							<label for="email">E-mail:</label> <!-- CAMPO EMAIL -->
							<input type="text" name="email" class="email validate[custom[email]] text-input" />
						</p>
						<p>
							<label for="telefono">Telefono:</label> <!-- CAMPO TELEFONO -->
							<input type="text" name="telefono" class="telefono text-input" />
						</p>
						<p>
							<label for="direccion">Direccion:</label> <!-- CAMPO DIRECCION -->
							<input type="text" name="direccion" class="direccion text-input uppercase" />
						</p>
						<p class="boton">
							<button type="submit" class="btn btn-primary btninsertar" name="insertar" value="insertar">Insertar</button> <!-- BOTON INSERTAR -->
						</p>
					</fieldset>
				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
		<!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
	</section>
	<?php
	if (isset($_GET['error'])) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['error'] . '</div>'; //VENTANA MODAL ERROR
	elseif (isset($_GET['mensaje'])) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['mensaje']. '</div>'; //VENTANA MODAL EXITOSO
	?>
</body>

</html>