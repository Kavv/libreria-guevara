<?php
// LA ACCION PRINCIPAL DE ESTA PAGINA ES MODIFICA UNA CIUDAD ENVIANDOLO A LISTAR

$r = '../../../';
//INCLUIR SESION Y CONECCION
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
$row = $db->query("SELECT * FROM bancos WHERE bancoid = " . $_GET['id1'])->fetch(PDO::FETCH_ASSOC); // CONSULTA A LA BD CON VARUABLES RECIBIDAS POR GET
$filtro = 'id=' . $_GET['id'] . '&nombre=' . $_GET['nombre']; //FILTRO GENERAL
?>
<!doctype html>
<html lang="es">

<head>
    <title>MODIFICAR BANCO</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
		<?php require($r . 'incluir/src/menu.php') ?>
		<!-- INCLUIMOS MENU PRINCIPAL -->
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Costos y Gastos</a>
				<div class="mapa_div"></div><a class="current">Bancos</a>
			</article>
			<article id="contenido">
				<div class="ui-widget">
					<form id="form" name="form" action="listar.php?<?php echo $filtro ?>" method="post">
						<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
							<legend class="ui-widget ui-widget-header ui-corner-all">Modificar Banco</legend>
							<p>
								<label for="id">Identificacion:</label> <!-- CAMPO IDENTIFICACION -->
								<input type="text" name="id" class="id" value="<?php echo $row['bancoid'] ?>" readonly />
							</p>
							<p>
								<label for="nombre">Nombre:</label> <!-- CAMPO NOMBRE -->
								<input type="text" name="nombre" class="nombre validate[required] text-input" value="<?php echo $row['banconombre'] ?>" title="Digite el nombre del Banco" />
							</p>
							<p>
								<label for="nit">RUC/Identificación:</label> 
								<input type="text" name="nit" class="validate[required] text-input uppercase" value="<?php echo $row['banconit'] ?>" />
							</p>
							<p>
								<label for="email">E-mail:</label> <!-- CAMPO EMAIL -->
								<input type="text" name="email" class="email validate[custom[email]] text-input" value="<?php echo $row['bancoemail'] ?>" />
							</p>
							<p>
								<label for="telefono">Telefono:</label> <!-- CAMPO TELEFONO -->
								<input type="text" name="telefono" class="telefono text-input" value="<?php echo $row['bancotelefono'] ?>" />
							</p>
							<p>
								<label for="direccion">Direccion:</label> <!-- CAMPO DIRECCION -->
								<input type="text" name="direccion" class="direccion uppercase text-input" value="<?php echo $row['bancodireccion'] ?>" />
							</p>
							<p class="boton">
								<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href='listar.php?<?php echo $filtro ?>'">Atras</button> <!--  BOTON PARA VOLVER ATRAS JUNTO CON FILTRO GENERAL-->
								<button type="submit" class="btn btn-primary btnmodificar" name="modificar" value="modificar">Modificar</button> <!-- BOTON MODIFICAR -->
							</p>
						</fieldset>
					</form>
				</div>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
		<!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
	</section>
</body>

</html>