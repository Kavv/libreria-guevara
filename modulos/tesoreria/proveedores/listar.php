<?php
	// LA ACCION PRINCIPAL DE ESTA PAGINA ES MOSTRAR LOS RESULTADOS DE LA CONSULTA GENERADA DESDE CONSULTAR.PHP
	// LUEGO DE HABER RECIBIDO LAS VARIABLES POR POST DESDE CONSULTAR.PHP PROCEDEMOS A EVALUAR ESAS VARIABLES PARA MOSTRAR RESULTADOS DE LA CONSULTA O MODIFICAR SI ASI CORRESPONDE

	//INCLUIR SESION Y CONECCION
	$r = '../../../';
	require($r . 'incluir/session.php');
	require($r . 'incluir/connection.php');

	// Obtenemos los datos mediante get
	if (isset($_GET['id'])) {
		$id = $_GET['id'];
		$nombre = $_GET['nombre'];
	}
	// Proceso de edicion
	if (isset($_POST['modificar'])) {
		// El formulario por defecto cuenta con los valores del filtro
		$filtro = 'id=' . $id . '&nombre=' . $nombre;

		$id = $_POST['id'];
		$nombre = (trim($_POST['nombre']));  // LO RECIBIDO DE ESTE CAMPO LO CAMBIAMOS A MAYUSCULA
		$nit = strtoupper(trim($_POST['nit']));

		// Verificamos si el RUC o Cedula ya estan registrados con otro proveedor
		$num = $db->query("SELECT * FROM proveedores_teso WHERE protesoid <> $id AND protesonit = '$nit'")->rowCount();  
		if ($num < 1) {  
			$email = strtolower(trim($_POST['email']));
			$telefono = trim($_POST['telefono']);
			$direccion = (trim($_POST['direccion']));
			$qry = $db->query("UPDATE proveedores_teso SET protesonombre = '$nombre', protesonit = '$nit', protesoemail = '$email', protesotelefono = '$telefono', protesodireccion = '$direccion' WHERE protesoid = $id"); // ACTUALIZAMOS LA INFORMACION CON LOS NUEVOS DATOS RECIBIDOS
	

			if ($qry){
				$mensaje = 'Se actualizo el proveedor'; // MENSAJE EXITOSO
				//Log de acciones realizadas por usuario en la BD
				$usuario = $_SESSION['id'];
				$msj = "MODIFICO LOS DATOS DEL PROVEEDOR CON EL ID $id";
				$ip =  $_SERVER['REMOTE_ADDR'];
	
				$qrylogsregister = $db->query("INSERT INTO logs (id_usuario , ip_usuario , accion_usuario, fecha_accion) VALUES ( 
				'$usuario', '$ip' , '$msj'  , NOW() );");

				header("location:listar.php?".$filtro."&mensaje=".$mensaje);
			} 
			else{
				$error = 'No se actualizo el proveedor';  // MENSAJE ERROR
				header("location:listar.php?".$filtro."&error=".$error);
			} 
		} else {
			$error = 'No se actualizo debido a que <br/>El RUC o Cedula esta asignado a otro proveedor';
			header("location:listar.php?".$filtro."&error=".$error);
		}
		exit();
	}

	// Proceso de eliminar
	if (isset($_GET['id1'])) { 
		$id1 = $_GET['id1'];
		$nit = $_GET['id'];
		$nombre = $_GET['nombre'];
		
		$filtro = 'id=' . $nit . '&nombre=' . $nombre;
		// Verificamos si el proveedor tiene relacion con algun registro de tesoreria
		$datos = $db->query("SELECT * FROM tesoreria WHERE tesoproveedor = $id1");
		$num = $datos->rowCount();
		if ($num < 1) {
			$datos = $datos->fetch(PDO::FETCH_OBJ);
			$qry = $db->query("DELETE FROM proveedores_teso WHERE protesoid = $id1"); //ELIMINAMOS EL PROEVEDOR
			if ($qry){

				$usuario = $_SESSION['id'];
				$msj = "ELIMINO DEL PROVEEDOR: $datos->protesonombre  NIT: $datos->protesonit CON EL ID $id";
				$ip =  $_SERVER['REMOTE_ADDR'];
	
				$qrylogsregister = $db->query("INSERT INTO logs (id_usuario , ip_usuario , accion_usuario, fecha_accion) VALUES ( 
				'$usuario', '$ip' , '$msj'  , NOW() );");

				$mensaje = 'Se elimino el proveedor';
				header("location:listar.php?".$filtro."&mensaje=".$mensaje);
			} 
			else{
				$error = 'No se pudo eliminar el proveedor';
				header("location:listar.php?".$filtro."&error=".$error);
			} 
		} else{
			$error = 'No se elimino! El proveedor posee registros de Tesoreria';
			header("location:listar.php?".$filtro."&error=".$error);
		}
		exit(); 
	}
	
	// obtenemos los datos mediante post
	if (isset($_POST['consultar'])) {
		$id = $_POST['id'];
		$nombre = $_POST['nombre'];
	}
	$filtro = 'id=' . $id . '&nombre=' . $nombre;

	$con = 'SELECT * FROM proveedores_teso';

	/* Los parametros de la consulta sql se genera dinamicamente 
	en base a los datos recibidos para delimitar los resultados */
	$parameters = [];
	if ($id != "")
		array_push($parameters, "protesonit LIKE '%$id%'");
	if ($nombre != "")
		array_push($parameters, "protesonombre LIKE '%$nombre%'");

	// Consulta base
	$sql = $con;
	foreach ($parameters as $index => $parameter) {
		// Se agregan los parametros del WHERE
		if ($index == 0)
			$sql .= " WHERE " . $parameter;
		else
			$sql .= " AND " . $parameter;
	}
	

	$qry = $db->query($sql);
?>
<!doctype html>
<html lang="es">

<head>
    <title>LISTAR PROVEEDOR DE TESORERIA</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
	<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
	<script>
		$(document).ready(function() {
			$('#tabla').dataTable({ //QRY PARA MANEJO Y VISUALIZACION DE TABLAS
				'bJQueryUI': true,
				"aaSorting": [[ 1, "asc" ]],
				'sPaginationType': "full_numbers",
				'oLanguage': {
					'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
				},
				'aoColumnDefs': [{
					'bSortable': false,
					'aTargets': [2, 3, 4, 5, 6, 7]
				}],
			});

			$('.confirmar').click(function(e) { // VENTANA MODAL PARA CONFIRMAR ACCIONES
				e.preventDefault();
				var targetUrl = $(this).attr("href");
				var $dialog_link_follow_confirm = $('<div></div>').html('<p><span class="ui-icon ui-icon-trash" style="float:left; margin:2px 7px 7px 0;"></span>Esta seguro que desea eliminar el proveedor?</p>').
				dialog({
					title: 'Porfavor confirmar',
					buttons: {
						'Si': function() {
							carga();
							window.location.href = targetUrl;
						},
						'No': function() {
							$(this).dialog("close");
						}
					},
					modal: true,
					width: 'auto',
					height: 'auto'
				});
				$dialog_link_follow_confirm.dialog('open');
			});

		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
		<?php require($r . 'incluir/src/menu.php') ?>
		<!-- INCLUIMOS MENU PRINCIPAL -->
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Costos y Gastos</a>
				<div class="mapa_div"></div><a class="current">Proveedores</a>
			</article>
			<article id="contenido">
				<h2>Listado de proveedores</h2>
				<table id="tabla">
					<thead>
						<tr>
							<th>ID</th>
							<th>Nombre</th>
							<th>RUC/Cedula</th>
							<th>Telefono</th>
							<th>Email</th>
							<th>Dirección</th>
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php
						while ($row = $qry->fetch(PDO::FETCH_ASSOC)) { //INFORMACION DE LA CONSULTA
						?>
							<tr>
								<td><?php echo $row['protesoid'] ?></td>
								<td><?php echo $row['protesonombre'] ?></td>
								<td> <?php echo $row['protesonit'] ?></td>
								<td align="center"><?php echo $row['protesotelefono'] ?></td>
								<td align="center"><a href="mailto:<?php echo $row['protesoemail'] ?>"><img src="<?php echo $r ?>imagenes/iconos/email.png" title="<?php echo $row['protesoemail'] ?>" /></td>
								<td align="center"><img src="<?php echo $r ?>imagenes/iconos/casa.png" title="<?php echo $row['protesodireccion'] ?>" /></td>
								<td align="center"><a href="modificar.php?<?php echo 'id1=' . $row['protesoid'] . '&' . $filtro ?>" onClick="carga()" title="editar"><img src="<?php echo $r ?>imagenes/iconos/lapiz.png" class="grayscale" /></a></td> <!-- ENVIA POR GET LAS VARIABLES NECESARIAS PARA MODIFICAR UN PROEVEDOR -->
								<td align="center"><a href="listar.php?<?php echo 'id1=' . $row['protesoid'] . '&' . $filtro ?>" title="eliminar" class="confirmar"><img src="<?php echo $r ?>imagenes/iconos/basura.png" class="grayscale" /></a></td> <!-- ENVIA LAS VARIABLES NECESARIAS PARA ELIMINAR UN PROEVEDOR JUNTO CON VENTANA MODAL PARA CONFIRMAR -->
							</tr>
						<?php
						}
						?>
					</tbody>
				</table>
				<p class="boton">
					<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href='consultar.php'">Atras</button> <!-- BOTON PARA VOLVER A CONSULTAR -->
				</p>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
		<!-- BOTON PARA VOLVER A LA PARTE SUPERIOR  -->
	</section>
	<?php
	if (isset($_GET['error'])) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['error'] . '</div>'; // VENTANA MODAL ERROR
	elseif (isset($_GET['mensaje'])) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['mensaje'] . '</div>';  // VENTANA MODAL EXITOSO
	?>
</body>

</html>