<?php
// LA ACCION PRINCIPAL ES ESTA PAGINA ES CONSULTAR LOS PROVEEDORES
$r = '../../../';
//INCLUIR SESION Y CONECCION
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
?>
<!doctype html>
<html lang="es">

<head>
    <title>CONSULTAR PROVEEDOR DE TESORERIA</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>

</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
		<?php require($r . 'incluir/src/menu.php') ?>
		<!-- INCLUIMOS MENU PRINCIPAL -->
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Costos y Gastos</a>
				<div class="mapa_div"></div><a class="current">Proveedores</a>
			</article>
			<article id="contenido">
				<div class="ui-widget">
					<form id="form" name="form" action="listar.php" method="post">
						<!-- ENVIO FORMULARIO POR POST A LISTAR.PHP -->
						<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
							<legend class="ui-widget ui-widget-header ui-corner-all">Consultar Proveedor Tesoreria</legend>
							<p>
								<label for="id">RUC o Cedula:</label> <!-- CAMPO IDENTIFICACION -->
								<input type="text" name="id" class="id validate[text-input" title="Digite la identificacion" />
							</p>
							<p>
								<label for="nombre">Nombre:</label> <!-- CAMPO NOMBRE -->
								<input type="text" name="nombre" class="nombre validate[custom[onlyLetterSp]] text-input" title="Digite el nombre del proveedor" />
							</p>
							<p class="boton">
								<button type="submit" class="btn btn-primary btnconsulta" name="consultar" value="consultar">Consultar</button> <!-- BOTON CONSULTAR -->
							</p>
						</fieldset>
					</form>
				</div>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
		<!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
	</section>
</body>

</html>