<?php
$r = '../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
include($r . 'incluir/fpdf/fpdf.php');

$iden = $_GET['iden'];
$sql = "SELECT * FROM nomina WHERE nominaid = " . $iden . ";";
$rowprin = $db->query($sql)->fetch(PDO::FETCH_ASSOC);

$qryusuario = $db->query("SELECT * FROM nomina LEFT JOIN usuarios ON usuid = nominausuario INNER JOIN empresas ON empid = nominaempresa where nominaid = " . $iden . "; ");
$rowusuario = $qryusuario->fetch(PDO::FETCH_ASSOC);
$resultados = $qryusuario->rowCount();
if (!empty($rowusuario['usunombre'])) {
	$nombre = $rowusuario['usunombre'];
	$empresa = $rowusuario['empnombre'];
	$cedula = $rowusuario['usuid'];
} else {
	$nombre = $rowusuario['nominanombreusuario'];
	$empresa = $rowusuario['empnombre'];
	$cedula = $rowusuario['nominausuario'];
}

$qryempresa = $db->query("SELECT * FROM empresas where empid = '" . $rowprin['nominaempresa'] . "'; ");
$rowempresa = $qryempresa->fetch(PDO::FETCH_ASSOC);
$empresa = $rowempresa['empnombre'];

$hoy = date('Y/m/d');

if (empty($rowprin['nominaconsecutivo'])) {
	$sqlconsecutivo = "SELECT * FROM nomina WHERE nominaempresa = '" . $rowprin['nominaempresa'] . "' AND nominatipo = 'DOC EQUIVALENTE' AND nominaconsecutivo <> '' order by nominaconsecutivo desc LIMIT 1;";
	$rowconsecutivo = $db->query($sqlconsecutivo)->fetch(PDO::FETCH_ASSOC);

	if (!empty($rowconsecutivo['nominaconsecutivo'])) {
		$consecutivo = $rowconsecutivo['nominaconsecutivo'] + 1;
	} else {
		$consecutivo = 1;
	}

	$qry = $db->query("UPDATE nomina SET nominaconsecutivo='$consecutivo' WHERE nominaid='$iden'; ");
} else {
	$consecutivo = $rowprin['nominaconsecutivo'];
}

$pdf = new FPDF();
$pdf->AddFont('LucidaConsole', '', 'lucon.php');
$pdf->AliasNbPages();
$pdf->AddPage('L');
$pdf->SetFont('Arial', '', 10);
if ($rowprin['nominaempresa'] == 900051528 or $rowprin['nominaempresa'] == 900813686 or $rowprin['nominaempresa'] == 900306685) {
	$pdf->Image($r . 'imagenes/logos/' . $rowprin['nominaempresa'] . '-doc.png', 5, 5, 80, 25);
}
$pdf->Cell(275, 4, 'No.        ' . $consecutivo . '', 0, 1, 'R');
$pdf->Cell(275, 4, 'DOCUMENTO EQUIVALENTE A FACTURA', 0, 1, 'R');
$pdf->Cell(275, 4, '', 0, 1, 'R');

$pdf->Ln(10);
$pdf->Cell(200, 4, 'Persona Natural de quien se adquiere los bienes y/o servicios: ', 0, 0, 'L');
$pdf->Cell(75, 4, '' . $nombre . '', 0, 1, 'L');
$pdf->Cell(200, 4, 'RUC o CEDULA:  ', 0, 0, 'L');
$pdf->Cell(75, 4, '' . $cedula . '', 0, 1, 'L');
$pdf->Cell(200, 4, 'CUIDAD Y FECHA DE OPERACION:  ', 0, 0, 'L');
$pdf->Cell(75, 4, 'MANAGUA ' . $hoy . '', 0, 1, 'L');



//$salud = $rowprin['nominasueldo'] * 0.04;
//$pension = $rowprin['nominasueldo'] * 0.04;
$totalpagos = $rowprin['nominasueldo'] + $rowprin['nominamovilizacion'];
//$totaldescuentos = $salud + $pension;

$netopagar = $totalpagos; //- $totaldescuentos;

$pdf->SetFont('Arial', '', 8.5);
$pdf->Ln(5);
$pdf->Cell(120, 4, 'Descripcion de la ', 'L R T', 0, 'C');
$pdf->Cell(31, 4, 'Valor de la ', 'L R T', 0, 'C');
$pdf->Cell(31, 4, 'Tarifa de iva a la que', 'L R T', 0, 'C');
$pdf->Cell(31, 4, 'IVA teorico generado', 'L R T', 0, 'C');
$pdf->Cell(31, 4, 'Tarifa de retencion', 'L R T', 0, 'C');
$pdf->Cell(31, 4, 'Valor del impuesto', 'L R T', 1, 'C');

$pdf->Cell(120, 4, 'operacion', 'L R B', 0, 'C');
$pdf->Cell(31, 4, 'misma', 'L R B', 0, 'C');
$pdf->Cell(31, 4, 'se halla grabado', 'L R B', 0, 'C');
$pdf->Cell(31, 4, 'en la operacion', 'L R B', 0, 'C');
$pdf->Cell(31, 4, 'de iva vigente', 'L R B', 0, 'C');
$pdf->Cell(31, 4, 'asumido', 'L R B', 1, 'C');

$pdf->SetFont('Arial', '', 8.5);

$qrypri = $db->query("SELECT * FROM infodocequi where infonomina = " . $iden . "; ");
$cont = 1;
$infovalor = 0;
$infoivateorico = 0;
$infovalorimpuesto = 0;
while ($rowpri = $qrypri->fetch(PDO::FETCH_ASSOC)) {

	$pdf->Cell(120, 4, '' . $rowpri['infodescripcion'] . '', 1, 0, 'L');
	$pdf->Cell(31, 4, '' . number_format($rowpri['infovalor'], 2) . '', 1, 0, 'C');
	$pdf->Cell(31, 4, '' . $rowpri['infotarifaiva'] . ' %', 1, 0, 'C');
	$pdf->Cell(31, 4, '' . number_format($rowpri['infoivateorico'], 2) . '', 1, 0, 'C');
	$pdf->Cell(31, 4, '' . $rowpri['infotarifaretencion'] . ' %', 1, 0, 'C');
	$pdf->Cell(31, 4, '' . number_format($rowpri['infovalorimpuesto'], 2) . '', 1, 1, 'C');

	$infovalor = $rowpri['infovalor'] + $infovalor;
	$infoivateorico = $rowpri['infoivateorico'] + $infoivateorico;
	$infovalorimpuesto = $rowpri['infovalorimpuesto'] + $infovalorimpuesto;

	$cont = $cont + 1;
}

for ($i = $cont; $i <= 12; $i++) {
	$pdf->Cell(120, 4, '', 1, 0, 'L');
	$pdf->Cell(31, 4, '', 1, 0, 'C');
	$pdf->Cell(31, 4, '', 1, 0, 'C');
	$pdf->Cell(31, 4, '', 1, 0, 'C');
	$pdf->Cell(31, 4, '', 1, 0, 'C');
	$pdf->Cell(31, 4, '', 1, 1, 'C');
}

$pdf->Cell(120, 4, 'Totales C$', 1, 0, 'R');
$pdf->Cell(31, 4, '' . number_format($infovalor, 2) . '', 1, 0, 'C');
$pdf->Cell(31, 4, '', 1, 0, 'C');
$pdf->Cell(31, 4, '' . number_format($infoivateorico, 2) . '', 1, 0, 'C');
$pdf->Cell(31, 4, '', 1, 0, 'C');
$pdf->Cell(31, 4, '' . number_format($infovalorimpuesto, 2) . '', 1, 1, 'C');

$qry = $db->query("UPDATE nomina SET nominainfovalor='$infovalor', nominainfoivateorico='$infoivateorico', nominainfovalorimpuesto='$infovalorimpuesto' WHERE nominaid='$iden'; ");


$pdf->Ln(3);
$pdf->Cell(275, 5, 'CONTABILIZACIONES', 1, 1, 'C');
$pdf->Ln(3);

$qrydocequi = $db->query("SELECT * FROM docequivalente where docequinomina = " . $iden . "; ");
$rowdocequi = $qrydocequi->fetch(PDO::FETCH_ASSOC);

$qrydocequi2 = $db->query("SELECT * FROM docequivalente2 where docequi2nomina = " . $iden . "; ");
$rowdocequi2 = $qrydocequi2->fetch(PDO::FETCH_ASSOC);

$qrydocequi3 = $db->query("SELECT * FROM docequivalente3 where docequi3nomina = " . $iden . "; ");
$rowdocequi3 = $qrydocequi3->fetch(PDO::FETCH_ASSOC);

//1
$pdf->Cell(50, 4, 'PUC', 1, 0, 'C');
$pdf->Cell(50, 4, 'cuenta', 1, 0, 'C');
$pdf->Cell(50, 4, 'DEBITO', 1, 0, 'C');
$pdf->Cell(50, 4, 'CREDITO', 1, 0, 'C');
$pdf->Cell(5, 4, '', 'L R', 0, 'C');
$pdf->Cell(70, 4, 'BASE RETENCION', 1, 1, 'C');
//2 

$valorservicios = number_format($rowdocequi2['docequi2valorservicios'], 2);
if ($valorservicios == 0) {
	$valorservicios = '';
}

$pdf->SetFont('Arial', '', 8.5);
$pdf->Cell(50, 4, '' . $rowdocequi['docequipucservicios'] . '', 1, 0, 'C');
$pdf->Cell(50, 4, 'Servicios', 1, 0, 'C');
$pdf->Cell(50, 4, '' . number_format($rowdocequi['docequidebitoservicios'], 2) . '', 1, 0, 'C');
$pdf->Cell(50, 4, '' . number_format($rowdocequi['docequicreditoservicios'], 2) . '', 1, 0, 'C');
$pdf->Cell(5, 4, '', 'L R', 0, 'C');
$pdf->Cell(35, 4, 'Valor servicios', 1, 0, 'C');
$pdf->Cell(35, 4, '' . $valorservicios . '', 1, 1, 'C');
//3

$menospagoaportes = number_format($rowdocequi2['docequi2menospagoaportes'], 2);
if ($menospagoaportes == 0) {
	$menospagoaportes = '';
}

$pdf->Cell(50, 4, '' . $rowdocequi['docequipucretencionfte'] . '', 1, 0, 'C');
$pdf->Cell(50, 4, 'Retencion Fte', 1, 0, 'C');
$pdf->Cell(50, 4, '' . number_format($rowdocequi['docequidebitoretencionfte'], 2) . '', 1, 0, 'C');
$pdf->Cell(50, 4, '' . number_format($rowdocequi['docequicreditoretencionfte'], 2) . '', 1, 0, 'C');
$pdf->Cell(5, 4, '', 'L R', 0, 'C');
$pdf->Cell(35, 4, 'Pago aportes', 1, 0, 'C');
$pdf->Cell(35, 4, '' . $menospagoaportes . '', 1, 1, 'C');
//4 

$totalbaseretencion = number_format($rowdocequi2['docequi2totalbaseretencion'], 2);
if ($totalbaseretencion == 0) {
	$totalbaseretencion = '';
}

$pdf->Cell(50, 4, '' . $rowdocequi['docequipucretencionica'] . '', 1, 0, 'C');
$pdf->Cell(50, 4, 'Retencion ICA', 1, 0, 'C');
$pdf->Cell(50, 4, '' . number_format($rowdocequi['docequidebitoretencionica'], 2) . '', 1, 0, 'C');
$pdf->Cell(50, 4, '' . number_format($rowdocequi['docequicreditoretencionica'], 2) . '', 1, 0, 'C');
$pdf->Cell(5, 4, '', 'L R', 0, 'C');
$pdf->Cell(35, 4, 'Total base retencion', 1, 0, 'C');
$pdf->Cell(35, 4, '' . $totalbaseretencion . '', 1, 1, 'C');
//5
$pdf->Cell(50, 4, '' . $rowdocequi['docequipucretencioniva'] . '', 1, 0, 'C');
$pdf->Cell(50, 4, 'Retencion IVA', 1, 0, 'C');
$pdf->Cell(50, 4, '' . number_format($rowdocequi['docequidebitoretencioniva'], 2) . '', 1, 0, 'C');
$pdf->Cell(50, 4, '' . number_format($rowdocequi['docequicreditoretencioniva'], 2) . '', 1, 0, 'C');
$pdf->Cell(5, 4, '', 'L R', 0, 'C');
$pdf->Cell(70, 4, 'ABONOS', 1, 1, 'C');
//6 

$abonos1 = $rowdocequi3['docequi3abonos1'];
if ($abonos1 == 'NONE') {
	$abonos1 = ' ';
}

$valorabonos1 = number_format($rowdocequi3['docequi3valorabonos1'], 2);
if ($valorabonos1 == 0) {
	$valorabonos1 = '';
}

$pdf->Cell(50, 4, '' . $rowdocequi['docequipucdescuento1'] . '', 1, 0, 'C');
$pdf->Cell(50, 4, '' . $rowdocequi['docequinombredescuento1'] . '', 1, 0, 'C');
$pdf->Cell(50, 4, '' . number_format($rowdocequi['docequidebitodescuento1'], 2) . '', 1, 0, 'C');
$pdf->Cell(50, 4, '' . number_format($rowdocequi['docequicreditodescuento1'], 2) . '', 1, 0, 'C');
$pdf->Cell(5, 4, '', 'L R', 0, 'C');
$pdf->Cell(35, 4, '' . $abonos1 . '', 1, 0, 'C');
$pdf->Cell(35, 4, '' . $valorabonos1 . '', 1, 1, 'C');
//7

$abonos2 = $rowdocequi3['docequi3abonos2'];
if ($abonos2 == 'NONE') {
	$abonos2 = ' ';
}

$valorabonos2 = number_format($rowdocequi3['docequi3valorabonos2'], 2);
if ($valorabonos2 == 0) {
	$valorabonos2 = '';
}

$pdf->Cell(50, 4, '' . $rowdocequi['docequipucdescuento2'] . '', 1, 0, 'C');
$pdf->Cell(50, 4, '' . $rowdocequi['docequinombredescuento2'] . '', 1, 0, 'C');
$pdf->Cell(50, 4, '' . number_format($rowdocequi['docequidebitodescuento2'], 2) . '', 1, 0, 'C');
$pdf->Cell(50, 4, '' . number_format($rowdocequi['docequicreditodescuento2'], 2) . '', 1, 0, 'C');
$pdf->Cell(5, 4, '', 'L R', 0, 'C');
$pdf->Cell(35, 4, '' . $abonos2 . '', 1, 0, 'C');
$pdf->Cell(35, 4, '' . $valorabonos2 . '', 1, 1, 'C');
//8 VACIOOOOOOOO

$abonos3 = $rowdocequi3['docequi3abonos3'];
if ($abonos3 == 'NONE') {
	$abonos3 = ' ';
}

$valorabonos3 = number_format($rowdocequi3['docequi3valorabonos3'], 2);
if ($valorabonos3 == 0) {
	$valorabonos3 = '';
}

$pdf->Cell(50, 4, '', 1, 0, 'C');
$pdf->Cell(50, 4, '', 1, 0, 'C');
$pdf->Cell(50, 4, '', 1, 0, 'C');
$pdf->Cell(50, 4, '', 1, 0, 'C');
$pdf->Cell(5, 4, '', 'L R', 0, 'C');
$pdf->Cell(35, 4, '' . $abonos3 . '', 1, 0, 'C');
$pdf->Cell(35, 4, '' . $valorabonos3 . '', 1, 1, 'C');
// 9

$totalabonos = number_format($rowdocequi3['docequi3totalabonos'], 2);
if ($totalabonos == 0) {
	$totalabonos = '';
}

$pdf->Cell(50, 4, '', 1, 0, 'C');
$pdf->Cell(50, 4, 'TOTAL', 1, 0, 'C');
$pdf->Cell(50, 4, '' . number_format($rowdocequi['docequidebitototal'], 2) . '', 1, 0, 'C');
$pdf->Cell(50, 4, '' . number_format($rowdocequi['docequicreditototal'], 2) . '', 1, 0, 'C');
$pdf->Cell(5, 4, '', 'L R', 0, 'C');
$pdf->Cell(35, 4, 'GIRAR', 1, 0, 'C');
$pdf->Cell(35, 4, '' . $totalabonos . '', 1, 1, 'C');

$pdf->Ln(5);

$pdf->Cell(200, 4, 'Certifico que he efectuado el pago a la seguridad social por los ingresos materia de facturacion igualmente manifiesto que estos aportes sirvieron ', 'T L R', 0, 'L');
$pdf->Cell(5, 4, '', 'L R', 0, 'C');

$nombre_fichero = $r . 'imagenes/firmas/' . $cedula . '.png';

if (file_exists($nombre_fichero)) {
	$pdf->Image($r . 'imagenes/firmas/' . $cedula . '.png', 230, 169, 35, 15);
}

$pdf->Cell(70, 4, 'FIRMA DEL BENEFICIARIO', 'T L R', 1, 'C');
$pdf->Cell(200, 4, 'para la disminucion de la base de retencion en la fuente en otro cobro         SI                NO     ', 'L R', 0, 'L');
$pdf->Cell(5, 4, '', 'L R', 0, 'C');
$pdf->Cell(70, 4, '', 'L R', 1, 'C');
$pdf->Cell(200, 4, '', 'B L R', 0, 'L');
$pdf->Cell(5, 4, '', 'L R', 0, 'C');
$pdf->Cell(70, 4, '', ' L R', 1, 'C');
$pdf->Cell(200, 4, '               Numero de pago de aportes', 'T L R', 0, 'L');
$pdf->Cell(5, 4, '', 'L R', 0, 'C');
$pdf->Cell(70, 4, '____________________________________', ' L R', 1, 'C');
$pdf->Cell(200, 4, '                       (Adjunto Copia)', 'B L R', 0, 'L');
$pdf->Cell(5, 4, '', 'L R', 0, 'C');
$pdf->Cell(70, 4, 'C.C.', 'B L R', 1, 'L');
$pdf->Cell(275, 4, date('Y/m/d H:i:s'), 0, 1, 'R');

$pdf->Output($r . 'pdf/nominas/' . $iden . '_DOC_EQUIVALENTE.pdf');
$pdf->Output();
