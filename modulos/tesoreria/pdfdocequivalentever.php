<?php
$r = '../../'; 
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
include($r.'incluir/fpdf/fpdf.php');

$iden = $_GET['iden'];
$sql = "SELECT * FROM nomina WHERE nominaid = ".$iden.";"; 
$rowprin = $db->query($sql)->fetch(PDO::FETCH_ASSOC);

$qryusuario = $db->query("SELECT * FROM nomina LEFT JOIN usuarios ON usuid = nominausuario INNER JOIN empresas ON empid = nominaempresa where nominaid = ".$iden."; ");
$rowusuario = $qryusuario->fetch(PDO::FETCH_ASSOC);
$resultados = $qryusuario->rowCount();
if (!empty($rowusuario['usunombre'])){
	$nombre = $rowusuario['usunombre'];
	$empresa = $rowusuario['empnombre'];
	$cedula = $rowusuario['usuid'];
} else {
	$nombre = $rowusuario['nominanombreusuario'];
	$empresa = $rowusuario['empnombre'];
	$cedula = $rowusuario['nominausuario'];
}

$qryempresa = $db->query("SELECT * FROM empresas where empid = ".$rowprin['nominaempresa']."; ");
$rowempresa = $qryempresa->fetch(PDO::FETCH_ASSOC);
$empresa = $rowempresa['empnombre'];

$hoy = date('Y/m/d');


$sqlconsecutivo = "SELECT * FROM nomina WHERE nominaempresa = ".$rowprin['nominaempresa']." AND nominatipo = 'DOC EQUIVALENTE' order by nominaid desc LIMIT 1;"; 
$rowconsecutivo = $db->query($sqlconsecutivo)->fetch(PDO::FETCH_ASSOC);

if(!empty($rowconsecutivo['nominaconsecutivo'])){
$consecutivo = $rowconsecutivo['nominaconsecutivo'] + 1;
} else {
	if ($rowprin['nominaempresa'] == 900306685 ){
		$consecutivo = 621;
	} elseif ($rowprin['nominaempresa'] == 900051528 ){
		$consecutivo = 3424;
	} elseif ($rowprin['nominaempresa'] == 900813686){
		$consecutivo = 220;
	} else {
		$consecutivo = 0;
	}
}

$qry = $db->query("UPDATE nomina SET nominaconsecutivo='$consecutivo' WHERE nominaid='$iden'; ");


$pdf = new FPDF();
$pdf->AddFont('LucidaConsole','','lucon.php');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Arial','',8);
$pdf->SetFont('Arial','',9);
if ($rowprin['nominaempresa'] == 900051528 or $rowprin['nominaempresa'] == 900813686 or $rowprin['nominaempresa'] == 900306685){
$pdf->Image($r.'imagenes/logos/'.$rowprin['nominaempresa'].'-doc.png',10,10,90,30);
}
$pdf->Cell(190,5,'No.        '.$consecutivo.'',0,1,'R');
$pdf->Cell(190,5,'DOCUMENTO EQUIVALENTE A FACTURA',0,1,'R');
$pdf->Cell(190,5,'',0,1,'R');

$pdf->Ln(15);
$pdf->Cell(100,5,'Persona Natural de quien se adquiere los bienes y/o servicios: ',0,0,'L');
$pdf->Cell(90,5,''.$usuario.'',0,1,'L');
$pdf->Cell(100,5,'Cedula:  ',0,0,'L');
$pdf->Cell(190,5,''.$cedula.'',0,1,'L');
$pdf->Cell(100,5,'NOMBRE:  ',0,0,'L');
$pdf->Cell(190,5,''.$nombre.'',0,1,'L');
$pdf->Cell(100,5,'CUIDAD Y FECHA DE OPERACION:  ',0,0,'L');
$pdf->Cell(190,5,'BOGOTA D.C '.$hoy.'',0,1,'L');



$salud = $rowprin['nominasueldo'] * 0.04;
$pension = $rowprin['nominasueldo'] * 0.04;
$totalpagos = $rowprin['nominasueldo'] + $rowprin['nominamovilizacion'];
$totaldescuentos = $salud + $pension;

$netopagar = $totalpagos - $totaldescuentos;

$pdf->SetFont('Arial','',8);
$pdf->Ln(5);
$pdf->Cell(33,4,'Descripcion de la ','L R T',0,'C');
$pdf->Cell(31,4,'Valor de la ','L R T',0,'C');
$pdf->Cell(31,4,'Tarifa de iva a la que','L R T',0,'C');
$pdf->Cell(31,4,'IVA teorico generado','L R T',0,'C');
$pdf->Cell(31,4,'Tarifa de retencion','L R T',0,'C');
$pdf->Cell(33,4,'Valor del impuesto','L R T',1,'C');

$pdf->Cell(33,4,'operacion','L R B',0,'C');
$pdf->Cell(31,4,'misma','L R B',0,'C');
$pdf->Cell(31,4,'se halla grabado','L R B',0,'C');
$pdf->Cell(31,4,'en la operacion','L R B',0,'C');
$pdf->Cell(31,4,'de iva vigente','L R B',0,'C');
$pdf->Cell(33,4,'asumido','L R B',1,'C');

$pdf->SetFont('Arial','',8);

$qrypri = $db->query("SELECT * FROM infodocequi where infonomina = ".$iden."; ");
$cont = 1;
while($rowpri = $qrypri->fetch(PDO::FETCH_ASSOC)){

$pdf->Cell(33,5,''.$rowpri['infodescripcion'].'',1,0,'L');
$pdf->Cell(31,5,''.number_format($rowpri['infovalor'],0,',','.').'',1,0,'C');
$pdf->Cell(31,5,''.$rowpri['infotarifaiva'].' %',1,0,'C');
$pdf->Cell(31,5,''.number_format($rowpri['infoivateorico'],0,',','.').'',1,0,'C');
$pdf->Cell(31,5,''.$rowpri['infotarifaretencion'].' %',1,0,'C');
$pdf->Cell(33,5,''.number_format($rowpri['infovalorimpuesto'],0,',','.').'',1,1,'C');

$infovalor = $rowpri['infovalor'] + $infovalor;
$infoivateorico = $rowpri['infoivateorico'] + $infoivateorico;
$infovalorimpuesto = $rowpri['infovalorimpuesto'] + $infovalorimpuesto;

$cont = $cont + 1;
}

for ($i = $cont; $i <= 12; $i++) {
$pdf->Cell(33,5,'',1,0,'L');
$pdf->Cell(31,5,'',1,0,'C');
$pdf->Cell(31,5,'',1,0,'C');
$pdf->Cell(31,5,'',1,0,'C');
$pdf->Cell(31,5,'',1,0,'C');
$pdf->Cell(33,5,'',1,1,'C');
}

$pdf->Cell(33,5,'Totales $',1,0,'R');
$pdf->Cell(31,5,''.number_format($infovalor,0,',','.').'',1,0,'C');
$pdf->Cell(31,5,'',1,0,'C');
$pdf->Cell(31,5,''.number_format($infoivateorico,0,',','.').'',1,0,'C');
$pdf->Cell(31,5,'',1,0,'C');
$pdf->Cell(33,5,''.number_format($infovalorimpuesto,0,',','.').'',1,1,'C');

$qry = $db->query("UPDATE nomina SET nominainfovalor='$infovalor', nominainfoivateorico='$infoivateorico', nominainfovalorimpuesto='$infovalorimpuesto' WHERE nominaid='$iden'; ");


$pdf->Ln(3);
$pdf->Cell(190,5,'CONTABILIZACIONES',1,1,'L');
$pdf->Ln(3);

$qrydocequi = $db->query("SELECT * FROM docequivalente where docequinomina = ".$iden."; ");
$rowdocequi = $qrydocequi->fetch(PDO::FETCH_ASSOC);

$qrydocequi2 = $db->query("SELECT * FROM docequivalente2 where docequi2nomina = ".$iden."; ");
$rowdocequi2 = $qrydocequi2->fetch(PDO::FETCH_ASSOC);

$qrydocequi3 = $db->query("SELECT * FROM docequivalente3 where docequi3nomina = ".$iden."; ");
$rowdocequi3 = $qrydocequi3->fetch(PDO::FETCH_ASSOC);

//1
$pdf->Cell(31,5,'PUC',1,0,'C');
$pdf->Cell(31,5,'cuenta',1,0,'C');
$pdf->Cell(31,5,'DEBITO',1,0,'C');
$pdf->Cell(31,5,'CREDITO',1,0,'C');
$pdf->Cell(4,5,'','L R',0,'C');
$pdf->Cell(62,5,'BASE RETENCION',1,1,'C');
//2 

$valorservicios = number_format($rowdocequi2['docequi2valorservicios'],0,',','.');
if ($valorservicios == 0){$valorservicios = '';}
 
$pdf->SetFont('Arial','',8);
$pdf->Cell(31,5,''.$rowdocequi['docequipucservicios'].'',1,0,'C');
$pdf->Cell(31,5,'Servicios',1,0,'C');
$pdf->Cell(31,5,''.number_format($rowdocequi['docequidebitoservicios'],0,',','.').'',1,0,'C');
$pdf->Cell(31,5,''.number_format($rowdocequi['docequicreditoservicios'],0,',','.').'',1,0,'C');
$pdf->Cell(4,5,'','L R',0,'C');
$pdf->Cell(31,5,'Valor servicios',1,0,'C');
$pdf->Cell(31,5,''.$valorservicios.'',1,1,'C');
//3

$menospagoaportes = number_format($rowdocequi2['docequi2menospagoaportes'],0,',','.');
if ($menospagoaportes == 0){$menospagoaportes = '';}
 
$pdf->Cell(31,5,''.$rowdocequi['docequipucretencionfte'].'',1,0,'C');
$pdf->Cell(31,5,'Retencion Fte',1,0,'C');
$pdf->Cell(31,5,''.number_format($rowdocequi['docequidebitoretencionfte'],0,',','.').'',1,0,'C');
$pdf->Cell(31,5,''.number_format($rowdocequi['docequicreditoretencionfte'],0,',','.').'',1,0,'C');
$pdf->Cell(4,5,'','L R',0,'C');
$pdf->Cell(31,5,'Pago aportes',1,0,'C');
$pdf->Cell(31,5,''.$menospagoaportes.'',1,1,'C');
//4 

$totalbaseretencion = number_format($rowdocequi2['docequi2totalbaseretencion'],0,',','.');
if ($totalbaseretencion == 0){$totalbaseretencion = '';}

$pdf->Cell(31,5,''.$rowdocequi['docequipucretencionica'].'',1,0,'C');
$pdf->Cell(31,5,'Retencion ICA',1,0,'C');
$pdf->Cell(31,5,''.number_format($rowdocequi['docequidebitoretencionica'],0,',','.').'',1,0,'C');
$pdf->Cell(31,5,''.number_format($rowdocequi['docequicreditoretencionica'],0,',','.').'',1,0,'C');
$pdf->Cell(4,5,'','L R',0,'C');
$pdf->Cell(31,5,'Total base retencion',1,0,'C');
$pdf->Cell(31,5,''.$totalbaseretencion.'',1,1,'C');
//5
$pdf->Cell(31,5,''.$rowdocequi['docequipucretencioniva'].'',1,0,'C');
$pdf->Cell(31,5,'Retencion IVA',1,0,'C');
$pdf->Cell(31,5,''.number_format($rowdocequi['docequidebitoretencioniva'],0,',','.').'',1,0,'C');
$pdf->Cell(31,5,''.number_format($rowdocequi['docequicreditoretencioniva'],0,',','.').'',1,0,'C');
$pdf->Cell(4,5,'','L R',0,'C');
$pdf->Cell(62,5,'ABONOS',1,1,'C');
//6 

$abonos1 = $rowdocequi3['docequi3abonos1'];
if ($abonos1 == 'NONE'){$abonos1 = ' ';}

$valorabonos1 = number_format($rowdocequi3['docequi3valorabonos1'],0,',','.');
if ($valorabonos1 == 0){$valorabonos1 = '';} 

$pdf->Cell(31,5,''.$rowdocequi['docequipucdescuento1'].'',1,0,'C');
$pdf->Cell(31,5,''.$rowdocequi['docequinombredescuento1'].'',1,0,'C');
$pdf->Cell(31,5,''.number_format($rowdocequi['docequidebitodescuento1'],0,',','.').'',1,0,'C');
$pdf->Cell(31,5,''.number_format($rowdocequi['docequicreditodescuento1'],0,',','.').'',1,0,'C');
$pdf->Cell(4,5,'','L R',0,'C');
$pdf->Cell(31,5,''.$abonos1.'',1,0,'C');
$pdf->Cell(31,5,''.$valorabonos1.'',1,1,'C');
//7

$abonos2 = $rowdocequi3['docequi3abonos2'];
if ($abonos2 == 'NONE'){$abonos2 = ' ';}

$valorabonos2 = number_format($rowdocequi3['docequi3valorabonos2'],0,',','.');
if ($valorabonos2 == 0){$valorabonos2 = '';} 

$pdf->Cell(31,5,''.$rowdocequi['docequipucdescuento2'].'',1,0,'C');
$pdf->Cell(31,5,''.$rowdocequi['docequinombredescuento2'].'',1,0,'C');
$pdf->Cell(31,5,''.number_format($rowdocequi['docequidebitodescuento2'],0,',','.').'',1,0,'C');
$pdf->Cell(31,5,''.number_format($rowdocequi['docequicreditodescuento2'],0,',','.').'',1,0,'C');
$pdf->Cell(4,5,'','L R',0,'C');
$pdf->Cell(31,5,''.$abonos2.'',1,0,'C');
$pdf->Cell(31,5,''.$valorabonos2.'',1,1,'C');
//8 VACIOOOOOOOO

$abonos3 = $rowdocequi3['docequi3abonos3'];
if ($abonos3 == 'NONE'){$abonos3 = ' ';}

$valorabonos3 = number_format($rowdocequi3['docequi3valorabonos3'],0,',','.');
if ($valorabonos3 == 0){$valorabonos3 = '';} 

$pdf->Cell(31,5,'',1,0,'C');
$pdf->Cell(31,5,'',1,0,'C');
$pdf->Cell(31,5,'',1,0,'C');
$pdf->Cell(31,5,'',1,0,'C');
$pdf->Cell(4,5,'','L R',0,'C');
$pdf->Cell(31,5,''.$abonos3.'',1,0,'C');
$pdf->Cell(31,5,''.$valorabonos3.'',1,1,'C');
// 9

$totalabonos = number_format($rowdocequi3['docequi3totalabonos'],0,',','.');
if ($totalabonos == 0){$totalabonos = '';} 

$pdf->Cell(31,5,'',1,0,'C');
$pdf->Cell(31,5,'TOTAL',1,0,'C');
$pdf->Cell(31,5,''.number_format($rowdocequi['docequidebitototal'],0,',','.').'',1,0,'C');
$pdf->Cell(31,5,''.number_format($rowdocequi['docequicreditototal'],0,',','.').'',1,0,'C');
$pdf->Cell(4,5,'','L R',0,'C');
$pdf->Cell(31,5,'GIRAR',1,0,'C');
$pdf->Cell(31,5,''.$totalabonos.'',1,1,'C');

$pdf->Ln(5);

$pdf->Cell(124,5,'Certifico que he efectuado el pago a la seguridad social por los ingresos materia de facturacion ','T L R',0,'L');
$pdf->Cell(4,5,'','L R',0,'C');
$pdf->Cell(62,5,'FIRMA DEL BENEFICIARIO','T L R',1,'C');
$pdf->Cell(124,4,'Igualmente manifiesto que estos aportes sirvieron para la disminucion de la base de retencion en ','L R',0,'L');
$pdf->Cell(4,4,'','L R',0,'C');
$pdf->Cell(62,4,'','L R',1,'C');
$pdf->Cell(124,4,'la fuente en otro cobro         SI                NO     ','B L R',0,'L');
$pdf->Cell(4,4,'','L R',0,'C');
$pdf->Cell(62,4,'',' L R',1,'C');
$pdf->Cell(124,5,'               Numero de pago de aportes','T L R',0,'L');
$pdf->Cell(4,5,'','L R',0,'C');
$pdf->Cell(62,5,'____________________________________',' L R',1,'C');
$pdf->Cell(124,5,'                       (Adjunto Copia)','B L R',0,'L');
$pdf->Cell(4,5,'','L R',0,'C');
$pdf->Cell(62,5,'C.C.','B L R',1,'L');
$pdf->Cell(190,5,date('Y/m/d H:i:s'),0,1,'R');

$pdf->Output($r.'pdf/nominas/'.$iden.'_DOC_EQUIVALENTE.pdf');
$pdf->Output();

?>