<?php
$r = '../../';
//INCLUIR SESION Y CONECCION
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
require($r.'incluir/funciones.php');

$mensaje = $_GET['mensaje'];
$error = $_GET['error'];

if(isset($_POST['consultar'])){
	$fecha1 = $_POST['fecha1'];
	$fecha2 = $_POST['fecha2'];
	$proveedor = $_POST['proveedor'];
	$empresa = $_POST['empresa'];
	$estado = $_POST['estado'];
}

if($_POST['btnvalidar']){
$realipagos = $_POST['realipagos'];
$array = array_envia($realipagos);
	if(empty($realipagos)){
		$error = $realipagos." Es Necesario que seleccione algun concepto a pagar, </br> Por favor realize el proceso nuevamente!";
		header('Location:pagomasivo_cyg.php?error='.$error.'');
		die();
	} 
	
$forma_pa = $_POST['forma_pa'];
$detalle = $_POST['detalle'];
$detcheque = $_POST['detcheque'];
$adicional = $_POST['adicional'];
$banco = $_POST['banco'];

if ($detcheque <> ''){ $numcheque = $detcheque;} else { $numcheque ='';}

foreach ($realipagos as $valor) {
   $qry = $db->query("UPDATE tesoreria SET tesovalid='2', tesocheque='".$numcheque."', tesobanco='".$banco."', tesomediopago='".$forma_pa."', tesomediopagodetalle='".$detalle."', tesoadicional='".$adicional."' WHERE tesoid='".$valor."';"); 
//Log de acciones realizadas por usuario en la BD	
		$qrylogsregister = $db->query("SELECT * FROM usuarios INNER JOIN perfiles ON usuperfil = perid WHERE usuid = ".$_SESSION['id']); //verificacion usuario por ID de sesion
		$rowlogsregister = $qrylogsregister->fetch(PDO::FETCH_ASSOC);	
		$qrylogsregister = $db->query("INSERT INTO logs ( usuario, id_usuario , ip_usuario , accion_usuario, fecha_accion) VALUES ( '".$rowlogsregister['usunombre']."', '".$_SESSION['id']."', '".$_SERVER['REMOTE_ADDR']."' , 'APLICO PAGO MASIVO EN TESORERIA ID NUMERO ".$valor." - ".$detalle." ".$detcheque." - ".$adicional." '  , '".date("Y-m-d H:i:s")."' );"); //anexo de informacion de accion a la BD tabla LOGS	
   if ($qry){
	   $mensaje = "Pagos realizados correctamente";
	   header('Location:pagomasivo_cyg.php?mensaje='.$mensaje.'');
   } else {
		$error = $realipagos." No se pudo efectuar el pago correctamente porfavor intente nuevamente, de lo contrario comuniquese con el encargado del portal";
		header('Location:pagomasivo_cyg.php?error='.$error.'');
		die();	   
   }
}
	
}

?>
<!doctype html>
<html>
<head>

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>
<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
<script type="text/javascript">
function tipopago(valor){
	if(valor == 'Cheque'){
		document.getElementById("detalle").placeholder ='¿A quien se giro el cheque?';
		document.getElementById("detcheque").disabled = false;
		document.getElementById("divdetcheque").style.display = 'Initial';
	}else if (valor == 'Consignacion'){
		document.getElementById("detalle").placeholder ='Ingrese nombre de a quien se consigno';
		document.getElementById("detcheque").disabled = true;
		document.getElementById("divdetcheque").style.display = 'none';
		document.getElementById("detcheque").value ='';
	}else if (valor == 'Giro'){
		document.getElementById("detalle").placeholder ='Ingrese nombre de a quien se realizo la transferencia';
		document.getElementById("detcheque").disabled = true;
		document.getElementById("divdetcheque").style.display = 'none';
		document.getElementById("detcheque").value ='';
	}else if (valor == 'Pago en Efectivo'){
		document.getElementById("detalle").placeholder ='Ingrese nombre de a quien se le pago';
		document.getElementById("detcheque").disabled = true;
		document.getElementById("divdetcheque").style.display = 'none';
		document.getElementById("detcheque").value ='';
	}else {
		document.getElementById("detcheque").disabled = true;
		document.getElementById("divdetcheque").style.display = 'none';
		document.getElementById("detcheque").value ='';
		document.getElementById("detalle").placeholder ='Detalle del pago';
	}
}
$(document).ready(function(){
	$('#form').validationEngine({ //VALIDACION DE CAMPOS REQUERIDOS EN FORMULARIO
		showOneMessage:true,
		onValidationComplete: function(form, status){
        	if(status) {               
            	carga();
            	return true;
			}
		}
	});

	
	$('.btnvalidar').button({ icons: { primary: 'ui-icon ui-icon-check' }});	
	$('.btnconsulta').button({ icons: { primary: 'ui-icon ui-icon-search' }}); //ACCION DE LA CLASE EN EL BOTON DE CONSULTA
	$('#dialog-message').dialog({ //CARACTERISTICAS DE LA VENTANA MODAL 
		height: 80,
		width: 'auto',
		modal: true
	});
	$('#checkAll').click(function () {    
    	$('input:checkbox').prop('checked', this.checked);    
	});
	$('#tabla').dataTable({
		'bJQueryUI': true,
		'bStateSave': true,
		'sPaginationType': 'full_numbers',
		'oLanguage': {
			'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
		},
		'bSort': false,
		'bLengthChange': false,
        'bFilter': false,
	});
});
</script>
<!-- ESTILOS EMBEBIDOS PARA MANEJAR FORMULARIO INDEPENDIENTEMENTE -->
<style type="text/css">
#form form{ width:660px; }
#form fieldset{ padding:10px; display:block; width:660px; margin:20px auto }
#form legend{ font-weight: bold; margin-left:5px; padding:5px }
#form label{ display:inline-block; width:80px; text-align:right; float:left; margin:0.3em 2% 0 0 }
#form p{ margin:5px 0 }
</style>
</head>
<body>
<?php require($r.'incluir/src/login.php') ?> <!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
<section id="principal">
<?php require($r.'incluir/src/cabeza.php') ?> <!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
<?php require($r.'incluir/src/menu.php') ?> <!-- INCLUIMOS MENU PRINCIPAL -->
<article id="cuerpo">
<article class="mapa">
<a href="#">Principal</a><div class="mapa_div"></div><a href="#">Tesoreria</a><div class="mapa_div"></div><a href="#">Costos y Gastos</a><div class="mapa_div"></div><a class="current">Confirmar Pago Masivo</a>
</article>
<article id="contenido">
<form id="form" name="form" action="pagomasivo_cyg.php" method="post"> <!-- ENVIO FORMULARIO POR POST A LISTAR.PHP -->
<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
<legend class="ui-widget ui-widget-header ui-corner-all">Confirmar Pago Masivo</legend>
	<p style="text-align:center; font-size:18px; padding:10px 0;"><strong>Recuerde que los datos que ingrese a continuacion seran asignados a todos los pagos que realize.</strong></p>
	<p>
		<label for="forma_pa">Forma de Pago:</label>
		<select name="forma_pa" class="validate[required] text-input" onChange="tipopago(this.value);">
		<option value="">SELECCIONE</option>
		<option value="Cheque">Cheque</option>
		<option value="Consignacion">Consignacion</option>
		<option value="Giro">Giro</option>
		<option value="Pago en Efectivo">Pago en Efectivo</option>			
		</select>
		-
		<input type="text" style="width:350px" placeholder="Detalle del pago" name="detalle" id="detalle" class="validate[required] text-input "/> 

	<div id="divdetcheque" style="display:none;">
	<label for="detcheque"></label>
	<input type="text" style="width:130px" name="detcheque" id="detcheque" placeholder="Numero de cheque" class="validate[required] text-input" disabled />
	</div>
	</p>
	<p>
		<label for="banco">Banco:</label>
		<select name="banco" class="validate[required] text-input"  >
		<option value="">SELECCIONE</option>
		<?php
		$qry = $db->query("SELECT * FROM bancos  ORDER BY banconombre");
		while($row = $qry->fetch(PDO::FETCH_ASSOC))
			echo '<option value='.$row['bancoid'].'>'.$row['banconombre'].'</option>';
		?>
		</select>
	</p>
	<p>
	<label for="adicional">Adicional:</label>
	<textarea rows="7" cols="70" name="adicional" class="validate[required]" id="adicional"> </textarea>
	</p>

</fieldset>
	<p style="text-align:center; font-size:13px; padding:10px 0;"> Seleccione porfavor los pagos que desea realizar.</p>
	<table id="tabla">
	<thead>
	<tr>
	<th title="Confirmar"></th>
	<th>Num. Factura</th>
	<th>Concepto</th>
	<th>Fecha de Pago</th>
	<th>Empresa</th>
	<th align="center">Proveedor</th>
	<th align="center">Valor</th>
	<th align="center">Abono</th>
	</tr>
	</thead>
	<tbody>
	<?php
	
	
	
	
	$filtro = 'fecha1='.$fecha1.'&fecha2='.$fecha2.'&proveedor='.$proveedor.'&empresa='.$empresa.'&estado='.$estado;
	$con = 'SELECT * FROM tesoreria WHERE tesovalid = "1" AND tesofechapagar <> "" AND tesousuario <> "" AND tesomediopago = ""  AND '; 
	$ord = 'ORDER BY tesofecha DESC'; // Ordenar la Consulta
	$group = ''; // Realizar Group By 

	if($fecha1 == ''  && $proveedor == '' && $empresa == '' && $estado == '' ) $sql = "$con  $group $ord";
	elseif($fecha1 != ''  && $proveedor == '' && $empresa == '' && $estado == '' ) $sql = "$con tesofechapagar between '$fecha1' AND '$fecha2'  $group $ord";
	elseif($fecha1 != ''  && $proveedor != '' && $empresa == '' && $estado == '' ) $sql = "$con tesofechapagar between '$fecha1' AND '$fecha2' AND tesoproveedor = '$proveedor' $group $ord";
	elseif($fecha1 != ''  && $proveedor != '' && $empresa != '' && $estado == '' ) $sql = "$con tesofechapagar between '$fecha1' AND '$fecha2' AND tesoproveedor = '$proveedor' AND tesoempresa = '$empresa' $group $ord";
	elseif($fecha1 == ''  && $proveedor != '' && $empresa == '' && $estado == '' ) $sql = "$con tesoproveedor = '$proveedor'  $group $ord";
	elseif($fecha1 == ''  && $proveedor != '' && $empresa != '' && $estado == '' ) $sql = "$con tesoproveedor = '$proveedor' AND tesoempresa = '$empresa' $group $ord";
	elseif($fecha1 == ''  && $proveedor == '' && $empresa != '' && $estado == '' ) $sql = "$con tesoempresa = '$empresa' $group $ord";
	elseif($fecha1 != ''  && $proveedor == '' && $empresa != '' && $estado == '' ) $sql = "$con tesofechapagar between '$fecha1' AND '$fecha2' AND tesoempresa = '$empresa' $group $ord";
	elseif($fecha1 == ''  && $proveedor == '' && $empresa == '' && $estado != '' ) $sql = "$con tesovalid = '$estado' $group $ord";
	elseif($fecha1 != ''  && $proveedor == '' && $empresa == '' && $estado != '' ) $sql = "$con tesofechapagar between '$fecha1' AND '$fecha2' AND tesovalid = '$estado' $group $ord";
	elseif($fecha1 == ''  && $proveedor != '' && $empresa == '' && $estado != '' ) $sql = "$con tesoproveedor = '$proveedor' AND tesovalid = '$estado' $group $ord";
	elseif($fecha1 == ''  && $proveedor == '' && $empresa != '' && $estado != '' ) $sql = "$con tesoempresa = '$empresa' AND tesovalid = '$estado' $group $ord";
	elseif($fecha1 == ''  && $proveedor != '' && $empresa != '' && $estado != '' ) $sql = "$con tesoproveedor = '$proveedor' AND tesoempresa = '$empresa' AND tesovalid = '$estado' $group $ord";
	elseif($fecha1 != ''  && $proveedor != '' && $empresa == '' && $estado != '' ) $sql = "$con tesofechapagar between '$fecha1' AND '$fecha2' AND tesoproveedor = '$proveedor' AND tesovalid = '$estado' $group $ord";
	elseif($fecha1 != ''  && $proveedor != '' && $empresa != '' && $estado != '' ) $sql = "$con tesofechapagar between '$fecha1' AND '$fecha2' AND tesoproveedor = '$proveedor' AND tesoempresa = '$empresa' AND tesovalid = '$estado' $group $ord";

	
	
	
	
	$qrypro = $db->query($sql);
	while($rowpro = $qrypro->fetch(PDO::FETCH_ASSOC)){
		$qryempresa = $db->query("SELECT * FROM empresas where empid = ".$rowpro['tesoempresa']."; ");
		$rowempresa = $qryempresa->fetch(PDO::FETCH_ASSOC);
		$empresa = $rowempresa['empnombre'];

		$qryproveedorname = $db->query("SELECT * FROM proveedores_teso where protesoid = ".$rowpro['tesoproveedor']."; ");
		$rowproveedorname = $qryproveedorname->fetch(PDO::FETCH_ASSOC);
		$proveedor_name = $rowproveedorname['protesonombre'];
	?>
	<tr>
	<td><?php echo '<input type="checkbox" title="'.$rowpro['tesoid'].'" name="realipagos[]" value='.$rowpro['tesoid'].'>'; ?></td>
	<td><?php echo $rowpro['tesonumfactura'] ?></td>
	<td><?php echo $rowpro['tesoconcepto'] ?></td>
	<td align="center"><?php echo $rowpro['tesofechapagar'] ?></td>
	<td><?php echo $empresa ?></td>
	<td align="center"><?php echo $proveedor_name ?></td>
	<td align="center"><?php echo number_format($rowpro['tesovalor'],0,',','.') ?></td>
	<td align="center"><?php if ($row['tesoabono'] == 'SI'){echo $row['tesoporcentaje']."<br>".number_format($row['tesovalorfinal'],0,',','.');}else{echo "NO";} ?></td>
	</tr>
	<?php 
	$vlr1 = $vlr1 + $rowpro['tesovalor'];
	$vlrab = $vlrab + $rowpro['tesovalorfinal'];
	} 
	?>
	</tbody>
	<tfoot>
	<tr><td colspan="5"></td><td colspan="1" align="center" bgcolor="#D1CFCF">Valor Total</td><td align="center" bgcolor="#D1CFCF"><?php echo number_format($vlr1,0,',','.') ?></td><td align="center" bgcolor="#D1CFCF"><?php echo number_format($vlrab,0,',','.') ?></td></tr>
	</tfoot>
	</table>

<p class="boton">
<button type="button" class="btnatras" onClick="carga(); location.href='con_pagomasivo_cyg.php'">Atras</button>
<button type="submit" class="btnvalidar" name="btnvalidar" value="validar">Confirmar</button> <!-- BOTON VALIDAR -->
</p>
</form>
</article>
</article>
<?php require($r.'incluir/src/pie.php') ?>  <!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
</section>
<?php
if(isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>'.$error.'</div>';
elseif(isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>'.$mensaje.'</div>';
?>
</body>
</html>