<?php
	$r = '../../';
	//INCLUIR SESION Y CONECCION
	require($r . 'incluir/session.php');
	require($r . 'incluir/connection.php');

	$id1 = $_GET['id1'];
	$fecha1 = $_GET['fecha1'];
	$fecha2 = $_GET['fecha2'];
	$proveedor = $_GET['proveedor'];
	$empresa = $_GET['empresa'];
	$estado = $_GET['estado'];

	$filtro = 'fecha1=' . $fecha1 . '&fecha2=' . $fecha2 . '&proveedor=' . $proveedor . '&empresa=' . $empresa . '&estado=' . $estado;

	$qry = $db->query("SELECT * FROM tesoreria WHERE tesoid='" . $id1 . "';");

?>
<!doctype html>
<html lang="es">

<head>
    <title>ASIGNAR FECHA DE PAGO</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#fechaone').datepicker({ // VALIDACION DE LA FECHA UNO
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
				onClose: function(selectedDate) {
					$('#fechatwo').datepicker('option', 'minDate', selectedDate);
				}
			}).keypress(function(event) {
				event.preventDefault()
			});
			$('#fechatwo').datepicker({ // VALIDACION DE LA FECHA DOS
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
				onClose: function(selectedDate) {
					$('#fechaone').datepicker('option', 'maxDate', selectedDate);
				}
			}).keypress(function(event) {
				event.preventDefault()
			});
		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
		<?php require($r . 'incluir/src/menu.php') ?>
		<!-- INCLUIMOS MENU PRINCIPAL -->
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Tesoreria</a>
				<div class="mapa_div"></div><a href="#">Costos y Gastos</a>
				<div class="mapa_div"></div><a class="current">Anexar Fecha</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="listar_cyg.php?<?php echo $filtro ?>" method="post">
					<!-- ENVIO FORMULARIO POR POST A LISTAR.PHP -->
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">Fecha de pago Costos y Gastos</legend>
						<?php
						while ($rowprin = $qry->fetch(PDO::FETCH_ASSOC)) { //INFORMACION DE LA CONSULTA

						?>
							<p>
								<label>Identificador:</label>
								<input type="text" name="id1" value="<?php echo $rowprin['tesoid']; ?>" id="id1" class="text-input" readonly />
							</p>
							<p>
								<label> Fecha:</label>
								<input type="text" name="fechaone" value="<?php echo $rowprin['tesofecha']; ?>" id="fechaone" class="text-input fecha" readonly />
							</p>
							<p>
								<label for="proveedor">Proveedor:</label>

								<?php
								$proveedor_teso = $rowprin['tesoproveedor'];
								if($proveedor_teso != "")
								{
									$qry = $db->query("SELECT * FROM proveedores_teso WHERE protesoid = '" . $rowprin['tesoproveedor'] . "' ORDER BY protesonombre");
									$row = $qry->fetch(PDO::FETCH_ASSOC);
									echo '<input type="hidden" name="proveedor" value="' . $row['protesoid'] . '">'.
									'<input type="text" value="' . $row['protesonombre'] . '" readonly>';
								}
								else{
									echo '<input type="hidden" name="proveedor" value="">'.
									'<input type="text" value="NO SE ASIGNO" readonly>';
								}
								?>
							</p>
							<p>
								<label for="empresa">Empresa:</label>
									
								<?php
									$qry = $db->query("SELECT * FROM empresas WHERE empid = '" . $rowprin['tesoempresa'] . "' ORDER BY empnombre");
									$row = $qry->fetch(PDO::FETCH_ASSOC);
									echo '<input type="hidden" name="empresa" value="' . $row['empid'] . '">'.
									'<input type="text" value="' . $row['empnombre'] . '" readonly>';
								?>
							</p>
							<p>
								<label> Fecha de pago:</label>
								<input type="text" name="fechatwo" id="fechatwo" class="validate[required] text-input fecha" />
							</p>


						<?php
						}
						?>
						<p class="boton">
							<button type="submit" class="btn btn-primary btnvalidar" name="anexofecha" value="validar">Validar</button> <!-- BOTON VALIDAR -->
						</p>
					</fieldset>
				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
		<!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
	</section>
	<?php
	if (isset($_GET['error'])) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['error'] . '</div>'; // MENSAJE MODAL ERROR
	?>
</body>

</html>