<?php
	$r = '../../';
	require($r . 'incluir/session.php');
	require($r . 'incluir/connection.php');
	require($r . 'incluir/funciones.php');

	if (isset($_GET['mensaje']))
		$mensaje = $_GET['mensaje'];
	if (isset($_GET['error']))
		$error = $_GET['error'];

	if (isset($_POST['consultar'])) {
		$fecha1 = $_POST['fecha1'];
		$fecha2 = $_POST['fecha2'];
		$usuario = $_POST['usuario'];
	} elseif (isset($_GET['fecha1'])) {
		$fecha1 = $_GET['fecha1'];
		$fecha2 = $_GET['fecha2'];
		$usuario = $_GET['usuario'];
	}

	if (isset($_GET['anular'])) {
		$id = $_GET['id'];
		$fecha1 = $_GET['fecha1'];
		$fecha2 = $_GET['fecha2'];
		$usuario = $_GET['usuario'];

		$filtro = 'fecha1=' . $fecha1 . '&fecha2=' . $fecha2 . '&usuario=' . $usuario;

		$qry = $db->query("UPDATE nomina SET nominaestado='ANULADO' WHERE nominaid='" . $id . "';");
		if ($qry) {
			//Log de acciones realizadas por usuario en la BD	
			$qrylogsregister = $db->query("SELECT * FROM usuarios INNER JOIN perfiles ON usuperfil = perid WHERE usuid = '" . $_SESSION['id']."'"); //verificacion usuario por ID de sesion
			$rowlogsregister = $qrylogsregister->fetch(PDO::FETCH_ASSOC);
			$qrylogsregister = $db->query("INSERT INTO logs ( usuario, id_usuario , ip_usuario , accion_usuario, fecha_accion) VALUES ( '" . $rowlogsregister['usunombre'] . "', '" . $_SESSION['id'] . "', '" . $_SERVER['REMOTE_ADDR'] . "' , 'HA ANULADO EL PROCESO EN AUX MOVILIZACION CON ID " . $id1 . "'  , '" . date("Y-m-d H:i:s") . "' );"); //anexo de informacion de accion a la BD tabla LOGS	
			$mensaje = "Se ha anulado correctamente.";
			header('Location:anularnomina.php?id=' . $id . '&' . $filtro);
		} else {
			$error = 'No se pudo anular, intente nuevamente o contacte con el administrador del portal.';
			header('Location:listartransporte.php?error=' . $error . '&' . $filtro);
		}
	}

	$filtro = 'fecha1=' . $fecha1 . '&fecha2=' . $fecha2 . '&usuario=' . $usuario;
	$con = "SELECT nomina.*, e.empnombre, e.empid, u.usunombre, u.usucargo FROM nomina 
	LEFT JOIN empresas e ON e.empid = nominaempresa
	LEFT JOIN usuarios u ON u.usuid = nominausuario";
	$ord = ' ORDER BY nominaid DESC'; // Ordenar la Consulta


	/* Los parametros de la consulta sql se genera dinamicamente 
			en base a los datos recibidos para delimitar los resultados */
	$parameters = [];
	array_push($parameters, "nominatipo = 'AUX TRANSPORTE'");
	if ($fecha1 != "")
		array_push($parameters, "nominafecha between '$fecha1' AND '$fecha2'");
	if ($usuario != "")
		array_push($parameters, "nominausuario LIKE '%$usuario%'");

	// Consulta base
	$sql = $con;
	foreach ($parameters as $index => $parameter) {
		// Se agregan los parametros del WHERE
		if ($index == 0)
			$sql .= " WHERE " . $parameter;
		else
			$sql .= " AND " . $parameter;
	}
	// Se completa la consulta
	$sql .= $ord;

	$qry = $db->query($sql);

?>
<!doctype html>
<html lang="es">

<head>
    <title>LISTAR AUX TRAMPORTE</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>

	<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
	<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>

	<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>
	<script>
		$(document).ready(function() {
			$('#tabla').dataTable({ //QRY PARA MANEJO Y VISUALIZACION DE TABLAS
				'bJQueryUI': true,
				'sPaginationType': "full_numbers",
				'oLanguage': {
					'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
				},
				'bSort': false,
			});


			$('.pdf').click(function() {
				newSrc = $(this).attr('data-rel');
				if (validation_file(newSrc) == 200)
					$('#modal').html("<iframe src='" + newSrc + "' width='100%' height='100%'></iframe>");
				else
					$('#modal').html("<spam>El PDF no fue encontrado</spam>");
				$('#modal').dialog({
					modal: true,
					width: '800',
					height: '900',
					title: 'PDF de la factura'
				});
			});
			$('.anular').click(function(e) { // VENTANA MODAL PARA CONFIRMAR ACCIONES
				e.preventDefault();
				var targetUrl = $(this).attr("href");
				var $dialog_link_follow_confirm = $('<div></div>').html('<p><span class="ui-icon ui-icon-alet" style="float:left; margin:2px 7px 7px 0;"></span>Esta seguro que desea anular este proceso?</p>').
				dialog({
					title: 'Porfavor confirmar',
					buttons: {
						'Si': function() {
							carga();
							window.location.href = targetUrl;
						},
						'No': function() {
							$(this).dialog("close");
						}
					},
					modal: true,
					width: 'auto',
					height: 'auto'
				});
				$dialog_link_follow_confirm.dialog('open');
			});
		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Tesoreria</a>
				<div class="mapa_div"></div><a href="#">Movilizacion</a>
				<div class="mapa_div"></div><a class="current">Listar</a>
			</article>
			<article id="contenido">
				<h2>Listado de Movilizacion</h2>
				<table id="tabla">
					<thead>
						<tr>
							<th>ID</th>
							<th>Estado</th>
							<th>Usuario</th>
							<th>Empresa</th>
							<th>Fecha</th>
							<th>Movilizacion</th>
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php
						$movilizacion = 0;
						while ($row = $qry->fetch(PDO::FETCH_ASSOC)) { //INFORMACION DE LA CONSULTA

							$empresa = $row['empnombre'];

						?>
							<tr <?php if ($row['nominaestado'] != 'ACTIVO') { echo 'style="background-color:#f9a5a5"';}?>>
								<td><?php echo $row['nominaid'] ?></td>
								<td><?php echo $row['nominaestado'] ?></td>
								<td><?php echo $row['usunombre'] ?></td>
								<td align="center"><?php echo $empresa ?></td>
								<td align="center"><?php echo $row['nominafecha'] ?></td>
								<td align="center"><?php echo number_format($row['nominamovilizacion'], 2) ?></td>
								<td align="center"><img src="<?php echo $r ?>imagenes/iconos/pdf.png" class="pdf" data-rel="<?php echo $r . 'pdf/nominas/' . $row['nominaid'] . '_AUX_TRANSPORTE.pdf' ?>" title="PDF" /></td>
								<?php if ($row['nominaestado'] == 'ACTIVO') { ?>
									<td align="center"><a href="listartransporte.php?<?php echo 'anular=1&id=' . $row['nominaid'] . '&' . $filtro ?>" title="Anular" class="anular"><img src="<?php echo $r ?>imagenes/iconos/cancelar.png" /></a></td> <!-- ENVIA LAS VARIABLES NECESARIAS PARA VALIDAR JUNTO CON VENTANA MODAL PARA ANULAR -->
								<?php } else { ?>
									<td align="center"><img class="gray" src="<?php echo $r ?>imagenes/iconos/cancelar.png" /></td>
								<?php } ?>
							</tr>
						<?php
							if ($row['nominaestado'] == 'ACTIVO') {
								$movilizacion = $movilizacion + $row['nominamovilizacion'];
							}
						}
						?>
					</tbody>
					<tfoot>
						<tr>
							<td colspan="3"></td>
							<td colspan="2" align="center" bgcolor="#D1CFCF">Valor Total</td>
							<td align="center" bgcolor="#D1CFCF"><?php echo number_format($movilizacion, 2) ?></td>
							<td colspan="3"></td>
						</tr>
					</tfoot>
				</table>
				<center>
					<br><br>
				</center>
				<p class="boton">
					<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href='consultartransporte.php'">Atras</button>
				</p>


			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<?php
	if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>';
	elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>';
	?>
	<div id="modal" style="display:none"></div>
	<div id="dialog2" style="display:none"></div>
</body>

</html>