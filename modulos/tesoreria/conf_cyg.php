<?php
$r = '../../';
//INCLUIR SESION Y CONECCION
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

$id1 = $_GET['id1'];
$fechafil1 = $_GET['fechafil1'];
$fechafil2 = $_GET['fechafil2'];

$filtro = 'fechafil1=' . $fechafil1 . '&fechafil2=' . $fechafil2;

$qry = $db->query("SELECT * FROM tesoreria WHERE tesoid='" . $id1 . "';");

?>
<!doctype html>
<html lang="es">

<head>
    <title>CONFIRMAR GASTOS PAGADOS</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>
	<script type="text/javascript">
		function tipopago(valor) {
			if (valor == 'Cheque') {
				document.getElementById("detalle").placeholder = '¿A quien se giro el cheque?';
				document.getElementById("detcheque").disabled = false;
				document.getElementById("divdetcheque").style.display = 'Initial';
				$("#banco").prop("required", true);
			} else if (valor == 'Consignacion') {
				document.getElementById("detalle").placeholder = 'Ingrese nombre de a quien se consigno';
				document.getElementById("detcheque").disabled = true;
				document.getElementById("divdetcheque").style.display = 'none';
				document.getElementById("detcheque").value = '';
				$("#banco").prop("required", false);
			} else if (valor == 'Giro') {
				document.getElementById("detalle").placeholder = 'Ingrese nombre de a quien se realizo la transferencia';
				document.getElementById("detcheque").disabled = true;
				document.getElementById("divdetcheque").style.display = 'none';
				document.getElementById("detcheque").value = '';
				$("#banco").prop("required", false);

			} else if (valor == 'Pago en Efectivo') {
				document.getElementById("detalle").placeholder = 'Ingrese nombre de a quien se le pago';
				document.getElementById("detcheque").disabled = true;
				document.getElementById("divdetcheque").style.display = 'none';
				document.getElementById("detcheque").value = '';
				$("#banco").prop("required", false);

			} else {
				document.getElementById("detcheque").disabled = true;
				document.getElementById("divdetcheque").style.display = 'none';
				document.getElementById("detcheque").value = '';
				document.getElementById("detalle").placeholder = 'Detalle del pago';
				$("#banco").prop("required", false);
			}
		}
		$(document).ready(function() {
			$('#fechaone').datepicker({ // VALIDACION DE LA FECHA UNO
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
				onClose: function(selectedDate) {
					$('#fechatwo').datepicker('option', 'minDate', selectedDate);
				}
			}).keypress(function(event) {
				event.preventDefault()
			});
			$('#fechatwo').datepicker({ // VALIDACION DE LA FECHA DOS
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
				onClose: function(selectedDate) {
					$('#fechaone').datepicker('option', 'maxDate', selectedDate);
				}
			}).keypress(function(event) {
				event.preventDefault()
			});

		});
	</script>

</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
		<?php require($r . 'incluir/src/menu.php') ?>
		<!-- INCLUIMOS MENU PRINCIPAL -->
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Tesoreria</a>
				<div class="mapa_div"></div><a href="#">Costos y Gastos</a>
				<div class="mapa_div"></div><a class="current">Confirmar Pago</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="c_pago_cyg.php" method="post">
					<!-- ENVIO FORMULARIO POR POST A LISTAR.PHP -->
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">Confirmar Pago</legend>
						<?php
						while ($rowprin = $qry->fetch(PDO::FETCH_ASSOC)) { //INFORMACION DE LA CONSULTA

						?>
							<p>
								<label>Identificador:</label>
								<input type="text" name="id1" value="<?php echo $rowprin['tesoid']; ?>" id="id1" class="text-input" readonly />
							</p>
							<p>
								<label> Fecha:</label>
								<input type="text" name="fechaone" value="<?php echo $rowprin['tesofecha']; ?>" id="fechaone" class="text-input fecha" readonly />
							</p>
							<p>
								<label title='Valor'>Por Valor: </label>
								<input type='text' name='valor' id='valor' value="<?php echo number_format($rowprin['tesovalor'], 2); ?>" class='valor validate[required] text-input' readonly />
							</p>
							<p>
								<label for="proveedor">Proveedor:</label>

								<?php
								$proveedor_teso = $rowprin['tesoproveedor'];
								if($proveedor_teso != "")
								{
									$qry = $db->query("SELECT * FROM proveedores_teso WHERE protesoid = '" . $rowprin['tesoproveedor'] . "' ORDER BY protesonombre");
									$row = $qry->fetch(PDO::FETCH_ASSOC);
									echo '<input type="hidden" name="proveedor" value="' . $row['protesoid'] . '">'.
									'<input type="text" value="' . $row['protesonombre'] . '" readonly>';
								}
								else{
									echo '<input type="hidden" name="proveedor" value="">'.
									'<input type="text" value="NO SE ASIGNO" readonly>';
								}
								?>
							</p>
							<p>
								<label for="empresa">Empresa:</label>
									
								<?php
									$qry = $db->query("SELECT * FROM empresas WHERE empid = '" . $rowprin['tesoempresa'] . "' ORDER BY empnombre");
									$row = $qry->fetch(PDO::FETCH_ASSOC);
									echo '<input type="hidden" name="empresa" value="' . $row['empid'] . '">'.
									'<input type="text" value="' . $row['empnombre'] . '" readonly>';
								?>
							</p>
							<p>
								<label> Fecha de pago:</label>
								<input type="text" name="fechatwo" id="fechatwo" value="<?php echo $rowprin['tesofechapagar']; ?>" class="validate[required] text-input fecha" readonly />
							</p>
							<p>
								<label for="forma_pa">Forma de Pago:</label>
								<select name="forma_pa" class="validate[required] text-input" onChange="tipopago(this.value);">
									<option value="">SELECCIONE</option>
									<option value="Cheque">CHEQUE</option>
									<option value="Consignacion">CONSIGNACIÓN</option>
									<option value="Giro">GIRO</option>
									<option value="Pago en Efectivo">PAGO EN EFECTIVO</option>
								</select>
								<label>Detalle del pago</label>
								<input type="text" placeholder="Detalle del pago" name="detalle" id="detalle" class="validate[required, maxSize[100]] text-input uppercase" />

								<div id="divdetcheque" style="display:none;">
									<label for="detcheque">Número de cheque</label>
									<input type="text" name="detcheque" id="detcheque" placeholder="Numero de cheque" class="validate[required] text-input uppercase" disabled />
								</div>
							</p>
							<p>
								<label for="banco">Banco:</label>
								<select id="banco" name="banco" class="text-input">
									<option value="">SELECCIONE</option>
									<?php
									$qry = $db->query("SELECT * FROM bancos  ORDER BY banconombre");
									while ($row = $qry->fetch(PDO::FETCH_ASSOC))
										echo '<option value=' . $row['bancoid'] . '>' . $row['banconombre'] . '</option>';
									?>
								</select>
							</p>
							<p>
								<label for="adicional">Adicional:</label>
								<textarea rows="5" class="form-control" name="adicional" class="validate[max[100]]" id="adicional"> </textarea>
							</p>
							<input type="hidden" name="fechafil1" value="<?php echo $fechafil1; ?>" />
							<input type="hidden" name="fechafil2" value="<?php echo $fechafil2; ?>" />

						<?php
						}
						?>
						<p class="boton">
							<button type="submit" class="btn btn-primary btnvalidar" name="confirmarpago" value="validar">Confirmar</button> <!-- BOTON VALIDAR -->
							<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href='c_pago_cyg.php?<?php echo $filtro ?>'">Atras</button>
						</p>
					</fieldset>
				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
		<!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
	</section>
	<?php
	if (isset($_GET['error'])) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['error'] . '</div>'; // MENSAJE MODAL ERROR
	?>
</body>

</html>