<?php
$r = '../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
include($r . 'incluir/fpdf/fpdf.php');
require($r.'incluir/letras.class.php');

$iden = $_GET['iden'];
$sql = "SELECT * FROM nomina WHERE nominaid = " . $iden . ";";
$rowprin = $db->query($sql)->fetch(PDO::FETCH_ASSOC);

$qryusuario = $db->query("SELECT * FROM nomina LEFT JOIN usuarios ON usuid = nominausuario INNER JOIN empresas ON empid = nominaempresa where nominaid = " . $iden . "; ");
$rowusuario = $qryusuario->fetch(PDO::FETCH_ASSOC);
$resultados = $qryusuario->rowCount();
if (!empty($rowusuario['usunombre'])) {
	$nombre = $rowusuario['usunombre'];
	$empresa = $rowusuario['empnombre'];
	$cedula = $rowusuario['usuid'];
} else {
	$nombre = $rowusuario['nominanombreusuario'];
	$empresa = $rowusuario['empnombre'];
	$cedula = $rowusuario['nominausuario'];
}

$qryempresa = $db->query("SELECT * FROM empresas where empid = '" . $rowprin['nominaempresa'] . "'; ");
$rowempresa = $qryempresa->fetch(PDO::FETCH_ASSOC);
$empresa = $rowempresa['empnombre'];

$hoy = date('Y/m/d');

if (empty($rowprin['nominaconsecutivo'])) {
	$sqlconsecutivo = "SELECT * FROM nomina WHERE nominaempresa = '" . $rowprin['nominaempresa'] . "' AND nominatipo = 'DOC EQUIVALENTE' AND nominaconsecutivo is not null order by nominaconsecutivo desc LIMIT 1;";
	$rowconsecutivo = $db->query($sqlconsecutivo)->fetch(PDO::FETCH_ASSOC);

	if (!empty($rowconsecutivo['nominaconsecutivo'])) {
		$consecutivo = $rowconsecutivo['nominaconsecutivo'] + 1;
	} else {
		$consecutivo = 1;
	}

	$qry = $db->query("UPDATE nomina SET nominaconsecutivo='$consecutivo' WHERE nominaid='$iden'; ");
} else {
	$consecutivo = $rowprin['nominaconsecutivo'];
}

$pdf = new FPDF('L','mm','Letter');
$pdf->AddFont('LucidaConsole', '', 'lucon.php');
$pdf->AliasNbPages();
$pdf->AddPage('L');
$pdf->SetFont('Arial', '', 10);
$ruta_logo = $r . "imagenes/logos/" . $rowempresa['empid'] . "/" . $rowempresa['logo'];
if(file_exists($ruta_logo) && $rowempresa['logo'] != "")
{
	$pdf->Image($ruta_logo, 5, 5, 80, 25);
}

/* if ($rowprin['nominaempresa'] == 900051528 or $rowprin['nominaempresa'] == 900813686 or $rowprin['nominaempresa'] == 900306685) {
	$pdf->Image($r . 'imagenes/logos/' . $rowprin['nominaempresa'] . '-doc.png', 5, 5, 80, 25);
} */
$pdf->Cell(260, 4, 'No.        ' . $consecutivo . '', 0, 1, 'R');
$pdf->Cell(260, 4, 'DOCUMENTO EQUIVALENTE A FACTURA', 0, 1, 'R');
$pdf->Cell(260, 4, '', 0, 1, 'R');

$pdf->Ln(10);
$pdf->Cell(200, 4, 'PERSONA NATURAL DE QUIEN SE ADQUIERE LOS BIENES Y/O SERVICIOS: ', 0, 0, 'L');
$pdf->Cell(60, 4, '' . $nombre . '', 0, 1, 'L');
$pdf->Cell(200, 4, 'RUC o CEDULA:  ', 0, 0, 'L');
$pdf->Cell(60, 4, '' . $cedula . '', 0, 1, 'L');
$pdf->Cell(200, 4, 'CUIDAD Y FECHA DE OPERACION:  ', 0, 0, 'L');
$pdf->Cell(60, 4, 'MANAGUA ' . $hoy . '', 0, 1, 'L');



//$salud = $rowprin['nominasueldo'] * 0.04;
//$pension = $rowprin['nominasueldo'] * 0.04;
$totalpagos = $rowprin['nominasueldo'] + $rowprin['nominamovilizacion'];
//$totaldescuentos = $salud + $pension;

$netopagar = $totalpagos; //- $totaldescuentos;

$pdf->SetFont('Arial', '', 8.5);
$pdf->Ln(5);
$pdf->Cell(110, 4, 'Descripcion de la operacion', 'L R T', 0, 'C');
$pdf->Cell(30, 4, 'Valor de la ', 'L R T', 0, 'C');
$pdf->Cell(30, 4, 'Tarifa de iva a la que', 'L R T', 0, 'C');
$pdf->Cell(30, 4, 'IVA teorico generado', 'L R T', 0, 'C');
$pdf->Cell(30, 4, 'Tarifa de retencion', 'L R T', 0, 'C');
$pdf->Cell(30, 4, 'Valor del impuesto', 'L R T', 1, 'C');

$pdf->Cell(110, 4, '', 'L R B', 0, 'C');
$pdf->Cell(30, 4, 'misma', 'L R B', 0, 'C');
$pdf->Cell(30, 4, 'se halla grabado', 'L R B', 0, 'C');
$pdf->Cell(30, 4, 'en la operacion', 'L R B', 0, 'C');
$pdf->Cell(30, 4, 'de iva vigente', 'L R B', 0, 'C');
$pdf->Cell(30, 4, 'asumido', 'L R B', 1, 'C');

$pdf->SetFont('Arial', '', 8.5);

$qrypri = $db->query("SELECT * FROM infodocequi where infonomina = '$iden'; ");
$cont = 1;
$infovalor = 0;
$infoivateorico = 0;
$infovalorimpuesto = 0;
while ($rowpri = $qrypri->fetch(PDO::FETCH_ASSOC)) {

	$pdf->Cell(110, 4, '' . $rowpri['infodescripcion'] . '', 1, 0, 'L');
	$pdf->Cell(30, 4, '' . number_format($rowpri['infovalor'], 2) . '', 1, 0, 'C');
	$pdf->Cell(30, 4, '' . $rowpri['infotarifaiva'] . ' %', 1, 0, 'C');
	$pdf->Cell(30, 4, '' . number_format($rowpri['infoivateorico'], 2) . '', 1, 0, 'C');
	$pdf->Cell(30, 4, '' . $rowpri['infotarifaretencion'] . ' %', 1, 0, 'C');
	$pdf->Cell(30, 4, '' . number_format($rowpri['infovalorimpuesto'], 2) . '', 1, 1, 'C');

	$infovalor = $rowpri['infovalor'] + $infovalor;
	$infoivateorico = $rowpri['infoivateorico'] + $infoivateorico;
	$infovalorimpuesto = $rowpri['infovalorimpuesto'] + $infovalorimpuesto;

	$cont = $cont + 1;
}

for ($i = $cont; $i <= 12; $i++) {
	$pdf->Cell(110, 4, '', 1, 0, 'L');
	$pdf->Cell(30, 4, '', 1, 0, 'C');
	$pdf->Cell(30, 4, '', 1, 0, 'C');
	$pdf->Cell(30, 4, '', 1, 0, 'C');
	$pdf->Cell(30, 4, '', 1, 0, 'C');
	$pdf->Cell(30, 4, '', 1, 1, 'C');
}

$pdf->Cell(110, 4, 'Totales C$', 1, 0, 'R');
$pdf->Cell(30, 4, '' . number_format($infovalor, 2) . '', 1, 0, 'C');
$pdf->Cell(30, 4, '', 1, 0, 'C');
$pdf->Cell(30, 4, '' . number_format($infoivateorico, 2) . '', 1, 0, 'C');
$pdf->Cell(30, 4, '', 1, 0, 'C');
$pdf->Cell(30, 4, '' . number_format($infovalorimpuesto, 2) . '', 1, 1, 'C');

$netopagar = $infovalor - $infoivateorico - $infovalorimpuesto;
$V = new EnLetras();
$totalletrasnetopagar = strtoupper($V->ValorEnLetras($netopagar,"cordobas"));
$pdf->Cell(260,7,'NETO A PAGAR = '.$totalletrasnetopagar.' C$ '.number_format($netopagar,2).'','T L B R',1,'C');	


$qry = $db->query("UPDATE nomina SET nominainfovalor='$infovalor', nominainfoivateorico='$infoivateorico', nominainfovalorimpuesto='$infovalorimpuesto' WHERE nominaid='$iden'; ");

$pdf->Ln(5);

/* $pdf->Cell(200, 4, 'Certifico que he efectuado el pago a la seguridad social por los ingresos materia de facturacion igualmente manifiesto que estos aportes sirvieron ', 'T L R', 0, 'L');
$pdf->Cell(5, 4, '', 'L R', 0, 'C'); */
/* 
$nombre_fichero = $r . 'imagenes/firmas/' . $cedula . '.png';

if (file_exists($nombre_fichero)) {
	$pdf->Image($r . 'imagenes/firmas/' . $cedula . '.png', 230, 169, 35, 15);
} */

$pdf->Cell(70, 4, 'FIRMA DEL BENEFICIARIO', 'T L R', 1, 'C');
//$pdf->Cell(200, 4, 'para la disminucion de la base de retencion en la fuente en otro cobro         SI                NO     ', 'L R', 0, 'L');
//$pdf->Cell(5, 4, '', 'L R', 0, 'C');
$pdf->Cell(70, 4, '', 'L R', 1, 'C');
//$pdf->Cell(200, 4, '', 'B L R', 0, 'L');
//$pdf->Cell(5, 4, '', 'L R', 0, 'C');
$pdf->Cell(70, 4, '', ' L R', 1, 'C');
//$pdf->Cell(200, 4, '               Numero de pago de aportes', 'T L R', 0, 'L');
//$pdf->Cell(5, 4, '', 'L R', 0, 'C');
$pdf->Cell(70, 4, '____________________________________', ' L R', 1, 'C');
//$pdf->Cell(200, 4, '                       (Adjunto Copia)', 'B L R', 0, 'L');
//$pdf->Cell(5, 4, '', 'L R', 0, 'C');
$pdf->Cell(70, 4, 'C.C.', 'B L R', 1, 'L');
$pdf->Cell(70, 4, date('Y/m/d H:i:s'), 0, 1, 'R');

$pdf->Output($r . 'pdf/nominas/' . $iden . '_DOC_EQUIVALENTE.pdf');
$pdf->Output();
