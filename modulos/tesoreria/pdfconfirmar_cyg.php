<?php
$r = '../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
require($r.'incluir/funciones.php');
require($r.'incluir/fpdf/fpdf.php');

$fecha1 = $_GET['fechafil1'];
$fecha2 = $_GET['fechafil2'];	

$sql = "SELECT tesoreria.*, empresas.empnombre, proveedores_teso.protesonombre FROM tesoreria
LEFT JOIN empresas on empid = tesoempresa
LEFT JOIN proveedores_teso on tesoproveedor = protesoid
WHERE tesovalid = '1'  AND tesofechapagar BETWEEN '$fecha1' AND '$fecha2' ORDER BY tesofechapagar DESC";


class PDF extends FPDF{
	function Header(){
		global $fecha;
		global $fecha1;
		global $fecha2;
		global $adicional;
		global $nombre_adicional;
    	$this->SetFont('LucidaConsole','',6);
		$this->Cell(50,5,date('Y/m/d H:i:s'),0,1);
		$this->SetFont('LucidaConsole','',8);
		$this->Cell(280,5,'--------------------------------------------------------------------------------------------------------------------------------------------------------------------',0,1,'C');
    	$this->Cell(280,5,'LISTADO DE PAGOS POR CONFIRMAR',0,1,'C');
		$this->Cell(280,5,'--------------------------------------------------------------------------------------------------------------------------------------------------------------------',0,1,'C');
    	$this->Ln(5);
	}

	function Footer(){
    	$this->SetY(-15);
    	$this->SetFont('LucidaConsole','',8);
		$this->Cell(0,5,'----------------------------------------------------------------------------------------------------------------------------------------------------------------------',0,1,'C');
		$this->SetFont('LucidaConsole','',8);
    	$this->Cell(0,5,'Pagina '.$this->PageNo().'/{nb}',0,0,'R');
	}
}

$pdf = new PDF();
$pdf->AddFont('LucidaConsole','','lucon.php');
$pdf->AliasNbPages();
$pdf->AddPage('L');

$pdf->SetFont('LucidaConsole','',8);
$pdf->SetFillColor(220,220,220);
// Suma total 
$pdf->Cell(15,5,'ID',1,0,'C',true);
$pdf->Cell(25,5,'#FACTURA',1,0,'C',true);
$pdf->Cell(25,5,'F. TESO',1,0,'C',true);
$pdf->Cell(25,5,'F. PAGO',1,0,'C',true);
$pdf->Cell(50,5,'CONCEPTO',1,0,'C',true);
$pdf->Cell(25,5,'VALOR',1,0,'C',true);
$pdf->Cell(15,5,'ABONO%',1,0,'C',true);
$pdf->Cell(25,5,'ABONO',1,0,'C',true);

$pdf->Cell(60,5,'PROVEEDOR',1,1,'C',true);

$i = 1;
$comision = 0;
$ttlbase = 0;
$ttlcomision = 0;
$qry = $db->query($sql);
$vlr1 = 0;
$vlrab = 0;
while($row = $qry->fetch(PDO::FETCH_ASSOC)){
	
	$empresa = $row['empnombre'];

	$proveedor_name = $row['protesonombre'];

	if ($row['tesovalid'] == 1) { $tesoestado = "PAGADO"; } elseif ($row['tesovalid'] == 0) { $tesoestado = "SIN PAGAR"; } elseif ($row['tesovalid'] == 2) { $tesoestado = "CONFIRMADO"; }

	if ($row['tesoabono'] == 'SI'){$abono = round($row['tesovalorfinal'],2);} else {$abono = 'NO';}

	$pdf->SetFont('LucidaConsole','',7);
	if($i%2 == 0) $x = 255;
	else $x = 235;
	$pdf->SetFillColor($x,$x,$x);
	$pdf->Cell(15,5,$row['tesoid'],1,0,'C',true);
	$pdf->Cell(25,5,$row['tesonumfactura'],1,0,'C',true);
	$pdf->Cell(25,5,$row['tesofecha'],1,0,'C',true);
	$pdf->Cell(25,5,$row['tesofechapagar'],1,0,'C',true);
	$pdf->Cell(50,5,substr($row['tesoconcepto'],0,28),1,0,'C',true);
	$pdf->Cell(25,5,round($row['tesovalor'],2),1,0,'C',true);
	$pdf->Cell(15,5,substr($row['tesoporcentaje'], 0, 45),1,0,'C',true);
	$pdf->Cell(25,5,$abono,1,0,'C',true);
	
	$pdf->Cell(60,5,substr($proveedor_name,0,35),1,1,'C',true);
	$i++;
	
	$vlr1 = $vlr1 + $row['tesovalor'];
	$vlrab = $vlrab + $row['tesovalorfinal'];
}


$pdf->SetFillColor(255,255,255);
$pdf->Cell(140,5,'',0,0,'',true);
$pdf->SetFillColor(220,220,220);
$pdf->Cell(25,5,round($vlr1,2),1,0,'C',true);
$pdf->Cell(15,5,'',0,0,'',true);
$pdf->Cell(25,5,round($vlrab,2),1,1,'C',true);

$pdf->Output('Listado Confirmacion Pagos.pdf','d');
?>