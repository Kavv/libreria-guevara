<?php
// LA ACCION PRINCIPAL ES ESTA PAGINA ES EL MANEJO DE LLAMADAS PARA EL CALL CENTER
$r = '../../';
//INCLUIR SESION Y CONECCION
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');


if (isset($_POST['validar'])) {

	$usuario = $_POST['usuario'];

	$rowusu = $db->query("SELECT * FROM usuarios WHERE usuid = '" . $usuario . "';")->fetch(PDO::FETCH_ASSOC);

	// $codigo = $rowusu['usucodigohelisa'];
	$empresa = $rowusu['usuempresa'];
	$hoy = date('Y/m/d');
	$fecha1 = $_POST['fecha1'];
	$fecha2 = $_POST['fecha2'];
	$salario = $_POST['salario'];
	if ($_POST['auxilio'] != "") {
		$auxilio = $_POST['auxilio'];
	} else {
		$auxilio = 0;
	}
	$otrosdecuentos = $_POST['otrosdecuentos'];
	$descripcionotrosdecuentos = trim(($_POST['descripcionotrosdecuentos']));
	$descuento1 = $_POST['descuento1'];
	$descripdescuento1 = trim(($_POST['descripdescuento1']));
	$descuento2 = $_POST['descuento2'];
	$descripdescuento2 = trim(($_POST['descripdescuento2']));
	$descuento3 = $_POST['descuento3'];
	$descripdescuento3 = trim(($_POST['descripdescuento3']));
	$numdias = $_POST['numdias'];
	$gestionado = $_SESSION['id'];



	if (empty($empresa)) {
		$error = "Para Continuar es necesario que le asigne una empresa al usuario al que desea generar la nomina.";
		header('Location:insernomina.php?error=' . $error . '');
	} elseif (empty($salario)) {
		$error = "Para Continuar es necesario que le asigne un salario al usuario al que desea generar la nomina.";
		header('Location:insernomina.php?error=' . $error . '');
	} /*elseif (empty($auxilio)){
		$error = "Para Continuar es necesario que le asigne un Aux de transporte al usuario al que desea generar la nomina.";
		header('Location:insernomina.php?error='.$error.'');
	} Esto se removio ya que no especificaran lo salarios elseif ($rowusu['ususalariocotizacion'] < $salario) {
		$error = "No es posible continuar con el proceso por que el valor de nomina que esta ingresando es mayor al salario asignado a este usuario que es de " . number_format($rowusu['ususalariocotizacion'], 2) . ".";
		header('Location:insernomina.php?error=' . $error . '');
	} elseif ($rowusu['usuauxiliotransporte'] < $auxilio) {
		$error = "No es posible continuar con el proceso por que el valor de auxilio de movilizacion que esta ingresando es mayor al asignado a este usuario que es de " . number_format($rowusu['usuauxiliotransporte'], 2) . ".";
		header('Location:insernomina.php?error=' . $error . '');
	}*/ else {

		if (empty($otrosdecuentos)) {
			$otrosdecuentos = 0;
			$descripcionotrosdecuentos = 'NONE';
		}
		if (empty($descuento1)) {
			$descuento1 = 0;
			$descripdescuento1 = 'NONE';
		}
		if (empty($descuento2)) {
			$descuento2 = 0;
			$descripdescuento2 = 'NONE';
		}
		if (empty($descuento3)) {
			$descuento3 = 0;
			$descripdescuento3 = 'NONE';
		}

		$qry = $db->query("INSERT INTO nomina (nominatipo, nominausuario, nominaempresa, nominafecha, nominaperiodo1, nominaperiodo2, nominasueldo, nominamovilizacion, nominaotrosdescuentos, nominadescripotrosdescuentos, nominadescuento1, nominadescripdescuento1, nominadescuento2, nominadescripdescuento2, nominadescuento3, nominadescripdescuento3, nominadias, nominagestionado) 
		                               VALUES ('NOMINA', '$usuario', '$empresa', '$hoy', '$fecha1', '$fecha2', '$salario', '$auxilio', '$otrosdecuentos', '$descripcionotrosdecuentos', '$descuento1', '$descripdescuento1', '$descuento2', '$descripdescuento2', '$descuento3', '$descripdescuento3', '$numdias', '$gestionado'); ");

		if ($qry) {
			$iden = $db->lastInsertId();
			header('Location:finalizarnomina.php?iden=' . $iden . '');
		} else {
			$error = "No se pudo ingresar por favor intente nuevamente o contacte con el encargado del portal. ";
			header('Location:insernomina.php?error=' . $error . '');
		}
	}
}
?>
<!doctype html>
<html lang="es">

<head>
    <title>AGREGAR NOMINA</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

	<script type="text/javascript">
		$(document).ready(function() {


			$('#fecha1').datepicker({ // VALIDACION DE LA FECHA UNO
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
				onClose: function(selectedDate) {
					$('#fecha2').datepicker('option', 'minDate', selectedDate);
				}
			})
			$('#fecha2').datepicker({ // VALIDACION DE LA FECHA DOS
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
				onClose: function(selectedDate) {
					$('#fecha1').datepicker('option', 'maxDate', selectedDate);
				}
			}).keypress(function(event) {
				event.preventDefault()
			});


			$('#departamento').change(function(event) {
				var id1 = $('#departamento').find(':selected').val();
				$('#ciudad').load('<?php echo $r ?>incluir/carga/ciudades.php?id1=' + id1);
			});

			$("#usuario").change(function(){
				user_data($(this).val());
			});

			function user_data(user)
			{
				limpiar();
				$.get("userdata.php?user="+user, function(res){
					var usuario = JSON.parse(res);
					if(usuario != false)
					{
						$("#salario").val(usuario.ususalariocotizacion);
						$("#auxilio").val(usuario.usuauxiliotransporte);
						$("#salario-max").text(usuario.ususalariocotizacion);
						$("#auxilio-max").text(usuario.usuauxiliotransporte);
						if(usuario.empnombre != "")
							$("#emp-asignada").text(usuario.empnombre);
						else
							$("#emp-asignada").text("NINGUNA");
					}
					else
					{
						alert("El usuario no tiene asignada una empresa, verifique los datos del usuario (El proceso no se llevara a cabo)");
					}
				});
			}
			function limpiar()
			{
				$("#salario").val("");
				$("#auxilio").val("");
				$("#salario-max").text("");
				$("#auxilio-max").text("");
				$("#emp-asignada").text("");
			}
			
			var $start = $("#datepicker");
			var $end = $("#datepicker2");
			var temp_start, temp_end;
			$('#fecha1').on( 'change', function (){
				dias();
			});

			$('#fecha2').on( 'change', function (){
				dias();
			});

			function dias()
			{
				var fechaInicio = new Date($("#fecha1").val()).getTime();
				var fechaFin    = new Date($("#fecha2").val()).getTime();

				diff = fechaFin - fechaInicio;
				diff = diff/(1000*60*60*24);
				console.log(diff);
				if(diff >= 0)
					$("#dias").val(diff+1);
			}

			$("#usuario").selectpicker();

		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
		<?php require($r . 'incluir/src/menu.php') ?>
		<!-- INCLUIMOS MENU PRINCIPAL -->
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Tesoreria</a>
				<div class="mapa_div"></div><a class="current">Insertar Nomina</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="insernomina.php" method="post">
					<!-- ENVIO FORMULARIO POR POST -->
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">Insertar Nomina </legend>
						</br>
						<p>
							<label for="usuario">Usuario:</label>
							<select id="usuario" name="usuario" class="selectpicker" required
							data-live-search="true" title="Seleccione un usuario" data-width="100%">
								<option value="">SELECCIONE</option>
								<?php
								// 56 =  Inactivo
								$qry = $db->query("SELECT * FROM usuarios ORDER BY usunombre ");
								while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
									echo '<option value="' . $row2["usuid"] . '">' . $row2["usuid"] .' / '.  $row2['usunombre'] . '</option>';
								}
								?>
							</select>
							<em style="font-size:14px">Empresa asignada:<span id="emp-asignada" style="color: #f00000; font-weight: bold;"></span></em>
						</p>
						<p>
							<label for="fecha1">Periodo de liquidacion: </label>
							<input type="text" name="fecha1" id="fecha1" class="validate[required] text-input fecha" /> 
							<label> - </label> 
							<input type="text" name="fecha2" id="fecha2" class="validate[required] text-input fecha" />
						</p>
						<p>
							<label for="numdias">Numero de días:</label>
							<input id="dias" type="number" name="numdias" class='validate[required]' min="1" max="31">
						</p>
						<p>
							<label for="salario">Por Nomina:</label>
							<input type='text' name='salario' id='salario' class='valor validate[required, custom[onlyNumberSp]] text-input' />
							<em style="font-size:14px">Valor maximo:<span id="salario-max" style="color: #f00000; font-weight: bold;"></span></em>
						</p>
						<p>
							<label for="auxilio">Auxilio de Transporte:</label>
							<input type='text' name='auxilio' id='auxilio' class='valor validate[custom[onlyNumberSp]] text-input' />
							<em style="font-size:14px">Valor maximo:<span id="auxilio-max" style="color: #f00000; font-weight: bold;"></span></em>
						</p>
						<p>
							<label> Conceptop del descuento 1 </label> 
							<input type='text' name='descripdescuento1' id='descripdescuento1' class='text-input' placeholder='Descuento 1...' />
							<label for="descuento1">Valor Descuento 1:</label>
							<input type='text' name='descuento1' id='descuento1' class='valor validate[custom[onlyNumberSp]] text-input' /> 
						</p>

						<p>
							<label> Concepto del descuento 2 </label> 
							<input type='text' name='descripdescuento2' id='descripdescuento2' class='text-input' placeholder='Descuento 2...' />
							<label for="descuento2">Valor Descuento 2:</label>
							<input type='text' name='descuento2' id='descuento2' class='valor validate[custom[onlyNumberSp]] text-input' /> 
						</p>

						<p>
							<label> Concepto del descuento 3 </label> 
							<input type='text' name='descripdescuento3' id='descripdescuento3' class='text-input' placeholder='Descuento 3...' />
							<label for="descuento3">Valor Descuento 3:</label>
							<input type='text' name='descuento3' id='descuento3' class='valor validate[custom[onlyNumberSp]] text-input' /> 
						</p>
						<p>
							<label> Concepto del descuento 4 </label> 
							<input type='text' name='descripcionotrosdecuentos' id='descripcionotrosdecuentos' class=' text-input' placeholder='Descripcion...' />
							<label for="otrosdecuentos">Valor Descuentos 4:</label>
							<input type='text' name='otrosdecuentos' id='otrosdecuentos' class='valor validate[custom[onlyNumberSp]] text-input' /> 
						</p>
						</br>
						<p class="boton">
							<button type="submit" class="btn btn-primary btnvalidar" name="validar" value="validar">Insertar</button> <!-- BOTON VALIDAR -->
						</p>
					</fieldset>
				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
		<!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
	</section>
	<?php
	if (isset($_GET['error'])) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['error'] . '</div>'; // MENSAJE MODAL ERROR
	if (isset($_GET['mensaje'])) echo '<div id="dialog-message" title="Exitoso"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['mensaje'] . '</div>'; // MENSAJE MODAL EXITOSO
	?>
	<div id="dialog2" title="Historico de Llamadas" style="display:none"></div>
</body>

</html>