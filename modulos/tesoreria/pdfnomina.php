<?php
$r = '../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
include($r . 'incluir/fpdf/fpdf.php');

$iden = $_GET['iden'];
$sql = "SELECT nomina.*, e.empnombre, e.empid, u.usunombre, 
u.usucargo, u.usufechaingreso, u.ususalariocotizacion FROM nomina 
LEFT JOIN empresas e ON e.empid = nominaempresa
LEFT JOIN usuarios u ON u.usuid = nominausuario
WHERE nominaid = ".$iden.";"; 
$rowprin = $db->query($sql)->fetch(PDO::FETCH_ASSOC);

$usuario = $rowprin['usunombre'];

$empresa = $rowprin['empnombre'];



$pdf = new FPDF();
$pdf->AddFont('LucidaConsole', '', 'lucon.php');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Arial', '', 8);
$pdf->Cell(0, 5, date('d/m/Y H:i:s'), 0, 1, 'R');
$pdf->SetFont('Arial', '', 9);
$pdf->Cell(93, 5, '' . $empresa . '', 'T L ', 0, 'L');
$pdf->Cell(40, 5, 'FECHA: ' . $rowprin['nominafecha'] . '', 'T  ', 0, 'L');

$newformat1 = date("d/m/Y", strtotime($rowprin['nominaperiodo1']));
$newformat2 = date("d/m/Y", strtotime($rowprin['nominaperiodo2']));
$pdf->Cell(57, 5, 'PERIODO: ' . $newformat1 . ' AL ' . $newformat2 . '', 'T R', 1, 'L');
$pdf->Cell(150, 5, 'NOMINA:   NOMINA DE VENTAS ', 'LB ', 0, 'L');
$pdf->Cell(40, 5, 'NIT: ' . $rowprin['nominaempresa'], 'RB ', 1, 'L');

$pdf->Cell(83, 5, 'FECHA DE INGRESO: ' . $rowprin['usufechaingreso'], 'T L ', 0, 'L');
$pdf->Cell(57, 5, 'SUELDO BASE: ' . number_format($rowprin['ususalariocotizacion'], 2), 'T  ', 0, 'L');
$pdf->Cell(50, 5, '', 'T R', 1, 'L');
$pdf->Cell(83, 5, 'IDENTIFICACION: ' . $rowprin['nominausuario'] . ' ', 'L ', 0, 'L');
$pdf->Cell(107, 5, 'NOMBRE: ' . $usuario . '', 'R ', 1, 'L');
$pdf->Cell(190, 5, 'CARGO: ' . $rowprin['usucargo'] . ' ', 'L R B', 1, 'L');


$salud = 0; //$rowprin['nominasueldo'] * 0.07;
$pension = 0; //$rowprin['nominasueldo'] * 0.04;
$totalpagos = $rowprin['nominasueldo'] + $rowprin['nominamovilizacion'];
$totaldescuentos = $salud + $pension + $rowprin['nominaotrosdescuentos'] + $rowprin['nominadescuento1'] + $rowprin['nominadescuento2'] + $rowprin['nominadescuento3'];

// Se desconoce el porque dividir (regla del ex negocio tal vez?)
$diasmitad = $rowprin['nominadias']; // / 2;
$netopagar = $totalpagos - $totaldescuentos;

$pdf->SetFont('Arial', 'B', 9);
$pdf->Cell(95, 5, 'PAGOS', 'T L ', 0, 'C');
$pdf->Cell(95, 5, 'DESCUENTOS', 'T R', 1, 'C');
$pdf->SetFont('Arial', '', 9);
$pdf->Cell(20, 5, 'DIAS ', 'LB ', 0, 'L');
$pdf->Cell(50, 5, 'DESCRIPCION', 'B ', 0, 'L');
$pdf->Cell(25, 5, 'VALOR', 'B ', 0, 'R');
$pdf->Cell(70, 5, 'DESCRIPCION', 'B ', 0, 'L');
$pdf->Cell(25, 5, 'VALOR', 'RB ', 1, 'L');

$pdf->Cell(20, 5, '' . $diasmitad . '', 'L ', 0, 'L');
$pdf->Cell(50, 5, 'SUELDO', ' ', 0, 'L');
$pdf->Cell(25, 5, '' . number_format($rowprin['nominasueldo'], 2) . '', 'R ', 0, 'R');


if ($rowprin['nominadescuento1'] != 0) {
    /* $pdf->Cell(20,5,'','L ',0,'L');
    $pdf->Cell(50,5,'',' ',0,'L');
    $pdf->Cell(25,5,'','R ',0,'R'); */
    $pdf->Cell(70, 5, '' . $rowprin['nominadescripdescuento1'] . '', 'L ', 0, 'L');
    $pdf->Cell(25, 5, '' . number_format($rowprin['nominadescuento1'], 2) . '', 'R ', 1, 'L');
} else {
    $pdf->Cell(70, 5, '', 'L ', 0, 'L');
    $pdf->Cell(25, 5, '', 'R ', 1, 'L');
}

$pdf->Cell(20, 5, '' . $diasmitad . '', 'L ', 0, 'L');
$pdf->Cell(50, 5, 'AUXILIO DE TRANSPORTE', ' ', 0, 'L');
$pdf->Cell(25, 5, '' . number_format($rowprin['nominamovilizacion'], 2) . '', 'R ', 0, 'R');
/* $pdf->Cell(70,5,'APORTE PENSION','L ',0,'L');
$pdf->Cell(25,5,''.number_format($pension,2).'','R ',1,'L'); */

$rellenar = false;

if ($rowprin['nominadescuento2'] != 0) {
    $pdf->Cell(70, 5, '' . $rowprin['nominadescripdescuento2'] . '', 'L ', 0, 'L');
    $pdf->Cell(25, 5, '' . number_format($rowprin['nominadescuento2'], 2) . '', 'R ', 1, 'L');
}

if ($rowprin['nominadescuento3'] != 0) {
    $pdf->Cell(20, 5, '', 'L ', 0, 'L');
    $pdf->Cell(50, 5, '', ' ', 0, 'L');
    $pdf->Cell(25, 5, '', 'R ', 0, 'R');
    $pdf->Cell(70, 5, '' . $rowprin['nominadescripdescuento3'] . '', 'L ', 0, 'L');
    $pdf->Cell(25, 5, '' . number_format($rowprin['nominadescuento3'], 2) . '', 'R ', 1, 'L');
}

if ($rowprin['nominaotrosdescuentos'] != 0) {
    $pdf->Cell(20, 5, '', 'L ', 0, 'L');
    $pdf->Cell(50, 5, '', ' ', 0, 'L');
    $pdf->Cell(25, 5, '', 'R ', 0, 'R');
    $pdf->Cell(70, 5, '' . $rowprin['nominadescripotrosdescuentos'] . '', 'L ', 0, 'L');
    $pdf->Cell(25, 5, '' . number_format($rowprin['nominaotrosdescuentos'], 2) . '', 'R ', 1, 'L');
}

$pdf->Cell(20, 5, '', 'L ', 0, 'L');
$pdf->Cell(50, 5, '', ' ', 0, 'L');
$pdf->Cell(25, 5, '', 'R ', 0, 'R');
$pdf->Cell(70, 5, '', 'L ', 0, 'L');
$pdf->Cell(25, 5, '', 'R ', 1, 'L');

$pdf->Cell(20, 5, '', 'L ', 0, 'L');
$pdf->Cell(50, 5, '', ' ', 0, 'L');
$pdf->Cell(25, 5, '', 'R ', 0, 'R');
$pdf->Cell(70, 5, '', 'L ', 0, 'L');
$pdf->Cell(25, 5, '', 'R ', 1, 'L');

$pdf->Cell(20, 5, '', 'L ', 0, 'L');
$pdf->Cell(50, 5, '', ' ', 0, 'L');
$pdf->Cell(25, 5, '', 'R ', 0, 'R');
$pdf->Cell(70, 5, '', 'L ', 0, 'L');
$pdf->Cell(25, 5, '', 'R ', 1, 'L');

$pdf->Cell(20, 5, '', 'L ', 0, 'L');
$pdf->Cell(50, 5, '', ' ', 0, 'L');
$pdf->Cell(25, 5, '', 'R ', 0, 'R');
$pdf->Cell(70, 5, '', 'L ', 0, 'L');
$pdf->Cell(25, 5, '', 'R ', 1, 'L');

$pdf->Cell(20, 5, '', 'L ', 0, 'L');
$pdf->Cell(50, 5, '', ' ', 0, 'L');
$pdf->Cell(25, 5, '', 'R ', 0, 'R');
$pdf->Cell(70, 5, '', 'L ', 0, 'L');
$pdf->Cell(25, 5, '', 'R ', 1, 'L');

$pdf->Cell(20, 5, '', 'L ', 0, 'L');
$pdf->Cell(50, 5, '', ' ', 0, 'L');
$pdf->Cell(25, 5, '', 'R ', 0, 'R');
$pdf->Cell(70, 5, '', 'L ', 0, 'L');
$pdf->Cell(25, 5, '', 'R ', 1, 'L');

$pdf->Cell(20, 5, '', 'L ', 0, 'L');
$pdf->Cell(50, 5, '', ' ', 0, 'L');
$pdf->Cell(25, 5, '', 'R ', 0, 'R');
$pdf->Cell(70, 5, '', 'L ', 0, 'L');
$pdf->Cell(25, 5, '', 'R ', 1, 'L');

$pdf->Cell(20, 5, '', 'L ', 0, 'L');
$pdf->Cell(50, 5, '', ' ', 0, 'L');
$pdf->Cell(25, 5, '', 'R ', 0, 'R');
$pdf->Cell(70, 5, '', 'L ', 0, 'L');
$pdf->Cell(25, 5, '', 'R ', 1, 'L');
$pdf->Cell(20, 5, '', 'L ', 0, 'L');
$pdf->Cell(50, 5, '', ' ', 0, 'L');
$pdf->Cell(25, 5, '', 'R ', 0, 'R');
$pdf->Cell(70, 5, '', 'L ', 0, 'L');
$pdf->Cell(25, 5, '', 'R ', 1, 'L');

$pdf->Cell(20, 5, '', 'LB ', 0, 'L');
$pdf->Cell(50, 5, '              TOTAL PAGOS', 'B ', 0, 'L');
$pdf->Cell(25, 5, '' . number_format($totalpagos, 2) . '', 'RB ', 0, 'R');
$pdf->Cell(70, 5, '              TOTAL DESCUENTOS', 'LB ', 0, 'L');
$pdf->Cell(25, 5, '' . number_format($totaldescuentos, 2) . '', 'RB ', 1, 'L');



$pdf->SetFillColor(250, 250, 250);
$pdf->SetFont('Arial', '', 13);
$pdf->Cell(95, 14, '           NETO A PAGAR:	' . number_format($netopagar, 2) . '', 1, 0, '', true);
$pdf->SetFont('Arial', '', 10);
$pdf->Cell(95, 14, 'Firma: _________________________________________', 1, 1, 'C', true);
$pdf->SetFont('Arial', '', 8);
$pdf->Cell(190, 4, 'Copias: Gestion Humana y Contabilidad', 1, 1, 'R', true);

$pdf->Output($r . 'pdf/nominas/' . $iden . '_NOMINA.pdf');
$pdf->Output();
