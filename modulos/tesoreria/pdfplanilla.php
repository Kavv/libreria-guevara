<?php

/* REPORTE ACTUALMENTE CALIFICADO COMO ERRONIO */
/* Especifica los movimientos por cada empresa especificando "ESTATICAMENTE" los id de las empresas */
/* En base al orden que se ha seguido no se conoce aun el movimiento con prefijo RC */
/* Fecha 03/11/2020 */

$r = '../../'; 
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
include($r.'incluir/fpdf/fpdf.php');

$fecha1 = $_POST['fecha1'];



// IDENCORP
$totaliden = 0;
$descuento = 0;

$sqliden = "SELECT sum(movvalor) as Total, sum(movdescuento) as Descuento FROM movimientos WHERE movprefijo = 'RC' AND movfecha = '$fecha1' AND movempresa = '900051528' ;"; 
$rowiden = $db->query($sqliden)->fetch(PDO::FETCH_ASSOC);
	$totaliden = $rowiden['Total'];
	$descuentoiden = $rowiden['Descuento'];

$totalventasiden = 0;
$totalcuotasiniiden = 0;
$totalfinaliden = 0;

$sqlventasiden = "SELECT sum(movvalor) as Total FROM solicitudes INNER JOIN movimientos ON solfactura = movdocumento  WHERE movempresa = '900051528' AND movfecha = '$fecha1' AND movprefijo = 'FV' AND  solncuota IN ('1','0') AND movestado <> 'ANULADA';"; 
$sqlcuotasiniiden =  "SELECT sum(solcuota) as Cuotas FROM solicitudes INNER JOIN movimientos ON solfactura = movdocumento  WHERE movempresa = '900051528' AND movfecha = '$fecha1' AND movprefijo = 'FV' AND  solcuota > 0 AND movestado = 'FACTURADO';"; // qry cuotas ini
$rowventasiden = $db->query($sqlventasiden)->fetch(PDO::FETCH_ASSOC);
$rowcuotasiniiden = $db->query($sqlcuotasiniiden)->fetch(PDO::FETCH_ASSOC);
	$totalventasiden = $rowventasiden['Total'];
	$totalcuotasiniiden = $rowcuotasiniiden['Cuotas'];
	$totalfinaliden = $totalventasiden + $totalcuotasiniiden;

//ESCUELA	
$totalescu = 0;
$descuentoescu = 0;

$sqlescu = "SELECT sum(movvalor) as Total, sum(movdescuento) as Descuento FROM movimientos WHERE movprefijo = 'RC' AND movfecha = '$fecha1' AND movempresa = '900487926' ;"; 
$rowescu = $db->query($sqlescu)->fetch(PDO::FETCH_ASSOC);
	$totalescu = $rowescu['Total'];
	$descuentoescu = $rowescu['Descuento'];
	
$totalventasescu = 0;
$totalcuotasiniescu = 0;
$totalfinalescu = 0;

$sqlventasescu = "SELECT sum(movvalor) as Total FROM solicitudes INNER JOIN movimientos ON solfactura = movdocumento  WHERE movempresa = '900487926' AND movfecha = '$fecha1' AND movprefijo = 'FV' AND  solncuota IN ('1','0') AND movestado <> 'ANULADA';"; 
$sqlcuotasiniescu =  "SELECT sum(solcuota) as Cuotas FROM solicitudes INNER JOIN movimientos ON solfactura = movdocumento  WHERE movempresa = '900487926' AND movfecha = '$fecha1' AND movprefijo = 'FV' AND  solcuota > 0 AND movestado = 'FACTURADO';"; // qry cuotas ini
$rowventasescu = $db->query($sqlventasescu)->fetch(PDO::FETCH_ASSOC);
$rowcuotasiniescu = $db->query($sqlcuotasiniescu)->fetch(PDO::FETCH_ASSOC);
	$totalventasescu = $rowventasescu['Total'];
	$totalcuotasiniescu = $rowcuotasiniescu['Cuotas'];
	$totalfinalescu = $totalventasescu + $totalcuotasiniescu;
	
//COOPINCORP	
$totalcoopin = 0;
$descuentocoopin = 0;

$sqlcoopin = "SELECT sum(movvalor) as Total, sum(movdescuento) as Descuento FROM movimientos WHERE movprefijo = 'RC' AND movfecha = '$fecha1' AND movempresa = '900306685' ;"; 
$rowcoopin = $db->query($sqlcoopin)->fetch(PDO::FETCH_ASSOC);
	$totalcoopin = $rowcoopin['Total'];
	$descuentocoopin = $rowcoopin['Descuento'];
	
$totalventascoopin = 0;
$totalcuotasinicoopin = 0;
$totalfinalcoopin = 0;

$sqlventascoopin = "SELECT sum(movvalor) as Total FROM solicitudes INNER JOIN movimientos ON solfactura = movdocumento  WHERE movempresa = '900306685' AND movfecha = '$fecha1' AND movprefijo = 'FV' AND  solncuota IN ('1','0') AND movestado <> 'ANULADA';"; 
$sqlcuotasinicoopin =  "SELECT sum(solcuota) as Cuotas FROM solicitudes INNER JOIN movimientos ON solfactura = movdocumento  WHERE movempresa = '900306685' AND movfecha = '$fecha1' AND movprefijo = 'FV' AND  solcuota > 0 AND movestado = 'FACTURADO';"; // qry cuotas ini
$rowventascoopin = $db->query($sqlventascoopin)->fetch(PDO::FETCH_ASSOC);
$rowcuotasinicoopin = $db->query($sqlcuotasinicoopin)->fetch(PDO::FETCH_ASSOC);
	$totalventascoopin = $rowventascoopin['Total'];
	$totalcuotasinicoopin = $rowcuotasinicoopin['Cuotas'];
	$totalfinalcoopin = $totalventascoopin + $totalcuotasinicoopin;
	
//SOPHYA
$totalsophya = 0;
$descuentosophya = 0;

$sqlsophya = "SELECT sum(movvalor) as Total, sum(movdescuento) as Descuento FROM movimientos WHERE movprefijo = 'RC' AND movfecha = '$fecha1' AND movempresa = '900813686' ;"; 
$rowsophya = $db->query($sqlsophya)->fetch(PDO::FETCH_ASSOC);
	$totalsophya = $rowsophya['Total'];
	$descuentosophya = $rowsophya['Descuento'];

$totalventassophya = 0;
$totalcuotasinisophya = 0;
$totalfinalsophya = 0;

$sqlventassophya = "SELECT sum(movvalor) as Total FROM solicitudes INNER JOIN movimientos ON solfactura = movdocumento  WHERE movempresa = '900813686' AND movfecha = '$fecha1' AND movprefijo = 'FV' AND  solncuota IN ('1','0') AND movestado <> 'ANULADA';"; 
$sqlcuotasinisophya =  "SELECT sum(solcuota) as Cuotas FROM solicitudes INNER JOIN movimientos ON solfactura = movdocumento  WHERE movempresa = '900813686' AND movfecha = '$fecha1' AND movprefijo = 'FV' AND  solcuota > 0 AND movestado = 'FACTURADO';"; // qry cuotas ini
$rowventassophya = $db->query($sqlventassophya)->fetch(PDO::FETCH_ASSOC);
$rowcuotasinisophya = $db->query($sqlcuotasinisophya)->fetch(PDO::FETCH_ASSOC);
	$totalventassophya = $rowventassophya['Total'];
	$totalcuotasinisophya = $rowcuotasinisophya['Cuotas'];
	$totalfinalsophya = $totalventassophya + $totalcuotasinisophya;

//SEVEN
$totalseven = 0;
$descuentoseven = 0;

$sqlseven = "SELECT sum(movvalor) as Total, sum(movdescuento) as Descuento FROM movimientos WHERE movprefijo = 'RC' AND movfecha = '$fecha1' AND movempresa = '900249109' ;"; 
$rowseven = $db->query($sqlseven)->fetch(PDO::FETCH_ASSOC);
	$totalseven = $rowseven['Total'];
	$descuentoseven = $rowseven['Descuento'];
	
$totalventasseven = 0;
$totalcuotasiniseven = 0;
$totalfinalseven = 0;

$sqlventasseven = "SELECT sum(movvalor) as Total FROM solicitudes INNER JOIN movimientos ON solfactura = movdocumento  WHERE movempresa = '900249109' AND movfecha = '$fecha1' AND movprefijo = 'FV' AND  solncuota IN ('1','0') AND movestado <> 'ANULADA';"; 
$sqlcuotasiniseven =  "SELECT sum(solcuota) as Cuotas FROM solicitudes INNER JOIN movimientos ON solfactura = movdocumento  WHERE movempresa = '900249109' AND movfecha = '$fecha1' AND movprefijo = 'FV' AND  solcuota > 0 AND movestado = 'FACTURADO';"; // qry cuotas ini
$rowventasseven = $db->query($sqlventasseven)->fetch(PDO::FETCH_ASSOC);
$rowcuotasiniseven = $db->query($sqlcuotasiniseven)->fetch(PDO::FETCH_ASSOC);
	$totalventasseven = $rowventasseven['Total'];
	$totalcuotasiniseven = $rowcuotasiniseven['Cuotas'];
	$totalfinalseven = $totalventasseven + $totalcuotasiniseven;


$pdf = new FPDF();
$pdf->AddFont('LucidaConsole','','lucon.php');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Arial','',7);
$pdf->Cell(0,4,date('Y/m/d H:i:s'),0,1,'R');
$pdf->SetFont('Arial','B',9);
$pdf->Cell(0,4,'GRUPO IDENCORP SAS',0,1,'L');
$pdf->Cell(0,4,'NIT 900051528',0,1,'L');
$pdf->Cell(0,4,'PLANILLA DIARIA DE CAJA                                                                                                                                 '.$iden.'',0,1,'L');
$pdf->Cell(0,4,'FECHA                                                                                                                                                                     '.$fecha1,0,1,'L');

$pdf->Ln(4);


$pdf->SetFont('Arial','B',8);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(93,4,'COBRANZA ',0,0,'C',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(4,4,'',0,0,'C',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(93,4,'VENTAS',0,1,'C',true);

$pdf->SetFont('Arial','',7);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'IDENCORP SAS ',1,0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'$ '.number_format($totaliden,0,',','.'),1,0,'R',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(4,4,'','L',0,'C',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'IDENCORP SAS','R',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'$ '.number_format($totalfinaliden,0,',','.'),1,1,'R',true);

$pdf->SetFont('Arial','',7);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'ESCUELA DE DESARROLLO ',1,0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'$ '.number_format($totalescu,0,',','.'),1,0,'R',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(4,4,'','L',0,'C',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'ESCUELA DE DESARROLLO','R',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'$ '.number_format($totalfinalescu,0,',','.'),1,1,'R',true);

$pdf->SetFont('Arial','',7);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'COOPINCORP INGRESO ',1,0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'$ '.number_format($totalcoopin,0,',','.'),1,0,'R',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(4,4,'','L',0,'C',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'COOPINCORP','R',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'$ '.number_format($totalfinalcoopin,0,',','.'),1,1,'R',true);

$pdf->SetFont('Arial','',7);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'SOPHYA EDITORES ',1,0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'$ '.number_format($totalsophya,0,',','.'),1,0,'R',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(4,4,'','L',0,'C',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'SOPHYA','R',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'$ '.number_format($totalfinalsophya,0,',','.'),1,1,'R',true);

$pdf->SetFont('Arial','',7);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'PRESTAMO SEVEN',1,0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'',1,0,'R',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(4,4,'','L',0,'C',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'','R',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'',1,1,'',true);

$pdf->SetFont('Arial','',7);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'APROVECHAMIENTO R.C',1,0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'',1,0,'R',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(4,4,'','L',0,'C',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'','R',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'',1,1,'',true);

$pdf->SetFont('Arial','',7);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'INGRESO DE LIBRANZA COOPINCORP',1,0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'',1,0,'R',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(4,4,'','L',0,'C',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'OTROS','R',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'$',1,1,'',true);

$pdf->SetFont('Arial','B',7);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'TOTAL COBRANZA ',1,0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'$ ',1,0,'R',true);
$pdf->SetFont('Arial','',7);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(4,4,'','L',0,'C',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'PRESTAMO SOCIOS','R',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'$',1,1,'',true);

$pdf->SetFont('Arial','B',7);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'CAMBIO DE CHEQUES','R L T',0,'C',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'',1,0,'R',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(4,4,'','L',0,'C',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'TOTAL',1,0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'$',1,1,'',true);

$pdf->SetFont('Arial','',7);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'INGRE CAMBIO CHEQUE IDEN C.E','R L',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'',1,1,'R',true);

$pdf->SetFont('Arial','',7);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'INGRE CAMBIO CHEQUE ESCUELA C.E 1850','R L',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'',1,1,'R',true);

$pdf->SetFont('Arial','B',7);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'TOTAL CAMBIO DE CHEQUES',1,0,'C',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'',1,1,'R',true);

$pdf->SetFont('Arial','',7);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'','R L',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'',1,1,'R',true);

$pdf->SetFont('Arial','',7);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'SALDO ANTERIOR','R L',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'',1,1,'R',true);

$pdf->SetFont('Arial','B',7);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'TOTAL INGRESOS','R L B T',0,'C',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'$',1,0,'R',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(4,4,'','L',0,'C',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'TOTAL INGRESOS',1,0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'$',1,1,'',true);

$pdf->Ln(4);






//INICIO SEGUNDA PARTE

$pdf->SetFont('Arial','B',8);
$pdf->SetFillColor(118,112,112);
$pdf->Cell(93,4,'DETALLES DE SALIDAS ',1,0,'C',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(4,4,'','R L',0,'C',true);
$pdf->SetFillColor(118,112,112);
$pdf->Cell(93,4,'CONSIGNACIONES',1,1,'C',true);

$pdf->SetFont('Arial','',7);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'','L T',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'','R T',0,'R',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(4,4,'','L R',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->SetFont('Arial','B',7);
$pdf->Cell(57,4,'BBVA',1,0,'R',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'',1,1,'',true);

$pdf->SetFont('Arial','',7);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'TARJETAS DE CREDITO IDEN','R L',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'',1,0,'R',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(4,4,'','L',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'','R L T',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'',1,1,'',true);

$pdf->SetFont('Arial','',7);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'CONSIGNACIONES','R L',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'$',1,0,'R',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(4,4,'','L',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'','R L',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'',1,1,'',true);

$pdf->SetFont('Arial','',7);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'DESCUENTOS IDENCORP','R L',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'$',1,0,'R',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(4,4,'','L',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'','R L',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'',1,1,'',true);

$pdf->SetFont('Arial','',7);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'DESCUENTOS ESCUELA','R L',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'$',1,0,'R',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(4,4,'','L',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'','R L',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'',1,1,'',true);

$pdf->SetFont('Arial','',7);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'DESCUENTOS COOPINCORP','R L',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'$',1,0,'R',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(4,4,'','L',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'','R L',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'',1,1,'',true);

$pdf->SetFont('Arial','',7);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'','R L',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'',1,0,'R',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(4,4,'','L',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'','R L',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'',1,1,'',true);

$pdf->SetFont('Arial','',7);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'','R L',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'',1,0,'R',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(4,4,'','L',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'','R L',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'',1,1,'',true);

$pdf->SetFont('Arial','B',7);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'PAGOS','R L',0,'C',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'',1,0,'R',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(4,4,'','L',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'','R L',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'',1,1,'',true);

$pdf->SetFont('Arial','',7);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'','R L',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'',1,0,'R',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(4,4,'','L',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'','R L',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'',1,1,'',true);

$pdf->SetFont('Arial','',7);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'','R L',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'',1,0,'R',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(4,4,'','L',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'','R L',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'',1,1,'',true);

$pdf->SetFont('Arial','',7);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'','R L',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'',1,0,'R',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(4,4,'','L',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'TOTAL','R L',0,'C',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'',1,1,'',true);

$pdf->SetFont('Arial','',7);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'','R L',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'',1,0,'R',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(4,4,'','L',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->SetFont('Arial','B',7);
$pdf->Cell(57,4,'BANCOLOMBIA',1,0,'R',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'',1,1,'',true);

$pdf->SetFont('Arial','',7);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'','R L ',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'',1,0,'R',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(4,4,'','L',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'','R L T',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'',1,1,'',true);

$pdf->SetFont('Arial','',7);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'','R L',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'',1,0,'R',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(4,4,'','L',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'','R L',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'',1,1,'',true);

$pdf->SetFont('Arial','',7);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'DINERO EN CAJA','R L B',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'$',1,0,'R',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(4,4,'','L',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->SetFont('Arial','B',7);
$pdf->Cell(57,4,'COLPATRIA',1,0,'R',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'',1,1,'',true);

$pdf->SetFont('Arial','B',7);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'SUBTOTAL',1,0,'L',true);
$pdf->SetFillColor(250,250,250); 
$pdf->Cell(36,4,'$ ',1,0,'R',true);
$pdf->SetFont('Arial','',7);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(4,4,'','L R',0,'C',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'','L R T',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'',1,1,'',true);

$pdf->SetFont('Arial','',7);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'RETENCION EN LA FUENTE','R T L',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'',1,0,'R',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(4,4,'','L',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'','R L',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'',1,1,'',true);

$pdf->SetFont('Arial','',7);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'','R L',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'',1,0,'R',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(4,4,'','L',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'','R L',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'',1,1,'',true);

$pdf->SetFont('Arial','',7);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'','R L',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'',1,0,'R',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(4,4,'','L',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'TOTAL','R L',0,'C',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'',1,1,'',true);

$pdf->SetFont('Arial','',7);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'','R L',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'',1,0,'R',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(4,4,'','L',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->SetFont('Arial','B',7);
$pdf->Cell(57,4,'DAVIVIENDA',1,0,'R',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'',1,1,'',true);

$pdf->SetFont('Arial','B',7);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'TOTAL',1,0,'L',true);
$pdf->SetFillColor(250,250,250); 
$pdf->Cell(36,4,'$ ',1,0,'R',true);
$pdf->SetFont('Arial','',7);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(4,4,'','L R',0,'C',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(57,4,'','L R T',0,'L',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(36,4,'',1,1,'',true);

$pdf->Output($r.'pdf/tesoreria/'.$iden.'.pdf');
$pdf->Output();

?>