<?php
$r = '../../';
//INCLUIR SESION Y CONECCION
require($r.'incluir/session.php');
require($r.'incluir/connection.php');


if($_POST['validar']){	
	
	$num_fac = strtoupper($_POST['num_fac']);
	$fecha1 = $_POST['fecha1'];
	$concepto = strtoupper($_POST['concepto']);
	$proveedor = $_POST['proveedor'];
	$empresa = $_POST['empresa'];
	$valor = $_POST['valor'];

	$num = $db->query("SELECT * FROM tesoreria WHERE tesonumfactura = '$num_fac' AND tesoempresa = '$empresa' ;")->rowCount();  // REALIZAMOS CONSULTA CON VARIABLES RECIBIDAS Y CONTAMOS LOS RESULTADOS OBTENIDOS
	if($num < 1) {  // SI LOS RESULTADOS DE LA CONSULTA SON MENORES A  1 REALIZAMOS LO SIGUIENTE
		$qryinsertar = $db->query("INSERT INTO tesoreria (tesonumfactura, tesofecha, tesoconcepto, tesovalor, tesoempresa, tesoproveedor) VALUES ('".$num_fac."', '".$fecha1."', '".$concepto."', '".$valor."', '".$empresa."', '".$proveedor."');	");
		
		if ($qryinsertar){
		$mensaje = "Se ha insertado correctamente.";
		header('Location:insertar_cyg.php?mensaje='.$mensaje.'');
		
			//Log de acciones realizadas por usuario en la BD	
			$qrylogsregister = $db->query("SELECT * FROM usuarios INNER JOIN perfiles ON usuperfil = perid WHERE usuid = ".$_SESSION['id']); //verificacion usuario por ID de sesion
			$rowlogsregister = $qrylogsregister->fetch(PDO::FETCH_ASSOC);	
			$qrylogsregister = $db->query("INSERT INTO logs ( usuario, id_usuario , ip_usuario , accion_usuario, fecha_accion) VALUES ( '".$rowlogsregister['usunombre']."', '".$_SESSION['id']."', '".$_SERVER['REMOTE_ADDR']."' , 'INSERTO DATOS EN TESORERIA CON LA SIGUIENTE INFORMACION NUM FACTURA $num_fac, FECHA $fecha1, CONCEPTO $concepto, VALOR $valor, EMPRESA $empresa, PROVEEDOR $proveedor'  , '".date("Y-m-d H:i:s")."' );"); //anexo de informacion de accion a la BD tabla LOGS	
		
		} else {
		$error = "No se pudo ingresar por favor intente nuevamente o contacte con el encargado del portal.";
		header('Location:insertar_cyg.php?error='.$error.'');
		}
	} else {	
	$error = "Ya se encuentra ese numero de factura asignado a esa empresa, porfavor verifique.";
	header('Location:insertar_cyg.php?error='.$error.'');
	}
} 
?>
<!doctype html>
<html>
<head>

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
<script type="text/javascript">

$(document).ready(function(){

	$('#form').validationEngine({ //VALIDACION DE CAMPOS REQUERIDOS EN FORMULARIO
		showOneMessage:true,
		onValidationComplete: function(form, status){
        	if(status) {               
            	carga();
            	return true;
			}
		}
	});

	$('#fecha1').datepicker({ // VALIDACION DE LA FECHA UNO
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		maxDate: '+50D',
	})
	
	$('.btnvalidar').button({ icons: { primary: 'ui-icon ui-icon-check' }});
	$('#dialog-message').dialog({ //CARACTERISTICAS DE LA VENTANA MODAL 
		height: 80,
		width: 'auto',
		modal: true
	});

});
</script>
<!-- ESTILOS EMBEBIDOS PARA MANEJAR FORMULARIO INDEPENDIENTEMENTE -->
<style type="text/css">
#form { width: 700px; margin:5px auto }
#form fieldset { padding:10px; display:block }
#form legend { font-weight: bold; margin-left:5px; padding:5px }
#form label { display:inline-block; width:90px; text-align:right; margin:0.3em 2px 0 0 }
#form .label { display:inline-block; width:100px; text-align:right; margin:0.3em 2px 0 0 }
#form p { margin:5px 0 }
</style>
</head>
<body>
<?php require($r.'incluir/src/login.php') ?> <!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
<section id="principal">
<?php require($r.'incluir/src/cabeza.php') ?> <!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
<?php require($r.'incluir/src/menu.php') ?> <!-- INCLUIMOS MENU PRINCIPAL -->
<article id="cuerpo">
<article class="mapa">
<a href="#">Principal</a><div class="mapa_div"></div><a href="#">Tesoreria</a><div class="mapa_div"></div><a href="#">Costos y Gastos</a><div class="mapa_div"></div><a class="current">Insertar</a>
</article>
<article id="contenido">
<form id="form" name="form" action="insertar_cyg.php" method="post"> <!-- ENVIO FORMULARIO POR POST -->
<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
<legend class="ui-widget ui-widget-header ui-corner-all">Insertar Costos y Gastos por Pagar</legend>
</br>
		<p>
		<label title ='Numero de Factura' >Num Factura: </label>
		<input type='text' name='num_fac' id='num_fac' class='validate[required] text-input'  />
		</p>
		<p>
		<label for="fecha1"> Fecha de pago: </label>
		<input type="text" name="fecha1" id="fecha1" class="validate[required] text-input fecha" /> 
		</p>
		<p>
		<label for="concepto"> Concepto: </label>
		<input style="width:140px" type="text" name="concepto" id="concepto" class="validate[required] text-input" /> 
		</p>
		<p>
		<label for="proveedor">Proveedor:</label>
		<select name="proveedor" class="validate[required] text-input">
		<option value="">SELECCIONE</option>
		<?php
		$qry = $db->query("SELECT * FROM proveedores_teso ORDER BY protesonombre");
		while($row = $qry->fetch(PDO::FETCH_ASSOC))
			echo '<option value='.$row['protesoid'].'>'.$row['protesonombre'].'</option>';
		?>
		</select>
		</p>
		<p>
		<label for="empresa">Empresa:</label>
		<select name="empresa" class="validate[required] text-input">
		<option value="">SELECCIONE</option>
		<?php
		$qry = $db->query("SELECT * FROM empresas ORDER BY empnombre");
		while($row = $qry->fetch(PDO::FETCH_ASSOC))
			echo '<option value='.$row['empid'].'>'.$row['empnombre'].'</option>';
		?>
		</select>
		</p>
		<p>
		<label title ='Valor' >Por Valor: </label>
		<input type='text' name='valor' id='valor' class='valor validate[required, custom[onlyNumberSp]] text-input'  />
		</p>
		</br>
	<p class="boton">
	<button type="submit" class="btnvalidar" name="validar" value="validar">Validar</button> <!-- BOTON VALIDAR -->
	</p>
</fieldset>
</form>
</article>
</article>
<?php require($r.'incluir/src/pie.php') ?>  <!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
</section>
<?php
if($_GET['error']) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>'.$_GET['error'].'</div>'; // MENSAJE MODAL ERROR
if($_GET['mensaje']) echo '<div id="dialog-message" title="Exitoso"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>'.$_GET['mensaje'].'</div>'; // MENSAJE MODAL EXITOSO
?>
</body>
</html>