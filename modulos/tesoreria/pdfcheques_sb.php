<?php
$r = '../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
require($r.'incluir/funciones.php');
require($r.'incluir/fpdf/fpdf.php');
	$fecha1 = $_GET['fecha1'];
	$fecha2 = $_GET['fecha2'];
	$proveedor = $_GET['proveedor'];
	$empresa = $_GET['empresa'];
	$numcheque = $_GET['numcheque'];

$filtro = 'fecha1=' . $fecha1 . '&fecha2=' . $fecha2 . '&proveedor=' . $proveedor . '&empresa=' . $empresa . '&numcheque=' . $numcheque;

$con = 'SELECT * FROM tesoreria
LEFT JOIN empresas on empid = tesoempresa
LEFT JOIN proveedores_teso on tesoproveedor = protesoid'; 
$ord = ' ORDER BY tesofecha DESC'; // Ordenar la Consulta\

    /* Los parametros de la consulta sql se genera dinamicamente 
    en base a los datos recibidos para delimitar los resultados */
    $parameters = [];
	array_push($parameters, "tesomediopago = 'Cheque' AND tesobanco is not null " );
	array_push($parameters, "tesovalid != 3" );
    if($fecha1 != "")
        array_push($parameters, "tesofecha between '$fecha1' AND '$fecha2'" );
    if($proveedor != "")
        array_push($parameters, "tesoproveedor = '$proveedor'" );
    if($empresa != "")
        array_push($parameters, "tesoempresa LIKE '%$empresa%'" );
    if($numcheque != "" && $asesor != "TODOS")
		array_push($parameters, "tesocheque = '$numcheque'" );
		
    // Consulta base
    $sql = $con;
    foreach ($parameters as $index => $parameter) {
        // Se agregan los parametros del WHERE
        if($index == 0)
            $sql .= " WHERE " . $parameter;
        else
            $sql .= " AND " . $parameter;
    }
    // Se completa la consulta
	$sql .= $ord;
class PDF extends FPDF{
	function Header(){
		global $fecha;
		global $fecha1;
		global $fecha2;
		global $adicional;
		global $nombre_adicional;
    	$this->SetFont('LucidaConsole','',6);
		$this->Cell(50,5,date('Y/m/d H:i:s'),0,1);
		$this->SetFont('LucidaConsole','',8);
		$this->Cell(260,5,'--------------------------------------------------------------------------------------------------------------------------------------------------------------------',0,1,'C');
    	$this->Cell(260,5,'LISTADO TESORERIA',0,1,'C');
		$this->Cell(260,5,'--------------------------------------------------------------------------------------------------------------------------------------------------------------------',0,1,'C');
    	$this->Ln(5);
	}

	function Footer(){
    	$this->SetY(-15);
    	$this->SetFont('LucidaConsole','',8);
		$this->Cell(0,5,'----------------------------------------------------------------------------------------------------------------------------------------------------------------------',0,1,'C');
		$this->SetFont('LucidaConsole','',8);
    	$this->Cell(0,5,'Pagina '.$this->PageNo().'/{nb}',0,0,'R');
	}
}

$pdf = new PDF('L','mm','Letter');
$pdf->AddFont('LucidaConsole','','lucon.php');
$pdf->AliasNbPages();
$pdf->AddPage('L');

$pdf->SetFont('LucidaConsole','',7);
$pdf->SetFillColor(220,220,220);
$pdf->SetX(4);
// Suma total 260
$pdf->Cell(10,5,'ID',1,0,'C',true);
$pdf->Cell(20,5,'NUM FACTURA',1,0,'C',true);
$pdf->Cell(20,5,'FECHA F.',1,0,'C',true);
$pdf->Cell(50,5,'CONCEPTO',1,0,'C',true);
$pdf->Cell(20,5,'VALOR',1,0,'C',true);

$pdf->Cell(35,5,'PROVEEDOR',1,0,'C',true);

$pdf->Cell(15,5,'N.CHEQUE',1,0,'C',true);
$pdf->Cell(15,5,'M. PAGO',1,0,'C',true);
$pdf->Cell(40,5,'A NOMBRE',1,0,'C',true);
$pdf->Cell(38,5,'C. ADICIONALES',1,1,'C',true);
$i = 1;

$qry = $db->query($sql);
$vlr1 = 0;
while($row = $qry->fetch(PDO::FETCH_ASSOC)){
	
$empresa = $row['empnombre'];

$proveedor_name = $row['protesonombre'];

if ($row['tesovalid'] == 1) { $tesoestado = "PAGADO"; } 
elseif ($row['tesovalid'] == 0) { $tesoestado = "SIN PAGAR"; } 
elseif ($row['tesovalid'] == 2) { $tesoestado = "CONFIRMADO"; }

	$pdf->SetFont('LucidaConsole','',6);
	if($i%2 == 0) $x = 255;
	else $x = 235;
	$pdf->SetFillColor($x,$x,$x);
	$pdf->SetX(4);
	$pdf->Cell(10,5,$row['tesoid'],1,0,'L',true);
	$pdf->Cell(20,5,$row['tesonumfactura'],1,0,'C',true);
	$pdf->Cell(20,5,$row['tesofecha'],1,0,'C',true);
	$pdf->Cell(50,5,utf8_decode(substr($row['tesoconcepto'],0,26)),1,0,'C',true);
	$pdf->Cell(20,5,round($row['tesovalor'],2),1,0,'C',true);
	$pdf->Cell(35,5,utf8_decode(substr($proveedor_name,0,25)),1,0,'C',true);
	$pdf->Cell(15,5,$row['tesocheque'],1,0,'R',true);
	$pdf->Cell(15,5,$row['tesomediopago'],1,0,'C',true);
	$pdf->Cell(40,5,$row['tesomediopagodetalle'],1,0,'C',true);
	$pdf->Cell(38,5,utf8_decode(substr($row['tesoadicional'],0,28)),1,1,'R',true);
	$i++;
	
$vlr1 = $vlr1 + $row['tesovalor'];
}

$pdf->SetX(4);

$pdf->SetFillColor(255,255,255);
$pdf->Cell(100,5,'',0,0,'',true);
$pdf->SetFillColor(220,220,220);
$pdf->Cell(20,5,round($vlr1,2),1,0,'C',true);

$pdf->Output('Listado cheques.pdf','d');
