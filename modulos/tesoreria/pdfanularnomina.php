<?php
$r = '../../';
require($r.'incluir/fpdf/fpdf.php');
require($r.'incluir/fpdf/fpdi.php');

$id = $_GET['id'];
$tipo = $_GET['tipo'];

if ($tipo == 'NOMINA'){
	$extra = '_NOMINA';
	$pdf = new FPDI();  
} elseif ($tipo == 'AUX TRANSPORTE') {
	$extra = '_AUX_TRANSPORTE';
	$pdf = new FPDI('L','mm','Letter');  
} elseif ($tipo == 'DOC EQUIVALENTE') {
	$extra = '_DOC_EQUIVALENTE';
	$pdf = new FPDI('L','mm','Letter');
}

$union = $id.$extra;
                    
$pagecount = $pdf->setSourceFile($r.'pdf/nominas/'.$union.'.pdf');
for($i = 1; $i <=  $pagecount; $i++){
	$tplidx = $pdf->importPage($i);
	$specs = $pdf->getTemplateSize($tplidx);
	$pdf->addPage($specs['h'] > $specs['w'] ? 'P' : 'L');
	$pdf->useTemplate($tplidx);
	$pdf->SetFont('Arial','B',50);
	$pdf->SetTextColor(255,0,0);
	$pdf->SetY(80);
	$pdf->Cell(0,35,'A  N  U  L  A  D  A',0,0,'C');
}
$pdf->Output($r.'pdf/nominas/'.$union.'.pdf');
$pdf->Output();
?>