<?php
	$r = '../../';
	//INCLUIR SESION Y CONECCION
	require($r . 'incluir/session.php');
	require($r . 'incluir/connection.php');


	if (isset($_POST['validar'])) {

		$num_fac = $_POST['num_fac'];
		$fecha1 = $_POST['fecha1'];
		$concepto = strtoupper($_POST['concepto']);
		$proveedor = $_POST['proveedor'];
		$empresa = $_POST['empresa'];
		$valor = $_POST['valor'];

		$porcentaje = "";
		$final = "";
		if(isset($_POST['porcentaje']))
		{
			$porcentaje = $_POST['porcentaje'];
			$final = $_POST['final'];
	
			if (!empty($porcentaje)) {
				if ($porcentaje == '0.1') {
					$porcentaje_fi = "10%";
				} elseif ($porcentaje == '0.2') {
					$porcentaje_fi = "20%";
				} elseif ($porcentaje == '0.3') {
					$porcentaje_fi = "30%";
				} elseif ($porcentaje == '0.4') {
					$porcentaje_fi = "40%";
				} elseif ($porcentaje == '0.5') {
					$porcentaje_fi = "50%";
				} elseif ($porcentaje == '0.6') {
					$porcentaje_fi = "60%";
				} elseif ($porcentaje == '0.7') {
					$porcentaje_fi = "70%";
				} elseif ($porcentaje == '0.8') {
					$porcentaje_fi = "80%";
				} elseif ($porcentaje == '0.9') {
					$porcentaje_fi = "90%";
				}
			}

		}


		// Se verifica si el registro ya existe
		$num = $db->query("SELECT * FROM tesoreria WHERE tesonumfactura = '$num_fac' AND tesoempresa = '$empresa' ;")->rowCount();  // REALIZAMOS CONSULTA CON VARIABLES RECIBIDAS Y CONTAMOS LOS RESULTADOS OBTENIDOS
		if ($num < 1) {  
			//1 PAGADO
			//0 SIN PAGAR
			//2 PAGOS CONFIRMADOS
			//3 ANULADOS
			$usuario = $_SESSION['id'];
			if ($porcentaje == '' && $final == '') {
				$estado = 1;
				if($proveedor != "")
				$qryinsertar = $db->query("INSERT INTO tesoreria (tesovalid, tesonumfactura, tesofecha, tesoconcepto, tesovalor, tesoempresa, tesoproveedor, tesousuario) VALUES ('$estado','$num_fac', '$fecha1', '$concepto', '$valor', '$empresa', '$proveedor', '$usuario');	") or die($db->errorInfo()[2]);
				else
				$qryinsertar = $db->query("INSERT INTO tesoreria (tesovalid, tesonumfactura, tesofecha, tesoconcepto, tesovalor, tesoempresa, tesousuario) VALUES ('$estado','$num_fac', '$fecha1', '$concepto', '$valor', '$empresa', '$usuario');	") or die($db->errorInfo()[2]);
			} else if ($porcentaje != '' && $final != '') {
				$estado = 1;
				if($proveedor != "")
				$qryinsertar = $db->query("INSERT INTO tesoreria (tesovalid, tesonumfactura, tesofecha, tesoconcepto, tesovalor, tesoempresa, tesoproveedor, tesoabono, tesoporcentaje, tesovalorfinal, tesousuario) VALUES ('$estado','$num_fac', '$fecha1', '$concepto', '$valor', '$empresa', '$proveedor', 'SI', '$porcentaje_fi', '$final', '$usuario');");
				else
				$qryinsertar = $db->query("INSERT INTO tesoreria (tesovalid, tesonumfactura, tesofecha, tesoconcepto, tesovalor, tesoempresa, tesoabono, tesoporcentaje, tesovalorfinal, tesousuario) VALUES ('$estado','$num_fac', '$fecha1', '$concepto', '$valor', '$empresa', 'SI', '$porcentaje_fi', '$final', '$usuario');");
			}
			if ($qryinsertar) {
				$mensaje = "Se ha insertado correctamente.";
				header('Location:insertar_cyg.php?mensaje=' . $mensaje . '');

				//Log de acciones realizadas por usuario en la BD	
				$qrylogsregister = $db->query("SELECT * FROM usuarios INNER JOIN perfiles ON usuperfil = perid WHERE usuid = " . $_SESSION['id']); //verificacion usuario por ID de sesion
				$rowlogsregister = $qrylogsregister->fetch(PDO::FETCH_ASSOC);
				if ($porcentaje == '' && $final == '') {
					$usuario = $_SESSION['id'];
					$msj = "INSERTO DATOS EN TESORERIA CON LA SIGUIENTE INFORMACION NUM FACTURA $num_fac, FECHA $fecha1, CONCEPTO $concepto, VALOR $valor, EMPRESA $empresa, PROVEEDOR $proveedor";
					$ip =  $_SERVER['REMOTE_ADDR'];
					$qrylogsregister = $db->query("INSERT INTO logs (id_usuario , ip_usuario , accion_usuario, fecha_accion) VALUES ( 
					'$usuario', '$ip' , '$msj'  , NOW() );"); 	
				} else if ($porcentaje != '' && $final != '') {
					$usuario = $_SESSION['id'];
					$msj = "INSERTO DATOS EN TESORERIA SIENDO UN ABONO CON LA SIGUIENTE INFORMACION NUM FACTURA $num_fac, PORCENTAJE DE ABONO $porcentaje_fi, VALOR FINAL CON ABONO $final, FECHA $fecha1, CONCEPTO $concepto, VALOR $valor, EMPRESA $empresa, PROVEEDOR $proveedor";
					$ip =  $_SERVER['REMOTE_ADDR'];
					$qrylogsregister = $db->query("INSERT INTO logs (id_usuario , ip_usuario , accion_usuario, fecha_accion) VALUES ( 
					'$usuario', '$ip' , '$msj'  , NOW() );"); 
				}
				exit();
			} else {
				$error = "No se pudo ingresar por favor intente nuevamente o contacte con el encargado del sistema.";
				header('Location:insertar_cyg.php?error=' . $error . '');
				exit();
			}
		} else {
			$error = "Ya se encuentra ese numero de factura asignado a esa empresa, porfavor verifique.";
			header('Location:insertar_cyg.php?error=' . $error . '');
			exit();
		}
	}
?>
<!doctype html>
<html lang="es">

<head>
    <title>AGREGAR COSTOS Y GASTOS POR PAGAR</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<script type="text/javascript">
		$(document).ready(function() {

			$("#valabono").change(function() {
				if ($("#valabono").is(':checked')) {
					document.getElementById("divabono").style.display = 'Initial';
					document.getElementById("porcentaje").disabled = false;
					document.getElementById("final").disabled = false;
				} else {
					document.getElementById("divabono").style.display = 'none';
					document.getElementById("porcentaje").disabled = true;
					document.getElementById("final").disabled = true;
					$("#porcentaje").prop("selectedIndex", 0);
					document.getElementById("final").value = 0;
				}
			});

			$('#valor, #porcentaje').change(function(event) {
				var valfinal = $('#valor').val() * $('#porcentaje').val();
				var valfinal = valfinal;
				$('#final').val(valfinal);
			});


			$('#fecha1').datepicker({ // VALIDACION DE LA FECHA UNO
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
			})

			$("#num_fac").blur(function(){
				factura($(this).val(), $("#empresa").val());
			});
			$("#empresa").blur(function(){
				factura($("#num_fac").val(), $(this).val());
			});

			function factura(fac, emp)
			{
				if(fac != "" && emp != "")
				{
					$.get("ajaxTesoreriaFac.php?fac=" + fac + "&emp=" + emp, function(res){
						if(res)
						{
							$("#error-fac").show();
							console.log("si");
						}
						else
						{
							$("#error-fac").hide();
							console.log("no");
						}
					});
				}
				
			}

		});
	</script>

</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
		<?php require($r . 'incluir/src/menu.php') ?>
		<!-- INCLUIMOS MENU PRINCIPAL -->
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Tesoreria</a>
				<div class="mapa_div"></div><a href="#">Costos y Gastos</a>
				<div class="mapa_div"></div><a class="current">Insertar</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="insertar_cyg.php" method="post">
					<!-- ENVIO FORMULARIO POR POST -->
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">Insertar Costos y Gastos por Pagar</legend>
						</br>
						<p>
							<label for="empresa">Empresa:</label>
							<select id="empresa" name="empresa" required class="validate[required] text-input">
								<?php
								$qry = $db->query("SELECT * FROM empresas ORDER BY empnombre");
								while ($row = $qry->fetch(PDO::FETCH_ASSOC))
									echo '<option value=' . $row['empid'] . '>' . $row['empnombre'] . '</option>';
								?>
							</select>
						</p>
						<p>
							<label title='Numero de Factura'>Num Factura: </label>
							<input type='text' name='num_fac' id='num_fac' required class='validate[required] text-input' />
							<span class="text-danger" id="error-fac" style="font-size:15px; display:none;">Esta factura ya existe en esta empresa</span>
						</p>
						<p>
							<label for="fecha1">Fecha de pago: </label>
							<input type="text" name="fecha1" id="fecha1" required class="validate[required] text-input fecha" />
						</p>
						<p>
							<label for="concepto">Concepto: </label>
							<input type="text" name="concepto" id="concepto" required class="uppercase validate[required] text-input" />
						</p>
						<p>
							<label for="proveedor">Proveedor:</label>
							<select name="proveedor" class=" text-input">
								<option value="">SELECCIONE</option>
								<?php
								$qry = $db->query("SELECT * FROM proveedores_teso ORDER BY protesonombre");
								while ($row = $qry->fetch(PDO::FETCH_ASSOC))
									echo '<option value=' . $row['protesoid'] . '>' . $row['protesonombre'] . '</option>';
								?>
							</select>
						</p>
						<p>
							<label class="not-w" for="valabono">¿Es un abono?</label>
							<input type='checkbox' name='valabono' id='valabono' />
						</p>
						<p>
							<label title='Valor'>Por Valor: </label>
							<input type='text' name='valor' id='valor' required class='valor validate[required, custom[onlyNumberSp]] text-input' />
						</p>
						<div id="divabono" style="display:none;">
							<p>
								<label for="porcentaje">Porcentaje:</label>
								<select name="porcentaje" id="porcentaje" class="validate[required] text-input" disabled>
									<option value="">SELECCIONE</option>
									<option value="0.1">10%</option>
									<option value="0.2">20%</option>
									<option value="0.3">30%</option>
									<option value="0.4">40%</option>
									<option value="0.5">50%</option>
									<option value="0.6">60%</option>
									<option value="0.7">70%</option>
									<option value="0.8">80%</option>
									<option value="0.9">90%</option>
								</select>
							</p>
							<p>
								<label title='Valor Final' for="final">Valor Final: </label>
								<input type='text' name='final' id='final' class='valor validate[required, custom[onlyNumberSp]] text-input' readonly disabled />
							</p>
						</div>
						</br>
						<p class="boton">
							<button type="submit" class="btn btn-primary btnvalidar" onclick="guardar()" name="validar" value="validar">GUARDAR</button>
						</p>
					</fieldset>
				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
		<!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
	</section>
	<?php
	if (isset($_GET['error'])) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['error'] . '</div>'; // MENSAJE MODAL ERROR
	if (isset($_GET['mensaje'])) echo '<div id="dialog-message" title="Exitoso"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['mensaje'] . '</div>'; // MENSAJE MODAL EXITOSO
	?>
	<?php require($r . 'incluir/src/loading.php'); ?>
</body>
</html>