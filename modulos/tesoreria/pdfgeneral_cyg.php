<?php
$r = '../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
require($r.'incluir/funciones.php');
require($r.'incluir/fpdf/fpdf.php');

	$fecha1 = $_GET['fecha1'];
	$fecha2 = $_GET['fecha2'];
	$proveedor = $_GET['proveedor'];
	$empresa = $_GET['empresa'];
	$estado = $_GET['estado'];

$filtro = 'fecha1='.$fecha1.'&fecha2='.$fecha2.'&proveedor='.$proveedor.'&empresa='.$empresa.'&estado='.$estado;
$con = 'SELECT tesoreria.*, empresas.empnombre, proveedores_teso.protesonombre FROM tesoreria
LEFT JOIN empresas on empid = tesoempresa
LEFT JOIN proveedores_teso on tesoproveedor = protesoid';
$ord = 'ORDER BY tesoid ASC'; // Ordenar la Consulta


	/* Los parametros de la consulta sql se genera dinamicamente 
	en base a los datos recibidos para delimitar los resultados */
	$parameters = [];
	array_push($parameters, "tesovalid != '3'");
	if ($fecha1 != "")
		array_push($parameters, "tesofecha between '$fecha1' AND '$fecha2'");
	if ($proveedor != "")
		array_push($parameters, "tesoproveedor = '$proveedor'");
	if ($empresa != "")
		array_push($parameters, "tesoempresa = '$empresa'");
	if ($estado != "")
		array_push($parameters, "tesovalid = '$estado'");

	// Consulta base
	$sql = $con;
	foreach ($parameters as $index => $parameter) {
		// Se agregan los parametros del WHERE
		if ($index == 0)
			$sql .= " WHERE " . $parameter;
		else
			$sql .= " AND " . $parameter;
	}
	// Se completa la consulta
	$sql .= $ord;

class PDF extends FPDF{
	function Header(){
		global $fecha;
		global $fecha1;
		global $fecha2;
		global $adicional;
		global $nombre_adicional;
    	$this->SetFont('LucidaConsole','',6);
		$this->Cell(50,5,date('d/m/Y H:i:s'),0,1);
		$this->SetFont('LucidaConsole','',8);
		$this->Cell(260,5,'--------------------------------------------------------------------------------------------------------------------------------------------------------------------',0,1,'C');
    	$this->Cell(260,5,'LISTADO TESORERIA',0,1,'C');
		$this->Cell(260,5,'--------------------------------------------------------------------------------------------------------------------------------------------------------------------',0,1,'C');
    	$this->Ln(5);
	}

	function Footer(){
    	$this->SetY(-15);
    	$this->SetFont('LucidaConsole','',8);
		$this->Cell(260,5,'----------------------------------------------------------------------------------------------------------------------------------------------------------------------',0,1,'C');
		$this->SetFont('LucidaConsole','',8);
    	$this->Cell(260,5,'Pagina '.$this->PageNo().'/{nb}',0,0,'R');
	}
}

$pdf = new PDF('L','mm','Letter');
$pdf->AddFont('LucidaConsole','','lucon.php');
$pdf->AliasNbPages();
$pdf->AddPage('L');

$pdf->SetFont('LucidaConsole','',7);
$pdf->SetFillColor(220,220,220);
$pdf->SetX(4);
// Suma total 260
$pdf->Cell(10,5,'ID',1,0,'C',true);
$pdf->Cell(15,5,'#FAC',1,0,'C',true);
$pdf->Cell(17,5,'FECHA',1,0,'C',true);
$pdf->Cell(55,5,'CONCEPTO',1,0,'C',true);
$pdf->Cell(20,5,'VALOR',1,0,'C',true);
$pdf->Cell(15,5,'ABONO%',1,0,'C',true);
$pdf->Cell(20,5,'ABONO',1,0,'C',true);
$pdf->Cell(35,5,'PROVEEDOR',1,0,'C',true);
$pdf->Cell(20,5,'ESTADO',1,0,'C',true);
$pdf->Cell(40,5,'M. PAGO',1,0,'C',true);
$pdf->Cell(25,5,'C. ADICIONALES',1,1,'C',true);
$i = 1;
$comision = 0;
$ttlbase = 0;
$ttlcomision = 0;
$qry = $db->query($sql);
$vlr1 = 0;
$vlrab = 0;
while($row = $qry->fetch(PDO::FETCH_ASSOC)){
$empresa = $row['empnombre'];
$proveedor_name = $row['protesonombre'];

if ($row['tesovalid'] == 1) { $tesoestado = "PAGADO"; } elseif ($row['tesovalid'] == 0) { $tesoestado = "SIN PAGAR"; } elseif ($row['tesovalid'] == 2) { $tesoestado = "CONFIRMADO"; }

if ($row['tesoabono'] == 'SI'){$abono = round($row['tesovalorfinal'],2);} else {$abono = 'NO';}

	$pdf->SetFont('LucidaConsole','',6);
	if($i%2 == 0) $x = 255;
	else $x = 235;
	$pdf->SetFillColor($x,$x,$x);
	$pdf->SetX(4);
	$pdf->Cell(10,5,$row['tesoid'],1,0,'L',true);
	$pdf->Cell(15,5,$row['tesonumfactura'],1,0,'C',true);
	$pdf->Cell(17,5,$row['tesofecha'],1,0,'C',true);
	$pdf->Cell(55,5,substr($row['tesoconcepto'],0,26),1,0,'C',true);
	$pdf->Cell(20,5,round($row['tesovalor'],2),1,0,'C',true);
	$pdf->Cell(15,5,substr($row['tesoporcentaje'], 0, 45),1,0,'C',true);
	$pdf->Cell(20,5,$abono,1,0,'C',true);
	$pdf->Cell(35,5,substr($proveedor_name,0,22),1,0,'C',true);
	$pdf->Cell(20,5,$tesoestado,1,0,'C',true);
	$pdf->Cell(40,5,substr($row['tesomediopago']." - ".$row['tesomediopagodetalle'],0,35),1,0,'C',true);
	$pdf->Cell(25,5,substr($row['tesoadicional'],0,28),1,1,'R',true);
	$i++;
	
$vlr1 = $vlr1 + $row['tesovalor'];
$vlrab = $vlrab + $row['tesovalorfinal'];
}

$pdf->SetX(4);

$pdf->SetFillColor(255,255,255);
$pdf->Cell(97,5,'',0,0,'',true);
$pdf->SetFillColor(220,220,220);
$pdf->Cell(20,5,round($vlr1,2),1,0,'C',true);
$pdf->Cell(15,5,'',0,0,'',true);
$pdf->Cell(20,5,round($vlrab,2),1,0,'C',true);

$pdf->Output('Listado tesoreria.pdf','d');
?>