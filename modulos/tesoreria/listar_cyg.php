<?php
	$r = '../../';
	require($r . 'incluir/session.php');
	require($r . 'incluir/connection.php');
	require($r . 'incluir/funciones.php');

	if (isset($_POST['consultar'])) {
		$fecha1 = $_POST['fecha1'];
		$fecha2 = $_POST['fecha2'];
		$proveedor = $_POST['proveedor'];
		$empresa = $_POST['empresa'];
		$estado = $_POST['estado'];
	} elseif (isset($_GET['fecha1'])) {
		$fecha1 = $_GET['fecha1'];
		$fecha2 = $_GET['fecha2'];
		$proveedor = $_GET['proveedor'];
		$empresa = $_GET['empresa'];
		$estado = $_GET['estado'];
	}
	if (isset($_GET['validar'])) {

		$id1 = $_GET['id1'];
		$fecha1 = $_GET['fecha1'];
		$fecha2 = $_GET['fecha2'];
		$proveedor = $_GET['proveedor'];
		$empresa = $_GET['empresa'];
		$estado = $_GET['estado'];

		$filtro = 'fecha1=' . $fecha1 . '&fecha2=' . $fecha2 . '&proveedor=' . $proveedor . '&empresa=' . $empresa . '&estado=' . $estado;

		$qry = $db->query("UPDATE tesoreria SET tesovalid='1', tesousuario='" . $_SESSION['id'] . "'  WHERE tesoid='" . $id1 . "';");
		if ($qry) {
			header('Location:anexfecha_cyg.php?id1=' . $id1 . '&' . $filtro);
			exit();
		} else {
			$error = 'No se pudo validar corectamente.';
			header('Location:listar_cyg.php?error=' . $error . '&' . $filtro);
			exit();
		}
	}

	if (isset($_GET['anular'])) {

		$id1 = $_GET['id1'];
		$fecha1 = $_GET['fecha1'];
		$fecha2 = $_GET['fecha2'];
		$proveedor = $_GET['proveedor'];
		$empresa = $_GET['empresa'];
		$estado = $_GET['estado'];

		$filtro = 'fecha1=' . $fecha1 . '&fecha2=' . $fecha2 . '&proveedor=' . $proveedor . '&empresa=' . $empresa . '&estado=' . $estado;

		$qry = $db->query("UPDATE tesoreria SET tesovalid='3' WHERE tesoid='" . $id1 . "';");
		if ($qry) {
			//Log de acciones realizadas por usuario en la BD	
			
			$usuario = $_SESSION['id'];
			$msj = "HA ANULADO EL PROCESO EN TESORERIA CON ID $id1";
			$ip =  $_SERVER['REMOTE_ADDR'];
			
			$qrylogsregister = $db->query("INSERT INTO logs (id_usuario , ip_usuario , accion_usuario, fecha_accion) VALUES ( 
			'$usuario', '$ip' , '$msj'  , NOW() );");

			$mensaje = "Se ha anulado correctamente.";
			header('Location:listar_cyg.php?mensaje=' . $mensaje . '&' . $filtro);
		} else {
			$error = 'No se pudo anular, intente nuevamente o contacte con el administrador del portal.';
			header('Location:listar_cyg.php?error=' . $error . '&' . $filtro);
		}
	}

	if (isset($_POST['anexofecha'])) {

		$id1 = $_POST['id1'];
		$fecha1 = $_POST['fechaone'];
		$fecha2 = $_POST['fechatwo'];
		$proveedor = $_POST['proveedor'];
		$empresa = $_POST['empresa'];
		//$estado = $_POST['estado'];

		$fecha1fil = $_GET['fecha1'];
		$fecha2fil = $_GET['fecha2'];
		$proveedorfil = $_GET['proveedor'];
		$empresafil = $_GET['empresa'];
		$estadofil = $_GET['estado'];


		$filtro = 'fecha1=' . $fecha1fil . '&fecha2=' . $fecha2fil . '&proveedor=' . $proveedorfil . '&empresa=' . $empresafil . '&estado=' . $estadofil;


		$qry = $db->query("UPDATE tesoreria SET tesofechapagar='" . $fecha2 . "', tesovalid='1', tesousuario='" . $_SESSION['id'] . "'  WHERE tesoid='" . $id1 . "';");
		if ($qry) {
			//Log de acciones realizadas por usuario en la BD	
			$usuario = $_SESSION['id'];
			$msj = "VALIDO TESORERIA CON ID NUMERO $id1 Y FECHA DE PAGO $fecha2";
			$ip =  $_SERVER['REMOTE_ADDR'];

			$qrylogsregister = $db->query("INSERT INTO logs (id_usuario , ip_usuario , accion_usuario, fecha_accion) VALUES ( 
			'$usuario', '$ip' , '$msj'  , NOW() );");

			$mensaje = 'Se ha pagado correctamente.';
			header('Location:listar_cyg.php?mensaje=' . $mensaje . '&' . $filtro);
			exit();
		} else {
			$error = 'No se pudo pagar corectamente.';
			header('Location:listar_cyg.php?error=' . $error . '&' . $filtro);
			exit();
		}
	}

	$filtro = 'fecha1=' . $fecha1 . '&fecha2=' . $fecha2 . '&proveedor=' . $proveedor . '&empresa=' . $empresa . '&estado=' . $estado;
	$con = 'SELECT tesoreria.*, empresas.empnombre, proveedores_teso.protesonombre FROM tesoreria
	LEFT JOIN empresas on empid = tesoempresa
	LEFT JOIN proveedores_teso on tesoproveedor = protesoid';
	$ord = 'ORDER BY tesofecha DESC'; // Ordenar la Consulta


	/* Los parametros de la consulta sql se genera dinamicamente 
		en base a los datos recibidos para delimitar los resultados */
	$parameters = [];
	if ($fecha1 != "")
		array_push($parameters, "tesofecha between '$fecha1' AND '$fecha2'");
	if ($proveedor != "")
		array_push($parameters, "tesoproveedor = '$proveedor'");
	if ($empresa != "")
		array_push($parameters, "tesoempresa = '$empresa'");
	if ($estado != "")
		array_push($parameters, "tesovalid = '$estado'");

	// Consulta base
	$sql = $con;
	foreach ($parameters as $index => $parameter) {
		// Se agregan los parametros del WHERE
		if ($index == 0)
			$sql .= " WHERE " . $parameter;
		else
			$sql .= " AND " . $parameter;
	}
	// Se completa la consulta
	$sql .= $ord;

	$qry = $db->query($sql);

?>
<!doctype html>
<html lang="es">

<head>
    <title>LISTA DE COSTOS Y GASTOS</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>

	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.7.0/css/buttons.dataTables.min.css" />
    <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>

	<link rel="stylesheet" href="<?php echo $r.'incluir/librerias/fontawesome/css/all.css'?>" />
    <script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
    
    <script src="<?php echo $r.'incluir/kavv_js/kavvdt.js'?>" ></script>

	<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>
	<script>
		$(document).ready(function() {
			$('#fechaone').datepicker({ // VALIDACION DE LA FECHA UNO
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
				onClose: function(selectedDate) {
					$('#fechatwo').datepicker('option', 'minDate', selectedDate);
				}
			}).keypress(function(event) {
				event.preventDefault()
			});
			$('#fechatwo').datepicker({ // VALIDACION DE LA FECHA DOS
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
				onClose: function(selectedDate) {
					$('#fechaone').datepicker('option', 'maxDate', selectedDate);
				}
			}).keypress(function(event) {
				event.preventDefault()
			});

			
            dt = createdt($("#tabla"),{col:2, buttons: bootstrapButtons, dom: bootstrapDesingSlimButtons});

			$('.confirmar').click(function(e) { // VENTANA MODAL PARA CONFIRMAR ACCIONES
				e.preventDefault();
				var targetUrl = $(this).attr("href");
				var $dialog_link_follow_confirm = $('<div></div>').html('<p><span class="ui-icon ui-icon-alet" style="float:left; margin:2px 7px 7px 0;"></span>Esta seguro que desea pagar?</p>').
				dialog({
					title: 'Porfavor confirmar',
					buttons: {
						'Si': function() {
							carga();
							window.location.href = targetUrl;
						},
						'No': function() {
							$(this).dialog("close");
						}
					},
					modal: true,
					width: 'auto',
					height: 'auto'
				});
				$dialog_link_follow_confirm.dialog('open');
			});
			$('.anular').click(function(e) { // VENTANA MODAL PARA CONFIRMAR ACCIONES
				e.preventDefault();
				var targetUrl = $(this).attr("href");
				var $dialog_link_follow_confirm = $('<div></div>').html('<p><span class="ui-icon ui-icon-alet" style="float:left; margin:2px 7px 7px 0;"></span>Esta seguro que desea anular este proceso?</p>').
				dialog({
					title: 'Porfavor confirmar',
					buttons: {
						'Si': function() {
							carga();
							window.location.href = targetUrl;
						},
						'No': function() {
							$(this).dialog("close");
						}
					},
					modal: true,
					width: 'auto',
					height: 'auto'
				});
				$dialog_link_follow_confirm.dialog('open');
			});

		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Tesoreria</a>
				<div class="mapa_div"></div><a href="#">Costos y Gastos</a>
				<div class="mapa_div"></div><a class="current">Listar</a>
			</article>
			<article id="contenido">
				<h2>Listado de Tesoreria</h2>
				<div class="reporte">
					<a href="pdfgeneral_cyg.php?<?php echo $filtro . "&generar=gnerar" ?>">
						<img src="<?php echo $r ?>imagenes/iconos/pdf.png" title="pdf" />
					</a> 
				</div>
				<form id="form" name="form" action="anexfecha_masivo_cyg.php?<?php echo $filtro ?>" method="post">
					<table id="tabla" class="table table-hover table-bordered" style="width:100%">
						<thead>
							<tr>
								<th title="Confirmar Pago Masivo">P.M</th>
								<th>ID</th>
								<th>#Fac</th>
								<th align="center">Fecha</th>
								<th align="center">Concepto</th>
								<th align="center">Valor</th>
								<th align="center">Abono%</th>
								<th align="center">Abono</th>
								<th align="center">Proveedor</th>
								<th>Estado</th>
								<th align="center" title="Medio de Pago + Detalle">M. Pago</th>
								<th align="center" title="Comentarios Adicionales">Comentario</th>
								<th>Pagar</th>
								<th>Anular</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$vlr1 = 0;
							$vlrab = 0;
							while ($row = $qry->fetch(PDO::FETCH_ASSOC)) { //INFORMACION DE LA CONSULTA

								$empresa = $row['empnombre'];
								$proveedor_name = $row['protesonombre'];

								$bg = "";
								if ($row['tesovalid'] == 1) {
									$texto_estado = "PAGADO";
								} elseif ($row['tesovalid'] == 0) {
									$texto_estado = "SIN PAGAR";
								} elseif ($row['tesovalid'] == 2) {
									$texto_estado = "CONFIRMADO";
								} elseif ($row['tesovalid'] == 3) {
									$texto_estado = "ANULADO";
									$bg = "bg-danger text-light";
								}
							?>
								<tr class="<?php echo $bg?>">
									<?php 
									if ($row['tesovalid'] == 0) {
										echo '<td><input type="checkbox" title="' . $row['tesoid'] . '" name="realipagos[]" value=' . $row['tesoid'] . '></td>';
									} else { 
										echo '<td><input type="checkbox" title="Este registro ya tiene un pago." disabled></td>';
									}?>
									<td><?php echo $row['tesoid'] ?></td>
									<td><?php echo $row['tesonumfactura'] ?></td>
									<td align="center"><?php echo $row['tesofecha'] ?></td>
									<td align="center"><?php echo $row['tesoconcepto'] ?></td>
									<td align="center"><?php echo round($row['tesovalor'], 2) ?></td>
									<td align="center"><?php echo $row['tesoporcentaje'] ?></td>
									<td align="center"><?php if ($row['tesoabono'] == 'SI') {
															echo round($row['tesovalorfinal'], 2);
														} else {
															echo "NO";
														} ?></td>
									<td align="center"><?php echo $proveedor_name ?></td>
									<td align="center"><?php if ($row['tesovalid'] == 1) {
															echo "PAGADO";
														} elseif ($row['tesovalid'] == 0) {
															echo "SIN PAGAR";
														} elseif ($row['tesovalid'] == 2) {
															echo "CONFIRMADO";
														} elseif ($row['tesovalid'] == 3) {
															echo "ANULADO";
														} ?></td>
									<td align="center"><?php echo $row['tesomediopago'] . " - " . $row['tesomediopagodetalle'] ?></td>
									<td align="center"><img src="<?php echo $r ?>imagenes/iconos/comment.png" title="<?php echo $row['tesoadicional'] ?>" /></td>
									<?php if ($row['tesovalid'] == 0 ) { ?>
										<td align="center"><a href="anexfecha_cyg.php?<?php echo 'validar=1&id1=' . $row['tesoid'] . '&' . $filtro ?>" title="Pagar" class="confirmar"><img src="<?php echo $r ?>imagenes/iconos/accept.png" /></a></td>
									<?php } else { ?>
										<td align="center"> <img src="<?php echo $r ?>imagenes/iconos/accept.png" class="gray" /> </td>
									<?php } ?>
									<!-- 30 = Super Admin y el estado es diferente a ANULADO -->
									<?php if ($row['tesovalid'] != 3) { ?>
										<td align="center"><a href="listar_cyg.php?<?php echo 'anular=1&id1=' . $row['tesoid'] . '&' . $filtro ?>" title="Anular" class="anular"><img src="<?php echo $r ?>imagenes/iconos/cancelar.png" /></a></td> 
									<?php } else {  ?>
										<td></td>
									<?php } ?>
								</tr>
							<?php
								if($row['tesovalid'] != 3)
								{
									$vlr1 += $row['tesovalor'];
									$vlrab += $row['tesovalorfinal'];
								}
							}
							?>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="3"></td>
								<td colspan="2" align="right" bgcolor="#D1CFCF">Valor Total:</td>
								<td align="center" bgcolor="#D1CFCF"><?php echo round($vlr1, 2) ?></td>
								<td align="right" bgcolor="#D1CFCF">Abonos:</td>
								<td align="center" bgcolor="#D1CFCF"><?php echo round($vlrab, 2) ?></td>
								<td colspan="6"></td>
							</tr>
						</tfoot>
					</table>
					<center>
						<br>
						<p class="col-md-6">
							<label> Fecha de pago:</label>
							<input type="text" name="fechatwo" id="fechatwo" class="validate[required] text-input fecha" />
						</p>
					</center>
					<p class="boton">
						<button type="submit" class="btn btn-success btnvalidar" name="btnvalidar" value="validar">Confirmar Pago Masivo</button>
						<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href='consultar_cyg.php'">Atras</button>
					</p>
				</form>

			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<?php
	if (isset($_GET['error'])) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['error'] . '</div>';
	elseif (isset($_GET['mensaje'])) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['mensaje'] . '</div>';
	?>
	<div id="modal" style="display:none"></div>
	<div id="dialog2" style="display:none"></div>
</body>

</html>