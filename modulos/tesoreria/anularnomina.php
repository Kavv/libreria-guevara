<?php
	$r = '../../';
	require($r . 'incluir/session.php');
	require($r . 'incluir/connection.php');
	require($r . 'incluir/funciones.php');

	$id = $_GET['id'];
	$fecha1 = $_GET['fecha1'];
	$fecha2 = $_GET['fecha2'];
	$usuario = $_GET['usuario'];

	$filtro = 'fecha1=' . $fecha1 . '&fecha2=' . $fecha2 . '&usuario=' . $usuario;

	$qrynomina = $db->query("SELECT * FROM nomina where nominaid = " . $id . "; ");
	$rownomina = $qrynomina->fetch(PDO::FETCH_ASSOC);

?>
<!doctype html>
<html lang="es">

<head>
    <title>PDF ANULADO - NOMINA</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Tesoreria</a>
				<div class="mapa_div"></div><a href="#">Documentos</a>
				<div class="mapa_div"></div><a class="current">Anular</a>
			</article>
			<article id="contenido">
				<p align="center">
					<iframe src="pdfanularnomina.php?id=<?php echo $id . '&tipo=' . $rownomina['nominatipo']; ?>" width="800" height="900"></iframe>
				</p>
				<?php
				$tipo = $rownomina['nominatipo'];

				if ($tipo == 'NOMINA') {
					$redireccion = 'listarnomina';
				} elseif ($tipo == 'AUX TRANSPORTE') {
					$redireccion = 'listartransporte';
				} elseif ($tipo == 'DOC EQUIVALENTE') {
					$redireccion = 'listardocequi';
				}
				?>
				<p class="boton">
					<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href = '<?php echo $redireccion ?>.php?<?php echo $filtro ?>'">Atras</button>
				</p>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
</body>

</html>