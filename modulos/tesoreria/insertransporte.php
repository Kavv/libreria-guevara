<?php
$r = '../../';
//INCLUIR SESION Y CONECCION
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');


if (isset($_POST['validar'])) {

	$usuario = $_POST['usuario'];

	$rowusu = $db->query("SELECT * FROM usuarios WHERE usuid = '" . $usuario . "';")->fetch(PDO::FETCH_ASSOC);

	$subtipo = $_POST['subtipo'];
	//$codigo = $rowusu['usucodigohelisa'];
	$empresa = $rowusu['usuempresa'];
	$auxilio = $_POST['auxilio'];
	$hoy = date('Y/m/d');
	$gestionado = $_SESSION['id'];

	$descuento1 = $_POST['descuento1'];
	$descripdescuento1 = trim(($_POST['descripdescuento1']));
	$descuento2 = $_POST['descuento2'];
	$descripdescuento2 = trim(($_POST['descripdescuento2']));
	$descuento3 = $_POST['descuento3'];
	$descripdescuento3 = trim(($_POST['descripdescuento3']));

	if (empty($empresa)) {
		$error = "Para Continuar es necesario que le asigne una empresa al usuario al que desea generar la nomina.";
		header('Location:insertransporte.php?error=' . $error . '');
	} elseif (empty($auxilio)) {
		$error = "Para Continuar es necesario que le asigne un Aux de transporte al usuario al que desea generar la nomina.";
		header('Location:insertransporte.php?error=' . $error . '');
	}/* elseif ($rowusu['usutopeauxiliotransporte'] < $auxilio) {
		$error = "No es posible continuar con el proceso por que el valor de auxilio de movilizacion que esta ingresando es mayor al asignado a este usuario que es de " . number_format($rowusu['usutopeauxiliotransporte'], 2) . ".";
		header('Location:insertransporte.php?error=' . $error . '');
	}*/ else {

		if (empty($descuento1)) {
			$descuento1 = 0;
			$descripdescuento1 = 'NONE';
		}
		if (empty($descuento2)) {
			$descuento2 = 0;
			$descripdescuento2 = 'NONE';
		}
		if (empty($descuento3)) {
			$descuento3 = 0;
			$descripdescuento3 = 'NONE';
		}

		$qry = $db->query("INSERT INTO nomina (nominatipo, nominasubtipo, nominausuario, nominaempresa, nominafecha, nominadescuento1, nominadescripdescuento1, nominadescuento2, nominadescripdescuento2, nominadescuento3, nominadescripdescuento3, nominamovilizacion, nominagestionado) VALUES ('AUX TRANSPORTE', '$subtipo', '$usuario', '$empresa', '$hoy', '$descuento1', '$descripdescuento1', '$descuento2', '$descripdescuento2', '$descuento3', '$descripdescuento3', '$auxilio', '$gestionado'); ");

		if ($qry) {
			$iden = $db->lastInsertId();
			header('Location:finalizartransporte.php?iden=' . $iden . '');
		} else {
			$error = "No se pudo ingresar por favor intente nuevamente o contacte con el encargado del portal.";
			header('Location:insertransporte.php?error=' . $error . '');
		}
	}
}
?>
<!doctype html>
<html lang="es">

<head>
    <title>AGREGAR AUXILIO DE MOVILIZACIÓN</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

	<script type="text/javascript">
		$(document).ready(function() {


			$('#fecha1').datepicker({ // VALIDACION DE LA FECHA UNO
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
				maxDate: '+50D',
				onClose: function(selectedDate) {
					$('#fecha2').datepicker('option', 'minDate', selectedDate);
				}
			})
			$('#fecha2').datepicker({ // VALIDACION DE LA FECHA DOS
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
				maxDate: '+50D',
				onClose: function(selectedDate) {
					$('#fecha1').datepicker('option', 'maxDate', selectedDate);
				}
			}).keypress(function(event) {
				event.preventDefault()
			});


			$('#departamento').change(function(event) {
				var id1 = $('#departamento').find(':selected').val();
				$('#ciudad').load('<?php echo $r ?>incluir/carga/ciudades.php?id1=' + id1);
			});

			$("#usuario").selectpicker();

			$("#usuario").change(function(){
				user_data($(this).val());
			});

			function user_data(user)
			{
				limpiar();
				$.get("userdata.php?user="+user, function(res){
					var usuario = JSON.parse(res);
					if(usuario != false)
					{
						$("#auxilio").val(usuario.usuauxiliotransporte);
						$("#auxilio-max").text(usuario.usutopeauxiliotransporte);
						if(usuario.empnombre != "")
							$("#emp-asignada").text(usuario.empnombre);
						else
							$("#emp-asignada").text("NINGUNA");
					}
					else
					{
						alert("El usuario no tiene asignada una empresa, verifique los datos del usuario (El proceso no se llevara a cabo)");
					}
				});
			}
			function limpiar()
			{
				$("#auxilio").val("");
				$("#auxilio-max").text("");
				$("#emp-asignada").text("");
			}
		});
	</script>

</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
		<?php require($r . 'incluir/src/menu.php') ?>
		<!-- INCLUIMOS MENU PRINCIPAL -->
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Tesoreria</a>
				<div class="mapa_div"></div><a class="current">Insertar Auxilio de Movilizacion</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="insertransporte.php" method="post">
					<!-- ENVIO FORMULARIO POR POST -->
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">Insertar Auxilio de Movilizacion </legend>
						</br>
						<p>
							<label for="usuario">Usuario:</label>
							<select id="usuario" name="usuario" class="selectpicker" required
							data-live-search="true" title="Seleccione un usuario" data-width="100%">
								<option value="">SELECCIONE</option>
								<?php
								// 56 = INACTIVO
								$qry = $db->query("SELECT * FROM usuarios ORDER BY usunombre ");
								while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
									echo '<option value="' . $row2["usuid"] . '">' . $row2["usuid"] .' / '.  $row2['usunombre'] . '</option>';
								}
								?>
							</select>
							<em style="font-size:14px">Empresa asignada:<span id="emp-asignada" style="color: #f00000; font-weight: bold;"></span></em>

						</p>
						<p>
							<label for="subtipo">Tipo:</label>
							<select name="subtipo" class="validate[required] text-input">
								<option value="">SELECCIONE</option>
								<option value="AUTORIZACION PAGO MEDIOS DE TRANSPORTE">PAGO MEDIOS DE TRANSPORTE</option>
								<option value="AUTORIZACION PAGO AUXILIO EXTRALEGAL PARA ESTUDIO DEL TRABAJADOR O LA FAMILIA">PAGO AUXILIO EXTRALEGAL PARA ESTUDIO </option>
								<option value="AUTORIZACION PAGO AUXILIO EXTRALEGAL DE ALIMENTACION Y/O VIVIENDA">PAGO AUXILIO EXTRALEGAL DE ALIMENTACION Y/O VIVIENDA</option>
							</select>
						</p>
						<p>
							<label for="auxilio">Auxilio de Movilizacion:</label>
							<input type='text' name='auxilio' id='auxilio' class='valor validate[required, custom[onlyNumberSp]] text-input' />
							<em style="font-size:14px">Valor maximo:<span id="auxilio-max" style="color: #f00000; font-weight: bold;"></span></em>

						</p>
						<p>
							<label> Concepto Descuento 1 </label>
							<input type='text' name='descripdescuento1' id='descripdescuento1' class='text-input' placeholder='Descuento 1...' />
							<label for="descuento1">Valor Descuento 1:</label>
							<input type='text' name='descuento1' id='descuento1' class='valor validate[custom[onlyNumberSp]] text-input' />
						</p>

						<p>
							<label> Concepto Descuento 2 </label>
							<input type='text' name='descripdescuento2' id='descripdescuento2' class='text-input' placeholder='Descuento 2...' />
							<label for="descuento2">Valor Descuento 2:</label>
							<input type='text' name='descuento2' id='descuento2' class='valor validate[custom[onlyNumberSp]] text-input' />
						</p>

						<p>
							<label> Concepto Descuento 3 </label>
							<input type='text' name='descripdescuento3' id='descripdescuento3' class='text-input' placeholder='Descuento 3...' />
							<label for="descuento3">Valor Descuento 3:</label>
							<input type='text' name='descuento3' id='descuento3' class='valor validate[custom[onlyNumberSp]] text-input' />
						</p>
						</br>
						<p class="boton">
							<button type="submit" class="btn btn-primary btnvalidar" name="validar" value="validar">Insertar</button> <!-- BOTON VALIDAR -->
						</p>
					</fieldset>
				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
		<!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
	</section>
	<?php
	if (isset($_GET['error'])) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['error'] . '</div>'; // MENSAJE MODAL ERROR
	if (isset($_GET['mensaje'])) echo '<div id="dialog-message" title="Exitoso"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['mensaje'] . '</div>'; // MENSAJE MODAL EXITOSO
	?>
	<div id="dialog2" title="Historico de Llamadas" style="display:none"></div>
</body>

</html>