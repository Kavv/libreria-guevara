<?php
$r = '../../'; 
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
include($r.'incluir/fpdf/fpdf.php');
require($r.'incluir/letras.class.php');

$iden = $_GET['iden'];
$sql = "SELECT nomina.*, e.empnombre, e.empid, u.usunombre, u.usucargo FROM nomina 
LEFT JOIN empresas e ON e.empid = nominaempresa
LEFT JOIN usuarios u ON u.usuid = nominausuario
WHERE nominaid = ".$iden.";"; 
$rowprin = $db->query($sql)->fetch(PDO::FETCH_ASSOC);

$usuario = $rowprin['usunombre'];

$empresa = $rowprin['empnombre'];

$hoy = date('d/m/Y');

$subTipo = $rowprin['nominasubtipo'];

if (empty($subTipo)){
	$subTipo = 'AUTORIZACION PAGO MEDIOS DE TRANSPORTE';
}

$pdf = new FPDF('L','mm','Letter');
$pdf->AddFont('LucidaConsole','','lucon.php');
$pdf->AliasNbPages();
$pdf->AddPage('L');
$pdf->SetFont('Arial','',9);
$pdf->Cell(260,7,date('Y/m/d H:i:s'),0,1,'R');
$pdf->SetFont('Arial','',10);
//$pdf->Cell(130,7,' ',' ',0,'L');
$pdf->Cell(260,7,' '.$subTipo.' ',' ',1,'R');
$pdf->SetFont('Arial','',9);
//$pdf->Cell(130,7,' ',' ',0,'L');
$pdf->Cell(260,7,'NIT: ' . $rowprin['nominaempresa'],' ',1,'R');
$V = new EnLetras();
$totalletras = strtoupper($V->ValorEnLetras($rowprin['nominamovilizacion'],"cordobas"));
$pdf->SetFont('Arial','',8);
$pdf->Cell(110,7,'VALOR ACORDADO ENTRE LAS PARTES ','T L B ',0,'L'); 
$pdf->Cell(110,7,''.$totalletras.'','T  B ',0,'L');
$pdf->Cell(40,7,'C$ '.round($rowprin['nominamovilizacion'],2).'','T R B ',1,'L');
$pdf->Cell(130,7,'CIUDAD Y FECHA','T L B R',0,'L');
$pdf->Cell(130,7,'MANAGUA '.$hoy.'','T L B R',1,'L');
$pdf->Cell(130,7,'EMPLEADO Y/O BENEFICIARIO ','T L B R',0,'L');
$pdf->Cell(130,7,''.$usuario.' ','T L B R',1,'L');
$pdf->Cell(130,7,'IDENTIFICACION CC No. ','T L B R',0,'L');
$pdf->Cell(130,7,''.$rowprin['nominausuario'].' ','T L B R',1,'L');
$pdf->Cell(130,7,'CARGO ','T L B R',0,'L');
$pdf->Cell(130,7,''.$rowprin['usucargo'].' ','T L B R',1,'L');
$pdf->Cell(130,7,'EMPRESA ','T L B R',0,'L');
$pdf->Cell(130,7,''.$empresa.' ','T L B R',1,'L');

if($rowprin['nominadescuento1'] != 0 || $rowprin['nominadescuento2'] != 0 || $rowprin['nominadescuento3'] != 0 )
	$pdf->Cell(260,7,'DESCUENTOS','T L B R',1,'C');	
	
	if ($rowprin['nominadescuento1'] != 0 ){
	$pdf->Cell(130,7,''.$rowprin['nominadescripdescuento1'].'','T L B R ',0,'L');
	$pdf->Cell(130,7,''.round($rowprin['nominadescuento1'],2).'','T L B R',1,'L');
	}
	if ($rowprin['nominadescuento2'] != 0 ){
	$pdf->Cell(130,7,''.$rowprin['nominadescripdescuento2'].'','T L B R ',0,'L');
	$pdf->Cell(130,7,''.round($rowprin['nominadescuento2'],2).'','T L B R',1,'L');
	}
	if ($rowprin['nominadescuento3'] != 0 ){
	$pdf->Cell(130,7,''.$rowprin['nominadescripdescuento3'].'','T L B R ',0,'L');
	$pdf->Cell(130,7,''.round($rowprin['nominadescuento3'],2).'','T L B R',1,'L');
	}

$netopagar = $rowprin['nominamovilizacion'] - $rowprin['nominadescuento1'] - $rowprin['nominadescuento2'] - $rowprin['nominadescuento3'];
$V = new EnLetras();
$totalletrasnetopagar = strtoupper($V->ValorEnLetras($netopagar,"cordobas"));
$pdf->Cell(260,7,'NETO A PAGAR = '.$totalletrasnetopagar.' C$ '.round($netopagar,2).'','T L B R',1,'C');	


if ($rowprin['nominasubtipo'] == 'AUTORIZACION PAGO MEDIOS DE TRANSPORTE'){
	$concepto = 'Medios de transporte para desarrollar la actividad laboral en la compania';
} elseif ($rowprin['nominasubtipo'] == 'AUTORIZACION PAGO AUXILIO EXTRALEGAL PARA ESTUDIO DEL TRABAJADOR O LA FAMILIA'){
	$concepto = 'Auxilio para la educacion y formacion para desarrollar la actividad laboral en la compania';
} elseif ($rowprin['nominasubtipo'] == 'AUTORIZACION PAGO AUXILIO EXTRALEGAL DE ALIMENTACION Y/O VIVIENDA'){
	$concepto = 'Auxilio para alimentacion y/o vivienda para desarrollar la actividad laboral en la compania';
} else {
	$concepto = 'Medios de transporte para desarrollar la actividad laboral en la compania';
}

$pdf->SetFillColor(250,250,250);
$pdf->SetFont('Arial','',9);
$pdf->MultiCell(260,6.4,'CONCEPTO: '.$concepto.'.','T R L ',1,'J',false);
/* $pdf->MultiCell(280,3.4,'NORMAS TRIBUTARIAS: No causa retencion en la fuente de acuredo con el concepto DIAN 18281 de Julio/30/1990, DIAN concepto 37327 de Junio/1995, DIAN concepto 25220 de Octubre/08/1988.',' R L ',1,'J',false);
$pdf->MultiCell(280,3.4,'NORMAS LABORALES: Articulo 128 C Laboral, subregado ley 50/90 Art 15, pagos que no son salarios.','B R L ',1,'J',false); */
$pdf->SetFillColor(250,250,250);
$pdf->SetFont('Arial','',12);
$pdf->Cell(130,17,'Firma Empleado:_________________________________',1,0,'',true);
$pdf->SetFont('Arial','',12);
$pdf->Cell(130,17,'Firma Autorizada: _______________________________',1,1,'C',true);
$pdf->SetFont('Arial','',9);
$pdf->Cell(260,5,'Copias: Gestion Humana y Contabilidad',1,1,'R',true); 

$pdf->Output($r.'pdf/nominas/'.$iden.'_AUX_TRANSPORTE.pdf');
$pdf->Output();
