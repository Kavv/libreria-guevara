<?php
$r = '../../';
//INCLUIR SESION Y CONECCION
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
?>
<!doctype html>
<html>

<head>

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

	<script type="text/javascript">
		$(document).ready(function() {

			$('#fecha1').datepicker({ // VALIDACION DE LA FECHA UNO
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
				onClose: function(selectedDate) {
					$('#fecha2').datepicker('option', 'minDate', selectedDate);
				}
			}).keypress(function(event) {
				event.preventDefault()
			});
			$('#fecha2').datepicker({ // VALIDACION DE LA FECHA DOS
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
				onClose: function(selectedDate) {
					$('#fecha1').datepicker('option', 'maxDate', selectedDate);
				}
			}).keypress(function(event) {
				event.preventDefault()
			});
			/* Valida que ambas fechas sean especificadas */
			$('#fecha1').change(function() {
				both_date();
			});

			$('#fecha2').change(function() {
				both_date();
			});

			function both_date() {
				if ($('#fecha2').val() != "" || $('#fecha1').val() != "") {
					$('#fecha1').attr("required", true);
					$('#fecha2').attr("required", true);
				} else {
					$('#fecha1').removeAttr("required", false);
					$('#fecha2').removeAttr("required", false);
				}
			}
			$("#user-list").selectpicker();


		});
	</script>

</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
		<?php require($r . 'incluir/src/menu.php') ?>
		<!-- INCLUIMOS MENU PRINCIPAL -->
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Tesoreria</a>
				<div class="mapa_div"></div><a href="#">Documento Equivalente</a>
				<div class="mapa_div"></div><a class="current">Consultar</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="listardocequi.php" method="post">
					<!-- ENVIO FORMULARIO POR POST A LISTAR.PHP -->
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">Consultar Documento Equivalente</legend>

						<p>
							<label for="fecha1"> Rango Fechas de gestion: </label>
							<input type="text" name="fecha1" id="fecha1" class="text-input fecha" /> <span> - </span> <input type="text" name="fecha2" id="fecha2" class="text-input fecha" />
						</p>
						<p>
							<label for="usuario">Usuario:</label>
							<select id="user-list" name="usuario" class="selectpicker" 
							data-live-search="true" title="Seleccione un usuario" data-width="100%">
								<option value="">SELECCIONE</option>
								<?php
								// 56 = inactivo
								$qry = $db->query("SELECT * FROM usuarios ORDER BY usunombre ");
								while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
									echo '<option value="' . $row2["usuid"] . '">' . $row2["usuid"] .' / '.  $row2['usunombre'] . '</option>';
								}
								?>
							</select>
						</p>
						</br>
						<p class="boton">
							<button type="submit" class="btn btn-primary btnconsulta" name="consultar" value="consultar">Consultar</button><!-- BOTON CONSULTAR -->
						</p>
					</fieldset>
				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
		<!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
	</section>
	<?php
	if (isset($_GET['error'])) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['error'] . '</div>'; // MENSAJE MODAL ERROR
	?>
</body>

</html>