<?php
	$r = '../../';
	//INCLUIR SESION Y CONECCION
	require($r . 'incluir/session.php');
	require($r . 'incluir/connection.php');
	require($r . 'incluir/funciones.php');

	if(isset($_GET['mensaje']))
		$mensaje = $_GET['mensaje'];
	if(isset($_GET['error']))
		$error = $_GET['error'];

	$fecha1 = $fecha2 = $banco = "";
	if (isset($_POST['consultar'])) {
		$fecha1 = $_POST['fecha1'];
		$fecha2 = $_POST['fecha2'];
		$banco = $_POST['banco'];
	} elseif (isset($_GET['fecha1'])) {
		$fecha1 = $_GET['fecha1'];
		$fecha2 = $_GET['fecha2'];
		$banco = $_GET['banco'];
	}
	
	$filtro = 'fecha1=' . $fecha1 . '&fecha2=' . $fecha2 . '&banco=' . $banco;


	if (isset($_POST['btnvalidar'])) {
		$valicheque = $_POST['valicheque'];
		$array = array_envia($valicheque);
		if (empty($valicheque)) {
			$error = $valicheque . " Es Necesario que seleccione algun cheque a validar, </br> Por favor realize el proceso nuevamente!";
			header('Location:cheques_sb.php?error=' . $error . '&'. $filtro);
			exit();
		}


		foreach ($valicheque as $valor) {
			$qry = $db->query("UPDATE tesoreria SET tesovalidcheque = '1' WHERE tesoid='" . $valor . "';");

			//Log de acciones realizadas por usuario en la BD	
			$usuario = $_SESSION['id'];
			$msj = "VALIDO CHEQUE TESORERIA TESO ID = $valor";
			$ip =  $_SERVER['REMOTE_ADDR'];

			$qrylogsregister = $db->query("INSERT INTO logs (id_usuario , ip_usuario , accion_usuario, fecha_accion) VALUES ( 
			'$usuario', '$ip' , '$msj'  , NOW() );");

		}
		$mensaje = "Validaciones realizadas correctamente";
		header('Location:cheques_sb.php?mensaje=' . $mensaje . '&'. $filtro);
		exit();
	}

	$con = "SELECT tesoreria.*, empresas.empnombre, proveedores_teso.protesonombre FROM tesoreria
	LEFT JOIN empresas on empid = tesoempresa
	LEFT JOIN proveedores_teso on tesoproveedor = protesoid";
	$ord = ' ORDER BY tesofecha DESC'; // Ordenar la Consulta
	$group = ''; // Realizar Group By 

	/* Los parametros de la consulta sql se genera dinamicamente 
		en base a los datos recibidos para delimitar los resultados */
	$parameters = [];
	array_push($parameters, "tesomediopago = 'Cheque' AND tesocheque is not null AND tesovalidcheque <> 1");
	if ($fecha1 != "")
		array_push($parameters, "tesofecha between '$fecha1' AND '$fecha2'");
	if ($banco != "")
		array_push($parameters, "tesobanco = '$banco'");

	// Consulta base
	$sql = $con;
	foreach ($parameters as $index => $parameter) {
		// Se agregan los parametros del WHERE
		if ($index == 0)
			$sql .= " WHERE " . $parameter;
		else
			$sql .= " AND " . $parameter;
	}
	// Se completa la consulta
	$sql .= $ord;
?>
<!doctype html>
<html lang="es">

<head>
    <title>VALIDAR CHEQUES</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>
	<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
	<script type="text/javascript">
		$(document).ready(function() {
			$('#checkAll').click(function() {
				$('input:checkbox').prop('checked', this.checked);
			});
			$('#tabla').dataTable({
				'bJQueryUI': true,
				'sPaginationType': 'full_numbers',
				'oLanguage': {
					'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
				},
				'bSort': false,
				'bLengthChange': false,
				'bFilter': false,
			});
		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
		<?php require($r . 'incluir/src/menu.php') ?>
		<!-- INCLUIMOS MENU PRINCIPAL -->
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Tesoreria</a>
				<div class="mapa_div"></div><a href="#">Saldos de banco</a>
				<div class="mapa_div"></div><a class="current">validar cheques</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="cheques_sb.php" method="post">
					<!-- ENVIO FORMULARIO POR POST A LISTAR.PHP -->

					<p style="text-align:center; font-size:20px; padding:10px 0;"> <strong>Seleccione porfavor los cheques que desea validar.</strong></p>
					<table id="tabla">
						<thead>
							<tr>
								<th title="Confirmar"></th>
								<th>#Cheque</th>
								<th>#Factura</th>
								<th>Concepto</th>
								<th>Fecha de Pago</th>
								<th>Para</th>
								<th align="center">Proveedor</th>
								<th align="center">Valor</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$qrypro = $db->query($sql);
							$vlr1 = 0;
							while ($rowpro = $qrypro->fetch(PDO::FETCH_ASSOC)) {
								$empresa = $rowpro['empnombre'];
								$proveedor_name = $rowpro['protesonombre'];
							?>
								<tr>
									<td><?php echo '<input type="checkbox" title="' . $rowpro['tesoid'] . '" name="valicheque[]" value=' . $rowpro['tesoid'] . '>'; ?></td>
									<td><?php echo $rowpro['tesocheque'] ?></td>
									<td><?php echo $rowpro['tesonumfactura'] ?></td>
									<td><?php echo $rowpro['tesoconcepto'] ?></td>
									<td><?php echo $rowpro['tesofechapagar'] ?></td>
									<td><?php echo $rowpro['tesomediopagodetalle'] ?></td>
									<td align="center"><?php echo $proveedor_name ?></td>
									<td align="center"><?php echo number_format($rowpro['tesovalor'], 2) ?></td>
								</tr>
							<?php
								$vlr1 = $vlr1 + $rowpro['tesovalor'];
							}
							?>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="6"></td>
								<td colspan="1" align="center" bgcolor="#D1CFCF">Valor Total</td>
								<td align="center" bgcolor="#D1CFCF"><?php echo number_format($vlr1, 2) ?></td>
							</tr>
						</tfoot>
					</table>

					<p class="boton">
						<button type="submit" class="btn btn-primary btnvalidar" name="btnvalidar" value="validar">Confirmar</button> <!-- BOTON VALIDAR -->
					</p>
				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
		<!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
	</section>
	<?php
	if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>';
	elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>';
	?>
</body>

</html>