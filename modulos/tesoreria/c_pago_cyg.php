<?php
$r = '../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

@$mensaje = $_GET['mensaje'];
@$error = $_GET['error'];

if (isset($_POST['consultar'])) {
	$fecha1 = $_POST['fecha1'];
	$fecha2 = $_POST['fecha2'];
} elseif (isset($_GET['fechafil1'])) {
	$fecha1 = $_GET['fechafil1'];
	$fecha2 = $_GET['fechafil2'];
}

if (isset($_POST['confirmarpago'])) {

	$id1 = $_POST['id1'];
	$forma_pa = $_POST['forma_pa'];
	$adicional = strtoupper($_POST['adicional']);
	$detalle = strtoupper($_POST['detalle']);
	$detcheque = "";
	if(isset($_POST['detcheque']))
		$detcheque = $_POST['detcheque'];

	$banco = $_POST['banco'];
	if($banco == "")
	$banco = "NULL";

	if ($detcheque <> '') {
		$numcheque = $detcheque;
	} else {
		$numcheque = 'NULL';
	}

	$fechafil1 = $_POST['fechafil1'];
	$fechafil2 = $_POST['fechafil2'];

	$filtro = "fechafil1=" . $fechafil1 . "&fechafil2=" . $fechafil2 . "";

	 // OBTENERMOS EL CONSECUENTE PARA ESTE PROCESO DE TESORERIA
	 $consecutivo = $db->query("select nextval('tesoreria_consecuente') as consecutivo")->fetch(PDO::FETCH_ASSOC);
	 if($consecutivo)
		 $consecutivo = $consecutivo['consecutivo'];
	 else 
		 $consecutivo = 0;
	$qry = $db->query("UPDATE tesoreria SET tesovalid='2', tesocheque=$numcheque, tesobanco=$banco, tesomediopago='" . $forma_pa . "', tesomediopagodetalle='" . $detalle . "', tesoadicional='" . $adicional . "', tesoconsecutivo='" . $consecutivo . "' WHERE tesoid='" . $id1 . "';");
	if ($qry) {
		$mensaje = 'Se ha confirmado el pago correctamente.';
		header('Location:c_pago_cyg.php?mensaje=' . $mensaje . '&' . $filtro);

		//Log de acciones realizadas por usuario en la BD	
		$usuario = $_SESSION['id'];
		$msj = "CONFIRMO EL PAGO CON ID $id1, NUM CHEQUE $numcheque, BANCO $banco, MEDIO DE PAGO $forma_pa, DETALLE $detalle, ADICIONAL $adicional";
		$ip =  $_SERVER['REMOTE_ADDR'];

		$qrylogsregister = $db->query("INSERT INTO logs (id_usuario , ip_usuario , accion_usuario, fecha_accion) VALUES ( 
		'$usuario', '$ip' , '$msj'  , NOW() );");
		exit();
	} else {
		$error = 'No se pudo confirmar el pago corectamente.';
		header('Location:c_pago_cyg.php?error=' . $error . '&' . $filtro);
		exit();
	}
}

$filtro = 'fechafil1=' . $fecha1 . '&fechafil2=' . $fecha2;
$sql = "SELECT tesoreria.*, empresas.empnombre, proveedores_teso.protesonombre FROM tesoreria
LEFT JOIN empresas on empid = tesoempresa
LEFT JOIN proveedores_teso on tesoproveedor = protesoid
WHERE tesovalid = '1'  AND tesofechapagar BETWEEN '$fecha1' AND '$fecha2' ORDER BY tesofechapagar DESC";

$qry = $db->query($sql);

?>
<!doctype html>
<html lang="es">

<head>
    <title>CONFIRMAR PAGOS</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">
	<meta charset="utf-8">
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>

	<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
	<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
	<script>
		$(document).ready(function() {
			$('#tabla').dataTable({ //QRY PARA MANEJO Y VISUALIZACION DE TABLAS
				'bJQueryUI': true,
				'sPaginationType': "full_numbers",
				'oLanguage': {
					'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
				},
				'aoColumnDefs': [{
					'bSortable': false,
					'aTargets': [2, 3, 4, 5, 8, 9, 10]
				}],
			});

			$('.confirmar').click(function(e) { // VENTANA MODAL PARA CONFIRMAR ACCIONES
				e.preventDefault();
				var targetUrl = $(this).attr("href");
				var $dialog_link_follow_confirm = $('<div></div>').html('<p><span class="ui-icon ui-icon-alet" style="float:left; margin:2px 7px 7px 0;"></span>Esta seguro que desea pagar?</p>').
				dialog({
					title: 'Porfavor confirmar',
					buttons: {
						'Si': function() {
							carga();
							window.location.href = targetUrl;
						},
						'No': function() {
							$(this).dialog("close");
						}
					},
					modal: true,
					width: 'auto',
					height: 'auto'
				});
				$dialog_link_follow_confirm.dialog('open');
			});

		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Tesoreria</a>
				<div class="mapa_div"></div><a href="#">Costos y Gastos</a>
				<div class="mapa_div"></div><a class="current">Confirmar Pago</a>
			</article>
			<article id="contenido">
				<h2>Confirmar Pago</h2>
				<div class="reporte">
					<a href="pdfconfirmar_cyg.php?<?php echo $filtro . "&generar=gnerar" ?>">
						<img src="<?php echo $r ?>imagenes/iconos/pdf.png" title="pdf" />
					</a> 
				</div>

				<table id="tabla">
					<thead>
						<tr>
							<th>ID</th>
							<th>#Factura</th>
							<th align="center">Fecha</th>
							<th align="center">Fecha de Pago</th>
							<th align="center">Concepto</th>
							<th align="center">Valor</th>
							<th align="center">Abono%</th>
							<th align="center">Abono</th>
							<th align="center">Proveedor</th>
							<th>Confirmar</th>
							<th>Consecutivo</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$vlr1 = 0;
						$vlrab = 0;
						while ($row = $qry->fetch(PDO::FETCH_ASSOC)) { //INFORMACION DE LA CONSULTA

							$empresa = $row['empnombre'];

							$proveedor_name = $row['protesonombre'];

						?>
							<tr>
								<td><?php echo $row['tesoid'] ?></td>
								<td><?php echo $row['tesonumfactura'] ?></td>
								<td align="center"><?php echo $row['tesofecha'] ?></td>
								<td align="center"><?php echo $row['tesofechapagar'] ?></td>
								<td align="center"><?php echo $row['tesoconcepto'] ?></td>
								<td align="center"><?php echo round($row['tesovalor'], 2) ?></td>
								<td align="center"><?php echo $row['tesoporcentaje']  ?></td>
								<td align="center"><?php if ($row['tesoabono'] == 'SI') {
														echo round($row['tesovalorfinal'], 2);
													} else {
														echo "NO";
													} ?></td>
								<td align="center"><?php echo $proveedor_name ?></td>
								<?php if ($row['tesovalid'] == 1 and $row['tesomediopago'] == '') { ?>
									<td align="center"><a href="conf_cyg.php?<?php echo 'validar=1&id1=' . $row['tesoid'] . '&' . $filtro ?>" title="Pagar"><img src="<?php echo $r ?>imagenes/iconos/money.png" /></a></td>
								<?php } else { ?>
									<td align="center"> <img src="<?php echo $r ?>imagenes/iconos/money.png" class="gray" /> </td>
								<?php } ?>
								<td align="center"><?php echo $row['tesoconsecutivo'] ?></td>
							</tr>
						<?php
							$vlr1 += $row['tesovalor'];
							$vlrab += $row['tesovalorfinal'];
						}
						?>
					</tbody>
					<tfoot>
						<tr>
							<td colspan="5"></td>
							<td colspan="1" align="center" bgcolor="#D1CFCF">Valor Total</td>
							<td align="center" bgcolor="#D1CFCF"><?php echo round($vlr1, 2) ?></td>
							<td align="center" bgcolor="#D1CFCF"><?php echo round($vlrab, 2) ?></td>
							<td colspan="2"></td>
						</tr>
					</tfoot>
				</table>
				<p class="boton">
					<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href='con_c_pago_cyg.php'">Atras</button>
				</p>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<?php
	if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>';
	elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>';
	?>
	<div id="modal" style="display:none"></div>
	<div id="dialog2" style="display:none"></div>
</body>

</html>