<?php
	$r = '../../';
	require($r . 'incluir/session.php');
	require($r . 'incluir/connection.php');
	require($r . 'incluir/funciones.php');
	$iden = $_GET['iden'];
?>
<!doctype html>
<html lang="es">

<head>
    <title>PDF - DOC. EQUIVALENTE</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>

</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Tesoreria</a>
				<div class="mapa_div"></div><a class="current">Insertar Documento Equivalente</a>
			</article>
			<article id="contenido">
				<p align="center">
					<iframe src="pdfdocequivalente.php?iden=<?php echo $iden ?>" width="950" height="700"></iframe>
				</p>
				<p class="boton">
					<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href = 'inserdocequi.php'">Atras</button>
				</p>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
</body>

</html>