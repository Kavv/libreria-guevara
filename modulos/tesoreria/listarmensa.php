<?php
	$r = '../../';
	require($r . 'incluir/session.php');
	require($r . 'incluir/connection.php');
	require($r . 'incluir/funciones.php');


	if (isset($_GET['mensaje']))
		$mensaje = $_GET['mensaje'];
	if (isset($_GET['error']))
		$error = $_GET['error'];

	$fecha1 = "";
	$fecha2 = "";
	$enviado = "";
	$gestionado = "";

	if (isset($_POST['consultar'])) {
		$fecha1 = $_POST['fecha1'];
		$fecha2 = $_POST['fecha2'];
		$enviado = $_POST['enviado'];
		$gestionado = $_POST['gestionado'];
	}

	if (isset($_GET['fecha1'])) {
		$fecha1 = $_GET['fecha1'];
		$fecha2 = $_GET['fecha2'];
		$enviado = $_GET['enviado'];
		$gestionado = $_GET['gestionado'];
	}

	if (isset($_GET['validar'])) {

		$filtro = 'fecha1=' . $fecha1 . '&fecha2=' . $fecha2 . '&enviado=' . $enviado . '&gestionado=' . $gestionado;

		$id1 = $_GET['id1'];
		$qry = $db->query("UPDATE mensajeria SET mensavalidado='1' WHERE mensaid='" . $id1 . "';");
		if ($qry) {
			$mensaje = 'Validado Correctamente.';
			header('Location:listarmensa.php?mensaje=' . $mensaje . '&' . $filtro);
		} else {
			$error = 'No se pudo validar corectamente.';
			header('Location:listarmensa.php?error=' . $error . '&' . $filtro);
		}
		exit();
	}


	$filtro = 'fecha1=' . $fecha1 . '&fecha2=' . $fecha2 . '&enviado=' . $enviado . '&gestionado=' . $gestionado;
	$con = 'SELECT * FROM mensajeria INNER JOIN usuarios on usuid = mensaenvia';
	$ord = ' ORDER BY mensafecha DESC'; // Ordenar la Consulta

	/* Los parametros de la consulta sql se genera dinamicamente 
		en base a los datos recibidos para delimitar los resultados */
	$parameters = [];
	if ($fecha1 != "")
		array_push($parameters, "mensafecha between '$fecha1' AND '$fecha2'");
	if ($enviado != "")
		array_push($parameters, "mensaenvia LIKE '%$enviado%'");
	if ($gestionado != "")
		array_push($parameters, "mensausuario LIKE '%$gestionado%'");

	// Consulta base
	$sql = $con;
	foreach ($parameters as $index => $parameter) {
		// Se agregan los parametros del WHERE
		if ($index == 0)
			$sql .= " WHERE " . $parameter;
		else
			$sql .= " AND " . $parameter;
	}
	$sql .= $ord;

	$qry = $db->query($sql);

?>
<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
	<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>

	<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>
	<script>
		$(document).ready(function() {

			$('#tabla').dataTable({ //QRY PARA MANEJO Y VISUALIZACION DE TABLAS
				'bJQueryUI': true,
				'sPaginationType': "full_numbers",
				'oLanguage': {
					'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
				},
				'bSort': false,
			});


			$('.confirmar').click(function(e) { // VENTANA MODAL PARA CONFIRMAR ACCIONES
				e.preventDefault();
				var targetUrl = $(this).attr("href");
				var $dialog_link_follow_confirm = $('<div></div>').html('<p><span class="ui-icon ui-icon-alet" style="float:left; margin:2px 7px 7px 0;"></span>Esta seguro que desea validar?</p>').
				dialog({
					title: 'Porfavor confirmar',
					buttons: {
						'Si': function() {
							carga();
							window.location.href = targetUrl;
						},
						'No': function() {
							$(this).dialog("close");
						}
					},
					modal: true,
					width: 'auto',
					height: 'auto'
				});
				$dialog_link_follow_confirm.dialog('open');
			});

		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Tesoreria</a>
				<div class="mapa_div"></div><a href="#">Mensajeria</a>
				<div class="mapa_div"></div><a class="current">Listar</a>
			</article>
			<article id="contenido">
				<h2>Listado de Mensajeria</h2>
				<div class="reporte">
					
				</div>
				<table id="tabla">
					<thead>
						<tr>
							<th>ID</th>
							<th>Enviado por </th>
							<th align="center">Fecha</th>
							<th align="center">Observacion</th>
							<th align="center">Gestionado por</th>

							<th>Validar</th>

						</tr>
					</thead>
					<tbody>
						<?php
						while ($row = $qry->fetch(PDO::FETCH_ASSOC)) { //INFORMACION DE LA CONSULTA

							$qryusuario = $db->query("SELECT * FROM usuarios where usuid = '" . $row['mensausuario'] . "'; ");
							$rowusuario = $qryusuario->fetch(PDO::FETCH_ASSOC);
							$usuariogestion = $rowusuario['usunombre'];

						?>
							<tr>
								<td><?php echo $row['mensaid'] ?></td>
								<td><?php echo $row['usunombre'] ?></td>
								<td align="center"><?php echo $row['mensafecha'] ?></td>
								<td><?php echo $row['mensaobservacion'] ?></td>
								<td><?php echo $usuariogestion ?></td>
								<?php if ($row['mensavalidado'] == 0) { ?>
									<td align="center"><a href="listarmensa.php?<?php echo 'validar=1&id1=' . $row['mensaid'] . '&' . $filtro ?>" title="Validar" class="confirmar"><img src="<?php echo $r ?>imagenes/iconos/accept.png" /></a></td>
								<?php } else { ?>
									<td align="center"> <img src="<?php echo $r ?>imagenes/iconos/accept.png" class="gray" /> </td>
								<?php } ?>
							</tr>
						<?php
						}
						?>
					</tbody>
				</table>

				<p class="boton">
					<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href='consultarmensa.php'">Atras</button>
				</p>


			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<?php
	if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>';
	elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>';
	?>
	<div id="modal" style="display:none"></div>
	<div id="dialog2" style="display:none"></div>
</body>

</html>