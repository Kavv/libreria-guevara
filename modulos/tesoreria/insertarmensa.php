<?php
// LA ACCION PRINCIPAL ES ESTA PAGINA ES EL MANEJO DE LLAMADAS PARA EL CALL CENTER
$r = '../../';
//INCLUIR SESION Y CONECCION
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');


if (isset($_POST['validar'])) {

	$enviado = $_POST['enviado'];
	$fecha1 = $_POST['fecha1'];
	$observaciones = strtoupper(trim(($_POST['observaciones'])));
	$gestionado = $_SESSION['id'];

	$qry = $db->query("INSERT INTO mensajeria (mensaenvia, mensafecha, mensaobservacion, mensausuario) VALUES ('$enviado', '$fecha1', '$observaciones', '$gestionado'); ") or die($db->errorInfo()[2]);

	if ($qry) {
		$mensaje = "Se ha ingresado correctamente el nuevo encargo al mensajero. ";
		header('Location:insertarmensa.php?mensaje=' . $mensaje . '');
	} else {
		$error = "No se pudo ingresar por favor intente nuevamente o contacte con el encargado del portal. ";
		header('Location:insertarmensa.php?error=' . $error . '');
	}
	exit();
}
?>
<!doctype html>
<html>

<head>

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

	<script type="text/javascript">
		$(document).ready(function() {

			$('#fecha1').datepicker({ // VALIDACION DE LA FECHA UNO
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
				maxDate: '+50D',
			})
			$("#usuario").selectpicker();

		});
	</script>

</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
		<?php require($r . 'incluir/src/menu.php') ?>
		<!-- INCLUIMOS MENU PRINCIPAL -->
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Tesoreria</a>
				<div class="mapa_div"></div><a class="current">Mensajeria</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="insertarmensa.php" method="post">
					<!-- ENVIO FORMULARIO POR POST -->
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">Mensajeria </legend>
						</br>
						<p>
							<label for="enviado">Enviado por:</label>
							<select id="usuario" name="enviado" class="selectpicker" required
							data-live-search="true" title="Seleccione un usuario" data-width="100%">
								<option value="">SELECCIONE</option>
								<?php
								$qry = $db->query("SELECT * FROM usuarios ORDER BY usunombre ");
								while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
									echo '<option value="' . $row2["usuid"] . '">' . $row2["usuid"] .' / '.  $row2['usunombre'] . '</option>';
								}
								?>
							</select>
						</p>
						<p>
							<label for="fecha1">Fecha:</label>
							<input type="text" name="fecha1" id="fecha1" class="validate[required] text-input fecha" />
						</p>
						<p>
							<label for="observaciones">Observaciones:</label>
							<textarea name="observaciones" rows="5" class="form-control validate[required] text-input"></textarea>
						</p>
						</br>
						<p class="boton">
							<button type="submit" class="btn btn-primary btnvalidar" name="validar" value="validar">Insertar</button> <!-- BOTON VALIDAR -->
						</p>
					</fieldset>
				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
		<!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
	</section>
	<?php
	if (isset($_GET['error'])) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['error'] . '</div>'; // MENSAJE MODAL ERROR
	if (isset($_GET['mensaje'])) echo '<div id="dialog-message" title="Exitoso"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['mensaje'] . '</div>'; // MENSAJE MODAL EXITOSO
	?>
	<div id="dialog2" title="Historico de Llamadas" style="display:none"></div>
</body>

</html>