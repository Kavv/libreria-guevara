<?php
// LA ACCION PRINCIPAL ES ESTA PAGINA ES EL MANEJO DE LLAMADAS PARA EL CALL CENTER
$r = '../../';
//INCLUIR SESION Y CONECCION
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

$iden = $_GET['iden'];

$qrynomina = $db->query("SELECT * FROM nomina LEFT JOIN usuarios ON usuid = nominausuario LEFT JOIN empresas ON empid = nominaempresa where nominaid = " . $iden . "; ");
$rownomina = $qrynomina->fetch(PDO::FETCH_ASSOC);
$resultados = $qrynomina->rowCount();

if (!empty($rownomina['usunombre'])) {
	$nombre = $rownomina['usunombre'];
	$empresa = $rownomina['empnombre'];
	$cedula = $rownomina['usuid'];
} else {
	$nombre = $rownomina['nominanombreusuario'];
	$empresa = $rownomina['empnombre'];
	$cedula = $rownomina['nominausuario'];
}


if (isset($_POST['validar'])) {

	$pucservicios = $_POST['pucservicios'];
	$debitoservicios = $_POST['debitoservicios'];
	$creditoservicios = $_POST['creditoservicios'];

	$pucretencionfte = $_POST['pucretencionfte'];
	$debitoretencionfte = $_POST['debitoretencionfte'];
	$creditoretencionfte = $_POST['creditoretencionfte'];

	$pucretencionica = $_POST['pucretencionica'];
	$debitoretencionica = $_POST['debitoretencionica'];
	$creditoretencionica = $_POST['creditoretencionica'];

	$pucretencioniva = $_POST['pucretencioniva'];
	$debitoretencioniva = $_POST['debitoretencioniva'];
	$creditoretencioniva = $_POST['creditoretencioniva'];

	$nombredescuento1 = trim(strtoupper($_POST['tipodescuento1']));
	$pucdescuento1 = $_POST['pucdescuento1'];
	$debitodescuento1 = $_POST['debitodescuento1'];
	$creditodescuento1 = $_POST['creditodescuento1'];

	$nombredescuento2 = trim(strtoupper($_POST['tipodescuento2']));
	$pucdescuento2 = $_POST['pucdescuento2'];
	$debitodescuento2 = $_POST['debitodescuento2'];
	$creditodescuento2 = $_POST['creditodescuento2'];

	$puctotal = $_POST['puctotal'];
	$debitototal = $_POST['debitototal'];
	$creditototal = $_POST['creditototal'];

	$qry = $db->query(" INSERT INTO docequivalente (docequinomina, docequipucservicios, docequidebitoservicios, docequicreditoservicios, docequipucretencionfte, docequidebitoretencionfte, docequicreditoretencionfte, docequipucretencionica, docequidebitoretencionica, docequicreditoretencionica, docequipucretencioniva, docequidebitoretencioniva, docequicreditoretencioniva, docequinombredescuento1, docequipucdescuento1, docequidebitodescuento1, docequicreditodescuento1, docequinombredescuento2, docequipucdescuento2, docequidebitodescuento2, docequicreditodescuento2, docequipuctotal, docequidebitototal, docequicreditototal) VALUES ('$iden', '$pucservicios', '$debitoservicios', '$creditoservicios', '$pucretencionfte', '$debitoretencionfte', '$creditoretencionfte', '$pucretencionica', '$debitoretencionica', '$creditoretencionica', '$pucretencioniva', '$debitoretencioniva', '$creditoretencioniva', '$nombredescuento1', '$pucdescuento1', '$debitodescuento1', '$creditodescuento1', '$nombredescuento2' ,'$pucdescuento2', '$debitodescuento2', '$creditodescuento2', '$puctotal', '$debitototal', '$creditototal');");

	if ($qry) {

		$valorservicios = $_POST['valorservicios'];
		$pagoaportes = $_POST['pagoaportes'];
		$totalbaseretencion = $_POST['totalbaseretencion'];

		if (empty($valorservicios)) {
			$valorservicios = 0;
		}
		if (empty($pagoaportes)) {
			$pagoaportes = 0;
		}
		if (empty($totalbaseretencion)) {
			$totalbaseretencion = 0;
		}

		$qry2 = $db->query("INSERT INTO docequivalente2 (docequi2nomina, docequi2valorservicios, docequi2menospagoaportes, docequi2totalbaseretencion) VALUES ('$iden', '$valorservicios', '$pagoaportes', '$totalbaseretencion'); ");

		if ($qry2) {

			$abonos1 = strtoupper(trim($_POST['abonos1']));
			$valorabonos1 = $_POST['valorabonos1'];
			$abonos2 = strtoupper(trim($_POST['abonos2']));
			$valorabonos2 = $_POST['valorabonos2'];
			$abonos3 = strtoupper(trim($_POST['abonos3']));
			$valorabonos3 = $_POST['valorabonos3'];
			$totalabonos = $_POST['totalabonos'];

			if (empty($abonos1) and empty($valorabonos1)) {
				$abonos1 = 'NONE';
				$valorabonos1 = 0;
			}
			if (empty($abonos2) and empty($valorabonos2)) {
				$abonos2 = 'NONE';
				$valorabonos2 = 0;
			}
			if (empty($abonos3) and empty($valorabonos3)) {
				$abonos3 = 'NONE';
				$valorabonos3 = 0;
			}
			if (empty($totalabonos)) {
				$totalabonos = 0;
			}

			$qry3 = $db->query(" INSERT INTO docequivalente3 (docequi3nomina, docequi3abonos1, docequi3valorabonos1, docequi3abonos2, docequi3valorabonos2, docequi3abonos3, docequi3valorabonos3, docequi3totalabonos) VALUES ('$iden', '$abonos1', '$valorabonos1', '$abonos2', '$valorabonos2', '$abonos3', '$valorabonos3', '$totalabonos');");

			if ($qry3) {
				header('Location:finalizardocequi.php?iden=' . $iden . '');
			} else {
				$error = "No se pudo ingresar la informacion del formulario numero tres por favor intente nuevamente o contacte con el encargado del portal.";
				header('Location:inserdocequi3.php?error=' . $error . '');
			}
		} else {
			$error = "No se pudo ingresar la informacion del formulario numero dos por favor intente nuevamente o contacte con el encargado del portal.";
			header('Location:inserdocequi3.php?error=' . $error . '');
		}
	} else {
		$error = "No se pudo ingresar la informacion del formulario numero uno por favor intente nuevamente o contacte con el encargado del portal.";
		header('Location:inserdocequi3.php?error=' . $error . '');
	}
}
?>
<!doctype html>
<html>

<head>

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<script type="text/javascript">
		$(document).ready(function() {


			$('#fecha1').datepicker({ // VALIDACION DE LA FECHA UNO
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
				maxDate: '+50D',
				onClose: function(selectedDate) {
					$('#fecha2').datepicker('option', 'minDate', selectedDate);
				}
			})
			$('#fecha2').datepicker({ // VALIDACION DE LA FECHA DOS
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
				maxDate: '+50D',
				onClose: function(selectedDate) {
					$('#fecha1').datepicker('option', 'maxDate', selectedDate);
				}
			}).keypress(function(event) {
				event.preventDefault()
			});


			$('#departamento').change(function(event) {
				var id1 = $('#departamento').find(':selected').val();
				$('#ciudad').load('<?php echo $r ?>incluir/carga/ciudades.php?id1=' + id1);
			});


			$('#creditoservicios, #creditoretencionfte, #creditoretencionica, #creditoretencioniva, #creditodescuento1, #creditodescuento2').change(function(event) {
				var creditototal = Math.ceil(parseFloat($('#creditoservicios').val()) + parseFloat($('#creditoretencionfte').val()) + parseFloat($('#creditoretencionica').val()) + parseFloat($('#creditoretencioniva').val()) + parseFloat($('#creditodescuento1').val()) + parseFloat($('#creditodescuento2').val()));
				var creditototal = creditototal;
				$('#creditototal').val(creditototal);
			});
			$('#debitoservicios, #debitoretencionfte, #debitoretencionica, #debitoretencioniva, #debitodescuento1, #debitodescuento2').change(function(event) {
				var debitototal = Math.ceil(parseFloat($('#debitoservicios').val()) + parseFloat($('#debitoretencionfte').val()) + parseFloat($('#debitoretencionica').val()) + parseFloat($('#debitoretencioniva').val()) + parseFloat($('#debitodescuento1').val()) + parseFloat($('#debitodescuento2').val()));
				var debitototal = debitototal;
				$('#debitototal').val(debitototal);
			});
			$('#valorservicios, #pagoaportes').change(function(event) {
				var totalbaseretencion = Math.ceil(parseFloat($('#valorservicios').val()) - parseFloat($('#pagoaportes').val()));
				var totalbaseretencion = totalbaseretencion;
				$('#totalbaseretencion').val(totalbaseretencion);
			});
			$('#valorabonos1, #valorabonos2, #valorabonos3').change(function(event) {
				var totalabonos = Math.ceil(parseFloat($('#valorabonos1').val()) + parseFloat($('#valorabonos2').val()) + parseFloat($('#valorabonos3').val()));
				var totalabonos = totalabonos;
				$('#totalabonos').val(totalabonos);
			});
		});
	</script>

</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
		<?php require($r . 'incluir/src/menu.php') ?>
		<!-- INCLUIMOS MENU PRINCIPAL -->
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Tesoreria</a>
				<div class="mapa_div"></div><a class="current">Insertar Documento Equivalente</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="inserdocequi3.php?iden=<?php echo $iden; ?>" method="post">
					<!-- ENVIO FORMULARIO POR POST -->
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">Insertar Documento Equivalente - Formulario 3</legend>
						<p>
							<label for="nomusuario">Persona Natural </label>
							<input style="width:350px;" name="nomusuario" type="text" value="<?php echo $nombre ?>" readonly />
						</p>
						<p>
							<label for="identificacionpersona">Identificacion Persona</label>
							<input style="width:350px;" name="identificacionpersona" type="text" value="<?php echo $cedula ?>" readonly />
						</p>
						<p>
							<label for="empresausuario">Empresa</label>
							<input style="width:350px;" name="empresausuario" type="text" value="<?php echo $empresa ?>" readonly />
						</p>
						</br>
						<label>-----PUC-----------------TIPO---------------DEBITO--------CREDITO-----</label>
						<div class="row mx-1">
							<input type="text" style="width:105px" name="pucservicios" id="pucservicios" class="col-md-3 not-w validate[custom[onlyNumberSp]] text-input" />
							<input type='text' name='tiposervicios' value="SERVICIOS" class='col-md-3 not-w text-input' readonly />
							<input type='text' name='debitoservicios' id='debitoservicios' class='col-md-3 not-w valor validate[required] text-input' />
							<input type='text' name='creditoservicios' id='creditoservicios' class='col-md-3 not-w valor validate[required] text-input' />
						</div>

						<div class="row mx-1">
							<input type="text" style="width:105px" name="pucretencionfte" id="pucretencionfte" class="col-md-3 not-w validate[custom[onlyNumberSp]] text-input" />
							<input type='text' name='tiporetencionfte' value="RETENCION FUENTE" class='col-md-3 not-w text-input' readonly />
							<input type='text' name='debitoretencionfte' id='debitoretencionfte' class='col-md-3 not-w valor validate[required] text-input' />
							<input type='text' name='creditoretencionfte' id='creditoretencionfte' class='col-md-3 not-w valor validate[required] text-input' />
						</div>

						<div class="row mx-1">
							<input type="text" style="width:105px" name="pucretencionica" id="pucretencionica" class="col-md-3 not-w validate[custom[onlyNumberSp]] text-input" />
							<input type='text' name='tiporetencionica' value="RETENCION ICA" class='col-md-3 not-w text-input' readonly />
							<input type='text' name='debitoretencionica' id='debitoretencionica' class='col-md-3 not-w valor validate[required] text-input' />
							<input type='text' name='creditoretencionica' id='creditoretencionica' class='col-md-3 not-w valor validate[required] text-input' />
						</div>

						<div class="row mx-1">
							<input type="text" style="width:105px" name="pucretencioniva" id="pucretencioniva" class="col-md-3 not-w validate[custom[onlyNumberSp]] text-input" />
							<input type='text' name='tiporetencioniva' value="RETENCION IVA" class='col-md-3 not-w text-input' readonly />
							<input type='text' name='debitoretencioniva' id='debitoretencioniva' class='col-md-3 not-w valor validate[required] text-input' />
							<input type='text' name='creditoretencioniva' id='creditoretencioniva' class='col-md-3 not-w valor validate[required] text-input' />
						</div>

						<div class="row mx-1">
							<input type="text" style="width:105px" name="pucdescuento1" id="pucdescuento1" class="col-md-3 not-w validate[custom[onlyNumberSp]] text-input" />
							<input type='text' name='tipodescuento1' placeholder="DESCUENTO 1" class='col-md-3 not-w text-input' />
							<input type='text' name='debitodescuento1' id='debitodescuento1' class='col-md-3 not-w valor validate[required] text-input' />
							<input type='text' name='creditodescuento1' id='creditodescuento1' class='col-md-3 not-w valor validate[required] text-input' />
						</div>

						<div class="row mx-1">
							<input type="text" style="width:105px" name="pucdescuento2" id="pucdescuento2" class="col-md-3 not-w validate[custom[onlyNumberSp]] text-input" />
							<input type='text' name='tipodescuento2' placeholder="DESCUENTO 2" class='col-md-3 not-w text-input' />
							<input type='text' name='debitodescuento2' id='debitodescuento2' class='col-md-3 not-w valor validate[required] text-input' />
							<input type='text' name='creditodescuento2' id='creditodescuento2' class='col-md-3 not-w valor validate[required] text-input' />
						</div>

						<div class="row mx-1">
							<input type="text" style="width:105px" name="puctotal" id="puctotal" class="col-md-3 not-w  text-input" value='---' readonly />
							<input type='text' name='tipototal' value="TOTAL" class='col-md-3 not-w text-input' readonly />
							<input type='text' name='debitototal' id='debitototal' class='col-md-3 not-w valor validate[required] text-input' readonly />
							<input type='text' name='creditototal' id='creditototal' class='col-md-3 not-w valor validate[required] text-input' readonly />
						</div>
						<br><br>
						<hr>
						</hr>
						<br><br>
						<p>
							<input type='text' value="VALOR SERVICIOS" class='text-input' readonly />
							<input type='text' name='valorservicios' id='valorservicios' class='valor validate[custom[onlyNumberSp]] text-input' />
						</p>
						<p>
							<input type='text' value="MENOS PAGO APORTES" class='text-input' readonly />
							<input type='text' name='pagoaportes' id='pagoaportes' class='valor validate[custom[onlyNumberSp]] text-input' />
						</p>
						<p>
							<input type='text' value="TOTAL BASE RETENCION" class='text-input' readonly />
							<input type='text' name='totalbaseretencion' id='totalbaseretencion' class='valor validate[custom[onlyNumberSp]] text-input' readonly />
						</p>
						</p>
						<br><br>
						<hr>
						</hr>
						<br><br>
						<p>
							<input type='text' placeholder="ABONOS" name="abonos1" class='text-input' />
							<input type='text' name='valorabonos1' id='valorabonos1' class='valor validate[custom[onlyNumberSp]] text-input' />
						</p>
						<p>
							<input type='text' placeholder="ABONOS" name="abonos2" class='text-input' />
							<input type='text' name='valorabonos2' id='valorabonos2' class='valor validate[custom[onlyNumberSp]] text-input' />
						</p>
						<p>
							<input type='text' placeholder="ABONOS" name="abonos3" class='text-input' />
							<input type='text' name='valorabonos3' id='valorabonos3' class='valor validate[custom[onlyNumberSp]] text-input' />
						</p>
						<p>
							<input type='text' value="TOTAL ABONOS" class='text-input' readonly />
							<input type='text' name='totalabonos' id='totalabonos' class='valor validate[custom[onlyNumberSp]] text-input' readonly />
						</p>
						<br><br>
						<p class="boton">
							<button type="submit" class="btn btn-primary btnvalidar" name="validar" value="validar">Generar PDF</button> <!-- BOTON VALIDAR -->
						</p>
					</fieldset>
				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
		<!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
	</section>
	<?php
	if (isset($_GET['error'])) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['error'] . '</div>'; // MENSAJE MODAL ERROR
	if (isset($_GET['mensaje'])) echo '<div id="dialog-message" title="Exitoso"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['mensaje'] . '</div>'; // MENSAJE MODAL EXITOSO
	?>
	<div id="dialog2" title="Historico de Llamadas" style="display:none"></div>
</body>

</html>