<?php
$r = '../../';
//INCLUIR SESION Y CONECCION
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
?>
<!doctype html>
<html>
<head>

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#form').validationEngine({ //VALIDACION DE CAMPOS REQUERIDOS EN FORMULARIO
		showOneMessage:true,
		onValidationComplete: function(form, status){
        	if(status) {               
            	carga();
            	return true;
			}
		}
	});
	$('#fecha1').datepicker({ // VALIDACION DE LA FECHA UNO
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		onClose: function(selectedDate) {
			$('#fecha2').datepicker('option', 'minDate', selectedDate);
		}
	}).keypress(function(event) { event.preventDefault() });
	$('#fecha2').datepicker({ // VALIDACION DE LA FECHA DOS
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		onClose: function(selectedDate) {
			$('#fecha1').datepicker('option', 'maxDate', selectedDate);
		}
	}).keypress(function(event) { event.preventDefault() });
	
	$('.btnconsulta').button({ icons: { primary: 'ui-icon ui-icon-search' }}); //ACCION DE LA CLASE EN EL BOTON DE CONSULTA
	$('#dialog-message').dialog({ //CARACTERISTICAS DE LA VENTANA MODAL 
		height: 80,
		width: 'auto',
		modal: true
	});
});
</script>
<!-- ESTILOS EMBEBIDOS PARA MANEJAR FORMULARIO INDEPENDIENTEMENTE -->
<style type="text/css">
#form form{ width:660px; }
#form fieldset{ padding:10px; display:block; width:660px; margin:20px auto }
#form legend{ font-weight: bold; margin-left:5px; padding:5px }
#form label{ display:inline-block; width:80px; text-align:right; float:left; margin:0.3em 2% 0 0 }
#form p{ margin:5px 0 }
</style>
</head>
<body>
<?php require($r.'incluir/src/login.php') ?> <!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
<section id="principal">
<?php require($r.'incluir/src/cabeza.php') ?> <!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
<?php require($r.'incluir/src/menu.php') ?> <!-- INCLUIMOS MENU PRINCIPAL -->
<article id="cuerpo">
<article class="mapa">
<a href="#">Principal</a><div class="mapa_div"></div><a href="#">Tesoreria</a><div class="mapa_div"></div><a href="#">Costos y Gastos</a><div class="mapa_div"></div><a class="current">Consultar pagos masivos</a>
</article>
<article id="contenido">
<form id="form" name="form" action="pagomasivo_cyg.php" method="post"> <!-- ENVIO FORMULARIO POR POST A LISTAR.PHP -->
<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
<legend class="ui-widget ui-widget-header ui-corner-all">Consultar Pagos masivos</legend>
	
	<p>
	<label>Fechas de pago</label>
	<input type="text" name="fecha1" id="fecha1" class="text-input fecha" /> <span>  -  </span> <input type="text" name="fecha2" id="fecha2" class="text-input fecha" />
	</p>
	<p>
		<label for="proveedor">Proveedor:</label>
		<select name="proveedor" class="text-input">
		<option value="">SELECCIONE</option>
		<?php
		$qry = $db->query("SELECT * FROM proveedores_teso ORDER BY protesonombre");
		while($row = $qry->fetch(PDO::FETCH_ASSOC))
			echo '<option value='.$row['protesoid'].'>'.$row['protesonombre'].'</option>';
		?>
		</select>
	</p>
	<p>
		<label for="empresa">Empresa:</label>
		<select name="empresa" class="text-input">
		<option value="">SELECCIONE</option>
		<?php
		$qry = $db->query("SELECT * FROM empresas ORDER BY empnombre");
		while($row = $qry->fetch(PDO::FETCH_ASSOC))
			echo '<option value='.$row['empid'].'>'.$row['empnombre'].'</option>';
		?>
		</select>
	</p>
	<!--
	<p>
		<label for="Estado">Estado:</label>
		<select name="estado" class="text-input">
		<option value="">SELECCIONE</option>
		<option value="1">PAGADO</option>
		<option value="0">POR PAGAR</option>
		<option value="2">PAGOS CONFIRMADOS</option>
		</select>
	</p>
	-->
	</br>
<p class="boton">
<button type="submit" class="btn btn-primary btnconsulta" name="consultar" value="consultar">Consultar</button><!-- BOTON CONSULTAR -->
</p>
</fieldset>
</form>
</article>
</article>
<?php require($r.'incluir/src/pie.php') ?>  <!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
</section>
<?php
if($_GET['error']) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>'.$_GET['error'].'</div>'; // MENSAJE MODAL ERROR
?>
</body>
</html>