<?php
	// LA ACCION PRINCIPAL ES ESTA PAGINA ES EL MANEJO DE LLAMADAS PARA EL CALL CENTER
	$r = '../../';
	//INCLUIR SESION Y CONECCION
	require($r . 'incluir/session.php');
	require($r . 'incluir/connection.php');


	if (isset($_POST['validar'])) {
		$hoy = date('Y/m/d');
		$gestionado = $_SESSION['id'];

		if (isset($_POST['usuario'])) 
		{
			$usuario = $_POST['usuario'];
			$empresa1 = $_POST['empresa1'];
			$rowusu = $db->query("SELECT * FROM usuarios WHERE usuid = '" . $usuario . "';")->fetch(PDO::FETCH_ASSOC);
		

			if (empty($usuario) || empty($empresa1)) {
				$error = "Para Continuar es necesario que especifique la cedula, el nombre y la empresa";
				header('Location:inserdocequi.php?error=' . $error . '');
				exit();
			}
			$qry = $db->query("INSERT INTO nomina (nominatipo, nominausuario, nominaempresa, nominafecha, nominagestionado) VALUES ('DOC EQUIVALENTE', '$usuario', '$empresa1', '$hoy', '$gestionado'); ");
		
		} 
		elseif (isset($_POST['cedula'])) 
		{
			$cedula = $_POST['cedula'];
			$nombre = trim($_POST['nombre']);
			$empresa = $_POST['empresa'];

			if (empty($cedula) || empty($nombre) || empty($empresa)) {
				$error = "Para Continuar es necesario que especifique la cedula, el nombre y la empresa";
				header('Location:inserdocequi.php?error=' . $error . '');
				exit();
			}

			$qry = $db->query("INSERT INTO nomina (nominatipo, nominausuario, nominanombreusuario, nominaempresa, nominafecha, nominagestionado) VALUES ('DOC EQUIVALENTE', '$cedula', '$nombre', '$empresa', '$hoy', '$gestionado'); ");
		}

		if ($qry) {
			$iden = $db->lastInsertId();
			header('Location:inserdocequi2.php?iden=' . $iden . '');
		} else {
			$error = "No se pudo ingresar por favor intente nuevamente o contacte con el encargado del portal. ";
			header('Location:inserdocequi.php?error=' . $error . '');
		}
	}
?>
<!doctype html>
<html lang="es">

<head>
    <title>AGREGAR DOCUMENTO EQUIVALENTE</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

	<script type="text/javascript">
		$(document).ready(function() {
			$("#usuario").selectpicker();


			$("#usuario").change(function(){
				user_data($(this).val());
			});

			function user_data(user)
			{
				$("#empresa1").val("");
				$("#emp-asignada").text("");
				$.get("userdata.php?user="+user, function(res){
					var usuario = JSON.parse(res);
					if(usuario != false)
					{
						if(usuario.empnombre != "")
						{
							$("#empresa1").val(usuario.empid);
							$("#emp-asignada").text(usuario.empnombre);
						}
						else
						{
							$("#empresa1").val("");
							$("#emp-asignada").text("NINGUNA");
						}
					}
					else
					{
						alert("El usuario no tiene asignada una empresa, verifique los datos del usuario (El proceso no se llevara a cabo)");
					}
				});
			}


		});
		
	</script>

</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
		<?php require($r . 'incluir/src/menu.php') ?>
		<!-- INCLUIMOS MENU PRINCIPAL -->
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Tesoreria</a>
				<div class="mapa_div"></div><a class="current">Insertar Documento Equivalente</a>
			</article>
			<article id="contenido">
				<div class="d-flex justify-content-center">

					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
						<form id="form" name="form" action="inserdocequi.php" method="post">
							<legend class="ui-widget ui-widget-header ui-corner-all">Insertar Documento Equivalente - Formulario 1</legend>
							</br>
							<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12">
								<legend class="ui-widget ui-widget-header ui-corner-all">Usuario Registrado</legend>
								<p>
									<label for="usuario">Usuario:</label>
									<select id="usuario" name="usuario"  class="selectpicker" required
									data-live-search="true" title="Seleccione un usuario" data-width="100%">
										<option value="">SELECCIONE</option>
										<?php
											$qry = $db->query("SELECT * FROM usuarios ORDER BY usunombre ");
											while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
												echo '<option value="' . $row2["usuid"] . '">' . $row2["usuid"] .' / '.  $row2['usunombre'] . '</option>';
											}
										?>
									</select>
									<em style="font-size:14px">Empresa asignada:<span id="emp-asignada" style="color: #f00000; font-weight: bold;"></span></em>
								</p>
								<p>
									<label for="empresa1">Empresa del documento:</label> <!-- CAMPO EMPRESA -->
									<select id="empresa1" name="empresa1">
										<?php
										$qry = $db->query("SELECT * FROM empresas ORDER BY empnombre");
										while ($row = $qry->fetch(PDO::FETCH_ASSOC))
											echo '<option value=' . $row['empid'] . '>' . $row['empnombre'] . '</option>';
										?>
									</select>
								</p>
								<p class="boton mt-3">
									<button type="submit" class="btn btn-primary btnvalidar" name="validar" value="validar">Continuar</button> <!-- BOTON VALIDAR -->
								</p>
							</fieldset>
						</form>
						<form id="form" name="form" action="inserdocequi.php" method="post">
							<br><br>
							<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12">
								<legend class="ui-widget ui-widget-header ui-corner-all">Usuario NO Registrado</legend>
								<p>
									<label for="cedula">Cedula:</label> <!-- CAMPO CEDULA-->
									<input type="text" name="cedula" id="id" class="id text-input" required>
								</p>
								<p>
									<label for="nombre">Nombre:</label> <!-- CAMPO NOMBRE -->
									<input type="text" id="nombre" name="nombre" class="nombre text-input" required title="Digite el nombre del usuario" />
								</p>
								<p>
									<label for="empresa">Empresa del documento:</label> <!-- CAMPO EMPRESA -->
									<select name="empresa" required>
										<?php
										$qry = $db->query("SELECT * FROM empresas ORDER BY empnombre");
										while ($row = $qry->fetch(PDO::FETCH_ASSOC))
											echo '<option value=' . $row['empid'] . '>' . $row['empnombre'] . '</option>';
										?>
									</select>
								</p>
								<p class="boton mt-3">
									<button type="submit" class="btn btn-primary btnvalidar" name="validar" value="validar">Continuar</button> <!-- BOTON VALIDAR -->
								</p>
							</fieldset>
						</form>
					</fieldset>
				</div>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
		<!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
	</section>
	<?php
	if (isset($_GET['error'])) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['error'] . '</div>'; // MENSAJE MODAL ERROR
	if (isset($_GET['mensaje'])) echo '<div id="dialog-message" title="Exitoso"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['mensaje'] . '</div>'; // MENSAJE MODAL EXITOSO
	?>
	<div id="dialog2" title="Historico de Llamadas" style="display:none"></div>
</body>

</html>