<?php
$r = '../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
require($r . 'incluir/funciones.php');
if (isset($_GET['mensaje']))
	$mensaje = $_GET['mensaje'];
if (isset($_GET['error']))
	$error = $_GET['error'];

if (isset($_POST['consultar'])) {
	$fecha1 = $_POST['fecha1'];
	$fecha2 = $_POST['fecha2'];
	$proveedor = $_POST['proveedor'];
	$empresa = $_POST['empresa'];
	$numcheque = $_POST['numcheque'];

} elseif (isset($_GET['fecha1'])) {
	$fecha1 = $_GET['fecha1'];
	$fecha2 = $_GET['fecha2'];
	$proveedor = $_GET['proveedor'];
	$empresa = $_GET['empresa'];
	$numcheque = $_GET['numcheque'];
}


$filtro = 'fecha1=' . $fecha1 . '&fecha2=' . $fecha2 . '&proveedor=' . $proveedor . '&empresa=' . $empresa . '&numcheque=' . $numcheque;
$con = 'SELECT tesoreria.*, empresas.empnombre, proveedores_teso.protesonombre FROM tesoreria
LEFT JOIN empresas on empid = tesoempresa
LEFT JOIN proveedores_teso on tesoproveedor = protesoid';
$ord = ' ORDER BY tesofecha DESC'; // Ordenar la Consulta


    /* Los parametros de la consulta sql se genera dinamicamente 
    en base a los datos recibidos para delimitar los resultados */
    $parameters = [];
	array_push($parameters, "tesomediopago = 'Cheque' AND tesobanco is not null " );
	array_push($parameters, "tesovalid != 3" );
    if($fecha1 != "")
        array_push($parameters, "tesofecha between '$fecha1' AND '$fecha2'" );
    if($proveedor != "")
        array_push($parameters, "tesoproveedor LIKE '%$proveedor%'" );
    if($empresa != "")
        array_push($parameters, "tesoempresa LIKE '%$empresa%'" );
    if($numcheque != "" && $asesor != "TODOS")
		array_push($parameters, "tesocheque = '$numcheque'" );
		
    // Consulta base
    $sql = $con;
    foreach ($parameters as $index => $parameter) {
        // Se agregan los parametros del WHERE
        if($index == 0)
            $sql .= " WHERE " . $parameter;
        else
            $sql .= " AND " . $parameter;
    }
    // Se completa la consulta
	$sql .= $ord;

$qry = $db->query($sql);

?>
<!doctype html>
<html lang="es">

<head>
    <title>LISTAR CHEQUES</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>

	<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
	<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>

	<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>
	<script>
		$(document).ready(function() {
			$('#fechaone').datepicker({ // VALIDACION DE LA FECHA UNO
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
				onClose: function(selectedDate) {
					$('#fechatwo').datepicker('option', 'minDate', selectedDate);
				}
			}).keypress(function(event) {
				event.preventDefault()
			});
			$('#fechatwo').datepicker({ // VALIDACION DE LA FECHA DOS
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
				onClose: function(selectedDate) {
					$('#fechaone').datepicker('option', 'maxDate', selectedDate);
				}
			}).keypress(function(event) {
				event.preventDefault()
			});

			$('#tabla').dataTable({ //QRY PARA MANEJO Y VISUALIZACION DE TABLAS
				'bJQueryUI': true,
				'sPaginationType': "full_numbers",
				'oLanguage': {
					'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
				},
				'aoColumnDefs': [{
					'bSortable': false,
					'aTargets': [0, 3, 4, 5, 6, 8, 9]
				}],

			});

			$('.confirmar').click(function(e) { // VENTANA MODAL PARA CONFIRMAR ACCIONES
				e.preventDefault();
				var targetUrl = $(this).attr("href");
				var $dialog_link_follow_confirm = $('<div></div>').html('<p><span class="ui-icon ui-icon-alet" style="float:left; margin:2px 7px 7px 0;"></span>Esta seguro que desea pagar?</p>').
				dialog({
					title: 'Porfavor confirmar',
					buttons: {
						'Si': function() {
							carga();
							window.location.href = targetUrl;
						},
						'No': function() {
							$(this).dialog("close");
						}
					},
					modal: true,
					width: 'auto',
					height: 'auto'
				});
				$dialog_link_follow_confirm.dialog('open');
			});

		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Tesoreria</a>
				<div class="mapa_div"></div><a href="#">Saldos de Banco</a>
				<div class="mapa_div"></div><a class="current">Listar</a>
			</article>
			<article id="contenido">
				<h2>Listado de Cheques</h2>
				<div class="reporte">
					<a href="pdfcheques_sb.php?<?php echo $filtro . "&generar=gnerar" ?>">
						<img src="<?php echo $r ?>imagenes/iconos/pdf.png" title="pdf" />
					</a> 
				</div>

				<table id="tabla">
					<thead>
						<tr>
							<th>ID</th>
							<th>Num Factura</th>
							<th align="center">Fecha</th>
							<th align="center">Empresa</th>
							<th align="center">Concepto</th>
							<th align="center">Valor</th>
							<th align="center">Proveedor</th>
							<th align="center" title="Numero de cheque">N. Cheque</th>
							<th align="center" title="Medio de Pago">M. Pago</th>
							<th align="center" title="Comentarios Adicionales">C. Adicionales</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$vlr1 = 0;
						while ($row = $qry->fetch(PDO::FETCH_ASSOC)) { //INFORMACION DE LA CONSULTA

							$empresa = $row['empnombre'];

							$proveedor_name = $row['protesonombre'];

						?>
							<tr>
								<td><?php echo $row['tesoid'] ?></td>
								<td><?php echo $row['tesonumfactura'] ?></td>
								<td align="center"><?php echo $row['tesofecha'] ?></td>
								<td align="center"><?php echo $empresa ?></td>
								<td align="center"><?php echo $row['tesoconcepto'] ?></td>
								<td align="center"><?php echo number_format($row['tesovalor'], 2) ?></td>
								<td align="center"><?php echo $proveedor_name ?></td>
								<td align="center"><?php echo $row['tesocheque'] ?></td>
								<td align="center"><?php echo $row['tesomediopagodetalle'] ?></td>
								<td align="center"><?php echo $row['tesoadicional'] ?></td>
							</tr>
						<?php
							$vlr1 = $vlr1 + $row['tesovalor'];
						}
						?>
					</tbody>
					<tfoot>
						<tr>
							<td colspan="4"></td>
							<td colspan="1" align="center" bgcolor="#D1CFCF">Valor Total</td>
							<td align="center" bgcolor="#D1CFCF"><?php echo number_format($vlr1, 2) ?></td>
							<td colspan="3"></td>
						</tr>
					</tfoot>
				</table>
				<center>
					<br><br><br>

					<br><br>
				</center>
				<p class="boton">
					<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href='consultar_cheques_sb.php'">Atras</button>
				</p>


			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<?php
	if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>';
	elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>';
	?>
	<div id="modal" style="display:none"></div>
	<div id="dialog2" style="display:none"></div>
</body>

</html>>