<?php
$r = '../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

$iden = $_GET['iden'];
$qrynomina = $db->query("SELECT * FROM nomina LEFT JOIN usuarios ON usuid = nominausuario LEFT JOIN empresas ON empid = nominaempresa where nominaid = " . $iden . "; ");
$rownomina = $qrynomina->fetch(PDO::FETCH_ASSOC);

if (!empty($rownomina['usunombre'])) {
	$nombre = $rownomina['usunombre'];
	$empresa = $rownomina['empnombre'];
	$cedula = $rownomina['usuid'];
} else {
	$nombre = $rownomina['nominanombreusuario'];
	$empresa = $rownomina['empnombre'];
	$cedula = $rownomina['nominausuario'];
}



if (isset($_GET['menos'])) {
	$identificador = $_GET['identificador'];
	$qry = $db->query("DELETE FROM infodocequi WHERE infoid='$identificador';");
	header('Location:inserdocequi2.php?iden=' . $iden . '');
	exit();
} elseif (isset($_POST['mas'])) {
	$descripcion = strtoupper(trim($_POST['descripcion']));
	$valor = $_POST['valor'];
	$tarifaiva = $_POST['tarifaiva'];
	$ivateorico = $_POST['ivateorico'];
	$tarifaretencion = $_POST['tarifaretencion'];
	$valorimpuesto = $_POST['valorimpuesto'];
	$qry = $db->query("INSERT INTO infodocequi (infonomina, infodescripcion, infovalor, infotarifaiva, infoivateorico, infotarifaretencion, infovalorimpuesto) VALUES ('$iden', '$descripcion', '$valor', '$tarifaiva', '$ivateorico', '$tarifaretencion', '$valorimpuesto');");
	header('Location:inserdocequi2.php?iden=' . $iden . '');
	exit();
}

?>
<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>

	<script type="text/javascript">
		$(document).ready(function() {

			$('#valor, #tarifaiva').change(function(event) {
				var ivateorico = ($('#valor').val() * $('#tarifaiva').val()) / 100;
				var ivateorico = ivateorico;
				$('#ivateorico').val(ivateorico);
			});

			$('#valor, #tarifaretencion').change(function(event) {
				var valorimpuesto = ($('#valor').val() * $('#tarifaretencion').val()) / 100;
				var valorimpuesto = valorimpuesto;
				$('#valorimpuesto').val(valorimpuesto);
			});

		});
	</script>

</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Tesoreria</a>
				<div class="mapa_div"></div><a class="current">Insertar Documento Equivalente</a>
			</article>
			<article id="contenido">

				<form id="form" name="form" action="inserdocequi2.php?<?php echo "iden=" . $iden; ?>" method="post">

					<fieldset id="field" class="ui-widget ui-widget-content ui-corner-all col-md-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">Insertar Documento Equivalente - Formulario 2 </legend>
						<br>
						<br>
						<p>
							<label for="nomusuario">Persona Natural de quien se adquiere los bienes y/o servicios</label>
							<input style="width:350px;" name="nomusuario" type="text" value="<?php echo $nombre ?>" readonly />
						</p>
						<p>
							<label for="identificacionpersona">Identificacion Persona</label>
							<input style="width:350px;" name="identificacionpersona" type="text" value="<?php echo $cedula ?>" readonly />
						</p>
						<p>
							<label for="empresausuario">Empresa</label>
							<input style="width:350px;" name="empresausuario" type="text" value="<?php echo $empresa ?>" readonly />
						</p>

						<br><br>
						<center>
							<p>
								<label>Descripcion del detalle</label>	
								<input type="text" name="descripcion" class="nombre validate[required] text-input" title="Digite la descripcion" />
								<label>Valor del detalle</label>
								<input type='text' name='valor' id='valor' class='valor  text-input validate[required]' />
								<label>Tarifa IVA</label>
								<input type='text' name='tarifaiva' id='tarifaiva' class='porcentaje text-input validate[required]' value="0" />
								<label>IVA Teorico</label>
								<input type='text' name='ivateorico' id='ivateorico' class='valor  text-input' readonly />
								<label>Tarifa Retención</label>
								<input type='text' name='tarifaretencion' id='tarifaretencion' class='porcentaje text-input validate[required]'  value="0"/>
								<label>Valor Retencion</label>
								<input type='text' name='valorimpuesto' id='valorimpuesto' class='valor  text-input' readonly />
								<button type="submit" class="btn btn-success col-md-12 btnmas" name="mas" value="mas">Agregar +</button>
							</p>
						</center>
						<br>
						<br>
						<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12">
							<legend class="ui-widget ui-widget-header ui-corner-all">Detalle de informacion</legend>
							<?php
							$qry = $db->query("SELECT * FROM infodocequi WHERE infonomina = '$iden' order by infofechausuario ");
							$numresul = $qry->rowCount();
							while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
							?>
								<p align="center">
									<label>Descripcion del detalle</label>	
									<input value="<?php echo $row2['infodescripcion'] ?>" type="text" class="nombre validate[required] text-input" title="Digite la descripcion" readonly />
									<label>Valor del detalle</label>
									<input value="<?php echo $row2['infovalor'] ?>" type='text' class='valor  text-input' readonly />
									<label>Tarifa IVA</label>
									<input value="<?php echo $row2['infotarifaiva'] . "%" ?>"  type='text' class='text-input' readonly />
									<label>IVA Teorico</label>
									<input value="<?php echo $row2['infoivateorico'] ?>" type='text' class='valor  text-input' readonly />
									<label>Tarifa Retención</label>
									<input value="<?php echo $row2['infotarifaretencion'] . "%" ?>"  type='text' class='text-input' readonly />
									<label>Valor Retencion</label>
									<input value="<?php echo $row2['infovalorimpuesto'] ?>" type='text' class='valor  text-input' readonly />
									<button type="button" class="btn btn-danger col-md-12 btnmenos" onclick="carga(); location.href = 'inserdocequi2.php?<?php echo 'iden=' . $iden . '&identificador=' . $row2['infoid'] . '&menos=1' ?>'"> Quitar -</button>
								</p>
								<div class="separador"></div>
							<?php
							}
							if ($numresul == 0) {
								echo "<p>Actualmente no se ha agregado detalle a este documento equivalente.</p>";
							}
							?>
						</fieldset>
						<p class="boton">
							<button type="button" class="btn btn-primary btnvalidar" onClick="carga(); location.href='finalizardocequi.php?<?php echo "iden=" . $iden; ?>'">Continuar</button> <!-- BOTON Continuar -->
						</p>
					</fieldset>
				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<?php
	if (isset($_GET['error'])) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['error'] . '</div>';
	?>
	<div id="modal" style="display:none"></div>
	<div id="dialog2" style="display:none"></div>
</body>

</html>