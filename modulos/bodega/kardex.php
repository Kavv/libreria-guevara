<?php
$r = '../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
$producto = $_POST['producto'];
$empresa = $_POST['empresa'];
$fecha1 = $_POST['fecha1'];
$fecha2 = $_POST['fecha2'];

// Evaluar como se debe transformar aquí la consulta, sugiero agregar una segunda consulta que haga lo mismo pero para la otra tabla
$qry = $db->query("SELECT * FROM (detmovimientos INNER JOIN movinventario ON (movempresa = dmoempresa AND movprefijo = dmoprefijo AND movnumero = dmonumero)) INNER JOIN tipdocumentos ON tipid = dmoprefijo WHERE dmoempresa = '$empresa' and dmoproducto = '$producto' AND movfecha BETWEEN '$fecha1' AND '$fecha2' ORDER BY dmoid ASC");
//$qry2 = $db->query("SELECT * FROM (detmovimientos INNER JOIN movinventario ON (movempresa = dmoempresa AND movprefijo = dmoprefijo AND movnumero = dmonumero)) INNER JOIN tipdocumentos ON tipid = dmoprefijo WHERE dmoempresa = '$empresa' and dmoproducto = '$producto' AND movfecha BETWEEN '$fecha1' AND '$fecha2' ORDER BY dmoid ASC");
$row2 = $db->query("SELECT * FROM productos WHERE proid = '$producto'")->fetch(PDO::FETCH_ASSOC);
?>
<!doctype html>

<html lang="es">

<head>
    <title>DETALLE KARDEX</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">
    <meta charset="utf-8">
    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>

    <link rel="stylesheet" type="text/css"
          href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css"/>
    <script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#tabla').dataTable({
                'bFilter': false,
                'bSort': false,
                'bJQueryUI': true,
                'bStateSave': true,
                'sPaginationType': 'full_numbers',
                'oLanguage': {
                    'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
                },
            });
        });
    </script>
</head>
<body>
<?php require($r . 'incluir/src/login.php') ?>
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <?php require($r . 'incluir/src/menu.php') ?>
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a class="current">Bodega</a>
        </article>
        <article id="contenido">
            <?php
            $qryuno = $db->query("SELECT * FROM empresas where empid = '$empresa'");
            while ($rowem = $qryuno->fetch(PDO::FETCH_ASSOC)) {
                echo '<h2>Kardex de ' . $row2['pronombre'] . ' desde ' . $fecha1 . ' a ' . $fecha2 . ' , ' . $rowem['empnombre'] . '</h2>';
            }
            ?>
            <table id="tabla">
                <thead>
                <tr>
                    <th colspan="2"></th>
                    <th >Entradas</th>
                    <th >Salidas</th>
                    <th >Saldos</th>
                </tr>
                <tr>
                    <th>Fecha</th>
                    <th>Documento</th>
                    <th>Cantidad</th>
                    <th>Cantidad</th>
                    <th>Cantidad</th>
                </tr>
                </thead>
                <tbody>
                <?php
                while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
                    if ($row['tiptipo'] == 'ENTINV' || $row['tiptipo'] == 'ENTFAC')
                        echo '<tr><td align="center">' . $row['movfecha'] . '</td><td align="center">' . $row['dmoprefijo'] . '-' . $row['dmonumero'] . '</td><td align="center">' . $row['dmocantidad'] . '</td><td></td><td align="center">' . $row['dmoinventario'] . '</td></tr>';
                    elseif ($row['tiptipo'] == 'SALINV' || $row['tiptipo'] == 'SALFAC')
                        echo '<tr><td align="center">' . $row['movfecha'] . '</td><td align="center">' . $row['dmoprefijo'] . '-' . $row['dmonumero'] . '</td><td></td><td align="center">' . $row['dmocantidad'] . '</td><td align="center">' . $row['dmoinventario'] . '</td></tr>';
                }
                ?>
                </tbody>
            </table>
            <p class="boton">

                <button type="button" class="btn btn-outline-primary btnatras"
                        onClick="carga(); location.href='conkardex.php'">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-left-square-fill"
                         fill="currentColor"
                         xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                              d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm9.5 8.5a.5.5 0 0 0 0-1H5.707l2.147-2.146a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708-.708L5.707 8.5H11.5z"/>
                    </svg>
                    Atras
                </button>
            </p>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php') ?>
</section>
</body>
</html>