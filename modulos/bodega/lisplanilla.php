<?php
$r = '../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
require($r . 'incluir/funciones.php');

if (isset($_POST['planilla'])) {
    $planilla = $_POST['planilla'];
    $fecha1 = $_POST['fecha1'];
    $fecha2 = $_POST['fecha2'];
    $empresa = $_POST['empresa'];
} else {
    $planilla = $_GET['planilla'];
    $fecha1 = isset($_GET['fecha1']) ? $_GET['fecha1'] : '';
    $fecha2 = isset($_GET['fecha2']) ? $_GET['fecha2'] : '';
    $empresa = isset($_GET['empresa']) ? $_GET['empresa'] : '';
}


$url = "planilla=";

?>
<!doctype html>
<html>
<head>

    < <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>
    <link rel="stylesheet" type="text/css"
          href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css"/>
    <script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#tabla').dataTable({
                'bJQueryUI': true,
                'bStateSave': true,
                'sPaginationType': 'full_numbers',
                'oLanguage': {
                    'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
                },
            });
            $('.pdf').click(function () {
                newSrc = $(this).attr('data-rel');
                if (validation_file(newSrc) == 200)
                    $('#modal').html("<iframe src='" + newSrc + "' width='100%' height='100%'></iframe>");
                else
                    $('#modal').html("<spam>El PDF no fue encontrado</spam>");
                $('#modal').dialog({modal: true, width: '600', height: '800', title: 'PDF de la solicitud'});
            });
            $('#dialog-message').dialog({
                height: 80,
                width: 'auto',
                modal: true
            });
        });
    </script>
</head>
<body>
<?php require($r . 'incluir/src/login.php') ?>
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <?php require($r . 'incluir/src/menu.php') ?>
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a class="current">Bodega</a>
        </article>
        <article id="contenido">
            <h2>Lista de planilla y movimientos</h2>
            <div class="reporte">

            </div>
            <table id="tabla">
                <thead>
                <tr>
                    <th>Empresa</th>
                    <th>Solicitud</th>
                    <th>Factura</th>
                    <th>Planilla</th>
                    <th>Cliente</th>
                    <th>Depto / Ciudad</th>
                    <th>Direccion de envio</th>
                    <th>F. Registro</th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php
                $con = "SELECT * FROM ((((solicitudes INNER JOIN empresas ON solempresa = empid) INNER JOIN clientes ON solcliente = cliid) INNER JOIN departamentos ON soldepentrega = depid) INNER JOIN ciudades ON (ciudepto = soldepentrega AND solciuentrega = ciuid)) INNER JOIN mdespachos ON solenvio = mdeid";
                $sql = crearConsulta($con, "",
                    array($planilla, "solplanilla = '$planilla'"),
                    array($fecha1, "solfechdespacho BETWEEN '$fecha1 00:00:01' AND '$fecha2 23:59:59'"),
                    array($empresa, "solempresa = '$empresa'"));

                $qry = $db->query($sql);
                while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
                    ?>
                    <tr>
                        <td title=" <?php echo $row['solempresa'] ?>"><?php echo $row['empnombre'] ?></td>
                        <td align="center"><?php echo $row['solid'] ?></td>
                        <td align="center"><?php echo $row['solfactura'] ?></td>
                        <td align="center"><?php echo $row['solplanilla'] ?></td>
                        <td title="<?php echo $row['solcliente'] ?>"><?php echo $row['clinombre'] . ' ' . $row['clinom2'] . ' ' . $row['cliape1'] . ' ' . $row['cliape2'] ?></td>
                        <td><?php echo $row['depnombre'] . ' / ' . $row['ciunombre'] ?></td>
                        <td><?php echo $row['solbarentrega'] . ", " . $row['solentrega'] ?></td>
                        <td align="center"><img src="<?php echo $r ?>imagenes/iconos/date.png"
                                                title="<?php echo $row['solfechreg'] ?>"/></td>
                        <?php
                        $qry2 = $db->query("SELECT * FROM detsolicitudes INNER JOIN productos ON detproducto = proid WHERE detempresa = '" . $row['solempresa'] . "' AND detsolicitud = '" . $row['solid'] . "'");
                        ?>
                        <td align="center"><img src="<?php echo $r ?>imagenes/iconos/pictures.png" title="<?php
                            while ($row2 = $qry2->fetch(PDO::FETCH_ASSOC))
                                echo '- ' . $row2['pronombre'] . ' (' . $row2['detcantidad'] . ') &#13;&#10;';
                            ?>"/></td>
                        <td align="center"><img src="<?php echo $r ?>imagenes/iconos/pdf.png" class="pdf"
                                                data-rel="<?php echo $r ?>pdf/solicitudes/<?php echo $row['solempresa'] . '/' . $row['solid'] . '.pdf' ?>"
                                                title="Solicitiud"></td>

                        <?php if ($row['solestado'] == 'ENVIADO') { ?>
                            <td align="center"><a
                                        href="valdescarga.php?empresa=<?php echo $row['solempresa'] . '&id=' . $row['solid'] . '&planilla=' . $planilla ?>"
                                        onClick="carga()" title="Descargar"><img
                                            src="<?php echo $r ?>imagenes/iconos/tick.png" class="grayscale"/></a></td>
                        <?php } else { ?>
                            <td align="center"><a
                                        title="Descargar"><img
                                            src="<?php echo $r ?>imagenes/iconos/tick.png" class="grayscale"/></a></td>
                        <?php } ?>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php') ?>
</section>
<div id="modal" style="display:none"></div>
</body>
</html>