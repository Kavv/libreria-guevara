<?php
$r = '../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
?>
<!doctype html>

<html lang="es">

<head>
    <title>CONSULTA KARDEX</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">
    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>

    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css"/>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>


    <script type="text/javascript"
            src="<?php echo $r ?>incluir/jquery/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            $('#producto').selectpicker();
            $('#form').validationEngine({
                showOneMessage: true,
                onValidationComplete: function (form, status) {
                    if (status) {
                        carga();
                        return true;
                    }
                }
            });
            $('#fecha1').datepicker({
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true,
            });
            $('#fecha2').datepicker({
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true,
            });
            $('.btnconsulta').button({icons: {primary: 'ui-icon ui-icon-search'}});
            $('#dialog-message').dialog({
                height: 80,
                width: 'auto',
                modal: true
            });
        });
    </script>
</head>
<body>
<?php require($r . 'incluir/src/login.php') ?>
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <?php require($r . 'incluir/src/menu.php') ?>
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a class="current">Bodega</a>
        </article>
        <article id="contenido">
            <form id="form" name="form" action="kardex.php" method="post">
                <fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
                    <legend class="ui-widget ui-widget-header ui-corner-all">Consulta de kardex</legend>
                    <p>
                        <label for="producto">Producto: </label>
                        <select name="producto" id="producto" data-live-search="true"
                                class="form-control validate[required]">
                            <option value="">SELECCIONE</option>
                            <?php
                            $qry = $db->query("SELECT * FROM productos ORDER BY pronombre");
                            while ($row = $qry->fetch(PDO::FETCH_ASSOC))
                                echo '<option value="' . $row['proid'] . '">'. $row['proid'] . ' / ' . $row['pronombre'] . '</option>';
                            ?>
                        </select>
                    </p>
                    <p>
                        <label for="empresa">Empresa:</label>
                        <select id="empresa" name="empresa" class="validate[required]">
                            <?php
                            $qry = $db->query("SELECT * FROM empresas ORDER BY empnombre");
                            while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
                                echo '<option value=' . $row['empid'] . '>' . $row['empnombre'] . '</option>';
                            }
                            ?>
                        </select>
                    </p>
                    <p>
                        <label for="fecha">Fechas:</label>
                        <input type="text" id="fecha1" name="fecha1" class="fecha validate[required]"/> - <input
                                type="text" id="fecha2" name="fecha2" class="fecha validate[required]"/>
                    </p>

                    <button type="submit" class="btnconsulta btn btn-primary btn-block mt-2" name="consultar"
                            value="Buscar">consultar
                    </button> <!-- BOTON CONSULTAR -->
                </fieldset>
            </form>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php') ?>
</section>
</body>
</html>