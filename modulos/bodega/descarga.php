<?php
$r = '../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
if (isset($_POST['planilla'])) $planilla = $_POST['planilla'];
else $planilla = $_GET['planilla'];
$row = $db->query("SELECT * FROM solicitudes INNER JOIN mdespachos ON mdeid = solenvio WHERE solplanilla = '$planilla'")->fetch(PDO::FETCH_ASSOC);


?>
<!doctype html>
<html>
<head>


    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>

    <link rel="stylesheet" type="text/css"
          href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css"/>
    <script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#tabla').dataTable({
                'bJQueryUI': true,
                'bStateSave': true,
                'sPaginationType': 'full_numbers',
                'oLanguage': {
                    'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
                },

            });
            $('.pdf').click(function () {
                newSrc = $(this).attr('data-rel');
                if(validation_file(newSrc) == 200)
                    $('#modal').html("<iframe src='" + newSrc + "' width='100%' height='100%'></iframe>");
                else
                    $('#modal').html("<spam>El PDF no fue encontrado</spam>");
                $('#modal').dialog({modal: true, width: '600', height: '800', title: 'PDF de la solicitud'});
            });
        });
    </script>
</head>
<body>
<?php require($r . 'incluir/src/login.php') ?>
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <?php require($r . 'incluir/src/menu.php') ?>
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a class="current">Bodega</a>
        </article>
        <article id="contenido">
            <h2>Planilla a descargar No. <?php echo $planilla . ' - ' . $row['mdenombre'] ?></h2>
            <div class="reporte">

            </div>
            <table id="tabla">
                <thead>
                <tr>
                    <th>Empresa</th>
                    <th>Solicitud</th>
                    <th>Factura</th>
                    <th>Cliente</th>
                    <th>Depto / Ciudad</th>
                    <th>Direccion de envio</th>
                    <th>F. Registro</th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php
                $qry = $db->query("SELECT * FROM ((((solicitudes INNER JOIN empresas ON solempresa = empid) INNER JOIN clientes ON solcliente = cliid) INNER JOIN departamentos ON soldepentrega = depid) INNER JOIN ciudades ON (ciudepto = soldepentrega AND solciuentrega = ciuid)) INNER JOIN mdespachos ON solenvio = mdeid WHERE solestado = 'ENVIADO' AND solplanilla = '$planilla'");
                while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
                    ?>
                    <tr>
                        <td title="<?php echo $row['solempresa'] ?>"><?php echo $row['empnombre'] ?></td>
                        <td align="center"><?php echo $row['solid'] ?></td>
                        <td align="center"><?php echo $row['solfactura'] ?></td>
                        <td title="<?php echo $row['solcliente'] ?>"><?php echo $row['clinombre'] . ' ' . $row['clinom2'] . ' ' . $row['cliape1'] . ' ' . $row['cliape2'] ?></td>
                        <td><?php echo $row['depnombre'] . ' / ' . $row['ciunombre'] ?></td>
                        <td><?php echo $row['solbarentrega'] . ", " . $row['solentrega'] ?></td>
                        <td align="center"><img src="<?php echo $r ?>imagenes/iconos/date.png"
                                                title="<?php echo $row['solfechreg'] ?>"/></td>
                        <?php
                        $qry2 = $db->query("SELECT * FROM detsolicitudes INNER JOIN productos ON detproducto = proid WHERE detempresa = '" . $row['solempresa'] . "' AND detsolicitud = '" . $row['solid'] . "'");
                        ?>
                        <td align="center"><img src="<?php echo $r ?>imagenes/iconos/pictures.png" title="<?php
                            while ($row2 = $qry2->fetch(PDO::FETCH_ASSOC))
                                echo '- ' . $row2['pronombre'] . ' (' . $row2['detcantidad'] . ') &#13;&#10;';
                            ?>"/></td>
                        <td align="center"><img src="<?php echo $r ?>imagenes/iconos/pdf.png" class="pdf"
                                                data-rel="<?php echo $r ?>pdf/solicitudes/<?php echo $row['solempresa'] . '/' . $row['solid'] . '.pdf' ?>"
                                                title="Solicitiud"></td>
                        <td align="center"><a
                                    href="valdescarga.php?empresa=<?php echo $row['solempresa'] . '&id=' . $row['solid'] . '&planilla=' . $planilla ?>"
                                    onClick="carga()" title="Descargar"><img
                                        src="<?php echo $r ?>imagenes/iconos/tick.png" class="grayscale"/></a></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
            <p class="boton">
                <button type="button" class="btn btn-outline-primary btnatras"
                        onClick="carga(); location.href='desplanilla.php'">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-left-square-fill"
                         fill="currentColor"
                         xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                              d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm9.5 8.5a.5.5 0 0 0 0-1H5.707l2.147-2.146a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708-.708L5.707 8.5H11.5z"/>
                    </svg>
                    Atras
                </button>

            </p>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php') ?>
</section>
<?php
if (isset($_GET['mensaje'])) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['mensaje'] . '</div>';
?>
<div id="modal" style="display:none"></div>
</body>
</html>