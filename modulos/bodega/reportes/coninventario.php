<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
?>
<!doctype html>
<html>
<head>
    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>
    <script type="text/javascript"
            src="<?php echo $r ?>incluir/jquery/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#fecha').datepicker({
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true,
                maxDate: '+0D',
            });
        });
    </script>
</head>
<body>
<?php require($r . 'incluir/src/login.php') ?>
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <?php require($r . 'incluir/src/menu.php') ?>
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a href="#">Bodega</a>
            <div class="mapa_div"></div>
            <a class="current">Reportes</a>
        </article>
        <article id="contenido">
            <form id="form" name="empresa" action="lisinventario.php" method="post">
                <fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
                    <legend class="ui-widget ui-widget-header ui-corner-all">Reporte inventario</legend>
                    <p>
                        <label for="fecha">Fecha:</label>
                        <input type="text" id="fecha" name="fecha" class="fecha validate[required]"/>
                    </p>
                    <p>
                        <label for="empresa">Empresa:</label>
                        <select id="empresa" name="empresa" class="validate[required]">
                            <option value="">SELECCIONE</option>
                            <?php
                            $qry = $db->query("SELECT * FROM empresas ORDER BY empnombre");
                            while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
                                echo '<option value=' . $row['empid'] . '>' . $row['empnombre'] . '</option>';
                            }
                            ?>
                        </select>
                    </p>
                    <p class="boton">
                        <button type="submit" class="btn btn-primary">consultar</button>
                    </p>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php') ?>
</section>
</body>
</html>