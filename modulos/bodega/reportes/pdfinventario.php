<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
require($r . 'incluir/fpdf/fpdf.php');
$fecha = $_GET['fecha'];
$empresa = $_GET['empresa'];


class PDF extends FPDF
{
    function Header()
    {
        global $fecha;
        $this->SetFont('LucidaConsole', '', 8);
        $this->Cell(50, 5, date('Y/m/d H:i:s'), 0, 1);
        $this->SetFont('LucidaConsole', '', 11);
        $this->Cell(280, 5, '------------------------------------------------------------------------------------------------------------------------', 0, 1, 'C');
        $this->Cell(280, 5, 'REPORTE INVENTARIO DE PRODUCTOS A ' . $fecha, 0, 1, 'C');
        $this->Cell(280, 5, '------------------------------------------------------------------------------------------------------------------------', 0, 1, 'C');
        $this->Ln(5);
    }

    function Footer()
    {
        $this->SetY(-15);
        $this->SetFont('LucidaConsole', '', 11);
        $this->Cell(0, 5, '------------------------------------------------------------------------------------------------------------------------', 0, 1, 'C');
        $this->SetFont('LucidaConsole', '', 8);
        $this->Cell(0, 5, 'Pagina ' . $this->PageNo() . '/{nb}', 0, 0, 'R');
    }
}

$pdf = new PDF();
$pdf->AddFont('LucidaConsole', '', 'lucon.php');
$pdf->AliasNbPages();
$pdf->AddPage('L');

$pdf->SetFont('LucidaConsole', '', 10);
$pdf->SetFillColor(220, 220, 220);
$pdf->SetX(17);
$pdf->Cell(25, 5, 'CODIGO', 1, 0, 'C', true);
$pdf->Cell(85, 5, 'NOMBRE', 1, 0, 'C', true);
$pdf->Cell(80, 5, 'DESCRIPCION', 1, 0, 'C', true);
$pdf->Cell(20, 5, 'CANTIDAD', 1, 0, 'C', true);
$pdf->Cell(25, 5, 'COSTO', 1, 0, 'C', true);
$pdf->Cell(25, 5, 'COSTO TOTAL', 1, 1, 'C', true);
$qry = $db->query("SELECT * FROM productos ORDER BY pronombre");
$i = 1;
$total = 0;

while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
    $qryIn = $db->query("SELECT * FROM detmovimientos INNER JOIN movimientos ON (movempresa = dmoempresa AND movprefijo = dmoprefijo AND movnumero = dmonumero) WHERE movfecha <= '$fecha' AND dmoempresa = '$empresa' AND dmoproducto = '" . $row['proid'] . "' ORDER BY movfecha DESC");

    $row2 = $qryIn->fetch(PDO::FETCH_ASSOC);


    $pdf->SetFont('LucidaConsole', '', 8);
    if ($i % 2 == 0) $x = 255;
    else $x = 235;
    $pdf->SetFillColor($x, $x, $x);
    $pdf->SetX(17);
    $pdf->Cell(25, 5, $row['proid'], 1, 0, 'C', true);
    $pdf->Cell(85, 5, $row['pronombre'], 1, 0, '', true);
    $pdf->Cell(80, 5, $row['prodescr'], 1, 0, '', true);
    $pdf->Cell(20, 5, number_format(isset($row2['dmoinventario']) ? $row2['dmoinventario'] : 0, 2), 1, 0, 'C', true);
    $pdf->Cell(25, 5, number_format(isset($row2['dmovinventario']) ? $row2['dmovinventario'] : 0, 2), 1, 0, 'R', true);
    $pdf->Cell(25, 5, number_format((isset($row2['dmoinventario']) ? $row2['dmoinventario'] : 0) * (isset($row2['dmovinventario']) ? $row2['dmovinventario'] : 0), 2), 1, 1, 'R', true);
    $i++;
    $total = $total +((isset($row2['dmoinventario']) ? $row2['dmoinventario'] : 0) * (isset($row2['dmovinventario']) ? $row2['dmovinventario'] : 0));
}
$pdf->SetX(17);
$pdf->SetFillColor(255, 255, 255);
$pdf->Cell(235, 5, '', 0, 0, '', true);
$pdf->SetFillColor(220, 220, 220);
$pdf->Cell(25, 5, number_format($total, 2), 1, 0, 'R', true);

$pdf->Output('inventario.pdf', 'd');
?>