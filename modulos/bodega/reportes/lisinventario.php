<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

$fecha = $_POST['fecha'];
$empresa = $_POST['empresa'];
$filtro = 'fecha=' . $fecha . '&empresa=' . $empresa;
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>

    <link rel="stylesheet" type="text/css"
          href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css"/>
    <script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#tabla').dataTable({
                'bStateSave': true,
                'bJQueryUI': true,
                'sPaginationType': 'full_numbers',
                'oLanguage': {
                    'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
                },
            });
        });
    </script>
</head>
<body>
<?php require($r . 'incluir/src/login.php') ?>
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <?php require($r . 'incluir/src/menu.php') ?>
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a href="#">Bodega</a>
            <div class="mapa_div"></div>
            <a class="current">Reportes</a>
        </article>
        <article id="contenido">
            <h2>Reporte inventario de productos a <?php echo $fecha . " de la empresa " . $empresa ?></h2>
            <div class="reporte">
                <a href="pdfinventario.php?<?php echo $filtro ?>"><img src="<?php echo $r ?>imagenes/iconos/pdf.png"
                                                                       title="pdf"/></a> <a
                        href="excelinventario.php?<?php echo $filtro ?>"><img
                            src="<?php echo $r ?>imagenes/iconos/excel.png" title="csv"/></a>
            </div>
            <table id="tabla">
                <thead>
                <tr>
                    <th>Codigo</th>
                    <th>Nombre</th>
                    <th>Descripcion</th>
                    <th>Cantidad</th>
                    <th>Costo</th>
                    <th>Costo total del inventario</th>
                </thead>
                <tbody>
                <?php
                $qry = $db->query("SELECT * FROM productos ORDER BY pronombre");

                $total = 0;
                $total_cantidad = 0;
                $total_costo = 0;

                while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
                    $row2 = $db->query("SELECT * FROM detmovimientos INNER JOIN movimientos ON (movempresa = dmoempresa AND movprefijo = dmoprefijo AND movnumero = dmonumero) WHERE movfecha <= '$fecha' AND dmoempresa = '$empresa' AND dmoproducto = '" . $row['proid'] . "' ORDER BY movfecha DESC")->fetch(PDO::FETCH_ASSOC);

                    ?>
                    <tr>
                        <td align="center" title="Id Num <?php echo $row2['dmoid'] ?>"><?php echo $row['proid'] ?></td>
                        <td><?php echo $row['pronombre'] ?></td>
                        <td><?php echo $row['prodescr'] ?></td>
                        <td align="center"><?php echo isset($row2['dmoinventario']) ? $row2['dmoinventario'] : 0 ?></td>
                        <td align="right"><?php echo number_format((isset($row2['dmovinventario']) ? $row2['dmovinventario'] : 0), 2) ?></td>
                        <td align="right"><?php echo number_format(((isset($row2['dmoinventario']) ? $row2['dmoinventario'] : 0) * (isset($row2['dmovinventario']) ? $row2['dmovinventario'] : 0)), 2) ?></td>
                        <?php
                        $total = $total + ((isset($row2['dmoinventario']) ? $row2['dmoinventario'] : 0) * (isset($row2['dmovinventario']) ? $row2['dmovinventario'] : 0));
                        $total_cantidad = $total_cantidad + (isset($row2['dmoinventario']) ? $row2['dmoinventario'] : 0);
                        $total_costo = $total_costo + (isset($row2['dmovinventario']) ? $row2['dmovinventario'] : 0);
                        ?>
                    </tr>
                    <?php
                }
                ?>
                <tfoot>
                <tr>
                    <td colspan="3"></td>
                    <td align="right" bgcolor="#C0C0C0"><?php echo number_format($total_cantidad, 2) ?></td>
                    <td align="right" bgcolor="#C0C0C0"><?php echo number_format($total_costo, 2) ?></td>
                    <td align="right" bgcolor="#C0C0C0"><?php echo number_format($total, 2) ?></td>
                </tr>
                </tfoot>
                </tbody>
            </table>
            <p class="boton">
                <button type="button" class="btn btn-outline-primary btnatras"
                        onClick="carga(); location.href='coninventario.php'">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-left-square-fill"
                         fill="currentColor"
                         xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                              d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm9.5 8.5a.5.5 0 0 0 0-1H5.707l2.147-2.146a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708-.708L5.707 8.5H11.5z"/>
                    </svg>
                    Atras
                </button>
            </p>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php') ?>
</section>
</body>
</html>