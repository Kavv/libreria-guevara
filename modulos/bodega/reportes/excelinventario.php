<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
require($r . 'incluir/phpexcel/Classes/PHPExcel.php');
$fecha = $_GET['fecha'];
$empresa = $_GET['empresa'];
$objPHPExcel = new PHPExcel();
$styleArray = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN
        )
    )
);
$objPHPExcel->getActiveSheet()->getStyle('A1:F2')->applyFromArray($styleArray);
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:F1')
    ->setCellValue('A1', 'REPORTE INVENTARIO DE PRODUCTOS A ' . $fecha);
$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A2', 'CODIGO')
    ->setCellValue('B2', 'NOMBRE')
    ->setCellValue('C2', 'DESCRIPCION')
    ->setCellValue('D2', 'CANTIDAD')
    ->setCellValue('E2', 'COSTO')
    ->setCellValue('F2', 'COSTO TOTAL');
$total = 0;
$i = 3;
$qry = $db->query("SELECT * FROM productos ORDER BY pronombre");
while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
    $qryIn = $db->query("SELECT * FROM detmovimientos INNER JOIN movimientos ON (movempresa = dmoempresa AND movprefijo = dmoprefijo AND movnumero = dmonumero) WHERE movfecha <= '$fecha' AND dmoempresa = '$empresa' AND dmoproducto = '" . $row['proid'] . "' ORDER BY movfecha DESC");

    $row2 = $qryIn->fetch(PDO::FETCH_ASSOC);


    $objPHPExcel->getActiveSheet()->getStyle('A' . $i . ':F' . $i)->applyFromArray($styleArray);
    $objPHPExcel->getActiveSheet()->getStyle('A' . $i)->getNumberFormat()->setFormatCode('000000');
    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A' . $i, $row['proid'])
        ->setCellValue('B' . $i, $row['pronombre'])
        ->setCellValue('C' . $i, $row['prodescr'])
        ->setCellValue('D' . $i, number_format(isset($row2['dmoinventario']) ? $row2['dmoinventario'] : 0, 2))
        ->setCellValue('E' . $i, number_format(isset($row2['dmovinventario']) ? $row2['dmovinventario'] : 0, 2))
        ->setCellValue('F' . $i, number_format((isset($row2['dmoinventario']) ? $row2['dmoinventario'] : 0) * (isset($row2['dmovinventario']) ? $row2['dmovinventario'] : 0), 2));
    $i++;
    $total = $total + ((isset($row2['dmoinventario']) ? $row2['dmoinventario'] : 0) * (isset($row2['dmovinventario']) ? $row2['dmovinventario'] : 0));
}
$objPHPExcel->getActiveSheet()->getStyle('A' . $i . ':F' . $i)->applyFromArray($styleArray);
$objPHPExcel->getActiveSheet()->getStyle('A' . $i . ':F' . $i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
$objPHPExcel->setActiveSheetIndex(0)
    ->mergeCells('A' . $i . ':E' . $i)
    ->setCellValue('A' . $i, 'COSTO TOTAL DEL INVENTARIO:')
    ->setCellValue('F' . $i, $total);

// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="inventario.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
?>