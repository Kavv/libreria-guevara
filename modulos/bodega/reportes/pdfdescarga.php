<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
require($r . 'incluir/fpdf/fpdf.php');
require($r . 'incluir/funciones.php');

$mdespacho = $_GET['mdespacho'];
$estado = $_GET['estado'];
$fecha1 = $_GET['fecha1'];
$fecha2 = $_GET['fecha2'];
$empresa = $_GET['empresa'];

$filtro = 'estado=' . $estado . '&mdespacho=' . $mdespacho . '&fecha1=' . $fecha1 . '&fecha2=' . $fecha2;

$sql = crearConsulta("SELECT * FROM ((((solicitudes INNER JOIN empresas ON solempresa = empid) INNER JOIN clientes ON solcliente = cliid) INNER JOIN usuarios ON solusudescarga = usuid) LEFT JOIN tcausales ON soltcadescarga = tcaid) LEFT JOIN causales ON (soltcadescarga = cautcausal AND solcaudescarga = cauid) ", "",
    array($fecha1, "solfechdescarga BETWEEN '$fecha1 00:00:01' AND '$fecha2 23:59:59'"),
    array($estado, "solestado = '$estado'"),
    array($mdespacho, "solenvio = '$mdespacho'"),
    array($empresa, "('$empresa' = 'TODAS' or solempresa = '$empresa' )"));

$qry = $db->query($sql);
$num = $qry->rowCount();

class PDF extends FPDF
{
    function Header()
    {
        global $db;
        global $mdespacho;
        global $estado;
        global $fecha1;
        global $fecha2;
        $row2 = $db->query("SELECT * FROM mdespachos WHERE mdeid = $mdespacho")->fetch(PDO::FETCH_ASSOC);
        $this->SetFont('LucidaConsole', '', 8);
        $this->Cell(0, 5, date('Y/m/d H:i:s'), 0, 1);
        $this->SetFont('LucidaConsole', '', 11);
        $this->Cell(0, 5, '------------------------------------------------------------------------------------------------------------------------------------------------', 0, 1, 'C');
        if ($estado != '')
            $this->Cell(0, 5, 'LISTADO DE SOLICITUDES ' . $estado . ', DESPACHADOS POR ' . $row2['mdenombre'] . ', DE ' . $fecha1 . ' AL ' . $fecha2, 0, 1, 'C');
        else $this->Cell(0, 5, 'LISTADO DE SOLICITUDES ENTREGADOS Y RECHAZADOS EN ENTREGA, DESPACHADOS POR ' . $row2['mdenombre'] . ' DE ' . $fecha1 . ' AL ' . $fecha2, 0, 1, 'C');
        $this->Cell(0, 5, '------------------------------------------------------------------------------------------------------------------------------------------------', 0, 1, 'C');
        $this->Ln(5);
    }

    function Footer()
    {
        $this->SetY(-15);
        $this->SetFont('LucidaConsole', '', 11);
        $this->Cell(0, 5, '------------------------------------------------------------------------------------------------------------------------------------------------', 0, 1, 'C');
        $this->SetFont('LucidaConsole', '', 8);
        $this->Cell(0, 5, 'Pagina ' . $this->PageNo() . '/{nb}', 0, 0, 'R');
    }
}

$pdf = new PDF('L', 'mm', 'Legal');
$pdf->AddFont('LucidaConsole', '', 'lucon.php');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('LucidaConsole', '', 10);
$pdf->SetFillColor(220, 220, 220);
$pdf->Cell(50, 5, 'EMPRESA', 1, 0, 'C', true);
$pdf->Cell(25, 5, 'SOLICITUD', 1, 0, 'C', true);
$pdf->Cell(45, 5, 'CLIENTE', 1, 0, 'C', true);
$pdf->Cell(25, 5, 'FACTURA', 1, 0, 'C', true);
$pdf->Cell(25, 5, 'TOTAL', 1, 0, 'C', true);
$pdf->Cell(45, 5, 'DESCARGADO', 1, 0, 'C', true);
$pdf->Cell(45, 5, 'FECHA DESCARGA', 1, 0, 'C', true);
$pdf->Cell(30.8, 5, 'ESTADO', 1, 0, 'C', true);
$pdf->Cell(45, 5, 'CAUSAL', 1, 1, 'C', true);
$total = 0;
$i = 1;
$pdf->SetFont('LucidaConsole', '', 8);
while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
    $total = $total + $row['soltotal'];
    if ($i % 2 == 0) $x = 255;
    else $x = 235;
    $pdf->SetFillColor($x, $x, $x);
    $pdf->Cell(50, 5, $row['empnombre'], 1, 0, '', true);
    $pdf->Cell(25, 5, $row['solid'], 1, 0, 'C', true);
    $pdf->Cell(45, 5, $row['clinombre'] . ' ' . $row['clinom2'] . ' ' . $row['cliape1'] . ' ' . $row['cliape2'], 1, 0, '', true);
    $pdf->Cell(25, 5, $row['solfactura'], 1, 0, 'C', true);
    $pdf->Cell(25, 5, number_format($row['soltotal'], 2), 1, 0, 'R', true);
    $pdf->Cell(45, 5, $row['usunombre'], 1, 0, '', true);
    $pdf->Cell(45, 5, $row['solfechdescarga'], 1, 0, 'C', true);
    $pdf->Cell(30.8, 5, $row['solestado'], 1, 0, '', true);
    $pdf->Cell(45, 5, $row['tcanombre'] . ' -> ' . $row['caunombre'], 1, 1, '', true);
    $i++;
}
$pdf->SetFillColor(255, 255, 255);
$pdf->Cell(50, 5, '', 0, 0, '', true);
$pdf->SetFillColor(220, 220, 220);
$pdf->Cell(25, 5, $num, 1, 0, 'C', true);
$pdf->SetFillColor(255, 255, 255);
$pdf->Cell(70, 5, '', 0, 0, '', true);
$pdf->SetFillColor(220, 220, 220);
$pdf->Cell(25, 5, number_format($total, 2), 1, 0, 'R', true);
$pdf->SetFillColor(255, 255, 255);
$pdf->Cell(120.8, 5, '', 0, 0, '', true);
$pdf->Output('descarga.pdf', 'd');
?>