<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
require($r . 'incluir/funciones.php');
require($r . 'incluir/phpexcel/Classes/PHPExcel.php');
$mdespacho = $_GET['mdespacho'];
$estado = $_GET['estado'];
$fecha1 = $_GET['fecha1'];
$fecha2 = $_GET['fecha2'];
$empresa = $_GET['empresa'];

$filtro = 'estado=' . $estado . '&mdespacho=' . $mdespacho . '&fecha1=' . $fecha1 . '&fecha2=' . $fecha2;

$sql = crearConsulta("SELECT * FROM ((((solicitudes INNER JOIN empresas ON solempresa = empid) INNER JOIN clientes ON solcliente = cliid) INNER JOIN usuarios ON solusudescarga = usuid) LEFT JOIN tcausales ON soltcadescarga = tcaid) LEFT JOIN causales ON (soltcadescarga = cautcausal AND solcaudescarga = cauid) ", "",
    array($fecha1, "solfechdescarga BETWEEN '$fecha1 00:00:01' AND '$fecha2 23:59:59'"),
    array($estado, "solestado = '$estado'"),
    array($mdespacho, "solenvio = '$mdespacho'"),
    array($empresa, "('$empresa' = 'TODAS' or solempresa = '$empresa' )"));

$qry = $db->query($sql);
$num = $qry->rowCount();
$row2 = $db->query("SELECT * FROM mdespachos WHERE mdeid = '$mdespacho'")->fetch(PDO::FETCH_ASSOC);
$objPHPExcel = new PHPExcel();
$styleArray = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN
        )
    )
);
$objPHPExcel->getActiveSheet()->getStyle('A1:I2')->applyFromArray($styleArray);
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:I1');
if ($estado != '')
    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1', 'LISTADO DE SOLICITUDES ' . $estado . ', DESPACHADOS POR ' . $row2['mdenombre'] . ', DE ' . $fecha1 . ' AL ' . $fecha2);
else
    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1', 'LISTADO DE SOLICITUDES ENTREGADOS Y RECHAZADOS EN ENTREGA, DESPACHADOS POR ' . $row2['mdenombre'] . ' DE ' . $fecha1 . ' AL ' . $fecha2);
$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A2', 'EMPRESA')
    ->setCellValue('B2', 'SOLICITUD')
    ->setCellValue('C2', 'CLIENTE')
    ->setCellValue('D2', 'FACTURA')
    ->setCellValue('E2', 'TOTAL')
    ->setCellValue('F2', 'DESCARGADO')
    ->setCellValue('G2', 'FECHA DESCARGA')
    ->setCellValue('H2', 'ESTADO')
    ->setCellValue('I2', 'CAUSAL');
$total = 0;
$i = 3;
while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
    $total = $total + $row['soltotal'];
    $objPHPExcel->getActiveSheet()->getStyle('A' . $i . ':I' . $i)->applyFromArray($styleArray);
    $objPHPExcel->getActiveSheet()->getStyle('D' . $i)->getNumberFormat()->setFormatCode(PHPExcel_Cell_DataType::TYPE_STRING);
    $objPHPExcel->getActiveSheet()->getStyle('G' . $i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A' . $i, $row['empnombre'])
        ->setCellValue('B' . $i, $row['solid'])
        ->setCellValue('C' . $i, $row['clinombre'] . ' ' . $row['clinom2'] . ' ' . $row['cliape1'] . ' ' . $row['cliape2'])
        ->setCellValue('D' . $i, $row['solfactura'])
        ->setCellValue('E' . $i, $row['soltotal'])
        ->setCellValue('F' . $i, $row['usunombre'])
        ->setCellValue('G' . $i, $row['solfechdescarga'])
        ->setCellValue('H' . $i, $row['solestado'])
        ->setCellValue('I' . $i, $row['tcanombre'] . ' -> ' . $row['caunombre']);
    $i++;
}
$objPHPExcel->getActiveSheet()->getStyle('A' . $i . ':I' . $i)->applyFromArray($styleArray);
$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('B' . $i, 'TOTALES:')
    ->setCellValue('D' . $i, $num)
    ->setCellValue('E' . $i, $total);

// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="descarga.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
?>