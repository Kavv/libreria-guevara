<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
?>
<!doctype html>
<html>
<head>
    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>
    <script type="text/javascript"
            src="<?php echo $r ?>incluir/jquery/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#form').validationEngine({
                showOneMessage: true,
                onValidationComplete: function (form, status) {
                    if (status == true) {
                        carga();
                        return true;
                    }
                }
            });
            $('.btnconsulta').button({icons: {primary: 'ui-icon ui-icon-search'}});
            $('#fecha1').datepicker({
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true,
                maxDate: '+0D',
                onClose: function (selectedDate) {
                    $('#fecha2').datepicker('option', 'minDate', selectedDate);
                }
            }).keypress(function (event) {
                event.preventDefault()
            });
            $('#fecha2').datepicker({
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true,
                maxDate: '+0D',
                onClose: function (selectedDate) {
                    $('#fecha1').datepicker('option', 'maxDate', selectedDate);
                }
            }).keypress(function (event) {
                event.preventDefault()
            });
        });
    </script>
    <style type="text/css">
        #form fieldset {
            padding: 10px;
            display: block;
            width: 500px;
            margin: 20px auto
        }

        #form legend {
            font-weight: bold;
            margin-left: 5px;
            padding: 5px
        }

        #form label {
            display: inline-block;
            width: 70px;
            text-align: right;
            margin: 0.3em 2% 0 0
        }

        #form p {
            margin: 5px 0
        }
    </style>
</head>
<body>
<?php require($r . 'incluir/src/login.php') ?>
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <?php require($r . 'incluir/src/menu.php') ?>
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a href="#">Bodega</a>
            <div class="mapa_div"></div>
            <a class="current">Reportes</a>
        </article>
        <article id="contenido">
            <form id="form" name="empresa" action="lisdescarga.php" method="post">
                <fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
                    <legend class="ui-widget ui-widget-header ui-corner-all">Reporte de entregados y rechazados</legend>
                    <p>
                        <label for="empresa">Empresa:</label>
                        <select id="empresa" name="empresa" class="validate[required]">
                            <option value="TODAS">TODAS</option>
                            <?php
                            $qry = $db->query("SELECT * FROM empresas ORDER BY empnombre");
                            while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
                                echo '<option value=' . $row['empid'] . '>' . $row['empnombre'] . '</option>';
                            }
                            ?>
                        </select>
                    </p>
                    <p>
                        <label for="mdespacho">Medio de despacho:</label>
                            <select name="mdespacho" class="validate[required]">
                            <option value="">SELECCIONE</option>
                            <?php
                            $qry = $db->query("SELECT * FROM mdespachos ORDER BY mdenombre");
                            while ($row = $qry->fetch(PDO::FETCH_ASSOC))
                                echo '<option value=' . $row['mdeid'] . '>' . $row['mdenombre'] . '</option>';
                            ?>
                        </select>
                    </p>
                    <p>
                        <label>Estado: </label>
                        <select name="estado">
                            <option value="">TODOS</option>
                            <option value="ENTREGADO">ENTREGADO</option>
                            <option value="RECHAZADO ENTREGA">RECHAZADO ENTREGA</option>
                        </select>
                    </p>
                    <p>
                        <label for="fecha">Fechas:</label>
                        <input type="text" id="fecha1" name="fecha1" class="fecha "/> - <input
                                type="text" id="fecha2" name="fecha2" class="fecha"/>
                    </p>
                    <p class="boton">
                        <button type="submit" class="btn btn-primary mt-1">consultar</button>
                    </p>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php') ?>
</section>
</body>
</html>