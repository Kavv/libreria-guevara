<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
require($r . 'incluir/funciones.php');

$mdespacho = $_POST['mdespacho'];
$estado = $_POST['estado'];
$fecha1 = $_POST['fecha1'];
$fecha2 = $_POST['fecha2'];
$empresa = $_POST['empresa'];

//PENDIENTE REALIZAR BUSQUEDA TENIENDO EN cuenta LA EMPRESA
$filtro = 'empresa=' . $empresa . '&estado=' . $estado . '&mdespacho=' . $mdespacho . '&fecha1=' . $fecha1 . '&fecha2=' . $fecha2;


$sql = crearConsulta("SELECT * FROM ((((solicitudes INNER JOIN empresas ON solempresa = empid) INNER JOIN clientes ON solcliente = cliid) INNER JOIN usuarios ON solusudescarga = usuid) LEFT JOIN tcausales ON soltcadescarga = tcaid) LEFT JOIN causales ON (soltcadescarga = cautcausal AND solcaudescarga = cauid) ", "",
    array($fecha1, "solfechdescarga BETWEEN '$fecha1 00:00:01' AND '$fecha2 23:59:59'"),
    array($estado, "solestado = '$estado'"),
    array($mdespacho, "solenvio = '$mdespacho'"),
    array($empresa, "('$empresa' = 'TODAS' or solempresa = '$empresa' )"));

$qry = $db->query($sql);
$num = $qry->rowCount();

$row2 = $db->query("SELECT * FROM mdespachos WHERE mdeid = '$mdespacho'")->fetch(PDO::FETCH_ASSOC);

?>
<!doctype html>
<html>
<head>
    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>

    <link rel="stylesheet" type="text/css"
          href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css"/>
    <script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#tabla').dataTable({
                'bStateSave': true,
                'bJQueryUI': true,
                'sPaginationType': 'full_numbers',
                'oLanguage': {
                    'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
                },
            });
        });
    </script>
</head>
<body>
<?php require($r . 'incluir/src/login.php') ?>
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <?php require($r . 'incluir/src/menu.php') ?>
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a href="#">Bodega</a>
            <div class="mapa_div"></div>
            <a class="current">Reportes</a>
        </article>
        <article id="contenido">
            <h2 style='text-align:center;'>Listado
                solicitudes <?php if ($estado != '') echo $estado . ', '; else echo 'ENTREGADOS Y RECHAZADOS EN ENTREGA, ' ?>
                despachados por <?php echo $row2['mdenombre'] ?>, desde <?php echo $fecha1 . ' al ' . $fecha2 ?></h2>
            <div class="reporte">
                <a href="pdfdescarga.php?<?php echo $filtro ?>"><img src="<?php echo $r ?>imagenes/iconos/pdf.png"
                                                                     title="pdf"/></a> <a
                        href="exceldescarga.php?<?php echo $filtro ?>"><img
                            src="<?php echo $r ?>imagenes/iconos/excel.png" title="csv"/></a>
            </div>
            <table id="tabla">
                <thead>
                <tr>
                    <th>Empresa</th>
                    <th>Solicitud</th>
                    <th>Cliente</th>
                    <th>Factura</th>
                    <th>Total</th>
                    <th>Descargado</th>
                    <th>F. Descarga</th>
                    <th>Estado</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $total = 0;
                while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
                    $total = $total + $row['soltotal'];
                    ?>
                    <tr>
                        <td title="<?php echo $row['solempresa'] ?>"><?php echo $row['empnombre'] ?></td>
                        <td align="center"><?php echo $row['solid'] ?></td>
                        <td title="<?php echo $row['solcliente'] ?>"><?php echo $row['clinombre'] . ' ' . $row['clinom2'] . ' ' . $row['cliape1'] . ' ' . $row['cliape2'] ?></td>
                        <td align="center"><?php echo $row['solfactura'] ?></td>
                        <td align="right"><?php echo number_format($row['soltotal'], 2) ?></td>
                        <td title="<?php echo $row['solusudescarga'] ?>"><?php echo $row['usunombre'] ?></td>
                        <td align="center"><img src="<?php echo $r ?>imagenes/iconos/date.png"
                                                title="<?php echo $row['solfechdescarga'] ?>"/></td>
                        <td title="<?php echo $row['tcanombre'] . ' -> ' . $row['caunombre'] ?>"><?php echo $row["solestado"] ?></td>
                    </tr>
                <?php } ?>
                </tbody>
                <tfoot>
                <tr>
                    <td></td>
                    <td align="center" bgcolor="#C0C0C0"><?php echo $num ?></td>
                    <td colspan="2"></td>
                    <td bgcolor="#C0C0C0" align="right"><?php echo number_format($total, 2) ?></td>
                    <td colspan="3"></td>
                </tr>
                </tfoot>
            </table>
            <p class="boton">
                <button type="button" class="btn btn-outline-primary btnatras"
                        onClick="carga(); location.href='condescarga.php'">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-left-square-fill"
                         fill="currentColor"
                         xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                              d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm9.5 8.5a.5.5 0 0 0 0-1H5.707l2.147-2.146a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708-.708L5.707 8.5H11.5z"/>
                    </svg>
                    Atras
                </button>
            </p>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php') ?>
</section>
</body>
</html>


