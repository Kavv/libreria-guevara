<?php
    $r = '../../../../';
    require($r . 'incluir/session.php');
    require($r . 'incluir/connection.php');

    $orden = trim($_GET['orden']);
    $tipo = $_GET['tipo'];
    $result = new stdClass();

    if($orden == "" || $orden == null || $orden == "null" || $orden < 1)
    {
        $result->code = 1;
        $result->msj = "El numero de orden no es valido";
        echo json_encode($result);
        exit();
    }

    $consulta = "SELECT m.movnumero, m.movdocumento, m.movprefijo, m.movfecha, m.created_at as mcreated, 
    d.dmoproducto, d.dmocantidad, d.dmounitario, d.dmototal, d.created_at as dcreated,
    p.pronombre 
    FROM movinventario as m 
    LEFT JOIN detmovimientos as d ON d.dmonumero = m.movnumero and m.movprefijo = d.dmoprefijo
    LEFT JOIN productos as p ON proid = d.dmoproducto
    WHERE m.movnumero = '$orden' AND m.movprefijo = '$tipo' ORDER BY d.created_at";
    $productos = $db->query($consulta);
    if(!$productos)
    {
        $aux = $db->query("SELECT tipnombre FROM tipdocumentos WHERE tipid = '$tipo'")->fetch(PDO::FETCH_OBJ);
        $result->code = 1;
        $result->msj = "Ocurrio un error al momento de buscar la orden #$orden de tipo $aux->tipnombre, si el error persiste solicitar soporte tecnico";
        echo json_encode($result);
        exit();
    }

    if($productos->rowCount() == 0)
    {
        $aux = $db->query("SELECT tipnombre FROM tipdocumentos WHERE tipid = '$tipo'")->fetch(PDO::FETCH_OBJ);
        $result->code = 1;
        $result->msj = "No se encontro la orden #$orden de tipo $aux->tipnombre";
        echo json_encode($result);
        exit();
    }

    $guardar = $_GET['guardar'];
    if($guardar == 0)
    {
        $movimiento = $_GET['movimiento'];

        $aux = $db->query("SELECT * FROM mov_distribuidores WHERE tipo = '$tipo' AND numero = '$orden' AND movimiento = '$movimiento' ");
        if($aux->rowCount() > 0)
        {
            
            $result->code = 1;
            $result->msj = "Esta orden ya se encuentra relacionado a esta factura";
            echo json_encode($result);
            exit();
        }

        $qry = $db->query("INSERT INTO mov_distribuidores(tipo, numero, movimiento) VALUES ('$tipo', '$orden', '$movimiento')");
        if(!$qry)
        {
            $aux = $db->query("SELECT tipnombre FROM tipdocumentos WHERE tipid = '$tipo'")->fetch(PDO::FETCH_OBJ);
            $result->code = 1;
            $result->msj = "Ocurrio un error al momento de agregar la orden #$orden a la factura de distribuidores, si el error persiste solicitar soporte tecnico";
            echo json_encode($result);
            exit();
        }
    }

    
    $result->code = 0;
    $result->msj = "Se agrego a la factura la orden #$orden";
    $result->data = $productos->fetchAll(PDO::FETCH_ASSOC);
    echo json_encode($result);
    exit();
?>
