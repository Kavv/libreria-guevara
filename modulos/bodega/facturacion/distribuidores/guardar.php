<?php
$r = '../../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

$empresa = '16027020274133';
$prefijo = 'FD';
$fecha = $_POST['fecha'];
$simbolo = $_POST['simbolo'];
$result = new stdClass();

//Consultamos si hay un movimiento en proceso
$num = $db->query("SELECT * FROM movinventario
INNER JOIN tipdocumentos ON tipid = movprefijo
WHERE tiptipo = 'FACDIS'
AND movestado = 'PROCESO' AND tipid = '$prefijo';")->rowCount();

if ($num > 0) {
    $result->code = 1;
    $result->msj = 'Hay un documento que no se ha finalizado, debe finalizarlo o cancelarlo para proceder';
    echo json_encode($result);
    exit();
}

// Obtenemos el # de movimiento consecuente en base al prefijo
$rowmax = $db->query("SELECT MAX(movnumero)+1 AS ultimo FROM movinventario WHERE movempresa = '$empresa' AND movprefijo = '$prefijo'")->fetch(PDO::FETCH_ASSOC);
if($rowmax['ultimo'] == '')
    $numero = 1;
else
    $numero = $rowmax['ultimo'];
 

// Procedimiento para crear la descripción del mov
$departamento = $_POST['departamento'];
$ciudad = $_POST['ciudad'];
$distribuidor = strupperEsp(trim($_POST['distribuidor']));
$entregado = strupperEsp(trim($_POST['entregado']));
$bodeguero = strupperEsp(trim($_POST['bodeguero']));
$telefono1 = $_POST['telefono1'];
$telefono2 = $_POST['telefono2'];

$direccion = strupperEsp(trim($_POST['direccion']));
$comentario = strupperEsp(trim($_POST['comentario']));

$digitador = $_SESSION['id'];

$qry = $db->query("INSERT INTO infoextra 
(comentario, departamento, ciudad, entregado, bodeguero, telefono1, telefono2, digitador, distribuidor, direccion)
VALUES ('$comentario', '$departamento', '$ciudad', '$entregado', '$bodeguero', '$telefono1', '$telefono2', '$digitador', '$distribuidor', '$direccion');") or die($db->errorInfo()[2]);

$id_info = $db->lastInsertId();

if(!$qry)
{
    $result->code = 1;
    $result->msj = 'Ocurrio un error durante al crear la información general';
    echo json_encode($result);
    exit();
}
// Creamos el mov
$qry = $db->query("INSERT INTO movinventario(movempresa, movprefijo, movnumero, movestado, movfecha, movinfo, movmoneda) VALUES ('$empresa', '$prefijo', '$numero', 'PROCESO', '$fecha', '$id_info', '$simbolo')");

if(!$qry)
{
    $result->code = 1;
    $result->msj = 'Ocurrio un error al crear el nuevo movimiento';
    echo json_encode($result);
    exit();
}


$result->code = 0;
$result->orden = $numero;
$result->msj = '¡Guardado exitosamente!';
echo json_encode($result);
exit();

?>