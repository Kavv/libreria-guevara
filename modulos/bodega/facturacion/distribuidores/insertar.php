<?php
$r = '../../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');


?>
<!doctype html>

<html lang="es">

<head>
    <title>CREAR FACTURA A DISTRIBUIDORES</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">

    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-not-form.php');
    ?>

    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

    <link rel="stylesheet" href="<?php echo $r.'incluir/librerias/fontawesome/css/all.css'?>" />
    <script src="<?php echo $r.'incluir/librerias/fontawesome/js/all.js'?>"></script>

    <link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
    
    <style>
        
    </style>
    <script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {

        //$('#producto').selectpicker();
        
        $('#form').validationEngine({
            showOneMessage: true,
            onValidationComplete: function(form, status) {
                if (status) {
                    return true;
                }
            }
        });
        $(".fecha").datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
        });

        $("#dialog-message").dialog({
            height: 80,
            width: 'auto',
            modal: true
        });
        
        // Almacenar las ciudades
        ruta = "<?php echo $r . 'modulos/principal/recibos/ajax/ciudades.php';?>" ;
        $ciudades = [];
        $.get(ruta, function(res){
            res = JSON.parse(res);
            res.forEach(function(data){

                var dep = data.ciudepto;
                var ciu = data.ciuid;
                var nombre = data.ciunombre;
                // Si es diferente a null significa que el arreglo en el indice x ya posee datos
                if($ciudades[dep] != null)
                {
                    $ciudades[dep].push([ciu, nombre]);
                }
                else
                {
                    // Si es la primera vez se asigna el primer dato como un arreglo
                    $ciudades[dep] = [[ciu, nombre]];
                }
            });
        });
        
        
    });
    </script>
    <style>
        .border-group{
            border: 1px dashed;
        }
    </style>
</head>

<body>
    <?php require($r . 'incluir/src/login.php') ?>
    <section id="principal">
        <?php require($r . 'incluir/src/cabeza.php') ?>
        <?php require($r . 'incluir/src/menu.php') ?>
        <article id="cuerpo" class="form-style">
            <article class="mapa">
                <a href="#">Principal</a>
                <div class="mapa_div"></div>
                <a href="#">Bodega</a>
                <div class="mapa_div"></div>
                <a href="#">Documentos</a>
                <div class="mapa_div"></div>
                <a class="current">Entrada</a>
            </article>
            <div id="msj"></div>




            <!-- DATOS GENERALES -->
            <div class="col-md-12">
                <fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12 fieldset-espejo">
                    <legend class="ui-widget ui-widget-header ui-corner-all">Factura para Distribuidores</legend>
                    
                    <div class="row">
                        <div class="col-md-3">
                            <label for=""><span style="color:red">*</span>Fecha</label>
                            <input type="text" id="fecha" class="form-control validate[custom[date], required] fecha" value="" name="fecha">
                        </div>
                        <div class="col-md-3">
                            <label for=""><span style="color:red">*</span>Departamento</label>
                            <select name="departamento" id="departamento" class="form-control validate[required] selectpicker" data-live-search="true">
                                <option value=""></option>
                                <?php 
                                    $departamentos = $db->query("SELECT * FROM departamentos ORDER BY depnombre ASC");
                                    while ($d = $departamentos->fetch(PDO::FETCH_ASSOC)) {
                                            echo "<option value='" . $d['depid'] . "'>" . $d['depnombre'] . "</option>";
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label for=""><span style="color:red">*</span>Ciudad</label>
                            <select name="ciudad" id="ciudad" class="form-control validate[required]">
                            </select>
                        </div>
                        
                        <div class="col-md-3">
                            <label for="">Moneda</label>
                            <select name="simbolo" id="simbolo" class="form-control validate['required']">
                                <?php 
                                    $monedas = $db->query("SELECT * FROM monedas ORDER BY nombre ASC");
                                    while ($moneda = $monedas->fetch(PDO::FETCH_ASSOC)) {
                                        $moneda_distribuidor = "en-US";
                                        if($moneda['currency'] == $moneda_distribuidor)
                                        echo "<option selected value='" . $moneda['currency'] . "'>" . $moneda['nombre']. "(" . $moneda['simbolo'].")</option>";
                                        else
                                        echo "<option value='" . $moneda['currency'] . "'>" . $moneda['nombre']. "(" . $moneda['simbolo'] . ")</option>";
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label for=""><span style="color:red">*</span>Distribuidor</label>
                            <input type="text" id="distribuidor" class="form-control validate[required, maxSize[50]] uppercase enter" value="" name="distribuidor">
                        </div>
                        <div class="col-md-4">
                            <label for="">Entregado</label>
                            <input type="text" id="entregado" class="form-control validate[maxSize[40]] uppercase enter" value="" name="entregado">
                        </div>
                        <div class="col-md-4">
                            <label for="">Bodeguero</label>
                            <input type="text" id="bodeguero" class="form-control validate[maxSize[40]] uppercase enter" value="" name="bodeguero">
                        </div>
                    </div>
                    <div class="row">
                        
                        <div class="col-md-3">
                            <label for="">Telefono 1</label>
                            <input type="text" id="telefono1" class="form-control validate[maxSize[15]] enter" value="" name="telefono1">
                        </div>
                        <div class="col-md-3">
                            <label for="">Telefono 2</label>
                            <input type="text" id="telefono2" class="form-control validate[maxSize[15]] enter" value="" name="telefono2">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label for="">Dirección</label>
                            <textarea type="text" id="direccion" class="form-control validate[maxSize[200] uppercase enter" name="direccion" rows="3"></textarea>
                        </div>
                        <div class="col-md-6">
                            <label>Comentario:</label> 
                            <textarea name="texto" id="comentario" style="text-transform: uppercase" cols="100" rows="3" class="form-control validate[maxSize[400]] enter"></textarea>
                        </div>
                    </div>
                    <div class="row my-3 d-flex justify-content-center">
                        <button class="btn btn-primary" onclick="guardar()">GUARDAR</button>
                    </div>
                </fieldset>
            </div>
            
        </article>
        <?php require($r . 'incluir/src/pie.php') ?>
    </section>
	<?php require($r . 'incluir/src/loading.php'); ?>

</body>
<script>
    
    $(".enter").keypress(function(e){
        if (e.which == 13) {
            guardar();
            return false;
        }
    });
    
    function validacion()
    {
        s0 = $("#fecha").validationEngine('validate');
        s1 = $("#departamento").validationEngine('validate');
        s2 = $("#ciudad").validationEngine('validate');
        s3 = $("#distribuidor").validationEngine('validate');
        s4 = $("#entregado").validationEngine('validate');
        s5 = $("#bodeguero").validationEngine('validate');
        s6 = $("#telefono1").validationEngine('validate');
        s7 = $("#telefono2").validationEngine('validate');
        s8 = $("#direccion").validationEngine('validate');
        s9 = $("#comentario").validationEngine('validate');
        s10 = $("#simbolo").validationEngine('validate');
        if(s0 || s1 || s2 || s3 || s4 || s5 || s6 || s7 || s8 || s9 || s10)
            return true;
        else
            return false;
    }

    function guardar()
    {
        var status = true;
            
        if(validacion())
            return;
            
        var fecha = $("#fecha").val();
        var departamento = $("#departamento").val();
        var ciudad = $("#ciudad").val();
        var distribuidor = $("#distribuidor").val();
        var entregado = $("#entregado").val();
        var bodeguero = $("#bodeguero").val();
        var telefono1 = $("#telefono1").val();
        var telefono2 = $("#telefono2").val();
        var direccion = $("#direccion").val();
        var comentario = $("#comentario").val();
        var simbolo = $("#simbolo").val();

        $("#loading").css('display', 'block');
        ruta = "guardar.php";
        $.ajax({
            url: ruta,
            type: 'POST',
            data: { 'fecha': fecha,
                    'departamento': departamento,
                    'ciudad':ciudad,
                    'distribuidor':distribuidor, 
                    'entregado':entregado, 
                    'bodeguero':bodeguero, 
                    'telefono1':telefono1, 
                    'telefono2':telefono2, 
                    'direccion':direccion, 
                    'comentario':comentario,
                    'simbolo': simbolo },
            success: function(res)
            {
                var res = JSON.parse(res);
                if(res.code == 0)
                {
                    console.log('redirección '+res.orden);
                    make_alert({'message':res.msj});
                    location.href="datos.php?orden="+res.orden;
                }
                if(res.code == 1)
                {
                    make_alert({'type':'danger', 'message':res.msj});
                    $("#loading").css('display', 'none');
                }

            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            make_alert({'type':'danger', 'message':'Ocurrio un error, intentelo nuevamente, si el error persiste solicitar asistencia tecnica'});
            $("#loading").css('display', 'none');
        }); 
    }
    
    $("#departamento").change(function(){
        $("#ciudad").empty();
        // Por defecto
        var html = '';
        $("#ciudad").append(html);

        dep = $(this).val();
		if(dep == "")
			cant = 0;
		else
        {
            cant = 0;
            if(typeof $ciudades[dep] !== 'undefined')
                cant = $ciudades[dep].length;
        }
        for(var i = 0; i < cant; i++)
        {
            html = "<option value='" + $ciudades[dep][i][0] + "'>" + $ciudades[dep][i][1] + "</option>";
            $("#ciudad").append(html);
        }
        $('#ciudad').selectpicker('refresh');
    });

    function make_alert({object = $('#msj'), type = 'success', message = ''})
    {
        object.slideUp();
        object.empty();

        var alerta = 
        '<div class="alert alert-'+ type +' alert-dismissible fade show" role="alert">'+
            message +
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
                '<span aria-hidden="true">&times;</span>'+
            '</button>'+
        '</div>';

        object.append(alerta);
        object.slideDown();
    }


</script>

</html>