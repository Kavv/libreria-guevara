<?php
$r = '../../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

$orden = $_GET['orden'];
$query = $db->query("SELECT * FROM movinventario 
LEFT JOIN infoextra as i ON i.id = movinfo
LEFT JOIN ciudades as c ON (c.ciudepto = i.departamento AND c.ciuid = i.ciudad)
WHERE movnumero = '$orden' and movprefijo = 'FD'");

if(!$query)
{
    echo "Ocurrio un error, contacte a soporte tecnico";
    exit();
}

if($query->rowCount() == 0)
{
    echo "No se encuentra el movimiento, intentelo nuevamente o solicite soporte tecnico";
    exit();
}

$data = $query->fetch(PDO::FETCH_OBJ);

$empresa = $data->movempresa;
$prefijo = $data->movprefijo;
$numero = $data->movnumero;
$id_info = $data->movinfo;
$departamento = $data->departamento;
$ciudad_c = $data->ciudad;
$ciudad_n = $data->ciunombre;
$entregado = $data->entregado;
$bodeguero = $data->bodeguero;
$comentario = $data->comentario;
$telefono1 = $data->telefono1;
$telefono2 = $data->telefono2;
$direccion = $data->direccion;
$distribuidor = $data->distribuidor;
$currency = $data->movmoneda;
$movfecha = $data->movfecha;




?>
<!doctype html>

<html lang="es">

<head>
    <title>FACTURA A DISTRIBUIDORES</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">
    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-not-form.php');
    ?>

    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

    <link rel="stylesheet" href="<?php echo $r.'incluir/librerias/fontawesome/css/all.css'?>" />
    <script src="<?php echo $r.'incluir/librerias/fontawesome/js/all.js'?>"></script>

    <link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
    
    <style>
        #monto-total{
            font-size: 20px!important;
            font-weight: bold;
        }
    </style>
    <script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {

        //$('#producto').selectpicker();
        
        $('#form').validationEngine({
            showOneMessage: true,
            onValidationComplete: function(form, status) {
                if (status) {
                    return true;
                }
            }
        });

        $("#dialog-message").dialog({
            height: 80,
            width: 'auto',
            modal: true
        });
        
        // Almacenar las ciudades
        ruta = "<?php echo $r . 'modulos/principal/recibos/ajax/ciudades.php';?>" ;
        $ciudades = [];
        $.get(ruta, function(res){
            res = JSON.parse(res);
            res.forEach(function(data){

                var dep = data.ciudepto;
                var ciu = data.ciuid;
                var nombre = data.ciunombre;
                // Si es diferente a null significa que el arreglo en el indice x ya posee datos
                if($ciudades[dep] != null)
                {
                    $ciudades[dep].push([ciu, nombre]);
                }
                else
                {
                    // Si es la primera vez se asigna el primer dato como un arreglo
                    $ciudades[dep] = [[ciu, nombre]];
                }
            });
        });
        
        $(".fecha").datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
        });
        
    });
    </script>
    <style>
        .border-group{
            border: 1px dashed;
        }
    </style>
</head>

<body>
    <?php require($r . 'incluir/src/login.php') ?>
    <section id="principal">
        <?php require($r . 'incluir/src/cabeza.php') ?>
        <?php require($r . 'incluir/src/menu.php') ?>
        <article id="cuerpo" class="form-style">
            <article class="mapa">
                <a href="#">Principal</a>
                <div class="mapa_div"></div>
                <a href="#">Bodega</a>
                <div class="mapa_div"></div>
                <a href="#">Documentos</a>
                <div class="mapa_div"></div>
                <a class="current">Entrada</a>
            </article>
            <div id="msj"></div>
            <div class="row">
                <div class="col-md-12">
                    <fieldset id="field" class="ui-widget ui-widget-content ui-corner-all col-md-12">
                        <legend class="ui-widget ui-widget-header ui-corner-all" style="font-size: 20px!important; background: #ececec;">Buscar orden de salida</legend>
                        <div class="row">
                            <div class="col-md-5">
                                <label for="">Tipo de documento de salida</label>
                                <select name="tipodoc" class="form-control" id="tipodoc">
                                    <?php
                                        $documentos = $db->query("SELECT * from tipdocumentos WHERE tiptipo = 'SALINV' ");
                                        while($data = $documentos->fetch(PDO::FETCH_OBJ))
                                        {
                                            echo "<option value='".$data->tipid."'>$data->tipnombre</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-5">
                                <label for=""># Orden de salida </label>
                                <input type="text" id="orden" class="form-control">
                            </div>
                            <div class="col-md-2">
                                <label for="" style="visibility:hidden">Agregar</label>
                                <button class="btn btn-success" onclick="buscar_orden();"><i class="fas fa-plus-circle"></i></button>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div id="" class="row">
                <div class="col-md-12">
                    <fieldset id="field" class="ui-widget ui-widget-content ui-corner-all col-md-12">
                        <legend class="ui-widget ui-widget-header ui-corner-all" style="font-size: 20px!important; background: #ececec;">FACTURA A DISTRIBUIDORES <span class="text-danger">#<?php echo $numero?></span></legend>
                        <div id="msj2"></div>
                        <div class="row mx-1" id="detalle">
                            <?php 
                                $consulta = "SELECT m.movnumero as m_movnumero, m.movfecha as m_movfecha, m.movprefijo as m_movprefijo, t.tipnombre
                                FROM movinventario as f
                                INNER JOIN mov_distribuidores as d ON d.movimiento = f.movnumero
                                INNER JOIN movinventario as m ON m.movnumero = d.numero AND m.movprefijo = d.tipo
                                INNER JOIN tipdocumentos as t ON tipid = m.movprefijo
                                WHERE f.movprefijo = 'FD' AND f.movnumero = '$orden'"; 
                                //echo $consulta;
                                $ordenes = $db->query($consulta);
                                //var_dump($ordenes);
                                
                                $monto_total = 0;
                                while($orden = $ordenes->fetch(PDO::FETCH_OBJ))
                                {
                                    $consulta = "SELECT m.movnumero, m.movdocumento, m.movprefijo, m.movfecha, m.created_at as mcreated, 
                                    d.dmoproducto, d.dmocantidad, d.dmounitario, d.dmototal, d.created_at as dcreated,
                                    p.pronombre 
                                    FROM movinventario as m 
                                    LEFT JOIN detmovimientos as d ON d.dmonumero = m.movnumero and m.movprefijo = d.dmoprefijo
                                    LEFT JOIN productos as p ON proid = d.dmoproducto
                                    WHERE m.movnumero = '$orden->m_movnumero' AND m.movprefijo = '$orden->m_movprefijo' ORDER BY d.created_at";
                                    $productos = $db->query($consulta);

                                    $index = 0;
                                    $html = "";
                                    while($producto = $productos->fetch(PDO::FETCH_OBJ))
                                    {
                                        
                                        if($index == 0)
                                        {
                                            $tipodoc = $orden->m_movprefijo;
                                            $tipodoc_n = $orden->tipnombre;
                                            $html .= '<div class="border-group col-md-12" id="mov-'.$orden->m_movprefijo."-".$orden->m_movnumero.'">'.
                                                '<div class="msj-detalle"></div>'.
                                                '<input type="hidden" class="edit-movimiento" value="'.$orden->m_movnumero.'" disabled>'.
                                                '<input type="hidden" class="edit-prefijo" value="'.$orden->m_movprefijo.'" disabled>'.
                                                '<div class="col-md-12 my-2">'.
                                                    '<label class="col-md-4 m-0 font-weight-bold">Tipo: '.$tipodoc_n.'</label>'.
                                                    '<label class="col-md-2 m-0 font-weight-bold">Orden: '.$orden->m_movnumero.'</label>'.
                                                    '<label class="col-md-3 m-0 font-weight-bold">F. orden: '.$orden->m_movfecha.'</label>'.
                                                    '<button class="col-md-1 btn btn-info" onclick="recargar_orden(\''.$orden->m_movnumero.'\',\''.$tipodoc.'\')"><i class="fas fa-sync-alt"></i></button>'.
                                                    '<button class="col-md-1 btn btn-danger" onclick="remover_orden(\''.$orden->m_movnumero.'\',\''.$tipodoc.'\')"><i class="fas fa-trash"></i></button>'.
                                                '</div>'.
                                                '<div class="col-md-12">'.
                                                    '<div class="input-group">'.
                                                        '<label type="text" class="form-control detalle-producto col-md-1 not-w mr-0"disabled>CODIGO</label>'.
                                                        '<label type="text" class="form-control detalle-producto col-md-3 not-w mr-0"disabled>PRODUCTO</label>'.
                                                        '<div class="input-group-append col-md-8 p-0">'.
                                                            '<label type="text" class="form-control detalle-cantidad col-md-3 not-w mr-0"disabled>CANTIDAD</label>'.
                                                            '<label type="text" class="form-control detalle-precio col-md-3 not-w mr-0"disabled>PRECIO</label>'.
                                                            '<label type="text" class="form-control col-md-3 not-w mr-0"disabled>TOTAL</label>'.
                                                        '</div>'.
                                                    '</div>'.
                                                '</div>';


                                            $index++;
                                        }
                                        
                                        $html .= '<div class="col-md-12" id="pro-'.$producto->dmoproducto.'">'.
                                            '<div class="input-group mb-3">'.
                                                '<input type="text" class="form-control detalle-producto col-md-1" disabled value="'.$producto->dmoproducto.'">'.
                                                '<input type="text" class="form-control detalle-producto col-md-3" disabled value="'.$producto->pronombre.'">'.
                                                '<div class="input-group-append col-md-8 p-0">'.
                                                    '<input type="hidden" class="detalle-code edit-producto" value="'.$producto->dmoproducto.'">'.
                                                    '<input type="text" class="form-control detalle-cantidad col-md-3 edit-cantidad" onkeypress="is_enter_key(event, this)" value="'.$producto->dmocantidad.'">'.
                                                    '<input type="text" class="form-control detalle-precio col-md-3 edit-vunitario" onkeypress="is_enter_key(event, this)" value="'.$producto->dmounitario.'">'.
                                                    '<input type="text" class="form-control detalle-total col-md-3 edit-total" disabled value="'.$producto->dmototal.'">'.
                                                    '<button type="button" class="btn btn-warning" onclick="edit_product_FD(\''.$producto->dmoproducto.'\', this);"  >'.
                                                        '<i class="fas fa-pencil-alt"></i>'.
                                                    '</button>'.
                                                '</div>'.
                                            '</div>'.
                                        '</div>';

                                        $monto_total += $producto->dmototal;
                                    }
                                    $html .= '</div>';
                                    $monto_total = round($monto_total,2);
                                    echo $html;
                                }
                            ?>


                        </div>
                    </fieldset>
                </div>
            </div>

            <div class="col-md-12 mb-3 text-center" id="border-monto">
                <p class="font-weight-bold" style="font-size:20px">MONTO TOTAL</p>
                <div class="col-md-12 d-flex justify-content-center">
                    <input type="text" id="monto-total" class="form-control col-md-4 text-center"  readonly value="<?php echo $monto_total;?>">
                </div>
            </div>


            <!-- DATOS GENERALES -->
            <div class="col-md-12">
                <fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12 fieldset-espejo">
                    <legend class="ui-widget ui-widget-header ui-corner-all">Datos Generales</legend>
                    
                    <input type="hidden" id="empresa" name="empresa" value="<?php echo $empresa ?>" />
                    <input type="hidden" id="prefijo" name="prefijo" value="<?php echo $prefijo ?>" />
                    <input type="hidden" id="movimiento" name="numero" value="<?php echo $numero ?>" />
                    <input type="hidden" id="infoextra" name="infoextra" value="<?php echo $id_info ?>" />
                    <div class="row">
                        <div class="col-md-3">
                            <label for=""><span style="color:red">*</span>Fecha</label>
                            <input type="text" id="fecha" class="form-control validate[custom[date], required] fecha" value="<?php echo $movfecha?>" name="fecha">
                        </div>
                        <div class="col-md-3">
                            <label for=""><span style="color:red">*</span>Departamento</label>
                            <select name="departamento" id="departamento" class="form-control validate[required] selectpicker">
                            <option value=""></option>
                            <?php 
                                $departamentos = $db->query("SELECT * FROM departamentos ORDER BY depnombre ASC");
                                while ($d = $departamentos->fetch(PDO::FETCH_ASSOC)) {
                                    if($departamento != "")
                                    {
                                        if($departamento == $d['depid'])
                                            echo "<option selected value='" . $d['depid'] . "'>" . $d['depnombre'] . "</option>";
                                        else
                                            echo "<option value='" . $d['depid'] . "'>" . $d['depnombre'] . "</option>";
                                    }
                                    else
                                        echo "<option value='" . $d['depid'] . "'>" . $d['depnombre'] . "</option>";
                                }
                            ?>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label for=""><span style="color:red">*</span>Ciudad</label>
                            <select name="ciudad" id="ciudad" class="form-control validate[required]">
                                <?php 
                                    if($ciudad_c != "")
                                    {
                                        echo "<option selected value='" . $ciudad_c . "'>" . $ciudad_n . "</option>";
                                    }
                                ?>
                            </select>
                        </div>
                        
                        <div class="col-md-3">
                            <label for="">Moneda</label>
                            <select name="simbolo" id="simbolo" class="form-control validate['required']">
                                <?php 
                                    $monedas = $db->query("SELECT * FROM monedas ORDER BY nombre ASC");
                                    while ($moneda = $monedas->fetch(PDO::FETCH_ASSOC)) {
                                        
                                        if($moneda['currency'] == $currency)
                                        echo "<option selected value='" . $moneda['currency'] . "'>" . $moneda['nombre']. "(" . $moneda['simbolo'].")</option>";
                                        else
                                        echo "<option value='" . $moneda['currency'] . "'>" . $moneda['nombre']. "(" . $moneda['simbolo'] . ")</option>";
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label for=""><span style="color:red">*</span>Distribuidor</label>
                            <input type="text" id="distribuidor" class="form-control validate[required, maxSize[50]] uppercase" value="<?php echo $distribuidor?>" name="distribuidor">
                        </div>
                        <div class="col-md-4">
                            <label for="">Entregado</label>
                            <input type="text" id="entregado" class="form-control validate[maxSize[40]] uppercase" value="<?php echo $entregado?>" name="entregado">
                        </div>
                        <div class="col-md-4">
                            <label for="">Bodeguero</label>
                            <input type="text" id="bodeguero" class="form-control validate[maxSize[40]] uppercase" value="<?php echo $bodeguero?>" name="bodeguero">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label for="">Telefono 1</label>
                            <input type="text" id="telefono1" class="form-control validate[maxSize[15]]" value="<?php echo $telefono1?>" name="telefono1">
                        </div>
                        <div class="col-md-4">
                            <label for="">Telefono 2</label>
                            <input type="text" id="telefono2" class="form-control validate[maxSize[15]]" value="<?php echo $telefono2?>" name="telefono2">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label for="">Dirección</label>
                            <textarea type="text" id="direccion" class="form-control validate[maxSize[200] uppercase" name="direccion" rows="3"><?php echo $direccion?></textarea>
                        </div>
                        <div class="col-md-6">
                            <label>Comentario:</label> 
                            <textarea name="texto" id="comentario" style="text-transform: uppercase" cols="100" rows="3" class="form-control validate[maxSize[400]]"><?php echo $comentario ?></textarea>
                        </div>
                    </div>
                </fieldset>
            </div>
            <div id="msj3"></div>
            <div class="col-md-12">
                <button class="btn btn-primary btn-block" onclick="guardar()">GUARDAR E IMPRIMIR</button>
            </div>
            
        </article>
        <?php require($r . 'incluir/src/pie.php') ?>
    </section>
	<?php require($r . 'incluir/src/loading.php'); ?>

</body>
<script>
    $("#orden").keypress(function(e){
        if (e.which == 13) {
            buscar_orden();
            return false;
        }
    });
    function buscar_orden(element = "#detalle", orden = 0, tipo = 0, guardar = 0)
    {
        if(orden == 0)
        var orden = $("#orden").val();
        if(tipo == 0)
        var tipo = $("#tipodoc").val();

        if($("#mov-"+tipo+"-"+orden).length > 0 && element == "#detalle")
        {
            make_alert({'type':'warning', 'message':'¡Este movimiento ya se encuentra en la lista!'});
            return;
        }
        var movimiento = $("#movimiento").val();
        var ruta = 'buscar_orden.php?orden='+orden+"&tipo="+tipo+"&guardar="+guardar+"&movimiento="+movimiento;
        $("#loading").css('display', 'block');
        $.ajax({
            url: ruta,
            type: 'GET',
            success: function(res){
                var res = JSON.parse(res);
                if(res.code == 0)
                {
                    var html = "";
                    var index = 0;
                    var status = 0;
                    res.data.forEach(function(data){
                        if(index == 0)
                        {
                            var tipodoc = $("#tipodoc").val();
                            var tipodoc_n = $("#tipodoc option:selected").text();
                            if(guardar == 0)
                            html += '<div class="border-group col-md-12" id="mov-'+data.movprefijo+'-'+data.movnumero+'">';
                            html += '<div class="msj-detalle"></div>'+
                                '<input type="hidden" class="edit-movimiento" value="'+data.movnumero+'" disabled>'+
                                '<input type="hidden" class="edit-prefijo" value="'+data.movprefijo+'" disabled>'+
                                '<div class="col-md-12 my-2">'+
                                    '<label class="col-md-4 m-0 font-weight-bold">Tipo: '+tipodoc_n+'</label>'+
                                    '<label class="col-md-2 m-0 font-weight-bold">Orden: '+data.movnumero+'</label>'+
                                    '<label class="col-md-3 m-0 font-weight-bold">F. orden: '+data.movfecha+'</label>'+
                                    '<button class="col-md-1 btn btn-info" onclick="recargar_orden(\''+data.movnumero+'\',\''+tipodoc+'\')"><i class="fas fa-sync-alt"></i></button>'+
                                    '<button class="col-md-1 btn btn-danger" onclick="remover_orden(\''+data.movnumero+'\',\''+tipodoc+'\')"><i class="fas fa-trash"></i></button>'+
                                '</div>';

                            if(data.dmoproducto == null)
                            {
                                make_alert({'type':'warning', 'message':'¡El movimiento existe pero no posee ningun producto asociado!'});
                                status = 1;
                                console.log("una vez");
                                return;
                            }
                            html += '<div class="col-md-12">'+
                                '<div class="input-group">'+
                                    '<input type="hidden" class="detalle-code" value="" disabled>'+
                                    '<label type="text" class="form-control detalle-producto col-md-1 not-w mr-0"disabled>CODIGO</label>'+
                                    '<label type="text" class="form-control detalle-producto col-md-3 not-w mr-0"disabled>PRODUCTO</label>'+
                                    '<div class="input-group-append col-md-8 p-0">'+
                                        '<label type="text" class="form-control detalle-cantidad col-md-3 not-w mr-0"disabled>CANTIDAD</label>'+
                                        '<label type="text" class="form-control detalle-precio col-md-3 not-w mr-0"disabled>PRECIO</label>'+
                                        '<label type="text" class="form-control col-md-3 not-w mr-0"disabled>TOTAL</label>'+
                                    '</div>'+
                                '</div>'+
                            '</div>';


                            index++;
                        }
                        
                        html += '<div class="col-md-12" id="pro-'+data.dmoproducto+'">'+
                            '<div class="input-group mb-3">'+
                                '<input type="text" class="form-control detalle-producto col-md-1" disabled value="'+data.dmoproducto+'">'+
                                '<input type="text" class="form-control detalle-producto col-md-3" disabled value="'+data.pronombre+'">'+
                                '<div class="input-group-append col-md-8 p-0">'+
                                    '<input type="hidden" class="detalle-code edit-producto" value="'+data.dmoproducto+'" disabled>'+
                                    '<input type="text" class="form-control detalle-cantidad col-md-3 edit-cantidad" onkeypress="is_enter_key(event, this)" value="'+data.dmocantidad+'">'+
                                    '<input type="text" class="form-control detalle-precio col-md-3 edit-vunitario" onkeypress="is_enter_key(event, this)" value="'+data.dmounitario+'">'+
                                    '<input type="text" class="form-control detalle-total col-md-3 edit-total" value="'+data.dmototal+'" disabled>'+
                                    '<button type="button" class="btn btn-warning" onclick="edit_product_FD(\''+data.dmoproducto+'\', this);"  >'+
                                        '<i class="fas fa-pencil-alt"></i>'+
                                    '</button>'+
                                '</div>'+
                            '</div>'+
                        '</div>';

                    });
                    if(guardar == 0)
                    html += "</div>";
                    if(element != "#detalle")
                    {
                        $(element).empty();
                        status = 2;
                        
                    }
                    
                    $(element).append(html);
                    if(status == 0)
                    make_alert({'message':res.msj});
                    if(status == 2)
                    {
                        var obj = $("#mov-"+tipo+"-"+orden+" .msj-detalle");
                        make_alert({'object': obj, 'type':'info', 'message':"¡Se ha recargado la orden!"});
                    }
                }
                if(res.code == 1)
                    make_alert({'type':'danger', 'message':res.msj});
                
                calcular_total()
                $("#loading").css('display', 'none');
                $("#orden").focus();
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            make_alert({'type':'danger', 'message':'Ocurrio un error, intentelo nuevamente, si el error persiste solicitar asistencia tecnica'});
            $("#loading").css('display', 'none');
        }); 
    }

    $("#departamento").change(function(){
        $("#ciudad").empty();
        // Por defecto
        var html = '';
        $("#ciudad").append(html);

        dep = $(this).val();
		if(dep == "")
			cant = 0;
		else
        cant = $ciudades[dep].length;
        for(var i = 0; i < cant; i++)
        {
            html = "<option value='" + $ciudades[dep][i][0] + "'>" + $ciudades[dep][i][1] + "</option>";
            $("#ciudad").append(html);
        }
        $('#ciudad').selectpicker('refresh');
    });

    function make_alert({object = $('#msj'), type = 'success', message = ''})
    {
        object.css('display', 'none');
        object.empty();

        var alerta = 
        '<div class="alert alert-'+ type +' alert-dismissible fade show" role="alert">'+
            message +
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
                '<span aria-hidden="true">&times;</span>'+
            '</button>'+
        '</div>';

        object.append(alerta);
        object.slideDown();
    }

    function recargar_orden(orden, tipo)
    {
        var guardar = 1;
        buscar_orden("#mov-"+tipo+"-"+orden, orden, tipo, guardar);
    }
    function remover_orden(orden, tipo)
    {
        var movimiento = $("#movimiento").val();
        var ruta = "remover_orden.php?orden="+orden+"&tipo="+tipo+"&movimiento="+movimiento;
        $("#loading").css('display', 'block');
        $.get(ruta, function(res){
            res = JSON.parse(res);
            if(res.code == 0)
            {
                make_alert({'object': $('#msj2'), 'type':'warning', 'message': res.msj});
                $("#mov-"+tipo+"-"+orden).eq(0).remove();
                calcular_total()
            }
            if(res.code == 1)
                make_alert({'object': $('#msj2'), 'type':'danger', 'message': res.msj});
            
            $("#loading").css('display', 'none');
        });

    }

    var prueba;
    function edit_product_FD(producto, element)
    {
        //prueba = fila_orden;
        var fila_orden = $(element).parents(".border-group").eq(0);
        var fila_producto = $(element).parents(".input-group-append").eq(0);
        var new_cantidad = parseInt(fila_producto.find(".edit-cantidad").val());
        var new_vunitario = parseFloat(fila_producto.find(".edit-vunitario").val());
        var producto = fila_producto.find(".edit-producto").val();
        fila_producto.find(".edit-total").val((new_cantidad*new_vunitario).toFixed(2));

        var empresa = $("#empresa").val();
        var prefijo = fila_orden.find(".edit-prefijo").val();
        var numero = fila_orden.find(".edit-movimiento").val();

        var obj_msj = fila_orden.find('.msj-detalle');

        var url = "salida_editar_FD.php?empresa="+empresa+"&prefijo="+prefijo+"&numero="+numero+"&producto="+producto+"&cantidad="+new_cantidad+"&vunitario="+new_vunitario;
        console.log(url);
        $("#loading").css('display', 'block');
        
        $.ajax({
            type: "GET",
            url: url,
            success:function(res){ 
                res = JSON.parse(res);
                if(res.code == 0)
                    make_alert({'object': obj_msj, 'type':'info', 'message':res.msj});
                else
                    make_alert({'object': obj_msj, 'type':'info', 'message':res.msj});

                calcular_total();
                $("#loading").css('display', 'none');
            }, fail: function(){
                make_alert({'object': obj_msj, 'type':'danger', 'message':'NO se edito la cantidad del producto, intentelo nuevamente o solicite soporte tecnico'});
                $("#loading").css('display', 'none');
            }
        });

    }

    function is_enter_key(e, element)
    {
        if (e.which == 13) {
            edit_product_FD(null, element);
            return false;
        }
    }

    function calcular_total()
    {
        var totales = $(".detalle-total");
        cantidad = totales.length;
        
        var total = 0;
        for(var i = 0; i < cantidad; i++)
            total += parseFloat(totales.eq(i).val());

        $("#monto-total").val(total.toFixed(2));
    }
    
    
    function validacion()
    {
        s0 = $("#fecha").validationEngine('validate');
        s1 = $("#departamento").validationEngine('validate');
        s2 = $("#ciudad").validationEngine('validate');
        s3 = $("#distribuidor").validationEngine('validate');
        s4 = $("#entregado").validationEngine('validate');
        s5 = $("#bodeguero").validationEngine('validate');
        s6 = $("#telefono1").validationEngine('validate');
        s7 = $("#telefono2").validationEngine('validate');
        s8 = $("#direccion").validationEngine('validate');
        s9 = $("#comentario").validationEngine('validate');
        s10 = $("#simbolo").validationEngine('validate');
        if(s0 || s1 || s2 || s3 || s4 || s5 || s6 || s7 || s8 || s9 || s10)
            return true;
        else
            return false;
    }

    function guardar()
    {
        if(validacion())
            return;
            
        var fecha = $("#fecha").val();
        var departamento = $("#departamento").val();
        var ciudad = $("#ciudad").val();
        var distribuidor = $("#distribuidor").val();
        var entregado = $("#entregado").val();
        var bodeguero = $("#bodeguero").val();
        var telefono1 = $("#telefono1").val();
        var telefono2 = $("#telefono2").val();
        var direccion = $("#direccion").val();
        var comentario = $("#comentario").val();
        var simbolo = $("#simbolo").val();
        var info_extra = $("#infoextra").val();
        var movimiento = $("#movimiento").val();
        var ruta = "actualizar_movimiento.php";
        $("#loading").css('display', 'block');
        $.ajax({
            url:ruta,
            type: 'POST',
            data: { 'fecha': fecha,
                    'departamento': departamento,
                    'ciudad':ciudad,
                    'distribuidor':distribuidor, 
                    'entregado':entregado, 
                    'bodeguero':bodeguero, 
                    'telefono1':telefono1, 
                    'telefono2':telefono2, 
                    'direccion':direccion, 
                    'comentario':comentario,
                    'simbolo': simbolo,
                    'infoextra':info_extra,
                    'movimiento':movimiento },
            success: function(res)
            {
                var res = JSON.parse(res);
                if(res.code == 0)
                {
                    make_alert({'object':$("#msj3"), 'message':res.msj});
                    location.href="finalizar.php?orden="+res.orden;
                }
                if(res.code == 1)
                {
                    make_alert({'object':$("#msj3"), 'type':'danger', 'message':res.msj});
                    $("#loading").css('display', 'none');
                }
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            make_alert({'object':$("#msj3"), 'type':'danger', 'message':'Ocurrio un error, intentelo nuevamente, si el error persiste solicitar asistencia tecnica'});
            $("#loading").css('display', 'none');
        }); 
    }
</script>

</html>