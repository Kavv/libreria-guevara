<?php
$r = '../../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

$empresa = 'F02900100885652';
$prefijo = 'FD';
// Obtenemos el identificar de la factura a distribuidores
$movimiento = 0;
if(isset($_POST['orden']))
    $movimiento = $_POST['orden'];

$result = new stdClass();

if($movimiento == 0 || $movimiento == "" || $movimiento == NULL || $movimiento < 1)
{
    $result->code = 1;
    $result->msj = 'El numero de factura no es valido';
    echo json_encode($result);
    exit();
}
//Consultamos si hay un movimiento en proceso
$num = $db->query("SELECT * from mov_distribuidores where movimiento = '$movimiento';")->rowCount();

if($movimiento == 0 || $movimiento == "" || $movimiento == NULL || $movimiento < 1)
{
    $result->code = 1;
    $result->msj = 'El numero de factura no es valido';
    echo json_encode($result);
    exit();
}

if ($num > 0) {
    $result->code = 1;
    $result->msj = 'Esta factura aún tiene ordenes asignadas, por favor primero desvincule las ordenes asigndas';
    echo json_encode($result);
    exit();
}
// Creamos el mov
$qry = $db->query("DELETE from movinventario WHERE movprefijo = '$prefijo' AND movnumero = '$movimiento';");

if(!$qry)
{
    $result->code = 1;
    $result->msj = 'Ocurrio un error al intentar eliminar la factura';
    echo json_encode($result);
    exit();
}


$result->code = 0;
$result->orden = $movimiento;
$result->msj = 'Eliminado exitosamente!';
echo json_encode($result);
exit();

?>