<?php
$r = '../../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
require($r . 'incluir/fpdf/fpdf.php');

$numero = $_GET['numero'];
$prefijo = 'FD';
$sql = "SELECT movinventario.*, empnombre, tipnombre, infoextra.*, departamentos.*, ciudades.*, monedas.simbolo FROM movinventario 
INNER JOIN empresas ON movempresa = empid 
INNER JOIN tipdocumentos ON movprefijo = tipid 
LEFT JOIN infoextra ON infoextra.id = movinfo
LEFT JOIN departamentos ON depid = departamento
LEFT JOIN ciudades ON (ciuid = ciudad AND ciudepto = departamento)
LEFT JOIN monedas ON currency = movmoneda
WHERE movprefijo = '$prefijo' AND movnumero = '$numero'";

$row = $db->query($sql)->fetch(PDO::FETCH_ASSOC);

$infoextra = $entregado = $bodeguero = $comentario = $entregado = $telefono1 = $telefono2 = $direccion = "";
if($row)
{
    $digitador = '';
    $id_info = $row['movinfo'];
    if($id_info != "")
    {
        $infoextra = $db->query("SELECT infoextra.*, ciunombre FROM infoextra INNER JOIN ciudades ON (ciuid = ciudad AND ciudepto = departamento) WHERE id = '$id_info'")->fetch(PDO::FETCH_ASSOC);
        $entregado = $infoextra['entregado'];
        $bodeguero = $infoextra['bodeguero'];
        $comentario = $infoextra['comentario'];
        $telefono1 = $infoextra['telefono1'];
        $telefono2 = $infoextra['telefono2'];
        if(isset($infoextra['digitador']))
        $digitador = $infoextra['digitador'];
        $distribuidor = $infoextra['distribuidor'];
        $direccion = $infoextra['direccion'];
    }
}

// Obtenemos la moneda correspondiente
$simbolo = $row['simbolo'];
$descuento = $row['movdescuento'];

$fecha = $row['movfecha'];
$fechaComoEntero = strtotime($fecha);

$year = date("y", $fechaComoEntero);
$month = date("m", $fechaComoEntero);
$day = date("d", $fechaComoEntero);

$nombre = strupperEsp($distribuidor);


$w_codigo = 16;
$w_cantidad = 16;
$w_producto = 64;
$w_precio = 18;
$w_total = 18;
$max_w = $w_codigo + $w_cantidad + $w_producto + $w_precio + $w_total;

//$margen_r = 8;
$margen_r = 0.1;

$font_size = 8;
$border = 0;


class PDF extends FPDF
{
    function Header()
    {
        global $row, $numero, $day, $month, $year, $nombre, $telefono1, $telefono2, $direccion, $direccion, $direccion;
        global $font_size, $margen_r, $max_w, $border;
        $this->SetTitle("FACTURA A DISTRIBUIDORES #".$numero);
        #Establecemos los márgenes izquierda, arriba y derecha:
        $this->SetMargins(5,0);


        $this->SetFont('LucidaConsole', '', $font_size);
        $this->ln(7);
        // Día Mes Año
        $this->Cell($margen_r, 5, '', 0, 0);
        $this->Cell($max_w, 5, $day ."       " . $month ."       " . $year . "  ", $border, 1, 'R');

        // Nombre y Cedula
        $this->ln(7);
        $this->Cell($margen_r, 5, '', 0, 0);
        $this->Cell($max_w*0.66, 5, utf8_decode($nombre), $border, 1);
        //$pdf->Cell($max_w*0.33, 5, $row['solcliente'], $border, 1, 'R');

        // Dir y Tel
        $this->ln(6);
        $this->Cell($margen_r, 5, '', 0, 0);
        $this->Cell($max_w*0.66, 5, utf8_decode($row['depnombre']." / ".$row['ciunombre']), $border, 0);
        $this->Cell($max_w*0.33, 5, $telefono1 . " / " . $telefono2, $border, 1, 'R');

        // Direccion
        $this->ln(1);
        $direccion1 = substr($direccion, 0, 76);
        $direccion2 = substr($direccion, 76, 76);
        $direccion3 = substr($direccion, 152, 76);

        $this->Cell($margen_r, 4, '', 0, 0);
        $this->Cell($max_w, 4, utf8_decode($direccion1), $border, 1);
        $this->Cell($margen_r, 4, '', 0, 0);
        $this->Cell($max_w, 4, utf8_decode($direccion2), $border, 1);
        $this->Cell($margen_r, 4, '', 0, 0);
        $this->Cell($max_w, 4, utf8_decode($direccion3), $border, 1);



        $this->Cell($margen_r, 5, '', 0, 0);
        $this->Cell(18, 5, '', 0, 0, 'C');
        $this->Cell(18, 5, '', 0, 0, 'C');
        $this->Cell(60, 5, '', 0, 0, 'C');
        $this->Cell(18, 5, '', 0, 0, 'C');
        $this->Cell(18, 5, '', 0, 1, 'C');
    }

    function Footer()
    {
        
        global $descuento, $comentario, $font_size, $w_codigo, $w_cantidad, $w_producto, $margen_r, $w_observacion, $border, $w_precio, $w_total, $simbolo, $subtotal;
        $this->SetFont('LucidaConsole', '', $font_size);
        // Observaciones

        $this->ln(2);
        $observacion1 = substr($comentario,0, 40);
        $observacion2 = substr($comentario,40, 55);
        $observacion3 = substr($comentario,95, 55);
        $observacion4 = substr($comentario,150, 55);

        $w_observacion = $w_codigo + $w_cantidad + $w_producto;

        $this->Cell($margen_r, 5, '', 0, 0);
        $this->Cell($w_observacion, 5, "               ".utf8_decode($observacion1), $border, 0);
        $this->Cell($w_precio, 5, "", $border, 0);
        $this->Cell($w_total, 5, $simbolo. "$subtotal", $border, 1, 'R');

        $this->Cell($margen_r, 5, '', 0, 0);
        $this->Cell($w_observacion, 5, utf8_decode($observacion2), $border, 0);
        $this->Cell($w_precio, 5, "", $border, 0);
        $this->Cell($w_total, 5, $simbolo. "$descuento", $border, 1, 'R');

        $this->Cell($margen_r, 5, '', 0, 0);
        $this->Cell($w_observacion, 5, utf8_decode($observacion3), $border, 0);
        $this->Cell($w_precio, 5, "", $border, 0);
        $this->Cell($w_total, 5, "", $border, 1, 'R');

        $gran_total = $subtotal - ($descuento);
        $this->Cell($margen_r, 5, '', 0, 0);
        $this->Cell($w_observacion, 5, utf8_decode($observacion4), $border, 0);
        $this->Cell($w_precio, 5, "", $border, 0);
        $this->Cell($w_total, 5, $simbolo. "$gran_total", $border, 1, 'R');

    }
}


$pdf = new PDF('P','mm',array(210,158));
$pdf->AddFont('LucidaConsole', '', 'lucon.php');
$pdf->AliasNbPages();
$pdf->AddPage();

//var_dump($ordenes);


$consulta = "SELECT d.dmoproducto, SUM(d.dmocantidad) as dmocantidad, SUM(d.dmounitario) as dmounitario, 
SUM(d.dmototal) as dmototal, MIN(d.created_at) as dcreated, p.pronombre
FROM movinventario as m
LEFT JOIN detmovimientos as d ON d.dmonumero = m.movnumero and m.movprefijo = d.dmoprefijo
LEFT JOIN productos as p ON proid = d.dmoproducto
INNER JOIN mov_distribuidores md on md.movimiento = '$numero' AND md.numero = m.movnumero AND md.tipo = m.movprefijo
GROUP BY d.dmoproducto, p.pronombre
ORDER BY dcreated";


$productos = $db->query($consulta);
$cantidad_max = $productos->rowCount();
$restante = $cantidad_max;

$pdf->SetFont('LucidaConsole', '', $font_size);


$index_fila = 0;
$max_filas = 20;

$subtotal = 0;
$need_footer = true;
while($producto = $productos->fetch(PDO::FETCH_ASSOC))
{
    $need_footer = false;
    $p_id = $producto['dmoproducto'];
    $p_nombre = substr($producto['pronombre'], 0, 35);
    $p_cantidad = $producto['dmocantidad'];
    $p_precio = $producto['dmounitario'];
    $p_total = $producto['dmototal'];

    $pdf->Cell($margen_r, 4, '', 0, 0);
    $pdf->Cell($w_codigo, 4, $p_id, $border, 0, 'R');
    $pdf->Cell($w_cantidad, 4, $p_cantidad, $border, 0, 'R');
    $pdf->Cell($w_producto, 4, utf8_decode($p_nombre), $border, 0);
    $pdf->Cell($w_precio, 4, $simbolo . $p_precio, $border, 0, 'R');
    $pdf->Cell($w_total, 4, $simbolo . $p_total, $border, 1, 'R');
    $subtotal += $p_total;
    $index_fila++;
    if($index_fila == $max_filas)
    {
        $restante -= $index_fila; 
        if(($restante) > 0)
        {
            $pdf->AddPage();
            $index_fila = 0;
        }
    }
}

while($index_fila < $max_filas)
{
    $pdf->Cell($margen_r, 4, '', 0, 0);
    $pdf->Cell($w_codigo, 4, "", $border, 0, 'C');
    $pdf->Cell($w_cantidad, 4, "", $border, 0, 'C');
    $pdf->Cell($w_producto, 4, "", $border, 0);
    $pdf->Cell($w_precio, 4, "", $border, 0, 'R');
    $pdf->Cell($w_total, 4, "", $border, 1, 'R');
    $index_fila++;
}





$pdf->Output("FACTURA A DISTRIBUIDOR #".$numero.".pdf" , 'I');
?>






