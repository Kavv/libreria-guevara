<?php
    $r = '../../../../';
    require($r . 'incluir/session.php');
    require($r . 'incluir/connection.php');

    $orden = trim($_GET['orden']);
    $tipo = $_GET['tipo'];
    $movimiento = $_GET['movimiento'];
    $result = new stdClass();

    if($orden == "" || $orden == null || $orden == "null" || $orden < 1)
    {
        $result->code = 1;
        $result->msj = "El numero de orden no es valido";
        echo json_encode($result);
        exit();
    }

    $qry = $db->query("SELECT * FROM mov_distribuidores WHERE movimiento = '$movimiento' AND tipo = '$tipo' AND numero = '$orden'");
    if(!$qry || $qry->rowCount() != 1)
    {
        $result->code = 1;
        $result->msj = "No se encontro el elmento que desea resvincular, si el error persiste solicite asistencia tecnica";
        echo json_encode($result);
        exit();
    }

    $qry = $db->query("DELETE FROM mov_distribuidores WHERE movimiento = '$movimiento' AND tipo = '$tipo' AND numero = '$orden'");
    if(!$qry)
    {
        $result->code = 1;
        $result->msj = "Ocurrio un error al tratar de desvincular la orden #$orden";
        echo json_encode($result);
        exit();
    }



    $result->code = 0;
    $result->msj = "Se desvinculo la orden #$orden";
    echo json_encode($result);
    exit();
?>
