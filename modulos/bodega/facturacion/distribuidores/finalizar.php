<?php
$r = '../../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

$numero = $_GET['orden'];

?>


<!doctype html>

<html lang="es">

<head>
    <title>PDF FACTURA A DISTRIBUIDORES</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">

    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>
    <style>
    @media print {
        iframe {
            width: 100%;
            height: 100%;
        }
    }
    </style>
</head>
<body>
<?php require($r . 'incluir/src/login.php') ?>
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <?php require($r . 'incluir/src/menu.php') ?>
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a href="#">Bodega</a>
            <div class="mapa_div"></div>
            <a href="#">Documentos</a>
            <div class="mapa_div"></div>
            <a class="current">Salida</a>
        </article>
        <article id="contenido">
            <p align="center">
                <iframe src="<?php echo 'pdf.php?numero=' . $numero?>"
                        width="800" height="550"></iframe>
            </p>
            <p class="boton">
                <button type="button" class="btn btn-primary btnatras"
                        onClick="carga(); location.href = 'insertar.php'">
                    Agregar nueva factura a distribuidores
                </button>
            </p>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php') ?>
</section>
</body>
</html>