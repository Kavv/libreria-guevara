<?php
$r = '../../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');


?>
<!doctype html>

<html lang="es">

<head>
    <title>CONSULTAR FACTURA DISTRIBUIDORES</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">
    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-not-form.php');
    ?>

    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

    <link rel="stylesheet" href="<?php echo $r.'incluir/librerias/fontawesome/css/all.css'?>" />
    <script src="<?php echo $r.'incluir/librerias/fontawesome/js/all.js'?>"></script>

    <link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
    
    <style>
        
    </style>
    <script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {

        //$('#producto').selectpicker();
        
        $('#form').validationEngine({
            showOneMessage: true,
            onValidationComplete: function(form, status) {
                if (status) {
                    return true;
                }
            }
        });
        $(".fecha").datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
        });

        $("#dialog-message").dialog({
            height: 80,
            width: 'auto',
            modal: true
        });
        
        // Almacenar las ciudades
        ruta = "<?php echo $r . 'modulos/principal/recibos/ajax/ciudades.php';?>" ;
        $ciudades = [];
        $.get(ruta, function(res){
            res = JSON.parse(res);
            res.forEach(function(data){

                var dep = data.ciudepto;
                var ciu = data.ciuid;
                var nombre = data.ciunombre;
                // Si es diferente a null significa que el arreglo en el indice x ya posee datos
                if($ciudades[dep] != null)
                {
                    $ciudades[dep].push([ciu, nombre]);
                }
                else
                {
                    // Si es la primera vez se asigna el primer dato como un arreglo
                    $ciudades[dep] = [[ciu, nombre]];
                }
            });
        });
        
        
    });
    </script>
    <style>
        
    </style>
</head>

<body>
    <?php require($r . 'incluir/src/login.php') ?>
    <section id="principal">
        <?php require($r . 'incluir/src/cabeza.php') ?>
        <?php require($r . 'incluir/src/menu.php') ?>
        <article id="cuerpo" class="form-style">
            <article class="mapa">
                <a href="#">Principal</a>
                <div class="mapa_div"></div>
                <a href="#">Bodega</a>
                <div class="mapa_div"></div>
                <a href="#">Documentos</a>
                <div class="mapa_div"></div>
                <a class="current">Entrada</a>
            </article>
            <div id="msj"></div>

            <article id="contenido">
                <form id="form" name="form" action="listar.php" method="GET">
                    <fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
                        <legend class="ui-widget ui-widget-header ui-corner-all">Consulta de documentos</legend>
                        
                        <p>
                            <label for="numero">Numero del doc en el sistema:</label>
                            <input type="text" name="numero_fd" class="form-control"/>
                        </p>
                        <p>
                            <label for="estado">Estado:</label>
                            <select name="estado_fd">
                                <option value="">TODOS</option>
                                <option value="PROCESO">PROCESO</option>
                                <option value="FINALIZADO">FINALIZADO</option>
                            </select>
                        </p>
                        <p>
                            <label for="documento">Tipo de la orden contenida:</label>
                            <select id="prefijo" name="prefijo_h" class="">
                                <option value="">TODOS</option>
                                <?php
                                $qry = $db->query("SELECT * FROM tipdocumentos WHERE tiptipo = 'SALINV' ORDER BY tipnombre");
                                while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
                                    echo '<option value=' . $row['tipid'] . '>' . $row['tipid'] . '/'  . $row['tipnombre'] . '</option>';
                                }
                                ?>
                            </select>
                        </p>
                        <p>
                            <label for="prefijo"># de la orden que contiene:</label>
                            <input type="text" name="orden_h" class="form-control"/>
                        </p>
                        
                        <div class="row">
                            <div class="col-md-6">
                                <label for="Departamento">Departamento:</label>
                                <select name="departamento" id="departamento" class="selectpicker" data-live-search="true" title="TODOS" data-width="100%">
                                <option value="">TODOS</option>
                                <?php
                                    $departamentos = $db->query("SELECT * FROM departamentos ORDER BY depnombre");
                                    while($departamento = $departamentos->fetch(PDO::FETCH_ASSOC))
                                    {
                                        $nombre_dep = $departamento['depnombre'];
                                        $id_dep = $departamento['depid'];
                                        echo "<option value='$id_dep'>$nombre_dep</option>"; 
                                    }
                                ?>
                                </select> 
                            </div>
                            <div class="col-md-6">
                                <label for="Ciudad">Ciudad:</label>
                                <select name="ciudad" id="ciudad" class="selectpicker" data-live-search="true" title="TODOS" data-width="100%">
                                <option value="">TODOS</option>
                                </select> 
                            </div>
                        </div>
                        
                        <p>
                            <label for="numero">Distribuidor:</label>
                            <input type="text" name="distribuidor" class="form-control"/>
                        </p>
                        <p>
                            <label for="fecha">Fechas de factura:</label>
                            <input type="text" id="fecha1" name="fecha1" class="fecha"/> 
                            <input type="text" id="fecha2" name="fecha2" class="fecha"/>
                        </p>
                        <button type="submit" class="btnconsulta btn btn-primary btn-block mt-2" name="consultar"
                                value="Buscar">consultar
                        </button> <!-- BOTON CONSULTAR -->
                    </fieldset>
                </form>
            
            </article>
        </article>
        <?php require($r . 'incluir/src/pie.php') ?>
    </section>
	<?php require($r . 'incluir/src/loading.php'); ?>

    <?php
    if (isset($_GET['error'])) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['error'] . '</div>';
    ?>
</body>
<script>

    
    $("#departamento").change(function(){
        $("#ciudad").empty();
        // Por defecto
        var html = '';
        $("#ciudad").append(html);

        dep = $(this).val();
		if(dep == "")
			cant = 0;
		else
        {
            cant = 0;
            if(typeof $ciudades[dep] !== 'undefined')
                cant = $ciudades[dep].length;
        }
        for(var i = 0; i < cant; i++)
        {
            html = "<option value='" + $ciudades[dep][i][0] + "'>" + $ciudades[dep][i][1] + "</option>";
            $("#ciudad").append(html);
        }
        $('#ciudad').selectpicker('refresh');
    });



</script>

</html>