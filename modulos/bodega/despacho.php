<?php
$r = '../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
if (isset($_POST['asignar'])) {
    $qry = $db->query("UPDATE solicitudes SET solestado = 'BANDEJA', solenvio = '" . $_POST['envio'] . "' WHERE solempresa = '" . $_POST['empresa'] . "' AND solid = '" . $_POST['id'] . "'");


    $mensaje = 'Se asigno correctamente';
    header('Location:lisdespacho.php?mensaje=' . $mensaje);
    exit();
}
$row = $db->query("SELECT * FROM (((solicitudes INNER JOIN empresas ON solempresa = empid) INNER JOIN clientes ON solcliente = cliid) INNER JOIN departamentos ON soldepentrega = depid)INNER JOIN ciudades ON (ciudepto = soldepentrega AND solciuentrega = ciuid) WHERE solempresa = '" . $_GET['empresa'] . "' AND solid = '" . $_GET['id'] . "'")->fetch(PDO::FETCH_ASSOC);
?>
<!doctype html>
<html>
<head>
    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
    
    
    <script type="text/javascript">
        $(document).ready(function () {

            $('.pdf').click(function () {
                newSrc = $(this).attr('data-rel');
                if(validation_file(newSrc) == 200)
                    $('#modal').html("<iframe src='" + newSrc + "' width='100%' height='100%'></iframe>");
                else
                    $('#modal').html("<spam>El PDF no fue encontrado</spam>");
                $('#modal').dialog({modal: true, width: '600', height: '800', title: 'PDF de la solicitud'});
            });
            $('.rotulo').click(function () {
                newSrc = $(this).attr('data-rel');
                if(validation_file(newSrc) == 200)
                    $('#modal').html("<iframe src='" + newSrc + "' width='100%' height='100%'></iframe>");
                else
                    $('#modal').html("<spam>El PDF no fue encontrado</spam>");
                $('#modal').dialog({modal: true, width: '600', height: '800', title: 'PDF del rotulo'});
            });
        });
    </script>
</head>
<body>
<?php require($r . 'incluir/src/login.php') ?>
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <?php require($r . 'incluir/src/menu.php') ?>
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a class="current">Bodega</a>
        </article>
        <article id="contenido">
            <form id="form" name="form" action="despacho.php" method="post">
                <fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
                    <legend class="ui-widget ui-widget-header ui-corner-all">Asignacion de solicitudes a despachar
                        <img src="<?php echo $r ?>imagenes/iconos/pdf.png" class="pdf"
                             data-rel="<?php echo $r . 'pdf/solicitudes/' . $row['solempresa'] . '/' . $row['solid'] . '.pdf' ?>"
                             title="Solicitud en PDF"/> <img src="<?php echo $r ?>imagenes/iconos/printer.png"
                                                             class="rotulo"
                                                             data-rel="pdfenvio.php?empresa=<?php echo $row['solempresa'] . '&id=' . $row['solid'] ?>"
                                                             title="Rotulo"></legend>
                    <p>
                        <label for="empresa">Empresa: </label><input type="hidden" name="empresa"
                                                                     value="<?php echo $_GET['empresa'] ?>"><?php echo $row['empnombre'] ?>
                    </p>
                    <p>
                        <label for="solicitud">Solicitud: </label> <input type="text" name="id" class="pedido"
                                                                          value="<?php echo $row['solid'] ?>" readonly/>
                    </p>
                    <p>
                        <label for="factura">Factura: </label> <input type="text" name="factura" class="consecutivo"
                                                                      value="<?php echo $row['solfactura'] ?>"
                                                                      readonly/>
                    </p>
                    <p>
                        <label for="ubicacion">Ubicacion: </label><?php echo $row['depnombre'] . ' / ' . $row['ciunombre'] . '. ' . $row['solbarentrega'] . ', ' . $row['solentrega'] ?>
                    </p>
                    <p>
                        <label>Envio por: </label>
                        <select id="envio" name="envio" class="selectpicker validate[required]" required data-live-search="true" title="Seleccione entregador" data-width="100%">
                            <option value="">SELECCIONE</option>
                            <?php
                            $qry = $db->query("SELECT * FROM mdespachos WHERE mdeestado = 1 ORDER BY mdenombre");
                            while ($row = $qry->fetch(PDO::FETCH_ASSOC))
                                echo '<option value="' . $row['mdeid'] . '">' . $row['mdenombre'] . '</option>';
                            ?>
                        </select>
                    </p>
                    <p class="boton">
                        <button type="button" class="btn btn-outline-primary btnatras"
                                onClick="carga(); location.href = 'lisdespacho.php'">
                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-left-square-fill"
                                 fill="currentColor"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                      d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm9.5 8.5a.5.5 0 0 0 0-1H5.707l2.147-2.146a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708-.708L5.707 8.5H11.5z"/>
                            </svg>
                            Atras
                        </button>
                        <button type="submit" class="btn btn-primary" name="asignar" value="asignar">asignar</button>
                    </p>
                </fieldset>
            </form>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php') ?>
</section>
<div id="modal" style="display:none"></div>
</body>
</html>