<?php
$r = '../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
if (isset($_GET['id'])) {
    $QRY = $db->query("UPDATE solicitudes SET solestado = 'FACTURADO', solenvio = NULL WHERE solempresa = '" . $_GET['empresa'] . "' AND solid = '" . $_GET['id'] . "'");
    $mensaje = 'Se devolvio para asignacion de despacho';
    header('Location:bandespacho.php?mensaje=' . $mensaje);
    exit();
}
$qry = $db->query("SELECT * FROM (((solicitudes INNER JOIN empresas ON solempresa = empid) INNER JOIN departamentos ON soldepentrega = depid) INNER JOIN ciudades ON (ciudepto = soldepentrega AND solciuentrega = ciuid) INNER JOIN clientes ON solcliente = cliid)  WHERE solestado = 'BANDEJA'");
?>
<!doctype html>
<html>
<head>

    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>
    <link rel="stylesheet" type="text/css"
          href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css"/>

    <script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#tabla').dataTable({
                'bJQueryUI': true,
                'bStateSave': true,
                'sPaginationType': 'full_numbers',
                'oLanguage': {
                    'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
                },
            });

            $(".confirmar").click(function (e) {
                e.preventDefault();
                var targetUrl = $(this).attr("href");
                var $dialog_link_follow_confirm = $('<div></div>').html("<p><span class='ui-icon ui-icon-trash' style='float:left; margin:2px 7px 7px 0;'></span>Esta seguro que desea devolver la asignacion de medio de envio?</p>").dialog({
                        title: 'Porfavor confirmar',
                        buttons: {
                            "Si": function () {
                                window.location.href = targetUrl;
                            },
                            "No": function () {
                                $(this).dialog("close");
                            }
                        },
                        modal: true,
                        width: 'auto',
                        height: 'auto'
                    }
                );
                $dialog_link_follow_confirm.dialog("open");
            });
        });
    </script>
</head>
<body>
<?php require($r . 'incluir/src/login.php') ?>
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <?php require($r . 'incluir/src/menu.php') ?>
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a class="current">Bodega</a>
        </article>
        <article id="contenido">
            <h2>Bandeja de despacho</h2>
            <table id="tabla">
                <thead>
                <tr>
                    <th>Empresa</th>
                    <th>Solicitud</th>
                    <th>Factura</th>
                    <th>Cliente</th>
                    <th>Depto / Ciudad</th>
                    <th>Direccion de denvio</th>
                    <th>Envio por</th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php
                while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
                    $row2 = $db->query("SELECT * FROM mdespachos WHERE mdeid = '" . $row['solenvio'] . "'")->fetch(PDO::FETCH_ASSOC);
                    ?>
                    <tr>
                        <td title="<?php echo $row['solempresa'] ?>"><?php echo $row['empnombre'] ?></td>
                        <td align="center"><?php echo $row['solid'] ?></td>
                        <td align="center"><?php echo $row['solfactura'] ?></td>
                        <td title="<?php echo $row['solcliente'] ?>"><?php echo $row['clinombre'] . ' ' . $row['clinom2'] . ' ' . $row['cliape1'] . ' ' . $row['cliape2'] ?></td>
                        <td><?php echo $row['depnombre'] . ' / ' . $row['ciunombre'] ?></td>
                        <td><?php echo $row['solbarentrega'] . ', ' . $row['solentrega'] ?></td>
                        <td><?php echo $row2['mdenombre'] ?></td>
                        <?php
                        $qry2 = $db->query("SELECT * FROM detsolicitudes INNER JOIN productos ON detproducto = proid WHERE detempresa = '" . $row['solempresa'] . "' AND detsolicitud = '" . $row['solid'] . "'");
                        ?>
                        <td align="center"><img src="<?php echo $r ?>imagenes/iconos/pictures.png" title="<?php
                            while ($row2 = $qry2->fetch(PDO::FETCH_ASSOC))
                                echo '- ' . $row2['pronombre'] . ' (' . $row2['detcantidad'] . ') &#13;&#10;';
                            ?>"/></td>
                        <td><a href="bandespacho.php?empresa=<?php echo $row['solempresa'] . '&id=' . $row['solid'] ?>"
                               title="Devolver" class="confirmar"><img
                                        src="<?php echo $r ?>imagenes/iconos/arrow_undo.png" class="grayscale"/></a>
                        </td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
            <div>
                <form id="form" name="form" method="post" action="finalizar.php">
                    <fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
                        <legend class="ui-widget ui-widget-header ui-corner-all">Generacion de planillas</legend>
                        <p>
                            <label>Generar planilla de: </label>
                            <select name="envio" class="validate[required]">
                                <option value="">SELECCIONE</option>
                                <?php
                                $qry = $db->query("SELECT * FROM mdespachos ORDER BY mdenombre");
                                while ($row = $qry->fetch(PDO::FETCH_ASSOC))
                                    echo '<option value="' . $row['mdeid'] . '">' . $row['mdenombre'] . '</option>';
                                ?>
                            </select>
                        </p>
                        <p class="boton">
                            <button type="submit" class="btn btn-primary" name="despachar" value="despachar">despachar
                            </button>
                        </p>
                </form>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php') ?>
</section>
<?php
if (isset($_GET["mensaje"])) echo "<div id='dialog-message' title='Correcto'><span class='ui-icon ui-icon-circle-check' style='float:left; margin:3px 7px 7px 0;'></span>" . $_GET["mensaje"] . "</div>";
?>
<div id="modal" style="display:none"></div>
</body>
</html>