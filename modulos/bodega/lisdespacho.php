<?php
$r = '../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
$qry = $db->query("SELECT * FROM (((solicitudes INNER JOIN empresas ON solempresa = empid) INNER JOIN departamentos ON soldepentrega = depid)INNER JOIN ciudades ON (ciudepto = soldepentrega AND solciuentrega = ciuid) INNER JOIN clientes ON solcliente = cliid) WHERE solestado = 'FACTURADO'");
?> <!-- prueba para implementacion real american logistic agregar          AND solamerican <> 1       a la consulta superior -->
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>

    <link rel="stylesheet" type="text/css"
          href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css"/>
    <script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#tabla').dataTable({
                'bJQueryUI': true,
                'bStateSave': true,
                'sPaginationType': 'full_numbers',
                'oLanguage': {
                    'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
                },
            });
            $('#dialog-message').dialog({
                height: 80,
                width: 'auto',
                modal: true
            });
            $('.pdf').click(function () {
                newSrc = $(this).attr('data-rel');
                if(validation_file(newSrc) == 200)
                    $('#modal').html("<iframe src='" + newSrc + "' width='100%' height='100%'></iframe>");
                else
                    $('#modal').html("<spam>El PDF no fue encontrado</spam>");
                newSrc = $(this).attr('data-rel');
                $('#modal').dialog({modal: true, width: '600', height: '800', title: 'PDF de la factura'});
            });
            $('.rotulo').click(function () {
                newSrc = $(this).attr('data-rel');
                if(validation_file(newSrc) == 200)
                    $('#modal').html("<iframe src='" + newSrc + "' width='100%' height='100%'></iframe>");
                else
                    $('#modal').html("<spam>El PDF no fue encontrado</spam>");
                newSrc = $(this).attr('data-rel');
                $('#modal').dialog({modal: true, width: '600', height: '450', title: 'PDF del rotulo'});
            });
        });
    </script>
</head>
<body>
<?php require($r . 'incluir/src/login.php') ?>
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <?php require($r . 'incluir/src/menu.php') ?>
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a class="current">Bodega</a>
        </article>
        <article id="contenido">
            <h2>Listado de solicitudes a despachar</h2>
            <div class="reporte">

            </div>
            <table id="tabla">
                <thead>
                <tr>
                    <th>Empresa</th>
                    <th>Solicitud</th>
                    <th>Factura</th>
                    <th>Cliente</th>
                    <th>Ciudad</th>
                    <th>Direccion de entrega</th>
                    <th>Fecha registro</th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php
                while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {

                    $sqleva = "SELECT * FROM  movimientos WHERE  movprefijo = 'DV' and movempresa = '" . $row['solempresa'] . "' and movdocumento = " . $row['solfactura'] . "; ";
                    $qry2 = $db->query($sqleva);

                    $num2 = $qry2->rowCount();

                    if ($num2 == 0) {
                        ?>
                        <tr>
                            <td title="<?php echo $row['solempresa'] ?>"><?php echo $row['empnombre'] ?></td>
                            <td align="center"><?php echo $row['solid'] ?></td>
                            <td align="center"><?php echo $row['solfactura'] ?></td>
                            <td title="<?php echo $row['solcliente'] ?>"><?php echo $row['clinombre'] . ' ' . $row['clinom2'] . ' ' . $row['cliape1'] . ' ' . $row['cliape2'] ?></td>
                            <td><?php echo $row['depnombre'] . ' / ' . $row['ciunombre'] ?></td>
                            <td><?php echo $row['solbarentrega'] . ' ' . $row['solentrega'] ?></td>
                            <td align="center"><img src="<?php echo $r ?>imagenes/iconos/date.png"
                                                    title="<?php echo $row['solfechreg'] ?>"/></td>
                            <?php
                            $qry2 = $db->query("SELECT * FROM detsolicitudes INNER JOIN productos ON detproducto = proid WHERE detempresa = '" . $row['solempresa'] . "' AND detsolicitud = '" . $row['solid'] . "'");

                            ?>
                            <td align="center"><img src="<?php echo $r ?>imagenes/iconos/pictures.png" title="<?php
                                while ($row2 = $qry2->fetch(PDO::FETCH_ASSOC))
                                    echo '- ' . $row2['pronombre'] . ' (' . $row2['detcantidad'] . ') &#13;&#10;';
                                ?>"/></td>
                            <td align="center"><img src="<?php echo $r ?>imagenes/iconos/pdf.png" class="pdf"
                                                    data-rel="<?php echo $r ?>pdf/solicitudes/<?php echo $row['solempresa'] . '/' . $row['solid'] . '.pdf' ?>"
                                                    title="Solicitiud"></td>
                            <td align="center"><img src="<?php echo $r ?>imagenes/iconos/printer.png" class="rotulo"
                                                    data-rel="pdfenvio.php?empresa=<?php echo $row['solempresa'] . '&id=' . $row['solid'] ?>"
                                                    title="Rotulo"></td>
                            <td align="center"><a
                                        href="despacho.php?empresa=<?php echo $row['solempresa'] . '&id=' . $row['solid'] ?>"
                                        onClick="carga()" title="Asignar"><img
                                            src="<?php echo $r ?>imagenes/iconos/tick.png" class="grayscale"/></a></td>
                        </tr>
                        <?php
                    }
                }
                ?>
                </tbody>
            </table>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php') ?>
</section>
<?php
if (isset($_GET['mensaje'])) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['mensaje'] . '</div>';
?>
<div id="modal" style="display:none"></div>
</body>
</html>