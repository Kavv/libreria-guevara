<?php
$r = '../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
require($r . 'incluir/fpdf/fpdf.php');
$envio = $_GET['envio'];
$rowmax = $db->query("SELECT MAX(solplanilla) AS ultplanilla FROM solicitudes")->fetch(PDO::FETCH_ASSOC);
$numplanilla = $rowmax['ultplanilla'] + 1;

class PDF extends FPDF
{
    function Header()
    {
        global $envio;
        global $numplanilla;
        global $db;
        $row = $db->query("SELECT * FROM mdespachos WHERE mdeid = '$envio'")->fetch(PDO::FETCH_ASSOC);
        $this->SetFont('LucidaConsole', '', 8);
        $this->Cell(0, 5, date('Y/m/d H:i:s'), 0, 1);
        $this->SetFont('LucidaConsole', '', 11);
        $this->Cell(0, 5, '------------------------------------------------------------------------------------------------------------------------', 0, 1, 'C');
        $this->Cell(0, 5, 'PLANILLA DE DESPACHO PARA ' . $row['mdenombre'], 0, 1, 'C');
        $this->Cell(0, 5, 'PLANILLA No.: ' . $numplanilla, 0, 1, 'C');
        $this->Cell(0, 5, '------------------------------------------------------------------------------------------------------------------------', 0, 1, 'C');
        $this->Ln(5);
    }

    function Footer()
    {
        $this->SetY(-15);
        $this->SetFont('LucidaConsole', '', 11);
        $this->Cell(0, 5, '------------------------------------------------------------------------------------------------------------------------', 0, 1, 'C');
        $this->SetFont('LucidaConsole', '', 8);
        $this->Cell(0, 5, 'Pagina ' . $this->PageNo() . '/{nb}', 0, 0, 'R');
    }
}

$pdf = new PDF("L");
$pdf->AddFont('LucidaConsole', '', 'lucon.php');
$pdf->AliasNbPages();
$pdf->AddPage();
$qry = $db->query("SELECT * FROM (((solicitudes INNER JOIN empresas ON solempresa = empid) INNER JOIN departamentos ON soldepentrega = depid) INNER JOIN ciudades ON (ciudepto = soldepentrega AND solciuentrega = ciuid)) INNER JOIN clientes ON solcliente = cliid WHERE (solestado = 'BANDEJA' or solestado = 'ENVIADO') AND solenvio = '$envio'");
$pdf->SetFont('LucidaConsole', '', 9);
$pdf->SetFillColor(220, 220, 220);

$pdf->Cell(55, 5, 'EMPRESA', 0, 0, 'C', true);
$pdf->Cell(20, 5, 'SOLICITUD', 0, 0, 'C', true);
$pdf->Cell(25, 5, 'FACTURA', 0, 0, 'C', true);
$pdf->Cell(50, 5, 'CLIENTE', 0, 0, 'C', true);
$pdf->Cell(30, 5, 'TELEFONO', 0, 0, 'C', true);
$pdf->Cell(30, 5, 'CIUDAD', 0, 0, 'C', true);
$pdf->Cell(45, 5, 'DIRECCION', 0, 1, 'C', true);

while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {

    $pdf->SetFont('LucidaConsole', '', 8);
    $pdf->SetFillColor(235, 235, 235);
    $pdf->Cell(55, 5, $row['empnombre'], 0, 0, 'L', true);
    $pdf->Cell(20, 5, $row['solid'], 0, 0, 'C', true);
    $pdf->Cell(25, 5, $row['solfactura'], 0, 0, 'C', true);
    $pdf->Cell(50, 5, $row['clinombre'] . ' ' . $row['clinom2'] . ' ' . $row['cliape1'] . ' ' . $row['cliape2'], 0, 0, 'L', true);
    $pdf->Cell(30, 5, $row['clitelresidencia'] . ' - ' . $row['clitelcomercio'] . ' - ' . $row['cliteladicional'], 0, 0, 'L', true);
    $pdf->Cell(30, 5, $row['ciunombre'], 0, 0, 'L', true);
    $pdf->Cell(45, 5, $row['solentrega'], 0, 1, 'L', true);
    $qry2 = $db->query("SELECT * FROM detsolicitudes INNER JOIN productos ON detproducto = proid WHERE detempresa = '" . $row['solempresa'] . "' AND detsolicitud = '" . $row['solid'] . "'");

}
$qry = $db->query("SELECT SUM(detcantidad) AS cantidad, detproducto, pronombre,solfactura FROM (solicitudes INNER JOIN detsolicitudes ON (solid = detsolicitud AND solempresa = detempresa)) INNER JOIN productos ON detproducto = proid WHERE (solestado = 'BANDEJA' or solestado = 'ENVIADO') AND solenvio = '$envio' GROUP BY detproducto,pronombre,solfactura");
$pdf->AddPage();
$pdf->SetFont('LucidaConsole', '', 10);
$pdf->SetFillColor(220, 220, 220);
$pdf->Cell(0, 5, 'PRODUCTOS DESPACHADOS', 0, 1, 'C');
$pdf->SetX(50);
$pdf->Cell(20, 5, 'CODIGO', 0, 0, 'C', true);
$pdf->Cell(120, 5, 'NOMBRE', 0, 0, 'C', true);
$pdf->Cell(20, 5, 'CANTIDAD', 0, 0, 'C', true);
$pdf->Cell(20, 5, 'FACTURA', 0, 1, 'C', true);

$i = 0;

while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
    if ($i % 2 == 0) $x = 235;
    else $x = 250;
    $pdf->SetX(50);
    $pdf->SetFillColor($x, $x, $x);
    $pdf->Cell(20, 5, $row['detproducto'], 0, 0, 'C', true);
    $pdf->Cell(120, 5, $row['pronombre'], 0, 0, 'L', true);
    $pdf->Cell(20, 5, $row['cantidad'], 0, 0, 'C', true);
    $pdf->Cell(20, 5, $row['solfactura'], 0, 1, 'C', true);

    $i++;
}
$qry = $db->query("UPDATE solicitudes SET solestado = 'ENVIADO', solplanilla = '$numplanilla', solfechdespacho = NOW() WHERE (solestado = 'BANDEJA' or solestado = 'ENVIADO') AND solenvio = '$envio'");
$pdf->Output();
?>