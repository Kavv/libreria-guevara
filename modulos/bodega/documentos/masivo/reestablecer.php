<?php
$r = '../../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

$empresa = '16027020274133';
$prefijo = 'OS';

$productos = $db->query("SELECT * FROM productos WHERE procantidad > 0");
$cantidad_productos = $productos->rowCount();
if($cantidad_productos < 1)
{
    $error = 'El proceso no se realizo, debido a que ningún producto tiene una cantidad mayor a 1';
    header('Location:establecer_en_cero.php?error=' . $error);
    exit();
}


// Obtenemos el # de movimiento consecuente en base al prefijo
$rowmax = $db->query("SELECT MAX(movnumero)+1 AS ultimo FROM movinventario  WHERE movempresa = '$empresa' AND movprefijo = '$prefijo'")->fetch(PDO::FETCH_ASSOC);
if($rowmax['ultimo'] == '')
    $numero = 1;
else
    $numero = $rowmax['ultimo'];

$num = $db->query("SELECT * FROM movinventario 
INNER JOIN tipdocumentos ON tipid = movprefijo
WHERE (tiptipo = 'ENTINV' OR tiptipo = 'SALINV')
AND movestado = 'PROCESO';")->rowCount();

if ($num > 0) {
    $error = 'Hay un documento que no se ha finalizado, debe finalizarlo o cancelarlo para proceder';
    header('Location:establecer_en_cero.php?error=' . $error);
    exit();
}

$comentario = strupperEsp(trim($_POST['texto']));
$digitador = $_SESSION['id'];
$entregado = $digitador;
$bodeguero = $digitador;
// MANAGUA
$departamento = '5';
// MANAGUA
$ciudad = '01';
$telefono1 = '';
$telefono2 = '';

$db->query("INSERT INTO infoextra 
(comentario, departamento, ciudad, entregado, bodeguero, telefono1, telefono2, digitador)
VALUES ('$comentario', '$departamento', '$ciudad', '$entregado', '$bodeguero', '$telefono1', '$telefono2', '$digitador');") or die($db->errorInfo()[2]);


$id_info = $db->lastInsertId();

$qry = $db->query("INSERT INTO movinventario (movempresa, movprefijo, movnumero, movfecha, movestado, movinfo) VALUES ('$empresa', '$prefijo', '$numero', NOW(), 'PROCESO', '$id_info')") or die($db->errorInfo()[2]);


while ($producto = $productos->fetch(PDO::FETCH_ASSOC)) {
    $proid = $producto['proid'];
    $cantidad = $producto['procantidad'];

    $qry = $db->query("UPDATE productos SET procantidad = 0 WHERE proid = '$proid'") or die($db->errorInfo()[2]);
    $qry = $db->query("INSERT INTO detmovimientos 
    (dmoempresa, dmoprefijo, dmonumero, dmoproducto, dmocantidad) 
    VALUES ('$empresa', '$prefijo', '$numero', '$proid', $cantidad)") or die($db->errorInfo()[2]);
    
}

$qry = $db->query("UPDATE movinventario  SET movestado = 'FINALIZADO' WHERE movempresa = '$empresa' AND movprefijo = '$prefijo' AND movnumero = '$numero'") or die($db->errorInfo()[2]);

$filtro = "?empresa=$empresa&prefijo=$prefijo&numero=$numero";
header("location:../salida/finalizar.php" . $filtro);


?>