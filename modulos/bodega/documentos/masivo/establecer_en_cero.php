<?php
$r = '../../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
?>
<!doctype html>
<html lang="es">

<head>
    <title>SALIDA MASIVA DE INVENTARIO A 0</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">
    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>
    <script type="text/javascript"
            src="<?php echo $r ?>incluir/jquery/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>
    <style>
        .danger{
            color:red;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#dialog-message").dialog({
                height: 'auto',
                width: 'auto',
                modal: true
            });
        });
    </script>
</head>
<body>
<?php require($r . 'incluir/src/login.php') ?>
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <?php require($r . 'incluir/src/menu.php') ?>
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a href="#">Bodega</a>
            <div class="mapa_div"></div>
            <a href="#">Documentos</a>
            <div class="mapa_div"></div>
            <a class="current">Salida Masiva</a>
        </article>
        <article id="contenido">
            <form id="form" name="form" action="reestablecer.php" method="POST">
                <fieldset class="ui-widget ui-widget-content ui-corner-all col-md-8">
                    <legend class="ui-widget ui-widget-header ui-corner-all">Salida Masiva</legend>
                    <p>Esta acción genera un <span class="danger">movimiento de salida</span>, en el cual se establece la cantidad a <span class="danger">0</span> de <span class="danger">TODOS</span> los 
                    productos con una actual cantidad <span class="danger">mayor a 1</span>.</p>
                    <p>En este movimiento lo único que se debe detallar es un <span class="danger">comentario</span> 
                    en el cual se plasma el porqué del mismo y automáticamente se establece quien lo ha sistematizado</p>
                    <p class="mt-2">Si está seguro de querer realizar esta función, debe presionar el botón <span class="danger">"Movimiento Masivo de Salida"</span></p>
                    <p>Este proceso puede <span class="danger">tardar unos cuantos minutos</span> dependiendo de la cantidad de datos que deba procesar, 
                    <span class="danger">por favor no apague o cierre la ventana si ha iniciado el proceso</span></p>
                   
                    <p style="position:relative;">
                        <label>Comentario:</label> 
                        <textarea name="texto" style="text-transform: uppercase" cols="100" rows="3" class="form-control validate[maxSize[400], required]"></textarea>
                    </p>
                   
                    <button type="submit" class="btnconsulta btn btn-danger btn-block mt-2" name="consultar" value="Buscar">
                        Movimiento Masivo de Salida
                    </button> <!-- BOTON CONSULTAR -->
                </fieldset>
            </form>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php') ?>
</section>
<?php
if (isset($_GET['error'])) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['error'] . '</div>';
if (isset($_GET['msj'])) echo '<div id="dialog-message" title="Exitoso"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['msj'] . '</div>';
?>
</body>
<script>
/* $(document).ready(function(){
    window.addEventListener("afterunload", function (e) { 
        var mensaje;
    var opcion = confirm("Clicka en Aceptar o Cancelar");
    if (opcion == true) {
        mensaje = "Has clickado OK";
	} else {
	    mensaje = "Has clickado Cancelar";
	}
	document.getElementById("ejemplo").innerHTML = mensaje
    });

}) */

</script>
</html>