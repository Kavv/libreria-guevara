<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
require($r . 'incluir/funciones.php');

$empresa = $_GET['empresa'];
$prefijo = $_GET['prefijo'];
$numero = $_GET['numero'];
$estado = $_GET['estado'];
$fecha1 = $_GET['fecha1'];
$fecha2 = $_GET['fecha2'];

$filtro = 'empresa=' . $empresa . '&prefijo=' . $prefijo . '&numero=' . $numero . '&estado=' . $estado . '&fecha1=' . $fecha1 . '&fecha2=' . $fecha2;
$con = 'SELECT * FROM (movinventario INNER JOIN empresas ON movempresa = empid) 
INNER JOIN tipdocumentos ON tipid = movprefijo
LEFT JOIN infoextra ON infoextra.id = movinfo
LEFT JOIN departamentos ON infoextra.departamento = depid
LEFT JOIN ciudades ON (ciuid = infoextra.ciudad AND ciudepto = infoextra.departamento)';
$ord = 'ORDER BY movfecha DESC, movnumero DESC';

$siempreVerdadero = true;

$sql = crearConsulta($con, $ord,
    array($siempreVerdadero, "(tiptipo = 'ENTINV' OR tiptipo = 'SALINV')"),
    array($empresa, "movempresa = '$empresa'  "),
    array($prefijo, "movprefijo = '$prefijo'"),
    array($numero, "movnumero = $numero"),
    array($estado, "movestado = '$estado'"),
    array($fecha1, "movfecha BETWEEN '$fecha1' AND '$fecha2'")
);
$qry = $db->query($sql);
?>
<!doctype html>
<html lang="es">

<head>
    <title>LISTA DE ORDENES</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">
    <meta charset="utf-8">
    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>

    <link rel="stylesheet" type="text/css"
          href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css"/>
    <script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#tabla').dataTable({
                'bJQueryUI': true,
                'sPaginationType': 'full_numbers',
                'oLanguage': {
                    'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
                },
            });
            $('.pdf').click(function () {
                newSrc = $(this).attr('data-rel');
                $('#modal').html("<iframe src='" + newSrc + "' width='100%' height='100%'></iframe>");
                $('#modal').dialog({modal: true, width: '1200', height: '600', title: 'PDF de la factura'});
            });
            
        });
    </script>
</head>
<body>
<?php require($r . 'incluir/src/login.php') ?>
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <?php require($r . 'incluir/src/menu.php') ?>
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a href="#">Bodega</a>
            <div class="mapa_div"></div>
            <a class="current">Documento</a>
        </article>
        <article id="contenido">
            <h2>Listado de documentos</h2>
            <table id="tabla" style="width:100%;">
                <thead>
                <th>Orden</th>
                <th>Tipo</th>
                <th>Departamento</th>
                <th>Ciudad</th>
                <th>Entregado</th>
                <th style="min-width:135px;">Fecha de orden</th>
                <th>Estado</th>
                <th></th>
                <th></th>
                </tr>
                </thead>
                <tbody>
                <?php
                while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
                    ?>
                    <tr>
                        <td align="center"><?php echo $row['movnumero'] ?></td>
                        <td align="center"><?php echo $row['movprefijo'] ?></td>
                        <td><?php echo $row['depnombre'] ?></td>
                        <td><?php echo $row['ciunombre'] ?></td>
                        <td><?php echo $row['entregado'] ?></td>
                        <td align="center"><?php echo $row['movfecha'] ?></td>
                        <td><?php echo $row['movestado'] ?></td>

                        <?php
                        if($row['tiptipo'] == 'SALINV')
                        {
                            $ruta_edit = "salida/salida.php?";
                            $ruta_pdf = 'salida/pdf.php?empresa=' . $row['movempresa'] . '&prefijo=' . $row['movprefijo'] . '&numero=' . $row['movnumero'];
                        }
                        else
                        {
                            $ruta_edit = "entrada/entrada.php?";
                            $ruta_pdf = 'entrada/pdf.php?empresa=' . $row['movempresa'] . '&prefijo=' . $row['movprefijo'] . '&numero=' . $row['movnumero'];
                        }

                        if ($row['movestado'] == 'PROCESO') {
                            $ruta_edit .= 'empresa=' . $row['movempresa'] . '&prefijo=' . $row['movprefijo'] . '&numero=' . $row['movnumero'];
                            echo '<td align="center"><a target="_blank" href="' . $ruta_edit . '" title="modificar"><img src="' . $r . 'imagenes/iconos/lapiz.png" class="grayscale" /></a></td>';
                        } else {
                            echo '<td></td>';
                        }

                        echo '<td align="center"><img title="Movimiento" src="' . $r . 'imagenes/iconos/pdf.png" class="pdf" data-rel="'.$ruta_pdf.'" /></td>';

                            
                        ?>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
            <p class="boton">
                <button type="button" class="btn btn-outline-primary btnatras"
                        onClick="carga(); location.href = 'consultar.php'">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-left-square-fill"
                         fill="currentColor"
                         xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                              d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm9.5 8.5a.5.5 0 0 0 0-1H5.707l2.147-2.146a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708-.708L5.707 8.5H11.5z"/>
                    </svg>
                    Atras
                </button>
            </p>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php') ?>
</section>
<div id="modal" style="display:none"></div>
</body>
</html>