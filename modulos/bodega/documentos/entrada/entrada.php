<?php
$r = '../../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

$hide = 0;
if (isset($_POST['ingresar'])) {

    $empresa = $_POST['empresa'];
    $prefijo = $_POST['prefijo'];
    // Obtenemos el # de movimiento consecuente en base al prefijo
    $rowmax = $db->query("SELECT MAX(movnumero)+1 AS ultimo FROM movinventario WHERE movempresa = '$empresa' AND movprefijo = '$prefijo'")->fetch(PDO::FETCH_ASSOC);
    if($rowmax['ultimo'] == '')
        $numero = 1;
    else
        $numero = $rowmax['ultimo'];

    $documento = isset($_POST['documento_prov']) ? $_POST['documento_prov'] : '';
    $tercero = isset($_POST['tercero']) ? $_POST['tercero'] : '';

    $num = $db->query("SELECT * FROM movinventario
    INNER JOIN tipdocumentos ON tipid = movprefijo
    WHERE (tiptipo = 'ENTINV' OR tiptipo = 'SALINV')
    AND movestado = 'PROCESO' AND tipid = '$prefijo';")->rowCount();

    if ($num > 0) {
        $error = 'Hay un documento que no se ha finalizado, debe finalizarlo o cancelarlo para proceder';
        header('Location:principal.php?error=' . $error);
        exit();
    }
    if ($tercero == '') {
        $qry = $db->query("INSERT INTO movinventario(movempresa, movprefijo, movnumero, movfecha, movestado) VALUES ('$empresa', '$prefijo', '$numero', NOW(), 'PROCESO')");
    } else {
        $qry = $db->query("INSERT INTO movinventario(movempresa, movprefijo, movnumero, movtercero, movdocumento, movfecha, movestado) VALUES ('$empresa', '$prefijo', $numero, '$tercero', '$documento', NOW(), 'PROCESO')");
    }

    $url = "empresa=$empresa&prefijo=$prefijo&numero=$numero";

    header('Location:entrada.php?' . $url);
} else {
    $empresa = $_GET['empresa'];
    $prefijo = $_GET['prefijo'];
    $numero = $_GET['numero'];
    if(isset($_GET['hide']))
        $hide = $_GET['hide'];
    
}

$row = $db->query("SELECT movinventario.*, empresas.*, tipnombre FROM movinventario 
LEFT JOIN empresas ON movempresa = empid 
LEFT JOIN tipdocumentos ON tipid = '$prefijo'
WHERE movempresa = '$empresa' 
AND movprefijo = '$prefijo' 
AND movnumero = $numero")->fetch(PDO::FETCH_ASSOC);
$infoextra = $entregado = $bodeguero = $comentario = $entregado = $telefono1 = $telefono2 = "";
if($row)
{
    $id_info = $row['movinfo'];
    if($id_info != "")
    {
        $infoextra = $db->query("SELECT infoextra.*, ciunombre FROM infoextra INNER JOIN ciudades ON (ciuid = ciudad AND ciudepto = departamento) WHERE id = '$id_info'")->fetch(PDO::FETCH_ASSOC);
        $entregado = $infoextra['entregado'];
        $bodeguero = $infoextra['bodeguero'];
        $comentario = $infoextra['comentario'];
        $telefono1 = $infoextra['telefono1'];
        $telefono2 = $infoextra['telefono2'];
        $id_info = $infoextra['id'];
    }

}
$hide_input = "";
?>
<!doctype html>

<html lang="es">

<head>
    <title>ORDEN DE ENTRADA #<?php echo $numero?></title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">

    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-not-form.php');
    ?>

    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

    <link rel="stylesheet" href="<?php echo $r.'incluir/librerias/fontawesome/css/all.css'?>" />
    <script src="<?php echo $r.'incluir/librerias/fontawesome/js/all.js'?>"></script>

    <link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
    
    <style>
        .fieldset-espejo{
            padding: 10px;
            display: block;
            margin: 20px auto;
            width: 100% !important;
        }
        .fieldset-espejo legend{
            font-weight: bold;
            padding: 5px;
            text-align: center;
        }
        #form2 input, #form2 select, #form2 textarea {
            border: 1px solid #505050 !important;
            text-transform: uppercase;
        }
        .index_producto {
            background: #0087be;
            color: #fff;
            font-weight: bold;
            border-radius: 10px;
            text-align: center!important;
        }
        #list-product{
            height: 35em;
            overflow-x: hidden;
            overflow-y: overlay;
            padding-right: 20px;
        }
    </style>
    <script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {

        $('#producto').selectpicker();

        $('#form2').validationEngine({
            showOneMessage: true,
            onValidationComplete: function(form, status) {
                if (status) {
                    carga();
                    return true;
                }
            }
        });
        
        $('#form').validationEngine({
            showOneMessage: true,
            onValidationComplete: function(form, status) {
                if (status) {
                    return true;
                }
            }
        });

        $("#dialog-message").dialog({
            height: 80,
            width: 'auto',
            modal: true
        });
        $(".confirmar").click(function(e) {
            e.preventDefault();
            var targetUrl = $(this).attr("href");
            var $dialog_link_follow_confirm = $('<div></div>').html(
                "<p><span class='ui-icon ui-icon-trash' style='float:left; margin:2px 7px 7px 0;'></span>Esta seguro que desea eliminar el producto del documento?</p>"
                ).dialog({
                title: 'Porfavor confirmar',
                buttons: {
                    "Si": function() {
                        window.location.href = targetUrl;
                    },
                    "No": function() {
                        $(this).dialog("close");
                    }
                },
                modal: true,
                width: 'auto',
                height: 135
            });
            $dialog_link_follow_confirm.dialog("open");
        });

        
        // Almacenar las ciudades
        ruta = "<?php echo $r . 'modulos/principal/recibos/ajax/ciudades.php';?>" ;
        $ciudades = [];
        $.get(ruta, function(res){
            res = JSON.parse(res);
            res.forEach(function(data){

                var dep = data.ciudepto;
                var ciu = data.ciuid;
                var nombre = data.ciunombre;
                // Si es diferente a null significa que el arreglo en el indice x ya posee datos
                if($ciudades[dep] != null)
                {
                    $ciudades[dep].push([ciu, nombre]);
                }
                else
                {
                    // Si es la primera vez se asigna el primer dato como un arreglo
                    $ciudades[dep] = [[ciu, nombre]];
                }
            });
        });
        
        var item_cant = $(".product-item").length;
        if(item_cant > 0)
            $("#btn-finalizar").css('display', 'inline-block');
    });
    </script>
</head>

<body>
    <?php require($r . 'incluir/src/login.php') ?>
    <section id="principal">
        <?php require($r . 'incluir/src/cabeza.php') ?>
        <?php require($r . 'incluir/src/menu.php') ?>
        <article id="cuerpo">
            <article class="mapa">
                <a href="#">Principal</a>
                <div class="mapa_div"></div>
                <a href="#">Bodega</a>
                <div class="mapa_div"></div>
                <a href="#">Documentos</a>
                <div class="mapa_div"></div>
                <a class="current">Entrada</a>
            </article>
            <div id="contenido" class="row">
                <div class="col-md-7">
                    <form id="form" name="form" action="entrada.php" onkeypress="is_enter_key(event);" method="post">

                        <fieldset id="field" class="ui-widget ui-widget-content ui-corner-all col-md-12">
                            <legend class="ui-widget ui-widget-header ui-corner-all">Datos del movimiento</legend>
                            <p>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Movimiento: <strong><?php echo $row['tipnombre'] ?></strong></label></label>
                                        <input id="h-prefijo" type="hidden" name="prefijo" class="documento not-w" value="<?php echo $prefijo ?>" readonly />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                    
                                        <label>Orden #: <strong><?php echo $numero ?></strong></label>
                                        <input id="h-numero" type="hidden" name="numero" class="consecutivo not-w" value="<?php echo $numero ?>" readonly />
                                        <input id="h-empresa" type="hidden" name="empresa" value="<?php echo $empresa ?>" />
                                    </div>
                                    <div class="col-md-6">
                                        <label>Fecha: <strong><?php echo $row['movfecha'] ?></strong></label>
                                        <input type="hidden" name="fecha" class="fecha" value="<?php echo $row['movfecha'] ?>" readonly />
                                    </div>
                                </div>
                            </p>
                            <fieldset class="ui-widget ui-widget-content ui-corner-all ">
                                <legend class="ui-widget ui-widget-header ui-corner-all">Datos productos</legend>
                                <div id="msj"></div>
                                <div id="data-productos">
                                    <div class="row">
                                        <div class="input-group mb-1 col-md-12">
                                            <label for="producto">Producto</label>

                                            <div class="col-md-7 p-0">
                                                <select id="producto" name="producto" 
                                                    class="selectpicker form-control validate[required] text-input"
                                                    data-live-search="true" data-width="100%">
                                                    <option value="">SELECCIONE</option>
                                                    <?php
                                                    $qry = $db->query("SELECT * FROM productos WHERE prodesactivado = 0 AND proid NOT IN (SELECT dmoproducto FROM detmovimientos WHERE dmoempresa = '$empresa' AND dmoprefijo = '$prefijo' AND dmonumero = '$numero') ORDER BY proid;");
                                                    while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
                                                        if ($row2['proestado'] == 'OK')
                                                            echo '<option value="' . $row2['proid'] . '">' . $row2['proid'] . " / " . $row2['pronombre'] . '</option>';
                                                        else 
                                                            echo '<option value="' . $row2['proid'] . '" style="background-color:RED" disabled>' . $row2['proid'] . " / " . $row2['pronombre'] . '</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="input-group-append col-md-5 p-0">
                                                <input type="text"  id="cantidad" name="cantidad"
                                                    class="col-md-9 cantidad validate[required, custom[onlyNumberSp], min[1]]" value="1" autocomplete='off' placeholder="Cantidad"/>
                                            
                                                <button type="button" class=" col-md-3 btn btn-success" onclick="add_product();"  >
                                                    <i class="fas fa-plus-square"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="list-product">
                                        <label for="" class="col-md-12 mt-4" style="text-align:center!important;">
                                            <strong>Productos Agregados</strong>
                                        </label>

                                        <?php
                                        $qry = $db->query("SELECT * FROM detmovimientos INNER JOIN productos ON dmoproducto = proid WHERE dmoempresa = '$empresa' AND dmoprefijo = '$prefijo' AND dmonumero = $numero  order by detmovimientos.created_at asc");

                                        $num = $qry->rowCount();
                                        $index_producto = 0;
                                        while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
                                        $index_producto++;
                                        ?>
                                            <p>
                                                <label class="index_producto default">#<?php echo $index_producto; ?></label>
                                            </p>
                                            <p align="center" class="product-item">
                                                <label>  <?php echo $row2['proid'] . " / " . $row2['pronombre'] ?> </label>
                                                <div class="row">
                                                    <div class="input-group mb-1 col-md-6">

                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">Cantidad</span>
                                                        </div>
                                                        <input type="text" class="edit-cantidad form-control not-w cantidad"
                                                            value="<?php echo $row2['dmocantidad'] ?>"  />
                                                    </div>
                                                    
                                                    <div class="col-md-2">
                                                        <button type="button" class="btn btn-block btn-warning"
                                                            onclick="edit_product('<?php echo $row2['dmoproducto']; ?>', this);"  >
                                                            <i class="fas fa-pencil-alt"></i>
                                                        </button>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <button type="button" class="btn btn-block btn-danger"
                                                            onclick="remove_product('<?php echo $row2['dmoproducto']; ?>');"  >
                                                            <i class="fas fa-trash"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </p>
                                        <?php } ?>
                                    </div> 
                                    
                                </div>
                            </fieldset>
                        </fieldset>
                    </form>
                </div>
                
                <div class="col-md-5">
                
                    <form id="form2" name="form2" action="finalizar.php" method="post">
                        <fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12 fieldset-espejo">
                            <legend class="ui-widget ui-widget-header ui-corner-all">Datos Generales</legend>
                            
                            <input type="hidden" name="empresa" value="<?php echo $empresa ?>" />
                            <input type="hidden" name="prefijo" value="<?php echo $prefijo ?>" />
                            <input type="hidden" name="numero" value="<?php echo $numero ?>" />
                            <input type="hidden" name="infoextra" value="<?php echo $id_info ?>" />
                            <p style="position: relative;">
                                <label for=""><span style="color:red">*</span>Departamento</label>
                                <select name="departamento" id="departamento" class="form-control validate[required] selectpicker">
                                <option value=""></option>
                                <?php 
                                    $departamentos = $db->query("SELECT * FROM departamentos ORDER BY depnombre ASC");
                                    while ($d = $departamentos->fetch(PDO::FETCH_ASSOC)) {
                                        if($infoextra != "")
                                        {
                                            if($infoextra['departamento'] == $d['depid'])
                                                echo "<option selected value='" . $d['depid'] . "'>" . $d['depnombre'] . "</option>";
                                            else
                                                echo "<option value='" . $d['depid'] . "'>" . $d['depnombre'] . "</option>";
                                        }
                                        else
                                            echo "<option value='" . $d['depid'] . "'>" . $d['depnombre'] . "</option>";
                                    }
                                ?>
                                </select>
                                <label for=""><span style="color:red">*</span>Ciudad</label>
                                <select name="ciudad" id="ciudad" class="form-control validate[required]">
                                    <?php 
                                        if($infoextra != "")
                                        {
                                            echo "<option selected value='" . $infoextra['ciudad'] . "'>" . $infoextra['ciunombre'] . "</option>";
                                        }
                                    ?>
                                </select>
                                <label for="">Entregado</label>
                                <input type="text" class="form-control validate[maxSize[40]]" value="<?php echo $entregado?>" name="entregado">
                                <label for="">Bodeguero</label>
                                <input type="text" class="form-control validate[maxSize[40]]" value="<?php echo $bodeguero?>" name="bodeguero">
                                <div  style="visibility: hidden;height: 0;">

                                    <label for="">Telefono 1</label>
                                    <input type="text" class="form-control validate[maxSize[15]]" value="<?php echo $telefono1?>" name="telefono1">
                                    <label for="">Telefono 2</label>
                                    <input type="text" class="form-control validate[maxSize[15]]" value="<?php echo $telefono2?>" name="telefono2">
                                </div>
                            </p>
                            <p style="position:relative;">
                                <label>Comentario:</label> 
                                <textarea name="texto" style="text-transform: uppercase" cols="100" rows="3" class="form-control validate[maxSize[400]]"><?php echo $comentario ?></textarea>
                            </p>
                        </fieldset>
                        
                    </form>
                </div>
            </div>
            <div class="row d-flex justify-content-center">
                <p class="boton mt-1"> 
                    <?php 
                        $text_button = "Finalizar";
                        if($hide != 0)
                            $text_button = "Actualizar"; 

                        if($num == 0)
                        {
                            $style_finalizar = "style='display:none;' ";
                            $style_cancelar = "style='display:block;' ";
                        }
                        else
                        {
                            $style_cancelar = "style='display:none;' ";
                            $style_finalizar = "style='display:block;' ";
                        }
                        
                        echo "
                            <button id='btn-cancelar' type='button' class='btn btn-danger' $style_cancelar".
                            "onClick=\"carga(); location.href='cancelar.php?empresa=$empresa&prefijo=$prefijo&numero=$numero' \">".
                                "Cancelar".
                            "</button>".
                            "<button  id='btn-finalizar' $style_finalizar type='button' class='btn btn-primary' name='finalizar' value='finalizar' onclick='finalizar_proceso()'>$text_button</button>";


                    ?>
                </p>
            </div>
        </article>
        <?php require($r . 'incluir/src/pie.php') ?>
    </section>
    <?php
if (isset($_GET['error'])) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['error'] . '</div>';
elseif (isset($_GET['mensaje'])) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['mensaje'] . '</div>';
?>
</body>
<script>
    function is_enter_key(e)
    {
        if (e.which == 13) {
            add_product();
            return false;
        }
    }
    $("#departamento").change(function(){
        $("#ciudad").empty();
        // Por defecto
        var html = '';
        $("#ciudad").append(html);

        dep = $(this).val();
		if(dep == "")
			cant = 0;
		else
        cant = $ciudades[dep].length;
        for(var i = 0; i < cant; i++)
        {
            html = "<option value='" + $ciudades[dep][i][0] + "'>" + $ciudades[dep][i][1] + "</option>";
            $("#ciudad").append(html);
        }
        $('#ciudad').selectpicker('refresh');
    });

    function add_product()
    {
        var vp = $("#producto").validationEngine('validate');
        var vc = $("#cantidad").validationEngine('validate');
        if(!vp && !vc)
        {
            var url = "../template/productos_add_remove.php";
            carga();
            $.ajax({
                type: "POST",
                url: url,
                data: $("#form").serialize(),
                success:function(res){ 
                    $("#data-productos").empty();
                    $("#data-productos").append(res);
                    $("#producto").selectpicker("refresh");
                    $("#carga").dialog("close");
                    $("#btn-finalizar").css('display', 'inline-block');
                    $("#btn-cancelar").css('display', 'none');
                    
                }, fail: function(){
                    $("#carga").dialog("close");
                    var item_cant = $(".product-item").length;
                    if(item_cant < 1)
                    {
                        $("#btn-finalizar").css('display', 'none');
                        $("#btn-cancelar").css('display', 'block');
                    }
                }
            });
        }
    }

    function remove_product(producto)
    {
        var empresa = $("#h-empresa").val();
        var prefijo = $("#h-prefijo").val();
        var numero = $("#h-numero").val();
        var url = "../template/productos_add_remove.php?empresa="+empresa+"&prefijo="+prefijo+"&numero="+numero+"&producto="+producto;
        carga();
        $.get(url, function(res){
            $("#data-productos").empty();
            $("#data-productos").append(res);
            $("#producto").selectpicker("refresh");
            $("#carga").dialog("close");
            var item_cant = $(".product-item").length;
            if(item_cant < 1)
            {
                $("#btn-finalizar").css('display', 'none');
                $("#btn-cancelar").css('display', 'block');
            }
        });

    }
    
    function edit_product(producto, element)
    {

        var new_cantidad = $(element).parent().parent().find(".edit-cantidad").val()

        var empresa = $("#h-empresa").val();
        var prefijo = $("#h-prefijo").val();
        var numero = $("#h-numero").val();
        var url = "../template/entrada_editar_p.php?empresa="+empresa+"&prefijo="+prefijo+"&numero="+numero+"&producto="+producto+"&cantidad="+new_cantidad;
        carga();

        $.ajax({
            type: "GET",
            url: url,
            success:function(res){ 
                res = JSON.parse(res);
                if(res.code == 0)
                    make_alert({'object': $("#msj"), 'type':'info', 'message':res.msj});
                else
                    make_alert({'object': $("#msj"), 'type':'info', 'message':res.msj});

                $("#carga").dialog("close");
            }, fail: function(){
                make_alert({'object': $("#msj"), 'type':'danger', 'message':'NO se edito la cantidad del producto, intentelo nuevamente o solicite soporte tecnico'});
                $("#carga").dialog("close");
            }
        });

    }

    function finalizar_proceso()
    {
        $("#form2").submit();
    }
    

    function make_alert({object = $('#msj'), type = 'success', message = ''})
    {
        object.slideUp();
        object.empty();

        var alerta = 
        '<div class="alert alert-'+ type +' alert-dismissible fade show" role="alert">'+
            message +
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
                '<span aria-hidden="true">&times;</span>'+
            '</button>'+
        '</div>';

        object.append(alerta);
        object.slideDown();
    }


</script>

</html>