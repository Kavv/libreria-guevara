<?php
$r = '../../../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
if (isset($_GET['empresa'])) {
    $empresa = $_GET['empresa'];
    $numero = $_GET['numero'];
    $documento = $_GET['documento'];
    $contrato = $_GET['contrato'];
} else {
    $empresa = $_POST['empresa'];
    $numero = $_POST['numero'];
    $documento = $_POST['documento'];

    $contrato = $_POST['contrato'];
    
    $departamento = $_POST['departamento'];
    $ciudad = $_POST['ciudad'];
    $entregado = strupperEsp(trim($_POST['entregado']));
    $bodeguero = strupperEsp(trim($_POST['bodeguero']));
    $telefono1 = $_POST['telefono1'];
    $telefono2 = $_POST['telefono2'];
    $comentario = strupperEsp(trim($_POST['texto']));


    $retorno = "empresa=$empresa&numero=$numero&documento=$documento&contrato=$contrato";

    $num = $db->query("SELECT * from detmovimientos WHERE dmoempresa = '$empresa' AND dmoprefijo = 'DV' AND dmonumero = '$numero'")->rowCount();
    if ($num > 0) {
        
        $db->query("INSERT INTO infoextra 
        (comentario, departamento, ciudad, entregado, bodeguero, telefono1, telefono2)
        VALUES ('$comentario', '$departamento', '$ciudad', '$entregado', '$bodeguero', '$telefono1', '$telefono2');") or die($db->errorInfo()[2]);
        
        $id_info = $db->lastInsertId();

        // Actualizamos el movimiento a FINALIZADO
        $qry = $db->query("UPDATE movimientos SET movestado = 'FINALIZADO', movinfo = '$id_info' WHERE movempresa = '$empresa' AND movprefijo = 'DV' AND movnumero = '$numero'") or die($db->errorInfo()[2]);

        $qry = $db->query("SELECT * FROM detmovimientos WHERE dmoempresa = '$empresa' AND dmoprefijo = 'DV' AND dmonumero = '$numero'");
        while ($row = $qry->fetch(PDO::FETCH_ASSOC))
            $db->query("UPDATE productos SET proestado = 'OK' WHERE proid = '" . $row['dmoproducto'] . "'");

        header("Location:finalizar.php?" . $retorno);
        exit();
    } else {
        $error = 'No se pudo finalizar el documento, ya que no posee productos.';
        header('Location:entrada.php?empresa=' . $empresa . '&numero=' . $numero . '&documento=' . $documento . '&error=' . $error);
        exit();
    }
}

?>
<!doctype html>
<html>
<head>

    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>
</head>
<body>
<?php require($r . 'incluir/src/login.php') ?>
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <?php require($r . 'incluir/src/menu.php') ?>
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a href="#">Bodega</a>
            <div class="mapa_div"></div>
            <a class="current">Documentos</a>
        </article>
        <article id="contenido">
            <p align="center">
                <iframe src="pdf.php?empresa=<?php echo $empresa . '&numero=' . $numero . '&documento=' . $documento ?>"
                        width="800" height="900"></iframe>
            </p>


            <p class="boton">
                <button type="button" class="btn btn-outline-primary btnatras"
                        onClick="carga(); location.href = 'principal.php'">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-left-square-fill"
                         fill="currentColor"
                         xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                              d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm9.5 8.5a.5.5 0 0 0 0-1H5.707l2.147-2.146a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708-.708L5.707 8.5H11.5z"/>
                    </svg>
                    Atras
                </button>
                <a href="<?php echo $r . "modulos/principal/contrato/editar.php?solid=".$contrato."&contrato=" . $contrato ?>" target="_blank" class="btn btn-success">EDITAR CONTRATO</a>
            </p>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php') ?>
</section>
</body>
</html>
</html>