<?php
$r = '../../../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

?>

<!doctype html>
<html>

<head>
	<?php
		require($r . 'incluir/src/head.php');
		require($r . 'incluir/src/head-not-form.php');
	?>
	<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>
	
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

	<!-- ESTILOS EMBEBIDOS PARA MANEJAR FORMULARIO INDEPENDIENTEMENTE -->
	<style type="text/css">

		#form label {
			display: inline-block;
			width: 100px;
			text-align: right;
			margin: 0.3em 2% 0 0
		}

		#form p {
			margin: 5px 0
		}
	</style>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
		<?php require($r . 'incluir/src/menu.php') ?>
		<!-- INCLUIMOS MENU PRINCIPAL -->
		<article id="cuerpo">
			<article class="mapa">
				<a href="<?php echo $r ?>index.php">Principal</a>
				<div class="mapa_div"></div><a>Contrato</a>
				<div class="mapa_div"></div><a class="current">Consultar</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="listar_contratos.php" method="post" target="_blank">
                    
					<fieldset class="ui-widget ui-widget-content ui-corner-all  col-md-10">
						<legend class="ui-widget ui-widget-header ui-corner-all">Consultar Contrato</legend>
						<p>
							<div class="row">
								<div class="col-md-6">
									<label for="fechas">Contratos entre las fechas de</label>
									<input type="text" class="fecha validate[custom[date]]" id="fecha1" name="fecha1" /> 
								</div>
								<div class="col-md-6">
									<label for="">Hasta</label>
									<input type="text" id="fecha2" class="fecha validate[custom[date]]" name="fecha2" />
								</div>
							</div>
						</p>
						
						<p>
							<div class="row">
								<div class="col-md-6">
									<label for="contrato">Contrato:</label> 
									<input type="text" name="contrato" id="contrato" class="form-control contrato" title="Digite # de contrato" />
								</div>
								<div class="col-md-6">
									<label for="contrato">Cedula cliente:</label> 
									<input type="text" name="cliente" id="cliente" class="form-control cliente" title="Digite cedula del cliente"  onkeypress="return check(event)"/>
								</div>
							</div>
                        </p>
						<p>
							<div class="row">
								<div class="col-md-6">
									<label for="contrato">Nombre cliente:</label> 
									<input type="text" name="nombre" id="nombre" class="form-control" title="Digite el nombre del cliente"  />
								</div>
								<div class="col-md-6">
									<label for="Estado">Estado:</label>
									<select name="estado" id="estado">
									<option value="">TODOS</option>
									<?php
										$estados = $db->query("SELECT * FROM estadoscartera ORDER BY estaid");
										while($estado = $estados->fetch(PDO::FETCH_ASSOC))
										{
											$descripcion = $estado['estadescripcion'];
											echo "<option value='$descripcion'>$descripcion</option>"; 
										}
									?>
									</select> 
								</div>
							</div>
                        </p>
						<p>
							<div class="row">
								<div class="col-md-6">
									<label for="Departamento">Departamento:</label>
									<select name="departamento" id="departamento" class="selectpicker" data-live-search="true" title="TODOS" data-width="100%">
									<option value="">TODOS</option>
									<?php
										$departamentos = $db->query("SELECT * FROM departamentos ORDER BY depnombre");
										while($departamento = $departamentos->fetch(PDO::FETCH_ASSOC))
										{
											$nombre_dep = $departamento['depnombre'];
											$id_dep = $departamento['depid'];
											echo "<option value='$id_dep'>$nombre_dep</option>"; 
										}
									?>
									</select> 
								</div>
								<div class="col-md-6">
									<label for="Ciudad">Ciudad:</label>
									<select name="ciudad" id="ciudad" class="selectpicker" data-live-search="true" title="TODOS" data-width="100%">
									<option value="">TODOS</option>
									</select> 
								</div>
							</div>
                        </p>
						<p>
							<div class="row">
								<div class="col-md-6">
									<label for="asesor">Asesor:</label>
									<select name="asesor" id="asesor" class="selectpicker" data-live-search="true" title="TODOS" data-width="100%">
									<option value="">TODOS</option>
									<?php
										$asesores = $db->query("SELECT * FROM usuarios WHERE usuasesor = 1 ORDER BY usunombre ASC");
										while($asesor = $asesores->fetch(PDO::FETCH_ASSOC))
										{
											$nombre_asesor = $asesor['usunombre'];
											$id_asesor = $asesor['usuid'];
											echo "<option value='$id_asesor'>$id_asesor / $nombre_asesor</option>"; 
										}
									?>
									</select> 
								</div>
								<div class="col-md-6">
									<label for="cobrador">Cobrador:</label>
									<select name="cobrador" id="cobrador" class="selectpicker" data-live-search="true" title="TODOS" data-width="100%">
									<option value="">TODOS</option>
									<?php
										$cobradores = $db->query("SELECT * FROM usuarios WHERE usurelacionista = 1 ORDER BY usunombre ASC");
										while($cobrador = $cobradores->fetch(PDO::FETCH_ASSOC))
										{
											$nombre_cobrador = $cobrador['usunombre'];
											$id_cobrador = $cobrador['usuid'];
											echo "<option value='$id_cobrador'>$id_cobrador / $nombre_cobrador</option>"; 
										}
									?>
									</select> 
								</div>
							</div>
                        </p>
						<p>
							<div class="row">
								<div class="col-md-6">
									<label for="entregador">Entregador:</label>
									<select name="entregador" id="entregador" class="selectpicker" data-live-search="true" title="TODOS" data-width="100%">
									<option value="">TODOS</option>
									<?php
										$entregadores = $db->query("SELECT * FROM mdespachos ORDER BY mdenombre ASC");
										while($entregador = $entregadores->fetch(PDO::FETCH_ASSOC))
										{
											$nombre_entregador = $entregador['mdenombre'];
											$id_entregador = $entregador['mdeid'];
											echo "<option value='$id_entregador'>$id_entregador / $nombre_entregador</option>"; 
										}
									?>
									</select> 
								</div>
							</div>
                        </p>

						<div class="row">
							<div class="col-md-12 col-lg-12">
								<button type="submit" class="btnconsulta btn btn-primary btn-block mt-2" name="consultar">consultar</button> <!-- BOTON CONSULTAR -->
							</div>
						</div>
					</fieldset>
				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
		<!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
	</section>
	
	<?php
	if (isset($_GET['error'])) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['error'] . '</div>'; //VENTANA MODAL EXITOSO
	?>
</body>
<script>
    $(document).ready(function(){

		
		$('#form').validationEngine({
			onValidationComplete: function(form, status) {
				if (status) {
					return true;
				}
			}
		});
		
        // Almacenar las ciudades
        ruta = "<?php echo $r . 'modulos/principal/recibos/ajax/ciudades.php';?>" ;
        $ciudades = [];
        $.get(ruta, function(res){
            res = JSON.parse(res);
            res.forEach(function(data){

                var dep = data.ciudepto;
                var ciu = data.ciuid;
                var nombre = data.ciunombre;
                // Si es diferente a null significa que el arreglo en el indice x ya posee datos
                if($ciudades[dep] != null)
                {
                    $ciudades[dep].push([ciu, nombre]);
                }
                else
                {
                    // Si es la primera vez se asigna el primer dato como un arreglo
                    $ciudades[dep] = [[ciu, nombre]];
                }
            });
        });
	});

    $('#fecha1').datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        onClose: function(selectedDate) {
			if(!$("#fecha1").validationEngine('validate') && !$("#fecha2").validationEngine('validate'))
            	$('#fecha2').datepicker('option', 'minDate', selectedDate);
        }
    })
    $('#fecha2').datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        onClose: function(selectedDate) {
			if(!$("#fecha1").validationEngine('validate') && !$("#fecha2").validationEngine('validate'))
            $('#fecha1').datepicker('option', 'maxDate', selectedDate);
        }
    })
    /* Ambas Fechas o ninguna  */
    /* Valida que ambas fechas sean especificadas */
    /* $('#fecha1').change(function() {
        both_date();
    });

    $('#fecha2').change(function() {
        both_date();
    }); */

    function both_date() {
        if ($('#fecha2').val() != "" || $('#fecha1').val() != "") {
            $('#fecha1').attr("required", true);
            $('#fecha2').attr("required", true);
        } else {
            $('#fecha1').removeAttr("required", false);
            $('#fecha2').removeAttr("required", false);
        }
    }
    
    function check(e) {
        tecla = (document.all) ? e.keyCode : e.which;

        //Tecla de retroceso para borrar, siempre la permite
        if (tecla == 8) {
            return true;
        }

        // Patron de entrada, en este caso solo acepta numeros y letras
        patron = /[A-Za-z0-9]/;
        tecla_final = String.fromCharCode(tecla);
        return patron.test(tecla_final);
    }
	
    $("#departamento").change(function(){
        $("#ciudad").empty();
        // Por defecto
        var html = '';
        $("#ciudad").append(html);

        dep = $(this).val();
		if(dep == "")
			cant = 0;
		else
        cant = $ciudades[dep].length;
		// Defecto
		html += "<option value=''>TODOS</option>";
        for(var i = 0; i < cant; i++)
        {
            html = "<option value='" + $ciudades[dep][i][0] + "'>" + $ciudades[dep][i][1] + "</option>";
            $("#ciudad").append(html);
        }
        $('#ciudad').selectpicker('refresh');
    });
</script>
</html>