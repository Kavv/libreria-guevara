<?php

$r = '../../../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
$numero = $cliente = $fecha1 = $fecha2 = $estado = "";
if (isset($_POST['consultar'])) {
    $contrato = strupperEsp(str_replace("'", "", $_POST['contrato']));
    $cliente = strupperEsp(str_replace("'", "", $_POST['cliente']));
    $fecha1 = $_POST['fecha1'];
    $fecha2 = $_POST['fecha2'];
    $estado = $_POST['estado'];
    $nombre = strupperEsp($_POST['nombre']);

    $dep = $_POST['departamento'];
    $ciu = $_POST['ciudad'];
    $asesor = $_POST['asesor'];
    $cobrador = $_POST['cobrador'];
    $entregador = $_POST['entregador'];
} elseif(isset($_GET['contrato'])) {
    $contrato = strupperEsp(str_replace("'", "", $_GET['contrato']));
    $cliente = strupperEsp(str_replace("'", "", $_GET['cliente']));
    $fecha1 = $_GET['fecha1'];
    $fecha2 = $_GET['fecha2'];
    $estado = $_GET['estado'];
    $nombre = strupperEsp($_GET['nombre']);

    $dep = $_GET['departamento'];
    $ciu = $_GET['ciudad'];
    $asesor = $_GET['asesor'];
    $cobrador = $_GET['cobrador'];
    $entregador = $_GET['entregador'];
}

$con = 'SELECT * FROM solicitudes INNER JOIN clientes ON cliid = solcliente LEFT JOIN departamentos ON depid = soldepentrega LEFT JOIN ciudades ON (ciuid = solciuentrega AND ciudepto = soldepentrega) LEFT JOIN carteras ON carfactura = solfactura';
$ord = ' ORDER BY solfecha';

/* Los parametros de la consulta sql se genera dinamicamente 
en base a los datos recibidos para delimitar los resultados */
$parameters = [];
if($contrato != "")
    array_push($parameters, "solid LIKE '%$contrato%'" );
if($cliente != "")
    array_push($parameters, "solcliente LIKE '%$cliente%'" );
if($fecha1 != "")
    array_push($parameters, "solfecha BETWEEN '$fecha1' AND '$fecha2'" );
if($estado != "")
    array_push($parameters, "carestado = '$estado'" );
if($nombre != "")
    array_push($parameters, "upper(concat(cliNombre, ' ', cliNom2, ' ', cliApe1, ' ', cliApe2)) LIKE '%$nombre%'" );

if($dep != "")
    array_push($parameters, "soldepentrega = '$dep'" );
if($ciu != "")
    array_push($parameters, "solciuentrega = '$ciu'" );
if($asesor != "")
    array_push($parameters, "solasesor = '$asesor'" );
if($cobrador != "")
    array_push($parameters, "solrelacionista = '$cobrador'" );
if($entregador != "")
    array_push($parameters, "solenvio = '$entregador'" );



if(count($parameters) == 0)
{
    header('location:consultar.php?error=Debe llenar almenos un campo');
    exit();
}

// Consulta base
$sql = $con;
foreach ($parameters as $index => $parameter) {
    // Se agregan los parametros del WHERE
    if($index == 0)
        $sql .= " WHERE " . $parameter;
    else
        $sql .= " AND " . $parameter;
}
// Se completa la consulta
$sql .= $ord;

$qry = $db->query($sql);
$cantidad_contrato = $qry->rowCount();

?>
<!doctype html>
<html lang="es">

<head>
<?php
require($r . 'incluir/src/head.php');
require($r . 'incluir/src/head-form.php');
?>

<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript">
    var table;
    $(document).ready(function() {
        table = $('#tabla').dataTable({
            'bJQueryUI': true,
            'bStateSave': true,
            'sPaginationType': 'full_numbers',
            'oLanguage': {
                'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
            },
            'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [0, 1]
            }],
        });
        
        
        table.on("draw", function(){
            console.log("hola");
            $('#loading').dialog('close');
        });
    });
    
</script>
</head>

<body>
<?php require($r . 'incluir/src/login.php') ?>
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <?php require($r . 'incluir/src/menu.php') ?>
    

    <div id="loading" title="Procesando..."><span class="" style="float:left; margin:3px 7px 7px 0;"></span>Este proceso puede demorar <br>Se estan cargando <span style="color:red"><?php echo $cantidad_contrato?></span> resgistros.</div>
    <script>
        $('#loading').dialog({
                autoOpen:false,
                modal:true,
                draggable: false,
                resizable:false
            }).parent('.ui-dialog').find('.ui-dialog-titlebar-close').remove();
        $('#loading').dialog('open');
    </script>
    <article id="cuerpo">
        <article class="mapa">
            <a href="<?php echo $r ?>index.php">Principal</a>
            <div class="mapa_div"></div><a>Contrato</a>
            <div class="mapa_div"></div><a class="current">Ver contratos</a>
        </article>
        <article id="contenido">
            <h2>Listado de cartera <?php  ?></h2>
            <table id="tabla">
                <thead>
                    <tr>
                        <th></th>
                        <th>Contrato</th>
                        <th>Estado</th>
                        <th>Fecha Contrato</th>
                        <th>Cedula</th>
                        <th>Nombre</th>
                        <th>Departa</th>
                        <th>Ciudad</th>
                        <th>Asesor</th>
                        <th>Cobrador</th>
                        <th>Entregador</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
                    
                        $full_name = $row['clinombre'];
                        if( $row['clinom2'] != "" )
                            $full_name .= " " . $row['clinom2'];
                        if( $row['cliape1'] != "" )
                            $full_name .= " " . $row['cliape1'];
                        if( $row['cliape2'] != "" )
                            $full_name .= " " . $row['cliape2'];
                        
                        $ruta = "solid=".$row['solid'];
                    ?>
                        <tr>
                            <td>
                                <a class="btn btn-secondary" target="_blank" href="entrada.php?<?php echo "ingresar=1&empresa=".$row['solempresa']."&documento=".$row['solfactura'] ;?>">Devolución</a>
                            </td>
                            <td><?php echo $row['solid'] ?></td>
                            <td><?php echo $row['carestado'] ?></td>
                            <td><?php echo $row['solfecha'] ?></td>
                            <td><?php echo $row['solcliente'] ?></td>
                            <td><?php echo $full_name ?></td>
                            <td><?php echo $row['depnombre'] ?></td>
                            <td><?php echo $row['ciunombre'] ?></td>
                            <td><?php echo $row['solasesor'] ?></td>
                            <td><?php echo $row['solrelacionista'] ?></td>
                            <td><?php echo $row['solenvio'] ?></td>
                            
                            </td>
                        </tr>
                    <?php
                    }
                    ?>
                </tbody>
            </table>
            <p class="boton">
                <button type="button" class="btn btn-primary btnatras" onClick=" carga(); location.href = 'consultar.php'">atras</button>
            </p>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php') ?>
</section>
<?php
if (isset($_GET['mensaje'])) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['mensaje'] . '</div>';
?>
</body>

</html>