<?php
$r = '../../../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
if (isset($_GET['ingresar'])) {
    $empresa = $_GET['empresa'];
    $documento = $_GET['documento'];

    $rowmax = $db->query("SELECT MAX(movnumero)+1 AS ultimo FROM movimientos WHERE movempresa = '$empresa' AND movprefijo = 'DV'")->fetch(PDO::FETCH_ASSOC);
    if($rowmax['ultimo'] == '')
        $numero = 1;
    else
        $numero = $rowmax['ultimo'];

    $filtro = "empresa=$empresa&numero=$numero&documento=$documento";

    $qry = $db->query("SELECT * FROM movimientos WHERE movempresa = '$empresa' AND movprefijo = 'FV' AND movdocumento = $documento");
    $num = $qry->rowCount();
    if ($num > 0) {
        $row = $qry->fetch(PDO::FETCH_ASSOC);
        $num = $db->query("SELECT * FROM movimientos WHERE (movprefijo <> 'NC' AND movestado = 'PROCESO') AND (movprefijo <> 'ND' AND movestado = 'PROCESO')")->rowCount();
        if($num < 1)
            $qry = $db->query("INSERT INTO movimientos (movempresa, movprefijo, movnumero, movtercero, movdocumento, movfecha, movestado) VALUES ('$empresa', 'DV', '$numero', '" . $row['movtercero'] . "', '$documento', NOW(), 'PROCESO')");
        else 
            $error = 'Un movimiento de bodega esta en proceso';
    } else $error = 'No existe la factura.';
    if (isset($error)) {
        header('Location: principal.php?error=' . $error);
        exit();
    }
    header("Location:entrada.php?" . $filtro);

} else {
    $empresa = $_GET['empresa'];
    $numero = $_GET['numero'];
    $documento = $_GET['documento'];
}

$row3 = $db->query("SELECT * FROM solicitudes INNER JOIN clientes ON solcliente = cliid INNER JOIN empresas ON solempresa = empid  WHERE solempresa = '$empresa' AND solfactura = '$documento'")->fetch(PDO::FETCH_ASSOC);

$row = $db->query("SELECT * FROM movimientos INNER JOIN empresas ON movempresa = empid WHERE movempresa = '$empresa' AND movprefijo = 'DV' AND movnumero = $numero")->fetch(PDO::FETCH_ASSOC);
$infoextra = $entregado = $bodeguero = $comentario = $entregado = $telefono1 = $telefono2 = "";
if($row)
{
    $id_info = $row['movinfo'];
    if($id_info != "")
    {
        $infoextra = $db->query("SELECT infoextra.*, ciunombre FROM infoextra INNER JOIN ciudades ON (ciuid = ciudad AND ciudepto = departamento) WHERE id = '$id_info'")->fetch(PDO::FETCH_ASSOC);
        $entregado = $infoextra['entregado'];
        $bodeguero = $infoextra['bodeguero'];
        $comentario = $infoextra['comentario'];
        $telefono1 = $infoextra['telefono1'];
        $telefono2 = $infoextra['telefono2'];
    }

}


?>
<!doctype html>
<html>
<head>

    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>

    <link rel="stylesheet" type="text/css"
          href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css"/>

    <link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/validation/css/validationEngine.jquery.css"/>

    <script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>

    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

    <style>

        form legend {
            font-weight: bold;
            padding: 5px;
            text-align: center;
        }

        form fieldset {
            padding: 10px;
            display: block;
            margin: 20px auto;
            width: 100% !important;
        }

        form label {
            display: inline-block;
            text-align: left !important;
            margin: 0.3em 2% 0 0;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {

            $('#tabla1').dataTable({
                'bPaginate': false,
                'bFilter': false,
                'bSort': false,
                'bInfo': false,
                'bJQueryUI': true
            });
            $('#tabla2').dataTable({
                'bPaginate': false,
                'bFilter': false,
                'bInfo': false,
                'bSort': false,
                'bJQueryUI': true
            });
        
            // Almacenar las ciudades
            ruta = "<?php echo $r . 'modulos/principal/recibos/ajax/ciudades.php';?>" ;
            $ciudades = [];
            $.get(ruta, function(res){
                res = JSON.parse(res);
                res.forEach(function(data){

                    var dep = data.ciudepto;
                    var ciu = data.ciuid;
                    var nombre = data.ciunombre;
                    // Si es diferente a null significa que el arreglo en el indice x ya posee datos
                    if($ciudades[dep] != null)
                    {
                        $ciudades[dep].push([ciu, nombre]);
                    }
                    else
                    {
                        // Si es la primera vez se asigna el primer dato como un arreglo
                        $ciudades[dep] = [[ciu, nombre]];
                    }
                });
            });
        });
    </script>
</head>
<body>
<?php require($r . 'incluir/src/login.php') ?>
<section id="principal">

    <?php require($r . 'incluir/src/cabeza.php') ?>
    <?php require($r . 'incluir/src/menu.php') ?>

    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a href="#">Bodega</a>
            <div class="mapa_div"></div>
            <a href="#">Documentos</a>
            <div class="mapa_div"></div>
            <a class="current">Devolucion</a>
        </article>
        <article id="contenido">
            <h2>Devolución de contrato</h2>
            <form class="form" name="form" method="post">
                <fieldset class="ui-widget ui-widget-content ui-corner-all col-md-10">
                    <legend class="ui-widget ui-widget-header ui-corner-all">Productos del contrato</legend>
                    <div class="row">
                        <div class="col-md-12 col-lg-6">
                            <label for=""><strong>Empresa </strong><?php echo $row3['empnombre'] ?></label>
                        </div>
                        <div class="col-md-12 col-lg-6">
                            <input type="hidden" name="cliente" class="usuario"
                                   value="  "
                                   readonly/>
                            <label for=""><strong>Cliente </strong><?php echo $row3['clinombre'] . ' ' . $row3['clinom2'] . ' ' . $row3['cliape1'] . ' ' . $row3['cliape2'] ?>
                            </label>
                        </div>
                        <div class="col-md-12 col-lg-12">

                            <label for=""><strong>Fecha de orden </strong><?php echo date('Y-m-d') ?>
                            </label>
                        </div>
                        <div class="col-md-6">

                            <label for="">
                                <strong>Documento </strong>
                            </label>
                            <div class="row ml-1">
                                <input type="text" class="form-control documento col-md-6" name="prefijo" value="DV" readonly/>
                                <input type="text" name="numero" class="form-control consecutivo col-md-6" value="<?php echo $numero ?>" readonly/>
                            </div>
                        </div>
                        <div class="col-md-6">

                            <label for="">
                                <strong>Contrato </strong>
                            </label>
                            <input type="text" class="form-control documento" value="<?php echo $row3['solid']?>" readonly/>
                            
                            <input type="hidden" class="form-control documento" name="FV" value="FV"/>
                            <input type="hidden" name="documento"
                                   class="consecutivo form-control "
                                   value="<?php echo $documento ?>"
                                   readonly/>
                        </div>
                    </div>
                    <br/>

                    <fieldset class="ui-widget ui-widget-content ui-corner-all ">
                        <legend class="ui-widget ui-widget-header ui-corner-all">Detalle</legend>
                        <table id="tabla1">
                            <thead>
                            <tr>
                                <th>Codigo</th>
                                <th>Producto</th>
                                <th>Cantidad</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $qry = $db->query("SELECT * FROM detsolicitudes INNER JOIN productos ON detproducto = proid WHERE detempresa = '$empresa' AND detsolicitud = '" . $row3['solid'] . "'");
                            $bandera = 0;

                            while ($row4 = $qry->fetch(PDO::FETCH_ASSOC)) {
                                ?>
                                <tr>
                                    <td align="center"><?php echo $row4['detproducto'] ?></td>
                                    <td><?php echo $row4['pronombre'] ?></td>
                                    <td align="center"><input value="<?php echo $row4['detcantidad'] ?>"
                                                              class="cantidad"
                                                              id="cantidad<?php echo $bandera; ?>" name="cantidad"/>
                                    </td>
                                    <td align="center">
                                        <button type="button" class="btn btn-success"
                                                onclick="carga(); location.href = 'devolver.php?empresa=<?php echo $empresa . '&numero=' . $numero . '&documento=' . $documento . '&producto=' . $row4['detproducto'] . '&maximo=' . $row4['detcantidad'] . '&cantidad=' ?>'+ $('#cantidad<?php echo $bandera ?>').val()">
                                            +
                                        </button>
                                    </td>
                                </tr>
                                <?php

                                $bandera++;
                            }
                            ?>
                            </tbody>
                        </table>
                        </br>

                    </fieldset>
                </fieldset>
            </form>
            <form id="form" class="form" name="form" action="finalizar.php" method="post">
                <fieldset class="ui-widget ui-widget-content ui-corner-all col-md-10">
                    <legend class="ui-widget ui-widget-header ui-corner-all">Devolucion</legend>
                    <input type="hidden" name="empresa" value="<?php echo $empresa ?>"/>
                    <input type="hidden" name="numero" value="<?php echo $numero ?>"/>
                    <input type="hidden" name="documento" value="<?php echo $documento ?>"/>
                    <input type="hidden" name="contrato" value="<?php echo $row3['solid']?>"/>
                    
                    <p style="position: relative;">
                        
                        <div class="row">
                            <div class="col-md-6">
                                <label for=""><span style="color:red">*</span>Departamento</label>
                                <select name="departamento" id="departamento" class="form-control validate[required] selectpicker" data-live-search="true" title="Seleccione un departamento" data-width="100%">
                                    <?php 
                                        $departamentos = $db->query("SELECT * FROM departamentos ORDER BY depnombre ASC");
                                        while ($d = $departamentos->fetch(PDO::FETCH_ASSOC)) {
                                            if($infoextra != "")
                                            {
                                                if($infoextra['departamento'] == $d['depid'])
                                                    echo "<option selected value='" . $d['depid'] . "'>" . $d['depnombre'] . "</option>";
                                                else
                                                    echo "<option value='" . $d['depid'] . "'>" . $d['depnombre'] . "</option>";
                                            }
                                            else
                                                echo "<option value='" . $d['depid'] . "'>" . $d['depnombre'] . "</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label for=""><span style="color:red">*</span>Ciudad</label>
                                <select name="ciudad" id="ciudad" class="form-control validate[required]">
                                    <?php 
                                        if($infoextra != "")
                                        {
                                            echo "<option selected value='" . $infoextra['ciudad'] . "'>" . $infoextra['ciunombre'] . "</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label for=""><span style="color:red">*</span>Entregado</label>
                                <input type="text" class="form-control validate[required, maxSize[40]]" value="" name="entregado">
                            </div>
                            <div class="col-md-6">
                                <label for=""><span style="color:red">*</span>Bodeguero</label>
                                <input type="text" class="form-control validate[required, maxSize[40]]" value="" name="bodeguero">
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-6">
                                <label for="">Telefono 1</label>
                                <input type="text" class="form-control validate[maxSize[15]]" value="" name="telefono1">
                            </div>
                            <div class="col-md-6">
                                <label for="">Telefono 2</label>
                                <input type="text" class="form-control validate[maxSize[15]]" value="" name="telefono2">
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-12">
                                <label>Comentario:</label> 
                                <textarea name="texto" style="text-transform: uppercase" cols="100" rows="3" class="form-control validate[maxSize[400]]"><?php echo $comentario ?></textarea>
                            </div>
                        </div>
                    </p>
                    <table id="tabla2">
                        <thead>
                        <tr>
                            <th>Codigo</th>
                            <th>Producto</th>
                            <th>Cantidad</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $qry = $db->query("SELECT * FROM detmovimientos INNER JOIN productos ON dmoproducto = proid WHERE dmoempresa = '$empresa' AND dmoprefijo = 'DV' AND dmonumero = '$numero'");
                        while ($row4 = $qry->fetch(PDO::FETCH_ASSOC)) {
                            ?>
                            <tr>
                                <td align="center"><?php echo $row4['dmoproducto'] ?></td>
                                <td><?php echo $row4['pronombre'] ?></td>
                                <td align="center"><input
                                            value="<?php echo $row4['dmocantidad'] ?>"
                                            class="cantidad" readonly/></td>
                                <td align="center">
                                    <button type="button" class="btn btn-danger confirmar"
                                            onclick="carga(); location.href = 'eliminar.php?empresa=<?php echo $empresa . '&prefijo=' . '&numero=' . $numero . '&documento=' . $documento . '&producto=' . $row4['dmoproducto'] . '&cantidad=' . $row4['dmocantidad'] ?>'">
                                        -
                                    </button>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                        </tbody>
                    </table>
                    
                    <p class="boton mt-1">
                        <button type="button" class="btn btn-danger"
                                onClick="carga(); location.href = 'cancelar.php?empresa=<?php echo $empresa . '&numero=' . $numero . '&documento=' . $documento ?>'">
                            cancelar
                        </button>
                        <button type="submit" class="btn btn-primary" name="finalizar" value="finalizar">finalizar
                        </button>
                    </p>
                </fieldset>
            </form>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php') ?>
</section>
<?php
if (isset($_GET['error']) && !empty($_GET['error'])) echo "<div id='dialog-message' title='Error'><span class='ui-icon ui-icon-circle-close' style='float:left; margin:3px 7px 7px 0;'></span>" . $_GET["error"] . "</div>";
elseif (isset($_GET['mensaje'])) echo "<div id='dialog-message' title='Correcto'><span class='ui-icon ui-icon-circle-check' style='float:left; margin:3px 7px 7px 0;'></span>" . $_GET["mensaje"] . "</div>";
?>
</body>
<script>

    $("#departamento").change(function(){
        $("#ciudad").empty();
        // Por defecto
        var html = '';
        $("#ciudad").append(html);

        dep = $(this).val();
		if(dep == "")
			cant = 0;
		else
        cant = $ciudades[dep].length;
        for(var i = 0; i < cant; i++)
        {
            html = "<option value='" + $ciudades[dep][i][0] + "'>" + $ciudades[dep][i][1] + "</option>";
            $("#ciudad").append(html);
        }
        $('#ciudad').selectpicker('refresh');
    });
</script>
</html>