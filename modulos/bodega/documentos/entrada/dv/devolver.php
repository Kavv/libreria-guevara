<?php
$r = '../../../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
$empresa = $_GET['empresa'];
$numero = $_GET['numero'];
$documento = $_GET['documento'];
$producto = $_GET['producto'];
$cantidad = $_GET['cantidad'];
$maximo = $_GET['maximo'];

$num = $db->query("SELECT * FROM detmovimientos WHERE dmoempresa = '$empresa' AND dmoprefijo = 'DV' AND dmonumero = '$numero' AND dmoproducto = '$producto'")->rowCount();

$filtro = "empresa=$empresa&numero=$numero&documento=$documento";
if ($num < 1) {

    if ($cantidad > $maximo){
        $cantidad = $maximo;
        $mensaje = "Lo máximo que puede devolver de este producto es $maximo";
        $filtro = $filtro . "&mensaje=$mensaje";
        header('Location:entrada.php?' . $filtro);
        exit();
    }

    $db->query("SET timezone = 'America/Managua';");
    $qry = $db->query("INSERT INTO detmovimientos (dmoempresa, dmoprefijo, dmonumero, dmoproducto, dmocantidad) VALUES ('$empresa', 'DV', '$numero', '$producto', $cantidad)");
    $row = $db->query("SELECT * FROM productos WHERE proid = '$producto'")->fetch(PDO::FETCH_ASSOC);
    $qry = $db->query("UPDATE detmovimientos SET dmoinventario = " . $row['procantidad'] . " + " . $cantidad . ", dmotinventario = (" . $row['procantidad'] . " * " . $row['procosto'] . "), dmovinventario = dmotinventario / dmoinventario  WHERE dmoempresa = '$empresa' AND dmoprefijo = 'DV' AND dmonumero = '$numero' AND dmoproducto = '$producto'");
    $qry = $db->query("UPDATE productos SET procantidad = procantidad + $cantidad, proestado = 'PROCESO' WHERE proid = '$producto'");


} else {
    $error = 'Este producto ya posee devolucion en esta factura.';
    $filtro = $filtro . "&error=$error";
}

header('Location:entrada.php?' . $filtro);

exit();
?>