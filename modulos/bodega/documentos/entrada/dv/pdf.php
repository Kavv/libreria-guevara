<?php
$r = '../../../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
include($r . 'incluir/fpdf/fpdf.php');
$empresa = $_GET['empresa'];
$numero = $_GET['numero'];

$sql = "SELECT movimientos.*, solid, clientes.*, empnombre FROM movimientos INNER JOIN solicitudes ON movdocumento = solfactura LEFT JOIN clientes ON movtercero = cliid INNER JOIN empresas ON movempresa = empid WHERE movempresa = '$empresa' AND movprefijo = 'DV' AND movnumero = '$numero'";
//echo $sql;
$row = $db->query($sql)->fetch(PDO::FETCH_ASSOC);
$infoextra = $entregado = $bodeguero = $comentario = $entregado = $telefono1 = $telefono2 = "";
if($row)
{
    $id_info = $row['movinfo'];
    if($id_info != "")
    {
        $infoextra = $db->query("SELECT infoextra.*, ciunombre FROM infoextra INNER JOIN ciudades ON (ciuid = ciudad AND ciudepto = departamento) WHERE id = '$id_info'")->fetch(PDO::FETCH_ASSOC);
        $entregado = $infoextra['entregado'];
        $bodeguero = $infoextra['bodeguero'];
        $comentario = $infoextra['comentario'];
        $telefono1 = $infoextra['telefono1'];
        $telefono2 = $infoextra['telefono2'];
    }
}
$pdf = new FPDF();
$pdf->AddFont('LucidaConsole', '', 'lucon.php');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetTitle('Devolucion del contrato '.$row['solid']);
$pdf->SetFont('LucidaConsole', '', 8);
$pdf->Cell(0, 5, '-----------------------------------------------------------------------------------------------------------------', 0, 1, 'C');
$pdf->SetFont('LucidaConsole', '', 10);
$pdf->Cell(0, 5, 'DEVOLUCION NO:' . $row['movnumero'], 0, 1, 'C');
$pdf->SetFont('LucidaConsole', '', 8);
$pdf->Cell(0, 5, '-----------------------------------------------------------------------------------------------------------------', 0, 1, 'C');
$pdf->Ln(5);
$pdf->SetFont('LucidaConsole', '', 10);
$pdf->Cell(0, 5, 'FECHA: ' . $row['movfecha'], 0, 1);
$pdf->Ln(2);
//$pdf->Cell(0, 5, 'No FACTURA: FV-' . $row['movdocumento'], 0, 1);
$pdf->Cell(0, 5, 'No CONTRATO: ' . $row['solid'], 0, 1);
$pdf->Ln(2);
$pdf->Cell(0, 5, 'EMPRESA: ' . $row['empnombre'], 0, 1);
$pdf->Ln(2);
$pdf->Cell(0, 5, 'CLIENTE: ' . $row['clinombre'] . ' ' . $row['clinom2'] . ' ' . $row['cliape1'] . ' ' . $row['cliape2'], 0, 1);


$pdf->Ln(2);
$pdf->Cell(0, 5, 'ENTREGADO: ' . $entregado, 0, 1);
$pdf->Ln(2);
$pdf->Cell(0, 5, 'BODEGUERO: ' . $bodeguero, 0, 1);

$pdf->Ln(2);
$pdf->Cell(20, 5, 'CANTIDAD', 1, 0, 'C');
$pdf->Cell(30, 5, 'CODIGO', 1, 0, 'C');
$pdf->Cell(140, 5, 'PRODUCTO', 1, 1, 'C');
$qry = $db->query("SELECT * FROM detmovimientos INNER JOIN productos ON dmoproducto = proid WHERE dmoempresa = '$empresa' AND dmoprefijo = 'DV' AND dmonumero = '$numero'");
while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
    $pdf->Cell(20, 5, $row2['dmocantidad'], 1, 0, 'C');
    $pdf->Cell(30, 5, $row2['dmoproducto'], 1, 0, 'C');
    $pdf->Cell(140, 5, $row2['pronombre'], 1, 1, 'L');
}
$pdf->Ln(2);
$pdf->Cell(0, 5, 'VALOR: C$ ' . number_format($row['movvalor'], 2), 0, 1);
$pdf->Ln(5);
$pdf->MultiCell(0, 5, 'OBSERVACION: ' . $comentario);
$pdf->Ln(20);
$pdf->Cell(60, 5, '__________________________', 0, 0, 'C');
$pdf->Cell(65, 5, '________________________', 0, 0, 'C');
$pdf->Cell(65, 5, '__________________________', 0, 1, 'C');
$pdf->SetFont('Arial', '', 7);
$pdf->Cell(60, 5, 'ENTREGO', 0, 0, 'C');
$pdf->Cell(65, 5, 'RECIBIO', 0, 0, 'C');
$pdf->Cell(65, 5, 'SISTEMATIZO', 0, 1, 'C');
$pdf->Output('Devolucion del contrato '.$row['solid'],'I');
?>