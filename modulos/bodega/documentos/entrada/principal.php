<?php
$r = '../../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
?>
<!doctype html>

<html lang="es">

<head>
    <title>TIPO ORDEN DE ENTRADA</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">

    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>


    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css"/>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {

            $('#empresa, #documento').change(function (event) {
                var id1 = $('#documento').val();
                if (id1 == 'FC') {
                    carga();
                    $('#adicional').load('proveedor.php', function () {
                        $("#tercero").selectpicker();
                        $("#carga").dialog("close");
                    });
                } else {
                    $('#adicional').load('blank.php');
                }

            });
        });
    </script>
</head>
<body>
<?php require($r . 'incluir/src/login.php') ?>
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <?php require($r . 'incluir/src/menu.php') ?>
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a href="#">Bodega</a>
            <div class="mapa_div"></div>
            <a href="#">Documentos</a>
            <div class="mapa_div"></div>
            <a class="current">Entrada</a>
        </article>
        <article id="contenido">
            <form id="form" name="form" action="entrada.php" method="post">
                <fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
                    <legend class="ui-widget ui-widget-header ui-corner-all">Documento de entrada</legend>
                    <p>
                        <label for="documento">Documento:</label>
                        <select id="documento" name="prefijo" class="validate[required]">
                            <?php
                            $qry = $db->query("SELECT * FROM tipdocumentos WHERE tiptipo = 'ENTINV' ORDER BY tipnombre");
                            while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
                                echo '<option value=' . $row['tipid'] . '>' .$row['tipid'] . '/' . $row['tipnombre'] . '</option>';
                            }
                            ?>
                        </select>
                    </p>
                    <p>
                        <label for="empresa">Empresa:</label>
                        <select id="empresa" name="empresa" class="validate[required]">
                            <?php
                            $qry = $db->query("SELECT * FROM empresas ORDER BY empnombre");
                            while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
                                echo '<option value=' . $row['empid'] . '>' . $row['empnombre'] . '</option>';
                            }
                            ?>
                        </select>
                    </p>
                    <p id="adicional"></p>
                    <p class="boton">
                        <button type="submit" class="btn btn-primary mt-1" name="ingresar" value="ingresar">ingresar
                        </button>
                    </p>


                </fieldset>
            </form>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php') ?>
</section>
<?php
if (isset($_GET['error'])) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['error'] . '</div>';
elseif (isset($_GET['mensaje'])) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['mensaje'] . '</div>';
?>
</body>
</html>