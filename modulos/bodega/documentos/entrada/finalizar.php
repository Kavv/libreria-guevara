<?php
$r = '../../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
if (isset($_POST['empresa'])) {
    $empresa = $_POST['empresa'];
    $prefijo = $_POST['prefijo'];
    $numero = $_POST['numero'];


    $departamento = $_POST['departamento'];
    $ciudad = $_POST['ciudad'];
    $entregado = strupperEsp(trim($_POST['entregado']));
    $bodeguero = strupperEsp(trim($_POST['bodeguero']));
    $telefono1 = $_POST['telefono1'];
    $telefono2 = $_POST['telefono2'];

    $filtro = "?empresa=$empresa&prefijo=$prefijo&numero=$numero";

    $comentario = strupperEsp(trim($_POST['texto']));
    $digitador = $_SESSION['id'];

    $id_info = $_POST['infoextra'];
    if($id_info != "")
    {
        $db->query("UPDATE infoextra SET comentario = '$comentario', departamento = '$departamento', ciudad = '$ciudad', 
        entregado = '$entregado', bodeguero = '$bodeguero', telefono1 = '$telefono1', telefono2 = '$telefono2',
         digitador = '$digitador'
         WHERE id = $id_info");
    }
    else
    {
        $db->query("INSERT INTO infoextra 
        (comentario, departamento, ciudad, entregado, bodeguero, telefono1, telefono2, digitador)
        VALUES ('$comentario', '$departamento', '$ciudad', '$entregado', '$bodeguero', '$telefono1', '$telefono2', '$digitador');") or die($db->errorInfo()[2]);
        
        $id_info = $db->lastInsertId();
    }
    // Actualizamos el movimiento a FINALIZADO
    $qry = $db->query("UPDATE movinventario SET movestado = 'FINALIZADO', movinfo = '$id_info' WHERE movempresa = '$empresa' AND movprefijo = '$prefijo' AND movnumero = '$numero'") or die($db->errorInfo()[2]);

    $qry = $db->query("SELECT * FROM detmovimientos WHERE dmoempresa = '$empresa' AND dmoprefijo = '$prefijo' AND dmonumero = '$numero'");
    // TODOS LOS PREDUCTOS QUE ESTABAN SIENDO AFECTADOS SON REGRESADOS A SU ESTADO NORMAL "OK"
    while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
        $qry2 = $db->query("UPDATE productos SET proestado = 'OK' WHERE proid = '" . $row['dmoproducto'] . "'") or die($db->errorInfo()[2]);
    }


    header("Location:finalizar.php" . $filtro);
    exit();
} else {

    $empresa = $_GET['empresa'];
    $prefijo = $_GET['prefijo'];
    $numero = $_GET['numero'];

    $filtro = "?empresa=$empresa&prefijo=$prefijo&numero=$numero";

}


?>
<!doctype html>

<html lang="es">

<head>
    <title>PDF DE LA ORDEN #<?php echo $numero?></title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">
    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>


</head>
<body>
<?php require($r . 'incluir/src/login.php') ?>
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <?php require($r . 'incluir/src/menu.php') ?>
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a href="#">Bodega</a>
            <div class="mapa_div"></div>
            <a href="#">Documentos</a>
            <div class="mapa_div"></div>
            <a class="current">Entrada</a>
        </article>
        <article id="contenido">
            <p align="center">
                <iframe src="pdf.php?empresa=<?php echo $empresa . '&prefijo=' . $prefijo . '&numero=' . $numero ?>"
                        width="800" height="550"></iframe>
            </p>
            <p class="boton">
                <button type="button" class="btn btn-primary btnatras"
                        onClick="carga(); location.href = 'principal.php'">
                    Agregar nuevo movimiento de entrada
                </button>
            </p>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php') ?>
</section>
</body>
</html>