<?php
$r = '../../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
if (isset($_GET['producto'])) {
    $empresa = $_GET['empresa'];
    $prefijo = $_GET['prefijo'];
    $numero = $_GET['numero'];
    $producto = $_GET['producto'];
    $new_cantidad = $_GET['cantidad']*1;
    $vunitario = $_GET['vunitario']*1;
    $new_tunitario = round($new_cantidad * $vunitario, 2);
    $result = new stdClass();


    // Obtenemos los datos del detalle de movimiento a eliminar
    $row = $db->query("SELECT * FROM detmovimientos WHERE dmoempresa = '$empresa' AND dmoprefijo = '$prefijo' AND dmonumero = '$numero' AND dmoproducto = '$producto'")->fetch(PDO::FETCH_ASSOC);
    
    if(!$row)
    {
        $result->codigo = 1;
        $result->msj = "No se encontro en la Base de datos el producto con codigo $producto";
        exit();
    }
    
    $cantidad_original = $row['dmocantidad'];
    $tunitario_original = $row['dmototal'];
    // Hacemos una resta del inventario restante (en base a lo registrado al crear el detalle) con la cantidad original asignada
    // Esto definiria el punto de origen del producto en el correspondiente movimiento
    // Para sumarle la cantidad nueva que se ha especificado
    $new_restante = ($row['dmoinventario'] + $cantidad_original) - $new_cantidad;

    
    $row2 = $db->query("SELECT * FROM productos WHERE proid = '$producto'")->fetch(PDO::FETCH_ASSOC);
    $nuecosto = $row2['procosto'];
    // Editamos el registro del detalle de movimiento correspondiente 
    $costo_inventario = round($nuecosto * $new_cantidad, 2);
    $qry = $db->query("UPDATE detmovimientos SET dmocantidad = $new_cantidad, dmounitario = $vunitario, dmototal = $new_tunitario, dmoinventario = $new_restante, dmovinventario = $nuecosto, dmotinventario = $costo_inventario
    WHERE dmoempresa = '$empresa' AND dmoprefijo = '$prefijo' AND dmonumero = '$numero' AND dmoproducto = '$producto'") or die($db->errorInfo()[2]);
    if(!$qry)
    {
        $result->codigo = 1;
        $result->msj = "Ocurrio un error durante la actualización del movimiento de producto con codigo $producto";
        exit();
    }
    
    $qry = $db->query("UPDATE productos SET procantidad = ( (procantidad + $cantidad_original) - $new_cantidad), proestado = 'OK' WHERE proid = '$producto'") or die($db->errorInfo()[2]);
    if(!$qry)
    {
        $result->codigo = 1;
        $result->msj = "Ocurrio un error durante la actualización del producto con codigo $producto";
        exit();
    }

    $qry = $db->query("UPDATE movinventario SET movvalor = (movvalor - $tunitario_original) + $new_tunitario WHERE movempresa = '$empresa' AND movprefijo = '$prefijo' AND movnumero = '$numero'");
    if(!$qry)
    {
        $result->codigo = 1;
        $result->msj = "Ocurrio un error durante la actualización del movimiento $producto";
        exit();
    }

    $result->codigo = 0;
    $result->msj = "Se edito la cantidad del producto con codigo $producto";
    echo json_encode($result);
} 