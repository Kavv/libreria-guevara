<?php
$r = '../../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
if (isset($_GET['producto'])) {
    $empresa = $_GET['empresa'];
    $prefijo = $_GET['prefijo'];
    $numero = $_GET['numero'];
    $producto = $_GET['producto'];
    $new_cantidad = $_GET['cantidad'];

    $result = new stdClass();


    // Obtenemos los datos del detalle de movimiento a eliminar
    $row = $db->query("SELECT * FROM detmovimientos WHERE dmoempresa = '$empresa' AND dmoprefijo = '$prefijo' AND dmonumero = '$numero' AND dmoproducto = '$producto'")->fetch(PDO::FETCH_ASSOC);
    
    if(!$row)
    {
        $result->codigo = 1;
        $result->msj = "No se encontro en la Base de datos el producto con codigo $producto";
        exit();
    }
    
    // Buscamos los datos del producto
    //$row = $db->query("SELECT * FROM productos WHERE proid = '$producto'")->fetch(PDO::FETCH_ASSOC);
    $cantidad_original = $row['dmocantidad'];
    // Hacemos una resta del inventario restante (en base a lo registrado al crear el detalle) con la cantidad original asignada
    // Esto definiria el punto de origen del producto en el correspondiente movimiento
    // Para sumarle la cantidad nueva que se ha especificado
    $new_restante = ($row['dmoinventario'] - $row['dmocantidad']) + $new_cantidad;

    
    // Editamos el registro del detalle de movimiento correspondiente 
    $qry = $db->query("UPDATE detmovimientos SET dmocantidad = $new_cantidad,  dmoinventario = $new_restante WHERE dmoempresa = '$empresa' AND dmoprefijo = '$prefijo' AND dmonumero = '$numero' AND dmoproducto = '$producto'") or die($db->errorInfo()[2]);
    if(!$qry)
    {
        $result->codigo = 1;
        $result->msj = "Ocurrio un error durante la actualización del movimiento de producto con codigo $producto";
        exit();
    }
    
    
    // Actualizamos el valor del movimiento principal restando el valor total del detalle del movimiento que se ha eliminado.
    // OBSOLETO $qry = $db->query("UPDATE movinventario SET movvalor = movvalor - " . $row['dmototal'] . " WHERE movempresa = '$empresa' AND movprefijo = '$prefijo' AND movnumero = '$numero'");
    // Obtenemos el costo del producto, consultando el ultimo detalle de movimiento del producto eliminado, 
    $row2 = $db->query("SELECT dmovinventario FROM detmovimientos WHERE dmoproducto = '$producto' ORDER BY dmoid DESC LIMIT 1")->fetch(PDO::FETCH_ASSOC);


    $qry = $db->query("UPDATE productos SET procantidad = ( (procantidad - $cantidad_original) + $new_cantidad), proestado = 'OK' WHERE proid = '$producto'") or die($db->errorInfo()[2]);
    if(!$qry)
    {
        $result->codigo = 1;
        $result->msj = "Ocurrio un error durante la actualización del producto con codigo $producto";
        exit();
    }


    $result->codigo = 0;
    $result->msj = "Se edito la cantidad del producto con codigo $producto";
    echo json_encode($result);
} 