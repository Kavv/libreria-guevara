<?php
$r = '../../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
$hide = 0;
if (isset($_GET['producto'])) {
    $empresa = $_GET['empresa'];
    $prefijo = $_GET['prefijo'];
    $numero = $_GET['numero'];
    $producto = $_GET['producto'];
    $row = $db->query("SELECT * FROM detmovimientos WHERE dmoempresa = '$empresa' AND dmoprefijo = '$prefijo' AND dmonumero = '$numero' AND dmoproducto = '$producto'")->fetch(PDO::FETCH_ASSOC);
    $qry = $db->query("DELETE FROM detmovimientos WHERE dmoempresa = '$empresa' AND dmoprefijo = '$prefijo' AND dmonumero = '$numero' AND dmoproducto = '$producto'");
    $qry = $db->query("UPDATE movinventario SET movvalor = movvalor - " . $row['dmototal'] . " WHERE movempresa = '$empresa' AND movprefijo = '$prefijo' AND movnumero = '$numero'");
    //$row2 = $db->query("SELECT dmovinventario FROM detmovimientos WHERE dmoproducto = '$producto' ORDER BY dmoid DESC LIMIT 1")->fetch(PDO::FETCH_ASSOC);
    
    //if ($row2['dmovinventario'] != '') 
    $qry = $db->query("UPDATE productos SET procantidad = procantidad + " . $row['dmocantidad'] . ", proestado = 'OK' WHERE proid = '$producto'");
    
    /*else 
        $qry = $db->query("UPDATE productos SET procantidad = 0, procosto = " . $row2['dmovinventario'] . ", proestado = 'OK' WHERE proid = '$producto'");
    */

} elseif (isset($_POST['empresa'])) {
    $empresa = $_POST['empresa'];
    $prefijo = $_POST['prefijo'];
    $numero = $_POST['numero'];
    $producto = $_POST['producto'];
    $cantidad = $_POST['cantidad'];
    $vunitario = $_POST['vunitario'];
    $tunitario = $_POST['tunitario'];
    $row = $db->query("SELECT * FROM productos WHERE proid = '$producto'")->fetch(PDO::FETCH_ASSOC);
    $nuecantidad = $row['procantidad'] - $cantidad;
    $nuecosto = $row['procosto'];
    //$qry = $db->query("UPDATE productos SET procantidad = $nuecantidad, procosto = $nuecosto, proestado = 'PROCESO' WHERE proid = '$producto'");
    $qry = $db->query("UPDATE productos SET procantidad = $nuecantidad, proestado = 'PROCESO' WHERE proid = '$producto'");
    $qry = $db->query("UPDATE movinventario SET movvalor = movvalor + $tunitario WHERE movempresa = '$empresa' AND movprefijo = '$prefijo' AND movnumero = '$numero'");
    $qry = $db->query("INSERT INTO detmovimientos (dmoempresa, dmoprefijo, dmonumero, dmoproducto, dmocantidad, dmounitario, dmototal, dmoinventario, dmovinventario, dmotinventario) 
    VALUES ('$empresa', '$prefijo', '$numero', '$producto', $cantidad, $vunitario, $tunitario, $nuecantidad, $nuecosto, $nuecantidad * $nuecosto)");

}

if(isset($_GET['hide']))
$hide = $_GET['hide'];

$row = $db->query("SELECT * FROM movinventario INNER JOIN empresas ON movempresa = empid WHERE movempresa = '$empresa' AND movprefijo = '$prefijo' AND movnumero = $numero")->fetch(PDO::FETCH_ASSOC);

?>

<div>
    <label for="producto">Producto</label>
    <select id="producto" name="producto" class="form-control validate[required] text-input" onchange="update_total(); select_product(event);" data-live-search="true">
        <option value="">SELECCIONE</option>
        <?php
        $qry = $db->query("SELECT * FROM productos WHERE procantidad > 0 AND prodesactivado = 0 AND proid NOT IN (SELECT dmoproducto FROM detmovimientos WHERE dmoempresa = '$empresa' AND dmoprefijo = '$prefijo' AND dmonumero = '$numero') ORDER BY proid;");
        while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
            if ($row2['proestado'] == 'OK')
                echo '<option value="' . $row2['proid'] . '">' . $row2['proid'] . ' / ' . $row2['pronombre'] . '</option>';
            else echo '<option value="' . $row2['proid'] . '" style="background-color:RED" disabled>' . $row2['proid'] . ' / ' . $row2['pronombre'] . '</option>';
        }
        ?>
    </select>

    <label for="cantidad">Cantidad en inventario</label>
    <input type="text" class="cantidad" disabled id="inventario" name="inventario" value=""/>
    <label for="cantidad">Cantidad</label>
    <input onkeyup="update_total();" type="text" id="cantidad" name="cantidad" class="cantidad validate[required, custom[onlyNumberSp], funcCall[checkCantidad], min[1]]" value="1" autocomplete='off'/>
    
    <?php if($row['movprefijo'] != "FD") {?>
        <input type="hidden" id="vunitario" name="vunitario" class="valor validate[required, custom[onlyNumberSp]]" value="0"/>
        <input type="hidden" id="tunitario" name="tunitario" class="form-control not-w valor validate[required, custom[onlyNumberSp]]"/>
        <button type="button" class="btn btn-block btn-success" name="insproducto" value="insproducto" onclick="add_product();">
            AGREGAR
        </button>
    <?php } else {?>
        <label for="vunitario">Valor unitario</label>
        <input onkeyup="update_total();" type="text" id="vunitario" name="vunitario" class="valor validate[required, custom[onlyNumberSp]]" value="0" autocomplete='off'/>
        <label for="tunitario">Sub total</label>
        <div class="input-group">
            <input type="text" id="tunitario" name="tunitario" class="form-control not-w valor validate[required, custom[onlyNumberSp]]" readonly/>
            <div class="input-group-append">
                <button type="button" class="btn btn-success" name="insproducto" value="insproducto" onclick="add_product();">
                    <i class="fas fa-plus-square"></i>
                </button>
            </div>
        </div>
    <?php } ?>
</div>
<div id="list-product">
    <label for="" class="col-md-12" style="text-align:center!important;">
        <strong>Productos Agregados</strong>
    </label>
    <?php if($row['movprefijo'] == "FD") {?>
        <div class="input-group">
            <div class="input-group-prepend">
                <label class="input-group-text font-weight-bold">Total de la salida</label>
            </div>
            <input type="text" class="valor form-control not-w mt-1" value="<?php echo $row['movvalor'] ?>" readonly /></td>
        </div>
    <?php } 
    
    $qry = $db->query("SELECT * FROM detmovimientos INNER JOIN productos ON dmoproducto = proid WHERE dmoempresa = '$empresa' AND dmoprefijo = '$prefijo' AND dmonumero = '$numero'");
    $num = $qry->rowCount();
    $hide_input = $num;
    $index_producto = 0;
    while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
    $index_producto++; ?>
    
        <p>
            <label class="index_producto default">#<?php echo $index_producto; ?></label>
        </p>
        <p align="center">
            <label>  <?php echo $row2['proid'] . " / " . $row2['pronombre'] ?> </label>
            
            <?php if($row['movprefijo'] == "FD") {?>
                <div class="row">
                    <div class="input-group mb-1 col-md-6">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Cantidad</span>
                        </div>
                        <input type="text" class="form-control not-w cantidad edit-cantidad"
                            value="<?php echo $row2['dmocantidad'] ?>"
                            />
                    </div>
                    <div class="input-group mb-1 col-md-6">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Valor unitario</span>
                        </div>
                        <input type="text" class="form-control not-w cantidad  edit-vunitario"
                            value="<?php echo $row2['dmounitario'] ?>"
                            />
                    </div>
                    <div class="input-group mb-1 col-md-12">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Sub total</span>
                        </div>
                        <input type="text" class="form-control not-w cantidad"
                            value="<?php echo $row2['dmototal'] ?>"
                            readonly/>
                        <div class="input-group-append">
                            <div class="">
                                <button type="button" class="btn btn-block btn-warning"
                                    onclick="edit_product_FD('<?php echo $row2['dmoproducto']; ?>', this);"  >
                                    <i class="fas fa-pencil-alt"></i>
                                </button>
                            </div>
                            <div class="">
                                <button type="button" class="btn btn-block btn-danger"
                                    onclick="remove_product('<?php echo $row2['dmoproducto']; ?>');"  >
                                    <i class="fas fa-trash"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } else { ?>
                <div class="row">
                    <div class="input-group mb-1 col-md-6">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Cantidad</span>
                        </div>
                        <input type="text" class="form-control not-w cantidad edit-cantidad"
                            value="<?php echo $row2['dmocantidad'] ?>"/>
                    </div>
                    
                    <div class="col-md-2">
                        <button type="button" class="btn btn-block btn-warning"
                            onclick="edit_product('<?php echo $row2['dmoproducto']; ?>', this);"  >
                            <i class="fas fa-pencil-alt"></i>
                        </button>
                    </div>
                    <div class="col-md-2">
                        <button type="button" class="btn btn-block btn-danger"
                            onclick="remove_product('<?php echo $row2['dmoproducto']; ?>');"  >
                            <i class="fas fa-trash"></i>
                        </button>
                    </div>
                </div>

            <?php } ?>

        </p>
    <?php } ?>

</div>