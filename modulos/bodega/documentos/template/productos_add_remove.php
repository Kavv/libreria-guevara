<?php
$r = '../../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
if (isset($_GET['producto'])) {
    $empresa = $_GET['empresa'];
    $prefijo = $_GET['prefijo'];
    $numero = $_GET['numero'];
    $producto = $_GET['producto'];
    // Obtenemos los datos del detalle de movimiento a eliminar
    $row = $db->query("SELECT * FROM detmovimientos WHERE dmoempresa = '$empresa' AND dmoprefijo = '$prefijo' AND dmonumero = '$numero' AND dmoproducto = '$producto'")->fetch(PDO::FETCH_ASSOC);
    // Eliminamos el registro del detalle de movimiento correspondiente 
    $qry = $db->query("DELETE FROM detmovimientos WHERE dmoempresa = '$empresa' AND dmoprefijo = '$prefijo' AND dmonumero = '$numero' AND dmoproducto = '$producto'");
    // Actualizamos el valor del movimiento principal restando el valor total del detalle del movimiento que se ha eliminado.
    $qry = $db->query("UPDATE movinventario SET movvalor = movvalor - " . $row['dmototal'] . " WHERE movempresa = '$empresa' AND movprefijo = '$prefijo' AND movnumero = '$numero'");
    // Obtenemos el costo del producto, consultando el ultimo detalle de movimiento del producto eliminado, 
    $row2 = $db->query("SELECT dmovinventario FROM detmovimientos WHERE dmoproducto = '$producto' ORDER BY dmoid DESC LIMIT 1")->fetch(PDO::FETCH_ASSOC);


    // En caso de encontrar un detalle de movimiento anterior del producto 
    // entonces actualizamos la cantidad del prodcuto restando 
    // la cantidad del detalle de movimiento que se ha eliminado
    if (!empty($row2)) 
    {
        if($row2['dmovinventario'] == '')
            $row2['dmovinventario'] = 0;
        $qry = $db->query("UPDATE productos SET procantidad = procantidad - " . $row['dmocantidad'] . ", procosto = " . $row2['dmovinventario'] . ", proestado = 'OK' WHERE proid = '$producto'");
    }
    // De no encontrar un detalle de movimiento previo
    // Entonces significa que el procuto debe estar en 0
    else $qry = $db->query("UPDATE productos SET procantidad = 0, procosto = 0, proestado = 'OK' WHERE proid = '$producto'");

} elseif (isset($_POST['empresa'])) {

    $empresa = $_POST['empresa'];
    $prefijo = $_POST['prefijo'];
    $numero = $_POST['numero'];
    $producto = $_POST['producto'];
    $cantidad = $_POST['cantidad'];
    $vunitario = 0; //$_POST['vunitario'];
    $tunitario = 0; //$_POST['tunitario'];

    $row = $db->query("SELECT * FROM productos WHERE proid = '$producto'")->fetch(PDO::FETCH_ASSOC);

    $nuecantidad = $row['procantidad'] + $cantidad;
    //$nuecosto = ($row['procosto'] * ($row['procantidad'] / $nuecantidad)) + ($vunitario * ($cantidad / $nuecantidad));
    $nuecosto = 0;
    //$qry = $db->query("UPDATE productos SET procantidad = $nuecantidad, procosto = $nuecosto, proestado = 'PROCESO' WHERE proid = '$producto'");
    $qry = $db->query("UPDATE productos SET procantidad = $nuecantidad, proestado = 'PROCESO' WHERE proid = '$producto'");


    // $qry = $db->query("UPDATE movimientos SET movvalor = movvalor + $tunitario WHERE movempresa = '$empresa' AND movprefijo = '$prefijo' AND movnumero = $numero");


    //$qry = $db->query("INSERT INTO detmovimientos (dmoempresa, dmoprefijo, dmonumero, dmoproducto, dmocantidad, dmounitario, dmototal, dmoinventario, dmovinventario, dmotinventario) VALUES ('$empresa', '$prefijo', $numero, '$producto', $cantidad, $vunitario, $tunitario, $nuecantidad, $nuecosto, $nuecantidad * $nuecosto)");
    $qry = $db->query("INSERT INTO detmovimientos (dmoempresa, dmoprefijo, dmonumero, dmoproducto, dmocantidad, dmoinventario) VALUES ('$empresa', '$prefijo', $numero, '$producto', $cantidad, $nuecantidad)");

}
$row = $db->query("SELECT * FROM movinventario INNER JOIN empresas ON movempresa = empid WHERE movempresa = '$empresa' AND movprefijo = '$prefijo' AND movnumero = $numero")->fetch(PDO::FETCH_ASSOC);

?>
    
    <div class="row">
        <div class="input-group mb-1 col-md-12">
            <label for="producto">Producto</label>

            <div class="col-md-7 p-0">
                <select id="producto" name="producto" 
                    class="selectpicker form-control validate[required] text-input"
                    data-live-search="true" data-width="100%">
                    <option value="">SELECCIONE</option>
                    <?php
                    $qry = $db->query("SELECT * FROM productos WHERE prodesactivado = 0 AND proid NOT IN (SELECT dmoproducto FROM detmovimientos WHERE dmoempresa = '$empresa' AND dmoprefijo = '$prefijo' AND dmonumero = '$numero') ORDER BY proid;");
                    while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
                        if ($row2['proestado'] == 'OK')
                            echo '<option value="' . $row2['proid'] . '">' . $row2['proid'] . " / " . $row2['pronombre'] . '</option>';
                        else 
                            echo '<option value="' . $row2['proid'] . '" style="background-color:RED" disabled>' . $row2['proid'] . " / " . $row2['pronombre'] . '</option>';
                    }
                    ?>
                </select>
            </div>
            <div class="input-group-append col-md-5 p-0">
                <input type="text"  id="cantidad" name="cantidad"
                    class="col-md-9 cantidad validate[required, custom[onlyNumberSp], min[1]]" value="1" autocomplete='off' placeholder="Cantidad"/>
            
                <button type="button" class=" col-md-3 btn btn-success" onclick="add_product();"  >
                    <i class="fas fa-plus-square"></i>
                </button>
            </div>
        </div>
    </div>
    <div>
        <label for="" class="col-md-12 mt-4" style="text-align:center!important;">
            <strong>Productos Agregados</strong>
        </label>

        <?php
        $qry = $db->query("SELECT * FROM detmovimientos INNER JOIN productos ON dmoproducto = proid WHERE dmoempresa = '$empresa' AND dmoprefijo = '$prefijo' AND dmonumero = $numero order by detmovimientos.created_at asc");

        $num = $qry->rowCount();
        $index_producto = 0;
        while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
        $index_producto++;
        ?>
            <p>
                <label class="index_producto default">#<?php echo $index_producto; ?></label>
            </p>
            <p align="center" class="product-item">
                <label>  <?php echo $row2['proid'] . " / " . $row2['pronombre'] ?> </label>
                <div class="row">
                    <div class="input-group mb-1 col-md-6">

                        <div class="input-group-prepend">
                            <span class="input-group-text">Cantidad</span>
                        </div>
                        <input type="text" class="edit-cantidad form-control not-w cantidad"
                            value="<?php echo $row2['dmocantidad'] ?>"  />
                    </div>
                    
                    <div class="col-md-2">
                        <button type="button" class="btn btn-block btn-warning"
                            onclick="edit_product('<?php echo $row2['dmoproducto']; ?>', this);"  >
                            <i class="fas fa-pencil-alt"></i>
                        </button>
                    </div>
                    <div class="col-md-2">
                        <button type="button" class="btn btn-block btn-danger"
                            onclick="remove_product('<?php echo $row2['dmoproducto']; ?>');"  >
                            <i class="fas fa-trash"></i>
                        </button>
                    </div>
                </div>
            </p>
        <?php } ?>
    </div> 