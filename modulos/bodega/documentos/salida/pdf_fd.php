<?php
$r = '../../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
require($r . 'incluir/fpdf/fpdf.php');
$empresa = $_GET['empresa'];
$prefijo = $_GET['prefijo'];
$numero = $_GET['numero'];
$sql = "SELECT movinventario.*, empnombre, tipnombre, infoextra.*, departamentos.*, ciudades.* FROM movinventario 
INNER JOIN empresas ON movempresa = empid 
INNER JOIN tipdocumentos ON movprefijo = tipid 
LEFT JOIN infoextra ON infoextra.id = movinfo
LEFT JOIN departamentos ON depid = departamento
LEFT JOIN ciudades ON (ciuid = ciudad AND ciudepto = departamento)
WHERE movempresa = '$empresa' AND movprefijo = '$prefijo' AND movnumero = '$numero'";

$row = $db->query($sql)->fetch(PDO::FETCH_ASSOC);

$infoextra = $entregado = $bodeguero = $comentario = $entregado = $telefono1 = $telefono2 = $direccion = "";
if($row)
{
    $digitador = '';
    $id_info = $row['movinfo'];
    if($id_info != "")
    {
        $infoextra = $db->query("SELECT infoextra.*, ciunombre FROM infoextra INNER JOIN ciudades ON (ciuid = ciudad AND ciudepto = departamento) WHERE id = '$id_info'")->fetch(PDO::FETCH_ASSOC);
        $entregado = $infoextra['entregado'];
        $bodeguero = $infoextra['bodeguero'];
        $comentario = $infoextra['comentario'];
        $telefono1 = $infoextra['telefono1'];
        $telefono2 = $infoextra['telefono2'];
        if(isset($infoextra['digitador']))
        $digitador = $infoextra['digitador'];
        $distribuidor = $infoextra['distribuidor'];
        $direccion = $infoextra['direccion'];
    }
}

// Obtenemos la moneda correspondiente
$simbolo = $_GET['simbolo'];


$fecha = $row['movfecha'];
$fechaComoEntero = strtotime($fecha);

$year = date("y", $fechaComoEntero);
$month = date("m", $fechaComoEntero);
$day = date("d", $fechaComoEntero);

$nombre = strupperEsp($distribuidor);

$pdf = new FPDF('P','mm',array(210,158));
$pdf->AddFont('LucidaConsole', '', 'lucon.php');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetTitle("FACTURA A DISTRIBUIDORES #".$numero);
#Establecemos los márgenes izquierda, arriba y derecha:
$pdf->SetMargins(5,0);

$w_codigo = 16;
$w_cantidad = 16;
$w_producto = 64;
$w_precio = 18;
$w_total = 18;
$max_w = $w_codigo + $w_cantidad + $w_producto + $w_precio + $w_total;

//$margen_r = 8;
$margen_r = 0.1;

$font_size = 8;
$border = 0;

$pdf->SetFont('LucidaConsole', '', $font_size);
$pdf->ln(7);
// Día Mes Año
$pdf->Cell($margen_r, 5, '', 0, 0);
$pdf->Cell($max_w, 5, $day ."       " . $month ."       " . $year . "  ", $border, 1, 'R');

// Nombre y Cedula
$pdf->ln(7);
$pdf->Cell($margen_r, 5, '', 0, 0);
$pdf->Cell($max_w*0.66, 5, utf8_decode($nombre), $border, 1);
//$pdf->Cell($max_w*0.33, 5, $row['solcliente'], $border, 1, 'R');

// Dir y Tel
$pdf->ln(6);
$pdf->Cell($margen_r, 5, '', 0, 0);
$pdf->Cell($max_w*0.66, 5, utf8_decode($row['depnombre']." / ".$row['ciunombre']), $border, 0);
$pdf->Cell($max_w*0.33, 5, $telefono1 . " / " . $telefono2, $border, 1, 'R');

// Direccion
$pdf->ln(1);
$direccion1 = substr($direccion, 0, 76);
$direccion2 = substr($direccion, 76, 76);
$direccion3 = substr($direccion, 152, 76);

$pdf->Cell($margen_r, 4, '', 0, 0);
$pdf->Cell($max_w, 4, utf8_decode($direccion1), $border, 1);
$pdf->Cell($margen_r, 4, '', 0, 0);
$pdf->Cell($max_w, 4, utf8_decode($direccion2), $border, 1);
$pdf->Cell($margen_r, 4, '', 0, 0);
$pdf->Cell($max_w, 4, utf8_decode($direccion3), $border, 1);



$pdf->Cell($margen_r, 5, '', 0, 0);
$pdf->Cell(18, 5, '', 0, 0, 'C');
$pdf->Cell(18, 5, '', 0, 0, 'C');
$pdf->Cell(60, 5, '', 0, 0, 'C');
$pdf->Cell(18, 5, '', 0, 0, 'C');
$pdf->Cell(18, 5, '', 0, 1, 'C');

//$productos = $db->query("SELECT detproducto, detcantidad, detprecio, pronombre FROM solicitudes INNER JOIN detmovimientos ON detsolicitud = solid INNER JOIN productos on proid = detproducto WHERE solid = '$contrato';");
$productos = $db->query("SELECT * FROM detmovimientos INNER JOIN productos ON dmoproducto = proid WHERE dmoempresa = '$empresa' AND dmoprefijo = '$prefijo' AND dmonumero = $numero");

$cantidad_productos = $productos->rowCount();
$index_fila = 0;
$max_filas = 20;
//$pdf->SetFont('LucidaConsole', '', 8);
$pdf->SetFont('LucidaConsole', '', $font_size);



$subtotal = 0;
while($producto = $productos->fetch(PDO::FETCH_ASSOC))
{
    $p_id = $producto['dmoproducto'];
    $p_nombre = substr($producto['pronombre'], 0, 35);
    $p_cantidad = $producto['dmocantidad'];
    $p_precio = $producto['dmounitario'];
    $p_total = round(($p_precio * $p_cantidad), 2);

    $pdf->Cell($margen_r, 4, '', 0, 0);
    $pdf->Cell($w_codigo, 4, $p_id, $border, 0, 'R');
    $pdf->Cell($w_cantidad, 4, $p_cantidad, $border, 0, 'R');
    $pdf->Cell($w_producto, 4, utf8_decode($p_nombre), $border, 0);
    $pdf->Cell($w_precio, 4, $simbolo . $p_precio, $border, 0, 'R');
    $pdf->Cell($w_total, 4, $simbolo . $p_total, $border, 1, 'R');
    $subtotal += $p_total;
    $index_fila++;
    if($index_fila == 20)
        break;
}
while($index_fila < $max_filas)
{
    $pdf->Cell($margen_r, 4, '', 0, 0);
    $pdf->Cell($w_codigo, 4, "", $border, 0, 'C');
    $pdf->Cell($w_cantidad, 4, "", $border, 0, 'C');
    $pdf->Cell($w_producto, 4, "", $border, 0);
    $pdf->Cell($w_precio, 4, "", $border, 0, 'R');
    $pdf->Cell($w_total, 4, "", $border, 1, 'R');
    $index_fila++;
}
/* $pdf->Cell($margen_r, 4, '', 0, 0);
$pdf->Cell($w_codigo, 4, '100', $border, 0, 'R');
$pdf->Cell($w_cantidad, 4, '20', $border, 0, 'R');
$pdf->Cell($w_producto, 4, 'PRODUCTO FINAL', $border, 0);
$pdf->Cell($w_precio, 4, '200', $border, 0, 'R');
$pdf->Cell($w_total, 4, '400', $border, 1, 'R'); */

$pdf->SetFont('LucidaConsole', '', $font_size);
// Observaciones

$pdf->ln(2);
$observacion1 = substr($comentario,0, 40);
$observacion2 = substr($comentario,40, 55);
$observacion3 = substr($comentario,95, 55);
$observacion4 = substr($comentario,150, 55);

$w_observacion = $w_codigo + $w_cantidad + $w_producto;

$pdf->Cell($margen_r, 5, '', 0, 0);
$pdf->Cell($w_observacion, 5, "               ".utf8_decode($observacion1), $border, 0);
$pdf->Cell($w_precio, 5, "", $border, 0);
$pdf->Cell($w_total, 5, $simbolo. "$subtotal", $border, 1, 'R');

$descuento = $row['movdescuento'];
$pdf->Cell($margen_r, 5, '', 0, 0);
$pdf->Cell($w_observacion, 5, utf8_decode($observacion2), $border, 0);
$pdf->Cell($w_precio, 5, "", $border, 0);
$pdf->Cell($w_total, 5, $simbolo. "$descuento", $border, 1, 'R');

$pdf->Cell($margen_r, 5, '', 0, 0);
$pdf->Cell($w_observacion, 5, utf8_decode($observacion3), $border, 0);
$pdf->Cell($w_precio, 5, "", $border, 0);
$pdf->Cell($w_total, 5, "", $border, 1, 'R');

$gran_total = $subtotal - ($descuento);
$pdf->Cell($margen_r, 5, '', 0, 0);
$pdf->Cell($w_observacion, 5, utf8_decode($observacion4), $border, 0);
$pdf->Cell($w_precio, 5, "", $border, 0);
$pdf->Cell($w_total, 5, $simbolo. "$gran_total", $border, 1, 'R');




$pdf->Output("FACTURA A DISTRIBUIDOR #".$numero , 'I');
?>






