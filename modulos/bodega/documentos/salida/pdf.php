<?php
$r = '../../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
require($r . 'incluir/fpdf/fpdf.php');
$empresa = $_GET['empresa'];
$prefijo = $_GET['prefijo'];
$numero = $_GET['numero'];
$prueba = 1;
if(isset($_GET['v']))
$prueba = $_GET['v'];

$sql = "SELECT movinventario.*, clientes.*, empnombre, tipnombre FROM movinventario 
LEFT JOIN clientes ON movtercero = cliid 
INNER JOIN empresas ON movempresa = empid 
INNER JOIN tipdocumentos ON movprefijo = tipid 
WHERE movempresa = '$empresa' AND movprefijo = '$prefijo' AND movnumero = '$numero'";

//echo $sql;
$row = $db->query($sql)->fetch(PDO::FETCH_ASSOC);
$infoextra = $entregado = $bodeguero = $comentario = $entregado = $telefono1 = $telefono2 = "";
if($row)
{
    $digitador = '';
    $id_info = $row['movinfo'];
    if($id_info != "")
    {
        $infoextra = $db->query("SELECT infoextra.*, depnombre, ciunombre FROM infoextra 
        INNER JOIN departamentos ON depid = departamento 
        LEFT JOIN ciudades ON ciudepto = departamento and ciuid = ciudad 
        WHERE id = '$id_info'")->fetch(PDO::FETCH_ASSOC);
        $entregado = $infoextra['entregado'];
        $bodeguero = $infoextra['bodeguero'];
        $comentario = $infoextra['comentario'];
        $telefono1 = $infoextra['telefono1'];
        $telefono2 = $infoextra['telefono2'];
        $departamento = $infoextra['depnombre'];
        $ciudad = $infoextra['ciunombre'];
        if(isset($infoextra['digitador']))
        $digitador = $infoextra['digitador'];
    }
}

class PDF extends FPDF
{
    function Header()
    {
        global $row, $departamento,$ciudad, $entregado, $bodeguero;
        $this->SetTitle($row['tipnombre'] . ' NO: ' . $row['movnumero']);
        $this->SetFont('LucidaConsole', '', 8);
        $this->Cell(0, 5, '-----------------------------------------------------------------------------------------------------------------', 0, 1, 'C');
        $this->SetFont('LucidaConsole', '', 12);
        $this->Cell(0, 5, utf8_encode($row['empnombre']), 0, 1, 'C');
        $this->Cell(0, 5, $row['tipnombre'] . ' NO: ' . $row['movnumero'], 0, 1, 'C');
        $this->SetFont('LucidaConsole', '', 8);
        $this->Cell(0, 5, '-----------------------------------------------------------------------------------------------------------------', 0, 1, 'C');
    
        $new_format = date("d/m/Y", strtotime($row['movfecha']));
        $this->SetFont('LucidaConsole', '', 10);
        $this->Cell(100, 5, 'FECHA: ' . $new_format , 0, 0);
        $this->Cell(110, 5, 'DEPARTAMENTO: ' . $departamento . " / " . $ciudad, 0, 1);
        $this->Cell(100, 5, 'ENTREGADO: ' . utf8_encode($entregado), 0, 0);
        $this->Cell(100, 5, 'BODEGUERO: ' . utf8_encode($bodeguero), 0, 1);
        
        $this->Ln(1);
        $this->Cell(20, 5, 'CODIGO', 1, 0, 'C');
        $this->Cell(15, 5, 'CANT.', 1, 0, 'C');
        $this->Cell(155, 5, 'PRODUCTO', 1, 1, 'C');
    
    }
    function Footer()
    {
        global $digitador;
        $this->Ln(1);
        $this->AddFont('LucidaConsole', '', 'lucon.php');
        $this->SetFont('LucidaConsole', '', 8);
        $this->Cell(60, 3, '', 0, 0, 'C');
        $this->Cell(65, 3, '', 0, 0, 'C');
        $this->Cell(65, 3, $digitador, 0, 1, 'C');
        
        $this->Cell(60, 2, '__________________________', 0, 0, 'C');
        $this->Cell(65, 2, '________________________', 0, 0, 'C');
        $this->Cell(65, 2, '__________________________', 0, 1, 'C');
        $this->SetFont('LucidaConsole', '', 8);
        $this->Cell(60, 5, 'ENTREGO', 0, 0, 'C');
        $this->Cell(65, 5, 'RECIBIO', 0, 0, 'C');
        $this->Cell(65, 5, 'SISTEMATIZO', 0, 1, 'C');
    }
}

//$pdf = new PDF();
if($prueba == 0)
$pdf = new PDF('L','mm',array(148,210));
if($prueba == 1)
$pdf = new PDF();

$pdf->AddFont('LucidaConsole', '', 'lucon.php');
$pdf->AliasNbPages();
$pdf->AddPage();
$row3 = $db->query("SELECT * FROM proveedores WHERE pvdid = '" . $row['movtercero'] . "'")->fetch(PDO::FETCH_ASSOC);
if (!isset($row3['pvdnombre'])) {
    $row3['pvdnombre'] = "";
}

$qry = $db->query("SELECT * FROM detmovimientos INNER JOIN productos ON dmoproducto = proid WHERE dmoempresa = '$empresa' AND dmoprefijo = '$prefijo' AND dmonumero = $numero");
$index_fila = 0;
$max_filas = 10;

if($prueba == 0)
{
    while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
        $pdf->Cell(20, 5, $row2['dmoproducto'], 1, 0, 'C');
        $pdf->Cell(15, 5, $row2['dmocantidad'], 1, 0, 'C');
        $pdf->Cell(155, 5, utf8_decode($row2['pronombre']), 1, 1, 'L');
        //$pdf->Cell(20, 5, number_format($row2['dmounitario'], 2), 1, 0, 'R');
        //$pdf->Cell(25, 5, number_format($row2['dmototal'], 2), 1, 1, 'R');
        $index_fila++;
        if($index_fila == $max_filas)
        {
            comentario();
            $pdf->AddPage();
            $index_fila = 0;
        }
    }
    while($index_fila < $max_filas)
    {
        $pdf->Cell(20, 5, '', 1, 0, 'C');
        $pdf->Cell(15, 5, '', 1, 0, 'C');
        $pdf->Cell(155, 5, '', 1, 1, 'C');
        $index_fila++;

        if($index_fila == $max_filas)
        {
            comentario();
        }
    }
}

$part = 1;
if($prueba == 1)
{
    
    while ($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
        $pdf->Cell(20, 5, $row2['dmoproducto'], 1, 0, 'C');
        $pdf->Cell(15, 5, $row2['dmocantidad'], 1, 0, 'C');
        $pdf->Cell(155, 5, utf8_decode($row2['pronombre']), 1, 1, 'L');
        
        $index_fila++;
        if($index_fila == $max_filas)
        {
            comentario();
            if($part == 1)
            {
                $pdf->Footer();
                $pdf->ln(30);
                $pdf->header();
                $part = 2;
            }
            else
            {
                $pdf->AddPage();
                $part = 1;
            }
            $index_fila = 0;
        }
    }
    while($index_fila < $max_filas)
    {
        $pdf->Cell(20, 5, '', 1, 0, 'C');
        $pdf->Cell(15, 5, '', 1, 0, 'C');
        $pdf->Cell(155, 5, '', 1, 1, 'C');
        $index_fila++;
    
        if($index_fila == $max_filas)
        {
            comentario();
        }
    }
}


function comentario()
{
    $w_observacion = 0;
    $border = 1;
    global $comentario, $pdf;
    $observacion1 = substr($comentario,0, 80);
    $observacion2 = substr($comentario,80, 100);
    $observacion3 = substr($comentario,180, 100);
    $observacion4 = substr($comentario,280, 100);
    
    $pdf->Ln(1);
    $pdf->SetFont('LucidaConsole', '', 8);
    $pdf->Cell($w_observacion, 5, 'OBSERVACION: ' . utf8_decode($observacion1), $border, 1);
    $pdf->Cell($w_observacion, 5, utf8_decode($observacion2), $border, 1);
    $pdf->Cell($w_observacion, 5, utf8_decode($observacion3), $border, 1);

}


$pdf->Output($row['tipnombre'] . ' NO ' . $row['movnumero']. '.pdf', 'I');
?>