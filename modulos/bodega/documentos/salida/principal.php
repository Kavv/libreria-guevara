<?php
$r = '../../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
?>
<!doctype html>

<html lang="es">

<head>
    <title>TIPO ORDEN SALIDA</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">
    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>
    <script type="text/javascript" src="<?php echo $r ?>incluir/funciones.js"></script>
    
</head>
<body>
<?php require($r . 'incluir/src/login.php') ?>
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <?php require($r . 'incluir/src/menu.php') ?>
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a href="#">Bodega</a>
            <div class="mapa_div"></div>
            <a href="#">Documentos</a>
            <div class="mapa_div"></div>
            <a class="current">Salida</a>
        </article>
        <article id="contenido">
            <form id="form" name="form" action="salida.php" method="post">
                <fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
                    <legend class="ui-widget ui-widget-header ui-corner-all">Documento de salida</legend>
                    <p>
                        <label for="documento">Documento:</label>
                        <select id="prefijo" name="prefijo" class="validate[required]">
                            <?php
                            $qry = $db->query("SELECT * FROM tipdocumentos WHERE tiptipo = 'SALINV' ORDER BY tipnombre");
                            while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
                                echo '<option value=' . $row['tipid'] . '>' . $row['tipid'] . '/'  . $row['tipnombre'] . '</option>';
                            }
                            ?>
                        </select>
                    </p>
                    <p>
                        <label for="empresa">Empresa:</label>
                        <select id="empresa" name="empresa" class="validate[required]">
                            <?php
                            $qry = $db->query("SELECT * FROM empresas ORDER BY empnombre");
                            while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
                                echo '<option value=' . $row['empid'] . '>' . $row['empnombre'] . '</option>';
                            }
                            ?>
                        </select>
                    </p>
                    <p class="boton">
                        <button type="submit" class="btn btn-primary" name="ingresar" value="ingresar">ingresar</button>
                    </p>
                </fieldset>
            </form>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php') ?>
</section>
<?php
if (isset($_GET['error'])) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['error'] . '</div>';
elseif (isset($_GET['mensaje'])) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['mensaje'] . '</div>';
?>
</body>
</html>