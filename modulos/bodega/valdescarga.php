<?php
$r = '../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
if (isset($_POST['descargar'])) {
    if ($_POST['estado'] == 'RECHAZADO') {
        $qry = $db->query("UPDATE solicitudes SET solestado = 'RECHAZADO ENTREGA', solusudescarga = '" . $_SESSION['id'] . "', soltcadescarga = '" . $_POST['tcausal'] . "', solcaudescarga = '" . $_POST['causal'] . "', solfechdescarga = NOW() WHERE solempresa = '" . $_POST['empresa'] . "' AND solid = '" . $_POST['id'] . "'");
        $mensaje = 'La solicitud se rechazo';
    } elseif ($_POST['estado'] == 'ENTREGADO') {
        $qry = $db->query("UPDATE solicitudes SET solestado = 'ENTREGADO', solusudescarga = '" . $_SESSION['id'] . "', solfechdescarga = NOW() WHERE solempresa = '" . $_POST['empresa'] . "' AND solid = '" . $_POST['id'] . "'");
        $mensaje = 'La solicitud se entrego';
    }
    header('Location:lisplanilla.php?planilla=' . $_POST['planilla'] . '&mensaje=' . $mensaje);
    exit();
}
$row = $db->query("SELECT * FROM (((solicitudes INNER JOIN empresas ON solempresa = empid) INNER JOIN clientes ON solcliente = cliid) INNER JOIN departamentos ON soldepentrega = depid)INNER JOIN ciudades ON (ciudepto = soldepentrega AND solciuentrega = ciuid) WHERE solempresa = '" . $_GET['empresa'] . "' AND solid = '" . $_GET['id'] . "'")->fetch(PDO::FETCH_ASSOC);
?>
<!doctype html>
<html>
<head>
    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>
    <script type="text/javascript">
        function habilitar(valor) {
            if (valor == 'RECHAZADO') {
                document.getElementById('tcausal').disabled = false;
                document.getElementById('causal').disabled = false;
            } else {
                document.getElementById('tcausal').disabled = true;
                document.getElementById('causal').disabled = true;
            }
        }

        $(document).ready(function () {
            $('#form').validationEngine({
                showOneMessage: true,
                onValidationComplete: function (form, status) {
                    if (status) {
                        carga();
                        return true;
                    }
                }
            });
            $('#tcausal').change(function (event) {
                var id1 = $('#tcausal').find(':selected').val();
                $('#causal').load('<?php echo $r ?>incluir/carga/causales.php?id1=' + id1);
            });

        });
    </script>
</head>
<body>
<?php require($r . 'incluir/src/login.php') ?>
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <?php require($r . 'incluir/src/menu.php') ?>
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a class="current">Bodega</a>
        </article>
        <article id="contenido">
            <form id="form" name="form" action="valdescarga.php" method="post">
                <fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
                    <legend class="ui-widget ui-widget-header ui-corner-all">Solicitud a descargar</legend>
                    <p>
                        <label for="empresa">Empresa: </label><input type="hidden" name="empresa"
                                                                     value="<?php echo $row['solempresa'] ?>"><?php echo $row['empnombre'] ?>
                    </p>
                    <p>
                        <label for="id">Solicitud: </label> <input type="text" name="id" class="pedido"
                                                                   value="<?php echo $row['solid'] ?>" readonly/>
                        <input type="hidden" name="planilla" value="<?php echo $_GET['planilla'] ?>"/>
                    </p>
                    <p>
                        <label for="factura">Factura: </label> <input type="text" name="factura" class="consecutivo"
                                                                      value="<?php echo $row['solfactura'] ?>"
                                                                      readonly/>
                    </p>
                    <p>
                        <label for="ubicacion">Ubicacion: </label><?php echo $row['depnombre'] . ' / ' . $row['ciunombre'] . '. ' . $row['solbarentrega'] . ', ' . $row['solentrega'] ?>
                    </p>
                    <p>
                        <label>Aprobacion: </label>
                        <select name="estado" id="estado" class="validate[required]" onChange="habilitar(this.value);">
                            <option value="">SELECCIONE</option>
                            <option value="ENTREGADO">ENTREGADO</option>
                            <option value="RECHAZADO">RECHAZADO</option>
                        </select>
                    </p>
                    <p>
                        <label>Tipo de causal: </label>
                        <select id="tcausal" name="tcausal" class="validate[required]" disabled>
                            <option value="">SELECCIONE</option>
                            <?php
                            $qry = $db->query("SELECT * FROM tcausales ORDER BY tcanombre");
                            while ($row = $qry->fetch(PDO::FETCH_ASSOC))
                                echo '<option value="' . $row['tcaid'] . '">' . $row['tcanombre'] . '</option>';
                            ?>
                        </select>
                    </p>
                    <p>
                        <label>Causal: </label>
                        <select id="causal" name="causal" class="validate[required]" disabled>
                            <option value="">SELECCIONE</option>
                        </select>
                    </p>
                    <p class="boton">
                        <button type="button" class="btn btn-outline-primary btnatras"
                                onClick="carga(); location.href = 'lisplanilla.php?planilla=<?php echo $_GET['planilla'] ?>'">
                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-left-square-fill"
                                 fill="currentColor"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                      d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm9.5 8.5a.5.5 0 0 0 0-1H5.707l2.147-2.146a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708-.708L5.707 8.5H11.5z"/>
                            </svg>
                            Atras
                        </button>
                        <button type="submit" class="btn btn-primary" name="descargar" value="descargar">descargar
                        </button>
                    </p>
                </fieldset>
            </form>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php') ?>
</section>
</body>
</html>