<?php
$r = '../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
if ($_POST['empresa']) $empresa = $_POST['empresa'];
else $empresa = $_GET['empresa'];
$row = $db->query("SELECT * FROM solicitudes INNER JOIN mdespachos ON mdeid = solenvio WHERE solempresa = '$empresa'")->fetch(PDO::FETCH_ASSOC);
?>
<!doctype html>
<html>
<head>
    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>

    <link rel="stylesheet" type="text/css"
          href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css"/>
    <script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#tabla').dataTable({
                'bJQueryUI': true,
                'bStateSave': true,
                'sPaginationType': 'full_numbers',
                'oLanguage': {
                    'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
                },
            });
            $('.pdf').click(function () {
                newSrc = $(this).attr('data-rel');
                if (validation_file(newSrc) == 200)
                    $('#modal').html("<iframe src='" + newSrc + "' width='100%' height='100%'></iframe>");
                else
                    $('#modal').html("<spam>El PDF no fue encontrado</spam>");
                $('#modal').dialog({modal: true, width: '600', height: '800', title: 'PDF de la factura'});
            });
        });
    </script>
</head>
<body>
<?php require($r . 'incluir/src/login.php') ?>
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <?php require($r . 'incluir/src/menu.php') ?>
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a class="current">Bodega</a>
        </article>
        <article id="contenido">
            <?php
            $qryuno = $db->query("SELECT * FROM empresas where empid = '$empresa'");
            while ($rowem = $qryuno->fetch(PDO::FETCH_ASSOC)) {
                echo '<h2> Planilla por empresa = ' . $rowem['empnombre'] . ' </h2>';
            }
            ?>
            <div class="reporte">

            </div>
            <table id="tabla">
                <thead>
                <tr>
                    <th>Empresa</th>
                    <th>Solicitud</th>
                    <th>Planilla</th>
                    <th>Factura</th>
                    <th>Cliente</th>
                    <th>Depto / Ciudad</th>
                    <th>Direccion de envio</th>
                    <th>F. Registro</th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php
                $qry = $db->query("SELECT * FROM ((((solicitudes INNER JOIN empresas ON solempresa = empid) INNER JOIN clientes ON solcliente = cliid) INNER JOIN departamentos ON soldepentrega = depid) INNER JOIN ciudades ON (ciudepto = soldepentrega AND solciuentrega = ciuid)) INNER JOIN mdespachos ON solenvio = mdeid WHERE solempresa = '$empresa'");
                while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
                    ?>
                    <tr>
                        <td title="<?php echo $row['solempresa'] ?>"><?php echo $row['empnombre'] ?></td>
                        <td align="center"><?php echo $row['solid'] ?></td>
                        <td align="center"><?php echo $row['solplanilla']?></td>
                        <td align="center"><?php echo $row['solfactura'] ?></td>
                        <td title="<?php echo $row['solcliente'] ?>"><?php echo $row['clinombre'] . ' ' . $row['clinom2'] . ' ' . $row['cliape1'] . ' ' . $row['cliape2'] ?></td>
                        <td><?php echo $row['depnombre'] . ' / ' . $row['ciunombre'] ?></td>
                        <td><?php echo $row['solbarentrega'] . ", " . $row['solentrega'] ?></td>
                        <td align="center"><img src="<?php echo $r ?>imagenes/iconos/date.png"
                                                title="<?php echo $row['solfechreg'] ?>"/></td>
                        <?php
                        $qry2 = $db->query("SELECT * FROM detsolicitudes INNER JOIN productos ON detproducto = proid WHERE detempresa = '" . $row['solempresa'] . "' AND detsolicitud = '" . $row['solid'] . "'");
                        ?>
                        <td align="center"><img src="<?php echo $r ?>imagenes/iconos/pictures.png" title="<?php
                            while ($row2 = $qry2->fetch(PDO::FETCH_ASSOC))
                                echo '- ' . $row2['pronombre'] . ' (' . $row2['detcantidad'] . ') &#13;&#10;';
                            ?>"/></td>
                        <td align="center"><img src="<?php echo $r ?>imagenes/iconos/pdf.png" class="pdf"
                                                data-rel="<?php echo $r ?>pdf/solicitudes/<?php echo $row['solempresa'] . '/' . $row['solid'] . '.pdf' ?>"
                                                title="Solicitiud"></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php') ?>
</section>
<div id="modal" style="display:none"></div>
</body>
</html>