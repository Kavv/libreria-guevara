<?php
$r = '../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
require($r . 'incluir/funciones.php');
if (isset($_POST['fecha1'])) {
    $fecha1 = $_POST['fecha1'];
    $fecha2 = $_POST['fecha2'];

    $strQuery = crearConsulta("SELECT * FROM solicitudes INNER JOIN mdespachos ON mdeid = solenvio", "GROUP BY solplanilla",
        array($fecha1, "solfechdespacho BETWEEN '$fecha1 00:00:01' AND '$fecha2 23:59:59'"));
} else {
    $strQuery = "SELECT * FROM solicitudes INNER JOIN mdespachos ON mdeid = solenvio  GROUP BY solplanilla";
}

$qry = $db->query($strQuery);
?>
<!doctype html>
<html>
<head>

    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>
    <link rel="stylesheet" type="text/css"
          href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css"/>
    <script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#tabla').dataTable({
                'bJQueryUI': true,
                'bStateSave': true,
                'sPaginationType': 'full_numbers',
                'oLanguage': {
                    'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
                },
            });
        });
    </script>
</head>
<body>
<?php require($r . 'incluir/src/login.php') ?>
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <?php require($r . 'incluir/src/menu.php') ?>
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a class="current">Bodega</a>
        </article>
        <article id="contenido">
            <h2>Planillas
                <?php if (isset($fecha1)) { ?>
                    <?php echo 'entre ' . $fecha1 . ' y ' . $fecha2 ?>
                <?php } ?>
            </h2>
            <div class="cuerpo centrado">
                <table id="tabla">
                    <thead>
                    <tr>
                        <th>No. PLanilla</th>
                        <th>Medio de envio</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
                        ?>
                        <tr>
                            <td align="center"><a
                                        href="lisplanilla.php?planilla=<?php echo $row['solplanilla'] ?>"><?php echo $row['solplanilla'] ?></a>
                            </td>
                            <td><?php echo $row['mdenombre'] ?></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
                <p class="boton">
                    <button type="button" class="btn btn-outline-primary btnatras"
                            onClick="carga(); location.href='conplafecha.php'">
                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-left-square-fill"
                             fill="currentColor"
                             xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd"
                                  d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm9.5 8.5a.5.5 0 0 0 0-1H5.707l2.147-2.146a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708-.708L5.707 8.5H11.5z"/>
                        </svg>
                        Atras
                    </button>
                </p>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php') ?>
</section>
<div id="modal" style="display:none"></div>
</body>
</html>