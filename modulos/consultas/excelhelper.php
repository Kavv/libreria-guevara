<?php
$r = '../../';

require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
require($r . 'incluir/phpexcel/Classes/PHPExcel.php');
require($r . 'incluir/funciones.php');

require $r . 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

if (isset($_POST['cdiid'])) {

    $cdiid = $_POST['cdiid'];


    $rowInfo = $db->query("select * from consultasdinamicas where cdiid = $cdiid")->fetch(PDO::FETCH_ASSOC);

    $nombreDelSp = $rowInfo['cdinombresp'];
    $nombreDeLaConsulta = $rowInfo['cdinombre'];
    $estaEnElXml = $rowInfo["estaenelxml"];

    if ($estaEnElXml == true) {

        $xml = simplexml_load_file("querys/querys.xml");

        $consulta = $xml->$nombreDelSp;

        $qryDatos = $db->prepare($consulta);

        foreach ($_POST as $key => $value) {
            if ($key != "cdiid" && $key != "consultar") {
                $qryDatos->bindValue(":$key", $value);
            }
        }
        //echo $consulta;exit();
        $qryDatos->execute();

        $cantidadDeFilas = $qryDatos->rowCount();

        $columnaDeExcelMaxima = obtenerNombreDeColumnaExcel($cantidadDeFilas);
    } else {
        $manual = $rowInfo["cdimanual"];
        if($manual == "")
        {
            $consultaInicio = "select * from $nombreDelSp (";

            $consultaMedio = "";

            foreach ($_POST as $key => $value) {
                if ($key != "cdiid" && $key != "consultar") {
                    $consultaMedio = $consultaMedio . ($consultaMedio == "" ? "" : ", ") . "$key := '$value'";

                }
            }

            $consultaFin = $consultaInicio . $consultaMedio . ")";
        }
        else
        {
            switch ($manual) {
                // 1 = Consulta manual para productos de manera dinamica (permite campos "vacios")
                case 1:
                    $p_codigo = $_POST['Codigo'];
                    $p_categoria = $_POST['Categoria'];
                    $p_tipo = $_POST['TipoProducto'];
                    $p_activo = $_POST['Activo'];
                    $valid_status = false;
                    if($p_activo >= 0 && $p_activo !="")
                        $valid_status = true;
                    $editorial = strtoupper(trim($_POST['Editorial']));
                    $con = "SELECT 
                    
                    proid as Código,
                    pronombre as Nombre,
                    prodescr as Descripción,
                    editorial as Editorial,
                    procantidad as Cantidad,
                    procosto as Costo,
                    proprecio as Precio,
                    protipo as Tipo,
                    prodesactivado as Activo,
                    categorias.nombre as Categoria
                    FROM productos
                    LEFT JOIN categorias ON productos.categoria = categorias.id";
                    $ord = 'ORDER BY pronombre ASC';
                    $consultaFin = crearConsulta($con, $ord,
                        array($p_codigo, "proid = '$p_codigo'"),
                        array($p_categoria, "categoria = '$p_categoria'  "),
                        array($editorial, "editorial = '$editorial'"),
                        array($p_tipo, "protipo = '$p_tipo'"),
                        array($valid_status, "prodesactivado = '$p_activo'"),
                    );
                    break;

            }
        }
        
        $qryDatos = $db->query($consultaFin);

        $cantidadDeFilas = $qryDatos->rowCount();

        $columnaDeExcelMaxima = obtenerNombreDeColumnaExcel($cantidadDeFilas);

    }
    $objPHPExcel = new Spreadsheet();

    $styleHeader = [
        'font' => [
            'bold' => true,
        ],
        'alignment' => [
            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            'wrapText' => true, 
        ],
        'borders' => [
            'allBorders' => [
                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                'color' => ['argb' => '828282']
            ],
        ],
        'fill' => [
            'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
            'startColor' => [
                'argb' => 'A9A9A9',
            ]
        ],
    ];


    $styleCell = [
        'alignment' => [
            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
            'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            'wrapText' => true, 
        ],
        'borders' => [
            'allBorders' => [
                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                'color' => ['argb' => '828282']
            ],
        ],
    ];


    $current = $objPHPExcel->getActiveSheet();

    $bandera = 1;
    while ($preResult = $qryDatos->fetch(PDO::FETCH_ASSOC)) {


        $banderaInterna = 0;

        foreach ($preResult as $key => $value) {

            $coordenada = obtenerNombreDeColumnaExcel($banderaInterna);

            if ($bandera == 1) {


                $current->getStyle(($coordenada . $bandera))->applyFromArray($styleHeader);
                $current->setCellValue(($coordenada . $bandera), $key);
                $current->getStyle(($coordenada . ($bandera + 1)))->applyFromArray($styleCell);


                $is_date = stripos($key, 'Fecha');
                $is_date2 = stripos($key, 'Procesado');
                $aux = mb_convert_encoding($value,"UTF-8","auto");

                if($is_date !== false || $is_date2 !== false)
                {
                    $current->getStyle($coordenada . ($bandera + 1))
                        ->getNumberFormat()
                        ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_YYYYMMDDSLASH);

                    $aux = \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($aux);
                }
                $current->setCellValue(($coordenada . ($bandera + 1)), ($aux));

                $objPHPExcel->getActiveSheet()->getColumnDimension($coordenada)->setWidth(strlen($key) * 2);
                // $objPHPExcel->getActiveSheet()->getColumnDimension($coordenada)->setAutoSize(true);

            } else {

                $is_date = stripos($key, 'Fecha');
                $is_date2 = stripos($key, 'Procesado');
                $aux = mb_convert_encoding($value,"UTF-8","auto");

                if($is_date !== false || $is_date2 !== false)
                {
                    $current->getStyle($coordenada . $bandera)
                        ->getNumberFormat()
                        ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_YYYYMMDDSLASH);

                    $aux = \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($aux);
                }
                $current->getStyle(($coordenada . $bandera))->applyFromArray($styleCell);
                $current->setCellValue(($coordenada . $bandera), ($aux));
            }

            $banderaInterna++;

        }

        if ($bandera == 1) {
            $bandera = $bandera + 2;
        } else {
            $bandera = $bandera + 1;
        }

    }

    $objPHPExcel->getActiveSheet()->setAutoFilter(
        $objPHPExcel->getActiveSheet()
            ->calculateWorksheetDimension()
    );

// Redirect output to a client’s web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=utf-8');
    header('Content-Disposition: attachment;filename="' . $nombreDeLaConsulta . '.xlsx"');
    header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
    header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header('Pragma: public'); // HTTP/1.0

    $objWriter = IOFactory::createWriter($objPHPExcel, 'Xlsx');
    $objWriter->save('php://output');

}