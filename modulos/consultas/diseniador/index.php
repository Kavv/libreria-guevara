<?php

$r = '../../../';

require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
?>

<!doctype html>
<html>
<head>

    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>

</head>
<body>
<?php require($r . 'incluir/src/login.php') ?>
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <?php require($r . 'incluir/src/menu.php') ?>
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a href="#">Bodega</a>
            <div class="mapa_div"></div>
            <a href="#">Documentos</a>
            <div class="mapa_div"></div>
            <a class="current">Entrada</a>
        </article>
        <article id="contenido">
        </article>
    </article>
</section>
</body>
</html>