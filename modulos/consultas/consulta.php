<?php

$r = '../../';

require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

$cdiid = $_GET['id'];

$nombre = $db->query("select cdinombre from consultasdinamicas where cdiid = $cdiid")->fetchColumn();


$consultaSql = "select parametrodeconsultadinamica.* " .
    "from consultasdinamicas " .
    "         inner join parametrodeconsultadinamica " .
    "                    on consultasdinamicas.cdiid = parametrodeconsultadinamica.pcdidconsultadinamica " .
    "where consultasdinamicas.cdiid = $cdiid; ";

$qryParametros = $db->query($consultaSql);


?>

<!doctype html>
<html>
<head>

    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>
    <script type="text/javascript"
            src="<?php echo $r ?>incluir/jquery/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>

    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css"/>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

    <script type="text/javascript">

        $(document).ready(function () {

            // Reescribiendo carga, para no mostrar el modal de carga

            carga = function () {
            }

            $('.fecha').each(function (i, obj) {
                $(obj).datepicker({
                    dateFormat: 'yy-mm-dd',
                    changeMonth: true,
                    changeYear: true
                })
            });

            $('.selectpicker').each(function (i, obj) {
                $(obj).selectpicker();
            });

        });
    </script>
</head>
<body>
<?php require($r . 'incluir/src/login.php') ?>
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <?php require($r . 'incluir/src/menu.php') ?>
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a href="#">Consultas</a>
            <div class="mapa_div"></div>
            <a href="#"><?php echo $nombre ?></a>
        </article>
        <article id="contenido">
            <form id="form" name="form" action="excelhelper.php" method="post">
                <fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
                    <legend class="ui-widget ui-widget-header ui-corner-all">Parametros</legend>
                    <p>
                        <input type="hidden" value="<?php echo $cdiid ?>" name="cdiid">
                    </p>


                    <?php

                    $consultaParametrosTipoCatalogo = "";
                    while ($row = $qryParametros->fetch(PDO::FETCH_ASSOC)) {

                        switch ($row['pcdtipodeinput']) {
                            case "inpEmpresa":

                                $consultaParametrosTipoCatalogo = "select empid,empnombre from empresas;";

                                $qryParametrosTipoCatalogo = $db->query($consultaParametrosTipoCatalogo);

                                ?>
                                <label for="<?php echo $row['pcdnombreparametro'] ?>"><?php echo $row['pcddescripcion'] ?></label>
                                <select id="<?php echo $row['pcdnombreparametro'] ?>"
                                        name="<?php echo $row['pcdnombreparametro'] ?>"
                                        class="<?php echo $row['pcdclases'] ?> selectpicker"
                                        data-live-search="true">

                                    <?php
                                    while ($rowIn = $qryParametrosTipoCatalogo->fetch(PDO::FETCH_ASSOC)) {
                                        ?>
                                        <option value="<?php echo $rowIn['empid'] ?>"><?php echo $rowIn['empnombre'] ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <?php
                                break;
                            case "inpUsuario":

                                $consultaParametrosTipoCatalogo = "select usuid,usunombre from usuarios;";

                                $qryParametrosTipoCatalogo = $db->query($consultaParametrosTipoCatalogo);

                                ?>
                                <label for="<?php echo $row['pcdnombreparametro'] ?>"><?php echo $row['pcddescripcion'] ?></label>
                                <select id="<?php echo $row['pcdnombreparametro'] ?>"
                                        name="<?php echo $row['pcdnombreparametro'] ?>"
                                        class="<?php echo $row['pcdclases'] ?> selectpicker"
                                        data-live-search="true">

                                    <?php
                                    while ($rowIn = $qryParametrosTipoCatalogo->fetch(PDO::FETCH_ASSOC)) {
                                        ?>
                                        <option value="<?php echo $rowIn['usuid'] ?>"><?php echo $rowIn['usuid'] . ' / ' . $rowIn['usunombre'] ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <?php
                                break;
                                
                            case "inpAsesor":

                                $consultaParametrosTipoCatalogo = "select usuId,usuNombre from usuarios where usuasesor = 1;";

                                $qryParametrosTipoCatalogo = $db->query($consultaParametrosTipoCatalogo);

                                if(isset($row['piddescripcion']))
                                {
                                    $label = $row['piddescripcion'];
                                    $nombre_param = $row['pidnombreparametro'];
                                    $clase = $row['pidclases'];
                                }
                                if(isset($row['pcddescripcion']))
                                {
                                    $label = $row['pcddescripcion'];
                                    $nombre_param = $row['pcdnombreparametro'];
                                    $clase = $row['pcdclases'];
                                }



                                ?>
                                <label for="<?php echo $nombre_param ?>"><?php echo $label ?></label>
                                <select id="<?php echo $nombre_param ?>"
                                        name="<?php echo $nombre_param ?>"
                                        class="<?php echo $clase ?> selectpicker"
                                        data-live-search="true">

                                    <?php
                                    while ($rowIn = $qryParametrosTipoCatalogo->fetch(PDO::FETCH_ASSOC)) {
                                        ?>
                                        <option value="<?php echo $rowIn['usuid'] ?>"><?php echo $rowIn['usuid'] . ' / ' . $rowIn['usunombre'] ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <?php
                                break;
                            case "inpCobrador":

                                $consultaParametrosTipoCatalogo = "select usuId,usuNombre from usuarios where usurelacionista = 1;";

                                $qryParametrosTipoCatalogo = $db->query($consultaParametrosTipoCatalogo);

                                ?>
                                <label for="<?php echo $row['pcdnombreparametro'] ?>"><?php echo $row['pcddescripcion'] ?></label>
                                <select id="<?php echo $row['pcdnombreparametro'] ?>"
                                        name="<?php echo $row['pcdnombreparametro'] ?>"
                                        class="<?php echo $row['pcdclases'] ?> selectpicker"
                                        data-live-search="true">

                                    <?php
                                    while ($rowIn = $qryParametrosTipoCatalogo->fetch(PDO::FETCH_ASSOC)) {
                                        ?>
                                        <option value="<?php echo $rowIn['usuid'] ?>"><?php echo $rowIn['usuid'] . ' / ' . $rowIn['usunombre'] ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <?php
                                break;
                            case "inpDepartamento":

                                $consultaParametrosTipoCatalogo = "select  * from departamentos order by depnombre;";

                                $qryParametrosTipoCatalogo = $db->query($consultaParametrosTipoCatalogo);

                                ?>
                                <label for="<?php echo $row['pcdnombreparametro'] ?>"><?php echo $row['pcddescripcion'] ?></label>
                                <select id="<?php echo $row['pcdnombreparametro'] ?>"
                                        name="<?php echo $row['pcdnombreparametro'] ?>"
                                        class="<?php echo $row['pcdclases'] ?> selectpicker"
                                        data-live-search="true">

                                    <?php
                                    while ($rowIn = $qryParametrosTipoCatalogo->fetch(PDO::FETCH_ASSOC)) {
                                        ?>
                                        <option value="<?php echo $rowIn['depid'] ?>"><?php echo $rowIn['depnombre'] ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <?php
                                break;
                            case "inpGrupo":

                                $consultaParametrosTipoCatalogo = "select  * from grupos;";

                                $qryParametrosTipoCatalogo = $db->query($consultaParametrosTipoCatalogo);

                                ?>
                                <label for="<?php echo $row['pcdnombreparametro'] ?>"><?php echo $row['pcddescripcion'] ?></label>
                                <select id="<?php echo $row['pcdnombreparametro'] ?>"
                                        name="<?php echo $row['pcdnombreparametro'] ?>"
                                        class="<?php echo $row['pcdclases'] ?> selectpicker"
                                        data-live-search="true">

                                    <?php
                                    while ($rowIn = $qryParametrosTipoCatalogo->fetch(PDO::FETCH_ASSOC)) {
                                        ?>
                                        <option value="<?php echo $rowIn['gruid'] ?>"><?php echo $rowIn['grunombre'] ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <?php
                                break;
                            case "inpCiudad":

                                $consultaParametrosTipoCatalogo = "select  * from ciudades;";

                                $qryParametrosTipoCatalogo = $db->query($consultaParametrosTipoCatalogo);

                                ?>
                                <label for="<?php echo $row['pcdnombreparametro'] ?>"><?php echo $row['pcddescripcion'] ?></label>
                                <select id="<?php echo $row['pcdnombreparametro'] ?>"
                                        name="<?php echo $row['pcdnombreparametro'] ?>"
                                        class="<?php echo $row['pcdclases'] ?> selectpicker"
                                        data-live-search="true">

                                    <?php
                                    while ($rowIn = $qryParametrosTipoCatalogo->fetch(PDO::FETCH_ASSOC)) {
                                        ?>
                                        <option value="<?php echo($rowIn['ciudepto'] . '-' . $rowIn['ciuid']) ?>"><?php echo $rowIn['ciunombre'] ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <?php
                                break;
                            case "inpCategoriaProducto":
                                
                                ?>
                                <label for="<?php echo $row['pcdnombreparametro'] ?>"><?php echo $row['pcddescripcion'] ?></label>
                                <select id="<?php echo $row['pcdnombreparametro'] ?>"
                                    name="<?php echo $row['pcdnombreparametro'] ?>"
                                    class="<?php echo $row['pcdclases'] ?> selectpicker"
                                    data-live-search="true">
									<option value="">TODAS</option>
									<option value="-1">SIN CATEGORIAS</option>

                                    <?php 
                                    $categorias = $db->query("SELECT * FROM categorias WHERE id > 0 ORDER BY nombre asc");
                                    while ($categoria = $categorias->fetch(PDO::FETCH_ASSOC)) {
                                        echo '<option value="'.$categoria['id'].'">'.$categoria['nombre'].'</option>';
                                    } ?>
                                </select>
                                <?php
                                break;
                                break;
                            case "inpTipoProducto":
                                
                                ?>
                                <label for="<?php echo $row['pcdnombreparametro'] ?>"><?php echo $row['pcddescripcion'] ?></label>
                                <select id="<?php echo $row['pcdnombreparametro'] ?>"
                                    name="<?php echo $row['pcdnombreparametro'] ?>"
                                    class="<?php echo $row['pcdclases'] ?>">

									<option value="">TODOS</option>
									<option value="PT">PRODUCTO TERMINADO</option>
									<option value="MP">MATERIA PRIMA</option>
								</select>
                                <?php
                                break;
                            case "inpActivoProducto":
                                
                                ?>
                                <label for="<?php echo $row['pcdnombreparametro'] ?>"><?php echo $row['pcddescripcion'] ?></label>
                                <select id="<?php echo $row['pcdnombreparametro'] ?>"
                                    name="<?php echo $row['pcdnombreparametro'] ?>"
                                    class="<?php echo $row['pcdclases'] ?>">

									<option value="">TODOS</option>
									<option value="0">Sí</option>
									<option value="1">No</option>
								</select>
                                <?php
                                break;

                            default:
                                ?>
                                <p>
                                    <label for="<?php echo $row['pcdnombreparametro'] ?>"><?php echo $row['pcddescripcion'] ?></label>
                                    <input id="<?php echo $row['pcdnombreparametro'] ?>"
                                           name="<?php echo $row['pcdnombreparametro'] ?>"
                                           type="<?php echo $row['pcdtipodeinput'] ?>"
                                           class="<?php echo $row['pcdclases'] ?>"/>
                                </p>
                                <?php break;
                        }
                    } ?>

                    <button type="submit" class="btnconsulta btn btn-primary btn-block mt-2" name="consultar"
                            value="Buscar">consultar
                    </button>
                </fieldset>

            </form>
        </article>
    </article>
</section>
</body>
</html>