<?php
$r = '../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
	<?php 
		require($r . 'incluir/src/head.php');
		require($r . 'incluir/src/head-form.php');
	?>	

<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#tabla').dataTable({
		'bJQueryUI': true,
		'bStateSave': true,
		'sPaginationType': 'full_numbers',
		'oLanguage': {
			'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
		},
		'bSort': false,
	});
	$('.btnatras').button({ icons: { primary: 'ui-icon ui-icon-arrowthick-1-w' }});
});
</script>
</head>
<body>
<?php require($r.'incluir/src/login.php') ?>
<section id="principal">
<?php require($r.'incluir/src/cabeza.php') ?>
<?php require($r.'incluir/src/menu.php') ?>
<article id="cuerpo">
<article class="mapa">
<a href="#">Principal</a><div class="mapa_div"></div><a href="#">Cartera</a><div class="mapa_div"></div><a class="current">Resultados Consulta Masiva</a>
</article>
<article id="contenido">
<?php 
$uploaddir = 'archivos_csv/';
$uploadfile = $uploaddir . "consulta_masiva.csv";


if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
    echo " <h2>Consulta Masiva, El archivo fue cargado exitosamente. </h2>";
} else {
    echo " <h2>Consulta Masiva, ¡Hubo un error por favor intenta nuevamente! </h2>";
}

$file = file('archivos_csv/consulta_masiva.csv');

?>
<div class="reporte">
<a href="#"><img src="<?php echo $r ?>imagenes/iconos/pdf.png" title="PDF" /></a><a href="#"><img src="<?php echo $r ?>imagenes/iconos/excel.png" title="CSV" /></a>
</div>
<table id="tabla">
<thead>
<tr>
<th>cuenta</th>
<th>Solicitud</th>
<th>Cedula Cliente</th>
<th>Cliente</th>
<th>Promotor</th>
</tr>
</thead>
<tbody>
<?php

foreach ($file as $num => $fila) 
{
$sql = "SELECT carfactura,solid,cliid,clinom2,clinombre,cliape1,cliape2,usuid,usunombre FROM (((carteras 
INNER JOIN empresas ON carempresa = empid) 
INNER JOIN clientes ON carcliente = cliid) 
INNER JOIN solicitudes ON (solempresa = carempresa AND solfactura = carfactura))
LEFT JOIN usuarios ON usuid = carpromotor WHERE cliid = ".$fila."  order by carfactura";

$qry = $db->query($sql);
$num = $qry->rowCount();

if ($num == 1) {

while($row = $qry->fetch(PDO::FETCH_ASSOC)){
	echo "<tr>";
	echo "<td align='center'>". $row['carfactura'] ."</td>";
	echo "<td align='center'>". $row['solid'] ."</td>";
	echo "<td align='center'>". $row['cliid'] ."</td>";
	echo "<td>". $row['clinombre'].' '.$row['clinom2'].' '.$row['cliape1'].' '.$row['cliape2'] ."</td>";
	echo "<td title=' ". $row['usuid']." '>". $row['usunombre'] ."</td>";
	echo "</tr>";
} // Fin While

} else {

	echo "<tr>";
	echo "<td align='center'>NONE</td>";
	echo "<td align='center'>NONE</td>";
	echo "<td style='color:red' align='center'>". $fila ."</td>";
	echo "<td> NO SE ENCONTRARON RESULTADOS CON ESTA IDENTIFICACION</td>";
	echo "<td> NONE </td>";
	echo "</tr>";

}



} // Fin Foreach
?>
</tbody>
</table>
<p class="boton">
<button type="button" class="btnatras" onClick=" carga(); location.href = 'consulta_masiva.php'">atras</button>
</p>
</article>
</article>
<?php require($r.'incluir/src/pie.php') ?>
</section>
</body>
</html>