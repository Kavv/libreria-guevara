<?php
unlink('../../Modulos/Cartera/archivos_csv/DeterioDetalladoCartera.csv');

$r = '../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
require($r.'incluir/phpexcel/Classes/PHPExcel.php');

$empresa = $_POST['empresa'];
$fecha1 = $_POST['fecha1'];
$fecha2 = $_POST['fecha2'];

$stmt = $db->prepare("CALL obtenerDeterioroCartera(:empresa,:fecha1,:fecha2)");
$stmt->bindParam(':empresa', $empresa, PDO::PARAM_STR, 50);    
$stmt->bindParam(':fecha1', $fecha1, PDO::PARAM_STR, 10);
$stmt->bindParam(':fecha2', $fecha2, PDO::PARAM_STR, 10);

$stmt->execute();

$results = array();
do 
{
    $results[] = $stmt->fetchAll();
} 
while ($stmt->nextRowset());

$datosConsulta = $results[0];

$nombreEmp = '';

if ($empresa != '0')
{
    $qry = $db->query("SELECT * FROM empresas WHERE empid = $empresa LIMIT 1");
    $col = $qry->fetch(PDO::FETCH_ASSOC);
	$nombreEmp = 'LA EMPRESA : '.$col['empnombre'];
}
else
{
	$nombreEmp = 'TODAS LAS EMPRESAS';
}

$file = fopen("../../Modulos/Cartera/archivos_csv/DeterioDetalladoCartera.csv","w");

$linea0 = 'REPORTE DE DETERIORO DE  '. strtoupper($nombreEmp) .'  DESDE  ' . $fecha1. '  HASTA '.$fecha2 ;
fwrite($file,$linea0);
fputcsv($file,explode(',',''));

$linea1 = "DOCUMENTO;NOMBRE;APELLIDO;FACTURA;N CUOTA;VALOR CUOTA;FECHA CUOTA;FECHA PAGO;FECHA CORTE;DIAS MORA;categoria;PORCENTAJE ABSOLUTO;PORCENTAJE ACUMULADO;DETERIORO ABSOLUTO;DETERIORO ACUMULADO;ULTIMA CONSULTA";
fwrite($file,$linea1);
fputcsv($file,explode(',',''));

$fecha = date("Y-m-d H:i:s"); 

foreach ($results[0] as $row)
{
    $idDet = $row[16];
    $idFact = $row[3];
    $idCout = $row[4];
    $consulta = $db->query("UPDATE deteriorocartera SET FechaConsulta = '$fecha' WHERE IdDeterioro = $idDet;");

    $linea = $row[0].';'.$row[1].';'.$row[2].';'.$row[3].';'.$row[4].';'.$row[5].';'.$row[6].';'.$row[7].';'.$row[8].';'.$row[9].';'.$row[10].';'.$row[11].';'.$row[12].';'.$row[13].';'.$row[14].';'.$row[15].';'.$fecha;
    fwrite($file,$linea);
    fputcsv($file,explode(',',''));
}

fclose($file); 

return true;

?>