<?php
$r = '../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
require($r.'incluir/phpexcel/Classes/PHPExcel.php');

$fecha1 = $_GET['fecha1'];
$fecha2 = $_GET['fecha2'];
$empresa = $_GET['empresa'];

if ($empresa == 'TODAS'){$empresa = '';}

$con = ' SELECT * FROM carteras  INNER JOIN detcarteras ON dcaempresa = carempresa AND carfactura = dcafactura ';
$ord = ' ORDER BY carfecha ASC ';
$title_fecha = "";
$title_emp = "";
    /* Los parametros de la consulta sql se genera dinamicamente 
    en base a los datos recibidos para delimitar los resultados */
    $parameters = [];
	if($empresa != "")
        array_push($parameters, "carempresa LIKE '%$empresa%'" );

	if($fecha1 != "")
	{
        array_push($parameters, "carfecha BETWEEN '$fecha1' AND '$fecha2'" );
		$title_fecha = $fecha1.' - '.$fecha2;
	}

    // Consulta base
    $sql = $con;
    foreach ($parameters as $index => $parameter) {
        // Se agregan los parametros del WHERE
        if($index == 0)
            $sql .= " WHERE " . $parameter;
        else
            $sql .= " AND " . $parameter;
    }
    // Se completa la consulta
    $sql .= $ord;

$qry2 = $db->query($sql);
$num2 = $qry2->rowCount();
	
$titulo = 'REPORTE CARTERAS - CUOTAS'.$title_fecha;

$objPHPExcel = new PHPExcel();
$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN
		)
	)
);
$objPHPExcel->getActiveSheet()->getStyle('A1:Q2')->applyFromArray($styleArray);
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:Q1')
			->setCellValue('A1', $titulo);
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A2', 'EMPRESA')
			->setCellValue('B2', 'FACTURA')	
            ->setCellValue('C2', 'CLIENTE')
            ->setCellValue('D2', 'VALOR TOTAL')
            ->setCellValue('E2', 'VALOR CUOTA')
			->setCellValue('F2', 'VALOR SALDO')
			->setCellValue('G2', 'NUMERO DE CUOTAS')
			->setCellValue('H2', 'FECHA DE INGRESO A CARTERA')
			->setCellValue('I2', 'ESTADO DE OBLIGACION')
			->setCellValue('J2', 'CUOTA NUMERO')
			->setCellValue('K2', 'FECHA A PAGAR')
			->setCellValue('L2', 'FECHA DE PAGO')
			->setCellValue('M2', 'MES')
			->setCellValue('N2', 'VALOR PAGADO')
			->setCellValue('O2', 'VALOR DESCUENTO')
			->setCellValue('P2', 'VALOR SALDO')
			->setCellValue('Q2', 'ASESOR')
			;	
$i = 3;
while($row2 = $qry2->fetch(PDO::FETCH_ASSOC)){
	
	$qrycliente= $db->query("SELECT * FROM clientes WHERE cliid = '".$row2['carcliente']."';");
	$rowcliente = $qrycliente->fetch(PDO::FETCH_ASSOC);
	$numcliente = $qrycliente->rowCount();
	if ($numcliente == 1){
		$cliente = $rowcliente['clinombre'].' '.$rowcliente['clinom2'].' '.$rowcliente['cliape1'].' '.$rowcliente['cliape2'];
	} else {
		$cliente = "NONE";	
	}
	
	
	$qryca = $db->query("SELECT usunombre FROM solicitudes INNER JOIN usuarios ON usuid = solasesor WHERE solfactura = '".$row2['carfactura']."' AND solempresa = '".$row2['carempresa']."'");
	$rowca = $qryca->fetch(PDO::FETCH_ASSOC);
	$capacitador = "SIN ASIGNAR";
	if($rowca)
		$capacitador = $rowca['usunombre'];
	
	$qryempresa = $db->query("SELECT * FROM empresas WHERE empid = '".$row2['carempresa']."';");
	$rowempresa = $qryempresa->fetch(PDO::FETCH_ASSOC);
	$empresaname = $rowempresa['empnombre'];
	
	$mespago = substr($row2['dcafepag'], 5, 2);
	
	
	if ($mespago == 1){$mes = 'ENERO';}
	elseif ($mespago == 2){$mes = 'FEBRERO';}
	elseif ($mespago == 3){$mes = 'MARZO';}
	elseif ($mespago == 4){$mes = 'ABRIL';}
	elseif ($mespago == 5){$mes = 'MAYO';}
	elseif ($mespago == 6){$mes = 'JUNIO';}
	elseif ($mespago == 7){$mes = 'JULIO';}
	elseif ($mespago == 8){$mes = 'AGOSTO';}
	elseif ($mespago == 9){$mes = 'SEPTIEMBRE';}
	elseif ($mespago == 10){$mes = 'OCTUBRE';}
	elseif ($mespago == 11){$mes = 'NOVIEMBRE';}
	elseif ($mespago == 12){$mes = 'DICIEMBRE';}
	elseif ($mespago == ''){$mes = '';}
	
		
	$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':Q'.$i)->applyFromArray($styleArray);
	$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('A'.$i, $empresaname)
    	->setCellValue('B'.$i, $row2['carfactura'])
		->setCellValue('C'.$i, $cliente)
    	->setCellValue('D'.$i, $row2['cartotal'])
    	->setCellValue('E'.$i, $row2['carcuota'])
		->setCellValue('F'.$i, $row2['carsaldo'])
		->setCellValue('G'.$i, $row2['carncuota'])
		->setCellValue('H'.$i, $row2['carfecha'])
		->setCellValue('I'.$i, $row2['dcaestado'])
		->setCellValue('J'.$i, $row2['dcacuota'])
		->setCellValue('K'.$i, $row2['dcafecha'])
		->setCellValue('L'.$i, $row2['dcafepag'])
		->setCellValue('M'.$i, $mes)
		->setCellValue('N'.$i, $row2['dcavalor'])
		->setCellValue('O'.$i, $row2['dcadescuento'])
		->setCellValue('P'.$i, $row2['dcasaldo'])
		->setCellValue('Q'.$i, $capacitador);
	$i++;

}


// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="REPORTE CARTERAS - CUOTAS '.$title_fecha.'.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
?>