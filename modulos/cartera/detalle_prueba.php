<?php
$r = '../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
require($r.'incluir/funciones.php');
$empresa = $_GET['id1'];
$numero = $_GET['id2'];
$filtro = 'empresa='.$_GET['empresa'].'&prefijo='.$_GET['prefijo'].'&numero='.$_GET['numero'].'&recibo='.$_GET['recibo'].'&cliente='.$_GET['cliente'].'&fecha1='.$_GET['fecha1'].'&fecha2='.$_GET['fecha2'];
$row = $db->query("SELECT * FROM (((((carteras INNER JOIN empresas ON carempresa = empid) INNER JOIN clientes ON carcliente = cliid) INNER JOIN solicitudes ON (carempresa = solempresa AND carfactura = solfactura)) INNER JOIN departamentos ON depid = soldepcobro) INNER JOIN ciudades ON (soldepcobro = ciudepto AND solciucobro = ciuid)) LEFT JOIN usuarios ON carpromotor = usuid WHERE carempresa = $empresa AND carfactura = '$numero'")->fetch(PDO::FETCH_ASSOC);
$row2 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '$empresa' AND dcafactura = '$numero' AND dcaestado = 'ACTIVA' ORDER BY dcacuota ASC")->fetch(PDO::FETCH_ASSOC);
if(date('Y-m-d') >= $row2['dcafecha'])
	$mora = fechaDif($row2['dcafecha'],date('Y-m-d'));
else $mora = 0;
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
	<?php 
		require($r . 'incluir/src/head.php');
		require($r . 'incluir/src/head-form.php');
	?>	

<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#tabla').dataTable({
		'bPaginate': false,
        'bLengthChange': false,
        'bFilter': false,
		'bSort': false,
        'bInfo': false,
        'bAutoWidth': false,
		'bStateSave': true,
		'bJQueryUI': true
	});
	$('.btnatras').button({ icons: { primary: 'ui-icon ui-icon-arrowthick-1-w' }});
	$('#pdf').click(function(){
		newSrc = $(this).attr('data-rel');
		$('#modal').html("<iframe src='" + newSrc + "' width='100%' height='100%'></iframe>");
		$('#modal').dialog({ modal: true, width: '600', height: '800', title : 'PDF del detalle' });
    });
});
</script>
<style type="text/css">
#columna span { display:inline-block; width:120px; text-align:left; float:left }
</style>
</head>
<body>
<?php require($r.'incluir/src/login.php') ?>
<section id="principal">
<?php require($r.'incluir/src/cabeza.php') ?>
<?php require($r.'incluir/src/menu.php') ?>
<article id="cuerpo">
<article class="mapa">
<a href="#">Principal</a><div class="mapa_div"></div><a class="current">Cartera</a>
</article>
<article id="contenido">
<h2>Detalle de la cuenta</h2>
<div id="columna">
<p>
<span><strong>Empresa:</strong></span> <?php echo $row['empnombre'] ?><br />
<span><strong>Solicitud:</strong></span> <?php echo $row['solid'] ?><br />
<span><strong>Factura:</strong></span> <?php echo $row['carfactura'] ?><br />
<span><strong>Cliente:</strong></span> <?php echo $row['clinombre'].' '.$row['clinom2'].' '.$row['cliape1'].' '.$row['cliape2'] ?><br />
<span><strong>Email:</strong></span> <?php echo $row['cliemail'] ?><br />
<span><strong>Promotor:</strong></span> <?php echo $row['usunombre'] ?><br />
<span><strong>Ubicacion:</strong></span> <?php echo $row['depnombre'].' / '.$row['ciunombre'] ?>
</p>
<p>
<span><strong>Dirección:</strong></span> <?php echo $row['solbarcobro'].', '.$row['solcobro'] ?><br />
<span><strong>Telefonos:</strong></span> <?php echo $row['clicelular'].' / '.$row['clitelresidencia'].' / '.$row['clitelcomercio'] ?><br />
<span><strong>Cuota inicial:</strong></span>  <?php echo number_format($row['solcuota'],0,',','.') ?><br />
<span><strong>Total credito:</strong></span>  <?php echo number_format($row['cartotal'],0,',','.') ?><br />
<span><strong>Valor cuota:</strong></span>  <?php echo number_format($row['carcuota'],0,',','.') ?><br />
<span><strong>Valor pagado:</strong></span>  <?php echo number_format($row['cartotal'] - $row['carsaldo'],0,',','.') ?><br />
<span><strong>Saldo:</strong></span>  <?php echo number_format($row['carsaldo'],0,',','.') ?><br />
<span><strong>Dias en mora:</strong></span>  <?php echo number_format($mora,0,',','.') ?>
</p>
</div>
<div class="reporte">
<a href="movimientos.php?<?php echo 'id1='.$empresa.'&id2='.$numero.'&'.$filtro ?>" onClick="carga()"><img src="<?php echo $r ?>imagenes/iconos/application_side_list.png" title="Movimientos" /></a> <a href="historial.php?<?php echo 'id1='.$empresa.'&id2='.$numero.'&'.$filtro ?>" onClick="carga()"><img src="<?php echo $r ?>imagenes/iconos/application_edit.png" title="Historial" /></a> 
<img src="<?php echo $r ?>imagenes/iconos/pdf.png" id="pdf" data-rel="pdfdetalle.php?<?php echo 'id1='.$empresa.'&id2='.$numero ?>" title="Pdf del detalle" /> 
<a href="#"><img src="<?php echo $r ?>imagenes/iconos/excel.png" title="Excel" /></a>
</div>
<table id="tabla">
<thead>
<tr>
<th>N.Cuota</th>
<th>F. Obligacion</th>
<th>F. Pago</th>
<th>Cuota</th>
<th>Descuento</th>
<th>Saldo</th>
</tr>
</thead>
<tbody>
<?php
$qry = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = ".$row['carempresa']." AND dcafactura = '".$row['carfactura'] ."' ORDER BY dcacuota");
while($row = $qry->fetch(PDO::FETCH_ASSOC)){
	if($row['dcaestado'] == 'CANCELADO'){
		echo '<tr style="background-color:#e1dfdf"><td align="center">'.$row['dcacuota'].'</td><td align="center">'.$row['dcafecha'].'</td><td align="center">'.$row['dcafepag'].'</td><td align="right">'.number_format($row['dcavalor'],0,',','.').'</td><td align="right">'.number_format($row['dcadescuento'],0,',','.').'</td><td align="right">'.number_format($row['dcasaldo'],0,',','.').'</td></tr>';
	}elseif($row['dcafecha'] < date('Y-m-d')){
		echo '<tr style="background-color:#f27070"><td align="center">'.$row['dcacuota'].'</td><td align="center">'.$row['dcafecha'].'</td><td align="center">'.$row['dcafepag'].'</td><td align="right">'.number_format($row['dcavalor'],0,',','.').'</td><td align="right">'.number_format($row['dcadescuento'],0,',','.').'</td><td align="right">'.number_format($row['dcasaldo'],0,',','.').'</td></tr>';
	}else{
		echo '<tr><td align="center">'.$row['dcacuota'].'</td><td align="center">'.$row['dcafecha'].'</td><td align="center">'.$row['dcafepag'].'</td><td align="right">'.number_format($row['dcavalor'],0,',','.').'</td><td align="right">'.number_format($row['dcadescuento'],0,',','.').'</td><td align="right">'.number_format($row['dcasaldo'],0,',','.').'</td></tr>';
	}
}
?>
</tbody>
</table>
<p class="boton">
<button type="button" class="btnatras" onClick="carga(); location.href = 'consultar.php'">atras</button>
</p>
</article>
</article>
<?php require($r.'incluir/src/pie.php') ?>
</section>
<div id="modal" style="display:none"></div>
</body>
</html>