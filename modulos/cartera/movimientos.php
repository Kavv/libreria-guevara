<?php
$r = '../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
require($r . 'incluir/funciones.php');
$empresa = $_GET['id1'];
$numero = $_GET['id2'];
$filtro = 'empresa=' . $_GET['empresa'] . '&prefijo=' . $_GET['prefijo'] . '&numero=' . $_GET['numero'] . '&cliente=' . $_GET['cliente'] . '&fecha1=' . $_GET['fecha1'] . '&fecha2=' . $_GET['fecha2'];
$row = $db->query("SELECT * FROM (((((carteras INNER JOIN empresas ON carempresa = empid) INNER JOIN clientes ON carcliente = cliid) INNER JOIN solicitudes ON (carempresa = solempresa AND carfactura = solfactura)) INNER JOIN departamentos ON depid = soldepcobro) INNER JOIN ciudades ON (soldepcobro = ciudepto AND solciucobro = ciuid)) LEFT JOIN usuarios ON carpromotor = usuid WHERE carempresa = '$empresa' AND carfactura = '$numero'")->fetch(PDO::FETCH_ASSOC);
$row2 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '$empresa' AND dcafactura = '$numero' AND dcaestado = 'ACTIVA' ORDER BY dcacuota ASC")->fetch(PDO::FETCH_ASSOC);

$mora = 0;
if($row2)
{
	if ((date('Y-m-d') > $row2['dcafecha']) && $row['carsaldo'] != 0)
		$mora = fechaDif($row2['dcafecha'], date('Y-m-d'));
}
?>
<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>

	<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
	<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#tabla').dataTable({
				'bPaginate': false,
				'bLengthChange': false,
				'bFilter': false,
				'bInfo': false,
				'bSort': false,
				'bAutoWidth': false,
				'bJQueryUI': true,
			});

			$('.pdf').click(function() {
				newSrc = $(this).attr('data-rel');
				if (validation_file(newSrc) == 200)
					$('#modal').html("<iframe src='" + newSrc + "' width='100%' height='100%'></iframe>");
				else
					$('#modal').html("<spam>El PDF no fue encontrado</spam>");
				$('#modal').dialog({
					modal: true,
					width: '600',
					height: '800',
					title: 'PDF del documento'
				});
			});
		});
	</script>
	<style type="text/css">
		#detalle span {
			margin-right: 10px;
		}

		#detalle label {
			border-bottom: 1px solid #000;
		}
	</style>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a class="current">Cartera</a>
			</article>
			<article id="contenido">
				<h2>Movimiento de la cuenta</h2>
				<div id="detalle" class="row">
					<div class="col-md-6">
						<div class="row">
							<span><strong>Empresa:</strong></span>
							<label class="not-w"><?php echo $row['empnombre'] ?><br /></label>
						</div>
						<div class="row">
							<span><strong>Solicitud:</strong></span>
							<label class="not-w"><?php echo $row['solid'] ?><br /></label>
						</div>
						<div class="row">
							<span><strong>Factura:</strong></span>
							<label class="not-w"><?php echo $row['carfactura'] ?><br /></label>
						</div>
						<div class="row">
							<span><strong>Cliente:</strong></span>
							<label class="not-w"><?php echo $row['clinombre'] . ' ' . $row['clinom2'] . ' ' . $row['cliape1'] . ' ' . $row['cliape2'] ?><br /></label>
						</div>
						<div class="row">
							<span><strong>Numero Identi:</strong></span>
							<label class="not-w"><?php echo $row['cliid'] ?><br /></label>
						</div>
						<div class="row">
							<span><strong>Email:</strong></span>
							<label class="not-w"><?php echo $row['cliemail'] ?><br /></label>
						</div>
						<div class="row">
							<span><strong>Promotor:</strong></span>
							<label class="not-w"><?php echo $row['usunombre'] ?><br /></label>
						</div>
						<div class="row">
							<span><strong>Ubicacion:</strong></span>
							<label class="not-w"><?php echo $row['depnombre'] . ' / ' . $row['ciunombre'] ?><br /></label>
						</div>
						<div class="row">
							<span><strong>Dirección:</strong></span>
							<label class="not-w"><?php echo $row['solbarcobro'] . ', ' . $row['solcobro'] ?><br /></label>
						</div>
					</div>
					<div class="col-md-6">

						<div class="row">
							<span><strong>Contacto Directo:</strong></span>
							<label class="not-w"><?php echo $row['clicelular'] ?><br /></label>
						</div>
						<div class="row">
							<span><strong>Teléfono Comercio:</strong></span>
							<label class="not-w"><?php echo $row['clitelcomercio'] ?><br /></label>
						</div>
						<div class="row">
							<span><strong>Número Adicional:</strong></span>
							<label class="not-w"><?php echo $row['clitelresidencia'] ?><br /></label>
						</div>
						<div class="row">
							<span><strong>Cuota inicial:</strong></span>
							<label class="not-w"><?php echo number_format($row['solcuota'], 2) ?><br /></label>
						</div>
						<div class="row">
							<span><strong>Total credito:</strong></span>
							<label class="not-w"><?php echo number_format($row['cartotal'], 2) ?><br /></label>
						</div>
						<div class="row">
							<span><strong>Valor cuota:</strong></span>
							<label class="not-w"><?php echo number_format($row['carcuota'], 2) ?><br /></label>
						</div>
						<div class="row">
							<span><strong>Valor pagado:</strong></span>
							<label class="not-w"><?php echo number_format($row['cartotal'] - $row['carsaldo'], 2) ?><br /></label>
						</div>
						<div class="row">
							<span><strong>Saldo:</strong></span>
							<label class="not-w"><?php echo number_format($row['carsaldo'], 2) ?><br /></label>
						</div>
						<div class="row">
							<span><strong>Dias en mora:</strong></span>
							<label class="not-w"><?php echo $mora; ?></label>
						</div>
					</div>
				</div>
				<div class="reporte">
					<a href="detalle.php?id1=<?php echo $empresa . '&id2=' . $numero . '&' . $filtro ?>" onClick="carga()"><img src="<?php echo $r ?>imagenes/iconos/briefcase.png" title="Cartera" /></a>
					<a href="historial.php?<?php echo 'id1=' . $empresa . '&id2=' . $numero . '&' . $filtro ?>" onClick="carga()"><img src="<?php echo $r ?>imagenes/iconos/application_edit.png" title="Historial" /></a>
				</div>
				<table id="tabla">
					<thead>
						<tr>
							<th>Fecha</th>
							<th>Factura</th>
							<th>Valor</th>
							<th>Financiacion</th>
							<th>Descuento</th>
							<th>Saldo</th>
							<th>Forma de pago</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php
						$qry = $db->query("SELECT movempresa, movfecha, movprefijo ,movnumero, movvalor, movfinan, movdescuento, movsaldo, movfpago, fpanombre  FROM movimientos INNER JOIN solicitudes ON (solfactura = movdocumento AND solempresa=movempresa) LEFT JOIN fpagos ON fpaid = solfpago WHERE movempresa = '" . $row['carempresa'] . "' AND movdocumento = '" . $row['carfactura'] . "' ORDER BY movimientos.created_at ASC");
						while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
							$row2 = $db->query("SELECT fpanombre FROM fpagos WHERE fpaid = '" . $row['movfpago'] . "'")->fetch(PDO::FETCH_ASSOC);
						?>
							<tr>
								<td align="center"><?php echo $row['movfecha'] ?></td>
								<td align="center"><?php echo $row['movprefijo'] . '-' . $row['movnumero'] ?></td>
								<td align="right"><?php echo number_format($row['movvalor'], 2) ?></td>
								<td align="right"><?php echo number_format($row['movfinan'], 2) ?></td>
								<td align="right"><?php echo number_format($row['movdescuento'], 2) ?></td>
								<td align="right"><?php echo number_format($row['movsaldo'], 2) ?></td>
								<?php
								if (!$row2)
									echo '<td>' . $row['fpanombre'] . '</td>';
								else
									echo '<td>' . $row2['fpanombre'] . '</td>';
								?>
								<td align="center">
								<?php
								if ($row['movprefijo'] == 'FV')
									echo '<img src="' . $r . 'imagenes/iconos/pdf.png" class="pdf" data-rel="' . $r . 'pdf/facturas/' . $row['movempresa'] . '/' . $row['movnumero'] . '.pdf" />';
								elseif ($row['movprefijo'] == 'RC')
									echo '<img src="' . $r . 'imagenes/iconos/pdf.png" class="pdf" data-rel="pdfrc.php?id1=' . $row['movempresa'] . '&id2=' . $row['movnumero'] . '" />';
								elseif ($row['movprefijo'] == 'DV')
									echo '<img src="' . $r . 'imagenes/iconos/pdf.png" class="pdf" data-rel="' . $r . 'modulos/bodega/documentos/entrada/DV/pdf.php?empresa=' . $row['movempresa'] . '&prefijo=' . $row['movprefijo'] . '&numero=' . $row['movnumero'] . '" />';
								elseif ($row['movprefijo'] == 'NC')
									echo '<img src="' . $r . 'imagenes/iconos/pdf.png" class="pdf" data-rel="' . $r . 'modulos/cartera/notas/NC/pdf.php?empresa=' . $row['movempresa'] . '&numero=' . $row['movnumero'] . '" />';
								elseif ($row['movprefijo'] == 'ND')
									echo '<img src="' . $r . 'imagenes/iconos/pdf.png" class="pdf" data-rel="' . $r . 'modulos/cartera/notas/ND/pdf.php?empresa=' . $row['movempresa'] . '&numero=' . $row['movnumero'] . '" />';
								echo '</td>';
							}
								?>
							</tr>
					</tbody>
				</table>
				<p class="boton">
					<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href = 'detalle.php?id1=<?php echo $empresa . '&id2=' . $numero . '&' . $filtro ?>'">atras</button>
				</p>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<div id="modal" style="display:none"></div>
</body>

</html>