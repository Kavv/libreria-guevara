<?php
$r = '../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
require($r.'incluir/funciones.php');
$empresa = $_GET['id1']; 
$numero = $_GET['id2'];
$filtro = 'empresa='.$_GET['empresa'].'&prefijo='.$_GET['prefijo'].'&numero='.$_GET['numero'].'&recibo='.$_GET['recibo'].'&cliente='.$_GET['cliente'].'&fecha1='.$_GET['fecha1'].'&fecha2='.$_GET['fecha2'];
if($_GET['adinota']){
	$nota = strtoupper(trim($_GET['nota']));
	$calificacion = $_GET['calificacion'];
	$qry = $db->query("INSERT INTO hiscarteras (hisempresa, hiscuenta, hisfecha, hisusuario, hisnota, hiscalificacion) VALUES (".$_GET['id1'].", '".$_GET['id2']."', NOW(), ".$_SESSION['id'].", '$nota', '$calificacion')");
}
$row = $db->query("SELECT * FROM (((((carteras INNER JOIN empresas ON carempresa = empid) INNER JOIN clientes ON carcliente = cliid) INNER JOIN solicitudes ON (carempresa = solempresa AND carfactura = solfactura)) INNER JOIN departamentos ON depid = soldepcobro) INNER JOIN ciudades ON (soldepcobro = ciudepto AND solciucobro = ciuid)) LEFT JOIN usuarios ON carpromotor = usuid WHERE carempresa = $empresa AND carfactura = '$numero'")->fetch(PDO::FETCH_ASSOC);
$row2 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '$empresa' AND dcafactura = '$numero' AND dcaestado = 'ACTIVA' ORDER BY dcacuota ASC")->fetch(PDO::FETCH_ASSOC);
if(date('Y-m-d') >= $row2['dcafecha'])
	$mora = fechaDif($row2['dcafecha'],date('Y-m-d'));
else $mora = 0;
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>. : : S o p h y a : : .</title>
<meta charset="utf-8" />
<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/css/normalize.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/jquery/css/smoothness/jquery-ui-1.10.3.custom.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/css/css.css" type="text/css" />
<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/css/menu.css" type="text/css" />
<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/validation/css/validationEngine.jquery.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/js/jquery-ui-1.10.3.custom.js"></script>
<script type="text/javascript" src="<?php echo $r ?>incluir/validation/js/languages/jquery.validationEngine-es.js"></script>
<script type="text/javascript" src="<?php echo $r ?>incluir/validation/js/jquery.validationEngine.js"></script>
<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#form').validationEngine({
		showOneMessage:true,
		promptPosition : "bottomLeft",
		onValidationComplete: function(form, status){
        	if(status) {               
            	carga();
            	return true;
			}
		}
	});
	$("#tabla").dataTable({
        'bJQueryUI': true,
		'bStateSave': true,
		'sPaginationType': 'full_numbers',
		'oLanguage': {
			'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
		},
		'bSort': false,
	});
	$('.btnatras').button({ icons: { primary: 'ui-icon ui-icon-arrowthick-1-w' }});
	$('.btnmas').button({ icons: { primary: 'ui-icon ui-icon-plus' }, text: false });
  	$('.nota').click(function(){
		$('#campo').dialog({
			height: 130,
			width: 680,
			modal: true
		});
	});
	$('.pdf').click (function(){
	newSrc = $(this).attr('data-rel');
		$('#modal').html("<iframe src='" + newSrc + "' width='100%' height='100%'></iframe>");
		$('#modal').dialog({ modal: true, width: '600', height: '800', title : 'PDF de la solicitud' });
	});
});
</script>
<style type="text/css">
#columna span { display:inline-block; width:120px; text-align:left; float:left }
</style>
</head>
<body>
<?php require($r.'incluir/src/login.php') ?>
<section id="principal">
<?php require($r.'incluir/src/cabeza.php') ?>
<?php require($r.'incluir/src/menu.php') ?>
<article id="cuerpo">
<article class="mapa">
<a href="#">Principal</a><div class="mapa_div"></div><a class="current">Cartera</a>
</article>
<article id="contenido">
<h2>Historial de la cuenta</h2>
<div id="columna">
<p>
<span><strong>Empresa:</strong></span> <?php echo $row['empnombre'] ?><br />
<span><strong>Solicitud:</strong></span> <?php echo $row['solid'] ?><br />
<span><strong>Factura:</strong></span> <?php echo $row['carfactura'] ?><br />
<span><strong>Cliente:</strong></span> <?php echo $row['clinombre'].' '.$row['clinom2'].' '.$row['cliape1'].' '.$row['cliape2'] ?><br />
<span><strong>Email:</strong></span> <?php echo $row['cliemail'] ?><br />
<span><strong>Promotor:</strong></span> <?php echo $row['usunombre'] ?><br />
<span><strong>Ubicacion:</strong></span> <?php echo $row['depnombre'].' / '.$row['ciunombre'] ?>
</p>
<p>
<span><strong>Dirección:</strong></span> <?php echo $row['solbarcobro'].', '.$row['solcobro'] ?><br />
<span><strong>Telefonos:</strong></span> <?php echo $row['clicelular'].' / '.$row['clitelresidencia'].' / '.$row['clitelcomercio'] ?><br />
<span><strong>Cuota inicial:</strong></span>  <?php echo number_format($row['solcuota'],0,',','.') ?><br />
<span><strong>Total credito:</strong></span>  <?php echo number_format($row['cartotal'],0,',','.') ?><br />
<span><strong>Valor cuota:</strong></span>  <?php echo number_format($row['carcuota'],0,',','.') ?><br />
<span><strong>Valor pagado:</strong></span>  <?php echo number_format($row['cartotal'] - $row['carsaldo'],0,',','.') ?><br />
<span><strong>Saldo:</strong></span>  <?php echo number_format($row['carsaldo'],0,',','.') ?><br />
<span><strong>Dias en mora:</strong></span>  <?php echo number_format($mora,0,',','.') ?>
</p>
</div>
<div class="reporte">
<img style="margin:0 3px;" src="<?php echo $r ?>imagenes/iconos/pdf.png" class="pdf" data-rel="<?php echo $r.'pdf/solicitudes/'.$row['solempresa'].'/'.$row['solid'].'.pdf' ?>" title="PDF de la Solicitud" />
<a href="detalle.php?id1=<?php echo $empresa.'&id2='.$numero.'&'.$filtro ?>" onClick="carga()"><img src="<?php echo $r ?>imagenes/iconos/briefcase.png" title="Cartera" /></a> <a href="movimientos.php?id1=<?php echo $empresa.'&id2='.$numero.'&'.$filtro ?>" onClick="carga()"><img src="<?php echo $r ?>imagenes/iconos/application_side_list.png" title="Movimientos" /></a> <img src="<?php echo $r ?>imagenes/iconos/add.png" class="nota" title="Agregar nota" />
</div>
<table id="tabla">
<thead>
<tr>
<th>fecha</th>
<th>Usuario</th>
<th>Nota</th>
<th>Calificacion</th>
</tr>
</thead>
<tbody>
<?php
$qry = $db->query("SELECT * FROM hiscarteras INNER JOIN usuarios ON hisusuario = usuid LEFT JOIN calificaciones ON hiscalificacion = califiid  WHERE hisempresa = ".$row['carempresa']." AND hiscuenta = '".$row['carfactura']."' ORDER BY hisfecha DESC");
while($row = $qry->fetch(PDO::FETCH_ASSOC)){
?>
<tr>
<td align="center"><?php echo $row['hisfecha'] ?></td>
<td><?php echo $row['usunombre'] ?></td>
<td><?php echo $row['hisnota'] ?></td>
<td><?php echo $row['califinombre']  ?></td>
</tr>
<?php
}
?>
</tbody>
</table>
<p class="boton">
<button type="button" class="btnatras" onClick="carga(); location.href = 'consultar.php'">atras</button>
</p>
</article>
</article>
<?php require($r.'incluir/src/pie.php') ?>
</section>
<div id="campo" title="Agregar historico" style="display:none">
<form id="form" name="form" action="historial.php?<?php echo $filtro ?>" method="get">
<input type="hidden" name="id1" value="<?php echo $empresa ?>">
<input type="hidden" name="id2" value="<?php echo $numero ?>">

<label for="calificacion">Calificacion:</label>
<select name="calificacion" class="validate[required]">
<option value="">SELECCIONE </option>
<option value="">TODOS</option>
<?php
$qrycal = $db->query("SELECT * FROM calificaciones ORDER BY califinombre");
while($rowcal = $qrycal->fetch(PDO::FETCH_ASSOC))
    echo '<option value='.$rowcal['califiid'].'>'.$rowcal['califinombre'].'</option>';
?>
</select>
</br></br>

<input type="text" name="nota" class="validate[required]" style="text-transform: uppercase; width: 55em" title="Digite la nota" />
<button type="submit" class="btnmas" name="adinota" value="adinota">+</button>
</div>
<div id="modal" style="display:none"></div>
<div id="dialog2" title="Historico de Solicitudes" style="display:none"></div>
</body>
</html>