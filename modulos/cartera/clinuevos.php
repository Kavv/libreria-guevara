<?php
$r = '../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

if(isset($_POST['consultar']))
{
	/* header("location:excelclinuevos.php?empresa=" .$_POST['empresa']. "&fecha1" .$_POST['fecha1']. "&fecha2" .$_POST['fecha2']);
	exit(); */
}


?>
<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {

			$('#fecha1').datepicker({
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
				maxDate: '+0D',
				onClose: function(selectedDate) {
					$('#fecha2').datepicker('option', 'minDate', selectedDate);
				}
			}).keypress(function(event) {
				event.preventDefault()
			});
			$('#fecha2').datepicker({
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
				maxDate: '+0D',
				onClose: function(selectedDate) {
					$('#fecha1').datepicker('option', 'maxDate', selectedDate);
				}
			}).keypress(function(event) {
				event.preventDefault()
			});

			/* Valida que ambas fechas sean especificadas */
			$('#fecha1').change(function(){
				both_date();
			});
			
			$('#fecha2').change(function(){
				both_date();
			});
			function both_date()
			{
				if($('#fecha2').val() != "" || $('#fecha1').val() != "")
				{
					$('#fecha1').attr("required",true);
					$('#fecha2').attr("required",true);
				}
				else
				{
					$('#fecha1').removeAttr("required",false);
					$('#fecha2').removeAttr("required",false);
				}
			}
			$('#carga').dialog('close');
			$('#fecha1').val("");
			$('#fecha2').val("");

		});
	</script>

</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Cartera</a>
				<div class="mapa_div"></div><a class="current">Clientes Nuevos</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="excelclinuevos.php" method="GET">
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">Reporte Carteras - Cuotas</legend>
						<p>
							<label for="empresa">Empresa:</label>
							<select name="empresa" class="empresa" class="validate[required]">
								<option class="validate[required]" value='TODAS'>TODAS</option>
								<?php
								$qry = $db->query("SELECT * FROM empresas ORDER BY empnombre");
								while ($row = $qry->fetch(PDO::FETCH_ASSOC))
									echo '<option value=' . $row['empid'] . '>' . $row['empnombre'] . '</option>';
								?>
							</select>
						</p>
						<p>
							<label for="fechas">Fechas:</label>
							<input type="text" name="fecha1" id="fecha1" class="fecha" /> 
							<label>-</label> 
							<input type="text" name="fecha2" id="fecha2" class="fecha" />
						</p>
						<p class="boton">
							<button type="submit" name="consultar" class="btn btn-primary btnconsulta">consultar</button>
						</p>
					</fieldset>
				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
</body>

</html>