<?php
$r = '../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
require($r.'incluir/funciones.php');
$promotor = $_POST['promotor'];
$array = array_envia($promotor);
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
	<?php 
		require($r . 'incluir/src/head.php');
		require($r . 'incluir/src/head-form.php');
	?>	

<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('.tabla').dataTable({
		'bJQueryUI': true,
		'bStateSave': true,
		'bSort': false,
		'sPaginationType': 'full_numbers',
		'oLanguage': {
			'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
		},
	});
	$('.btnatras').button({ icons: { primary: 'ui-icon ui-icon-arrowthick-1-w' }});
});
</script>
</head>
<body>
<?php require($r.'incluir/src/login.php') ?>
<section id="principal">
<?php require($r.'incluir/src/cabeza.php') ?>
<?php require($r.'incluir/src/menu.php') ?>
<article id="cuerpo">
<article class="mapa">
<a href="#">Principal</a><div class="mapa_div"></div><a href="#">Cartera</a><div class="mapa_div"></div><a class="current">Reportes</a>
</article>
<article id="contenido">
<h2>Reporte detallado morosos</h2>
<div class="reporte">
<a href="#"><img src="<?php echo $r ?>imagenes/iconos/pdf.png" title="pdf" /></a> <a href="exceldetmorore.php?<?php echo $filtro ?>"><img src="<?php echo $r ?>imagenes/iconos/excel.png" title="csv" /></a>
</div>
<h3><b>RANGO: Al dia</b></h3><br />
<table class="tabla">
<thead>
<tr>
<th>Empresa</th>
<th>cuenta</th>
<th>Cliente</th>
<th>Promotor</th>
<th>C. pacta.</th>
<th>Valor cuota</th>
<th>D. mora</th>
<th>C. mora</th>
<th>C. pend.</th>
<th>Vlr. mora</th>
<th>Saldo actual</th>	
<th>Ult. pago</th>
</tr>
</thead>
<tbody>
<?php
$cta1 = 0;
$vlr1 = 0;
$saldo1 = 0;

	$qry = $db->query("SELECT * FROM ((carteras INNER JOIN clientes ON carcliente = cliid) INNER JOIN empresas ON carempresa = empid) INNER JOIN departamentos ON  depid = clidepresidencia  WHERE carempresa = 900487926 ");
	while($row = $qry->fetch(PDO::FETCH_ASSOC)){	
		$qry2 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row['carempresa']."' AND dcafactura = '".$row['carfactura']."' AND dcaestado = 'ACTIVA' ORDER BY dcacuota ASC");
		$num2 = $qry2->rowCount();
		$row2 = $qry2->fetch(PDO::FETCH_ASSOC);

			$cta1++;
			$saldo1 = $saldo1 + $row['carsaldo'];
			if ($row['carestado'] == 'CANCELADO'){
			$mora = 0;
			} else {
			$mora = fechaDif($row2['dcafecha'],date('Y-m-d'));	
			}
			echo '<tr><td title="'.$row['empid'].'">'.$row['empnombre'].'</td><td align="center">'.$row['carfactura'].'</td><td>'.$row2['clinombre'].' '.$row['clinom2'].' '.$row['cliape1'].' '.$row['cliape2'].'</td><td>'.$row['usunombre'].'</td><td align="center">'.$row['carncuota'].'</td><td align="right">'.number_format($row['carcuota'],0,',','.').'</td><td align="center">'.$mora.'</td><td align="center">0</td><td align="center">'.$num2.'</td><td align="right">'.number_format($ncmora * $row['carcuota'],0,',','.').'</td><td align="right">'.number_format($row['carsaldo'],0,',','.').'</td><td align="center">'.$ultpago.'</td></tr>';

	}

?>
</tbody>
<tfoot>
<tr><td></td><td align="center" bgcolor="#D1CFCF"><?php echo $cta1 ?></td><td colspan="7"></td><td align="right" bgcolor="#D1CFCF"><?php echo number_format($vlr1,0,',','.') ?></td><td align="right" bgcolor="#D1CFCF"><?php echo number_format($saldo1,0,',','.') ?></td><td></td></tr>
</tfoot>
</table>


<p class="boton">
<button type="button" class="btnatras" onClick="carga(); location.href='condetmoro.php'">atras</button>
</p>
</article>
</article>
<?php require($r.'incluir/src/pie.php') ?>
</section>
</body>
</html>