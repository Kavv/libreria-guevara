<?php
$r = '../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');

$fecha = $_GET['fecha'];

$filtro = "fecha=".$fecha;

if($_GET['menos']){
	$identificador = $_GET['identificador'];
	$qry = $db->query("DELETE FROM comovamos WHERE comvaid='$identificador';");
}elseif($_POST['mas']){
	$fecha = $_GET['fecha'];
	$promotor = $_POST['promotor'];
	$valor = $_POST['valor'];
	$cedulas = $_POST['cedulas'];
	$qry = $db->query("INSERT INTO comovamos (comvausuario, comvafecha, comvacedulas, comvavalor) VALUES ('$promotor', '$fecha', '$cedulas', '$valor');");
}

?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>. : : S o p h y a : : .</title>
<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/css/normalize.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/jquery/css/smoothness/jquery-ui-1.10.3.custom.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/css/css.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/css/menu.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/validation/css/validationEngine.jquery.css" />
<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/js/jquery-ui-1.10.3.custom.js"></script>
<script type="text/javascript" src="<?php echo $r ?>incluir/validation/js/languages/jquery.validationEngine-es.js"></script>
<script type="text/javascript" src="<?php echo $r ?>incluir/validation/js/jquery.validationEngine.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#form').validationEngine({
		showOneMessage:true,
		onValidationComplete: function(form, status){
        	if(status) {               
            	carga();
            	return true;
			}
		}
	});
	$('.btnvalidar').button({ icons: { primary: 'ui-icon ui-icon-check' }});
	$('.btnatras').button({ icons: { primary: 'ui-icon ui-icon-arrowthick-1-w' }});
	$('.btnmas').button({ icons: { primary: 'ui-icon ui-icon-plus' }, text: false });
	$('.btnmenos').button({ icons: { primary: 'ui-icon ui-icon-minus' }, text: false });
	$('.btnsiguiente').button({ icons: { secondary: 'ui-icon ui-icon-arrowthick-1-e' }});
	$('#dialog-message').dialog({
		height: 80,
		width: 'auto',
		modal: true
	});
});
</script>
<style type="text/css">
#field { width: 500px; margin:5px auto }
#form fieldset, #form2 fieldset { padding:10px; display:block }
#form legend, #form2 legend, #field legend { font-weight: bold; margin-left:5px; padding:5px }
#form label, #form2 label { display:inline-block; width:240px; text-align:right; margin:0.3em 1em 0 0 }
#form p, #form2 p { margin:5px 0 }
</style>
</head>
<body>
<?php require($r.'incluir/src/login.php') ?>
<section id="principal">
<?php require($r.'incluir/src/cabeza.php') ?>
<?php require($r.'incluir/src/menu.php') ?>
<article id="cuerpo">
<article class="mapa">
<a href="#">Principal</a><div class="mapa_div"></div><a class="current">Cartera</a>
</article>
<article id="contenido">

<fieldset id="field" class="ui-widget ui-widget-content ui-corner-all">
<legend class="ui-widget ui-widget-header ui-corner-all">Reporte Como Vamos 2 </legend>
<form id="form" name="form" action="convamos2.php?<?php echo $filtro; ?>" method="post">
<br>
<br>
<p>
<label for="fecha">Fecha Seleccionada:</label>
<input type="text" id="fecha" value ="<?php echo $fecha ?>" name="fecha" class="fecha" readonly />
</p>

<br><br>
<center>
<p>
<select name="promotor" class="validate[required] text-input" value="Promotor" style="width:20em">
<option value="">SELECCIONE</option>
<?php
$qry = $db->query("SELECT * FROM usuarios WHERE usupromotor = '1' AND usuperfil <> '56' AND usuid NOT IN (SELECT comvausuario FROM comovamos WHERE comvafecha = '$fecha') ORDER BY usunombre");
while($row2 = $qry->fetch(PDO::FETCH_ASSOC)) {
	echo '<option value="'.$row2['usuid'].'">'.$row2['usunombre'].'</option>';
}
?>
</select> 
<input type='text' name='valor' id='valor' title="Valor"  class='valor validate[required, custom[onlyNumberSp]]  text-input'  />
<input style="width:50px;" type='text' title="Numero de Cedulas" name='cedulas' id='cedulas'  class=' validate[required , custom[onlyNumberSp]] text-input'  />
<button type="submit" class="btnmas" name="mas" value="mas">+</button>
</p>
</center>
<br>
<br>
<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
<legend class="ui-widget ui-widget-header ui-corner-all">Detalle de informacion</legend>
<?php
$qry = $db->query("SELECT * FROM comovamos INNER JOIN usuarios ON usuid = comvausuario WHERE comvafecha = '$fecha' ");
$numresul = $qry->rowCount();
while($row2 = $qry->fetch(PDO::FETCH_ASSOC)){
?>
<p align="center">
<input value="<?php echo $row2['usunombre'] ?>"  type="text" class="validate[required] text-input" title="Promotor" style="width:20em" readonly /> 
<input value="<?php echo $row2['comvavalor'] ?>" type='text'   class='valor  text-input'  readonly />
<input value="<?php echo $row2['comvacedulas'] ?>" type='text'  class='valor  text-input' readonly />
&nbsp;<button type="button" class="btnmenos" onclick="carga(); location.href = 'convamos2.php?<?php echo 'fecha='.$fecha.'&identificador='.$row2['comvaid'].'&menos=aIUOfHbHjFHoLm48129692jF45n' ?>'">-</button>
</p>
<?php
}
if ($numresul == 0) {echo "<p>Actualmente no se han agregado promotores a este mes.</p>";}
?>
</fieldset>
<p class="boton">
<button type="submit"  onClick="location.href='convamos3.php?<?php echo "fecha=".$fecha; ?>'" class="btnsiguiente" name="siguiente" value="siguiente">Siguiente</button>
</p>
</form>
</fieldset>
</article>
</article>
<?php require($r.'incluir/src/pie.php') ?>
</section>
<?php
if($_GET['error']) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>'.$_GET['error'].'</div>';
?>
<div id="modal" style="display:none"></div>
<div id="dialog2" style="display:none"></div>
</body>
</html>