<?php
$r = '../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
include($r.'incluir/phpexcel/Classes/PHPExcel.php');


$fecha1 = $_GET['fecha1'];
$fecha2 = $_GET['fecha2'];


$objPHPExcel = new PHPExcel();
$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN
		)
	)
);
$objPHPExcel->getActiveSheet()->getStyle('A1:I2')->applyFromArray($styleArray);
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:I1');

	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'CLIENTES CON PAGO DE CONTADO DEL '.$fecha1.' al '.$fecha2.'.' );	
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A2', 'CLIENTE')
            ->setCellValue('B2', 'IDENTIFICACION')
            ->setCellValue('C2', 'SOLICITUD')
            ->setCellValue('D2', 'FACTURA')
			->setCellValue('E2', 'ESTADO')
			->setCellValue('F2', 'FECHA SCREEN')
			->setCellValue('G2', 'FECHA FACTURACION')
			->setCellValue('H2', 'TOTAL DE LA FACTURA')
			->setCellValue('I2', 'ANALISTA');
$i = 3;

$qry = $db->query("SELECT * FROM solicitudes INNER JOIN clientes on cliid = solcliente LEFT JOIN carteras on (carfactura = solfactura and solempresa = carempresa) WHERE solposcontado = 1 AND DATE(solfechscreen) BETWEEN '".$fecha1."' AND '".$fecha2."' ORDER BY solfechscreen DESC;");
while($row = $qry->fetch(PDO::FETCH_ASSOC)){
	
	$cliente = $row['clinombre']." ".$row['clinom2']." ".$row['cliape1']." ".$row['cliape2'];
	
	$qry2 = $db->query("SELECT * FROM usuarios WHERE usuid = '".$row['carpromotor']."' ;");
	$row2 = $qry2->fetch(PDO::FETCH_ASSOC);
	
	$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':I'.$i)->applyFromArray($styleArray);
	$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A'.$i, $cliente)
	->setCellValue('B'.$i, $row['cliid'])
	->setCellValue('C'.$i, $row['solid'])
	->setCellValue('D'.$i, $row['solfactura'])
	->setCellValue('E'.$i, $row['solestado'])
	->setCellValue('F'.$i, substr($row['solfechscreen'], 0, 10))
	->setCellValue('G'.$i, substr($row['solfechafac'], 0, 10))
	->setCellValue('H'.$i, $row['soltotal'])
	->setCellValue('I'.$i, $row2['usunombre']);
	
	$i++;
}

// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Clientes con pago de contado del  '.$fecha1.' al '.$fecha2.'.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
?>