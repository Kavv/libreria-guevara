<?php
$r = '../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');


if($_GET['menos']){
	$identificador = $_GET['identificador'];
	$qry = $db->query("DELETE FROM consultaniff WHERE consuId='$identificador';");
}elseif($_POST['mas']){
	$descripcion = $_POST['descripcion'];
	$qry = $db->query("INSERT INTO consultaniff (consuIden) VALUES ('$descripcion');");
}

?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>. : : S o p h y a : : .</title>
<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/css/normalize.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/jquery/css/smoothness/jquery-ui-1.10.3.custom.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/css/css.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/css/menu.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/validation/css/validationEngine.jquery.css" />
<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/js/jquery-ui-1.10.3.custom.js"></script>
<script type="text/javascript" src="<?php echo $r ?>incluir/validation/js/languages/jquery.validationEngine-es.js"></script>
<script type="text/javascript" src="<?php echo $r ?>incluir/validation/js/jquery.validationEngine.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#form').validationEngine({
		showOneMessage:true,
		onValidationComplete: function(form, status){
        	if(status) {               
            	carga();
            	return true;
			}
		}
	});
	$('.btnvalidar').button({ icons: { primary: 'ui-icon ui-icon-check' }});
	$('.btnatras').button({ icons: { primary: 'ui-icon ui-icon-arrowthick-1-w' }});
	$('.btnmas').button({ icons: { primary: 'ui-icon ui-icon-plus' }, text: false });
	$('.btnmenos').button({ icons: { primary: 'ui-icon ui-icon-minus' }, text: false });
	$('#dialog-message').dialog({
		height: 80,
		width: 'auto',
		modal: true
	});

});
</script>
<style type="text/css">
#field { width: 550px; margin:5px auto }
#form fieldset, #form2 fieldset { padding:10px; display:block }
#form legend, #form2 legend, #field legend { font-weight: bold; margin-left:5px; padding:5px }
#form label, #form2 label { display:inline-block; width:280px; text-align:right; margin:0.3em 1em 0 0 }
#form p, #form2 p { margin:5px 0 }
</style>
</head>
<body>
<?php require($r.'incluir/src/login.php') ?>
<section id="principal">
<?php require($r.'incluir/src/cabeza.php') ?>
<?php require($r.'incluir/src/menu.php') ?>
<article id="cuerpo">
<article class="mapa">
<a href="#">Principal</a><div class="mapa_div"></div><a href="#">Cartera</a><div class="mapa_div"></div><a class="current">Consultar NIFF</a>
</article>
<article id="contenido">

<fieldset id="field" class="ui-widget ui-widget-content ui-corner-all">
<legend class="ui-widget ui-widget-header ui-corner-all">Consultar NIFF </legend>
<form id="form" name="form" action="conniff.php" method="post">
<br>
<br>

<center>
<p>
<select style="width:310px;" name="descripcion" class="validate[required] text-input">
<option value="">SELECCIONE</option>
<?php
$qry = $db->query("SELECT * FROM niff LEFT JOIN empresas ON empid = niffempresa WHERE nifftipo = 'MORA' group by nifffecha,niffempresa");
while($row = $qry->fetch(PDO::FETCH_ASSOC))
    echo '<option value='.$row['niffid'].'>'.$row['niffempresa'].' - '.$row['nifffecha'].' '.$row['niffano'].'</option>';
?>
</select>
<button type="submit" class="btnmas" name="mas" value="mas">+</button>
</p>
</center>
<br>
<br>
<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
<legend class="ui-widget ui-widget-header ui-corner-all">Detalle de informacion</legend>
<?php
$qry = $db->query("SELECT * FROM consultaniff INNER JOIN niff ON niffid = consuIden LEFT JOIN empresas ON empid = niffempresa ");
$numresul = $qry->rowCount();
while($row2 = $qry->fetch(PDO::FETCH_ASSOC)){
?>
<p align="center">
<input value="<?php echo $row2['niffempresa'].' - '.$row2['nifffecha'].' '.$row2['niffano']  ?>" style="width:310px;" type="text" class="validate[required] text-input" title="Descripcion" readonly /> 
&nbsp;<button type="button" class="btnmenos" onclick="carga(); location.href = 'conniff.php?<?php echo 'identificador='.$row2['consuId'].'&menos=aIUOfHbHjFHoLm48129692jF45n' ?>'">-</button>
</p>
<?php
}
if ($numresul == 0) {echo "<p>Actualmente no se ha agregado detalle a este documento equivalente.</p>";}
?>
</fieldset>
<p class="boton"> 
<button type="button" class="btnvalidar" onClick="carga(); location.href='lisniff.php'">Consultar</button> <!-- BOTON Continuar -->
</p>
</form>
</fieldset>
</article>
</article>
<?php require($r.'incluir/src/pie.php') ?>
</section>
<?php
if($_GET['error']) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>'.$_GET['error'].'</div>';
?>
<div id="modal" style="display:none"></div>
<div id="dialog2" style="display:none"></div>
</body>
</html>