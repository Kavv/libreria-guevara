<?php
$r = '../../../';
require($r.'incluir/connection.php');
require($r.'incluir/fpdf/fpdf.php');
require($r.'incluir/funciones.php');

	$fecha1 = $_GET['fecha1'];
	$fecha2 = $_GET['fecha2'];

$filtro = "fecha1=".$fecha1."&fecha2=".$fecha2;

class PDF extends FPDF{
	function Header(){
		global $fecha1;
		global $fecha2;
    	$this->SetFont('LucidaConsole','',8);
		$this->Cell(0,5,date('Y/m/d H:i:s'),0,1);
		$this->SetFont('LucidaConsole','',11);
		$this->Cell(0,5,'------------------------------------------------------------------------------------------------------------------------------------------------',0,1,'C');
		$this->Cell(0,5,'REPORTE CALIFICACIONES DE PRODUCTOS DESDE '.$fecha1.' A '.$fecha2.'',0,1,'C');
		$this->Cell(0,5,'------------------------------------------------------------------------------------------------------------------------------------------------',0,1,'C');
    	$this->Ln(1);
	}

	function Footer(){
    	$this->SetY(-15);
    	$this->SetFont('LucidaConsole','',11);
		$this->Cell(0,5,'------------------------------------------------------------------------------------------------------------------------------------------------',0,1,'C');
		$this->SetFont('LucidaConsole','',8);
    	$this->Cell(0,5,'Pagina '.$this->PageNo().'/{nb}',0,0,'C');
	}
}

$pdf = new PDF('L','mm','Legal');
$pdf->AddFont('LucidaConsole','','lucon.php');
$pdf->AliasNbPages();
$pdf->AddPage();

$pdf->SetFont('LucidaConsole','',8);

$pdf->SetFillColor(230,230,230);
$pdf->Cell(45,5,'PROMOTOR',1,0,'',true);
$pdf->Cell(22,5,'cuentaS',1,0,'C',true);
$pdf->Cell(27,5,'GESTIONES',1,0,'C',true);
$pdf->Cell(22,5,'CALIFICADOS',1,0,'C',true);
$pdf->Cell(27,5,'NO CALIFICADOS',1,0,'C',true);
$pdf->Cell(22,5,'P.C',1,0,'C',true);
$pdf->Cell(27,5,'P.N.C',1,0,'C',true);
$pdf->Cell(22,5,'NO APLICA',1,0,'C',true);
$pdf->Cell(27,5,'INSUFICIENTE',1,0,'C',true);
$pdf->Cell(22,5,'MALO',1,0,'C',true);
$pdf->Cell(27,5,'ACEPTABLE',1,0,'C',true);
$pdf->Cell(22,5,'BUENO',1,0,'C',true);
$pdf->Cell(27,5,'EXCELENTE',1,1,'C',true);
		
$qry = $db->query("SELECT * FROM usuarios WHERE usupromotor = '1'");
while($row = $qry->fetch(PDO::FETCH_ASSOC)){

	$cuentas = 0;
	$gestiones = 0;
	$calificados = 0;
	$nocalificados = 0;
	$porcentaje = 0;
	$noaplica = 0;
	$insuficiente = 0;
	$malo = 0;
	$aceptable = 0;
	$bueno = 0;
	$excelente = 0;

	$qry2 = $db->query("SELECT * FROM hiscarteras WHERE hisusuario = '".$row['usuid']."' AND DATE(hisfecha) BETWEEN '$fecha1' and '$fecha2' ");
	$gestiones = $qry2->rowCount();
	$qrycu = $db->query("SELECT * FROM hiscarteras WHERE hisusuario = '".$row['usuid']."' AND DATE(hisfecha) BETWEEN '$fecha1' and '$fecha2' GROUP BY hiscuenta,hisempresa ");
	$cuentas = $qrycu->rowCount();
	while($row2 = $qry2->fetch(PDO::FETCH_ASSOC)){
		
		if ($row2['hiscalificacion'] == 6 ){
			$noaplica = $noaplica + 1; 
		}
		if ($row2['hiscalificacion'] == 1){
			$insuficiente = $insuficiente + 1;
		}
		if ($row2['hiscalificacion'] == 2){
			$malo = $malo + 1;
		}
		if ($row2['hiscalificacion'] == 3){
			$aceptable = $aceptable + 1;
		}
		if ($row2['hiscalificacion'] == 4){
			$bueno = $bueno + 1;
		}
		if ($row2['hiscalificacion'] == 5){
			$excelente = $excelente + 1;
		}
		if($row2['hiscalificacion'] == NULL){
			$nocalificados = $nocalificados + 1;
		}
		if ($row2['hiscalificacion'] != NULL){
			$calificados = $calificados + 1;
		}
		
	}
	
	$porcentajecalificados = round((100 * $calificados) / $gestiones);
	$porcentajenocalificados = round((100 * $nocalificados) / $gestiones);

	
	
	$pdf->SetFont('LucidaConsole','',7);
	if($i%2 == 0) $x = 255;
	else $x = 235;
	$pdf->SetFillColor($x,$x,$x);
	$pdf->Cell(45,5,$row['usunombre'],1,0,'',true);
	$pdf->Cell(22,5,$cuentas,1,0,'C',true);
	$pdf->Cell(27,5,$gestiones,1,0,'C',true);
	$pdf->Cell(22,5,$calificados,1,0,'C',true);
	$pdf->Cell(27,5,$nocalificados,1,0,'C',true);
	$pdf->Cell(22,5,'% '.$porcentajecalificados,1,0,'C',true);
	$pdf->Cell(27,5,'% '.$porcentajenocalificados,1,0,'C',true);
	$pdf->Cell(22,5,$noaplica,1,0,'C',true);
	$pdf->Cell(27,5,$insuficiente,1,0,'C',true);
	$pdf->Cell(22,5,$malo,1,0,'C',true);
	$pdf->Cell(27,5,$aceptable,1,0,'C',true);
	$pdf->Cell(22,5,$bueno,1,0,'C',true);
	$pdf->Cell(27,5,$excelente,1,1,'C',true);

	$i++;	
	$totalcuentas = $totalcuentas + $cuentas;
	$totalgestiones = $totalgestiones + $gestiones;
	$totalcalificados = $totalcalificados + $calificados;
	$totalnocalificados = $totalnocalificados + $nocalificados;
	$totalnoaplica = $totalnoaplica + $noaplica;
	$totalinsuficiente = $totalinsuficiente + $insuficiente;
	$totalmalo = $totalmalo + $malo;
	$totalaceptable = $totalaceptable + $aceptable;
	$totalbueno = $totalbueno + $bueno;
	$totalexcelente = $totalexcelente + $excelente;
}
$pdf->SetFillColor(255,255,255);
$pdf->Cell(45,5,'',0,0,'',true);
$pdf->SetFillColor(220,220,220);
$pdf->Cell(22,5,$totalcuentas,1,0,'C',true);
$pdf->Cell(27,5,$totalgestiones,1,0,'C',true);
$pdf->Cell(22,5,$totalcalificados,1,0,'C',true);
$pdf->Cell(27,5,$totalnocalificados,1,0,'C',true);
$pdf->Cell(22,5,'',1,0,'C',true);
$pdf->Cell(27,5,'',1,0,'C',true);
$pdf->Cell(22,5,$totalnoaplica,1,0,'C',true);
$pdf->Cell(27,5,$totalinsuficiente,1,0,'C',true);
$pdf->Cell(22,5,$totalmalo,1,0,'C',true);
$pdf->Cell(27,5,$totalaceptable,1,0,'C',true);
$pdf->Cell(22,5,$totalbueno,1,0,'C',true);
$pdf->Cell(27,5,$totalexcelente,1,1,'C',true);
$pdf->Output('REPORTE VALORACIONES DE PRODUCTOS.pdf','d');
?>