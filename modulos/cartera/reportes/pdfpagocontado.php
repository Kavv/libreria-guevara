<?php
$r = '../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
require($r.'incluir/funciones.php');
require($r.'incluir/fpdf/fpdf.php');

$fecha1 = $_GET['fecha1'];
$fecha2 = $_GET['fecha2'];



class PDF extends FPDF{
	function Header(){
		global $fecha1;
		global $fecha2;
    	$this->SetFont('LucidaConsole','',6);
		$this->Cell(50,5,date('Y/m/d'),0,1);
		$this->SetFont('LucidaConsole','',8);
		$this->Cell(280,5,'--------------------------------------------------------------------------------------------------------------------------------------------------------------------',0,1,'C');
    	$this->Cell(280,5,'CLIENTES PAGO POSIBLE DE CONTADO ENTRE  EL '.$fecha1.' AL '.$fecha2,0,1,'C');
		$this->Cell(280,5,'--------------------------------------------------------------------------------------------------------------------------------------------------------------------',0,1,'C');
    	$this->Ln(5);
	}

	function Footer(){
    	$this->SetY(-15);
    	$this->SetFont('LucidaConsole','',8);
		$this->Cell(0,5,'----------------------------------------------------------------------------------------------------------------------------------------------------------------------',0,1,'C');
		$this->SetFont('LucidaConsole','',8);
    	$this->Cell(0,5,'Pagina '.$this->PageNo().'/{nb}',0,0,'R');
	}
}

$pdf = new PDF();
$pdf->AddFont('LucidaConsole','','lucon.php');
$pdf->AliasNbPages();
$pdf->AddPage('L');

$pdf->SetFont('LucidaConsole','',10);
$pdf->SetFillColor(220,220,220);
$pdf->SetX(8);
// Suma total 260
$pdf->Cell(80,5,'CLIENTE',1,0,'C',true);
$pdf->Cell(40,5,'SOLICITUD',1,0,'C',true);
$pdf->Cell(40,5,'FACTURA',1,0,'C',true);
$pdf->Cell(50,5,'ESTADO',1,0,'C',true);
$pdf->Cell(35,5,'F. SCREEN',1,0,'C',true);
$pdf->Cell(35,5,'F. FACTURACION',1,1,'C',true);

$i = 1;
$qry = $db->query("SELECT * FROM solicitudes INNER JOIN clientes on cliid = solcliente WHERE solposcontado = 1 AND DATE(solfechscreen) BETWEEN '".$fecha1."' AND '".$fecha2."' ORDER BY solfechscreen DESC;");
$num = $qry->rowCount();
while($row = $qry->fetch(PDO::FETCH_ASSOC)){

	$pdf->SetFont('LucidaConsole','',8);
	if($i%2 == 0) $x = 255;
	else $x = 235;
	$pdf->SetFillColor($x,$x,$x);
	$pdf->SetX(8);
	$pdf->Cell(80,5,$row['clinombre'].' '.$row['clinom2'].' '.$row['cliape1'].' '.$row['cliape2'],1,0,'L',true);
	$pdf->Cell(40,5,$row['solid'],1,0,'C',true);
	$pdf->Cell(40,5,$row['solfactura'],1,0,'C',true);
	$pdf->Cell(50,5,$row['solestado'],1,0,'C',true);
	//$pdf->Cell(20,5,substr($row['solfechafac'],0,10),1,0,'C',true);
	$pdf->Cell(35,5,substr($row['solfechscreen'],0,10),1,0,'C',true);
	$pdf->Cell(35,5,substr($row['solfechafac'],0,10),1,1,'C',true);
	$i++;
}
$pdf->SetX(8);
$pdf->SetFillColor(255,255,255);
$pdf->Cell(80,5,'',0,0,'',true);
$pdf->SetFillColor(220,220,220);
$pdf->Cell(40,5,number_format($num,0,',','.'),1,0,'C',true);


$pdf->Output('Clientes pago posible contado entre el '.$fecha1.' a '.$fecha2.'.pdf','d');
?>