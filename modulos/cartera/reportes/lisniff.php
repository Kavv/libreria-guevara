<?php
$r = '../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
require($r.'incluir/funciones.php');

$fecha1 = $_POST['fecha1'];
$fecha2 = $_POST['fecha2'];
$empresa = $_POST['empresa'];

$filtro = 'empresa='.$empresa.'&fecha1='.$fecha1.'&fecha2='.$fecha2;
$meshoy = date('n');

$anofecha1 = substr($fecha1, 0, 4);
$mesfecha1 = substr($fecha1, 5, 2);
if ($mesfecha1 == '01') {$mes1 = '1';}
elseif ($mesfecha1 == '02') {$mes1 = '2';}
elseif ($mesfecha1 == '03') {$mes1 = '3';}
elseif ($mesfecha1 == '04') {$mes1 = '4';}
elseif ($mesfecha1 == '05') {$mes1 = '5';}
elseif ($mesfecha1 == '06') {$mes1 = '6';}
elseif ($mesfecha1 == '07') {$mes1 = '7';}
elseif ($mesfecha1 == '08') {$mes1 = '8';}
elseif ($mesfecha1 == '09') {$mes1 = '9';}
elseif ($mesfecha1 == '10') {$mes1 = '10';}
elseif ($mesfecha1 == '11') {$mes1 = '11';}
elseif ($mesfecha1 == '12') {$mes1 = '12';}

$anofecha2 = substr($fecha2, 0, 4);
$mesfecha2 = substr($fecha2, 5, 2);
if ($mesfecha2 == '01') {$mes2 = '1';}
elseif ($mesfecha2 == '02') {$mes2 = '2';}
elseif ($mesfecha2 == '03') {$mes2 = '3';}
elseif ($mesfecha2 == '04') {$mes2 = '4';}
elseif ($mesfecha2 == '05') {$mes2 = '5';}
elseif ($mesfecha2 == '06') {$mes2 = '6';}
elseif ($mesfecha2 == '07') {$mes2 = '7';}
elseif ($mesfecha2 == '08') {$mes2 = '8';}
elseif ($mesfecha2 == '09') {$mes2 = '9';}
elseif ($mesfecha2 == '10') {$mes2 = '10';}
elseif ($mesfecha2 == '11') {$mes2 = '11';}
elseif ($mesfecha2 == '12') {$mes2 = '12';}

if (($mes1 <> $meshoy) and ($mes2 == $meshoy)){
	$error ="Recuerde que el mes en curso no puede ser consultado junto con meses pasados.";
	header('Location:conniff.php?error='.$error.'&'.$filtro);
} elseif ($anofecha1 <> $anofecha2) {
	$error ="Recuerde que las consultas solo se pueden realizar dentro de un mismo año.";
	header('Location:conniff.php?error='.$error.'&'.$filtro);	
}

if($empresa <> 'TODAS'){
	$qryem = $db->query("SELECT * FROM empresas where empid = '$empresa' ORDER BY empnombre");
	$rowem = $qryem->fetch(PDO::FETCH_ASSOC);
}

if($empresa <> 'TODAS'){
	$anexempresa = " AND niffempresa = '$empresa'";
}
$sql = "SELECT * FROM niff INNER JOIN empresas ON empid = niffempresa WHERE nifftipo = 'MORA' AND nifffecha BETWEEN '$mes1' and '$mes2' and niffano =  '$anofecha1' ".$anexempresa;

$qry = $db->query($sql);
while($row = $qry->fetch(PDO::FETCH_ASSOC)){
	if ($row['niffdigito'] == '1'){
		$valorkaldia = $valorkaldia + $row['niffvalork'];
		$financiacionaldia = $financiacionaldia + $row['niffvalorfinan'];
		$clientesaldia = $clientesaldia + $row['niffclientes'];
	}
	if ($row['niffdigito'] == '2'){
		$valork1_30 = $valork1_30 + $row['niffvalork'];
		$financiacion1_30 = $financiacion1_30 + $row['niffvalorfinan'];
		$clientes1_30 = $clientes1_30 + $row['niffclientes'];
		$deterioro1_30 = $deterioro1_30 + $row['niffdeterioro'];
	}
	if ($row['niffdigito'] == '3'){
		$valork31_60 = $valork31_60+ $row['niffvalork'];
		$financiacion31_60 = $financiacion31_60 + $row['niffvalorfinan'];
		$clientes31_60 = $clientes31_60 + $row['niffclientes'];
		$deterioro31_60 = $deterioro31_60+ $row['niffdeterioro'];
	}
	if ($row['niffdigito'] == '4'){
		$valork61_90 = $valork61_90+ $row['niffvalork'];
		$financiacion61_90 = $financiacion61_90 + $row['niffvalorfinan'];
		$clientes61_90 = $clientes61_90 + $row['niffclientes'];
		$deterioro61_90 = $deterioro61_90+ $row['niffdeterioro'];
	}
	if ($row['niffdigito'] == '5'){
		$valork91_120 = $valork91_120+ $row['niffvalork'];
		$financiacion91_120 = $financiacion91_120 + $row['niffvalorfinan'];
		$clientes91_120 = $clientes91_120 + $row['niffclientes'];
		$deterioro91_120 = $deterioro91_120+ $row['niffdeterioro'];
	}
	if ($row['niffdigito'] == '6'){
		$valork121_150 = $valork121_150+ $row['niffvalork'];
		$financiacion121_150 = $financiacion121_150 + $row['niffvalorfinan'];
		$clientes121_150 = $clientes121_150 + $row['niffclientes'];
		$deterioro121_150 = $deterioro121_150+ $row['niffdeterioro'];
	}
	if ($row['niffdigito'] == '7'){
		$valork151_180 = $valork151_180+ $row['niffvalork'];
		$financiacion151_180 = $financiacion151_180 + $row['niffvalorfinan'];
		$clientes151_180 = $clientes151_180 + $row['niffclientes'];
		$deterioro151_180 = $deterioro151_180+ $row['niffdeterioro'];
	}
	if ($row['niffdigito'] == '8'){
		$valork181_210 = $valork181_210+ $row['niffvalork'];
		$financiacion181_210 = $financiacion181_210 + $row['niffvalorfinan'];
		$clientes181_210 = $clientes181_210 + $row['niffclientes'];
		$deterioro181_210 = $deterioro181_210+ $row['niffdeterioro'];
	}
	if ($row['niffdigito'] == '9'){
		$valork211_240 = $valork211_240+ $row['niffvalork'];
		$financiacion211_240 = $financiacion211_240 + $row['niffvalorfinan'];
		$clientes211_240 = $clientes211_240 + $row['niffclientes'];
		$deterioro211_240 = $deterioro211_240+ $row['niffdeterioro'];
	}
	if ($row['niffdigito'] == '10'){
		$valork241_270 = $valork241_270+ $row['niffvalork'];
		$financiacion241_270 = $financiacion241_270 + $row['niffvalorfinan'];
		$clientes241_270 = $clientes241_270 + $row['niffclientes'];
		$deterioro241_270 = $deterioro241_270+ $row['niffdeterioro'];
	}
	if ($row['niffdigito'] == '11'){
		$valork271_300 = $valork271_300+ $row['niffvalork'];
		$financiacion271_300 = $financiacion271_300 + $row['niffvalorfinan'];
		$clientes271_300 = $clientes271_300 + $row['niffclientes'];
		$deterioro271_300 = $deterioro271_300+ $row['niffdeterioro'];
	}
	if ($row['niffdigito'] == '12'){
		$valork301_330 = $valork301_330+ $row['niffvalork'];
		$financiacion301_330 = $financiacion301_330 + $row['niffvalorfinan'];
		$clientes301_330 = $clientes301_330 + $row['niffclientes'];
		$deterioro301_330 = $deterioro301_330+ $row['niffdeterioro'];
	}
	if ($row['niffdigito'] == '13'){
		$valork331_360 = $valork331_360+ $row['niffvalork'];
		$financiacion331_360 = $financiacion331_360 + $row['niffvalorfinan'];
		$clientes331_360 = $clientes331_360 + $row['niffclientes'];
		$deterioro331_360 = $deterioro331_360+ $row['niffdeterioro'];
	}
	if ($row['niffdigito'] == '14'){
		$valorkcastigada = $valorkcastigada+ $row['niffvalork'];
		$financiacioncastigada = $financiacioncastigada + $row['niffvalorfinan'];
		$clientescastigada = $clientescastigada + $row['niffclientes'];
	}

}

?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
	<?php 
		require($r . 'incluir/src/head.php');
		require($r . 'incluir/src/head-form.php');
	?>	

<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#tabla').dataTable({
		'bPaginate': false,
        'bLengthChange': false,
        'bFilter': false,
		'bSort': false,
        'bInfo': false,
        'bAutoWidth': false,
		'bStateSave': true,
		'bJQueryUI': true
	});
	$('.btnatras').button({ icons: { primary: 'ui-icon ui-icon-arrowthick-1-w' }});
});
</script>
</head>
<body>
<?php require($r.'incluir/src/login.php') ?>
<section id="principal">
<?php require($r.'incluir/src/cabeza.php') ?>
<?php require($r.'incluir/src/menu.php') ?>
<article id="cuerpo">
<article class="mapa">
<a href="#">Principal</a><div class="mapa_div"></div><a href="#">Cartera</a><div class="mapa_div"></div><a class="current">Reportes</a>
</article>
<article id="contenido">
<h2>Reporte NIFF Mora del <?php  echo $fecha1.' al '.$fecha2; if($empresa != 'TODAS') {echo ' de la empresa '.$rowem['empnombre'].' ';} elseif($empresa == 'TODAS') {echo ' de todas las empresas. ';} ?></h2>
<div class="reporte">
<a href="pdfniff.php?<?php echo $filtro ?>"><img src="<?php echo $r ?>imagenes/iconos/pdf.png" title="pdf" /></a> <a href="#"><img src="<?php echo $r ?>imagenes/iconos/excel.png" title="csv" /></a>
</div>
<table id="tabla">
<thead>
<tr>
<th></th>
<th>Saldo Cartera</th>
<th>Valor K</th>
<th>Valor Financiacion</th>
<th>Nro Clientes</th>
<th>Valor Deterioro</th>
</tr>
</thead>
<tbody>

<tr> <td>A</td> <td>AL DIA </td> <td align="center"><?php echo number_format($valorkaldia ,0,',','.') ?></td> <td align="center"><?php echo number_format($financiacionaldia ,0,',','.') ?></td> <td align="center"><?php echo number_format($clientesaldia ,0,',','.') ?></td> <td align="center"><?php echo number_format($no ,0,',','.') ?></td>  </tr>
<tr><td>A</td> <td>MORA 30 </td> <td align="center"><?php echo number_format($valork1_30 ,0,',','.') ?></td> <td align="center"><?php echo number_format($financiacion1_30 ,0,',','.') ?></td> <td align="center"><?php echo number_format($clientes1_30 ,0,',','.') ?></td> <td align="center"><?php echo number_format($deterioro1_30 ,0,',','.') ?></td>  </tr>
<tr><td>B</td> <td>MORA 60 </td> <td align="center"><?php echo number_format($valork31_60 ,0,',','.') ?></td> <td align="center"><?php echo number_format($financiacion31_60 ,0,',','.') ?></td> <td align="center"><?php echo number_format($clientes31_60 ,0,',','.') ?></td> <td align="center"><?php echo number_format($deterioro31_60 ,0,',','.') ?></td>  </tr>
<tr><td>B</td> <td>MORA 90 </td> <td align="center"><?php echo number_format($valork61_90 ,0,',','.') ?></td> <td align="center"><?php echo number_format($financiacion61_90 ,0,',','.') ?></td> <td align="center"><?php echo number_format($clientes61_90 ,0,',','.') ?></td> <td align="center"><?php echo number_format($deterioro61_90 ,0,',','.') ?></td>  </tr>
<tr><td>B</td> <td>MORA 120 </td> <td align="center"><?php echo number_format($valork91_120 ,0,',','.') ?></td> <td align="center"><?php echo number_format($financiacion91_120 ,0,',','.') ?></td> <td align="center"><?php echo number_format($clientes91_120 ,0,',','.') ?></td> <td align="center"><?php echo number_format($deterioro91_120 ,0,',','.') ?></td>  </tr>
<tr><td>C</td> <td>MORA 150 </td> <td align="center"><?php echo number_format($valork121_150 ,0,',','.') ?></td> <td align="center"><?php echo number_format($financiacion121_150 ,0,',','.') ?></td> <td align="center"><?php echo number_format($clientes121_150 ,0,',','.') ?></td> <td align="center"><?php echo number_format($deterioro121_150 ,0,',','.') ?></td>  </tr>
<tr><td>C</td> <td>MORA 180 </td> <td align="center"><?php echo number_format($valork151_180 ,0,',','.') ?></td> <td align="center"><?php echo number_format($financiacion151_180 ,0,',','.') ?></td> <td align="center"><?php echo number_format($clientes151_180 ,0,',','.') ?></td> <td align="center"><?php echo number_format($deterioro151_180 ,0,',','.') ?></td>  </tr>
<tr><td>C</td> <td>MORA 210 </td> <td align="center"><?php echo number_format($valork181_210 ,0,',','.') ?></td> <td align="center"><?php echo number_format($financiacion181_210 ,0,',','.') ?></td> <td align="center"><?php echo number_format($clientes181_210 ,0,',','.') ?></td> <td align="center"><?php echo number_format($deterioro181_210 ,0,',','.') ?></td>  </tr>
<tr> <td>D</td><td>MORA 240 </td> <td align="center"><?php echo number_format($valork211_240 ,0,',','.') ?></td> <td align="center"><?php echo number_format($financiacion211_240 ,0,',','.') ?></td> <td align="center"><?php echo number_format($clientes211_240 ,0,',','.') ?></td> <td align="center"><?php echo number_format($deterioro211_240 ,0,',','.') ?></td>  </tr>
<tr> <td>D</td><td>MORA 270 </td> <td align="center"><?php echo number_format($valork241_270 ,0,',','.') ?></td> <td align="center"><?php echo number_format($financiacion241_270 ,0,',','.') ?></td> <td align="center"><?php echo number_format($clientes241_270 ,0,',','.') ?></td> <td align="center"><?php echo number_format($deterioro241_270 ,0,',','.') ?></td>  </tr>
<tr> <td>D</td><td>MORA 300 </td> <td align="center"><?php echo number_format($valork271_300 ,0,',','.') ?></td> <td align="center"><?php echo number_format($financiacion271_300 ,0,',','.') ?></td> <td align="center"><?php echo number_format($clientes271_300 ,0,',','.') ?></td> <td align="center"><?php echo number_format($deterioro271_300 ,0,',','.') ?></td>  </tr>
<tr> <td>D</td><td>MORA 330 </td> <td align="center"><?php echo number_format($valork301_330 ,0,',','.') ?></td> <td align="center"><?php echo number_format($financiacion301_330 ,0,',','.') ?></td> <td align="center"><?php echo number_format($clientes301_330 ,0,',','.') ?></td> <td align="center"><?php echo number_format($deterioro301_330 ,0,',','.') ?></td>  </tr>
<tr> <td>D</td><td>MORA 360 </td> <td align="center"><?php echo number_format($valork331_360 ,0,',','.') ?></td> <td align="center"><?php echo number_format($financiacion331_360 ,0,',','.') ?></td> <td align="center"><?php echo number_format($clientes331_360 ,0,',','.') ?></td> <td align="center"><?php echo number_format($deterioro331_360 ,0,',','.') ?></td>  </tr>
<tr> <td>E</td><td>CARTERA CASTIGADA </td> <td align="center"><?php echo number_format($valorkcastigada ,0,',','.') ?></td> <td align="center"><?php echo number_format($financiacioncastigada ,0,',','.') ?></td> <td align="center"><?php echo number_format($clientescastigada ,0,',','.') ?></td> <td align="center"><?php echo number_format($no ,0,',','.') ?></td>  </tr>

<?php
$totalvalork = $valorkaldia + $valork1_30 + $valork31_60 + $valork61_90 + $valork91_120 + $valork121_150 + $valork151_180 + $valork181_210 + $valork211_240 + $valork241_270 + $valork271_300 + $valork301_330 + $valork331_360 + $valorkcastigada;
$totalfinanciacion = $financiacionaldia + $financiacion1_30 + $financiacion31_60 + $financiacion61_90 + $financiacion91_120 + $financiacion121_150 + $financiacion151_180 + $financiacion181_210 + $financiacion211_240 + $financiacion241_270 + $financiacion271_300 + $financiacion301_330 + $financiacion331_360 + $financiacioncastigada;
$totalclientes = $clientesaldia + $clientes1_30 + $clientes31_60 + $clientes61_90 + $clientes91_120 + $clientes121_150 + $clientes151_180 + $clientes181_210 + $clientes211_240 + $clientes241_270 + $clientes271_300 + $clientes301_330 + $clientes331_360 + $clientescastigada;
$totaldeterioro = $deterioro1_30 + $deterioro31_60 + $deterioro61_90 + $deterioro91_120 + $deterioro121_150 + $deterioro151_180 + $deterioro181_210 + $deterioro211_240 + $deterioro241_270 + $deterioro271_300 + $deterioro301_330 + $deterioro331_360;
?>
<tr> <td></td><td>TOTALES </td> <td align="center"><?php echo number_format($totalvalork ,0,',','.') ?></td> <td align="center"><?php echo number_format($totalfinanciacion ,0,',','.') ?></td> <td align="center"><?php echo number_format($totalclientes ,0,',','.') ?></td> <td align="center"><?php echo number_format($totaldeterioro ,0,',','.') ?></td>  </tr>

</tbody>
</table>
<p class="boton">
<button type="button" class="btnatras" onClick="carga(); location.href='conniff.php'">atras</button>
</p>
</article>
</article>
<?php require($r.'incluir/src/pie.php') ?>
</section>
</body>
</html>