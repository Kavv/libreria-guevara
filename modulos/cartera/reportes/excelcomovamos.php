<?php
$r = '../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
require($r.'incluir/phpexcel/Classes/PHPExcel.php');

$fecha = $_GET['fecha'];
$filtro = "fecha=".$fecha;


$sql = "SELECT * FROM comoVamos INNER JOIN usuarios ON usuid = comvausuario INNER JOIN comovamosgrupodeta ON grupousuario = comvausuario WHERE comvafecha = '$fecha' AND grupofecha = '$fecha';";

$qry = $db->query($sql);
$objPHPExcel = new PHPExcel();
$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN
		)
	)
);
$styleArrayPrin = array(
    'font' => array(
        'bold' => true,
		'size' => 15
    ),
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
        ),
    ),
    'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_solid,
    ),
	'code' => PHPExcel_Style_NumberFormat::FORMAT_GENERAL,
);
$styleArrayOne = array(
    'font' => array(
        'bold' => true,
		'size' => 11.8
    ),
	'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
    ),
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
        ),
    ),
    'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_solid,
    ),
);

$styleArrayTwo = array(
    'font' => array(
        'bold' => true,
		'size' => 11.5,
		'color' => array('rgb' => 'FF0000')
    ),
	'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
    ),
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
        ),
    ),
    'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_solid,
        'color' => array('rgb' => 'BDBDBD')
    ),
);

$styleArrayThree = array(
    'font' => array(
        'bold' => true,
		'size' => 11.5,
		'color' => array('rgb' => 'FF0000')
    ),
	'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
    ),
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
        ),
    ),
    'fill' => array(
		'type' => PHPExcel_Style_Fill::FILL_solid,
        'color' => array('rgb' => '00EFFF')
    ),
);

$objPHPExcel->getActiveSheet()->getStyle('A1:BQ2')->applyFromArray($styleArrayPrin);
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:BQ1')
			->setCellValue('A1', 'REPORTE COMO VAMOS DEL MES '.$fecha);
			$i = 2;
			
	
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$i.':C'.$i);
$objPHPExcel->getActiveSheet()->getStyle('A'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$i, 'CALL CENTER');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('D'.$i.':E'.$i);
$objPHPExcel->getActiveSheet()->getStyle('D'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$i, 'REALIZADO');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('F'.$i.':G'.$i);
$objPHPExcel->getActiveSheet()->getStyle('F'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$i, ' % REALIZADO');
 
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('H'.$i.':I'.$i);
$objPHPExcel->getActiveSheet()->getStyle('H'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$i, '1');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('J'.$i.':K'.$i);
$objPHPExcel->getActiveSheet()->getStyle('J'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.$i, '2');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('L'.$i.':M'.$i);
$objPHPExcel->getActiveSheet()->getStyle('L'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('L'.$i, '3');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('N'.$i.':O'.$i);
$objPHPExcel->getActiveSheet()->getStyle('N'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('N'.$i, '4');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('P'.$i.':Q'.$i);
$objPHPExcel->getActiveSheet()->getStyle('P'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('P'.$i, '5');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('R'.$i.':S'.$i);
$objPHPExcel->getActiveSheet()->getStyle('L'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('R'.$i, '6');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('T'.$i.':U'.$i);
$objPHPExcel->getActiveSheet()->getStyle('T'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('T'.$i, '7');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('V'.$i.':W'.$i);
$objPHPExcel->getActiveSheet()->getStyle('V'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('V'.$i, '8');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('X'.$i.':Y'.$i);
$objPHPExcel->getActiveSheet()->getStyle('X'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('X'.$i, '9');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('Z'.$i.':AA'.$i);
$objPHPExcel->getActiveSheet()->getStyle('Z'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('Z'.$i, '10');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AB'.$i.':AC'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AB'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AB'.$i, '11');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AB'.$i.':AC'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AB'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AB'.$i, '11');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AD'.$i.':AE'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AD'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AD'.$i, '12');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AF'.$i.':AG'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AF'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AF'.$i, '13');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AH'.$i.':AI'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AH'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AH'.$i, '14');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AJ'.$i.':AK'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AJ'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AJ'.$i, '15');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AL'.$i.':AM'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AL'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AL'.$i, '16');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AN'.$i.':AO'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AN'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AN'.$i, '17');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AP'.$i.':AQ'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AP'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AP'.$i, '18');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AR'.$i.':AS'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AR'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AR'.$i, '19');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AT'.$i.':AU'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AT'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AT'.$i, '20');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AV'.$i.':AW'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AV'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AV'.$i, '21');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AX'.$i.':AY'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AX'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AX'.$i, '22');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AZ'.$i.':BA'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AZ'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AZ'.$i, '23');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BB'.$i.':BC'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BB'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BB'.$i, '24');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BD'.$i.':BE'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BD'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BD'.$i, '25');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BF'.$i.':BG'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BF'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BF'.$i, '26');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BH'.$i.':BI'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BH'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BH'.$i, '27');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BJ'.$i.':BK'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BJ'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BJ'.$i, '28');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BL'.$i.':BM'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BL'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BL'.$i, '29');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BN'.$i.':BO'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BN'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BN'.$i, '30');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BP'.$i.':BQ'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BP'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BP'.$i, '31');


$i++;
$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':BQ'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$i, 'ASESOR')
            ->setCellValue('B'.$i, 'CEDULAS')
            ->setCellValue('C'.$i, 'VALOR')
            ->setCellValue('D'.$i, 'CEDULAS')
			->setCellValue('E'.$i, 'VALOR')
			->setCellValue('F'.$i, 'CEDULAS')
			->setCellValue('G'.$i, 'VALOR')	
			->setCellValue('H'.$i, 'CEDULAS')
			->setCellValue('I'.$i, 'RECAUDO')
			->setCellValue('J'.$i, 'CEDULAS')
			->setCellValue('K'.$i, 'RECAUDO')	
			->setCellValue('L'.$i, 'CEDULAS')
			->setCellValue('M'.$i, 'RECAUDO')	
			->setCellValue('N'.$i, 'CEDULAS')
			->setCellValue('O'.$i, 'RECAUDO')	
			->setCellValue('P'.$i, 'CEDULAS')
			->setCellValue('Q'.$i, 'RECAUDO')	
			->setCellValue('R'.$i, 'CEDULAS')
			->setCellValue('S'.$i, 'RECAUDO')	
			->setCellValue('T'.$i, 'CEDULAS')
			->setCellValue('U'.$i, 'RECAUDO')	
			->setCellValue('V'.$i, 'CEDULAS')
			->setCellValue('W'.$i, 'RECAUDO')	
			->setCellValue('X'.$i, 'CEDULAS')
			->setCellValue('Y'.$i, 'RECAUDO')	
			->setCellValue('Z'.$i, 'CEDULAS')
			->setCellValue('AA'.$i, 'RECAUDO')	
			->setCellValue('AB'.$i, 'CEDULAS')
			->setCellValue('AC'.$i, 'RECAUDO')	
			->setCellValue('AD'.$i, 'CEDULAS')
			->setCellValue('AE'.$i, 'RECAUDO')	
			->setCellValue('AF'.$i, 'CEDULAS')
			->setCellValue('AG'.$i, 'RECAUDO')	
			->setCellValue('AH'.$i, 'CEDULAS')
			->setCellValue('AI'.$i, 'RECAUDO')	
			->setCellValue('AJ'.$i, 'CEDULAS')
			->setCellValue('AK'.$i, 'RECAUDO')	
			->setCellValue('AL'.$i, 'CEDULAS')
			->setCellValue('AM'.$i, 'RECAUDO')	
			->setCellValue('AN'.$i, 'CEDULAS')
			->setCellValue('AO'.$i, 'RECAUDO')	
			->setCellValue('AP'.$i, 'CEDULAS')
			->setCellValue('AQ'.$i, 'RECAUDO')	
			->setCellValue('AR'.$i, 'CEDULAS')
			->setCellValue('AS'.$i, 'RECAUDO')	
			->setCellValue('AT'.$i, 'CEDULAS')
			->setCellValue('AU'.$i, 'RECAUDO')	
			->setCellValue('AV'.$i, 'CEDULAS')
			->setCellValue('AW'.$i, 'RECAUDO')	
			->setCellValue('AX'.$i, 'CEDULAS')
			->setCellValue('AY'.$i, 'RECAUDO')	
			->setCellValue('AZ'.$i, 'CEDULAS')
			->setCellValue('BA'.$i, 'RECAUDO')	
			->setCellValue('BB'.$i, 'CEDULAS')
			->setCellValue('BC'.$i, 'RECAUDO')	
			->setCellValue('BD'.$i, 'CEDULAS')
			->setCellValue('BE'.$i, 'RECAUDO')	
			->setCellValue('BF'.$i, 'CEDULAS')
			->setCellValue('BG'.$i, 'RECAUDO')	
			->setCellValue('BH'.$i, 'CEDULAS')
			->setCellValue('BI'.$i, 'RECAUDO')	
			->setCellValue('BJ'.$i, 'CEDULAS')
			->setCellValue('BK'.$i, 'RECAUDO')	
			->setCellValue('BL'.$i, 'CEDULAS')
			->setCellValue('BM'.$i, 'RECAUDO')	
			->setCellValue('BN'.$i, 'CEDULAS')
			->setCellValue('BO'.$i, 'RECAUDO')	
			->setCellValue('BP'.$i, 'CEDULAS')
			->setCellValue('BQ'.$i, 'RECAUDO');
			$i++;

while($row = $qry->fetch(PDO::FETCH_ASSOC)){
if ($row['gruponombre'] == 'CALL CENTER		') {
	
	$sqlrealizado = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha >= '$fecha'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";

	$qryrealizado = $db->query($sqlrealizado);	
	$rowrealizado = $qryrealizado->fetch(PDO::FETCH_ASSOC);	
	
	$porcencedulas = ($rowrealizado['Cedulas'] / $row['comvacedulas']) * 100;
	$porcenvalor = ($rowrealizado['Valor'] /  $row['comvavalor']) * 100;
	
	$fechasindias = substr($fecha, 0, -2);
	

	$sqldia1 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."01'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia1 = $db->query($sqldia1);	
	$rowdia1 = $qrydia1->fetch(PDO::FETCH_ASSOC);
	
	$cedulas1 = $rowdia1['Cedulas'];
	$recaudo1 = $rowdia1['Valor'];
	
	
	$sqldia2 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."02'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia2 = $db->query($sqldia2);	
	$rowdia2 = $qrydia2->fetch(PDO::FETCH_ASSOC);
	
	$cedulas2 = $rowdia2['Cedulas'];
	$recaudo2 = $rowdia2['Valor'];


	$sqldia3 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."03'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia3 = $db->query($sqldia3);	
	$rowdia3 = $qrydia3->fetch(PDO::FETCH_ASSOC);
	
	$cedulas3 = $rowdia3['Cedulas'];
	$recaudo3 = $rowdia3['Valor'];


	$sqldia4 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."04'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia4 = $db->query($sqldia4);	
	$rowdia4 = $qrydia4->fetch(PDO::FETCH_ASSOC);
	
	$cedulas4 = $rowdia4['Cedulas'];
	$recaudo4 = $rowdia4['Valor'];


	$sqldia5 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."05'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia5 = $db->query($sqldia5);	
	$rowdia5 = $qrydia5->fetch(PDO::FETCH_ASSOC);
	
	$cedulas5 = $rowdia5['Cedulas'];
	$recaudo5 = $rowdia5['Valor'];


	$sqldia6 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."06'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia6 = $db->query($sqldia6);	
	$rowdia6 = $qrydia6->fetch(PDO::FETCH_ASSOC);
	
	$cedulas6 = $rowdia6['Cedulas'];
	$recaudo6 = $rowdia6['Valor'];


	$sqldia7 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."07'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia7 = $db->query($sqldia7);	
	$rowdia7 = $qrydia7->fetch(PDO::FETCH_ASSOC);
	
	$cedulas7 = $rowdia7['Cedulas'];
	$recaudo7 = $rowdia7['Valor'];


	$sqldia8 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."08'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia8 = $db->query($sqldia8);	
	$rowdia8 = $qrydia8->fetch(PDO::FETCH_ASSOC);
	
	$cedulas8 = $rowdia8['Cedulas'];
	$recaudo8 = $rowdia8['Valor'];


	$sqldia9 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."09'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia9 = $db->query($sqldia9);	
	$rowdia9 = $qrydia9->fetch(PDO::FETCH_ASSOC);
	
	$cedulas9 = $rowdia9['Cedulas'];
	$recaudo9 = $rowdia9['Valor'];


		
	$sqldia10 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."10'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia10 = $db->query($sqldia10);	
	$rowdia10 = $qrydia10->fetch(PDO::FETCH_ASSOC);
	
	$cedulas10 = $rowdia10['Cedulas'];
	$recaudo10 = $rowdia10['Valor'];


		
	$sqldia11 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."11'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia11 = $db->query($sqldia11);	
	$rowdia11 = $qrydia11->fetch(PDO::FETCH_ASSOC);
	
	$cedulas11 = $rowdia11['Cedulas'];
	$recaudo11 = $rowdia11['Valor'];


	
	$sqldia12 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."12'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia12 = $db->query($sqldia12);	
	$rowdia12 = $qrydia12->fetch(PDO::FETCH_ASSOC);
	
	$cedulas12 = $rowdia12['Cedulas'];
	$recaudo12 = $rowdia12['Valor'];


	
	$sqldia13 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."13'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia13 = $db->query($sqldia13);	
	$rowdia13 = $qrydia13->fetch(PDO::FETCH_ASSOC);
	
	$cedulas13 = $rowdia13['Cedulas'];
	$recaudo13 = $rowdia13['Valor'];


	
	$sqldia14 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."14'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia14 = $db->query($sqldia14);	
	$rowdia14 = $qrydia14->fetch(PDO::FETCH_ASSOC);
	
	$cedulas14 = $rowdia14['Cedulas'];
	$recaudo14 = $rowdia14['Valor'];


	
	$sqldia15 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."15'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia15 = $db->query($sqldia15);	
	$rowdia15 = $qrydia15->fetch(PDO::FETCH_ASSOC);
	
	$cedulas15 = $rowdia15['Cedulas'];
	$recaudo15 = $rowdia15['Valor'];


	
	$sqldia16 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."16'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia16 = $db->query($sqldia16);	
	$rowdia16 = $qrydia16->fetch(PDO::FETCH_ASSOC);
	
	$cedulas16 = $rowdia16['Cedulas'];
	$recaudo16 = $rowdia16['Valor'];


	
	$sqldia17 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."17'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia17 = $db->query($sqldia17);	
	$rowdia17 = $qrydia17->fetch(PDO::FETCH_ASSOC);
	
	$cedulas17 = $rowdia17['Cedulas'];
	$recaudo17 = $rowdia17['Valor'];


	
	$sqldia18 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."18'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia18 = $db->query($sqldia18);	
	$rowdia18 = $qrydia18->fetch(PDO::FETCH_ASSOC);
	
	$cedulas18 = $rowdia18['Cedulas'];
	$recaudo18 = $rowdia18['Valor'];


	
	$sqldia19 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."19'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia19 = $db->query($sqldia19);	
	$rowdia19 = $qrydia19->fetch(PDO::FETCH_ASSOC);
	
	$cedulas19 = $rowdia19['Cedulas'];
	$recaudo19 = $rowdia19['Valor'];


	
	$sqldia20 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."20'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia20 = $db->query($sqldia20);	
	$rowdia20 = $qrydia20->fetch(PDO::FETCH_ASSOC);
	
	$cedulas20 = $rowdia20['Cedulas'];
	$recaudo20 = $rowdia20['Valor'];


	
	$sqldia21 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."21'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia21 = $db->query($sqldia21);	
	$rowdia21 = $qrydia21->fetch(PDO::FETCH_ASSOC);
	
	$cedulas21 = $rowdia21['Cedulas'];
	$recaudo21 = $rowdia21['Valor'];


	
	$sqldia22 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."22'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia22 = $db->query($sqldia22);	
	$rowdia22 = $qrydia22->fetch(PDO::FETCH_ASSOC);
	
	$cedulas22 = $rowdia22['Cedulas'];
	$recaudo22 = $rowdia22['Valor'];


	
	$sqldia23 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."23'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia23 = $db->query($sqldia23);	
	$rowdia23 = $qrydia23->fetch(PDO::FETCH_ASSOC);
	
	$cedulas23 = $rowdia23['Cedulas'];
	$recaudo23 = $rowdia23['Valor'];


	
	$sqldia24 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."24'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia24 = $db->query($sqldia24);	
	$rowdia24 = $qrydia24->fetch(PDO::FETCH_ASSOC);
	
	$cedulas24 = $rowdia24['Cedulas'];
	$recaudo24 = $rowdia24['Valor'];


	
	$sqldia25 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."25'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia25 = $db->query($sqldia25);	
	$rowdia25 = $qrydia25->fetch(PDO::FETCH_ASSOC);
	
	$cedulas25 = $rowdia25['Cedulas'];
	$recaudo25 = $rowdia25['Valor'];


	
	$sqldia26 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."26'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia26 = $db->query($sqldia26);	
	$rowdia26 = $qrydia26->fetch(PDO::FETCH_ASSOC);
	
	$cedulas26 = $rowdia26['Cedulas'];
	$recaudo26 = $rowdia26['Valor'];


	
	$sqldia27 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."27'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia27 = $db->query($sqldia27);	
	$rowdia27 = $qrydia27->fetch(PDO::FETCH_ASSOC);
	
	$cedulas27 = $rowdia27['Cedulas'];
	$recaudo27 = $rowdia27['Valor'];


	
	$sqldia28 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."28'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia28 = $db->query($sqldia28);	
	$rowdia28 = $qrydia28->fetch(PDO::FETCH_ASSOC);
	
	$cedulas28 = $rowdia28['Cedulas'];
	$recaudo28 = $rowdia28['Valor'];


	
	$sqldia29 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."29'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia29 = $db->query($sqldia29);	
	$rowdia29 = $qrydia29->fetch(PDO::FETCH_ASSOC);
	
	$cedulas29 = $rowdia29['Cedulas'];
	$recaudo29 = $rowdia29['Valor'];


	
	$sqldia30 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."30'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia30 = $db->query($sqldia30);	
	$rowdia30 = $qrydia30->fetch(PDO::FETCH_ASSOC);
	
	$cedulas30 = $rowdia30['Cedulas'];
	$recaudo30 = $rowdia30['Valor'];


	
	$sqldia31 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."31'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia31 = $db->query($sqldia31);	
	$rowdia31 = $qrydia31->fetch(PDO::FETCH_ASSOC);
	
	$cedulas31 = $rowdia31['Cedulas'];
	$recaudo31 = $rowdia31['Valor'];

	$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':BQ'.$i)->applyFromArray($styleArray);
	$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('A'.$i, $row['usunombre'])
    	->setCellValue('B'.$i, $row['comvacedulas'])
		->setCellValue('C'.$i, $row['comvavalor'])
		->setCellValue('D'.$i, $rowrealizado['Cedulas'])
		->setCellValue('E'.$i, $rowrealizado['Valor'])
		->setCellValue('F'.$i, round($porcencedulas,1).' %')
		->setCellValue('G'.$i, round($porcenvalor,1).' %')
		->setCellValue('H'.$i, $cedulas1)
		->setCellValue('I'.$i, $recaudo1)
		->setCellValue('J'.$i, $cedulas2)
		->setCellValue('K'.$i, $recaudo2)	
		->setCellValue('L'.$i, $cedulas3)
		->setCellValue('M'.$i, $recaudo3)	
		->setCellValue('N'.$i, $cedulas4)
		->setCellValue('O'.$i, $recaudo4)	
		->setCellValue('P'.$i, $cedulas5)
		->setCellValue('Q'.$i, $recaudo5)	
		->setCellValue('R'.$i, $cedulas6)
		->setCellValue('S'.$i, $recaudo6)	
		->setCellValue('T'.$i, $cedulas7)
		->setCellValue('U'.$i, $recaudo7)	
		->setCellValue('V'.$i, $cedulas8)
		->setCellValue('W'.$i, $recaudo8)	
		->setCellValue('X'.$i, $cedulas9)
		->setCellValue('Y'.$i, $recaudo9)	
		->setCellValue('Z'.$i, $cedulas10)
		->setCellValue('AA'.$i, $recaudo10)	
		->setCellValue('AB'.$i, $cedulas11)
		->setCellValue('AC'.$i, $recaudo11)	
		->setCellValue('AD'.$i, $cedulas12)
		->setCellValue('AE'.$i, $recaudo12)	
		->setCellValue('AF'.$i, $cedulas13)
		->setCellValue('AG'.$i, $recaudo13)	
		->setCellValue('AH'.$i, $cedulas14)
		->setCellValue('AI'.$i, $recaudo14)	
		->setCellValue('AJ'.$i, $cedulas15)
		->setCellValue('AK'.$i, $recaudo15)	
		->setCellValue('AL'.$i, $cedulas16)
		->setCellValue('AM'.$i, $recaudo16)	
		->setCellValue('AN'.$i, $cedulas17)
		->setCellValue('AO'.$i, $recaudo17)	
		->setCellValue('AP'.$i, $cedulas18)
		->setCellValue('AQ'.$i, $recaudo18)	
		->setCellValue('AR'.$i, $cedulas19)
		->setCellValue('AS'.$i, $recaudo19)	
		->setCellValue('AT'.$i, $cedulas20)
		->setCellValue('AU'.$i, $recaudo20)	
		->setCellValue('AV'.$i, $cedulas21)
		->setCellValue('AW'.$i, $recaudo21)	
		->setCellValue('AX'.$i, $cedulas22)
		->setCellValue('AY'.$i, $recaudo22)	
		->setCellValue('AZ'.$i, $cedulas23)
		->setCellValue('BA'.$i, $recaudo23)	
		->setCellValue('BB'.$i, $cedulas24)
		->setCellValue('BC'.$i, $recaudo24)	
		->setCellValue('BD'.$i, $cedulas25)
		->setCellValue('BE'.$i, $recaudo25)	
		->setCellValue('BF'.$i, $cedulas26)
		->setCellValue('BG'.$i, $recaudo26)	
		->setCellValue('BH'.$i, $cedulas27)
		->setCellValue('BI'.$i, $recaudo27)	
		->setCellValue('BJ'.$i, $cedulas28)
		->setCellValue('BK'.$i, $recaudo28)	
		->setCellValue('BL'.$i, $cedulas29)
		->setCellValue('BM'.$i, $recaudo29)	
		->setCellValue('BN'.$i, $cedulas30)
		->setCellValue('BO'.$i, $recaudo30)	
		->setCellValue('BP'.$i, $cedulas31)
		->setCellValue('BQ'.$i, $recaudo31);	
	$i++;

	$tcedulas = $row['comvacedulas'] + $tcedulas;
	$tvalor = $row['comvavalor'] + $tvalor;
	$tcedurealizado = $rowrealizado['Cedulas'] + $tcedurealizado;
	$tvalorrealizado = $rowrealizado['Valor'] + $tvalorrealizado;
	$tporcencedulas = ($tcedurealizado / $tcedulas) * 100;
	$tporcenvalor = ($tvalorrealizado /  $tvalor) * 100;
	
$tcedulas1 = $rowdia1['Cedulas'] + $tcedulas1;
$trecaudo1 = $rowdia1['Valor'] + $trecaudo1;
$tcedulas2 = $rowdia2['Cedulas'] + $tcedulas2;
$trecaudo2 = $rowdia2['Valor'] + $trecaudo2;
$tcedulas3 = $rowdia3['Cedulas'] + $tcedulas3;
$trecaudo3 = $rowdia3['Valor'] + $trecaudo3;
$tcedulas4 = $rowdia4['Cedulas'] + $tcedulas4;
$trecaudo4 = $rowdia4['Valor'] + $trecaudo4;
$tcedulas5 = $rowdia5['Cedulas'] + $tcedulas5;
$trecaudo5 = $rowdia5['Valor'] + $trecaudo5;
$tcedulas6 = $rowdia6['Cedulas'] + $tcedulas6;
$trecaudo6 = $rowdia6['Valor'] + $trecaudo6;
$tcedulas7 = $rowdia7['Cedulas'] + $tcedulas7;
$trecaudo7 = $rowdia7['Valor'] + $trecaudo7;
$tcedulas8 = $rowdia8['Cedulas'] + $tcedulas8;
$trecaudo8 = $rowdia8['Valor'] + $trecaudo8;
$tcedulas9 = $rowdia9['Cedulas'] + $tcedulas9;
$trecaudo9 = $rowdia9['Valor'] + $trecaudo9;
$tcedulas10 = $rowdia10['Cedulas'] + $tcedulas10;
$trecaudo10 = $rowdia10['Valor'] + $trecaudo10;
$tcedulas11 = $rowdia11['Cedulas'] + $tcedulas11;
$trecaudo11 = $rowdia11['Valor'] + $trecaudo11;
$tcedulas12 = $rowdia12['Cedulas'] + $tcedulas12;
$trecaudo12 = $rowdia12['Valor'] + $trecaudo12;
$tcedulas13 = $rowdia13['Cedulas'] + $tcedulas13;
$trecaudo13 = $rowdia13['Valor'] + $trecaudo13;
$tcedulas14 = $rowdia14['Cedulas'] + $tcedulas14;
$trecaudo14 = $rowdia14['Valor'] + $trecaudo14;
$tcedulas15 = $rowdia15['Cedulas'] + $tcedulas15;
$trecaudo15 = $rowdia15['Valor'] + $trecaudo15;
$tcedulas16 = $rowdia16['Cedulas'] + $tcedulas16;
$trecaudo16 = $rowdia16['Valor'] + $trecaudo16;
$tcedulas17 = $rowdia17['Cedulas'] + $tcedulas17;
$trecaudo17 = $rowdia17['Valor'] + $trecaudo17;
$tcedulas18 = $rowdia18['Cedulas'] + $tcedulas18;
$trecaudo18 = $rowdia18['Valor'] + $trecaudo18;
$tcedulas19 = $rowdia19['Cedulas'] + $tcedulas19;
$trecaudo19 = $rowdia19['Valor'] + $trecaudo19;
$tcedulas20 = $rowdia20['Cedulas'] + $tcedulas20;
$trecaudo20 = $rowdia20['Valor'] + $trecaudo20;
$tcedulas21 = $rowdia21['Cedulas'] + $tcedulas21;
$trecaudo21 = $rowdia21['Valor'] + $trecaudo21;
$tcedulas22 = $rowdia22['Cedulas'] + $tcedulas22;
$trecaudo22 = $rowdia22['Valor'] + $trecaudo22;
$tcedulas23 = $rowdia23['Cedulas'] + $tcedulas23;
$trecaudo23 = $rowdia23['Valor'] + $trecaudo23;
$tcedulas24 = $rowdia24['Cedulas'] + $tcedulas24;
$trecaudo24 = $rowdia24['Valor'] + $trecaudo24;
$tcedulas25 = $rowdia25['Cedulas'] + $tcedulas25;
$trecaudo25 = $rowdia25['Valor'] + $trecaudo25;
$tcedulas26 = $rowdia26['Cedulas'] + $tcedulas26;
$trecaudo26 = $rowdia26['Valor'] + $trecaudo26;
$tcedulas27 = $rowdia27['Cedulas'] + $tcedulas27;
$trecaudo27 = $rowdia27['Valor'] + $trecaudo27;
$tcedulas28 = $rowdia28['Cedulas'] + $tcedulas28;
$trecaudo28 = $rowdia28['Valor'] + $trecaudo28;
$tcedulas29 = $rowdia29['Cedulas'] + $tcedulas29;
$trecaudo29 = $rowdia29['Valor'] + $trecaudo29;
$tcedulas30 = $rowdia30['Cedulas'] + $tcedulas30;
$trecaudo30 = $rowdia30['Valor'] + $trecaudo30;
$tcedulas31 = $rowdia31['Cedulas'] + $tcedulas31;
$trecaudo31 = $rowdia31['Valor'] + $trecaudo31;

$tcedulas1cllcenter =  $tcedulas1;
$trecaudo1cllcenter =  $trecaudo1;
$tcedulas2cllcenter =  $tcedulas2;
$trecaudo2cllcenter =  $trecaudo2;
$tcedulas3cllcenter =  $tcedulas3;
$trecaudo3cllcenter = $trecaudo3;
$tcedulas4cllcenter = $tcedulas4;
$trecaudo4cllcenter = $trecaudo4;
$tcedulas5cllcenter =  $tcedulas5;
$trecaudo5cllcenter =  $trecaudo5;
$tcedulas6cllcenter = $tcedulas6;
$trecaudo6cllcenter =  $trecaudo6;
$tcedulas7cllcenter =  $tcedulas7;
$trecaudo7cllcenter =  $trecaudo7;
$tcedulas8cllcenter = $tcedulas8;
$trecaudo8cllcenter = $trecaudo8;
$tcedulas9cllcenter =  $tcedulas9;
$trecaudo9cllcenter = $trecaudo9;
$tcedulas10cllcenter =  $tcedulas10;
$trecaudo10cllcenter =  $trecaudo10;
$tcedulas11cllcenter =  $tcedulas11;
$trecaudo11cllcenter =  $trecaudo11;
$tcedulas12cllcenter =  $tcedulas12;
$trecaudo12cllcenter =  $trecaudo12;
$tcedulas13cllcenter =  $tcedulas13;
$trecaudo13cllcenter =  $trecaudo13;
$tcedulas14cllcenter =  $tcedulas14;
$trecaudo14cllcenter =  $trecaudo14;
$tcedulas15cllcenter =  $tcedulas15;
$trecaudo15cllcenter =  $trecaudo15;
$tcedulas16cllcenter =  $tcedulas16;
$trecaudo16cllcenter = $trecaudo16;
$tcedulas17cllcenter =  $tcedulas17;
$trecaudo17cllcenter = $trecaudo17;
$tcedulas18cllcenter =  $tcedulas18;
$trecaudo18cllcenter =  $trecaudo18;
$tcedulas19cllcenter = $tcedulas19;
$trecaudo19cllcenter = $trecaudo19;
$tcedulas20cllcenter =  $tcedulas20;
$trecaudo20cllcenter = $trecaudo20;
$tcedulas21cllcenter = $tcedulas21;
$trecaudo21cllcenter = $trecaudo21;
$tcedulas22cllcenter =  $tcedulas22;
$trecaudo22cllcenter = $trecaudo22;
$tcedulas23cllcenter =  $tcedulas23;
$trecaudo23cllcenter =  $trecaudo23;
$tcedulas24cllcenter =  $tcedulas24;
$trecaudo24cllcenter =  $trecaudo24;
$tcedulas25cllcenter = $tcedulas25;
$trecaudo25cllcenter = $trecaudo25;
$tcedulas26cllcenter = $tcedulas26;
$trecaudo26cllcenter =  $trecaudo26;
$tcedulas27cllcenter =  $tcedulas27;
$trecaudo27cllcenter =  $trecaudo27;
$tcedulas28cllcenter = $tcedulas28;
$trecaudo28cllcenter =  $trecaudo28;
$tcedulas29cllcenter =  $tcedulas29;
$trecaudo29cllcenter = $trecaudo29;
$tcedulas30cllcenter = $tcedulas30;
$trecaudo30cllcenter =  $trecaudo30;
$tcedulas31cllcenter = $tcedulas31;
$trecaudo31cllcenter = $trecaudo31;
$tcedulascllcenter = $tcedulas;
$tvalorcllcenter = $tvlor;
$tcedurealizadocllcenter = $tcedurealizado;
$tvalorrealizadocllcenter = $tvalorrealizado;


}
}
	$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':BQ'.$i)->applyFromArray($styleArrayTwo);
	$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('A'.$i, 'TOTAL')
    	->setCellValue('B'.$i, $tcedulas)
		->setCellValue('C'.$i, $tvalor)
		->setCellValue('D'.$i, $tcedurealizado)
		->setCellValue('E'.$i, $tvalorrealizado)
		->setCellValue('F'.$i, round($tporcencedulas,1).' %')
		->setCellValue('G'.$i, round($tporcenvalor,1).' %')
		->setCellValue('H'.$i, $tcedulas1)
		->setCellValue('I'.$i, $trecaudo1)
		->setCellValue('J'.$i, $tcedulas2)
		->setCellValue('K'.$i, $trecaudo2)	
		->setCellValue('L'.$i, $tcedulas3)
		->setCellValue('M'.$i, $trecaudo3)	
		->setCellValue('N'.$i, $tcedulas4)
		->setCellValue('O'.$i, $trecaudo4)	
		->setCellValue('P'.$i, $tcedulas5)
		->setCellValue('Q'.$i, $trecaudo5)	
		->setCellValue('R'.$i, $tcedulas6)
		->setCellValue('S'.$i, $trecaudo6)	
		->setCellValue('T'.$i, $tcedulas7)
		->setCellValue('U'.$i, $trecaudo7)	
		->setCellValue('V'.$i, $tcedulas8)
		->setCellValue('W'.$i, $trecaudo8)	
		->setCellValue('X'.$i, $tcedulas9)
		->setCellValue('Y'.$i, $trecaudo9)	
		->setCellValue('Z'.$i, $tcedulas10)
		->setCellValue('AA'.$i, $trecaudo10)	
		->setCellValue('AB'.$i, $tcedulas11)
		->setCellValue('AC'.$i, $trecaudo11)	
		->setCellValue('AD'.$i, $tcedulas12)
		->setCellValue('AE'.$i, $trecaudo12)	
		->setCellValue('AF'.$i, $tcedulas13)
		->setCellValue('AG'.$i, $trecaudo13)	
		->setCellValue('AH'.$i, $tcedulas14)
		->setCellValue('AI'.$i, $trecaudo14)	
		->setCellValue('AJ'.$i, $tcedulas15)
		->setCellValue('AK'.$i, $trecaudo15)	
		->setCellValue('AL'.$i, $tcedulas16)
		->setCellValue('AM'.$i, $trecaudo16)	
		->setCellValue('AN'.$i, $tcedulas17)
		->setCellValue('AO'.$i, $trecaudo17)	
		->setCellValue('AP'.$i, $tcedulas18)
		->setCellValue('AQ'.$i, $trecaudo18)	
		->setCellValue('AR'.$i, $tcedulas19)
		->setCellValue('AS'.$i, $trecaudo19)	
		->setCellValue('AT'.$i, $tcedulas20)
		->setCellValue('AU'.$i, $trecaudo20)	
		->setCellValue('AV'.$i, $tcedulas21)
		->setCellValue('AW'.$i, $trecaudo21)	
		->setCellValue('AX'.$i, $tcedulas22)
		->setCellValue('AY'.$i, $trecaudo22)	
		->setCellValue('AZ'.$i, $tcedulas23)
		->setCellValue('BA'.$i, $trecaudo23)	
		->setCellValue('BB'.$i, $tcedulas24)
		->setCellValue('BC'.$i, $trecaudo24)	
		->setCellValue('BD'.$i, $tcedulas25)
		->setCellValue('BE'.$i, $trecaudo25)	
		->setCellValue('BF'.$i, $tcedulas26)
		->setCellValue('BG'.$i, $trecaudo26)	
		->setCellValue('BH'.$i, $tcedulas27)
		->setCellValue('BI'.$i, $trecaudo27)	
		->setCellValue('BJ'.$i, $tcedulas28)
		->setCellValue('BK'.$i, $trecaudo28)	
		->setCellValue('BL'.$i, $tcedulas29)
		->setCellValue('BM'.$i, $trecaudo29)	
		->setCellValue('BN'.$i, $tcedulas30)
		->setCellValue('BO'.$i, $trecaudo30)	
		->setCellValue('BP'.$i, $tcedulas31)
		->setCellValue('BQ'.$i, $trecaudo31);	
	$i++;	

// ----------------------------------------------------------------------------------------------------------


$tcedulas1  = 0;
$tcedulas2  = 0;
$tcedulas3  = 0;
$tcedulas4  = 0;
$tcedulas5  = 0;
$tcedulas6  = 0;
$tcedulas7  = 0;
$tcedulas8  = 0;
$tcedulas9  = 0;
$tcedulas10  = 0;
$tcedulas11  = 0;
$tcedulas12  = 0;
$tcedulas13  = 0;
$tcedulas14  = 0;
$tcedulas15  = 0;
$tcedulas16  = 0;
$tcedulas17  = 0;
$tcedulas18  = 0;
$tcedulas19  = 0;
$tcedulas20  = 0;
$tcedulas21  = 0;
$tcedulas22  = 0;
$tcedulas23  = 0;
$tcedulas24  = 0;
$tcedulas25  = 0;
$tcedulas26  = 0;
$tcedulas27  = 0;
$tcedulas28  = 0;
$tcedulas29  = 0;
$tcedulas30  = 0;
$tcedulas31  = 0;
$trecaudo1  = 0;
$trecaudo2  = 0;
$trecaudo3  = 0;
$trecaudo4  = 0;
$trecaudo5  = 0;
$trecaudo6  = 0;
$trecaudo7  = 0;
$trecaudo8  = 0;
$trecaudo9  = 0;
$trecaudo10  = 0;
$trecaudo11  = 0;
$trecaudo12  = 0;
$trecaudo13  = 0;
$trecaudo14  = 0;
$trecaudo15  = 0;
$trecaudo16  = 0;
$trecaudo17  = 0;
$trecaudo18  = 0;
$trecaudo19  = 0;
$trecaudo20  = 0;
$trecaudo21  = 0;
$trecaudo22  = 0;
$trecaudo23  = 0;
$trecaudo24  = 0;
$trecaudo25  = 0;
$trecaudo26  = 0;
$trecaudo27  = 0;
$trecaudo28  = 0;
$trecaudo29  = 0;
$trecaudo30  = 0;
$trecaudo31  = 0;
$tcedulas = 0;
$tvalor = 0;
$tcedurealizado = 0;
$tvalorrealizado = 0;
$tporcencedulas = 0;
$tporcenvalor = 0;

	
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$i.':C'.$i);
$objPHPExcel->getActiveSheet()->getStyle('A'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$i, 'CONCILIACION 1 - 30');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('D'.$i.':E'.$i);
$objPHPExcel->getActiveSheet()->getStyle('D'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$i, 'REALIZADO');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('F'.$i.':G'.$i);
$objPHPExcel->getActiveSheet()->getStyle('F'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$i, ' % REALIZADO');
 
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('H'.$i.':I'.$i);
$objPHPExcel->getActiveSheet()->getStyle('H'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$i, '1');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('J'.$i.':K'.$i);
$objPHPExcel->getActiveSheet()->getStyle('J'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.$i, '2');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('L'.$i.':M'.$i);
$objPHPExcel->getActiveSheet()->getStyle('L'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('L'.$i, '3');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('N'.$i.':O'.$i);
$objPHPExcel->getActiveSheet()->getStyle('N'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('N'.$i, '4');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('P'.$i.':Q'.$i);
$objPHPExcel->getActiveSheet()->getStyle('P'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('P'.$i, '5');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('R'.$i.':S'.$i);
$objPHPExcel->getActiveSheet()->getStyle('L'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('R'.$i, '6');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('T'.$i.':U'.$i);
$objPHPExcel->getActiveSheet()->getStyle('T'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('T'.$i, '7');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('V'.$i.':W'.$i);
$objPHPExcel->getActiveSheet()->getStyle('V'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('V'.$i, '8');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('X'.$i.':Y'.$i);
$objPHPExcel->getActiveSheet()->getStyle('X'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('X'.$i, '9');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('Z'.$i.':AA'.$i);
$objPHPExcel->getActiveSheet()->getStyle('Z'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('Z'.$i, '10');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AB'.$i.':AC'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AB'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AB'.$i, '11');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AB'.$i.':AC'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AB'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AB'.$i, '11');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AD'.$i.':AE'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AD'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AD'.$i, '12');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AF'.$i.':AG'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AF'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AF'.$i, '13');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AH'.$i.':AI'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AH'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AH'.$i, '14');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AJ'.$i.':AK'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AJ'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AJ'.$i, '15');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AL'.$i.':AM'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AL'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AL'.$i, '16');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AN'.$i.':AO'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AN'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AN'.$i, '17');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AP'.$i.':AQ'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AP'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AP'.$i, '18');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AR'.$i.':AS'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AR'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AR'.$i, '19');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AT'.$i.':AU'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AT'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AT'.$i, '20');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AV'.$i.':AW'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AV'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AV'.$i, '21');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AX'.$i.':AY'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AX'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AX'.$i, '22');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AZ'.$i.':BA'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AZ'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AZ'.$i, '23');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BB'.$i.':BC'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BB'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BB'.$i, '24');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BD'.$i.':BE'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BD'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BD'.$i, '25');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BF'.$i.':BG'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BF'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BF'.$i, '26');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BH'.$i.':BI'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BH'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BH'.$i, '27');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BJ'.$i.':BK'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BJ'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BJ'.$i, '28');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BL'.$i.':BM'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BL'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BL'.$i, '29');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BN'.$i.':BO'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BN'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BN'.$i, '30');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BP'.$i.':BQ'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BP'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BP'.$i, '31');


$i++;
$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':BQ'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$i, 'ASESOR')
            ->setCellValue('B'.$i, 'CEDULAS')
            ->setCellValue('C'.$i, 'VALOR')
            ->setCellValue('D'.$i, 'CEDULAS')
			->setCellValue('E'.$i, 'VALOR')
			->setCellValue('F'.$i, 'CEDULAS')
			->setCellValue('G'.$i, 'VALOR')	
			->setCellValue('H'.$i, 'CEDULAS')
			->setCellValue('I'.$i, 'RECAUDO')
			->setCellValue('J'.$i, 'CEDULAS')
			->setCellValue('K'.$i, 'RECAUDO')	
			->setCellValue('L'.$i, 'CEDULAS')
			->setCellValue('M'.$i, 'RECAUDO')	
			->setCellValue('N'.$i, 'CEDULAS')
			->setCellValue('O'.$i, 'RECAUDO')	
			->setCellValue('P'.$i, 'CEDULAS')
			->setCellValue('Q'.$i, 'RECAUDO')	
			->setCellValue('R'.$i, 'CEDULAS')
			->setCellValue('S'.$i, 'RECAUDO')	
			->setCellValue('T'.$i, 'CEDULAS')
			->setCellValue('U'.$i, 'RECAUDO')	
			->setCellValue('V'.$i, 'CEDULAS')
			->setCellValue('W'.$i, 'RECAUDO')	
			->setCellValue('X'.$i, 'CEDULAS')
			->setCellValue('Y'.$i, 'RECAUDO')	
			->setCellValue('Z'.$i, 'CEDULAS')
			->setCellValue('AA'.$i, 'RECAUDO')	
			->setCellValue('AB'.$i, 'CEDULAS')
			->setCellValue('AC'.$i, 'RECAUDO')	
			->setCellValue('AD'.$i, 'CEDULAS')
			->setCellValue('AE'.$i, 'RECAUDO')	
			->setCellValue('AF'.$i, 'CEDULAS')
			->setCellValue('AG'.$i, 'RECAUDO')	
			->setCellValue('AH'.$i, 'CEDULAS')
			->setCellValue('AI'.$i, 'RECAUDO')	
			->setCellValue('AJ'.$i, 'CEDULAS')
			->setCellValue('AK'.$i, 'RECAUDO')	
			->setCellValue('AL'.$i, 'CEDULAS')
			->setCellValue('AM'.$i, 'RECAUDO')	
			->setCellValue('AN'.$i, 'CEDULAS')
			->setCellValue('AO'.$i, 'RECAUDO')	
			->setCellValue('AP'.$i, 'CEDULAS')
			->setCellValue('AQ'.$i, 'RECAUDO')	
			->setCellValue('AR'.$i, 'CEDULAS')
			->setCellValue('AS'.$i, 'RECAUDO')	
			->setCellValue('AT'.$i, 'CEDULAS')
			->setCellValue('AU'.$i, 'RECAUDO')	
			->setCellValue('AV'.$i, 'CEDULAS')
			->setCellValue('AW'.$i, 'RECAUDO')	
			->setCellValue('AX'.$i, 'CEDULAS')
			->setCellValue('AY'.$i, 'RECAUDO')	
			->setCellValue('AZ'.$i, 'CEDULAS')
			->setCellValue('BA'.$i, 'RECAUDO')	
			->setCellValue('BB'.$i, 'CEDULAS')
			->setCellValue('BC'.$i, 'RECAUDO')	
			->setCellValue('BD'.$i, 'CEDULAS')
			->setCellValue('BE'.$i, 'RECAUDO')	
			->setCellValue('BF'.$i, 'CEDULAS')
			->setCellValue('BG'.$i, 'RECAUDO')	
			->setCellValue('BH'.$i, 'CEDULAS')
			->setCellValue('BI'.$i, 'RECAUDO')	
			->setCellValue('BJ'.$i, 'CEDULAS')
			->setCellValue('BK'.$i, 'RECAUDO')	
			->setCellValue('BL'.$i, 'CEDULAS')
			->setCellValue('BM'.$i, 'RECAUDO')	
			->setCellValue('BN'.$i, 'CEDULAS')
			->setCellValue('BO'.$i, 'RECAUDO')	
			->setCellValue('BP'.$i, 'CEDULAS')
			->setCellValue('BQ'.$i, 'RECAUDO');
			$i++;	

$sql = "SELECT * FROM comoVamos INNER JOIN usuarios ON usuid = comvausuario INNER JOIN comovamosgrupodeta ON grupousuario = comvausuario WHERE comvafecha = '$fecha' AND grupofecha = '$fecha';";
$qry = $db->query($sql);			

while($row = $qry->fetch(PDO::FETCH_ASSOC)){
if ($row['gruponombre'] == 'CONCILIACION 1 - 30') {
	
	$sqlrealizado = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha >= '$fecha'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";

	$qryrealizado = $db->query($sqlrealizado);	
	$rowrealizado = $qryrealizado->fetch(PDO::FETCH_ASSOC);	
	
	$porcencedulas = ($rowrealizado['Cedulas'] / $row['comvacedulas']) * 100;
	$porcenvalor = ($rowrealizado['Valor'] /  $row['comvavalor']) * 100;
	
	$fechasindias = substr($fecha, 0, -2);
	

	$sqldia1 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."01'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia1 = $db->query($sqldia1);	
	$rowdia1 = $qrydia1->fetch(PDO::FETCH_ASSOC);
	
	$cedulas1 = $rowdia1['Cedulas'];
	$recaudo1 = $rowdia1['Valor'];
	
	
	$sqldia2 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."02'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia2 = $db->query($sqldia2);	
	$rowdia2 = $qrydia2->fetch(PDO::FETCH_ASSOC);
	
	$cedulas2 = $rowdia2['Cedulas'];
	$recaudo2 = $rowdia2['Valor'];


	$sqldia3 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."03'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia3 = $db->query($sqldia3);	
	$rowdia3 = $qrydia3->fetch(PDO::FETCH_ASSOC);
	
	$cedulas3 = $rowdia3['Cedulas'];
	$recaudo3 = $rowdia3['Valor'];


	$sqldia4 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."04'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia4 = $db->query($sqldia4);	
	$rowdia4 = $qrydia4->fetch(PDO::FETCH_ASSOC);
	
	$cedulas4 = $rowdia4['Cedulas'];
	$recaudo4 = $rowdia4['Valor'];


	$sqldia5 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."05'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia5 = $db->query($sqldia5);	
	$rowdia5 = $qrydia5->fetch(PDO::FETCH_ASSOC);
	
	$cedulas5 = $rowdia5['Cedulas'];
	$recaudo5 = $rowdia5['Valor'];


	$sqldia6 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."06'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia6 = $db->query($sqldia6);	
	$rowdia6 = $qrydia6->fetch(PDO::FETCH_ASSOC);
	
	$cedulas6 = $rowdia6['Cedulas'];
	$recaudo6 = $rowdia6['Valor'];


	$sqldia7 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."07'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia7 = $db->query($sqldia7);	
	$rowdia7 = $qrydia7->fetch(PDO::FETCH_ASSOC);
	
	$cedulas7 = $rowdia7['Cedulas'];
	$recaudo7 = $rowdia7['Valor'];


	$sqldia8 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."08'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia8 = $db->query($sqldia8);	
	$rowdia8 = $qrydia8->fetch(PDO::FETCH_ASSOC);
	
	$cedulas8 = $rowdia8['Cedulas'];
	$recaudo8 = $rowdia8['Valor'];


	$sqldia9 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."09'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia9 = $db->query($sqldia9);	
	$rowdia9 = $qrydia9->fetch(PDO::FETCH_ASSOC);
	
	$cedulas9 = $rowdia9['Cedulas'];
	$recaudo9 = $rowdia9['Valor'];


		
	$sqldia10 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."10'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia10 = $db->query($sqldia10);	
	$rowdia10 = $qrydia10->fetch(PDO::FETCH_ASSOC);
	
	$cedulas10 = $rowdia10['Cedulas'];
	$recaudo10 = $rowdia10['Valor'];


		
	$sqldia11 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."11'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia11 = $db->query($sqldia11);	
	$rowdia11 = $qrydia11->fetch(PDO::FETCH_ASSOC);
	
	$cedulas11 = $rowdia11['Cedulas'];
	$recaudo11 = $rowdia11['Valor'];


	
	$sqldia12 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."12'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia12 = $db->query($sqldia12);	
	$rowdia12 = $qrydia12->fetch(PDO::FETCH_ASSOC);
	
	$cedulas12 = $rowdia12['Cedulas'];
	$recaudo12 = $rowdia12['Valor'];


	
	$sqldia13 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."13'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia13 = $db->query($sqldia13);	
	$rowdia13 = $qrydia13->fetch(PDO::FETCH_ASSOC);
	
	$cedulas13 = $rowdia13['Cedulas'];
	$recaudo13 = $rowdia13['Valor'];


	
	$sqldia14 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."14'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia14 = $db->query($sqldia14);	
	$rowdia14 = $qrydia14->fetch(PDO::FETCH_ASSOC);
	
	$cedulas14 = $rowdia14['Cedulas'];
	$recaudo14 = $rowdia14['Valor'];


	
	$sqldia15 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."15'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia15 = $db->query($sqldia15);	
	$rowdia15 = $qrydia15->fetch(PDO::FETCH_ASSOC);
	
	$cedulas15 = $rowdia15['Cedulas'];
	$recaudo15 = $rowdia15['Valor'];


	
	$sqldia16 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."16'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia16 = $db->query($sqldia16);	
	$rowdia16 = $qrydia16->fetch(PDO::FETCH_ASSOC);
	
	$cedulas16 = $rowdia16['Cedulas'];
	$recaudo16 = $rowdia16['Valor'];


	
	$sqldia17 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."17'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia17 = $db->query($sqldia17);	
	$rowdia17 = $qrydia17->fetch(PDO::FETCH_ASSOC);
	
	$cedulas17 = $rowdia17['Cedulas'];
	$recaudo17 = $rowdia17['Valor'];


	
	$sqldia18 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."18'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia18 = $db->query($sqldia18);	
	$rowdia18 = $qrydia18->fetch(PDO::FETCH_ASSOC);
	
	$cedulas18 = $rowdia18['Cedulas'];
	$recaudo18 = $rowdia18['Valor'];


	
	$sqldia19 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."19'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia19 = $db->query($sqldia19);	
	$rowdia19 = $qrydia19->fetch(PDO::FETCH_ASSOC);
	
	$cedulas19 = $rowdia19['Cedulas'];
	$recaudo19 = $rowdia19['Valor'];


	
	$sqldia20 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."20'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia20 = $db->query($sqldia20);	
	$rowdia20 = $qrydia20->fetch(PDO::FETCH_ASSOC);
	
	$cedulas20 = $rowdia20['Cedulas'];
	$recaudo20 = $rowdia20['Valor'];


	
	$sqldia21 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."21'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia21 = $db->query($sqldia21);	
	$rowdia21 = $qrydia21->fetch(PDO::FETCH_ASSOC);
	
	$cedulas21 = $rowdia21['Cedulas'];
	$recaudo21 = $rowdia21['Valor'];


	
	$sqldia22 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."22'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia22 = $db->query($sqldia22);	
	$rowdia22 = $qrydia22->fetch(PDO::FETCH_ASSOC);
	
	$cedulas22 = $rowdia22['Cedulas'];
	$recaudo22 = $rowdia22['Valor'];


	
	$sqldia23 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."23'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia23 = $db->query($sqldia23);	
	$rowdia23 = $qrydia23->fetch(PDO::FETCH_ASSOC);
	
	$cedulas23 = $rowdia23['Cedulas'];
	$recaudo23 = $rowdia23['Valor'];


	
	$sqldia24 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."24'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia24 = $db->query($sqldia24);	
	$rowdia24 = $qrydia24->fetch(PDO::FETCH_ASSOC);
	
	$cedulas24 = $rowdia24['Cedulas'];
	$recaudo24 = $rowdia24['Valor'];


	
	$sqldia25 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."25'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia25 = $db->query($sqldia25);	
	$rowdia25 = $qrydia25->fetch(PDO::FETCH_ASSOC);
	
	$cedulas25 = $rowdia25['Cedulas'];
	$recaudo25 = $rowdia25['Valor'];


	
	$sqldia26 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."26'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia26 = $db->query($sqldia26);	
	$rowdia26 = $qrydia26->fetch(PDO::FETCH_ASSOC);
	
	$cedulas26 = $rowdia26['Cedulas'];
	$recaudo26 = $rowdia26['Valor'];


	
	$sqldia27 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."27'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia27 = $db->query($sqldia27);	
	$rowdia27 = $qrydia27->fetch(PDO::FETCH_ASSOC);
	
	$cedulas27 = $rowdia27['Cedulas'];
	$recaudo27 = $rowdia27['Valor'];


	
	$sqldia28 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."28'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia28 = $db->query($sqldia28);	
	$rowdia28 = $qrydia28->fetch(PDO::FETCH_ASSOC);
	
	$cedulas28 = $rowdia28['Cedulas'];
	$recaudo28 = $rowdia28['Valor'];


	
	$sqldia29 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."29'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia29 = $db->query($sqldia29);	
	$rowdia29 = $qrydia29->fetch(PDO::FETCH_ASSOC);
	
	$cedulas29 = $rowdia29['Cedulas'];
	$recaudo29 = $rowdia29['Valor'];


	
	$sqldia30 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."30'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia30 = $db->query($sqldia30);	
	$rowdia30 = $qrydia30->fetch(PDO::FETCH_ASSOC);
	
	$cedulas30 = $rowdia30['Cedulas'];
	$recaudo30 = $rowdia30['Valor'];


	
	$sqldia31 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."31'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia31 = $db->query($sqldia31);	
	$rowdia31 = $qrydia31->fetch(PDO::FETCH_ASSOC);
	
	$cedulas31 = $rowdia31['Cedulas'];
	$recaudo31 = $rowdia31['Valor'];

	$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':BQ'.$i)->applyFromArray($styleArray);
	$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('A'.$i, $row['usunombre'])
    	->setCellValue('B'.$i, $row['comvacedulas'])
		->setCellValue('C'.$i, $row['comvavalor'])
		->setCellValue('D'.$i, $rowrealizado['Cedulas'])
		->setCellValue('E'.$i, $rowrealizado['Valor'])
		->setCellValue('F'.$i, round($porcencedulas,1).' %')
		->setCellValue('G'.$i, round($porcenvalor,1).' %')
		->setCellValue('H'.$i, $cedulas1)
		->setCellValue('I'.$i, $recaudo1)
		->setCellValue('J'.$i, $cedulas2)
		->setCellValue('K'.$i, $recaudo2)	
		->setCellValue('L'.$i, $cedulas3)
		->setCellValue('M'.$i, $recaudo3)	
		->setCellValue('N'.$i, $cedulas4)
		->setCellValue('O'.$i, $recaudo4)	
		->setCellValue('P'.$i, $cedulas5)
		->setCellValue('Q'.$i, $recaudo5)	
		->setCellValue('R'.$i, $cedulas6)
		->setCellValue('S'.$i, $recaudo6)	
		->setCellValue('T'.$i, $cedulas7)
		->setCellValue('U'.$i, $recaudo7)	
		->setCellValue('V'.$i, $cedulas8)
		->setCellValue('W'.$i, $recaudo8)	
		->setCellValue('X'.$i, $cedulas9)
		->setCellValue('Y'.$i, $recaudo9)	
		->setCellValue('Z'.$i, $cedulas10)
		->setCellValue('AA'.$i, $recaudo10)	
		->setCellValue('AB'.$i, $cedulas11)
		->setCellValue('AC'.$i, $recaudo11)	
		->setCellValue('AD'.$i, $cedulas12)
		->setCellValue('AE'.$i, $recaudo12)	
		->setCellValue('AF'.$i, $cedulas13)
		->setCellValue('AG'.$i, $recaudo13)	
		->setCellValue('AH'.$i, $cedulas14)
		->setCellValue('AI'.$i, $recaudo14)	
		->setCellValue('AJ'.$i, $cedulas15)
		->setCellValue('AK'.$i, $recaudo15)	
		->setCellValue('AL'.$i, $cedulas16)
		->setCellValue('AM'.$i, $recaudo16)	
		->setCellValue('AN'.$i, $cedulas17)
		->setCellValue('AO'.$i, $recaudo17)	
		->setCellValue('AP'.$i, $cedulas18)
		->setCellValue('AQ'.$i, $recaudo18)	
		->setCellValue('AR'.$i, $cedulas19)
		->setCellValue('AS'.$i, $recaudo19)	
		->setCellValue('AT'.$i, $cedulas20)
		->setCellValue('AU'.$i, $recaudo20)	
		->setCellValue('AV'.$i, $cedulas21)
		->setCellValue('AW'.$i, $recaudo21)	
		->setCellValue('AX'.$i, $cedulas22)
		->setCellValue('AY'.$i, $recaudo22)	
		->setCellValue('AZ'.$i, $cedulas23)
		->setCellValue('BA'.$i, $recaudo23)	
		->setCellValue('BB'.$i, $cedulas24)
		->setCellValue('BC'.$i, $recaudo24)	
		->setCellValue('BD'.$i, $cedulas25)
		->setCellValue('BE'.$i, $recaudo25)	
		->setCellValue('BF'.$i, $cedulas26)
		->setCellValue('BG'.$i, $recaudo26)	
		->setCellValue('BH'.$i, $cedulas27)
		->setCellValue('BI'.$i, $recaudo27)	
		->setCellValue('BJ'.$i, $cedulas28)
		->setCellValue('BK'.$i, $recaudo28)	
		->setCellValue('BL'.$i, $cedulas29)
		->setCellValue('BM'.$i, $recaudo29)	
		->setCellValue('BN'.$i, $cedulas30)
		->setCellValue('BO'.$i, $recaudo30)	
		->setCellValue('BP'.$i, $cedulas31)
		->setCellValue('BQ'.$i, $recaudo31);	
	$i++;

	$tcedulas = $row['comvacedulas'] + $tcedulas;
	$tvalor = $row['comvavalor'] + $tvalor;
	$tcedurealizado = $rowrealizado['Cedulas'] + $tcedurealizado;
	$tvalorrealizado = $rowrealizado['Valor'] + $tvalorrealizado;
	$tporcencedulas = ($tcedurealizado / $tcedulas) * 100;
	$tporcenvalor = ($tvalorrealizado /  $tvalor) * 100;
	
$tcedulas1 = $rowdia1['Cedulas'] + $tcedulas1;
$trecaudo1 = $rowdia1['Valor'] + $trecaudo1;
$tcedulas2 = $rowdia2['Cedulas'] + $tcedulas2;
$trecaudo2 = $rowdia2['Valor'] + $trecaudo2;
$tcedulas3 = $rowdia3['Cedulas'] + $tcedulas3;
$trecaudo3 = $rowdia3['Valor'] + $trecaudo3;
$tcedulas4 = $rowdia4['Cedulas'] + $tcedulas4;
$trecaudo4 = $rowdia4['Valor'] + $trecaudo4;
$tcedulas5 = $rowdia5['Cedulas'] + $tcedulas5;
$trecaudo5 = $rowdia5['Valor'] + $trecaudo5;
$tcedulas6 = $rowdia6['Cedulas'] + $tcedulas6;
$trecaudo6 = $rowdia6['Valor'] + $trecaudo6;
$tcedulas7 = $rowdia7['Cedulas'] + $tcedulas7;
$trecaudo7 = $rowdia7['Valor'] + $trecaudo7;
$tcedulas8 = $rowdia8['Cedulas'] + $tcedulas8;
$trecaudo8 = $rowdia8['Valor'] + $trecaudo8;
$tcedulas9 = $rowdia9['Cedulas'] + $tcedulas9;
$trecaudo9 = $rowdia9['Valor'] + $trecaudo9;
$tcedulas10 = $rowdia10['Cedulas'] + $tcedulas10;
$trecaudo10 = $rowdia10['Valor'] + $trecaudo10;
$tcedulas11 = $rowdia11['Cedulas'] + $tcedulas11;
$trecaudo11 = $rowdia11['Valor'] + $trecaudo11;
$tcedulas12 = $rowdia12['Cedulas'] + $tcedulas12;
$trecaudo12 = $rowdia12['Valor'] + $trecaudo12;
$tcedulas13 = $rowdia13['Cedulas'] + $tcedulas13;
$trecaudo13 = $rowdia13['Valor'] + $trecaudo13;
$tcedulas14 = $rowdia14['Cedulas'] + $tcedulas14;
$trecaudo14 = $rowdia14['Valor'] + $trecaudo14;
$tcedulas15 = $rowdia15['Cedulas'] + $tcedulas15;
$trecaudo15 = $rowdia15['Valor'] + $trecaudo15;
$tcedulas16 = $rowdia16['Cedulas'] + $tcedulas16;
$trecaudo16 = $rowdia16['Valor'] + $trecaudo16;
$tcedulas17 = $rowdia17['Cedulas'] + $tcedulas17;
$trecaudo17 = $rowdia17['Valor'] + $trecaudo17;
$tcedulas18 = $rowdia18['Cedulas'] + $tcedulas18;
$trecaudo18 = $rowdia18['Valor'] + $trecaudo18;
$tcedulas19 = $rowdia19['Cedulas'] + $tcedulas19;
$trecaudo19 = $rowdia19['Valor'] + $trecaudo19;
$tcedulas20 = $rowdia20['Cedulas'] + $tcedulas20;
$trecaudo20 = $rowdia20['Valor'] + $trecaudo20;
$tcedulas21 = $rowdia21['Cedulas'] + $tcedulas21;
$trecaudo21 = $rowdia21['Valor'] + $trecaudo21;
$tcedulas22 = $rowdia22['Cedulas'] + $tcedulas22;
$trecaudo22 = $rowdia22['Valor'] + $trecaudo22;
$tcedulas23 = $rowdia23['Cedulas'] + $tcedulas23;
$trecaudo23 = $rowdia23['Valor'] + $trecaudo23;
$tcedulas24 = $rowdia24['Cedulas'] + $tcedulas24;
$trecaudo24 = $rowdia24['Valor'] + $trecaudo24;
$tcedulas25 = $rowdia25['Cedulas'] + $tcedulas25;
$trecaudo25 = $rowdia25['Valor'] + $trecaudo25;
$tcedulas26 = $rowdia26['Cedulas'] + $tcedulas26;
$trecaudo26 = $rowdia26['Valor'] + $trecaudo26;
$tcedulas27 = $rowdia27['Cedulas'] + $tcedulas27;
$trecaudo27 = $rowdia27['Valor'] + $trecaudo27;
$tcedulas28 = $rowdia28['Cedulas'] + $tcedulas28;
$trecaudo28 = $rowdia28['Valor'] + $trecaudo28;
$tcedulas29 = $rowdia29['Cedulas'] + $tcedulas29;
$trecaudo29 = $rowdia29['Valor'] + $trecaudo29;
$tcedulas30 = $rowdia30['Cedulas'] + $tcedulas30;
$trecaudo30 = $rowdia30['Valor'] + $trecaudo30;
$tcedulas31 = $rowdia31['Cedulas'] + $tcedulas31;
$trecaudo31 = $rowdia31['Valor'] + $trecaudo31;

	
$tcedulas1conciliacion =  $tcedulas1;
$trecaudo1conciliacion =  $trecaudo1;
$tcedulas2conciliacion = $tcedulas2;
$trecaudo2conciliacion =  $trecaudo2;
$tcedulas3conciliacion =  $tcedulas3;
$trecaudo3conciliacion = $trecaudo3;
$tcedulas4conciliacion =  $tcedulas4;
$trecaudo4conciliacion =  $trecaudo4;
$tcedulas5conciliacion =  $tcedulas5;
$trecaudo5conciliacion = $trecaudo5;
$tcedulas6conciliacion = $tcedulas6;
$trecaudo6conciliacion = $trecaudo6;
$tcedulas7conciliacion =  $tcedulas7;
$trecaudo7conciliacion =  $trecaudo7;
$tcedulas8conciliacion = $tcedulas8;
$trecaudo8conciliacion = $trecaudo8;
$tcedulas9conciliacion =  $tcedulas9;
$trecaudo9conciliacion = $trecaudo9;
$tcedulas10conciliacion = $tcedulas10;
$trecaudo10conciliacion =  $trecaudo10;
$tcedulas11conciliacion = $tcedulas11;
$trecaudo11conciliacion = $trecaudo11;
$tcedulas12conciliacion =  $tcedulas12;
$trecaudo12conciliacion =  $trecaudo12;
$tcedulas13conciliacion =  $tcedulas13;
$trecaudo13conciliacion = $trecaudo13;
$tcedulas14conciliacion = $tcedulas14;
$trecaudo14conciliacion =  $trecaudo14;
$tcedulas15conciliacion =  $tcedulas15;
$trecaudo15conciliacion =  $trecaudo15;
$tcedulas16conciliacion =  $tcedulas16;
$trecaudo16conciliacion =  $trecaudo16;
$tcedulas17conciliacion =  $tcedulas17;
$trecaudo17conciliacion =  $trecaudo17;
$tcedulas18conciliacion =  $tcedulas18;
$trecaudo18conciliacion =  $trecaudo18;
$tcedulas19conciliacion =  $tcedulas19;
$trecaudo19conciliacion =  $trecaudo19;
$tcedulas20conciliacion =  $tcedulas20;
$trecaudo20conciliacion =  $trecaudo20;
$tcedulas21conciliacion =  $tcedulas21;
$trecaudo21conciliacion =  $trecaudo21;
$tcedulas22conciliacion =  $tcedulas22;
$trecaudo22conciliacion = $trecaudo22;
$tcedulas23conciliacion = $tcedulas23;
$trecaudo23conciliacion =  $trecaudo23;
$tcedulas24conciliacion =  $tcedulas24;
$trecaudo24conciliacion =  $trecaudo24;
$tcedulas25conciliacion =  $tcedulas25;
$trecaudo25conciliacion =  $trecaudo25;
$tcedulas26conciliacion =  $tcedulas26;
$trecaudo26conciliacion =  $trecaudo26;
$tcedulas27conciliacion = $tcedulas27;
$trecaudo27conciliacion =  $trecaudo27;
$tcedulas28conciliacion = $tcedulas28;
$trecaudo28conciliacion =  $trecaudo28;
$tcedulas29conciliacion = $tcedulas29;
$trecaudo29conciliacion = $trecaudo29;
$tcedulas30conciliacion = $tcedulas30;
$trecaudo30conciliacion =  $trecaudo30;
$tcedulas31conciliacion = $tcedulas31;
$trecaudo31conciliacion =  $trecaudo31;
$tcedulasconciliacion = $tcedulas;
$tvalorconciliacion = $tvlor;
$tcedurealizadoconciliacion = $tcedurealizado;
$tvalorrealizadoconciliacion = $tvalorrealizado;



}
}
	$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':BQ'.$i)->applyFromArray($styleArrayTwo);
	$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('A'.$i, 'TOTAL')
    	->setCellValue('B'.$i, $tcedulas)
		->setCellValue('C'.$i, $tvalor)
		->setCellValue('D'.$i, $tcedurealizado)
		->setCellValue('E'.$i, $tvalorrealizado)
		->setCellValue('F'.$i, round($tporcencedulas,1).' %')
		->setCellValue('G'.$i, round($tporcenvalor,1).' %')
		->setCellValue('H'.$i, $tcedulas1)
		->setCellValue('I'.$i, $trecaudo1)
		->setCellValue('J'.$i, $tcedulas2)
		->setCellValue('K'.$i, $trecaudo2)	
		->setCellValue('L'.$i, $tcedulas3)
		->setCellValue('M'.$i, $trecaudo3)	
		->setCellValue('N'.$i, $tcedulas4)
		->setCellValue('O'.$i, $trecaudo4)	
		->setCellValue('P'.$i, $tcedulas5)
		->setCellValue('Q'.$i, $trecaudo5)	
		->setCellValue('R'.$i, $tcedulas6)
		->setCellValue('S'.$i, $trecaudo6)	
		->setCellValue('T'.$i, $tcedulas7)
		->setCellValue('U'.$i, $trecaudo7)	
		->setCellValue('V'.$i, $tcedulas8)
		->setCellValue('W'.$i, $trecaudo8)	
		->setCellValue('X'.$i, $tcedulas9)
		->setCellValue('Y'.$i, $trecaudo9)	
		->setCellValue('Z'.$i, $tcedulas10)
		->setCellValue('AA'.$i, $trecaudo10)	
		->setCellValue('AB'.$i, $tcedulas11)
		->setCellValue('AC'.$i, $trecaudo11)	
		->setCellValue('AD'.$i, $tcedulas12)
		->setCellValue('AE'.$i, $trecaudo12)	
		->setCellValue('AF'.$i, $tcedulas13)
		->setCellValue('AG'.$i, $trecaudo13)	
		->setCellValue('AH'.$i, $tcedulas14)
		->setCellValue('AI'.$i, $trecaudo14)	
		->setCellValue('AJ'.$i, $tcedulas15)
		->setCellValue('AK'.$i, $trecaudo15)	
		->setCellValue('AL'.$i, $tcedulas16)
		->setCellValue('AM'.$i, $trecaudo16)	
		->setCellValue('AN'.$i, $tcedulas17)
		->setCellValue('AO'.$i, $trecaudo17)	
		->setCellValue('AP'.$i, $tcedulas18)
		->setCellValue('AQ'.$i, $trecaudo18)	
		->setCellValue('AR'.$i, $tcedulas19)
		->setCellValue('AS'.$i, $trecaudo19)	
		->setCellValue('AT'.$i, $tcedulas20)
		->setCellValue('AU'.$i, $trecaudo20)	
		->setCellValue('AV'.$i, $tcedulas21)
		->setCellValue('AW'.$i, $trecaudo21)	
		->setCellValue('AX'.$i, $tcedulas22)
		->setCellValue('AY'.$i, $trecaudo22)	
		->setCellValue('AZ'.$i, $tcedulas23)
		->setCellValue('BA'.$i, $trecaudo23)	
		->setCellValue('BB'.$i, $tcedulas24)
		->setCellValue('BC'.$i, $trecaudo24)	
		->setCellValue('BD'.$i, $tcedulas25)
		->setCellValue('BE'.$i, $trecaudo25)	
		->setCellValue('BF'.$i, $tcedulas26)
		->setCellValue('BG'.$i, $trecaudo26)	
		->setCellValue('BH'.$i, $tcedulas27)
		->setCellValue('BI'.$i, $trecaudo27)	
		->setCellValue('BJ'.$i, $tcedulas28)
		->setCellValue('BK'.$i, $trecaudo28)	
		->setCellValue('BL'.$i, $tcedulas29)
		->setCellValue('BM'.$i, $trecaudo29)	
		->setCellValue('BN'.$i, $tcedulas30)
		->setCellValue('BO'.$i, $trecaudo30)	
		->setCellValue('BP'.$i, $tcedulas31)
		->setCellValue('BQ'.$i, $trecaudo31);	
	$i++;				
	

// ----------------------------------------------------------------------------------------------------------


$tcedulas1  = 0;
$tcedulas2  = 0;
$tcedulas3  = 0;
$tcedulas4  = 0;
$tcedulas5  = 0;
$tcedulas6  = 0;
$tcedulas7  = 0;
$tcedulas8  = 0;
$tcedulas9  = 0;
$tcedulas10  = 0;
$tcedulas11  = 0;
$tcedulas12  = 0;
$tcedulas13  = 0;
$tcedulas14  = 0;
$tcedulas15  = 0;
$tcedulas16  = 0;
$tcedulas17  = 0;
$tcedulas18  = 0;
$tcedulas19  = 0;
$tcedulas20  = 0;
$tcedulas21  = 0;
$tcedulas22  = 0;
$tcedulas23  = 0;
$tcedulas24  = 0;
$tcedulas25  = 0;
$tcedulas26  = 0;
$tcedulas27  = 0;
$tcedulas28  = 0;
$tcedulas29  = 0;
$tcedulas30  = 0;
$tcedulas31  = 0;
$trecaudo1  = 0;
$trecaudo2  = 0;
$trecaudo3  = 0;
$trecaudo4  = 0;
$trecaudo5  = 0;
$trecaudo6  = 0;
$trecaudo7  = 0;
$trecaudo8  = 0;
$trecaudo9  = 0;
$trecaudo10  = 0;
$trecaudo11  = 0;
$trecaudo12  = 0;
$trecaudo13  = 0;
$trecaudo14  = 0;
$trecaudo15  = 0;
$trecaudo16  = 0;
$trecaudo17  = 0;
$trecaudo18  = 0;
$trecaudo19  = 0;
$trecaudo20  = 0;
$trecaudo21  = 0;
$trecaudo22  = 0;
$trecaudo23  = 0;
$trecaudo24  = 0;
$trecaudo25  = 0;
$trecaudo26  = 0;
$trecaudo27  = 0;
$trecaudo28  = 0;
$trecaudo29  = 0;
$trecaudo30  = 0;
$trecaudo31  = 0;
$tcedulas = 0;
$tvalor = 0;
$tcedurealizado = 0;
$tvalorrealizado = 0;
$tporcencedulas = 0;
$tporcenvalor = 0;

	
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$i.':C'.$i);
$objPHPExcel->getActiveSheet()->getStyle('A'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$i, 'MORA 31 - 60');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('D'.$i.':E'.$i);
$objPHPExcel->getActiveSheet()->getStyle('D'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$i, 'REALIZADO');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('F'.$i.':G'.$i);
$objPHPExcel->getActiveSheet()->getStyle('F'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$i, ' % REALIZADO');
 
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('H'.$i.':I'.$i);
$objPHPExcel->getActiveSheet()->getStyle('H'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$i, '1');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('J'.$i.':K'.$i);
$objPHPExcel->getActiveSheet()->getStyle('J'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.$i, '2');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('L'.$i.':M'.$i);
$objPHPExcel->getActiveSheet()->getStyle('L'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('L'.$i, '3');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('N'.$i.':O'.$i);
$objPHPExcel->getActiveSheet()->getStyle('N'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('N'.$i, '4');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('P'.$i.':Q'.$i);
$objPHPExcel->getActiveSheet()->getStyle('P'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('P'.$i, '5');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('R'.$i.':S'.$i);
$objPHPExcel->getActiveSheet()->getStyle('L'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('R'.$i, '6');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('T'.$i.':U'.$i);
$objPHPExcel->getActiveSheet()->getStyle('T'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('T'.$i, '7');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('V'.$i.':W'.$i);
$objPHPExcel->getActiveSheet()->getStyle('V'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('V'.$i, '8');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('X'.$i.':Y'.$i);
$objPHPExcel->getActiveSheet()->getStyle('X'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('X'.$i, '9');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('Z'.$i.':AA'.$i);
$objPHPExcel->getActiveSheet()->getStyle('Z'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('Z'.$i, '10');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AB'.$i.':AC'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AB'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AB'.$i, '11');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AB'.$i.':AC'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AB'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AB'.$i, '11');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AD'.$i.':AE'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AD'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AD'.$i, '12');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AF'.$i.':AG'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AF'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AF'.$i, '13');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AH'.$i.':AI'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AH'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AH'.$i, '14');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AJ'.$i.':AK'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AJ'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AJ'.$i, '15');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AL'.$i.':AM'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AL'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AL'.$i, '16');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AN'.$i.':AO'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AN'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AN'.$i, '17');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AP'.$i.':AQ'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AP'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AP'.$i, '18');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AR'.$i.':AS'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AR'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AR'.$i, '19');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AT'.$i.':AU'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AT'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AT'.$i, '20');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AV'.$i.':AW'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AV'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AV'.$i, '21');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AX'.$i.':AY'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AX'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AX'.$i, '22');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AZ'.$i.':BA'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AZ'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AZ'.$i, '23');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BB'.$i.':BC'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BB'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BB'.$i, '24');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BD'.$i.':BE'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BD'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BD'.$i, '25');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BF'.$i.':BG'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BF'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BF'.$i, '26');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BH'.$i.':BI'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BH'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BH'.$i, '27');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BJ'.$i.':BK'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BJ'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BJ'.$i, '28');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BL'.$i.':BM'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BL'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BL'.$i, '29');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BN'.$i.':BO'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BN'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BN'.$i, '30');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BP'.$i.':BQ'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BP'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BP'.$i, '31');


$i++;
$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':BQ'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$i, 'ASESOR')
            ->setCellValue('B'.$i, 'CEDULAS')
            ->setCellValue('C'.$i, 'VALOR')
            ->setCellValue('D'.$i, 'CEDULAS')
			->setCellValue('E'.$i, 'VALOR')
			->setCellValue('F'.$i, 'CEDULAS')
			->setCellValue('G'.$i, 'VALOR')	
			->setCellValue('H'.$i, 'CEDULAS')
			->setCellValue('I'.$i, 'RECAUDO')
			->setCellValue('J'.$i, 'CEDULAS')
			->setCellValue('K'.$i, 'RECAUDO')	
			->setCellValue('L'.$i, 'CEDULAS')
			->setCellValue('M'.$i, 'RECAUDO')	
			->setCellValue('N'.$i, 'CEDULAS')
			->setCellValue('O'.$i, 'RECAUDO')	
			->setCellValue('P'.$i, 'CEDULAS')
			->setCellValue('Q'.$i, 'RECAUDO')	
			->setCellValue('R'.$i, 'CEDULAS')
			->setCellValue('S'.$i, 'RECAUDO')	
			->setCellValue('T'.$i, 'CEDULAS')
			->setCellValue('U'.$i, 'RECAUDO')	
			->setCellValue('V'.$i, 'CEDULAS')
			->setCellValue('W'.$i, 'RECAUDO')	
			->setCellValue('X'.$i, 'CEDULAS')
			->setCellValue('Y'.$i, 'RECAUDO')	
			->setCellValue('Z'.$i, 'CEDULAS')
			->setCellValue('AA'.$i, 'RECAUDO')	
			->setCellValue('AB'.$i, 'CEDULAS')
			->setCellValue('AC'.$i, 'RECAUDO')	
			->setCellValue('AD'.$i, 'CEDULAS')
			->setCellValue('AE'.$i, 'RECAUDO')	
			->setCellValue('AF'.$i, 'CEDULAS')
			->setCellValue('AG'.$i, 'RECAUDO')	
			->setCellValue('AH'.$i, 'CEDULAS')
			->setCellValue('AI'.$i, 'RECAUDO')	
			->setCellValue('AJ'.$i, 'CEDULAS')
			->setCellValue('AK'.$i, 'RECAUDO')	
			->setCellValue('AL'.$i, 'CEDULAS')
			->setCellValue('AM'.$i, 'RECAUDO')	
			->setCellValue('AN'.$i, 'CEDULAS')
			->setCellValue('AO'.$i, 'RECAUDO')	
			->setCellValue('AP'.$i, 'CEDULAS')
			->setCellValue('AQ'.$i, 'RECAUDO')	
			->setCellValue('AR'.$i, 'CEDULAS')
			->setCellValue('AS'.$i, 'RECAUDO')	
			->setCellValue('AT'.$i, 'CEDULAS')
			->setCellValue('AU'.$i, 'RECAUDO')	
			->setCellValue('AV'.$i, 'CEDULAS')
			->setCellValue('AW'.$i, 'RECAUDO')	
			->setCellValue('AX'.$i, 'CEDULAS')
			->setCellValue('AY'.$i, 'RECAUDO')	
			->setCellValue('AZ'.$i, 'CEDULAS')
			->setCellValue('BA'.$i, 'RECAUDO')	
			->setCellValue('BB'.$i, 'CEDULAS')
			->setCellValue('BC'.$i, 'RECAUDO')	
			->setCellValue('BD'.$i, 'CEDULAS')
			->setCellValue('BE'.$i, 'RECAUDO')	
			->setCellValue('BF'.$i, 'CEDULAS')
			->setCellValue('BG'.$i, 'RECAUDO')	
			->setCellValue('BH'.$i, 'CEDULAS')
			->setCellValue('BI'.$i, 'RECAUDO')	
			->setCellValue('BJ'.$i, 'CEDULAS')
			->setCellValue('BK'.$i, 'RECAUDO')	
			->setCellValue('BL'.$i, 'CEDULAS')
			->setCellValue('BM'.$i, 'RECAUDO')	
			->setCellValue('BN'.$i, 'CEDULAS')
			->setCellValue('BO'.$i, 'RECAUDO')	
			->setCellValue('BP'.$i, 'CEDULAS')
			->setCellValue('BQ'.$i, 'RECAUDO');
			$i++;	

$sql = "SELECT * FROM comoVamos INNER JOIN usuarios ON usuid = comvausuario INNER JOIN comovamosgrupodeta ON grupousuario = comvausuario WHERE comvafecha = '$fecha' AND grupofecha = '$fecha';";
$qry = $db->query($sql);			

while($row = $qry->fetch(PDO::FETCH_ASSOC)){
if ($row['gruponombre'] == 'MORA 31 - 60') {
	
	$sqlrealizado = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha >= '$fecha'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";

	$qryrealizado = $db->query($sqlrealizado);	
	$rowrealizado = $qryrealizado->fetch(PDO::FETCH_ASSOC);	
	
	$porcencedulas = ($rowrealizado['Cedulas'] / $row['comvacedulas']) * 100;
	$porcenvalor = ($rowrealizado['Valor'] /  $row['comvavalor']) * 100;
	
	$fechasindias = substr($fecha, 0, -2);
	

	$sqldia1 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."01'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia1 = $db->query($sqldia1);	
	$rowdia1 = $qrydia1->fetch(PDO::FETCH_ASSOC);
	
	$cedulas1 = $rowdia1['Cedulas'];
	$recaudo1 = $rowdia1['Valor'];
	
	
	$sqldia2 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."02'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia2 = $db->query($sqldia2);	
	$rowdia2 = $qrydia2->fetch(PDO::FETCH_ASSOC);
	
	$cedulas2 = $rowdia2['Cedulas'];
	$recaudo2 = $rowdia2['Valor'];


	$sqldia3 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."03'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia3 = $db->query($sqldia3);	
	$rowdia3 = $qrydia3->fetch(PDO::FETCH_ASSOC);
	
	$cedulas3 = $rowdia3['Cedulas'];
	$recaudo3 = $rowdia3['Valor'];


	$sqldia4 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."04'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia4 = $db->query($sqldia4);	
	$rowdia4 = $qrydia4->fetch(PDO::FETCH_ASSOC);
	
	$cedulas4 = $rowdia4['Cedulas'];
	$recaudo4 = $rowdia4['Valor'];


	$sqldia5 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."05'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia5 = $db->query($sqldia5);	
	$rowdia5 = $qrydia5->fetch(PDO::FETCH_ASSOC);
	
	$cedulas5 = $rowdia5['Cedulas'];
	$recaudo5 = $rowdia5['Valor'];


	$sqldia6 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."06'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia6 = $db->query($sqldia6);	
	$rowdia6 = $qrydia6->fetch(PDO::FETCH_ASSOC);
	
	$cedulas6 = $rowdia6['Cedulas'];
	$recaudo6 = $rowdia6['Valor'];


	$sqldia7 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."07'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia7 = $db->query($sqldia7);	
	$rowdia7 = $qrydia7->fetch(PDO::FETCH_ASSOC);
	
	$cedulas7 = $rowdia7['Cedulas'];
	$recaudo7 = $rowdia7['Valor'];


	$sqldia8 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."08'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia8 = $db->query($sqldia8);	
	$rowdia8 = $qrydia8->fetch(PDO::FETCH_ASSOC);
	
	$cedulas8 = $rowdia8['Cedulas'];
	$recaudo8 = $rowdia8['Valor'];


	$sqldia9 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."09'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia9 = $db->query($sqldia9);	
	$rowdia9 = $qrydia9->fetch(PDO::FETCH_ASSOC);
	
	$cedulas9 = $rowdia9['Cedulas'];
	$recaudo9 = $rowdia9['Valor'];


		
	$sqldia10 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."10'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia10 = $db->query($sqldia10);	
	$rowdia10 = $qrydia10->fetch(PDO::FETCH_ASSOC);
	
	$cedulas10 = $rowdia10['Cedulas'];
	$recaudo10 = $rowdia10['Valor'];


		
	$sqldia11 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."11'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia11 = $db->query($sqldia11);	
	$rowdia11 = $qrydia11->fetch(PDO::FETCH_ASSOC);
	
	$cedulas11 = $rowdia11['Cedulas'];
	$recaudo11 = $rowdia11['Valor'];


	
	$sqldia12 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."12'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia12 = $db->query($sqldia12);	
	$rowdia12 = $qrydia12->fetch(PDO::FETCH_ASSOC);
	
	$cedulas12 = $rowdia12['Cedulas'];
	$recaudo12 = $rowdia12['Valor'];


	
	$sqldia13 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."13'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia13 = $db->query($sqldia13);	
	$rowdia13 = $qrydia13->fetch(PDO::FETCH_ASSOC);
	
	$cedulas13 = $rowdia13['Cedulas'];
	$recaudo13 = $rowdia13['Valor'];


	
	$sqldia14 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."14'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia14 = $db->query($sqldia14);	
	$rowdia14 = $qrydia14->fetch(PDO::FETCH_ASSOC);
	
	$cedulas14 = $rowdia14['Cedulas'];
	$recaudo14 = $rowdia14['Valor'];


	
	$sqldia15 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."15'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia15 = $db->query($sqldia15);	
	$rowdia15 = $qrydia15->fetch(PDO::FETCH_ASSOC);
	
	$cedulas15 = $rowdia15['Cedulas'];
	$recaudo15 = $rowdia15['Valor'];


	
	$sqldia16 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."16'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia16 = $db->query($sqldia16);	
	$rowdia16 = $qrydia16->fetch(PDO::FETCH_ASSOC);
	
	$cedulas16 = $rowdia16['Cedulas'];
	$recaudo16 = $rowdia16['Valor'];


	
	$sqldia17 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."17'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia17 = $db->query($sqldia17);	
	$rowdia17 = $qrydia17->fetch(PDO::FETCH_ASSOC);
	
	$cedulas17 = $rowdia17['Cedulas'];
	$recaudo17 = $rowdia17['Valor'];


	
	$sqldia18 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."18'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia18 = $db->query($sqldia18);	
	$rowdia18 = $qrydia18->fetch(PDO::FETCH_ASSOC);
	
	$cedulas18 = $rowdia18['Cedulas'];
	$recaudo18 = $rowdia18['Valor'];


	
	$sqldia19 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."19'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia19 = $db->query($sqldia19);	
	$rowdia19 = $qrydia19->fetch(PDO::FETCH_ASSOC);
	
	$cedulas19 = $rowdia19['Cedulas'];
	$recaudo19 = $rowdia19['Valor'];


	
	$sqldia20 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."20'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia20 = $db->query($sqldia20);	
	$rowdia20 = $qrydia20->fetch(PDO::FETCH_ASSOC);
	
	$cedulas20 = $rowdia20['Cedulas'];
	$recaudo20 = $rowdia20['Valor'];


	
	$sqldia21 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."21'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia21 = $db->query($sqldia21);	
	$rowdia21 = $qrydia21->fetch(PDO::FETCH_ASSOC);
	
	$cedulas21 = $rowdia21['Cedulas'];
	$recaudo21 = $rowdia21['Valor'];


	
	$sqldia22 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."22'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia22 = $db->query($sqldia22);	
	$rowdia22 = $qrydia22->fetch(PDO::FETCH_ASSOC);
	
	$cedulas22 = $rowdia22['Cedulas'];
	$recaudo22 = $rowdia22['Valor'];


	
	$sqldia23 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."23'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia23 = $db->query($sqldia23);	
	$rowdia23 = $qrydia23->fetch(PDO::FETCH_ASSOC);
	
	$cedulas23 = $rowdia23['Cedulas'];
	$recaudo23 = $rowdia23['Valor'];


	
	$sqldia24 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."24'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia24 = $db->query($sqldia24);	
	$rowdia24 = $qrydia24->fetch(PDO::FETCH_ASSOC);
	
	$cedulas24 = $rowdia24['Cedulas'];
	$recaudo24 = $rowdia24['Valor'];


	
	$sqldia25 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."25'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia25 = $db->query($sqldia25);	
	$rowdia25 = $qrydia25->fetch(PDO::FETCH_ASSOC);
	
	$cedulas25 = $rowdia25['Cedulas'];
	$recaudo25 = $rowdia25['Valor'];


	
	$sqldia26 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."26'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia26 = $db->query($sqldia26);	
	$rowdia26 = $qrydia26->fetch(PDO::FETCH_ASSOC);
	
	$cedulas26 = $rowdia26['Cedulas'];
	$recaudo26 = $rowdia26['Valor'];


	
	$sqldia27 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."27'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia27 = $db->query($sqldia27);	
	$rowdia27 = $qrydia27->fetch(PDO::FETCH_ASSOC);
	
	$cedulas27 = $rowdia27['Cedulas'];
	$recaudo27 = $rowdia27['Valor'];


	
	$sqldia28 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."28'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia28 = $db->query($sqldia28);	
	$rowdia28 = $qrydia28->fetch(PDO::FETCH_ASSOC);
	
	$cedulas28 = $rowdia28['Cedulas'];
	$recaudo28 = $rowdia28['Valor'];


	
	$sqldia29 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."29'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia29 = $db->query($sqldia29);	
	$rowdia29 = $qrydia29->fetch(PDO::FETCH_ASSOC);
	
	$cedulas29 = $rowdia29['Cedulas'];
	$recaudo29 = $rowdia29['Valor'];


	
	$sqldia30 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."30'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia30 = $db->query($sqldia30);	
	$rowdia30 = $qrydia30->fetch(PDO::FETCH_ASSOC);
	
	$cedulas30 = $rowdia30['Cedulas'];
	$recaudo30 = $rowdia30['Valor'];


	
	$sqldia31 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."31'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia31 = $db->query($sqldia31);	
	$rowdia31 = $qrydia31->fetch(PDO::FETCH_ASSOC);
	
	$cedulas31 = $rowdia31['Cedulas'];
	$recaudo31 = $rowdia31['Valor'];

	$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':BQ'.$i)->applyFromArray($styleArray);
	$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('A'.$i, $row['usunombre'])
    	->setCellValue('B'.$i, $row['comvacedulas'])
		->setCellValue('C'.$i, $row['comvavalor'])
		->setCellValue('D'.$i, $rowrealizado['Cedulas'])
		->setCellValue('E'.$i, $rowrealizado['Valor'])
		->setCellValue('F'.$i, round($porcencedulas,1).' %')
		->setCellValue('G'.$i, round($porcenvalor,1).' %')
		->setCellValue('H'.$i, $cedulas1)
		->setCellValue('I'.$i, $recaudo1)
		->setCellValue('J'.$i, $cedulas2)
		->setCellValue('K'.$i, $recaudo2)	
		->setCellValue('L'.$i, $cedulas3)
		->setCellValue('M'.$i, $recaudo3)	
		->setCellValue('N'.$i, $cedulas4)
		->setCellValue('O'.$i, $recaudo4)	
		->setCellValue('P'.$i, $cedulas5)
		->setCellValue('Q'.$i, $recaudo5)	
		->setCellValue('R'.$i, $cedulas6)
		->setCellValue('S'.$i, $recaudo6)	
		->setCellValue('T'.$i, $cedulas7)
		->setCellValue('U'.$i, $recaudo7)	
		->setCellValue('V'.$i, $cedulas8)
		->setCellValue('W'.$i, $recaudo8)	
		->setCellValue('X'.$i, $cedulas9)
		->setCellValue('Y'.$i, $recaudo9)	
		->setCellValue('Z'.$i, $cedulas10)
		->setCellValue('AA'.$i, $recaudo10)	
		->setCellValue('AB'.$i, $cedulas11)
		->setCellValue('AC'.$i, $recaudo11)	
		->setCellValue('AD'.$i, $cedulas12)
		->setCellValue('AE'.$i, $recaudo12)	
		->setCellValue('AF'.$i, $cedulas13)
		->setCellValue('AG'.$i, $recaudo13)	
		->setCellValue('AH'.$i, $cedulas14)
		->setCellValue('AI'.$i, $recaudo14)	
		->setCellValue('AJ'.$i, $cedulas15)
		->setCellValue('AK'.$i, $recaudo15)	
		->setCellValue('AL'.$i, $cedulas16)
		->setCellValue('AM'.$i, $recaudo16)	
		->setCellValue('AN'.$i, $cedulas17)
		->setCellValue('AO'.$i, $recaudo17)	
		->setCellValue('AP'.$i, $cedulas18)
		->setCellValue('AQ'.$i, $recaudo18)	
		->setCellValue('AR'.$i, $cedulas19)
		->setCellValue('AS'.$i, $recaudo19)	
		->setCellValue('AT'.$i, $cedulas20)
		->setCellValue('AU'.$i, $recaudo20)	
		->setCellValue('AV'.$i, $cedulas21)
		->setCellValue('AW'.$i, $recaudo21)	
		->setCellValue('AX'.$i, $cedulas22)
		->setCellValue('AY'.$i, $recaudo22)	
		->setCellValue('AZ'.$i, $cedulas23)
		->setCellValue('BA'.$i, $recaudo23)	
		->setCellValue('BB'.$i, $cedulas24)
		->setCellValue('BC'.$i, $recaudo24)	
		->setCellValue('BD'.$i, $cedulas25)
		->setCellValue('BE'.$i, $recaudo25)	
		->setCellValue('BF'.$i, $cedulas26)
		->setCellValue('BG'.$i, $recaudo26)	
		->setCellValue('BH'.$i, $cedulas27)
		->setCellValue('BI'.$i, $recaudo27)	
		->setCellValue('BJ'.$i, $cedulas28)
		->setCellValue('BK'.$i, $recaudo28)	
		->setCellValue('BL'.$i, $cedulas29)
		->setCellValue('BM'.$i, $recaudo29)	
		->setCellValue('BN'.$i, $cedulas30)
		->setCellValue('BO'.$i, $recaudo30)	
		->setCellValue('BP'.$i, $cedulas31)
		->setCellValue('BQ'.$i, $recaudo31);	
	$i++;

	$tcedulas = $row['comvacedulas'] + $tcedulas;
	$tvalor = $row['comvavalor'] + $tvalor;
	$tcedurealizado = $rowrealizado['Cedulas'] + $tcedurealizado;
	$tvalorrealizado = $rowrealizado['Valor'] + $tvalorrealizado;
	$tporcencedulas = ($tcedurealizado / $tcedulas) * 100;
	$tporcenvalor = ($tvalorrealizado /  $tvalor) * 100;
	
$tcedulas1 = $rowdia1['Cedulas'] + $tcedulas1;
$trecaudo1 = $rowdia1['Valor'] + $trecaudo1;
$tcedulas2 = $rowdia2['Cedulas'] + $tcedulas2;
$trecaudo2 = $rowdia2['Valor'] + $trecaudo2;
$tcedulas3 = $rowdia3['Cedulas'] + $tcedulas3;
$trecaudo3 = $rowdia3['Valor'] + $trecaudo3;
$tcedulas4 = $rowdia4['Cedulas'] + $tcedulas4;
$trecaudo4 = $rowdia4['Valor'] + $trecaudo4;
$tcedulas5 = $rowdia5['Cedulas'] + $tcedulas5;
$trecaudo5 = $rowdia5['Valor'] + $trecaudo5;
$tcedulas6 = $rowdia6['Cedulas'] + $tcedulas6;
$trecaudo6 = $rowdia6['Valor'] + $trecaudo6;
$tcedulas7 = $rowdia7['Cedulas'] + $tcedulas7;
$trecaudo7 = $rowdia7['Valor'] + $trecaudo7;
$tcedulas8 = $rowdia8['Cedulas'] + $tcedulas8;
$trecaudo8 = $rowdia8['Valor'] + $trecaudo8;
$tcedulas9 = $rowdia9['Cedulas'] + $tcedulas9;
$trecaudo9 = $rowdia9['Valor'] + $trecaudo9;
$tcedulas10 = $rowdia10['Cedulas'] + $tcedulas10;
$trecaudo10 = $rowdia10['Valor'] + $trecaudo10;
$tcedulas11 = $rowdia11['Cedulas'] + $tcedulas11;
$trecaudo11 = $rowdia11['Valor'] + $trecaudo11;
$tcedulas12 = $rowdia12['Cedulas'] + $tcedulas12;
$trecaudo12 = $rowdia12['Valor'] + $trecaudo12;
$tcedulas13 = $rowdia13['Cedulas'] + $tcedulas13;
$trecaudo13 = $rowdia13['Valor'] + $trecaudo13;
$tcedulas14 = $rowdia14['Cedulas'] + $tcedulas14;
$trecaudo14 = $rowdia14['Valor'] + $trecaudo14;
$tcedulas15 = $rowdia15['Cedulas'] + $tcedulas15;
$trecaudo15 = $rowdia15['Valor'] + $trecaudo15;
$tcedulas16 = $rowdia16['Cedulas'] + $tcedulas16;
$trecaudo16 = $rowdia16['Valor'] + $trecaudo16;
$tcedulas17 = $rowdia17['Cedulas'] + $tcedulas17;
$trecaudo17 = $rowdia17['Valor'] + $trecaudo17;
$tcedulas18 = $rowdia18['Cedulas'] + $tcedulas18;
$trecaudo18 = $rowdia18['Valor'] + $trecaudo18;
$tcedulas19 = $rowdia19['Cedulas'] + $tcedulas19;
$trecaudo19 = $rowdia19['Valor'] + $trecaudo19;
$tcedulas20 = $rowdia20['Cedulas'] + $tcedulas20;
$trecaudo20 = $rowdia20['Valor'] + $trecaudo20;
$tcedulas21 = $rowdia21['Cedulas'] + $tcedulas21;
$trecaudo21 = $rowdia21['Valor'] + $trecaudo21;
$tcedulas22 = $rowdia22['Cedulas'] + $tcedulas22;
$trecaudo22 = $rowdia22['Valor'] + $trecaudo22;
$tcedulas23 = $rowdia23['Cedulas'] + $tcedulas23;
$trecaudo23 = $rowdia23['Valor'] + $trecaudo23;
$tcedulas24 = $rowdia24['Cedulas'] + $tcedulas24;
$trecaudo24 = $rowdia24['Valor'] + $trecaudo24;
$tcedulas25 = $rowdia25['Cedulas'] + $tcedulas25;
$trecaudo25 = $rowdia25['Valor'] + $trecaudo25;
$tcedulas26 = $rowdia26['Cedulas'] + $tcedulas26;
$trecaudo26 = $rowdia26['Valor'] + $trecaudo26;
$tcedulas27 = $rowdia27['Cedulas'] + $tcedulas27;
$trecaudo27 = $rowdia27['Valor'] + $trecaudo27;
$tcedulas28 = $rowdia28['Cedulas'] + $tcedulas28;
$trecaudo28 = $rowdia28['Valor'] + $trecaudo28;
$tcedulas29 = $rowdia29['Cedulas'] + $tcedulas29;
$trecaudo29 = $rowdia29['Valor'] + $trecaudo29;
$tcedulas30 = $rowdia30['Cedulas'] + $tcedulas30;
$trecaudo30 = $rowdia30['Valor'] + $trecaudo30;
$tcedulas31 = $rowdia31['Cedulas'] + $tcedulas31;
$trecaudo31 = $rowdia31['Valor'] + $trecaudo31;


$tcedulas1mora31 = $tcedulas1;
$trecaudo1mora31 =  $trecaudo1;
$tcedulas2mora31 =  $tcedulas2;
$trecaudo2mora31 =  $trecaudo2;
$tcedulas3mora31 =  $tcedulas3;
$trecaudo3mora31 =  $trecaudo3;
$tcedulas4mora31 = $tcedulas4;
$trecaudo4mora31 = $trecaudo4;
$tcedulas5mora31 =  $tcedulas5;
$trecaudo5mora31 =  $trecaudo5;
$tcedulas6mora31 =  $tcedulas6;
$trecaudo6mora31 =  $trecaudo6;
$tcedulas7mora31 =  $tcedulas7;
$trecaudo7mora31 =  $trecaudo7;
$tcedulas8mora31 =  $tcedulas8;
$trecaudo8mora31 = $trecaudo8;
$tcedulas9mora31 =  $tcedulas9;
$trecaudo9mora31 =  $trecaudo9;
$tcedulas10mora31 =  $tcedulas10;
$trecaudo10mora31 = $trecaudo10;
$tcedulas11mora31 =  $tcedulas11;
$trecaudo11mora31 =  $trecaudo11;
$tcedulas12mora31 =  $tcedulas12;
$trecaudo12mora31 =  $trecaudo12;
$tcedulas13mora31 =  $tcedulas13;
$trecaudo13mora31 =  $trecaudo13;
$tcedulas14mora31 =  $tcedulas14;
$trecaudo14mora31 =  $trecaudo14;
$tcedulas15mora31 =  $tcedulas15;
$trecaudo15mora31 =  $trecaudo15;
$tcedulas16mora31 = $tcedulas16;
$trecaudo16mora31 =  $trecaudo16;
$tcedulas17mora31 =  $tcedulas17;
$trecaudo17mora31 =  $trecaudo17;
$tcedulas18mora31 =  $tcedulas18;
$trecaudo18mora31 =  $trecaudo18;
$tcedulas19mora31 =  $tcedulas19;
$trecaudo19mora31 =  $trecaudo19;
$tcedulas20mora31 =  $tcedulas20;
$trecaudo20mora31 =  $trecaudo20;
$tcedulas21mora31 = $tcedulas21;
$trecaudo21mora31 = $trecaudo21;
$tcedulas22mora31 =  $tcedulas22;
$trecaudo22mora31 =  $trecaudo22;
$tcedulas23mora31 =  $tcedulas23;
$trecaudo23mora31 =  $trecaudo23;
$tcedulas24mora31 = $tcedulas24;
$trecaudo24mora31 =  $trecaudo24;
$tcedulas25mora31 =  $tcedulas25;
$trecaudo25mora31 = $trecaudo25;
$tcedulas26mora31 =  $tcedulas26;
$trecaudo26mora31 =  $trecaudo26;
$tcedulas27mora31 =  $tcedulas27;
$trecaudo27mora31 =  $trecaudo27;
$tcedulas28mora31 =  $tcedulas28;
$trecaudo28mora31 =  $trecaudo28;
$tcedulas29mora31 = $tcedulas29;
$trecaudo29mora31 =  $trecaudo29;
$tcedulas30mora31 =  $tcedulas30;
$trecaudo30mora31 =  $trecaudo30;
$tcedulas31mora31 =  $tcedulas31;
$trecaudo31mora31 =  $trecaudo31;
$tcedulasmora31 = $tcedulas;
$tvalormora31 = $tvlor;
$tcedurealizadomora31 = $tcedurealizado;
$tvalorrealizadomora31 = $tvalorrealizado;




}
}
	$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':BQ'.$i)->applyFromArray($styleArrayTwo);
	$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('A'.$i, 'TOTAL')
    	->setCellValue('B'.$i, $tcedulas)
		->setCellValue('C'.$i, $tvalor)
		->setCellValue('D'.$i, $tcedurealizado)
		->setCellValue('E'.$i, $tvalorrealizado)
		->setCellValue('F'.$i, round($tporcencedulas,1).' %')
		->setCellValue('G'.$i, round($tporcenvalor,1).' %')
		->setCellValue('H'.$i, $tcedulas1)
		->setCellValue('I'.$i, $trecaudo1)
		->setCellValue('J'.$i, $tcedulas2)
		->setCellValue('K'.$i, $trecaudo2)	
		->setCellValue('L'.$i, $tcedulas3)
		->setCellValue('M'.$i, $trecaudo3)	
		->setCellValue('N'.$i, $tcedulas4)
		->setCellValue('O'.$i, $trecaudo4)	
		->setCellValue('P'.$i, $tcedulas5)
		->setCellValue('Q'.$i, $trecaudo5)	
		->setCellValue('R'.$i, $tcedulas6)
		->setCellValue('S'.$i, $trecaudo6)	
		->setCellValue('T'.$i, $tcedulas7)
		->setCellValue('U'.$i, $trecaudo7)	
		->setCellValue('V'.$i, $tcedulas8)
		->setCellValue('W'.$i, $trecaudo8)	
		->setCellValue('X'.$i, $tcedulas9)
		->setCellValue('Y'.$i, $trecaudo9)	
		->setCellValue('Z'.$i, $tcedulas10)
		->setCellValue('AA'.$i, $trecaudo10)	
		->setCellValue('AB'.$i, $tcedulas11)
		->setCellValue('AC'.$i, $trecaudo11)	
		->setCellValue('AD'.$i, $tcedulas12)
		->setCellValue('AE'.$i, $trecaudo12)	
		->setCellValue('AF'.$i, $tcedulas13)
		->setCellValue('AG'.$i, $trecaudo13)	
		->setCellValue('AH'.$i, $tcedulas14)
		->setCellValue('AI'.$i, $trecaudo14)	
		->setCellValue('AJ'.$i, $tcedulas15)
		->setCellValue('AK'.$i, $trecaudo15)	
		->setCellValue('AL'.$i, $tcedulas16)
		->setCellValue('AM'.$i, $trecaudo16)	
		->setCellValue('AN'.$i, $tcedulas17)
		->setCellValue('AO'.$i, $trecaudo17)	
		->setCellValue('AP'.$i, $tcedulas18)
		->setCellValue('AQ'.$i, $trecaudo18)	
		->setCellValue('AR'.$i, $tcedulas19)
		->setCellValue('AS'.$i, $trecaudo19)	
		->setCellValue('AT'.$i, $tcedulas20)
		->setCellValue('AU'.$i, $trecaudo20)	
		->setCellValue('AV'.$i, $tcedulas21)
		->setCellValue('AW'.$i, $trecaudo21)	
		->setCellValue('AX'.$i, $tcedulas22)
		->setCellValue('AY'.$i, $trecaudo22)	
		->setCellValue('AZ'.$i, $tcedulas23)
		->setCellValue('BA'.$i, $trecaudo23)	
		->setCellValue('BB'.$i, $tcedulas24)
		->setCellValue('BC'.$i, $trecaudo24)	
		->setCellValue('BD'.$i, $tcedulas25)
		->setCellValue('BE'.$i, $trecaudo25)	
		->setCellValue('BF'.$i, $tcedulas26)
		->setCellValue('BG'.$i, $trecaudo26)	
		->setCellValue('BH'.$i, $tcedulas27)
		->setCellValue('BI'.$i, $trecaudo27)	
		->setCellValue('BJ'.$i, $tcedulas28)
		->setCellValue('BK'.$i, $trecaudo28)	
		->setCellValue('BL'.$i, $tcedulas29)
		->setCellValue('BM'.$i, $trecaudo29)	
		->setCellValue('BN'.$i, $tcedulas30)
		->setCellValue('BO'.$i, $trecaudo30)	
		->setCellValue('BP'.$i, $tcedulas31)
		->setCellValue('BQ'.$i, $trecaudo31);	
	$i++;				


// ----------------------------------------------------------------------------------------------------------


$tcedulas1  = 0;
$tcedulas2  = 0;
$tcedulas3  = 0;
$tcedulas4  = 0;
$tcedulas5  = 0;
$tcedulas6  = 0;
$tcedulas7  = 0;
$tcedulas8  = 0;
$tcedulas9  = 0;
$tcedulas10  = 0;
$tcedulas11  = 0;
$tcedulas12  = 0;
$tcedulas13  = 0;
$tcedulas14  = 0;
$tcedulas15  = 0;
$tcedulas16  = 0;
$tcedulas17  = 0;
$tcedulas18  = 0;
$tcedulas19  = 0;
$tcedulas20  = 0;
$tcedulas21  = 0;
$tcedulas22  = 0;
$tcedulas23  = 0;
$tcedulas24  = 0;
$tcedulas25  = 0;
$tcedulas26  = 0;
$tcedulas27  = 0;
$tcedulas28  = 0;
$tcedulas29  = 0;
$tcedulas30  = 0;
$tcedulas31  = 0;
$trecaudo1  = 0;
$trecaudo2  = 0;
$trecaudo3  = 0;
$trecaudo4  = 0;
$trecaudo5  = 0;
$trecaudo6  = 0;
$trecaudo7  = 0;
$trecaudo8  = 0;
$trecaudo9  = 0;
$trecaudo10  = 0;
$trecaudo11  = 0;
$trecaudo12  = 0;
$trecaudo13  = 0;
$trecaudo14  = 0;
$trecaudo15  = 0;
$trecaudo16  = 0;
$trecaudo17  = 0;
$trecaudo18  = 0;
$trecaudo19  = 0;
$trecaudo20  = 0;
$trecaudo21  = 0;
$trecaudo22  = 0;
$trecaudo23  = 0;
$trecaudo24  = 0;
$trecaudo25  = 0;
$trecaudo26  = 0;
$trecaudo27  = 0;
$trecaudo28  = 0;
$trecaudo29  = 0;
$trecaudo30  = 0;
$trecaudo31  = 0;
$tcedulas = 0;
$tvalor = 0;
$tcedurealizado = 0;
$tvalorrealizado = 0;
$tporcencedulas = 0;
$tporcenvalor = 0;

	
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$i.':C'.$i);
$objPHPExcel->getActiveSheet()->getStyle('A'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$i, 'MORA ALTA');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('D'.$i.':E'.$i);
$objPHPExcel->getActiveSheet()->getStyle('D'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$i, 'REALIZADO');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('F'.$i.':G'.$i);
$objPHPExcel->getActiveSheet()->getStyle('F'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$i, ' % REALIZADO');
 
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('H'.$i.':I'.$i);
$objPHPExcel->getActiveSheet()->getStyle('H'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$i, '1');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('J'.$i.':K'.$i);
$objPHPExcel->getActiveSheet()->getStyle('J'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.$i, '2');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('L'.$i.':M'.$i);
$objPHPExcel->getActiveSheet()->getStyle('L'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('L'.$i, '3');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('N'.$i.':O'.$i);
$objPHPExcel->getActiveSheet()->getStyle('N'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('N'.$i, '4');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('P'.$i.':Q'.$i);
$objPHPExcel->getActiveSheet()->getStyle('P'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('P'.$i, '5');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('R'.$i.':S'.$i);
$objPHPExcel->getActiveSheet()->getStyle('L'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('R'.$i, '6');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('T'.$i.':U'.$i);
$objPHPExcel->getActiveSheet()->getStyle('T'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('T'.$i, '7');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('V'.$i.':W'.$i);
$objPHPExcel->getActiveSheet()->getStyle('V'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('V'.$i, '8');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('X'.$i.':Y'.$i);
$objPHPExcel->getActiveSheet()->getStyle('X'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('X'.$i, '9');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('Z'.$i.':AA'.$i);
$objPHPExcel->getActiveSheet()->getStyle('Z'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('Z'.$i, '10');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AB'.$i.':AC'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AB'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AB'.$i, '11');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AB'.$i.':AC'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AB'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AB'.$i, '11');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AD'.$i.':AE'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AD'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AD'.$i, '12');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AF'.$i.':AG'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AF'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AF'.$i, '13');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AH'.$i.':AI'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AH'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AH'.$i, '14');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AJ'.$i.':AK'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AJ'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AJ'.$i, '15');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AL'.$i.':AM'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AL'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AL'.$i, '16');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AN'.$i.':AO'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AN'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AN'.$i, '17');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AP'.$i.':AQ'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AP'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AP'.$i, '18');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AR'.$i.':AS'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AR'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AR'.$i, '19');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AT'.$i.':AU'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AT'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AT'.$i, '20');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AV'.$i.':AW'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AV'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AV'.$i, '21');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AX'.$i.':AY'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AX'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AX'.$i, '22');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AZ'.$i.':BA'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AZ'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AZ'.$i, '23');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BB'.$i.':BC'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BB'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BB'.$i, '24');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BD'.$i.':BE'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BD'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BD'.$i, '25');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BF'.$i.':BG'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BF'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BF'.$i, '26');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BH'.$i.':BI'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BH'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BH'.$i, '27');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BJ'.$i.':BK'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BJ'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BJ'.$i, '28');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BL'.$i.':BM'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BL'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BL'.$i, '29');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BN'.$i.':BO'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BN'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BN'.$i, '30');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BP'.$i.':BQ'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BP'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BP'.$i, '31');


$i++;
$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':BQ'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$i, 'ASESOR')
            ->setCellValue('B'.$i, 'CEDULAS')
            ->setCellValue('C'.$i, 'VALOR')
            ->setCellValue('D'.$i, 'CEDULAS')
			->setCellValue('E'.$i, 'VALOR')
			->setCellValue('F'.$i, 'CEDULAS')
			->setCellValue('G'.$i, 'VALOR')	
			->setCellValue('H'.$i, 'CEDULAS')
			->setCellValue('I'.$i, 'RECAUDO')
			->setCellValue('J'.$i, 'CEDULAS')
			->setCellValue('K'.$i, 'RECAUDO')	
			->setCellValue('L'.$i, 'CEDULAS')
			->setCellValue('M'.$i, 'RECAUDO')	
			->setCellValue('N'.$i, 'CEDULAS')
			->setCellValue('O'.$i, 'RECAUDO')	
			->setCellValue('P'.$i, 'CEDULAS')
			->setCellValue('Q'.$i, 'RECAUDO')	
			->setCellValue('R'.$i, 'CEDULAS')
			->setCellValue('S'.$i, 'RECAUDO')	
			->setCellValue('T'.$i, 'CEDULAS')
			->setCellValue('U'.$i, 'RECAUDO')	
			->setCellValue('V'.$i, 'CEDULAS')
			->setCellValue('W'.$i, 'RECAUDO')	
			->setCellValue('X'.$i, 'CEDULAS')
			->setCellValue('Y'.$i, 'RECAUDO')	
			->setCellValue('Z'.$i, 'CEDULAS')
			->setCellValue('AA'.$i, 'RECAUDO')	
			->setCellValue('AB'.$i, 'CEDULAS')
			->setCellValue('AC'.$i, 'RECAUDO')	
			->setCellValue('AD'.$i, 'CEDULAS')
			->setCellValue('AE'.$i, 'RECAUDO')	
			->setCellValue('AF'.$i, 'CEDULAS')
			->setCellValue('AG'.$i, 'RECAUDO')	
			->setCellValue('AH'.$i, 'CEDULAS')
			->setCellValue('AI'.$i, 'RECAUDO')	
			->setCellValue('AJ'.$i, 'CEDULAS')
			->setCellValue('AK'.$i, 'RECAUDO')	
			->setCellValue('AL'.$i, 'CEDULAS')
			->setCellValue('AM'.$i, 'RECAUDO')	
			->setCellValue('AN'.$i, 'CEDULAS')
			->setCellValue('AO'.$i, 'RECAUDO')	
			->setCellValue('AP'.$i, 'CEDULAS')
			->setCellValue('AQ'.$i, 'RECAUDO')	
			->setCellValue('AR'.$i, 'CEDULAS')
			->setCellValue('AS'.$i, 'RECAUDO')	
			->setCellValue('AT'.$i, 'CEDULAS')
			->setCellValue('AU'.$i, 'RECAUDO')	
			->setCellValue('AV'.$i, 'CEDULAS')
			->setCellValue('AW'.$i, 'RECAUDO')	
			->setCellValue('AX'.$i, 'CEDULAS')
			->setCellValue('AY'.$i, 'RECAUDO')	
			->setCellValue('AZ'.$i, 'CEDULAS')
			->setCellValue('BA'.$i, 'RECAUDO')	
			->setCellValue('BB'.$i, 'CEDULAS')
			->setCellValue('BC'.$i, 'RECAUDO')	
			->setCellValue('BD'.$i, 'CEDULAS')
			->setCellValue('BE'.$i, 'RECAUDO')	
			->setCellValue('BF'.$i, 'CEDULAS')
			->setCellValue('BG'.$i, 'RECAUDO')	
			->setCellValue('BH'.$i, 'CEDULAS')
			->setCellValue('BI'.$i, 'RECAUDO')	
			->setCellValue('BJ'.$i, 'CEDULAS')
			->setCellValue('BK'.$i, 'RECAUDO')	
			->setCellValue('BL'.$i, 'CEDULAS')
			->setCellValue('BM'.$i, 'RECAUDO')	
			->setCellValue('BN'.$i, 'CEDULAS')
			->setCellValue('BO'.$i, 'RECAUDO')	
			->setCellValue('BP'.$i, 'CEDULAS')
			->setCellValue('BQ'.$i, 'RECAUDO');
			$i++;	

$sql = "SELECT * FROM comoVamos INNER JOIN usuarios ON usuid = comvausuario INNER JOIN comovamosgrupodeta ON grupousuario = comvausuario WHERE comvafecha = '$fecha' AND grupofecha = '$fecha';";
$qry = $db->query($sql);			

while($row = $qry->fetch(PDO::FETCH_ASSOC)){
if ($row['gruponombre'] == 'MORA ALTA') {
	
	$sqlrealizado = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha >= '$fecha'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";

	$qryrealizado = $db->query($sqlrealizado);	
	$rowrealizado = $qryrealizado->fetch(PDO::FETCH_ASSOC);	
	
	$porcencedulas = ($rowrealizado['Cedulas'] / $row['comvacedulas']) * 100;
	$porcenvalor = ($rowrealizado['Valor'] /  $row['comvavalor']) * 100;
	
	$fechasindias = substr($fecha, 0, -2);
	

	$sqldia1 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."01'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia1 = $db->query($sqldia1);	
	$rowdia1 = $qrydia1->fetch(PDO::FETCH_ASSOC);
	
	$cedulas1 = $rowdia1['Cedulas'];
	$recaudo1 = $rowdia1['Valor'];
	
	
	$sqldia2 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."02'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia2 = $db->query($sqldia2);	
	$rowdia2 = $qrydia2->fetch(PDO::FETCH_ASSOC);
	
	$cedulas2 = $rowdia2['Cedulas'];
	$recaudo2 = $rowdia2['Valor'];


	$sqldia3 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."03'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia3 = $db->query($sqldia3);	
	$rowdia3 = $qrydia3->fetch(PDO::FETCH_ASSOC);
	
	$cedulas3 = $rowdia3['Cedulas'];
	$recaudo3 = $rowdia3['Valor'];


	$sqldia4 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."04'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia4 = $db->query($sqldia4);	
	$rowdia4 = $qrydia4->fetch(PDO::FETCH_ASSOC);
	
	$cedulas4 = $rowdia4['Cedulas'];
	$recaudo4 = $rowdia4['Valor'];


	$sqldia5 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."05'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia5 = $db->query($sqldia5);	
	$rowdia5 = $qrydia5->fetch(PDO::FETCH_ASSOC);
	
	$cedulas5 = $rowdia5['Cedulas'];
	$recaudo5 = $rowdia5['Valor'];


	$sqldia6 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."06'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia6 = $db->query($sqldia6);	
	$rowdia6 = $qrydia6->fetch(PDO::FETCH_ASSOC);
	
	$cedulas6 = $rowdia6['Cedulas'];
	$recaudo6 = $rowdia6['Valor'];


	$sqldia7 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."07'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia7 = $db->query($sqldia7);	
	$rowdia7 = $qrydia7->fetch(PDO::FETCH_ASSOC);
	
	$cedulas7 = $rowdia7['Cedulas'];
	$recaudo7 = $rowdia7['Valor'];


	$sqldia8 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."08'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia8 = $db->query($sqldia8);	
	$rowdia8 = $qrydia8->fetch(PDO::FETCH_ASSOC);
	
	$cedulas8 = $rowdia8['Cedulas'];
	$recaudo8 = $rowdia8['Valor'];


	$sqldia9 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."09'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia9 = $db->query($sqldia9);	
	$rowdia9 = $qrydia9->fetch(PDO::FETCH_ASSOC);
	
	$cedulas9 = $rowdia9['Cedulas'];
	$recaudo9 = $rowdia9['Valor'];


		
	$sqldia10 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."10'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia10 = $db->query($sqldia10);	
	$rowdia10 = $qrydia10->fetch(PDO::FETCH_ASSOC);
	
	$cedulas10 = $rowdia10['Cedulas'];
	$recaudo10 = $rowdia10['Valor'];


		
	$sqldia11 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."11'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia11 = $db->query($sqldia11);	
	$rowdia11 = $qrydia11->fetch(PDO::FETCH_ASSOC);
	
	$cedulas11 = $rowdia11['Cedulas'];
	$recaudo11 = $rowdia11['Valor'];


	
	$sqldia12 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."12'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia12 = $db->query($sqldia12);	
	$rowdia12 = $qrydia12->fetch(PDO::FETCH_ASSOC);
	
	$cedulas12 = $rowdia12['Cedulas'];
	$recaudo12 = $rowdia12['Valor'];


	
	$sqldia13 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."13'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia13 = $db->query($sqldia13);	
	$rowdia13 = $qrydia13->fetch(PDO::FETCH_ASSOC);
	
	$cedulas13 = $rowdia13['Cedulas'];
	$recaudo13 = $rowdia13['Valor'];


	
	$sqldia14 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."14'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia14 = $db->query($sqldia14);	
	$rowdia14 = $qrydia14->fetch(PDO::FETCH_ASSOC);
	
	$cedulas14 = $rowdia14['Cedulas'];
	$recaudo14 = $rowdia14['Valor'];


	
	$sqldia15 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."15'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia15 = $db->query($sqldia15);	
	$rowdia15 = $qrydia15->fetch(PDO::FETCH_ASSOC);
	
	$cedulas15 = $rowdia15['Cedulas'];
	$recaudo15 = $rowdia15['Valor'];


	
	$sqldia16 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."16'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia16 = $db->query($sqldia16);	
	$rowdia16 = $qrydia16->fetch(PDO::FETCH_ASSOC);
	
	$cedulas16 = $rowdia16['Cedulas'];
	$recaudo16 = $rowdia16['Valor'];


	
	$sqldia17 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."17'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia17 = $db->query($sqldia17);	
	$rowdia17 = $qrydia17->fetch(PDO::FETCH_ASSOC);
	
	$cedulas17 = $rowdia17['Cedulas'];
	$recaudo17 = $rowdia17['Valor'];


	
	$sqldia18 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."18'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia18 = $db->query($sqldia18);	
	$rowdia18 = $qrydia18->fetch(PDO::FETCH_ASSOC);
	
	$cedulas18 = $rowdia18['Cedulas'];
	$recaudo18 = $rowdia18['Valor'];


	
	$sqldia19 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."19'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia19 = $db->query($sqldia19);	
	$rowdia19 = $qrydia19->fetch(PDO::FETCH_ASSOC);
	
	$cedulas19 = $rowdia19['Cedulas'];
	$recaudo19 = $rowdia19['Valor'];


	
	$sqldia20 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."20'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia20 = $db->query($sqldia20);	
	$rowdia20 = $qrydia20->fetch(PDO::FETCH_ASSOC);
	
	$cedulas20 = $rowdia20['Cedulas'];
	$recaudo20 = $rowdia20['Valor'];


	
	$sqldia21 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."21'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia21 = $db->query($sqldia21);	
	$rowdia21 = $qrydia21->fetch(PDO::FETCH_ASSOC);
	
	$cedulas21 = $rowdia21['Cedulas'];
	$recaudo21 = $rowdia21['Valor'];


	
	$sqldia22 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."22'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia22 = $db->query($sqldia22);	
	$rowdia22 = $qrydia22->fetch(PDO::FETCH_ASSOC);
	
	$cedulas22 = $rowdia22['Cedulas'];
	$recaudo22 = $rowdia22['Valor'];


	
	$sqldia23 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."23'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia23 = $db->query($sqldia23);	
	$rowdia23 = $qrydia23->fetch(PDO::FETCH_ASSOC);
	
	$cedulas23 = $rowdia23['Cedulas'];
	$recaudo23 = $rowdia23['Valor'];


	
	$sqldia24 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."24'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia24 = $db->query($sqldia24);	
	$rowdia24 = $qrydia24->fetch(PDO::FETCH_ASSOC);
	
	$cedulas24 = $rowdia24['Cedulas'];
	$recaudo24 = $rowdia24['Valor'];


	
	$sqldia25 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."25'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia25 = $db->query($sqldia25);	
	$rowdia25 = $qrydia25->fetch(PDO::FETCH_ASSOC);
	
	$cedulas25 = $rowdia25['Cedulas'];
	$recaudo25 = $rowdia25['Valor'];


	
	$sqldia26 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."26'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia26 = $db->query($sqldia26);	
	$rowdia26 = $qrydia26->fetch(PDO::FETCH_ASSOC);
	
	$cedulas26 = $rowdia26['Cedulas'];
	$recaudo26 = $rowdia26['Valor'];


	
	$sqldia27 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."27'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia27 = $db->query($sqldia27);	
	$rowdia27 = $qrydia27->fetch(PDO::FETCH_ASSOC);
	
	$cedulas27 = $rowdia27['Cedulas'];
	$recaudo27 = $rowdia27['Valor'];


	
	$sqldia28 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."28'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia28 = $db->query($sqldia28);	
	$rowdia28 = $qrydia28->fetch(PDO::FETCH_ASSOC);
	
	$cedulas28 = $rowdia28['Cedulas'];
	$recaudo28 = $rowdia28['Valor'];


	
	$sqldia29 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."29'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia29 = $db->query($sqldia29);	
	$rowdia29 = $qrydia29->fetch(PDO::FETCH_ASSOC);
	
	$cedulas29 = $rowdia29['Cedulas'];
	$recaudo29 = $rowdia29['Valor'];


	
	$sqldia30 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."30'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia30 = $db->query($sqldia30);	
	$rowdia30 = $qrydia30->fetch(PDO::FETCH_ASSOC);
	
	$cedulas30 = $rowdia30['Cedulas'];
	$recaudo30 = $rowdia30['Valor'];


	
	$sqldia31 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."31'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia31 = $db->query($sqldia31);	
	$rowdia31 = $qrydia31->fetch(PDO::FETCH_ASSOC);
	
	$cedulas31 = $rowdia31['Cedulas'];
	$recaudo31 = $rowdia31['Valor'];

	$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':BQ'.$i)->applyFromArray($styleArray);
	$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('A'.$i, $row['usunombre'])
    	->setCellValue('B'.$i, $row['comvacedulas'])
		->setCellValue('C'.$i, $row['comvavalor'])
		->setCellValue('D'.$i, $rowrealizado['Cedulas'])
		->setCellValue('E'.$i, $rowrealizado['Valor'])
		->setCellValue('F'.$i, round($porcencedulas,1).' %')
		->setCellValue('G'.$i, round($porcenvalor,1).' %')
		->setCellValue('H'.$i, $cedulas1)
		->setCellValue('I'.$i, $recaudo1)
		->setCellValue('J'.$i, $cedulas2)
		->setCellValue('K'.$i, $recaudo2)	
		->setCellValue('L'.$i, $cedulas3)
		->setCellValue('M'.$i, $recaudo3)	
		->setCellValue('N'.$i, $cedulas4)
		->setCellValue('O'.$i, $recaudo4)	
		->setCellValue('P'.$i, $cedulas5)
		->setCellValue('Q'.$i, $recaudo5)	
		->setCellValue('R'.$i, $cedulas6)
		->setCellValue('S'.$i, $recaudo6)	
		->setCellValue('T'.$i, $cedulas7)
		->setCellValue('U'.$i, $recaudo7)	
		->setCellValue('V'.$i, $cedulas8)
		->setCellValue('W'.$i, $recaudo8)	
		->setCellValue('X'.$i, $cedulas9)
		->setCellValue('Y'.$i, $recaudo9)	
		->setCellValue('Z'.$i, $cedulas10)
		->setCellValue('AA'.$i, $recaudo10)	
		->setCellValue('AB'.$i, $cedulas11)
		->setCellValue('AC'.$i, $recaudo11)	
		->setCellValue('AD'.$i, $cedulas12)
		->setCellValue('AE'.$i, $recaudo12)	
		->setCellValue('AF'.$i, $cedulas13)
		->setCellValue('AG'.$i, $recaudo13)	
		->setCellValue('AH'.$i, $cedulas14)
		->setCellValue('AI'.$i, $recaudo14)	
		->setCellValue('AJ'.$i, $cedulas15)
		->setCellValue('AK'.$i, $recaudo15)	
		->setCellValue('AL'.$i, $cedulas16)
		->setCellValue('AM'.$i, $recaudo16)	
		->setCellValue('AN'.$i, $cedulas17)
		->setCellValue('AO'.$i, $recaudo17)	
		->setCellValue('AP'.$i, $cedulas18)
		->setCellValue('AQ'.$i, $recaudo18)	
		->setCellValue('AR'.$i, $cedulas19)
		->setCellValue('AS'.$i, $recaudo19)	
		->setCellValue('AT'.$i, $cedulas20)
		->setCellValue('AU'.$i, $recaudo20)	
		->setCellValue('AV'.$i, $cedulas21)
		->setCellValue('AW'.$i, $recaudo21)	
		->setCellValue('AX'.$i, $cedulas22)
		->setCellValue('AY'.$i, $recaudo22)	
		->setCellValue('AZ'.$i, $cedulas23)
		->setCellValue('BA'.$i, $recaudo23)	
		->setCellValue('BB'.$i, $cedulas24)
		->setCellValue('BC'.$i, $recaudo24)	
		->setCellValue('BD'.$i, $cedulas25)
		->setCellValue('BE'.$i, $recaudo25)	
		->setCellValue('BF'.$i, $cedulas26)
		->setCellValue('BG'.$i, $recaudo26)	
		->setCellValue('BH'.$i, $cedulas27)
		->setCellValue('BI'.$i, $recaudo27)	
		->setCellValue('BJ'.$i, $cedulas28)
		->setCellValue('BK'.$i, $recaudo28)	
		->setCellValue('BL'.$i, $cedulas29)
		->setCellValue('BM'.$i, $recaudo29)	
		->setCellValue('BN'.$i, $cedulas30)
		->setCellValue('BO'.$i, $recaudo30)	
		->setCellValue('BP'.$i, $cedulas31)
		->setCellValue('BQ'.$i, $recaudo31);	
	$i++;

	$tcedulas = $row['comvacedulas'] + $tcedulas;
	$tvalor = $row['comvavalor'] + $tvalor;
	$tcedurealizado = $rowrealizado['Cedulas'] + $tcedurealizado;
	$tvalorrealizado = $rowrealizado['Valor'] + $tvalorrealizado;
	$tporcencedulas = ($tcedurealizado / $tcedulas) * 100;
	$tporcenvalor = ($tvalorrealizado /  $tvalor) * 100;
	
$tcedulas1 = $rowdia1['Cedulas'] + $tcedulas1;
$trecaudo1 = $rowdia1['Valor'] + $trecaudo1;
$tcedulas2 = $rowdia2['Cedulas'] + $tcedulas2;
$trecaudo2 = $rowdia2['Valor'] + $trecaudo2;
$tcedulas3 = $rowdia3['Cedulas'] + $tcedulas3;
$trecaudo3 = $rowdia3['Valor'] + $trecaudo3;
$tcedulas4 = $rowdia4['Cedulas'] + $tcedulas4;
$trecaudo4 = $rowdia4['Valor'] + $trecaudo4;
$tcedulas5 = $rowdia5['Cedulas'] + $tcedulas5;
$trecaudo5 = $rowdia5['Valor'] + $trecaudo5;
$tcedulas6 = $rowdia6['Cedulas'] + $tcedulas6;
$trecaudo6 = $rowdia6['Valor'] + $trecaudo6;
$tcedulas7 = $rowdia7['Cedulas'] + $tcedulas7;
$trecaudo7 = $rowdia7['Valor'] + $trecaudo7;
$tcedulas8 = $rowdia8['Cedulas'] + $tcedulas8;
$trecaudo8 = $rowdia8['Valor'] + $trecaudo8;
$tcedulas9 = $rowdia9['Cedulas'] + $tcedulas9;
$trecaudo9 = $rowdia9['Valor'] + $trecaudo9;
$tcedulas10 = $rowdia10['Cedulas'] + $tcedulas10;
$trecaudo10 = $rowdia10['Valor'] + $trecaudo10;
$tcedulas11 = $rowdia11['Cedulas'] + $tcedulas11;
$trecaudo11 = $rowdia11['Valor'] + $trecaudo11;
$tcedulas12 = $rowdia12['Cedulas'] + $tcedulas12;
$trecaudo12 = $rowdia12['Valor'] + $trecaudo12;
$tcedulas13 = $rowdia13['Cedulas'] + $tcedulas13;
$trecaudo13 = $rowdia13['Valor'] + $trecaudo13;
$tcedulas14 = $rowdia14['Cedulas'] + $tcedulas14;
$trecaudo14 = $rowdia14['Valor'] + $trecaudo14;
$tcedulas15 = $rowdia15['Cedulas'] + $tcedulas15;
$trecaudo15 = $rowdia15['Valor'] + $trecaudo15;
$tcedulas16 = $rowdia16['Cedulas'] + $tcedulas16;
$trecaudo16 = $rowdia16['Valor'] + $trecaudo16;
$tcedulas17 = $rowdia17['Cedulas'] + $tcedulas17;
$trecaudo17 = $rowdia17['Valor'] + $trecaudo17;
$tcedulas18 = $rowdia18['Cedulas'] + $tcedulas18;
$trecaudo18 = $rowdia18['Valor'] + $trecaudo18;
$tcedulas19 = $rowdia19['Cedulas'] + $tcedulas19;
$trecaudo19 = $rowdia19['Valor'] + $trecaudo19;
$tcedulas20 = $rowdia20['Cedulas'] + $tcedulas20;
$trecaudo20 = $rowdia20['Valor'] + $trecaudo20;
$tcedulas21 = $rowdia21['Cedulas'] + $tcedulas21;
$trecaudo21 = $rowdia21['Valor'] + $trecaudo21;
$tcedulas22 = $rowdia22['Cedulas'] + $tcedulas22;
$trecaudo22 = $rowdia22['Valor'] + $trecaudo22;
$tcedulas23 = $rowdia23['Cedulas'] + $tcedulas23;
$trecaudo23 = $rowdia23['Valor'] + $trecaudo23;
$tcedulas24 = $rowdia24['Cedulas'] + $tcedulas24;
$trecaudo24 = $rowdia24['Valor'] + $trecaudo24;
$tcedulas25 = $rowdia25['Cedulas'] + $tcedulas25;
$trecaudo25 = $rowdia25['Valor'] + $trecaudo25;
$tcedulas26 = $rowdia26['Cedulas'] + $tcedulas26;
$trecaudo26 = $rowdia26['Valor'] + $trecaudo26;
$tcedulas27 = $rowdia27['Cedulas'] + $tcedulas27;
$trecaudo27 = $rowdia27['Valor'] + $trecaudo27;
$tcedulas28 = $rowdia28['Cedulas'] + $tcedulas28;
$trecaudo28 = $rowdia28['Valor'] + $trecaudo28;
$tcedulas29 = $rowdia29['Cedulas'] + $tcedulas29;
$trecaudo29 = $rowdia29['Valor'] + $trecaudo29;
$tcedulas30 = $rowdia30['Cedulas'] + $tcedulas30;
$trecaudo30 = $rowdia30['Valor'] + $trecaudo30;
$tcedulas31 = $rowdia31['Cedulas'] + $tcedulas31;
$trecaudo31 = $rowdia31['Valor'] + $trecaudo31;

	
$tcedulas1moraalta =  $tcedulas1;
$trecaudo1moraalta =  $trecaudo1;
$tcedulas2moraalta =  $tcedulas2;
$trecaudo2moraalta =  $trecaudo2;
$tcedulas3moraalta =  $tcedulas3;
$trecaudo3moraalta = $trecaudo3;
$tcedulas4moraalta =  $tcedulas4;
$trecaudo4moraalta =  $trecaudo4;
$tcedulas5moraalta =  $tcedulas5;
$trecaudo5moraalta =  $trecaudo5;
$tcedulas6moraalta = $tcedulas6;
$trecaudo6moraalta =  $trecaudo6;
$tcedulas7moraalta = $tcedulas7;
$trecaudo7moraalta =  $trecaudo7;
$tcedulas8moraalta =  $tcedulas8;
$trecaudo8moraalta = $trecaudo8;
$tcedulas9moraalta = $tcedulas9;
$trecaudo9moraalta =  $trecaudo9;
$tcedulas10moraalta = $tcedulas10;
$trecaudo10moraalta = $trecaudo10;
$tcedulas11moraalta =  $tcedulas11;
$trecaudo11moraalta =  $trecaudo11;
$tcedulas12moraalta =  $tcedulas12;
$trecaudo12moraalta =  $trecaudo12;
$tcedulas13moraalta =  $tcedulas13;
$trecaudo13moraalta = $trecaudo13;
$tcedulas14moraalta =  $tcedulas14;
$trecaudo14moraalta =  $trecaudo14;
$tcedulas15moraalta =  $tcedulas15;
$trecaudo15moraalta =  $trecaudo15;
$tcedulas16moraalta =  $tcedulas16;
$trecaudo16moraalta =  $trecaudo16;
$tcedulas17moraalta =  $tcedulas17;
$trecaudo17moraalta =  $trecaudo17;
$tcedulas18moraalta =  $tcedulas18;
$trecaudo18moraalta =  $trecaudo18;
$tcedulas19moraalta =  $tcedulas19;
$trecaudo19moraalta = $trecaudo19;
$tcedulas20moraalta =  $tcedulas20;
$trecaudo20moraalta =  $trecaudo20;
$tcedulas21moraalta =  $tcedulas21;
$trecaudo21moraalta =  $trecaudo21;
$tcedulas22moraalta =  $tcedulas22;
$trecaudo22moraalta =  $trecaudo22;
$tcedulas23moraalta =  $tcedulas23;
$trecaudo23moraalta =  $trecaudo23;
$tcedulas24moraalta =  $tcedulas24;
$trecaudo24moraalta =  $trecaudo24;
$tcedulas25moraalta =  $tcedulas25;
$trecaudo25moraalta =  $trecaudo25;
$tcedulas26moraalta =  $tcedulas26;
$trecaudo26moraalta = $trecaudo26;
$tcedulas27moraalta = $tcedulas27;
$trecaudo27moraalta =  $trecaudo27;
$tcedulas28moraalta =  $tcedulas28;
$trecaudo28moraalta =  $trecaudo28;
$tcedulas29moraalta =  $tcedulas29;
$trecaudo29moraalta =  $trecaudo29;
$tcedulas30moraalta =  $tcedulas30;
$trecaudo30moraalta =  $trecaudo30;
$tcedulas31moraalta =  $tcedulas31;
$trecaudo31moraalta =  $trecaudo31;
$tcedulasmoraalta = $tcedulas;
$tvalormoraalta = $tvlor;
$tcedurealizadomoraalta = $tcedurealizado;
$tvalorrealizadomoraalta = $tvalorrealizado;




}
}
	$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':BQ'.$i)->applyFromArray($styleArrayTwo);
	$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('A'.$i, 'TOTAL')
    	->setCellValue('B'.$i, $tcedulas)
		->setCellValue('C'.$i, $tvalor)
		->setCellValue('D'.$i, $tcedurealizado)
		->setCellValue('E'.$i, $tvalorrealizado)
		->setCellValue('F'.$i, round($tporcencedulas,1).' %')
		->setCellValue('G'.$i, round($tporcenvalor,1).' %')
		->setCellValue('H'.$i, $tcedulas1)
		->setCellValue('I'.$i, $trecaudo1)
		->setCellValue('J'.$i, $tcedulas2)
		->setCellValue('K'.$i, $trecaudo2)	
		->setCellValue('L'.$i, $tcedulas3)
		->setCellValue('M'.$i, $trecaudo3)	
		->setCellValue('N'.$i, $tcedulas4)
		->setCellValue('O'.$i, $trecaudo4)	
		->setCellValue('P'.$i, $tcedulas5)
		->setCellValue('Q'.$i, $trecaudo5)	
		->setCellValue('R'.$i, $tcedulas6)
		->setCellValue('S'.$i, $trecaudo6)	
		->setCellValue('T'.$i, $tcedulas7)
		->setCellValue('U'.$i, $trecaudo7)	
		->setCellValue('V'.$i, $tcedulas8)
		->setCellValue('W'.$i, $trecaudo8)	
		->setCellValue('X'.$i, $tcedulas9)
		->setCellValue('Y'.$i, $trecaudo9)	
		->setCellValue('Z'.$i, $tcedulas10)
		->setCellValue('AA'.$i, $trecaudo10)	
		->setCellValue('AB'.$i, $tcedulas11)
		->setCellValue('AC'.$i, $trecaudo11)	
		->setCellValue('AD'.$i, $tcedulas12)
		->setCellValue('AE'.$i, $trecaudo12)	
		->setCellValue('AF'.$i, $tcedulas13)
		->setCellValue('AG'.$i, $trecaudo13)	
		->setCellValue('AH'.$i, $tcedulas14)
		->setCellValue('AI'.$i, $trecaudo14)	
		->setCellValue('AJ'.$i, $tcedulas15)
		->setCellValue('AK'.$i, $trecaudo15)	
		->setCellValue('AL'.$i, $tcedulas16)
		->setCellValue('AM'.$i, $trecaudo16)	
		->setCellValue('AN'.$i, $tcedulas17)
		->setCellValue('AO'.$i, $trecaudo17)	
		->setCellValue('AP'.$i, $tcedulas18)
		->setCellValue('AQ'.$i, $trecaudo18)	
		->setCellValue('AR'.$i, $tcedulas19)
		->setCellValue('AS'.$i, $trecaudo19)	
		->setCellValue('AT'.$i, $tcedulas20)
		->setCellValue('AU'.$i, $trecaudo20)	
		->setCellValue('AV'.$i, $tcedulas21)
		->setCellValue('AW'.$i, $trecaudo21)	
		->setCellValue('AX'.$i, $tcedulas22)
		->setCellValue('AY'.$i, $trecaudo22)	
		->setCellValue('AZ'.$i, $tcedulas23)
		->setCellValue('BA'.$i, $trecaudo23)	
		->setCellValue('BB'.$i, $tcedulas24)
		->setCellValue('BC'.$i, $trecaudo24)	
		->setCellValue('BD'.$i, $tcedulas25)
		->setCellValue('BE'.$i, $trecaudo25)	
		->setCellValue('BF'.$i, $tcedulas26)
		->setCellValue('BG'.$i, $trecaudo26)	
		->setCellValue('BH'.$i, $tcedulas27)
		->setCellValue('BI'.$i, $trecaudo27)	
		->setCellValue('BJ'.$i, $tcedulas28)
		->setCellValue('BK'.$i, $trecaudo28)	
		->setCellValue('BL'.$i, $tcedulas29)
		->setCellValue('BM'.$i, $trecaudo29)	
		->setCellValue('BN'.$i, $tcedulas30)
		->setCellValue('BO'.$i, $trecaudo30)	
		->setCellValue('BP'.$i, $tcedulas31)
		->setCellValue('BQ'.$i, $trecaudo31);	
	$i++;				
	

// ----------------------------------------------------------------------------------------------------------


$tcedulas1  = 0;
$tcedulas2  = 0;
$tcedulas3  = 0;
$tcedulas4  = 0;
$tcedulas5  = 0;
$tcedulas6  = 0;
$tcedulas7  = 0;
$tcedulas8  = 0;
$tcedulas9  = 0;
$tcedulas10  = 0;
$tcedulas11  = 0;
$tcedulas12  = 0;
$tcedulas13  = 0;
$tcedulas14  = 0;
$tcedulas15  = 0;
$tcedulas16  = 0;
$tcedulas17  = 0;
$tcedulas18  = 0;
$tcedulas19  = 0;
$tcedulas20  = 0;
$tcedulas21  = 0;
$tcedulas22  = 0;
$tcedulas23  = 0;
$tcedulas24  = 0;
$tcedulas25  = 0;
$tcedulas26  = 0;
$tcedulas27  = 0;
$tcedulas28  = 0;
$tcedulas29  = 0;
$tcedulas30  = 0;
$tcedulas31  = 0;
$trecaudo1  = 0;
$trecaudo2  = 0;
$trecaudo3  = 0;
$trecaudo4  = 0;
$trecaudo5  = 0;
$trecaudo6  = 0;
$trecaudo7  = 0;
$trecaudo8  = 0;
$trecaudo9  = 0;
$trecaudo10  = 0;
$trecaudo11  = 0;
$trecaudo12  = 0;
$trecaudo13  = 0;
$trecaudo14  = 0;
$trecaudo15  = 0;
$trecaudo16  = 0;
$trecaudo17  = 0;
$trecaudo18  = 0;
$trecaudo19  = 0;
$trecaudo20  = 0;
$trecaudo21  = 0;
$trecaudo22  = 0;
$trecaudo23  = 0;
$trecaudo24  = 0;
$trecaudo25  = 0;
$trecaudo26  = 0;
$trecaudo27  = 0;
$trecaudo28  = 0;
$trecaudo29  = 0;
$trecaudo30  = 0;
$trecaudo31  = 0;
$tcedulas = 0;
$tvalor = 0;
$tcedurealizado = 0;
$tvalorrealizado = 0;
$tporcencedulas = 0;
$tporcenvalor = 0;

	
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$i.':C'.$i);
$objPHPExcel->getActiveSheet()->getStyle('A'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$i, 'CASTIGADA');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('D'.$i.':E'.$i);
$objPHPExcel->getActiveSheet()->getStyle('D'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$i, 'REALIZADO');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('F'.$i.':G'.$i);
$objPHPExcel->getActiveSheet()->getStyle('F'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$i, ' % REALIZADO');
 
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('H'.$i.':I'.$i);
$objPHPExcel->getActiveSheet()->getStyle('H'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$i, '1');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('J'.$i.':K'.$i);
$objPHPExcel->getActiveSheet()->getStyle('J'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.$i, '2');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('L'.$i.':M'.$i);
$objPHPExcel->getActiveSheet()->getStyle('L'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('L'.$i, '3');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('N'.$i.':O'.$i);
$objPHPExcel->getActiveSheet()->getStyle('N'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('N'.$i, '4');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('P'.$i.':Q'.$i);
$objPHPExcel->getActiveSheet()->getStyle('P'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('P'.$i, '5');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('R'.$i.':S'.$i);
$objPHPExcel->getActiveSheet()->getStyle('L'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('R'.$i, '6');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('T'.$i.':U'.$i);
$objPHPExcel->getActiveSheet()->getStyle('T'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('T'.$i, '7');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('V'.$i.':W'.$i);
$objPHPExcel->getActiveSheet()->getStyle('V'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('V'.$i, '8');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('X'.$i.':Y'.$i);
$objPHPExcel->getActiveSheet()->getStyle('X'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('X'.$i, '9');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('Z'.$i.':AA'.$i);
$objPHPExcel->getActiveSheet()->getStyle('Z'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('Z'.$i, '10');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AB'.$i.':AC'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AB'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AB'.$i, '11');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AB'.$i.':AC'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AB'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AB'.$i, '11');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AD'.$i.':AE'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AD'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AD'.$i, '12');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AF'.$i.':AG'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AF'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AF'.$i, '13');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AH'.$i.':AI'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AH'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AH'.$i, '14');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AJ'.$i.':AK'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AJ'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AJ'.$i, '15');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AL'.$i.':AM'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AL'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AL'.$i, '16');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AN'.$i.':AO'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AN'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AN'.$i, '17');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AP'.$i.':AQ'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AP'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AP'.$i, '18');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AR'.$i.':AS'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AR'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AR'.$i, '19');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AT'.$i.':AU'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AT'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AT'.$i, '20');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AV'.$i.':AW'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AV'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AV'.$i, '21');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AX'.$i.':AY'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AX'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AX'.$i, '22');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AZ'.$i.':BA'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AZ'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AZ'.$i, '23');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BB'.$i.':BC'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BB'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BB'.$i, '24');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BD'.$i.':BE'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BD'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BD'.$i, '25');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BF'.$i.':BG'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BF'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BF'.$i, '26');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BH'.$i.':BI'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BH'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BH'.$i, '27');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BJ'.$i.':BK'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BJ'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BJ'.$i, '28');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BL'.$i.':BM'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BL'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BL'.$i, '29');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BN'.$i.':BO'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BN'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BN'.$i, '30');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BP'.$i.':BQ'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BP'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BP'.$i, '31');


$i++;
$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':BQ'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$i, 'ASESOR')
            ->setCellValue('B'.$i, 'CEDULAS')
            ->setCellValue('C'.$i, 'VALOR')
            ->setCellValue('D'.$i, 'CEDULAS')
			->setCellValue('E'.$i, 'VALOR')
			->setCellValue('F'.$i, 'CEDULAS')
			->setCellValue('G'.$i, 'VALOR')	
			->setCellValue('H'.$i, 'CEDULAS')
			->setCellValue('I'.$i, 'RECAUDO')
			->setCellValue('J'.$i, 'CEDULAS')
			->setCellValue('K'.$i, 'RECAUDO')	
			->setCellValue('L'.$i, 'CEDULAS')
			->setCellValue('M'.$i, 'RECAUDO')	
			->setCellValue('N'.$i, 'CEDULAS')
			->setCellValue('O'.$i, 'RECAUDO')	
			->setCellValue('P'.$i, 'CEDULAS')
			->setCellValue('Q'.$i, 'RECAUDO')	
			->setCellValue('R'.$i, 'CEDULAS')
			->setCellValue('S'.$i, 'RECAUDO')	
			->setCellValue('T'.$i, 'CEDULAS')
			->setCellValue('U'.$i, 'RECAUDO')	
			->setCellValue('V'.$i, 'CEDULAS')
			->setCellValue('W'.$i, 'RECAUDO')	
			->setCellValue('X'.$i, 'CEDULAS')
			->setCellValue('Y'.$i, 'RECAUDO')	
			->setCellValue('Z'.$i, 'CEDULAS')
			->setCellValue('AA'.$i, 'RECAUDO')	
			->setCellValue('AB'.$i, 'CEDULAS')
			->setCellValue('AC'.$i, 'RECAUDO')	
			->setCellValue('AD'.$i, 'CEDULAS')
			->setCellValue('AE'.$i, 'RECAUDO')	
			->setCellValue('AF'.$i, 'CEDULAS')
			->setCellValue('AG'.$i, 'RECAUDO')	
			->setCellValue('AH'.$i, 'CEDULAS')
			->setCellValue('AI'.$i, 'RECAUDO')	
			->setCellValue('AJ'.$i, 'CEDULAS')
			->setCellValue('AK'.$i, 'RECAUDO')	
			->setCellValue('AL'.$i, 'CEDULAS')
			->setCellValue('AM'.$i, 'RECAUDO')	
			->setCellValue('AN'.$i, 'CEDULAS')
			->setCellValue('AO'.$i, 'RECAUDO')	
			->setCellValue('AP'.$i, 'CEDULAS')
			->setCellValue('AQ'.$i, 'RECAUDO')	
			->setCellValue('AR'.$i, 'CEDULAS')
			->setCellValue('AS'.$i, 'RECAUDO')	
			->setCellValue('AT'.$i, 'CEDULAS')
			->setCellValue('AU'.$i, 'RECAUDO')	
			->setCellValue('AV'.$i, 'CEDULAS')
			->setCellValue('AW'.$i, 'RECAUDO')	
			->setCellValue('AX'.$i, 'CEDULAS')
			->setCellValue('AY'.$i, 'RECAUDO')	
			->setCellValue('AZ'.$i, 'CEDULAS')
			->setCellValue('BA'.$i, 'RECAUDO')	
			->setCellValue('BB'.$i, 'CEDULAS')
			->setCellValue('BC'.$i, 'RECAUDO')	
			->setCellValue('BD'.$i, 'CEDULAS')
			->setCellValue('BE'.$i, 'RECAUDO')	
			->setCellValue('BF'.$i, 'CEDULAS')
			->setCellValue('BG'.$i, 'RECAUDO')	
			->setCellValue('BH'.$i, 'CEDULAS')
			->setCellValue('BI'.$i, 'RECAUDO')	
			->setCellValue('BJ'.$i, 'CEDULAS')
			->setCellValue('BK'.$i, 'RECAUDO')	
			->setCellValue('BL'.$i, 'CEDULAS')
			->setCellValue('BM'.$i, 'RECAUDO')	
			->setCellValue('BN'.$i, 'CEDULAS')
			->setCellValue('BO'.$i, 'RECAUDO')	
			->setCellValue('BP'.$i, 'CEDULAS')
			->setCellValue('BQ'.$i, 'RECAUDO');
			$i++;	

$sql = "SELECT * FROM comoVamos INNER JOIN usuarios ON usuid = comvausuario INNER JOIN comovamosgrupodeta ON grupousuario = comvausuario WHERE comvafecha = '$fecha' AND grupofecha = '$fecha';";
$qry = $db->query($sql);			

while($row = $qry->fetch(PDO::FETCH_ASSOC)){
if ($row['gruponombre'] == 'CASTIGADA') {
	
	$sqlrealizado = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha >= '$fecha'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";

	$qryrealizado = $db->query($sqlrealizado);	
	$rowrealizado = $qryrealizado->fetch(PDO::FETCH_ASSOC);	
	
	$porcencedulas = ($rowrealizado['Cedulas'] / $row['comvacedulas']) * 100;
	$porcenvalor = ($rowrealizado['Valor'] /  $row['comvavalor']) * 100;
	
	$fechasindias = substr($fecha, 0, -2);
	

	$sqldia1 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."01'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia1 = $db->query($sqldia1);	
	$rowdia1 = $qrydia1->fetch(PDO::FETCH_ASSOC);
	
	$cedulas1 = $rowdia1['Cedulas'];
	$recaudo1 = $rowdia1['Valor'];
	
	
	$sqldia2 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."02'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia2 = $db->query($sqldia2);	
	$rowdia2 = $qrydia2->fetch(PDO::FETCH_ASSOC);
	
	$cedulas2 = $rowdia2['Cedulas'];
	$recaudo2 = $rowdia2['Valor'];


	$sqldia3 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."03'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia3 = $db->query($sqldia3);	
	$rowdia3 = $qrydia3->fetch(PDO::FETCH_ASSOC);
	
	$cedulas3 = $rowdia3['Cedulas'];
	$recaudo3 = $rowdia3['Valor'];


	$sqldia4 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."04'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia4 = $db->query($sqldia4);	
	$rowdia4 = $qrydia4->fetch(PDO::FETCH_ASSOC);
	
	$cedulas4 = $rowdia4['Cedulas'];
	$recaudo4 = $rowdia4['Valor'];


	$sqldia5 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."05'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia5 = $db->query($sqldia5);	
	$rowdia5 = $qrydia5->fetch(PDO::FETCH_ASSOC);
	
	$cedulas5 = $rowdia5['Cedulas'];
	$recaudo5 = $rowdia5['Valor'];


	$sqldia6 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."06'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia6 = $db->query($sqldia6);	
	$rowdia6 = $qrydia6->fetch(PDO::FETCH_ASSOC);
	
	$cedulas6 = $rowdia6['Cedulas'];
	$recaudo6 = $rowdia6['Valor'];


	$sqldia7 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."07'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia7 = $db->query($sqldia7);	
	$rowdia7 = $qrydia7->fetch(PDO::FETCH_ASSOC);
	
	$cedulas7 = $rowdia7['Cedulas'];
	$recaudo7 = $rowdia7['Valor'];


	$sqldia8 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."08'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia8 = $db->query($sqldia8);	
	$rowdia8 = $qrydia8->fetch(PDO::FETCH_ASSOC);
	
	$cedulas8 = $rowdia8['Cedulas'];
	$recaudo8 = $rowdia8['Valor'];


	$sqldia9 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."09'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia9 = $db->query($sqldia9);	
	$rowdia9 = $qrydia9->fetch(PDO::FETCH_ASSOC);
	
	$cedulas9 = $rowdia9['Cedulas'];
	$recaudo9 = $rowdia9['Valor'];


		
	$sqldia10 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."10'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia10 = $db->query($sqldia10);	
	$rowdia10 = $qrydia10->fetch(PDO::FETCH_ASSOC);
	
	$cedulas10 = $rowdia10['Cedulas'];
	$recaudo10 = $rowdia10['Valor'];


		
	$sqldia11 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."11'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia11 = $db->query($sqldia11);	
	$rowdia11 = $qrydia11->fetch(PDO::FETCH_ASSOC);
	
	$cedulas11 = $rowdia11['Cedulas'];
	$recaudo11 = $rowdia11['Valor'];


	
	$sqldia12 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."12'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia12 = $db->query($sqldia12);	
	$rowdia12 = $qrydia12->fetch(PDO::FETCH_ASSOC);
	
	$cedulas12 = $rowdia12['Cedulas'];
	$recaudo12 = $rowdia12['Valor'];


	
	$sqldia13 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."13'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia13 = $db->query($sqldia13);	
	$rowdia13 = $qrydia13->fetch(PDO::FETCH_ASSOC);
	
	$cedulas13 = $rowdia13['Cedulas'];
	$recaudo13 = $rowdia13['Valor'];


	
	$sqldia14 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."14'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia14 = $db->query($sqldia14);	
	$rowdia14 = $qrydia14->fetch(PDO::FETCH_ASSOC);
	
	$cedulas14 = $rowdia14['Cedulas'];
	$recaudo14 = $rowdia14['Valor'];


	
	$sqldia15 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."15'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia15 = $db->query($sqldia15);	
	$rowdia15 = $qrydia15->fetch(PDO::FETCH_ASSOC);
	
	$cedulas15 = $rowdia15['Cedulas'];
	$recaudo15 = $rowdia15['Valor'];


	
	$sqldia16 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."16'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia16 = $db->query($sqldia16);	
	$rowdia16 = $qrydia16->fetch(PDO::FETCH_ASSOC);
	
	$cedulas16 = $rowdia16['Cedulas'];
	$recaudo16 = $rowdia16['Valor'];


	
	$sqldia17 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."17'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia17 = $db->query($sqldia17);	
	$rowdia17 = $qrydia17->fetch(PDO::FETCH_ASSOC);
	
	$cedulas17 = $rowdia17['Cedulas'];
	$recaudo17 = $rowdia17['Valor'];


	
	$sqldia18 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."18'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia18 = $db->query($sqldia18);	
	$rowdia18 = $qrydia18->fetch(PDO::FETCH_ASSOC);
	
	$cedulas18 = $rowdia18['Cedulas'];
	$recaudo18 = $rowdia18['Valor'];


	
	$sqldia19 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."19'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia19 = $db->query($sqldia19);	
	$rowdia19 = $qrydia19->fetch(PDO::FETCH_ASSOC);
	
	$cedulas19 = $rowdia19['Cedulas'];
	$recaudo19 = $rowdia19['Valor'];


	
	$sqldia20 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."20'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia20 = $db->query($sqldia20);	
	$rowdia20 = $qrydia20->fetch(PDO::FETCH_ASSOC);
	
	$cedulas20 = $rowdia20['Cedulas'];
	$recaudo20 = $rowdia20['Valor'];


	
	$sqldia21 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."21'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia21 = $db->query($sqldia21);	
	$rowdia21 = $qrydia21->fetch(PDO::FETCH_ASSOC);
	
	$cedulas21 = $rowdia21['Cedulas'];
	$recaudo21 = $rowdia21['Valor'];


	
	$sqldia22 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."22'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia22 = $db->query($sqldia22);	
	$rowdia22 = $qrydia22->fetch(PDO::FETCH_ASSOC);
	
	$cedulas22 = $rowdia22['Cedulas'];
	$recaudo22 = $rowdia22['Valor'];


	
	$sqldia23 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."23'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia23 = $db->query($sqldia23);	
	$rowdia23 = $qrydia23->fetch(PDO::FETCH_ASSOC);
	
	$cedulas23 = $rowdia23['Cedulas'];
	$recaudo23 = $rowdia23['Valor'];


	
	$sqldia24 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."24'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia24 = $db->query($sqldia24);	
	$rowdia24 = $qrydia24->fetch(PDO::FETCH_ASSOC);
	
	$cedulas24 = $rowdia24['Cedulas'];
	$recaudo24 = $rowdia24['Valor'];


	
	$sqldia25 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."25'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia25 = $db->query($sqldia25);	
	$rowdia25 = $qrydia25->fetch(PDO::FETCH_ASSOC);
	
	$cedulas25 = $rowdia25['Cedulas'];
	$recaudo25 = $rowdia25['Valor'];


	
	$sqldia26 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."26'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia26 = $db->query($sqldia26);	
	$rowdia26 = $qrydia26->fetch(PDO::FETCH_ASSOC);
	
	$cedulas26 = $rowdia26['Cedulas'];
	$recaudo26 = $rowdia26['Valor'];


	
	$sqldia27 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."27'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia27 = $db->query($sqldia27);	
	$rowdia27 = $qrydia27->fetch(PDO::FETCH_ASSOC);
	
	$cedulas27 = $rowdia27['Cedulas'];
	$recaudo27 = $rowdia27['Valor'];


	
	$sqldia28 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."28'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia28 = $db->query($sqldia28);	
	$rowdia28 = $qrydia28->fetch(PDO::FETCH_ASSOC);
	
	$cedulas28 = $rowdia28['Cedulas'];
	$recaudo28 = $rowdia28['Valor'];


	
	$sqldia29 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."29'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia29 = $db->query($sqldia29);	
	$rowdia29 = $qrydia29->fetch(PDO::FETCH_ASSOC);
	
	$cedulas29 = $rowdia29['Cedulas'];
	$recaudo29 = $rowdia29['Valor'];


	
	$sqldia30 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."30'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia30 = $db->query($sqldia30);	
	$rowdia30 = $qrydia30->fetch(PDO::FETCH_ASSOC);
	
	$cedulas30 = $rowdia30['Cedulas'];
	$recaudo30 = $rowdia30['Valor'];


	
	$sqldia31 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."31'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia31 = $db->query($sqldia31);	
	$rowdia31 = $qrydia31->fetch(PDO::FETCH_ASSOC);
	
	$cedulas31 = $rowdia31['Cedulas'];
	$recaudo31 = $rowdia31['Valor'];

	$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':BQ'.$i)->applyFromArray($styleArray);
	$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('A'.$i, $row['usunombre'])
    	->setCellValue('B'.$i, $row['comvacedulas'])
		->setCellValue('C'.$i, $row['comvavalor'])
		->setCellValue('D'.$i, $rowrealizado['Cedulas'])
		->setCellValue('E'.$i, $rowrealizado['Valor'])
		->setCellValue('F'.$i, round($porcencedulas,1).' %')
		->setCellValue('G'.$i, round($porcenvalor,1).' %')
		->setCellValue('H'.$i, $cedulas1)
		->setCellValue('I'.$i, $recaudo1)
		->setCellValue('J'.$i, $cedulas2)
		->setCellValue('K'.$i, $recaudo2)	
		->setCellValue('L'.$i, $cedulas3)
		->setCellValue('M'.$i, $recaudo3)	
		->setCellValue('N'.$i, $cedulas4)
		->setCellValue('O'.$i, $recaudo4)	
		->setCellValue('P'.$i, $cedulas5)
		->setCellValue('Q'.$i, $recaudo5)	
		->setCellValue('R'.$i, $cedulas6)
		->setCellValue('S'.$i, $recaudo6)	
		->setCellValue('T'.$i, $cedulas7)
		->setCellValue('U'.$i, $recaudo7)	
		->setCellValue('V'.$i, $cedulas8)
		->setCellValue('W'.$i, $recaudo8)	
		->setCellValue('X'.$i, $cedulas9)
		->setCellValue('Y'.$i, $recaudo9)	
		->setCellValue('Z'.$i, $cedulas10)
		->setCellValue('AA'.$i, $recaudo10)	
		->setCellValue('AB'.$i, $cedulas11)
		->setCellValue('AC'.$i, $recaudo11)	
		->setCellValue('AD'.$i, $cedulas12)
		->setCellValue('AE'.$i, $recaudo12)	
		->setCellValue('AF'.$i, $cedulas13)
		->setCellValue('AG'.$i, $recaudo13)	
		->setCellValue('AH'.$i, $cedulas14)
		->setCellValue('AI'.$i, $recaudo14)	
		->setCellValue('AJ'.$i, $cedulas15)
		->setCellValue('AK'.$i, $recaudo15)	
		->setCellValue('AL'.$i, $cedulas16)
		->setCellValue('AM'.$i, $recaudo16)	
		->setCellValue('AN'.$i, $cedulas17)
		->setCellValue('AO'.$i, $recaudo17)	
		->setCellValue('AP'.$i, $cedulas18)
		->setCellValue('AQ'.$i, $recaudo18)	
		->setCellValue('AR'.$i, $cedulas19)
		->setCellValue('AS'.$i, $recaudo19)	
		->setCellValue('AT'.$i, $cedulas20)
		->setCellValue('AU'.$i, $recaudo20)	
		->setCellValue('AV'.$i, $cedulas21)
		->setCellValue('AW'.$i, $recaudo21)	
		->setCellValue('AX'.$i, $cedulas22)
		->setCellValue('AY'.$i, $recaudo22)	
		->setCellValue('AZ'.$i, $cedulas23)
		->setCellValue('BA'.$i, $recaudo23)	
		->setCellValue('BB'.$i, $cedulas24)
		->setCellValue('BC'.$i, $recaudo24)	
		->setCellValue('BD'.$i, $cedulas25)
		->setCellValue('BE'.$i, $recaudo25)	
		->setCellValue('BF'.$i, $cedulas26)
		->setCellValue('BG'.$i, $recaudo26)	
		->setCellValue('BH'.$i, $cedulas27)
		->setCellValue('BI'.$i, $recaudo27)	
		->setCellValue('BJ'.$i, $cedulas28)
		->setCellValue('BK'.$i, $recaudo28)	
		->setCellValue('BL'.$i, $cedulas29)
		->setCellValue('BM'.$i, $recaudo29)	
		->setCellValue('BN'.$i, $cedulas30)
		->setCellValue('BO'.$i, $recaudo30)	
		->setCellValue('BP'.$i, $cedulas31)
		->setCellValue('BQ'.$i, $recaudo31);	
	$i++;

	$tcedulas = $row['comvacedulas'] + $tcedulas;
	$tvalor = $row['comvavalor'] + $tvalor;
	$tcedurealizado = $rowrealizado['Cedulas'] + $tcedurealizado;
	$tvalorrealizado = $rowrealizado['Valor'] + $tvalorrealizado;
	$tporcencedulas = ($tcedurealizado / $tcedulas) * 100;
	$tporcenvalor = ($tvalorrealizado /  $tvalor) * 100;
	
$tcedulas1 = $rowdia1['Cedulas'] + $tcedulas1;
$trecaudo1 = $rowdia1['Valor'] + $trecaudo1;
$tcedulas2 = $rowdia2['Cedulas'] + $tcedulas2;
$trecaudo2 = $rowdia2['Valor'] + $trecaudo2;
$tcedulas3 = $rowdia3['Cedulas'] + $tcedulas3;
$trecaudo3 = $rowdia3['Valor'] + $trecaudo3;
$tcedulas4 = $rowdia4['Cedulas'] + $tcedulas4;
$trecaudo4 = $rowdia4['Valor'] + $trecaudo4;
$tcedulas5 = $rowdia5['Cedulas'] + $tcedulas5;
$trecaudo5 = $rowdia5['Valor'] + $trecaudo5;
$tcedulas6 = $rowdia6['Cedulas'] + $tcedulas6;
$trecaudo6 = $rowdia6['Valor'] + $trecaudo6;
$tcedulas7 = $rowdia7['Cedulas'] + $tcedulas7;
$trecaudo7 = $rowdia7['Valor'] + $trecaudo7;
$tcedulas8 = $rowdia8['Cedulas'] + $tcedulas8;
$trecaudo8 = $rowdia8['Valor'] + $trecaudo8;
$tcedulas9 = $rowdia9['Cedulas'] + $tcedulas9;
$trecaudo9 = $rowdia9['Valor'] + $trecaudo9;
$tcedulas10 = $rowdia10['Cedulas'] + $tcedulas10;
$trecaudo10 = $rowdia10['Valor'] + $trecaudo10;
$tcedulas11 = $rowdia11['Cedulas'] + $tcedulas11;
$trecaudo11 = $rowdia11['Valor'] + $trecaudo11;
$tcedulas12 = $rowdia12['Cedulas'] + $tcedulas12;
$trecaudo12 = $rowdia12['Valor'] + $trecaudo12;
$tcedulas13 = $rowdia13['Cedulas'] + $tcedulas13;
$trecaudo13 = $rowdia13['Valor'] + $trecaudo13;
$tcedulas14 = $rowdia14['Cedulas'] + $tcedulas14;
$trecaudo14 = $rowdia14['Valor'] + $trecaudo14;
$tcedulas15 = $rowdia15['Cedulas'] + $tcedulas15;
$trecaudo15 = $rowdia15['Valor'] + $trecaudo15;
$tcedulas16 = $rowdia16['Cedulas'] + $tcedulas16;
$trecaudo16 = $rowdia16['Valor'] + $trecaudo16;
$tcedulas17 = $rowdia17['Cedulas'] + $tcedulas17;
$trecaudo17 = $rowdia17['Valor'] + $trecaudo17;
$tcedulas18 = $rowdia18['Cedulas'] + $tcedulas18;
$trecaudo18 = $rowdia18['Valor'] + $trecaudo18;
$tcedulas19 = $rowdia19['Cedulas'] + $tcedulas19;
$trecaudo19 = $rowdia19['Valor'] + $trecaudo19;
$tcedulas20 = $rowdia20['Cedulas'] + $tcedulas20;
$trecaudo20 = $rowdia20['Valor'] + $trecaudo20;
$tcedulas21 = $rowdia21['Cedulas'] + $tcedulas21;
$trecaudo21 = $rowdia21['Valor'] + $trecaudo21;
$tcedulas22 = $rowdia22['Cedulas'] + $tcedulas22;
$trecaudo22 = $rowdia22['Valor'] + $trecaudo22;
$tcedulas23 = $rowdia23['Cedulas'] + $tcedulas23;
$trecaudo23 = $rowdia23['Valor'] + $trecaudo23;
$tcedulas24 = $rowdia24['Cedulas'] + $tcedulas24;
$trecaudo24 = $rowdia24['Valor'] + $trecaudo24;
$tcedulas25 = $rowdia25['Cedulas'] + $tcedulas25;
$trecaudo25 = $rowdia25['Valor'] + $trecaudo25;
$tcedulas26 = $rowdia26['Cedulas'] + $tcedulas26;
$trecaudo26 = $rowdia26['Valor'] + $trecaudo26;
$tcedulas27 = $rowdia27['Cedulas'] + $tcedulas27;
$trecaudo27 = $rowdia27['Valor'] + $trecaudo27;
$tcedulas28 = $rowdia28['Cedulas'] + $tcedulas28;
$trecaudo28 = $rowdia28['Valor'] + $trecaudo28;
$tcedulas29 = $rowdia29['Cedulas'] + $tcedulas29;
$trecaudo29 = $rowdia29['Valor'] + $trecaudo29;
$tcedulas30 = $rowdia30['Cedulas'] + $tcedulas30;
$trecaudo30 = $rowdia30['Valor'] + $trecaudo30;
$tcedulas31 = $rowdia31['Cedulas'] + $tcedulas31;
$trecaudo31 = $rowdia31['Valor'] + $trecaudo31;

	
$tcedulas1castigada = $tcedulas1;
$trecaudo1castigada = $trecaudo1;
$tcedulas2castigada =  $tcedulas2;
$trecaudo2castigada = $trecaudo2;
$tcedulas3castigada =  $tcedulas3;
$trecaudo3castigada = $trecaudo3;
$tcedulas4castigada = $tcedulas4;
$trecaudo4castigada = $trecaudo4;
$tcedulas5castigada =  $tcedulas5;
$trecaudo5castigada =  $trecaudo5;
$tcedulas6castigada =  $tcedulas6;
$trecaudo6castigada =  $trecaudo6;
$tcedulas7castigada = $tcedulas7;
$trecaudo7castigada =  $trecaudo7;
$tcedulas8castigada =  $tcedulas8;
$trecaudo8castigada =  $trecaudo8;
$tcedulas9castigada =  $tcedulas9;
$trecaudo9castigada = $trecaudo9;
$tcedulas10castigada =  $tcedulas10;
$trecaudo10castigada =  $trecaudo10;
$tcedulas11castigada = $tcedulas11;
$trecaudo11castigada =  $trecaudo11;
$tcedulas12castigada = $tcedulas12;
$trecaudo12castigada =  $trecaudo12;
$tcedulas13castigada = $tcedulas13;
$trecaudo13castigada =  $trecaudo13;
$tcedulas14castigada = $tcedulas14;
$trecaudo14castigada = $trecaudo14;
$tcedulas15castigada =  $tcedulas15;
$trecaudo15castigada =  $trecaudo15;
$tcedulas16castigada =  $tcedulas16;
$trecaudo16castigada =  $trecaudo16;
$tcedulas17castigada =  $tcedulas17;
$trecaudo17castigada = $trecaudo17;
$tcedulas18castigada = $tcedulas18;
$trecaudo18castigada =  $trecaudo18;
$tcedulas19castigada =  $tcedulas19;
$trecaudo19castigada = $trecaudo19;
$tcedulas20castigada = $tcedulas20;
$trecaudo20castigada =  $trecaudo20;
$tcedulas21castigada =  $tcedulas21;
$trecaudo21castigada = $trecaudo21;
$tcedulas22castigada =  $tcedulas22;
$trecaudo22castigada =  $trecaudo22;
$tcedulas23castigada =  $tcedulas23;
$trecaudo23castigada =  $trecaudo23;
$tcedulas24castigada =  $tcedulas24;
$trecaudo24castigada =  $trecaudo24;
$tcedulas25castigada = $tcedulas25;
$trecaudo25castigada =  $trecaudo25;
$tcedulas26castigada = $tcedulas26;
$trecaudo26castigada = $trecaudo26;
$tcedulas27castigada = $tcedulas27;
$trecaudo27castigada = $trecaudo27;
$tcedulas28castigada =  $tcedulas28;
$trecaudo28castigada = $trecaudo28;
$tcedulas29castigada =  $tcedulas29;
$trecaudo29castigada = $trecaudo29;
$tcedulas30castigada = $tcedulas30;
$trecaudo30castigada = $trecaudo30;
$tcedulas31castigada = $tcedulas31;
$trecaudo31castigada =  $trecaudo31;
$tcedulascastigada= $tcedulas;
$tvalorcastigada = $tvlor;
$tcedurealizadocastigada = $tcedurealizado;
$tvalorrealizadocastigada = $tvalorrealizado;





}
}
	$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':BQ'.$i)->applyFromArray($styleArrayTwo);
	$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('A'.$i, 'TOTAL')
    	->setCellValue('B'.$i, $tcedulas)
		->setCellValue('C'.$i, $tvalor)
		->setCellValue('D'.$i, $tcedurealizado)
		->setCellValue('E'.$i, $tvalorrealizado)
		->setCellValue('F'.$i, round($tporcencedulas,1).' %')
		->setCellValue('G'.$i, round($tporcenvalor,1).' %')
		->setCellValue('H'.$i, $tcedulas1)
		->setCellValue('I'.$i, $trecaudo1)
		->setCellValue('J'.$i, $tcedulas2)
		->setCellValue('K'.$i, $trecaudo2)	
		->setCellValue('L'.$i, $tcedulas3)
		->setCellValue('M'.$i, $trecaudo3)	
		->setCellValue('N'.$i, $tcedulas4)
		->setCellValue('O'.$i, $trecaudo4)	
		->setCellValue('P'.$i, $tcedulas5)
		->setCellValue('Q'.$i, $trecaudo5)	
		->setCellValue('R'.$i, $tcedulas6)
		->setCellValue('S'.$i, $trecaudo6)	
		->setCellValue('T'.$i, $tcedulas7)
		->setCellValue('U'.$i, $trecaudo7)	
		->setCellValue('V'.$i, $tcedulas8)
		->setCellValue('W'.$i, $trecaudo8)	
		->setCellValue('X'.$i, $tcedulas9)
		->setCellValue('Y'.$i, $trecaudo9)	
		->setCellValue('Z'.$i, $tcedulas10)
		->setCellValue('AA'.$i, $trecaudo10)	
		->setCellValue('AB'.$i, $tcedulas11)
		->setCellValue('AC'.$i, $trecaudo11)	
		->setCellValue('AD'.$i, $tcedulas12)
		->setCellValue('AE'.$i, $trecaudo12)	
		->setCellValue('AF'.$i, $tcedulas13)
		->setCellValue('AG'.$i, $trecaudo13)	
		->setCellValue('AH'.$i, $tcedulas14)
		->setCellValue('AI'.$i, $trecaudo14)	
		->setCellValue('AJ'.$i, $tcedulas15)
		->setCellValue('AK'.$i, $trecaudo15)	
		->setCellValue('AL'.$i, $tcedulas16)
		->setCellValue('AM'.$i, $trecaudo16)	
		->setCellValue('AN'.$i, $tcedulas17)
		->setCellValue('AO'.$i, $trecaudo17)	
		->setCellValue('AP'.$i, $tcedulas18)
		->setCellValue('AQ'.$i, $trecaudo18)	
		->setCellValue('AR'.$i, $tcedulas19)
		->setCellValue('AS'.$i, $trecaudo19)	
		->setCellValue('AT'.$i, $tcedulas20)
		->setCellValue('AU'.$i, $trecaudo20)	
		->setCellValue('AV'.$i, $tcedulas21)
		->setCellValue('AW'.$i, $trecaudo21)	
		->setCellValue('AX'.$i, $tcedulas22)
		->setCellValue('AY'.$i, $trecaudo22)	
		->setCellValue('AZ'.$i, $tcedulas23)
		->setCellValue('BA'.$i, $trecaudo23)	
		->setCellValue('BB'.$i, $tcedulas24)
		->setCellValue('BC'.$i, $trecaudo24)	
		->setCellValue('BD'.$i, $tcedulas25)
		->setCellValue('BE'.$i, $trecaudo25)	
		->setCellValue('BF'.$i, $tcedulas26)
		->setCellValue('BG'.$i, $trecaudo26)	
		->setCellValue('BH'.$i, $tcedulas27)
		->setCellValue('BI'.$i, $trecaudo27)	
		->setCellValue('BJ'.$i, $tcedulas28)
		->setCellValue('BK'.$i, $trecaudo28)	
		->setCellValue('BL'.$i, $tcedulas29)
		->setCellValue('BM'.$i, $trecaudo29)	
		->setCellValue('BN'.$i, $tcedulas30)
		->setCellValue('BO'.$i, $trecaudo30)	
		->setCellValue('BP'.$i, $tcedulas31)
		->setCellValue('BQ'.$i, $trecaudo31);	
	$i++;				
	

// ----------------------------------------------------------------------------------------------------------


$tcedulas1  = 0;
$tcedulas2  = 0;
$tcedulas3  = 0;
$tcedulas4  = 0;
$tcedulas5  = 0;
$tcedulas6  = 0;
$tcedulas7  = 0;
$tcedulas8  = 0;
$tcedulas9  = 0;
$tcedulas10  = 0;
$tcedulas11  = 0;
$tcedulas12  = 0;
$tcedulas13  = 0;
$tcedulas14  = 0;
$tcedulas15  = 0;
$tcedulas16  = 0;
$tcedulas17  = 0;
$tcedulas18  = 0;
$tcedulas19  = 0;
$tcedulas20  = 0;
$tcedulas21  = 0;
$tcedulas22  = 0;
$tcedulas23  = 0;
$tcedulas24  = 0;
$tcedulas25  = 0;
$tcedulas26  = 0;
$tcedulas27  = 0;
$tcedulas28  = 0;
$tcedulas29  = 0;
$tcedulas30  = 0;
$tcedulas31  = 0;
$trecaudo1  = 0;
$trecaudo2  = 0;
$trecaudo3  = 0;
$trecaudo4  = 0;
$trecaudo5  = 0;
$trecaudo6  = 0;
$trecaudo7  = 0;
$trecaudo8  = 0;
$trecaudo9  = 0;
$trecaudo10  = 0;
$trecaudo11  = 0;
$trecaudo12  = 0;
$trecaudo13  = 0;
$trecaudo14  = 0;
$trecaudo15  = 0;
$trecaudo16  = 0;
$trecaudo17  = 0;
$trecaudo18  = 0;
$trecaudo19  = 0;
$trecaudo20  = 0;
$trecaudo21  = 0;
$trecaudo22  = 0;
$trecaudo23  = 0;
$trecaudo24  = 0;
$trecaudo25  = 0;
$trecaudo26  = 0;
$trecaudo27  = 0;
$trecaudo28  = 0;
$trecaudo29  = 0;
$trecaudo30  = 0;
$trecaudo31  = 0;
$tcedulas = 0;
$tvalor = 0;
$tcedurealizado = 0;
$tvalorrealizado = 0;
$tporcencedulas = 0;
$tporcenvalor = 0;

	
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$i.':C'.$i);
$objPHPExcel->getActiveSheet()->getStyle('A'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$i, 'CONTADOS');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('D'.$i.':E'.$i);
$objPHPExcel->getActiveSheet()->getStyle('D'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$i, 'REALIZADO');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('F'.$i.':G'.$i);
$objPHPExcel->getActiveSheet()->getStyle('F'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$i, ' % REALIZADO');
 
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('H'.$i.':I'.$i);
$objPHPExcel->getActiveSheet()->getStyle('H'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$i, '1');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('J'.$i.':K'.$i);
$objPHPExcel->getActiveSheet()->getStyle('J'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.$i, '2');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('L'.$i.':M'.$i);
$objPHPExcel->getActiveSheet()->getStyle('L'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('L'.$i, '3');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('N'.$i.':O'.$i);
$objPHPExcel->getActiveSheet()->getStyle('N'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('N'.$i, '4');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('P'.$i.':Q'.$i);
$objPHPExcel->getActiveSheet()->getStyle('P'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('P'.$i, '5');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('R'.$i.':S'.$i);
$objPHPExcel->getActiveSheet()->getStyle('L'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('R'.$i, '6');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('T'.$i.':U'.$i);
$objPHPExcel->getActiveSheet()->getStyle('T'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('T'.$i, '7');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('V'.$i.':W'.$i);
$objPHPExcel->getActiveSheet()->getStyle('V'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('V'.$i, '8');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('X'.$i.':Y'.$i);
$objPHPExcel->getActiveSheet()->getStyle('X'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('X'.$i, '9');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('Z'.$i.':AA'.$i);
$objPHPExcel->getActiveSheet()->getStyle('Z'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('Z'.$i, '10');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AB'.$i.':AC'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AB'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AB'.$i, '11');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AB'.$i.':AC'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AB'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AB'.$i, '11');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AD'.$i.':AE'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AD'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AD'.$i, '12');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AF'.$i.':AG'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AF'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AF'.$i, '13');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AH'.$i.':AI'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AH'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AH'.$i, '14');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AJ'.$i.':AK'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AJ'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AJ'.$i, '15');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AL'.$i.':AM'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AL'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AL'.$i, '16');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AN'.$i.':AO'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AN'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AN'.$i, '17');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AP'.$i.':AQ'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AP'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AP'.$i, '18');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AR'.$i.':AS'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AR'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AR'.$i, '19');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AT'.$i.':AU'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AT'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AT'.$i, '20');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AV'.$i.':AW'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AV'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AV'.$i, '21');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AX'.$i.':AY'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AX'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AX'.$i, '22');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AZ'.$i.':BA'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AZ'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AZ'.$i, '23');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BB'.$i.':BC'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BB'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BB'.$i, '24');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BD'.$i.':BE'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BD'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BD'.$i, '25');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BF'.$i.':BG'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BF'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BF'.$i, '26');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BH'.$i.':BI'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BH'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BH'.$i, '27');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BJ'.$i.':BK'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BJ'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BJ'.$i, '28');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BL'.$i.':BM'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BL'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BL'.$i, '29');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BN'.$i.':BO'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BN'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BN'.$i, '30');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BP'.$i.':BQ'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BP'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BP'.$i, '31');


$i++;
$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':BQ'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$i, 'ASESOR')
            ->setCellValue('B'.$i, 'CEDULAS')
            ->setCellValue('C'.$i, 'VALOR')
            ->setCellValue('D'.$i, 'CEDULAS')
			->setCellValue('E'.$i, 'VALOR')
			->setCellValue('F'.$i, 'CEDULAS')
			->setCellValue('G'.$i, 'VALOR')	
			->setCellValue('H'.$i, 'CEDULAS')
			->setCellValue('I'.$i, 'RECAUDO')
			->setCellValue('J'.$i, 'CEDULAS')
			->setCellValue('K'.$i, 'RECAUDO')	
			->setCellValue('L'.$i, 'CEDULAS')
			->setCellValue('M'.$i, 'RECAUDO')	
			->setCellValue('N'.$i, 'CEDULAS')
			->setCellValue('O'.$i, 'RECAUDO')	
			->setCellValue('P'.$i, 'CEDULAS')
			->setCellValue('Q'.$i, 'RECAUDO')	
			->setCellValue('R'.$i, 'CEDULAS')
			->setCellValue('S'.$i, 'RECAUDO')	
			->setCellValue('T'.$i, 'CEDULAS')
			->setCellValue('U'.$i, 'RECAUDO')	
			->setCellValue('V'.$i, 'CEDULAS')
			->setCellValue('W'.$i, 'RECAUDO')	
			->setCellValue('X'.$i, 'CEDULAS')
			->setCellValue('Y'.$i, 'RECAUDO')	
			->setCellValue('Z'.$i, 'CEDULAS')
			->setCellValue('AA'.$i, 'RECAUDO')	
			->setCellValue('AB'.$i, 'CEDULAS')
			->setCellValue('AC'.$i, 'RECAUDO')	
			->setCellValue('AD'.$i, 'CEDULAS')
			->setCellValue('AE'.$i, 'RECAUDO')	
			->setCellValue('AF'.$i, 'CEDULAS')
			->setCellValue('AG'.$i, 'RECAUDO')	
			->setCellValue('AH'.$i, 'CEDULAS')
			->setCellValue('AI'.$i, 'RECAUDO')	
			->setCellValue('AJ'.$i, 'CEDULAS')
			->setCellValue('AK'.$i, 'RECAUDO')	
			->setCellValue('AL'.$i, 'CEDULAS')
			->setCellValue('AM'.$i, 'RECAUDO')	
			->setCellValue('AN'.$i, 'CEDULAS')
			->setCellValue('AO'.$i, 'RECAUDO')	
			->setCellValue('AP'.$i, 'CEDULAS')
			->setCellValue('AQ'.$i, 'RECAUDO')	
			->setCellValue('AR'.$i, 'CEDULAS')
			->setCellValue('AS'.$i, 'RECAUDO')	
			->setCellValue('AT'.$i, 'CEDULAS')
			->setCellValue('AU'.$i, 'RECAUDO')	
			->setCellValue('AV'.$i, 'CEDULAS')
			->setCellValue('AW'.$i, 'RECAUDO')	
			->setCellValue('AX'.$i, 'CEDULAS')
			->setCellValue('AY'.$i, 'RECAUDO')	
			->setCellValue('AZ'.$i, 'CEDULAS')
			->setCellValue('BA'.$i, 'RECAUDO')	
			->setCellValue('BB'.$i, 'CEDULAS')
			->setCellValue('BC'.$i, 'RECAUDO')	
			->setCellValue('BD'.$i, 'CEDULAS')
			->setCellValue('BE'.$i, 'RECAUDO')	
			->setCellValue('BF'.$i, 'CEDULAS')
			->setCellValue('BG'.$i, 'RECAUDO')	
			->setCellValue('BH'.$i, 'CEDULAS')
			->setCellValue('BI'.$i, 'RECAUDO')	
			->setCellValue('BJ'.$i, 'CEDULAS')
			->setCellValue('BK'.$i, 'RECAUDO')	
			->setCellValue('BL'.$i, 'CEDULAS')
			->setCellValue('BM'.$i, 'RECAUDO')	
			->setCellValue('BN'.$i, 'CEDULAS')
			->setCellValue('BO'.$i, 'RECAUDO')	
			->setCellValue('BP'.$i, 'CEDULAS')
			->setCellValue('BQ'.$i, 'RECAUDO');
			$i++;	

$sql = "SELECT * FROM comoVamos INNER JOIN usuarios ON usuid = comvausuario INNER JOIN comovamosgrupodeta ON grupousuario = comvausuario WHERE comvafecha = '$fecha' AND grupofecha = '$fecha';";
$qry = $db->query($sql);			

while($row = $qry->fetch(PDO::FETCH_ASSOC)){
if ($row['gruponombre'] == 'CONTADOS') {
	
	$sqlrealizado = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha >= '$fecha'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";

	$qryrealizado = $db->query($sqlrealizado);	
	$rowrealizado = $qryrealizado->fetch(PDO::FETCH_ASSOC);	
	
	$porcencedulas = ($rowrealizado['Cedulas'] / $row['comvacedulas']) * 100;
	$porcenvalor = ($rowrealizado['Valor'] /  $row['comvavalor']) * 100;
	
	$fechasindias = substr($fecha, 0, -2);
	

	$sqldia1 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."01'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia1 = $db->query($sqldia1);	
	$rowdia1 = $qrydia1->fetch(PDO::FETCH_ASSOC);
	
	$cedulas1 = $rowdia1['Cedulas'];
	$recaudo1 = $rowdia1['Valor'];
	
	
	$sqldia2 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."02'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia2 = $db->query($sqldia2);	
	$rowdia2 = $qrydia2->fetch(PDO::FETCH_ASSOC);
	
	$cedulas2 = $rowdia2['Cedulas'];
	$recaudo2 = $rowdia2['Valor'];


	$sqldia3 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."03'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia3 = $db->query($sqldia3);	
	$rowdia3 = $qrydia3->fetch(PDO::FETCH_ASSOC);
	
	$cedulas3 = $rowdia3['Cedulas'];
	$recaudo3 = $rowdia3['Valor'];


	$sqldia4 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."04'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia4 = $db->query($sqldia4);	
	$rowdia4 = $qrydia4->fetch(PDO::FETCH_ASSOC);
	
	$cedulas4 = $rowdia4['Cedulas'];
	$recaudo4 = $rowdia4['Valor'];


	$sqldia5 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."05'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia5 = $db->query($sqldia5);	
	$rowdia5 = $qrydia5->fetch(PDO::FETCH_ASSOC);
	
	$cedulas5 = $rowdia5['Cedulas'];
	$recaudo5 = $rowdia5['Valor'];


	$sqldia6 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."06'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia6 = $db->query($sqldia6);	
	$rowdia6 = $qrydia6->fetch(PDO::FETCH_ASSOC);
	
	$cedulas6 = $rowdia6['Cedulas'];
	$recaudo6 = $rowdia6['Valor'];


	$sqldia7 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."07'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia7 = $db->query($sqldia7);	
	$rowdia7 = $qrydia7->fetch(PDO::FETCH_ASSOC);
	
	$cedulas7 = $rowdia7['Cedulas'];
	$recaudo7 = $rowdia7['Valor'];


	$sqldia8 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."08'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia8 = $db->query($sqldia8);	
	$rowdia8 = $qrydia8->fetch(PDO::FETCH_ASSOC);
	
	$cedulas8 = $rowdia8['Cedulas'];
	$recaudo8 = $rowdia8['Valor'];


	$sqldia9 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."09'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia9 = $db->query($sqldia9);	
	$rowdia9 = $qrydia9->fetch(PDO::FETCH_ASSOC);
	
	$cedulas9 = $rowdia9['Cedulas'];
	$recaudo9 = $rowdia9['Valor'];


		
	$sqldia10 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."10'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia10 = $db->query($sqldia10);	
	$rowdia10 = $qrydia10->fetch(PDO::FETCH_ASSOC);
	
	$cedulas10 = $rowdia10['Cedulas'];
	$recaudo10 = $rowdia10['Valor'];


		
	$sqldia11 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."11'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia11 = $db->query($sqldia11);	
	$rowdia11 = $qrydia11->fetch(PDO::FETCH_ASSOC);
	
	$cedulas11 = $rowdia11['Cedulas'];
	$recaudo11 = $rowdia11['Valor'];


	
	$sqldia12 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."12'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia12 = $db->query($sqldia12);	
	$rowdia12 = $qrydia12->fetch(PDO::FETCH_ASSOC);
	
	$cedulas12 = $rowdia12['Cedulas'];
	$recaudo12 = $rowdia12['Valor'];


	
	$sqldia13 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."13'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia13 = $db->query($sqldia13);	
	$rowdia13 = $qrydia13->fetch(PDO::FETCH_ASSOC);
	
	$cedulas13 = $rowdia13['Cedulas'];
	$recaudo13 = $rowdia13['Valor'];


	
	$sqldia14 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."14'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia14 = $db->query($sqldia14);	
	$rowdia14 = $qrydia14->fetch(PDO::FETCH_ASSOC);
	
	$cedulas14 = $rowdia14['Cedulas'];
	$recaudo14 = $rowdia14['Valor'];


	
	$sqldia15 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."15'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia15 = $db->query($sqldia15);	
	$rowdia15 = $qrydia15->fetch(PDO::FETCH_ASSOC);
	
	$cedulas15 = $rowdia15['Cedulas'];
	$recaudo15 = $rowdia15['Valor'];


	
	$sqldia16 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."16'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia16 = $db->query($sqldia16);	
	$rowdia16 = $qrydia16->fetch(PDO::FETCH_ASSOC);
	
	$cedulas16 = $rowdia16['Cedulas'];
	$recaudo16 = $rowdia16['Valor'];


	
	$sqldia17 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."17'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia17 = $db->query($sqldia17);	
	$rowdia17 = $qrydia17->fetch(PDO::FETCH_ASSOC);
	
	$cedulas17 = $rowdia17['Cedulas'];
	$recaudo17 = $rowdia17['Valor'];


	
	$sqldia18 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."18'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia18 = $db->query($sqldia18);	
	$rowdia18 = $qrydia18->fetch(PDO::FETCH_ASSOC);
	
	$cedulas18 = $rowdia18['Cedulas'];
	$recaudo18 = $rowdia18['Valor'];


	
	$sqldia19 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."19'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia19 = $db->query($sqldia19);	
	$rowdia19 = $qrydia19->fetch(PDO::FETCH_ASSOC);
	
	$cedulas19 = $rowdia19['Cedulas'];
	$recaudo19 = $rowdia19['Valor'];


	
	$sqldia20 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."20'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia20 = $db->query($sqldia20);	
	$rowdia20 = $qrydia20->fetch(PDO::FETCH_ASSOC);
	
	$cedulas20 = $rowdia20['Cedulas'];
	$recaudo20 = $rowdia20['Valor'];


	
	$sqldia21 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."21'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia21 = $db->query($sqldia21);	
	$rowdia21 = $qrydia21->fetch(PDO::FETCH_ASSOC);
	
	$cedulas21 = $rowdia21['Cedulas'];
	$recaudo21 = $rowdia21['Valor'];


	
	$sqldia22 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."22'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia22 = $db->query($sqldia22);	
	$rowdia22 = $qrydia22->fetch(PDO::FETCH_ASSOC);
	
	$cedulas22 = $rowdia22['Cedulas'];
	$recaudo22 = $rowdia22['Valor'];


	
	$sqldia23 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."23'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia23 = $db->query($sqldia23);	
	$rowdia23 = $qrydia23->fetch(PDO::FETCH_ASSOC);
	
	$cedulas23 = $rowdia23['Cedulas'];
	$recaudo23 = $rowdia23['Valor'];


	
	$sqldia24 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."24'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia24 = $db->query($sqldia24);	
	$rowdia24 = $qrydia24->fetch(PDO::FETCH_ASSOC);
	
	$cedulas24 = $rowdia24['Cedulas'];
	$recaudo24 = $rowdia24['Valor'];


	
	$sqldia25 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."25'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia25 = $db->query($sqldia25);	
	$rowdia25 = $qrydia25->fetch(PDO::FETCH_ASSOC);
	
	$cedulas25 = $rowdia25['Cedulas'];
	$recaudo25 = $rowdia25['Valor'];


	
	$sqldia26 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."26'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia26 = $db->query($sqldia26);	
	$rowdia26 = $qrydia26->fetch(PDO::FETCH_ASSOC);
	
	$cedulas26 = $rowdia26['Cedulas'];
	$recaudo26 = $rowdia26['Valor'];


	
	$sqldia27 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."27'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia27 = $db->query($sqldia27);	
	$rowdia27 = $qrydia27->fetch(PDO::FETCH_ASSOC);
	
	$cedulas27 = $rowdia27['Cedulas'];
	$recaudo27 = $rowdia27['Valor'];


	
	$sqldia28 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."28'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia28 = $db->query($sqldia28);	
	$rowdia28 = $qrydia28->fetch(PDO::FETCH_ASSOC);
	
	$cedulas28 = $rowdia28['Cedulas'];
	$recaudo28 = $rowdia28['Valor'];


	
	$sqldia29 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."29'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia29 = $db->query($sqldia29);	
	$rowdia29 = $qrydia29->fetch(PDO::FETCH_ASSOC);
	
	$cedulas29 = $rowdia29['Cedulas'];
	$recaudo29 = $rowdia29['Valor'];


	
	$sqldia30 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."30'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia30 = $db->query($sqldia30);	
	$rowdia30 = $qrydia30->fetch(PDO::FETCH_ASSOC);
	
	$cedulas30 = $rowdia30['Cedulas'];
	$recaudo30 = $rowdia30['Valor'];


	
	$sqldia31 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."31'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia31 = $db->query($sqldia31);	
	$rowdia31 = $qrydia31->fetch(PDO::FETCH_ASSOC);
	
	$cedulas31 = $rowdia31['Cedulas'];
	$recaudo31 = $rowdia31['Valor'];

	$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':BQ'.$i)->applyFromArray($styleArray);
	$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('A'.$i, $row['usunombre'])
    	->setCellValue('B'.$i, $row['comvacedulas'])
		->setCellValue('C'.$i, $row['comvavalor'])
		->setCellValue('D'.$i, $rowrealizado['Cedulas'])
		->setCellValue('E'.$i, $rowrealizado['Valor'])
		->setCellValue('F'.$i, round($porcencedulas,1).' %')
		->setCellValue('G'.$i, round($porcenvalor,1).' %')
		->setCellValue('H'.$i, $cedulas1)
		->setCellValue('I'.$i, $recaudo1)
		->setCellValue('J'.$i, $cedulas2)
		->setCellValue('K'.$i, $recaudo2)	
		->setCellValue('L'.$i, $cedulas3)
		->setCellValue('M'.$i, $recaudo3)	
		->setCellValue('N'.$i, $cedulas4)
		->setCellValue('O'.$i, $recaudo4)	
		->setCellValue('P'.$i, $cedulas5)
		->setCellValue('Q'.$i, $recaudo5)	
		->setCellValue('R'.$i, $cedulas6)
		->setCellValue('S'.$i, $recaudo6)	
		->setCellValue('T'.$i, $cedulas7)
		->setCellValue('U'.$i, $recaudo7)	
		->setCellValue('V'.$i, $cedulas8)
		->setCellValue('W'.$i, $recaudo8)	
		->setCellValue('X'.$i, $cedulas9)
		->setCellValue('Y'.$i, $recaudo9)	
		->setCellValue('Z'.$i, $cedulas10)
		->setCellValue('AA'.$i, $recaudo10)	
		->setCellValue('AB'.$i, $cedulas11)
		->setCellValue('AC'.$i, $recaudo11)	
		->setCellValue('AD'.$i, $cedulas12)
		->setCellValue('AE'.$i, $recaudo12)	
		->setCellValue('AF'.$i, $cedulas13)
		->setCellValue('AG'.$i, $recaudo13)	
		->setCellValue('AH'.$i, $cedulas14)
		->setCellValue('AI'.$i, $recaudo14)	
		->setCellValue('AJ'.$i, $cedulas15)
		->setCellValue('AK'.$i, $recaudo15)	
		->setCellValue('AL'.$i, $cedulas16)
		->setCellValue('AM'.$i, $recaudo16)	
		->setCellValue('AN'.$i, $cedulas17)
		->setCellValue('AO'.$i, $recaudo17)	
		->setCellValue('AP'.$i, $cedulas18)
		->setCellValue('AQ'.$i, $recaudo18)	
		->setCellValue('AR'.$i, $cedulas19)
		->setCellValue('AS'.$i, $recaudo19)	
		->setCellValue('AT'.$i, $cedulas20)
		->setCellValue('AU'.$i, $recaudo20)	
		->setCellValue('AV'.$i, $cedulas21)
		->setCellValue('AW'.$i, $recaudo21)	
		->setCellValue('AX'.$i, $cedulas22)
		->setCellValue('AY'.$i, $recaudo22)	
		->setCellValue('AZ'.$i, $cedulas23)
		->setCellValue('BA'.$i, $recaudo23)	
		->setCellValue('BB'.$i, $cedulas24)
		->setCellValue('BC'.$i, $recaudo24)	
		->setCellValue('BD'.$i, $cedulas25)
		->setCellValue('BE'.$i, $recaudo25)	
		->setCellValue('BF'.$i, $cedulas26)
		->setCellValue('BG'.$i, $recaudo26)	
		->setCellValue('BH'.$i, $cedulas27)
		->setCellValue('BI'.$i, $recaudo27)	
		->setCellValue('BJ'.$i, $cedulas28)
		->setCellValue('BK'.$i, $recaudo28)	
		->setCellValue('BL'.$i, $cedulas29)
		->setCellValue('BM'.$i, $recaudo29)	
		->setCellValue('BN'.$i, $cedulas30)
		->setCellValue('BO'.$i, $recaudo30)	
		->setCellValue('BP'.$i, $cedulas31)
		->setCellValue('BQ'.$i, $recaudo31);	
	$i++;

	$tcedulas = $row['comvacedulas'] + $tcedulas;
	$tvalor = $row['comvavalor'] + $tvalor;
	$tcedurealizado = $rowrealizado['Cedulas'] + $tcedurealizado;
	$tvalorrealizado = $rowrealizado['Valor'] + $tvalorrealizado;
	$tporcencedulas = ($tcedurealizado / $tcedulas) * 100;
	$tporcenvalor = ($tvalorrealizado /  $tvalor) * 100;
	
$tcedulas1 = $rowdia1['Cedulas'] + $tcedulas1;
$trecaudo1 = $rowdia1['Valor'] + $trecaudo1;
$tcedulas2 = $rowdia2['Cedulas'] + $tcedulas2;
$trecaudo2 = $rowdia2['Valor'] + $trecaudo2;
$tcedulas3 = $rowdia3['Cedulas'] + $tcedulas3;
$trecaudo3 = $rowdia3['Valor'] + $trecaudo3;
$tcedulas4 = $rowdia4['Cedulas'] + $tcedulas4;
$trecaudo4 = $rowdia4['Valor'] + $trecaudo4;
$tcedulas5 = $rowdia5['Cedulas'] + $tcedulas5;
$trecaudo5 = $rowdia5['Valor'] + $trecaudo5;
$tcedulas6 = $rowdia6['Cedulas'] + $tcedulas6;
$trecaudo6 = $rowdia6['Valor'] + $trecaudo6;
$tcedulas7 = $rowdia7['Cedulas'] + $tcedulas7;
$trecaudo7 = $rowdia7['Valor'] + $trecaudo7;
$tcedulas8 = $rowdia8['Cedulas'] + $tcedulas8;
$trecaudo8 = $rowdia8['Valor'] + $trecaudo8;
$tcedulas9 = $rowdia9['Cedulas'] + $tcedulas9;
$trecaudo9 = $rowdia9['Valor'] + $trecaudo9;
$tcedulas10 = $rowdia10['Cedulas'] + $tcedulas10;
$trecaudo10 = $rowdia10['Valor'] + $trecaudo10;
$tcedulas11 = $rowdia11['Cedulas'] + $tcedulas11;
$trecaudo11 = $rowdia11['Valor'] + $trecaudo11;
$tcedulas12 = $rowdia12['Cedulas'] + $tcedulas12;
$trecaudo12 = $rowdia12['Valor'] + $trecaudo12;
$tcedulas13 = $rowdia13['Cedulas'] + $tcedulas13;
$trecaudo13 = $rowdia13['Valor'] + $trecaudo13;
$tcedulas14 = $rowdia14['Cedulas'] + $tcedulas14;
$trecaudo14 = $rowdia14['Valor'] + $trecaudo14;
$tcedulas15 = $rowdia15['Cedulas'] + $tcedulas15;
$trecaudo15 = $rowdia15['Valor'] + $trecaudo15;
$tcedulas16 = $rowdia16['Cedulas'] + $tcedulas16;
$trecaudo16 = $rowdia16['Valor'] + $trecaudo16;
$tcedulas17 = $rowdia17['Cedulas'] + $tcedulas17;
$trecaudo17 = $rowdia17['Valor'] + $trecaudo17;
$tcedulas18 = $rowdia18['Cedulas'] + $tcedulas18;
$trecaudo18 = $rowdia18['Valor'] + $trecaudo18;
$tcedulas19 = $rowdia19['Cedulas'] + $tcedulas19;
$trecaudo19 = $rowdia19['Valor'] + $trecaudo19;
$tcedulas20 = $rowdia20['Cedulas'] + $tcedulas20;
$trecaudo20 = $rowdia20['Valor'] + $trecaudo20;
$tcedulas21 = $rowdia21['Cedulas'] + $tcedulas21;
$trecaudo21 = $rowdia21['Valor'] + $trecaudo21;
$tcedulas22 = $rowdia22['Cedulas'] + $tcedulas22;
$trecaudo22 = $rowdia22['Valor'] + $trecaudo22;
$tcedulas23 = $rowdia23['Cedulas'] + $tcedulas23;
$trecaudo23 = $rowdia23['Valor'] + $trecaudo23;
$tcedulas24 = $rowdia24['Cedulas'] + $tcedulas24;
$trecaudo24 = $rowdia24['Valor'] + $trecaudo24;
$tcedulas25 = $rowdia25['Cedulas'] + $tcedulas25;
$trecaudo25 = $rowdia25['Valor'] + $trecaudo25;
$tcedulas26 = $rowdia26['Cedulas'] + $tcedulas26;
$trecaudo26 = $rowdia26['Valor'] + $trecaudo26;
$tcedulas27 = $rowdia27['Cedulas'] + $tcedulas27;
$trecaudo27 = $rowdia27['Valor'] + $trecaudo27;
$tcedulas28 = $rowdia28['Cedulas'] + $tcedulas28;
$trecaudo28 = $rowdia28['Valor'] + $trecaudo28;
$tcedulas29 = $rowdia29['Cedulas'] + $tcedulas29;
$trecaudo29 = $rowdia29['Valor'] + $trecaudo29;
$tcedulas30 = $rowdia30['Cedulas'] + $tcedulas30;
$trecaudo30 = $rowdia30['Valor'] + $trecaudo30;
$tcedulas31 = $rowdia31['Cedulas'] + $tcedulas31;
$trecaudo31 = $rowdia31['Valor'] + $trecaudo31;


$tcedulas1contados =  $tcedulas1;
$trecaudo1contados =  $trecaudo1;
$tcedulas2contados =  $tcedulas2;
$trecaudo2contados = $trecaudo2;
$tcedulas3contados =  $tcedulas3;
$trecaudo3contados =  $trecaudo3;
$tcedulas4contados =  $tcedulas4;
$trecaudo4contados = $trecaudo4;
$tcedulas5contados =  $tcedulas5;
$trecaudo5contados =  $trecaudo5;
$tcedulas6contados =  $tcedulas6;
$trecaudo6contados =  $trecaudo6;
$tcedulas7contados =  $tcedulas7;
$trecaudo7contados =  $trecaudo7;
$tcedulas8contados =  $tcedulas8;
$trecaudo8contados = $trecaudo8;
$tcedulas9contados = $tcedulas9;
$trecaudo9contados = $trecaudo9;
$tcedulas10contados =  $tcedulas10;
$trecaudo10contados = $trecaudo10;
$tcedulas11contados =  $tcedulas11;
$trecaudo11contados = $trecaudo11;
$tcedulas12contados =  $tcedulas12;
$trecaudo12contados =  $trecaudo12;
$tcedulas13contados = $tcedulas13;
$trecaudo13contados =  $trecaudo13;
$tcedulas14contados = $tcedulas14;
$trecaudo14contados =  $trecaudo14;
$tcedulas15contados = $tcedulas15;
$trecaudo15contados =  $trecaudo15;
$tcedulas16contados =  $tcedulas16;
$trecaudo16contados =  $trecaudo16;
$tcedulas17contados =  $tcedulas17;
$trecaudo17contados = $trecaudo17;
$tcedulas18contados =  $tcedulas18;
$trecaudo18contados =  $trecaudo18;
$tcedulas19contados = $tcedulas19;
$trecaudo19contados = $trecaudo19;
$tcedulas20contados =  $tcedulas20;
$trecaudo20contados =  $trecaudo20;
$tcedulas21contados =  $tcedulas21;
$trecaudo21contados = $trecaudo21;
$tcedulas22contados = $tcedulas22;
$trecaudo22contados = $trecaudo22;
$tcedulas23contados = $tcedulas23;
$trecaudo23contados = $trecaudo23;
$tcedulas24contados =  $tcedulas24;
$trecaudo24contados = $trecaudo24;
$tcedulas25contados =  $tcedulas25;
$trecaudo25contados = $trecaudo25;
$tcedulas26contados =  $tcedulas26;
$trecaudo26contados =  $trecaudo26;
$tcedulas27contados = $tcedulas27;
$trecaudo27contados =  $trecaudo27;
$tcedulas28contados = $tcedulas28;
$trecaudo28contados =  $trecaudo28;
$tcedulas29contados = $tcedulas29;
$trecaudo29contados =  $trecaudo29;
$tcedulas30contados =  $tcedulas30;
$trecaudo30contados =  $trecaudo30;
$tcedulas31contados = $tcedulas31;
$trecaudo31contados =  $trecaudo31;
$tcedulascontados = $tcedulas;
$tvalorcontados = $tvlor;
$tcedurealizadocontados = $tcedurealizado;
$tvalorrealizadocontados = $tvalorrealizado; 





}
}
	$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':BQ'.$i)->applyFromArray($styleArrayTwo);
	$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('A'.$i, 'TOTAL')
    	->setCellValue('B'.$i, $tcedulas)
		->setCellValue('C'.$i, $tvalor)
		->setCellValue('D'.$i, $tcedurealizado)
		->setCellValue('E'.$i, $tvalorrealizado)
		->setCellValue('F'.$i, round($tporcencedulas,1).' %')
		->setCellValue('G'.$i, round($tporcenvalor,1).' %')
		->setCellValue('H'.$i, $tcedulas1)
		->setCellValue('I'.$i, $trecaudo1)
		->setCellValue('J'.$i, $tcedulas2)
		->setCellValue('K'.$i, $trecaudo2)	
		->setCellValue('L'.$i, $tcedulas3)
		->setCellValue('M'.$i, $trecaudo3)	
		->setCellValue('N'.$i, $tcedulas4)
		->setCellValue('O'.$i, $trecaudo4)	
		->setCellValue('P'.$i, $tcedulas5)
		->setCellValue('Q'.$i, $trecaudo5)	
		->setCellValue('R'.$i, $tcedulas6)
		->setCellValue('S'.$i, $trecaudo6)	
		->setCellValue('T'.$i, $tcedulas7)
		->setCellValue('U'.$i, $trecaudo7)	
		->setCellValue('V'.$i, $tcedulas8)
		->setCellValue('W'.$i, $trecaudo8)	
		->setCellValue('X'.$i, $tcedulas9)
		->setCellValue('Y'.$i, $trecaudo9)	
		->setCellValue('Z'.$i, $tcedulas10)
		->setCellValue('AA'.$i, $trecaudo10)	
		->setCellValue('AB'.$i, $tcedulas11)
		->setCellValue('AC'.$i, $trecaudo11)	
		->setCellValue('AD'.$i, $tcedulas12)
		->setCellValue('AE'.$i, $trecaudo12)	
		->setCellValue('AF'.$i, $tcedulas13)
		->setCellValue('AG'.$i, $trecaudo13)	
		->setCellValue('AH'.$i, $tcedulas14)
		->setCellValue('AI'.$i, $trecaudo14)	
		->setCellValue('AJ'.$i, $tcedulas15)
		->setCellValue('AK'.$i, $trecaudo15)	
		->setCellValue('AL'.$i, $tcedulas16)
		->setCellValue('AM'.$i, $trecaudo16)	
		->setCellValue('AN'.$i, $tcedulas17)
		->setCellValue('AO'.$i, $trecaudo17)	
		->setCellValue('AP'.$i, $tcedulas18)
		->setCellValue('AQ'.$i, $trecaudo18)	
		->setCellValue('AR'.$i, $tcedulas19)
		->setCellValue('AS'.$i, $trecaudo19)	
		->setCellValue('AT'.$i, $tcedulas20)
		->setCellValue('AU'.$i, $trecaudo20)	
		->setCellValue('AV'.$i, $tcedulas21)
		->setCellValue('AW'.$i, $trecaudo21)	
		->setCellValue('AX'.$i, $tcedulas22)
		->setCellValue('AY'.$i, $trecaudo22)	
		->setCellValue('AZ'.$i, $tcedulas23)
		->setCellValue('BA'.$i, $trecaudo23)	
		->setCellValue('BB'.$i, $tcedulas24)
		->setCellValue('BC'.$i, $trecaudo24)	
		->setCellValue('BD'.$i, $tcedulas25)
		->setCellValue('BE'.$i, $trecaudo25)	
		->setCellValue('BF'.$i, $tcedulas26)
		->setCellValue('BG'.$i, $trecaudo26)	
		->setCellValue('BH'.$i, $tcedulas27)
		->setCellValue('BI'.$i, $trecaudo27)	
		->setCellValue('BJ'.$i, $tcedulas28)
		->setCellValue('BK'.$i, $trecaudo28)	
		->setCellValue('BL'.$i, $tcedulas29)
		->setCellValue('BM'.$i, $trecaudo29)	
		->setCellValue('BN'.$i, $tcedulas30)
		->setCellValue('BO'.$i, $trecaudo30)	
		->setCellValue('BP'.$i, $tcedulas31)
		->setCellValue('BQ'.$i, $trecaudo31);	
	$i++;				
	

// ----------------------------------------------------------------------------------------------------------


$tcedulas1  = 0;
$tcedulas2  = 0;
$tcedulas3  = 0;
$tcedulas4  = 0;
$tcedulas5  = 0;
$tcedulas6  = 0;
$tcedulas7  = 0;
$tcedulas8  = 0;
$tcedulas9  = 0;
$tcedulas10  = 0;
$tcedulas11  = 0;
$tcedulas12  = 0;
$tcedulas13  = 0;
$tcedulas14  = 0;
$tcedulas15  = 0;
$tcedulas16  = 0;
$tcedulas17  = 0;
$tcedulas18  = 0;
$tcedulas19  = 0;
$tcedulas20  = 0;
$tcedulas21  = 0;
$tcedulas22  = 0;
$tcedulas23  = 0;
$tcedulas24  = 0;
$tcedulas25  = 0;
$tcedulas26  = 0;
$tcedulas27  = 0;
$tcedulas28  = 0;
$tcedulas29  = 0;
$tcedulas30  = 0;
$tcedulas31  = 0;
$trecaudo1  = 0;
$trecaudo2  = 0;
$trecaudo3  = 0;
$trecaudo4  = 0;
$trecaudo5  = 0;
$trecaudo6  = 0;
$trecaudo7  = 0;
$trecaudo8  = 0;
$trecaudo9  = 0;
$trecaudo10  = 0;
$trecaudo11  = 0;
$trecaudo12  = 0;
$trecaudo13  = 0;
$trecaudo14  = 0;
$trecaudo15  = 0;
$trecaudo16  = 0;
$trecaudo17  = 0;
$trecaudo18  = 0;
$trecaudo19  = 0;
$trecaudo20  = 0;
$trecaudo21  = 0;
$trecaudo22  = 0;
$trecaudo23  = 0;
$trecaudo24  = 0;
$trecaudo25  = 0;
$trecaudo26  = 0;
$trecaudo27  = 0;
$trecaudo28  = 0;
$trecaudo29  = 0;
$trecaudo30  = 0;
$trecaudo31  = 0;
$tcedulas = 0;
$tvalor = 0;
$tcedurealizado = 0;
$tvalorrealizado = 0;
$tporcencedulas = 0;
$tporcenvalor = 0;

	
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$i.':C'.$i);
$objPHPExcel->getActiveSheet()->getStyle('A'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$i, 'OFICINA');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('D'.$i.':E'.$i);
$objPHPExcel->getActiveSheet()->getStyle('D'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$i, 'REALIZADO');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('F'.$i.':G'.$i);
$objPHPExcel->getActiveSheet()->getStyle('F'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$i, ' % REALIZADO');
 
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('H'.$i.':I'.$i);
$objPHPExcel->getActiveSheet()->getStyle('H'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$i, '1');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('J'.$i.':K'.$i);
$objPHPExcel->getActiveSheet()->getStyle('J'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.$i, '2');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('L'.$i.':M'.$i);
$objPHPExcel->getActiveSheet()->getStyle('L'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('L'.$i, '3');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('N'.$i.':O'.$i);
$objPHPExcel->getActiveSheet()->getStyle('N'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('N'.$i, '4');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('P'.$i.':Q'.$i);
$objPHPExcel->getActiveSheet()->getStyle('P'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('P'.$i, '5');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('R'.$i.':S'.$i);
$objPHPExcel->getActiveSheet()->getStyle('L'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('R'.$i, '6');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('T'.$i.':U'.$i);
$objPHPExcel->getActiveSheet()->getStyle('T'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('T'.$i, '7');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('V'.$i.':W'.$i);
$objPHPExcel->getActiveSheet()->getStyle('V'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('V'.$i, '8');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('X'.$i.':Y'.$i);
$objPHPExcel->getActiveSheet()->getStyle('X'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('X'.$i, '9');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('Z'.$i.':AA'.$i);
$objPHPExcel->getActiveSheet()->getStyle('Z'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('Z'.$i, '10');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AB'.$i.':AC'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AB'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AB'.$i, '11');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AB'.$i.':AC'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AB'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AB'.$i, '11');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AD'.$i.':AE'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AD'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AD'.$i, '12');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AF'.$i.':AG'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AF'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AF'.$i, '13');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AH'.$i.':AI'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AH'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AH'.$i, '14');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AJ'.$i.':AK'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AJ'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AJ'.$i, '15');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AL'.$i.':AM'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AL'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AL'.$i, '16');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AN'.$i.':AO'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AN'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AN'.$i, '17');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AP'.$i.':AQ'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AP'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AP'.$i, '18');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AR'.$i.':AS'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AR'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AR'.$i, '19');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AT'.$i.':AU'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AT'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AT'.$i, '20');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AV'.$i.':AW'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AV'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AV'.$i, '21');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AX'.$i.':AY'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AX'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AX'.$i, '22');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AZ'.$i.':BA'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AZ'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AZ'.$i, '23');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BB'.$i.':BC'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BB'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BB'.$i, '24');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BD'.$i.':BE'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BD'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BD'.$i, '25');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BF'.$i.':BG'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BF'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BF'.$i, '26');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BH'.$i.':BI'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BH'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BH'.$i, '27');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BJ'.$i.':BK'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BJ'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BJ'.$i, '28');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BL'.$i.':BM'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BL'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BL'.$i, '29');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BN'.$i.':BO'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BN'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BN'.$i, '30');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BP'.$i.':BQ'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BP'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BP'.$i, '31');


$i++;
$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':BQ'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$i, 'ASESOR')
            ->setCellValue('B'.$i, 'CEDULAS')
            ->setCellValue('C'.$i, 'VALOR')
            ->setCellValue('D'.$i, 'CEDULAS')
			->setCellValue('E'.$i, 'VALOR')
			->setCellValue('F'.$i, 'CEDULAS')
			->setCellValue('G'.$i, 'VALOR')	
			->setCellValue('H'.$i, 'CEDULAS')
			->setCellValue('I'.$i, 'RECAUDO')
			->setCellValue('J'.$i, 'CEDULAS')
			->setCellValue('K'.$i, 'RECAUDO')	
			->setCellValue('L'.$i, 'CEDULAS')
			->setCellValue('M'.$i, 'RECAUDO')	
			->setCellValue('N'.$i, 'CEDULAS')
			->setCellValue('O'.$i, 'RECAUDO')	
			->setCellValue('P'.$i, 'CEDULAS')
			->setCellValue('Q'.$i, 'RECAUDO')	
			->setCellValue('R'.$i, 'CEDULAS')
			->setCellValue('S'.$i, 'RECAUDO')	
			->setCellValue('T'.$i, 'CEDULAS')
			->setCellValue('U'.$i, 'RECAUDO')	
			->setCellValue('V'.$i, 'CEDULAS')
			->setCellValue('W'.$i, 'RECAUDO')	
			->setCellValue('X'.$i, 'CEDULAS')
			->setCellValue('Y'.$i, 'RECAUDO')	
			->setCellValue('Z'.$i, 'CEDULAS')
			->setCellValue('AA'.$i, 'RECAUDO')	
			->setCellValue('AB'.$i, 'CEDULAS')
			->setCellValue('AC'.$i, 'RECAUDO')	
			->setCellValue('AD'.$i, 'CEDULAS')
			->setCellValue('AE'.$i, 'RECAUDO')	
			->setCellValue('AF'.$i, 'CEDULAS')
			->setCellValue('AG'.$i, 'RECAUDO')	
			->setCellValue('AH'.$i, 'CEDULAS')
			->setCellValue('AI'.$i, 'RECAUDO')	
			->setCellValue('AJ'.$i, 'CEDULAS')
			->setCellValue('AK'.$i, 'RECAUDO')	
			->setCellValue('AL'.$i, 'CEDULAS')
			->setCellValue('AM'.$i, 'RECAUDO')	
			->setCellValue('AN'.$i, 'CEDULAS')
			->setCellValue('AO'.$i, 'RECAUDO')	
			->setCellValue('AP'.$i, 'CEDULAS')
			->setCellValue('AQ'.$i, 'RECAUDO')	
			->setCellValue('AR'.$i, 'CEDULAS')
			->setCellValue('AS'.$i, 'RECAUDO')	
			->setCellValue('AT'.$i, 'CEDULAS')
			->setCellValue('AU'.$i, 'RECAUDO')	
			->setCellValue('AV'.$i, 'CEDULAS')
			->setCellValue('AW'.$i, 'RECAUDO')	
			->setCellValue('AX'.$i, 'CEDULAS')
			->setCellValue('AY'.$i, 'RECAUDO')	
			->setCellValue('AZ'.$i, 'CEDULAS')
			->setCellValue('BA'.$i, 'RECAUDO')	
			->setCellValue('BB'.$i, 'CEDULAS')
			->setCellValue('BC'.$i, 'RECAUDO')	
			->setCellValue('BD'.$i, 'CEDULAS')
			->setCellValue('BE'.$i, 'RECAUDO')	
			->setCellValue('BF'.$i, 'CEDULAS')
			->setCellValue('BG'.$i, 'RECAUDO')	
			->setCellValue('BH'.$i, 'CEDULAS')
			->setCellValue('BI'.$i, 'RECAUDO')	
			->setCellValue('BJ'.$i, 'CEDULAS')
			->setCellValue('BK'.$i, 'RECAUDO')	
			->setCellValue('BL'.$i, 'CEDULAS')
			->setCellValue('BM'.$i, 'RECAUDO')	
			->setCellValue('BN'.$i, 'CEDULAS')
			->setCellValue('BO'.$i, 'RECAUDO')	
			->setCellValue('BP'.$i, 'CEDULAS')
			->setCellValue('BQ'.$i, 'RECAUDO');
			$i++;	

$sql = "SELECT * FROM comoVamos INNER JOIN usuarios ON usuid = comvausuario INNER JOIN comovamosgrupodeta ON grupousuario = comvausuario WHERE comvafecha = '$fecha' AND grupofecha = '$fecha';";
$qry = $db->query($sql);			

while($row = $qry->fetch(PDO::FETCH_ASSOC)){
if ($row['gruponombre'] == 'OFICINA') {
	
	$sqlrealizado = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha >= '$fecha'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";

	$qryrealizado = $db->query($sqlrealizado);	
	$rowrealizado = $qryrealizado->fetch(PDO::FETCH_ASSOC);	
	
	$porcencedulas = ($rowrealizado['Cedulas'] / $row['comvacedulas']) * 100;
	$porcenvalor = ($rowrealizado['Valor'] /  $row['comvavalor']) * 100;
	
	$fechasindias = substr($fecha, 0, -2);
	

	$sqldia1 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."01'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia1 = $db->query($sqldia1);	
	$rowdia1 = $qrydia1->fetch(PDO::FETCH_ASSOC);
	
	$cedulas1 = $rowdia1['Cedulas'];
	$recaudo1 = $rowdia1['Valor'];
	
	
	$sqldia2 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."02'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia2 = $db->query($sqldia2);	
	$rowdia2 = $qrydia2->fetch(PDO::FETCH_ASSOC);
	
	$cedulas2 = $rowdia2['Cedulas'];
	$recaudo2 = $rowdia2['Valor'];


	$sqldia3 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."03'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia3 = $db->query($sqldia3);	
	$rowdia3 = $qrydia3->fetch(PDO::FETCH_ASSOC);
	
	$cedulas3 = $rowdia3['Cedulas'];
	$recaudo3 = $rowdia3['Valor'];


	$sqldia4 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."04'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia4 = $db->query($sqldia4);	
	$rowdia4 = $qrydia4->fetch(PDO::FETCH_ASSOC);
	
	$cedulas4 = $rowdia4['Cedulas'];
	$recaudo4 = $rowdia4['Valor'];


	$sqldia5 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."05'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia5 = $db->query($sqldia5);	
	$rowdia5 = $qrydia5->fetch(PDO::FETCH_ASSOC);
	
	$cedulas5 = $rowdia5['Cedulas'];
	$recaudo5 = $rowdia5['Valor'];


	$sqldia6 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."06'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia6 = $db->query($sqldia6);	
	$rowdia6 = $qrydia6->fetch(PDO::FETCH_ASSOC);
	
	$cedulas6 = $rowdia6['Cedulas'];
	$recaudo6 = $rowdia6['Valor'];


	$sqldia7 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."07'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia7 = $db->query($sqldia7);	
	$rowdia7 = $qrydia7->fetch(PDO::FETCH_ASSOC);
	
	$cedulas7 = $rowdia7['Cedulas'];
	$recaudo7 = $rowdia7['Valor'];


	$sqldia8 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."08'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia8 = $db->query($sqldia8);	
	$rowdia8 = $qrydia8->fetch(PDO::FETCH_ASSOC);
	
	$cedulas8 = $rowdia8['Cedulas'];
	$recaudo8 = $rowdia8['Valor'];


	$sqldia9 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."09'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia9 = $db->query($sqldia9);	
	$rowdia9 = $qrydia9->fetch(PDO::FETCH_ASSOC);
	
	$cedulas9 = $rowdia9['Cedulas'];
	$recaudo9 = $rowdia9['Valor'];


		
	$sqldia10 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."10'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia10 = $db->query($sqldia10);	
	$rowdia10 = $qrydia10->fetch(PDO::FETCH_ASSOC);
	
	$cedulas10 = $rowdia10['Cedulas'];
	$recaudo10 = $rowdia10['Valor'];


		
	$sqldia11 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."11'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia11 = $db->query($sqldia11);	
	$rowdia11 = $qrydia11->fetch(PDO::FETCH_ASSOC);
	
	$cedulas11 = $rowdia11['Cedulas'];
	$recaudo11 = $rowdia11['Valor'];


	
	$sqldia12 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."12'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia12 = $db->query($sqldia12);	
	$rowdia12 = $qrydia12->fetch(PDO::FETCH_ASSOC);
	
	$cedulas12 = $rowdia12['Cedulas'];
	$recaudo12 = $rowdia12['Valor'];


	
	$sqldia13 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."13'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia13 = $db->query($sqldia13);	
	$rowdia13 = $qrydia13->fetch(PDO::FETCH_ASSOC);
	
	$cedulas13 = $rowdia13['Cedulas'];
	$recaudo13 = $rowdia13['Valor'];


	
	$sqldia14 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."14'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia14 = $db->query($sqldia14);	
	$rowdia14 = $qrydia14->fetch(PDO::FETCH_ASSOC);
	
	$cedulas14 = $rowdia14['Cedulas'];
	$recaudo14 = $rowdia14['Valor'];


	
	$sqldia15 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."15'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia15 = $db->query($sqldia15);	
	$rowdia15 = $qrydia15->fetch(PDO::FETCH_ASSOC);
	
	$cedulas15 = $rowdia15['Cedulas'];
	$recaudo15 = $rowdia15['Valor'];


	
	$sqldia16 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."16'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia16 = $db->query($sqldia16);	
	$rowdia16 = $qrydia16->fetch(PDO::FETCH_ASSOC);
	
	$cedulas16 = $rowdia16['Cedulas'];
	$recaudo16 = $rowdia16['Valor'];


	
	$sqldia17 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."17'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia17 = $db->query($sqldia17);	
	$rowdia17 = $qrydia17->fetch(PDO::FETCH_ASSOC);
	
	$cedulas17 = $rowdia17['Cedulas'];
	$recaudo17 = $rowdia17['Valor'];


	
	$sqldia18 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."18'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia18 = $db->query($sqldia18);	
	$rowdia18 = $qrydia18->fetch(PDO::FETCH_ASSOC);
	
	$cedulas18 = $rowdia18['Cedulas'];
	$recaudo18 = $rowdia18['Valor'];


	
	$sqldia19 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."19'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia19 = $db->query($sqldia19);	
	$rowdia19 = $qrydia19->fetch(PDO::FETCH_ASSOC);
	
	$cedulas19 = $rowdia19['Cedulas'];
	$recaudo19 = $rowdia19['Valor'];


	
	$sqldia20 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."20'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia20 = $db->query($sqldia20);	
	$rowdia20 = $qrydia20->fetch(PDO::FETCH_ASSOC);
	
	$cedulas20 = $rowdia20['Cedulas'];
	$recaudo20 = $rowdia20['Valor'];


	
	$sqldia21 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."21'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia21 = $db->query($sqldia21);	
	$rowdia21 = $qrydia21->fetch(PDO::FETCH_ASSOC);
	
	$cedulas21 = $rowdia21['Cedulas'];
	$recaudo21 = $rowdia21['Valor'];


	
	$sqldia22 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."22'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia22 = $db->query($sqldia22);	
	$rowdia22 = $qrydia22->fetch(PDO::FETCH_ASSOC);
	
	$cedulas22 = $rowdia22['Cedulas'];
	$recaudo22 = $rowdia22['Valor'];


	
	$sqldia23 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."23'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia23 = $db->query($sqldia23);	
	$rowdia23 = $qrydia23->fetch(PDO::FETCH_ASSOC);
	
	$cedulas23 = $rowdia23['Cedulas'];
	$recaudo23 = $rowdia23['Valor'];


	
	$sqldia24 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."24'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia24 = $db->query($sqldia24);	
	$rowdia24 = $qrydia24->fetch(PDO::FETCH_ASSOC);
	
	$cedulas24 = $rowdia24['Cedulas'];
	$recaudo24 = $rowdia24['Valor'];


	
	$sqldia25 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."25'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia25 = $db->query($sqldia25);	
	$rowdia25 = $qrydia25->fetch(PDO::FETCH_ASSOC);
	
	$cedulas25 = $rowdia25['Cedulas'];
	$recaudo25 = $rowdia25['Valor'];


	
	$sqldia26 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."26'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia26 = $db->query($sqldia26);	
	$rowdia26 = $qrydia26->fetch(PDO::FETCH_ASSOC);
	
	$cedulas26 = $rowdia26['Cedulas'];
	$recaudo26 = $rowdia26['Valor'];


	
	$sqldia27 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."27'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia27 = $db->query($sqldia27);	
	$rowdia27 = $qrydia27->fetch(PDO::FETCH_ASSOC);
	
	$cedulas27 = $rowdia27['Cedulas'];
	$recaudo27 = $rowdia27['Valor'];


	
	$sqldia28 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."28'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia28 = $db->query($sqldia28);	
	$rowdia28 = $qrydia28->fetch(PDO::FETCH_ASSOC);
	
	$cedulas28 = $rowdia28['Cedulas'];
	$recaudo28 = $rowdia28['Valor'];


	
	$sqldia29 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."29'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia29 = $db->query($sqldia29);	
	$rowdia29 = $qrydia29->fetch(PDO::FETCH_ASSOC);
	
	$cedulas29 = $rowdia29['Cedulas'];
	$recaudo29 = $rowdia29['Valor'];


	
	$sqldia30 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."30'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia30 = $db->query($sqldia30);	
	$rowdia30 = $qrydia30->fetch(PDO::FETCH_ASSOC);
	
	$cedulas30 = $rowdia30['Cedulas'];
	$recaudo30 = $rowdia30['Valor'];


	
	$sqldia31 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."31'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia31 = $db->query($sqldia31);	
	$rowdia31 = $qrydia31->fetch(PDO::FETCH_ASSOC);
	
	$cedulas31 = $rowdia31['Cedulas'];
	$recaudo31 = $rowdia31['Valor'];

	$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':BQ'.$i)->applyFromArray($styleArray);
	$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('A'.$i, $row['usunombre'])
    	->setCellValue('B'.$i, $row['comvacedulas'])
		->setCellValue('C'.$i, $row['comvavalor'])
		->setCellValue('D'.$i, $rowrealizado['Cedulas'])
		->setCellValue('E'.$i, $rowrealizado['Valor'])
		->setCellValue('F'.$i, round($porcencedulas,1).' %')
		->setCellValue('G'.$i, round($porcenvalor,1).' %')
		->setCellValue('H'.$i, $cedulas1)
		->setCellValue('I'.$i, $recaudo1)
		->setCellValue('J'.$i, $cedulas2)
		->setCellValue('K'.$i, $recaudo2)	
		->setCellValue('L'.$i, $cedulas3)
		->setCellValue('M'.$i, $recaudo3)	
		->setCellValue('N'.$i, $cedulas4)
		->setCellValue('O'.$i, $recaudo4)	
		->setCellValue('P'.$i, $cedulas5)
		->setCellValue('Q'.$i, $recaudo5)	
		->setCellValue('R'.$i, $cedulas6)
		->setCellValue('S'.$i, $recaudo6)	
		->setCellValue('T'.$i, $cedulas7)
		->setCellValue('U'.$i, $recaudo7)	
		->setCellValue('V'.$i, $cedulas8)
		->setCellValue('W'.$i, $recaudo8)	
		->setCellValue('X'.$i, $cedulas9)
		->setCellValue('Y'.$i, $recaudo9)	
		->setCellValue('Z'.$i, $cedulas10)
		->setCellValue('AA'.$i, $recaudo10)	
		->setCellValue('AB'.$i, $cedulas11)
		->setCellValue('AC'.$i, $recaudo11)	
		->setCellValue('AD'.$i, $cedulas12)
		->setCellValue('AE'.$i, $recaudo12)	
		->setCellValue('AF'.$i, $cedulas13)
		->setCellValue('AG'.$i, $recaudo13)	
		->setCellValue('AH'.$i, $cedulas14)
		->setCellValue('AI'.$i, $recaudo14)	
		->setCellValue('AJ'.$i, $cedulas15)
		->setCellValue('AK'.$i, $recaudo15)	
		->setCellValue('AL'.$i, $cedulas16)
		->setCellValue('AM'.$i, $recaudo16)	
		->setCellValue('AN'.$i, $cedulas17)
		->setCellValue('AO'.$i, $recaudo17)	
		->setCellValue('AP'.$i, $cedulas18)
		->setCellValue('AQ'.$i, $recaudo18)	
		->setCellValue('AR'.$i, $cedulas19)
		->setCellValue('AS'.$i, $recaudo19)	
		->setCellValue('AT'.$i, $cedulas20)
		->setCellValue('AU'.$i, $recaudo20)	
		->setCellValue('AV'.$i, $cedulas21)
		->setCellValue('AW'.$i, $recaudo21)	
		->setCellValue('AX'.$i, $cedulas22)
		->setCellValue('AY'.$i, $recaudo22)	
		->setCellValue('AZ'.$i, $cedulas23)
		->setCellValue('BA'.$i, $recaudo23)	
		->setCellValue('BB'.$i, $cedulas24)
		->setCellValue('BC'.$i, $recaudo24)	
		->setCellValue('BD'.$i, $cedulas25)
		->setCellValue('BE'.$i, $recaudo25)	
		->setCellValue('BF'.$i, $cedulas26)
		->setCellValue('BG'.$i, $recaudo26)	
		->setCellValue('BH'.$i, $cedulas27)
		->setCellValue('BI'.$i, $recaudo27)	
		->setCellValue('BJ'.$i, $cedulas28)
		->setCellValue('BK'.$i, $recaudo28)	
		->setCellValue('BL'.$i, $cedulas29)
		->setCellValue('BM'.$i, $recaudo29)	
		->setCellValue('BN'.$i, $cedulas30)
		->setCellValue('BO'.$i, $recaudo30)	
		->setCellValue('BP'.$i, $cedulas31)
		->setCellValue('BQ'.$i, $recaudo31);	
	$i++;

	$tcedulas = $row['comvacedulas'] + $tcedulas;
	$tvalor = $row['comvavalor'] + $tvalor;
	$tcedurealizado = $rowrealizado['Cedulas'] + $tcedurealizado;
	$tvalorrealizado = $rowrealizado['Valor'] + $tvalorrealizado;
	$tporcencedulas = ($tcedurealizado / $tcedulas) * 100;
	$tporcenvalor = ($tvalorrealizado /  $tvalor) * 100;
	
$tcedulas1 = $rowdia1['Cedulas'] + $tcedulas1;
$trecaudo1 = $rowdia1['Valor'] + $trecaudo1;
$tcedulas2 = $rowdia2['Cedulas'] + $tcedulas2;
$trecaudo2 = $rowdia2['Valor'] + $trecaudo2;
$tcedulas3 = $rowdia3['Cedulas'] + $tcedulas3;
$trecaudo3 = $rowdia3['Valor'] + $trecaudo3;
$tcedulas4 = $rowdia4['Cedulas'] + $tcedulas4;
$trecaudo4 = $rowdia4['Valor'] + $trecaudo4;
$tcedulas5 = $rowdia5['Cedulas'] + $tcedulas5;
$trecaudo5 = $rowdia5['Valor'] + $trecaudo5;
$tcedulas6 = $rowdia6['Cedulas'] + $tcedulas6;
$trecaudo6 = $rowdia6['Valor'] + $trecaudo6;
$tcedulas7 = $rowdia7['Cedulas'] + $tcedulas7;
$trecaudo7 = $rowdia7['Valor'] + $trecaudo7;
$tcedulas8 = $rowdia8['Cedulas'] + $tcedulas8;
$trecaudo8 = $rowdia8['Valor'] + $trecaudo8;
$tcedulas9 = $rowdia9['Cedulas'] + $tcedulas9;
$trecaudo9 = $rowdia9['Valor'] + $trecaudo9;
$tcedulas10 = $rowdia10['Cedulas'] + $tcedulas10;
$trecaudo10 = $rowdia10['Valor'] + $trecaudo10;
$tcedulas11 = $rowdia11['Cedulas'] + $tcedulas11;
$trecaudo11 = $rowdia11['Valor'] + $trecaudo11;
$tcedulas12 = $rowdia12['Cedulas'] + $tcedulas12;
$trecaudo12 = $rowdia12['Valor'] + $trecaudo12;
$tcedulas13 = $rowdia13['Cedulas'] + $tcedulas13;
$trecaudo13 = $rowdia13['Valor'] + $trecaudo13;
$tcedulas14 = $rowdia14['Cedulas'] + $tcedulas14;
$trecaudo14 = $rowdia14['Valor'] + $trecaudo14;
$tcedulas15 = $rowdia15['Cedulas'] + $tcedulas15;
$trecaudo15 = $rowdia15['Valor'] + $trecaudo15;
$tcedulas16 = $rowdia16['Cedulas'] + $tcedulas16;
$trecaudo16 = $rowdia16['Valor'] + $trecaudo16;
$tcedulas17 = $rowdia17['Cedulas'] + $tcedulas17;
$trecaudo17 = $rowdia17['Valor'] + $trecaudo17;
$tcedulas18 = $rowdia18['Cedulas'] + $tcedulas18;
$trecaudo18 = $rowdia18['Valor'] + $trecaudo18;
$tcedulas19 = $rowdia19['Cedulas'] + $tcedulas19;
$trecaudo19 = $rowdia19['Valor'] + $trecaudo19;
$tcedulas20 = $rowdia20['Cedulas'] + $tcedulas20;
$trecaudo20 = $rowdia20['Valor'] + $trecaudo20;
$tcedulas21 = $rowdia21['Cedulas'] + $tcedulas21;
$trecaudo21 = $rowdia21['Valor'] + $trecaudo21;
$tcedulas22 = $rowdia22['Cedulas'] + $tcedulas22;
$trecaudo22 = $rowdia22['Valor'] + $trecaudo22;
$tcedulas23 = $rowdia23['Cedulas'] + $tcedulas23;
$trecaudo23 = $rowdia23['Valor'] + $trecaudo23;
$tcedulas24 = $rowdia24['Cedulas'] + $tcedulas24;
$trecaudo24 = $rowdia24['Valor'] + $trecaudo24;
$tcedulas25 = $rowdia25['Cedulas'] + $tcedulas25;
$trecaudo25 = $rowdia25['Valor'] + $trecaudo25;
$tcedulas26 = $rowdia26['Cedulas'] + $tcedulas26;
$trecaudo26 = $rowdia26['Valor'] + $trecaudo26;
$tcedulas27 = $rowdia27['Cedulas'] + $tcedulas27;
$trecaudo27 = $rowdia27['Valor'] + $trecaudo27;
$tcedulas28 = $rowdia28['Cedulas'] + $tcedulas28;
$trecaudo28 = $rowdia28['Valor'] + $trecaudo28;
$tcedulas29 = $rowdia29['Cedulas'] + $tcedulas29;
$trecaudo29 = $rowdia29['Valor'] + $trecaudo29;
$tcedulas30 = $rowdia30['Cedulas'] + $tcedulas30;
$trecaudo30 = $rowdia30['Valor'] + $trecaudo30;
$tcedulas31 = $rowdia31['Cedulas'] + $tcedulas31;
$trecaudo31 = $rowdia31['Valor'] + $trecaudo31;

	
$tcedulas1oficina =  $tcedulas1;
$trecaudo1oficina =  $trecaudo1;
$tcedulas2oficina = $tcedulas2;
$trecaudo2oficina =  $trecaudo2;
$tcedulas3oficina =  $tcedulas3;
$trecaudo3oficina =  $trecaudo3;
$tcedulas4oficina = $tcedulas4;
$trecaudo4oficina =  $trecaudo4;
$tcedulas5oficina = $tcedulas5;
$trecaudo5oficina = $trecaudo5;
$tcedulas6oficina =  $tcedulas6;
$trecaudo6oficina =  $trecaudo6;
$tcedulas7oficina = $tcedulas7;
$trecaudo7oficina = $trecaudo7;
$tcedulas8oficina =  $tcedulas8;
$trecaudo8oficina =  $trecaudo8;
$tcedulas9oficina =  $tcedulas9;
$trecaudo9oficina =  $trecaudo9;
$tcedulas10oficina =  $tcedulas10;
$trecaudo10oficina =  $trecaudo10;
$tcedulas11oficina =  $tcedulas11;
$trecaudo11oficina =  $trecaudo11;
$tcedulas12oficina =  $tcedulas12;
$trecaudo12oficina =  $trecaudo12;
$tcedulas13oficina =  $tcedulas13;
$trecaudo13oficina =  $trecaudo13;
$tcedulas14oficina =  $tcedulas14;
$trecaudo14oficina =  $trecaudo14;
$tcedulas15oficina =  $tcedulas15;
$trecaudo15oficina =  $trecaudo15;
$tcedulas16oficina =  $tcedulas16;
$trecaudo16oficina = $trecaudo16;
$tcedulas17oficina =  $tcedulas17;
$trecaudo17oficina =  $trecaudo17;
$tcedulas18oficina =  $tcedulas18;
$trecaudo18oficina =  $trecaudo18;
$tcedulas19oficina =  $tcedulas19;
$trecaudo19oficina =  $trecaudo19;
$tcedulas20oficina =  $tcedulas20;
$trecaudo20oficina = $trecaudo20;
$tcedulas21oficina =  $tcedulas21;
$trecaudo21oficina =  $trecaudo21;
$tcedulas22oficina =  $tcedulas22;
$trecaudo22oficina =  $trecaudo22;
$tcedulas23oficina =  $tcedulas23;
$trecaudo23oficina =  $trecaudo23;
$tcedulas24oficina =  $tcedulas24;
$trecaudo24oficina =  $trecaudo24;
$tcedulas25oficina =  $tcedulas25;
$trecaudo25oficina =  $trecaudo25;
$tcedulas26oficina =  $tcedulas26;
$trecaudo26oficina = $trecaudo26;
$tcedulas27oficina =  $tcedulas27;
$trecaudo27oficina =  $trecaudo27;
$tcedulas28oficina =  $tcedulas28;
$trecaudo28oficina =  $trecaudo28;
$tcedulas29oficina =  $tcedulas29;
$trecaudo29oficina =  $trecaudo29;
$tcedulas30oficina =  $tcedulas30;
$trecaudo30oficina =  $trecaudo30;
$tcedulas31oficina =  $tcedulas31;
$trecaudo31oficina =  $trecaudo31;
$tcedulasoficina = $tcedulas;
$tvaloroficina = $tvlor;
$tcedurealizadooficina = $tcedurealizado;
$tvalorrealizadooficina = $tvalorrealizado; 





}
}
	$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':BQ'.$i)->applyFromArray($styleArrayTwo);
	$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('A'.$i, 'TOTAL')
    	->setCellValue('B'.$i, $tcedulas)
		->setCellValue('C'.$i, $tvalor)
		->setCellValue('D'.$i, $tcedurealizado)
		->setCellValue('E'.$i, $tvalorrealizado)
		->setCellValue('F'.$i, round($tporcencedulas,1).' %')
		->setCellValue('G'.$i, round($tporcenvalor,1).' %')
		->setCellValue('H'.$i, $tcedulas1)
		->setCellValue('I'.$i, $trecaudo1)
		->setCellValue('J'.$i, $tcedulas2)
		->setCellValue('K'.$i, $trecaudo2)	
		->setCellValue('L'.$i, $tcedulas3)
		->setCellValue('M'.$i, $trecaudo3)	
		->setCellValue('N'.$i, $tcedulas4)
		->setCellValue('O'.$i, $trecaudo4)	
		->setCellValue('P'.$i, $tcedulas5)
		->setCellValue('Q'.$i, $trecaudo5)	
		->setCellValue('R'.$i, $tcedulas6)
		->setCellValue('S'.$i, $trecaudo6)	
		->setCellValue('T'.$i, $tcedulas7)
		->setCellValue('U'.$i, $trecaudo7)	
		->setCellValue('V'.$i, $tcedulas8)
		->setCellValue('W'.$i, $trecaudo8)	
		->setCellValue('X'.$i, $tcedulas9)
		->setCellValue('Y'.$i, $trecaudo9)	
		->setCellValue('Z'.$i, $tcedulas10)
		->setCellValue('AA'.$i, $trecaudo10)	
		->setCellValue('AB'.$i, $tcedulas11)
		->setCellValue('AC'.$i, $trecaudo11)	
		->setCellValue('AD'.$i, $tcedulas12)
		->setCellValue('AE'.$i, $trecaudo12)	
		->setCellValue('AF'.$i, $tcedulas13)
		->setCellValue('AG'.$i, $trecaudo13)	
		->setCellValue('AH'.$i, $tcedulas14)
		->setCellValue('AI'.$i, $trecaudo14)	
		->setCellValue('AJ'.$i, $tcedulas15)
		->setCellValue('AK'.$i, $trecaudo15)	
		->setCellValue('AL'.$i, $tcedulas16)
		->setCellValue('AM'.$i, $trecaudo16)	
		->setCellValue('AN'.$i, $tcedulas17)
		->setCellValue('AO'.$i, $trecaudo17)	
		->setCellValue('AP'.$i, $tcedulas18)
		->setCellValue('AQ'.$i, $trecaudo18)	
		->setCellValue('AR'.$i, $tcedulas19)
		->setCellValue('AS'.$i, $trecaudo19)	
		->setCellValue('AT'.$i, $tcedulas20)
		->setCellValue('AU'.$i, $trecaudo20)	
		->setCellValue('AV'.$i, $tcedulas21)
		->setCellValue('AW'.$i, $trecaudo21)	
		->setCellValue('AX'.$i, $tcedulas22)
		->setCellValue('AY'.$i, $trecaudo22)	
		->setCellValue('AZ'.$i, $tcedulas23)
		->setCellValue('BA'.$i, $trecaudo23)	
		->setCellValue('BB'.$i, $tcedulas24)
		->setCellValue('BC'.$i, $trecaudo24)	
		->setCellValue('BD'.$i, $tcedulas25)
		->setCellValue('BE'.$i, $trecaudo25)	
		->setCellValue('BF'.$i, $tcedulas26)
		->setCellValue('BG'.$i, $trecaudo26)	
		->setCellValue('BH'.$i, $tcedulas27)
		->setCellValue('BI'.$i, $trecaudo27)	
		->setCellValue('BJ'.$i, $tcedulas28)
		->setCellValue('BK'.$i, $trecaudo28)	
		->setCellValue('BL'.$i, $tcedulas29)
		->setCellValue('BM'.$i, $trecaudo29)	
		->setCellValue('BN'.$i, $tcedulas30)
		->setCellValue('BO'.$i, $trecaudo30)	
		->setCellValue('BP'.$i, $tcedulas31)
		->setCellValue('BQ'.$i, $trecaudo31);	
	$i++;				


// ----------------------------------------------------------------------------------------------------------


$tcedulas1  = 0;
$tcedulas2  = 0;
$tcedulas3  = 0;
$tcedulas4  = 0;
$tcedulas5  = 0;
$tcedulas6  = 0;
$tcedulas7  = 0;
$tcedulas8  = 0;
$tcedulas9  = 0;
$tcedulas10  = 0;
$tcedulas11  = 0;
$tcedulas12  = 0;
$tcedulas13  = 0;
$tcedulas14  = 0;
$tcedulas15  = 0;
$tcedulas16  = 0;
$tcedulas17  = 0;
$tcedulas18  = 0;
$tcedulas19  = 0;
$tcedulas20  = 0;
$tcedulas21  = 0;
$tcedulas22  = 0;
$tcedulas23  = 0;
$tcedulas24  = 0;
$tcedulas25  = 0;
$tcedulas26  = 0;
$tcedulas27  = 0;
$tcedulas28  = 0;
$tcedulas29  = 0;
$tcedulas30  = 0;
$tcedulas31  = 0;
$trecaudo1  = 0;
$trecaudo2  = 0;
$trecaudo3  = 0;
$trecaudo4  = 0;
$trecaudo5  = 0;
$trecaudo6  = 0;
$trecaudo7  = 0;
$trecaudo8  = 0;
$trecaudo9  = 0;
$trecaudo10  = 0;
$trecaudo11  = 0;
$trecaudo12  = 0;
$trecaudo13  = 0;
$trecaudo14  = 0;
$trecaudo15  = 0;
$trecaudo16  = 0;
$trecaudo17  = 0;
$trecaudo18  = 0;
$trecaudo19  = 0;
$trecaudo20  = 0;
$trecaudo21  = 0;
$trecaudo22  = 0;
$trecaudo23  = 0;
$trecaudo24  = 0;
$trecaudo25  = 0;
$trecaudo26  = 0;
$trecaudo27  = 0;
$trecaudo28  = 0;
$trecaudo29  = 0;
$trecaudo30  = 0;
$trecaudo31  = 0;
$tcedulas = 0;
$tvalor = 0;
$tcedurealizado = 0;
$tvalorrealizado = 0;
$tporcencedulas = 0;
$tporcenvalor = 0;

	
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$i.':C'.$i);
$objPHPExcel->getActiveSheet()->getStyle('A'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$i, 'LIBRANZA');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('D'.$i.':E'.$i);
$objPHPExcel->getActiveSheet()->getStyle('D'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$i, 'REALIZADO');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('F'.$i.':G'.$i);
$objPHPExcel->getActiveSheet()->getStyle('F'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$i, ' % REALIZADO');
 
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('H'.$i.':I'.$i);
$objPHPExcel->getActiveSheet()->getStyle('H'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$i, '1');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('J'.$i.':K'.$i);
$objPHPExcel->getActiveSheet()->getStyle('J'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J'.$i, '2');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('L'.$i.':M'.$i);
$objPHPExcel->getActiveSheet()->getStyle('L'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('L'.$i, '3');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('N'.$i.':O'.$i);
$objPHPExcel->getActiveSheet()->getStyle('N'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('N'.$i, '4');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('P'.$i.':Q'.$i);
$objPHPExcel->getActiveSheet()->getStyle('P'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('P'.$i, '5');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('R'.$i.':S'.$i);
$objPHPExcel->getActiveSheet()->getStyle('L'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('R'.$i, '6');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('T'.$i.':U'.$i);
$objPHPExcel->getActiveSheet()->getStyle('T'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('T'.$i, '7');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('V'.$i.':W'.$i);
$objPHPExcel->getActiveSheet()->getStyle('V'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('V'.$i, '8');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('X'.$i.':Y'.$i);
$objPHPExcel->getActiveSheet()->getStyle('X'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('X'.$i, '9');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('Z'.$i.':AA'.$i);
$objPHPExcel->getActiveSheet()->getStyle('Z'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('Z'.$i, '10');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AB'.$i.':AC'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AB'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AB'.$i, '11');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AB'.$i.':AC'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AB'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AB'.$i, '11');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AD'.$i.':AE'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AD'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AD'.$i, '12');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AF'.$i.':AG'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AF'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AF'.$i, '13');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AH'.$i.':AI'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AH'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AH'.$i, '14');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AJ'.$i.':AK'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AJ'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AJ'.$i, '15');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AL'.$i.':AM'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AL'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AL'.$i, '16');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AN'.$i.':AO'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AN'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AN'.$i, '17');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AP'.$i.':AQ'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AP'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AP'.$i, '18');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AR'.$i.':AS'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AR'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AR'.$i, '19');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AT'.$i.':AU'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AT'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AT'.$i, '20');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AV'.$i.':AW'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AV'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AV'.$i, '21');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AX'.$i.':AY'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AX'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AX'.$i, '22');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AZ'.$i.':BA'.$i);
$objPHPExcel->getActiveSheet()->getStyle('AZ'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AZ'.$i, '23');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BB'.$i.':BC'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BB'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BB'.$i, '24');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BD'.$i.':BE'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BD'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BD'.$i, '25');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BF'.$i.':BG'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BF'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BF'.$i, '26');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BH'.$i.':BI'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BH'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BH'.$i, '27');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BJ'.$i.':BK'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BJ'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BJ'.$i, '28');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BL'.$i.':BM'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BL'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BL'.$i, '29');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BN'.$i.':BO'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BN'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BN'.$i, '30');

$objPHPExcel->setActiveSheetIndex(0)->mergeCells('BP'.$i.':BQ'.$i);
$objPHPExcel->getActiveSheet()->getStyle('BP'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('BP'.$i, '31');


$i++;
$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':BQ'.$i)->applyFromArray($styleArrayOne);
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$i, 'ASESOR')
            ->setCellValue('B'.$i, 'CEDULAS')
            ->setCellValue('C'.$i, 'VALOR')
            ->setCellValue('D'.$i, 'CEDULAS')
			->setCellValue('E'.$i, 'VALOR')
			->setCellValue('F'.$i, 'CEDULAS')
			->setCellValue('G'.$i, 'VALOR')	
			->setCellValue('H'.$i, 'CEDULAS')
			->setCellValue('I'.$i, 'RECAUDO')
			->setCellValue('J'.$i, 'CEDULAS')
			->setCellValue('K'.$i, 'RECAUDO')	
			->setCellValue('L'.$i, 'CEDULAS')
			->setCellValue('M'.$i, 'RECAUDO')	
			->setCellValue('N'.$i, 'CEDULAS')
			->setCellValue('O'.$i, 'RECAUDO')	
			->setCellValue('P'.$i, 'CEDULAS')
			->setCellValue('Q'.$i, 'RECAUDO')	
			->setCellValue('R'.$i, 'CEDULAS')
			->setCellValue('S'.$i, 'RECAUDO')	
			->setCellValue('T'.$i, 'CEDULAS')
			->setCellValue('U'.$i, 'RECAUDO')	
			->setCellValue('V'.$i, 'CEDULAS')
			->setCellValue('W'.$i, 'RECAUDO')	
			->setCellValue('X'.$i, 'CEDULAS')
			->setCellValue('Y'.$i, 'RECAUDO')	
			->setCellValue('Z'.$i, 'CEDULAS')
			->setCellValue('AA'.$i, 'RECAUDO')	
			->setCellValue('AB'.$i, 'CEDULAS')
			->setCellValue('AC'.$i, 'RECAUDO')	
			->setCellValue('AD'.$i, 'CEDULAS')
			->setCellValue('AE'.$i, 'RECAUDO')	
			->setCellValue('AF'.$i, 'CEDULAS')
			->setCellValue('AG'.$i, 'RECAUDO')	
			->setCellValue('AH'.$i, 'CEDULAS')
			->setCellValue('AI'.$i, 'RECAUDO')	
			->setCellValue('AJ'.$i, 'CEDULAS')
			->setCellValue('AK'.$i, 'RECAUDO')	
			->setCellValue('AL'.$i, 'CEDULAS')
			->setCellValue('AM'.$i, 'RECAUDO')	
			->setCellValue('AN'.$i, 'CEDULAS')
			->setCellValue('AO'.$i, 'RECAUDO')	
			->setCellValue('AP'.$i, 'CEDULAS')
			->setCellValue('AQ'.$i, 'RECAUDO')	
			->setCellValue('AR'.$i, 'CEDULAS')
			->setCellValue('AS'.$i, 'RECAUDO')	
			->setCellValue('AT'.$i, 'CEDULAS')
			->setCellValue('AU'.$i, 'RECAUDO')	
			->setCellValue('AV'.$i, 'CEDULAS')
			->setCellValue('AW'.$i, 'RECAUDO')	
			->setCellValue('AX'.$i, 'CEDULAS')
			->setCellValue('AY'.$i, 'RECAUDO')	
			->setCellValue('AZ'.$i, 'CEDULAS')
			->setCellValue('BA'.$i, 'RECAUDO')	
			->setCellValue('BB'.$i, 'CEDULAS')
			->setCellValue('BC'.$i, 'RECAUDO')	
			->setCellValue('BD'.$i, 'CEDULAS')
			->setCellValue('BE'.$i, 'RECAUDO')	
			->setCellValue('BF'.$i, 'CEDULAS')
			->setCellValue('BG'.$i, 'RECAUDO')	
			->setCellValue('BH'.$i, 'CEDULAS')
			->setCellValue('BI'.$i, 'RECAUDO')	
			->setCellValue('BJ'.$i, 'CEDULAS')
			->setCellValue('BK'.$i, 'RECAUDO')	
			->setCellValue('BL'.$i, 'CEDULAS')
			->setCellValue('BM'.$i, 'RECAUDO')	
			->setCellValue('BN'.$i, 'CEDULAS')
			->setCellValue('BO'.$i, 'RECAUDO')	
			->setCellValue('BP'.$i, 'CEDULAS')
			->setCellValue('BQ'.$i, 'RECAUDO');
			$i++;	

$sql = "SELECT * FROM comoVamos INNER JOIN usuarios ON usuid = comvausuario INNER JOIN comovamosgrupodeta ON grupousuario = comvausuario WHERE comvafecha = '$fecha' AND grupofecha = '$fecha';";
$qry = $db->query($sql);			

while($row = $qry->fetch(PDO::FETCH_ASSOC)){
if ($row['gruponombre'] == 'LIBRANZA') {
	
	$sqlrealizado = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha >= '$fecha'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";

	$qryrealizado = $db->query($sqlrealizado);	
	$rowrealizado = $qryrealizado->fetch(PDO::FETCH_ASSOC);	
	
	$porcencedulas = ($rowrealizado['Cedulas'] / $row['comvacedulas']) * 100;
	$porcenvalor = ($rowrealizado['Valor'] /  $row['comvavalor']) * 100;
	
	$fechasindias = substr($fecha, 0, -2);
	

	$sqldia1 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."01'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia1 = $db->query($sqldia1);	
	$rowdia1 = $qrydia1->fetch(PDO::FETCH_ASSOC);
	
	$cedulas1 = $rowdia1['Cedulas'];
	$recaudo1 = $rowdia1['Valor'];
	
	
	$sqldia2 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."02'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia2 = $db->query($sqldia2);	
	$rowdia2 = $qrydia2->fetch(PDO::FETCH_ASSOC);
	
	$cedulas2 = $rowdia2['Cedulas'];
	$recaudo2 = $rowdia2['Valor'];


	$sqldia3 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."03'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia3 = $db->query($sqldia3);	
	$rowdia3 = $qrydia3->fetch(PDO::FETCH_ASSOC);
	
	$cedulas3 = $rowdia3['Cedulas'];
	$recaudo3 = $rowdia3['Valor'];


	$sqldia4 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."04'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia4 = $db->query($sqldia4);	
	$rowdia4 = $qrydia4->fetch(PDO::FETCH_ASSOC);
	
	$cedulas4 = $rowdia4['Cedulas'];
	$recaudo4 = $rowdia4['Valor'];


	$sqldia5 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."05'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia5 = $db->query($sqldia5);	
	$rowdia5 = $qrydia5->fetch(PDO::FETCH_ASSOC);
	
	$cedulas5 = $rowdia5['Cedulas'];
	$recaudo5 = $rowdia5['Valor'];


	$sqldia6 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."06'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia6 = $db->query($sqldia6);	
	$rowdia6 = $qrydia6->fetch(PDO::FETCH_ASSOC);
	
	$cedulas6 = $rowdia6['Cedulas'];
	$recaudo6 = $rowdia6['Valor'];


	$sqldia7 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."07'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia7 = $db->query($sqldia7);	
	$rowdia7 = $qrydia7->fetch(PDO::FETCH_ASSOC);
	
	$cedulas7 = $rowdia7['Cedulas'];
	$recaudo7 = $rowdia7['Valor'];


	$sqldia8 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."08'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia8 = $db->query($sqldia8);	
	$rowdia8 = $qrydia8->fetch(PDO::FETCH_ASSOC);
	
	$cedulas8 = $rowdia8['Cedulas'];
	$recaudo8 = $rowdia8['Valor'];


	$sqldia9 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."09'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia9 = $db->query($sqldia9);	
	$rowdia9 = $qrydia9->fetch(PDO::FETCH_ASSOC);
	
	$cedulas9 = $rowdia9['Cedulas'];
	$recaudo9 = $rowdia9['Valor'];


		
	$sqldia10 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."10'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia10 = $db->query($sqldia10);	
	$rowdia10 = $qrydia10->fetch(PDO::FETCH_ASSOC);
	
	$cedulas10 = $rowdia10['Cedulas'];
	$recaudo10 = $rowdia10['Valor'];


		
	$sqldia11 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."11'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia11 = $db->query($sqldia11);	
	$rowdia11 = $qrydia11->fetch(PDO::FETCH_ASSOC);
	
	$cedulas11 = $rowdia11['Cedulas'];
	$recaudo11 = $rowdia11['Valor'];


	
	$sqldia12 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."12'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia12 = $db->query($sqldia12);	
	$rowdia12 = $qrydia12->fetch(PDO::FETCH_ASSOC);
	
	$cedulas12 = $rowdia12['Cedulas'];
	$recaudo12 = $rowdia12['Valor'];


	
	$sqldia13 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."13'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia13 = $db->query($sqldia13);	
	$rowdia13 = $qrydia13->fetch(PDO::FETCH_ASSOC);
	
	$cedulas13 = $rowdia13['Cedulas'];
	$recaudo13 = $rowdia13['Valor'];


	
	$sqldia14 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."14'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia14 = $db->query($sqldia14);	
	$rowdia14 = $qrydia14->fetch(PDO::FETCH_ASSOC);
	
	$cedulas14 = $rowdia14['Cedulas'];
	$recaudo14 = $rowdia14['Valor'];


	
	$sqldia15 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."15'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia15 = $db->query($sqldia15);	
	$rowdia15 = $qrydia15->fetch(PDO::FETCH_ASSOC);
	
	$cedulas15 = $rowdia15['Cedulas'];
	$recaudo15 = $rowdia15['Valor'];


	
	$sqldia16 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."16'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia16 = $db->query($sqldia16);	
	$rowdia16 = $qrydia16->fetch(PDO::FETCH_ASSOC);
	
	$cedulas16 = $rowdia16['Cedulas'];
	$recaudo16 = $rowdia16['Valor'];


	
	$sqldia17 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."17'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia17 = $db->query($sqldia17);	
	$rowdia17 = $qrydia17->fetch(PDO::FETCH_ASSOC);
	
	$cedulas17 = $rowdia17['Cedulas'];
	$recaudo17 = $rowdia17['Valor'];


	
	$sqldia18 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."18'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia18 = $db->query($sqldia18);	
	$rowdia18 = $qrydia18->fetch(PDO::FETCH_ASSOC);
	
	$cedulas18 = $rowdia18['Cedulas'];
	$recaudo18 = $rowdia18['Valor'];


	
	$sqldia19 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."19'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia19 = $db->query($sqldia19);	
	$rowdia19 = $qrydia19->fetch(PDO::FETCH_ASSOC);
	
	$cedulas19 = $rowdia19['Cedulas'];
	$recaudo19 = $rowdia19['Valor'];


	
	$sqldia20 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."20'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia20 = $db->query($sqldia20);	
	$rowdia20 = $qrydia20->fetch(PDO::FETCH_ASSOC);
	
	$cedulas20 = $rowdia20['Cedulas'];
	$recaudo20 = $rowdia20['Valor'];


	
	$sqldia21 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."21'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia21 = $db->query($sqldia21);	
	$rowdia21 = $qrydia21->fetch(PDO::FETCH_ASSOC);
	
	$cedulas21 = $rowdia21['Cedulas'];
	$recaudo21 = $rowdia21['Valor'];


	
	$sqldia22 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."22'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia22 = $db->query($sqldia22);	
	$rowdia22 = $qrydia22->fetch(PDO::FETCH_ASSOC);
	
	$cedulas22 = $rowdia22['Cedulas'];
	$recaudo22 = $rowdia22['Valor'];


	
	$sqldia23 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."23'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia23 = $db->query($sqldia23);	
	$rowdia23 = $qrydia23->fetch(PDO::FETCH_ASSOC);
	
	$cedulas23 = $rowdia23['Cedulas'];
	$recaudo23 = $rowdia23['Valor'];


	
	$sqldia24 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."24'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia24 = $db->query($sqldia24);	
	$rowdia24 = $qrydia24->fetch(PDO::FETCH_ASSOC);
	
	$cedulas24 = $rowdia24['Cedulas'];
	$recaudo24 = $rowdia24['Valor'];


	
	$sqldia25 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."25'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia25 = $db->query($sqldia25);	
	$rowdia25 = $qrydia25->fetch(PDO::FETCH_ASSOC);
	
	$cedulas25 = $rowdia25['Cedulas'];
	$recaudo25 = $rowdia25['Valor'];


	
	$sqldia26 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."26'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia26 = $db->query($sqldia26);	
	$rowdia26 = $qrydia26->fetch(PDO::FETCH_ASSOC);
	
	$cedulas26 = $rowdia26['Cedulas'];
	$recaudo26 = $rowdia26['Valor'];


	
	$sqldia27 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."27'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia27 = $db->query($sqldia27);	
	$rowdia27 = $qrydia27->fetch(PDO::FETCH_ASSOC);
	
	$cedulas27 = $rowdia27['Cedulas'];
	$recaudo27 = $rowdia27['Valor'];


	
	$sqldia28 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."28'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia28 = $db->query($sqldia28);	
	$rowdia28 = $qrydia28->fetch(PDO::FETCH_ASSOC);
	
	$cedulas28 = $rowdia28['Cedulas'];
	$recaudo28 = $rowdia28['Valor'];


	
	$sqldia29 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."29'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia29 = $db->query($sqldia29);	
	$rowdia29 = $qrydia29->fetch(PDO::FETCH_ASSOC);
	
	$cedulas29 = $rowdia29['Cedulas'];
	$recaudo29 = $rowdia29['Valor'];


	
	$sqldia30 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."30'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia30 = $db->query($sqldia30);	
	$rowdia30 = $qrydia30->fetch(PDO::FETCH_ASSOC);
	
	$cedulas30 = $rowdia30['Cedulas'];
	$recaudo30 = $rowdia30['Valor'];


	
	$sqldia31 = "SELECT carpromotor as Promotor, SUM(movvalor) as Valor, COUNT(movvalor) as Cedulas 
	FROM movimientos AS M

		INNER JOIN carteras
		AS C
		ON C.carfactura = M.movdocumento AND C.carempresa = M.movempresa

	WHERE M.movprefijo = 'RC'
	AND M.movestado = 'FINALIZADO'
	AND M.movfecha = '".$fechasindias."31'
	AND M.movtercero NOT IN ('900440758',
	'900306685',
	'900299709',
	'900051528',
	'900487926',
	'900838719',
	'1',
	'9000515281',
	'900813686',
	'900249109',
	'900487915')
	AND C.carpromotor = '".$row['usuid']."'
	GROUP BY carpromotor";
	$qrydia31 = $db->query($sqldia31);	
	$rowdia31 = $qrydia31->fetch(PDO::FETCH_ASSOC);
	
	$cedulas31 = $rowdia31['Cedulas'];
	$recaudo31 = $rowdia31['Valor'];

	$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':BQ'.$i)->applyFromArray($styleArray);
	$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('A'.$i, $row['usunombre'])
    	->setCellValue('B'.$i, $row['comvacedulas'])
		->setCellValue('C'.$i, $row['comvavalor'])
		->setCellValue('D'.$i, $rowrealizado['Cedulas'])
		->setCellValue('E'.$i, $rowrealizado['Valor'])
		->setCellValue('F'.$i, round($porcencedulas,1).' %')
		->setCellValue('G'.$i, round($porcenvalor,1).' %')
		->setCellValue('H'.$i, $cedulas1)
		->setCellValue('I'.$i, $recaudo1)
		->setCellValue('J'.$i, $cedulas2)
		->setCellValue('K'.$i, $recaudo2)	
		->setCellValue('L'.$i, $cedulas3)
		->setCellValue('M'.$i, $recaudo3)	
		->setCellValue('N'.$i, $cedulas4)
		->setCellValue('O'.$i, $recaudo4)	
		->setCellValue('P'.$i, $cedulas5)
		->setCellValue('Q'.$i, $recaudo5)	
		->setCellValue('R'.$i, $cedulas6)
		->setCellValue('S'.$i, $recaudo6)	
		->setCellValue('T'.$i, $cedulas7)
		->setCellValue('U'.$i, $recaudo7)	
		->setCellValue('V'.$i, $cedulas8)
		->setCellValue('W'.$i, $recaudo8)	
		->setCellValue('X'.$i, $cedulas9)
		->setCellValue('Y'.$i, $recaudo9)	
		->setCellValue('Z'.$i, $cedulas10)
		->setCellValue('AA'.$i, $recaudo10)	
		->setCellValue('AB'.$i, $cedulas11)
		->setCellValue('AC'.$i, $recaudo11)	
		->setCellValue('AD'.$i, $cedulas12)
		->setCellValue('AE'.$i, $recaudo12)	
		->setCellValue('AF'.$i, $cedulas13)
		->setCellValue('AG'.$i, $recaudo13)	
		->setCellValue('AH'.$i, $cedulas14)
		->setCellValue('AI'.$i, $recaudo14)	
		->setCellValue('AJ'.$i, $cedulas15)
		->setCellValue('AK'.$i, $recaudo15)	
		->setCellValue('AL'.$i, $cedulas16)
		->setCellValue('AM'.$i, $recaudo16)	
		->setCellValue('AN'.$i, $cedulas17)
		->setCellValue('AO'.$i, $recaudo17)	
		->setCellValue('AP'.$i, $cedulas18)
		->setCellValue('AQ'.$i, $recaudo18)	
		->setCellValue('AR'.$i, $cedulas19)
		->setCellValue('AS'.$i, $recaudo19)	
		->setCellValue('AT'.$i, $cedulas20)
		->setCellValue('AU'.$i, $recaudo20)	
		->setCellValue('AV'.$i, $cedulas21)
		->setCellValue('AW'.$i, $recaudo21)	
		->setCellValue('AX'.$i, $cedulas22)
		->setCellValue('AY'.$i, $recaudo22)	
		->setCellValue('AZ'.$i, $cedulas23)
		->setCellValue('BA'.$i, $recaudo23)	
		->setCellValue('BB'.$i, $cedulas24)
		->setCellValue('BC'.$i, $recaudo24)	
		->setCellValue('BD'.$i, $cedulas25)
		->setCellValue('BE'.$i, $recaudo25)	
		->setCellValue('BF'.$i, $cedulas26)
		->setCellValue('BG'.$i, $recaudo26)	
		->setCellValue('BH'.$i, $cedulas27)
		->setCellValue('BI'.$i, $recaudo27)	
		->setCellValue('BJ'.$i, $cedulas28)
		->setCellValue('BK'.$i, $recaudo28)	
		->setCellValue('BL'.$i, $cedulas29)
		->setCellValue('BM'.$i, $recaudo29)	
		->setCellValue('BN'.$i, $cedulas30)
		->setCellValue('BO'.$i, $recaudo30)	
		->setCellValue('BP'.$i, $cedulas31)
		->setCellValue('BQ'.$i, $recaudo31);	
	$i++;

	$tcedulas = $row['comvacedulas'] + $tcedulas;
	$tvalor = $row['comvavalor'] + $tvalor;
	$tcedurealizado = $rowrealizado['Cedulas'] + $tcedurealizado;
	$tvalorrealizado = $rowrealizado['Valor'] + $tvalorrealizado;
	$tporcencedulas = ($tcedurealizado / $tcedulas) * 100;
	$tporcenvalor = ($tvalorrealizado /  $tvalor) * 100;
	
$tcedulas1 = $rowdia1['Cedulas'] + $tcedulas1;
$trecaudo1 = $rowdia1['Valor'] + $trecaudo1;
$tcedulas2 = $rowdia2['Cedulas'] + $tcedulas2;
$trecaudo2 = $rowdia2['Valor'] + $trecaudo2;
$tcedulas3 = $rowdia3['Cedulas'] + $tcedulas3;
$trecaudo3 = $rowdia3['Valor'] + $trecaudo3;
$tcedulas4 = $rowdia4['Cedulas'] + $tcedulas4;
$trecaudo4 = $rowdia4['Valor'] + $trecaudo4;
$tcedulas5 = $rowdia5['Cedulas'] + $tcedulas5;
$trecaudo5 = $rowdia5['Valor'] + $trecaudo5;
$tcedulas6 = $rowdia6['Cedulas'] + $tcedulas6;
$trecaudo6 = $rowdia6['Valor'] + $trecaudo6;
$tcedulas7 = $rowdia7['Cedulas'] + $tcedulas7;
$trecaudo7 = $rowdia7['Valor'] + $trecaudo7;
$tcedulas8 = $rowdia8['Cedulas'] + $tcedulas8;
$trecaudo8 = $rowdia8['Valor'] + $trecaudo8;
$tcedulas9 = $rowdia9['Cedulas'] + $tcedulas9;
$trecaudo9 = $rowdia9['Valor'] + $trecaudo9;
$tcedulas10 = $rowdia10['Cedulas'] + $tcedulas10;
$trecaudo10 = $rowdia10['Valor'] + $trecaudo10;
$tcedulas11 = $rowdia11['Cedulas'] + $tcedulas11;
$trecaudo11 = $rowdia11['Valor'] + $trecaudo11;
$tcedulas12 = $rowdia12['Cedulas'] + $tcedulas12;
$trecaudo12 = $rowdia12['Valor'] + $trecaudo12;
$tcedulas13 = $rowdia13['Cedulas'] + $tcedulas13;
$trecaudo13 = $rowdia13['Valor'] + $trecaudo13;
$tcedulas14 = $rowdia14['Cedulas'] + $tcedulas14;
$trecaudo14 = $rowdia14['Valor'] + $trecaudo14;
$tcedulas15 = $rowdia15['Cedulas'] + $tcedulas15;
$trecaudo15 = $rowdia15['Valor'] + $trecaudo15;
$tcedulas16 = $rowdia16['Cedulas'] + $tcedulas16;
$trecaudo16 = $rowdia16['Valor'] + $trecaudo16;
$tcedulas17 = $rowdia17['Cedulas'] + $tcedulas17;
$trecaudo17 = $rowdia17['Valor'] + $trecaudo17;
$tcedulas18 = $rowdia18['Cedulas'] + $tcedulas18;
$trecaudo18 = $rowdia18['Valor'] + $trecaudo18;
$tcedulas19 = $rowdia19['Cedulas'] + $tcedulas19;
$trecaudo19 = $rowdia19['Valor'] + $trecaudo19;
$tcedulas20 = $rowdia20['Cedulas'] + $tcedulas20;
$trecaudo20 = $rowdia20['Valor'] + $trecaudo20;
$tcedulas21 = $rowdia21['Cedulas'] + $tcedulas21;
$trecaudo21 = $rowdia21['Valor'] + $trecaudo21;
$tcedulas22 = $rowdia22['Cedulas'] + $tcedulas22;
$trecaudo22 = $rowdia22['Valor'] + $trecaudo22;
$tcedulas23 = $rowdia23['Cedulas'] + $tcedulas23;
$trecaudo23 = $rowdia23['Valor'] + $trecaudo23;
$tcedulas24 = $rowdia24['Cedulas'] + $tcedulas24;
$trecaudo24 = $rowdia24['Valor'] + $trecaudo24;
$tcedulas25 = $rowdia25['Cedulas'] + $tcedulas25;
$trecaudo25 = $rowdia25['Valor'] + $trecaudo25;
$tcedulas26 = $rowdia26['Cedulas'] + $tcedulas26;
$trecaudo26 = $rowdia26['Valor'] + $trecaudo26;
$tcedulas27 = $rowdia27['Cedulas'] + $tcedulas27;
$trecaudo27 = $rowdia27['Valor'] + $trecaudo27;
$tcedulas28 = $rowdia28['Cedulas'] + $tcedulas28;
$trecaudo28 = $rowdia28['Valor'] + $trecaudo28;
$tcedulas29 = $rowdia29['Cedulas'] + $tcedulas29;
$trecaudo29 = $rowdia29['Valor'] + $trecaudo29;
$tcedulas30 = $rowdia30['Cedulas'] + $tcedulas30;
$trecaudo30 = $rowdia30['Valor'] + $trecaudo30;
$tcedulas31 = $rowdia31['Cedulas'] + $tcedulas31;
$trecaudo31 = $rowdia31['Valor'] + $trecaudo31;


	
$tcedulas1libranza =  $tcedulas1;
$trecaudo1libranza =  $trecaudo1;
$tcedulas2libranza =  $tcedulas2;
$trecaudo2libranza =  $trecaudo2;
$tcedulas3libranza =  $tcedulas3;
$trecaudo3libranza =  $trecaudo3;
$tcedulas4libranza =  $tcedulas4;
$trecaudo4libranza =  $trecaudo4;
$tcedulas5libranza =  $tcedulas5;
$trecaudo5libranza =  $trecaudo5;
$tcedulas6libranza =  $tcedulas6;
$trecaudo6libranza =  $trecaudo6;
$tcedulas7libranza =  $tcedulas7;
$trecaudo7libranza =  $trecaudo7;
$tcedulas8libranza =  $tcedulas8;
$trecaudo8libranza =  $trecaudo8;
$tcedulas9libranza =  $tcedulas9;
$trecaudo9libranza =  $trecaudo9;
$tcedulas10libranza =  $tcedulas10;
$trecaudo10libranza =  $trecaudo10;
$tcedulas11libranza =  $tcedulas11;
$trecaudo11libranza =  $trecaudo11;
$tcedulas12libranza =  $tcedulas12;
$trecaudo12libranza =  $trecaudo12;
$tcedulas13libranza =  $tcedulas13;
$trecaudo13libranza =  $trecaudo13;
$tcedulas14libranza =  $tcedulas14;
$trecaudo14libranza =  $trecaudo14;
$tcedulas15libranza =  $tcedulas15;
$trecaudo15libranza =  $trecaudo15;
$tcedulas16libranza =  $tcedulas16;
$trecaudo16libranza =  $trecaudo16;
$tcedulas17libranza =  $tcedulas17;
$trecaudo17libranza =  $trecaudo17;
$tcedulas18libranza =  $tcedulas18;
$trecaudo18libranza =  $trecaudo18;
$tcedulas19libranza =  $tcedulas19;
$trecaudo19libranza =  $trecaudo19;
$tcedulas20libranza =  $tcedulas20;
$trecaudo20libranza =  $trecaudo20;
$tcedulas21libranza =  $tcedulas21;
$trecaudo21libranza =  $trecaudo21;
$tcedulas22libranza =  $tcedulas22;
$trecaudo22libranza =  $trecaudo22;
$tcedulas23libranza =  $tcedulas23;
$trecaudo23libranza =  $trecaudo23;
$tcedulas24libranza =  $tcedulas24;
$trecaudo24libranza =  $trecaudo24;
$tcedulas25libranza =  $tcedulas25;
$trecaudo25libranza =  $trecaudo25;
$tcedulas26libranza =  $tcedulas26;
$trecaudo26libranza =  $trecaudo26;
$tcedulas27libranza =  $tcedulas27;
$trecaudo27libranza =  $trecaudo27;
$tcedulas28libranza =  $tcedulas28;
$trecaudo28libranza =  $trecaudo28;
$tcedulas29libranza =  $tcedulas29;
$trecaudo29libranza =  $trecaudo29;
$tcedulas30libranza =  $tcedulas30;
$trecaudo30libranza =  $trecaudo30;
$tcedulas31libranza =  $tcedulas31;
$trecaudo31libranza =  $trecaudo31;
$tcedulaslibranza= $tcedulas;
$tvalorlibranza = $tvlor;
$tcedurealizadolibranza = $tcedurealizado;
$tvalorrealizadolibranza = $tvalorrealizado; 



}
}
	$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':BQ'.$i)->applyFromArray($styleArrayTwo);
	$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('A'.$i, 'TOTAL')
    	->setCellValue('B'.$i, $tcedulas)
		->setCellValue('C'.$i, $tvalor)
		->setCellValue('D'.$i, $tcedurealizado)
		->setCellValue('E'.$i, $tvalorrealizado)
		->setCellValue('F'.$i, round($tporcencedulas,1).' %')
		->setCellValue('G'.$i, round($tporcenvalor,1).' %')
		->setCellValue('H'.$i, $tcedulas1)
		->setCellValue('I'.$i, $trecaudo1)
		->setCellValue('J'.$i, $tcedulas2)
		->setCellValue('K'.$i, $trecaudo2)	
		->setCellValue('L'.$i, $tcedulas3)
		->setCellValue('M'.$i, $trecaudo3)	
		->setCellValue('N'.$i, $tcedulas4)
		->setCellValue('O'.$i, $trecaudo4)	
		->setCellValue('P'.$i, $tcedulas5)
		->setCellValue('Q'.$i, $trecaudo5)	
		->setCellValue('R'.$i, $tcedulas6)
		->setCellValue('S'.$i, $trecaudo6)	
		->setCellValue('T'.$i, $tcedulas7)
		->setCellValue('U'.$i, $trecaudo7)	
		->setCellValue('V'.$i, $tcedulas8)
		->setCellValue('W'.$i, $trecaudo8)	
		->setCellValue('X'.$i, $tcedulas9)
		->setCellValue('Y'.$i, $trecaudo9)	
		->setCellValue('Z'.$i, $tcedulas10)
		->setCellValue('AA'.$i, $trecaudo10)	
		->setCellValue('AB'.$i, $tcedulas11)
		->setCellValue('AC'.$i, $trecaudo11)	
		->setCellValue('AD'.$i, $tcedulas12)
		->setCellValue('AE'.$i, $trecaudo12)	
		->setCellValue('AF'.$i, $tcedulas13)
		->setCellValue('AG'.$i, $trecaudo13)	
		->setCellValue('AH'.$i, $tcedulas14)
		->setCellValue('AI'.$i, $trecaudo14)	
		->setCellValue('AJ'.$i, $tcedulas15)
		->setCellValue('AK'.$i, $trecaudo15)	
		->setCellValue('AL'.$i, $tcedulas16)
		->setCellValue('AM'.$i, $trecaudo16)	
		->setCellValue('AN'.$i, $tcedulas17)
		->setCellValue('AO'.$i, $trecaudo17)	
		->setCellValue('AP'.$i, $tcedulas18)
		->setCellValue('AQ'.$i, $trecaudo18)	
		->setCellValue('AR'.$i, $tcedulas19)
		->setCellValue('AS'.$i, $trecaudo19)	
		->setCellValue('AT'.$i, $tcedulas20)
		->setCellValue('AU'.$i, $trecaudo20)	
		->setCellValue('AV'.$i, $tcedulas21)
		->setCellValue('AW'.$i, $trecaudo21)	
		->setCellValue('AX'.$i, $tcedulas22)
		->setCellValue('AY'.$i, $trecaudo22)	
		->setCellValue('AZ'.$i, $tcedulas23)
		->setCellValue('BA'.$i, $trecaudo23)	
		->setCellValue('BB'.$i, $tcedulas24)
		->setCellValue('BC'.$i, $trecaudo24)	
		->setCellValue('BD'.$i, $tcedulas25)
		->setCellValue('BE'.$i, $trecaudo25)	
		->setCellValue('BF'.$i, $tcedulas26)
		->setCellValue('BG'.$i, $trecaudo26)	
		->setCellValue('BH'.$i, $tcedulas27)
		->setCellValue('BI'.$i, $trecaudo27)	
		->setCellValue('BJ'.$i, $tcedulas28)
		->setCellValue('BK'.$i, $trecaudo28)	
		->setCellValue('BL'.$i, $tcedulas29)
		->setCellValue('BM'.$i, $trecaudo29)	
		->setCellValue('BN'.$i, $tcedulas30)
		->setCellValue('BO'.$i, $trecaudo30)	
		->setCellValue('BP'.$i, $tcedulas31)
		->setCellValue('BQ'.$i, $trecaudo31);	
	$i++;	

/* ---------------------------------------------------------------------------------------- */

$trecaudo1final = $trecaudo1cllcenter + $trecaudo1conciliacion + $trecaudo1mora31 + $trecaudo1moraalta + $trecaudo1contados + $trecaudo1oficina + $trecaudo1castigada + $trecaudo1libranza;

$trecaudo2final = $trecaudo2cllcenter + $trecaudo2conciliacion + $trecaudo2mora31 + $trecaudo2moraalta + $trecaudo2contados + $trecaudo2oficina + $trecaudo2castigada + $trecaudo2libranza;

$trecaudo3final = $trecaudo3cllcenter + $trecaudo3conciliacion + $trecaudo3mora31 + $trecaudo3moraalta + $trecaudo3contados + $trecaudo3oficina + $trecaudo3castigada + $trecaudo3libranza;

$trecaudo4final = $trecaudo4cllcenter + $trecaudo4conciliacion + $trecaudo4mora31 + $trecaudo4moraalta + $trecaudo4contados + $trecaudo4oficina + $trecaudo4castigada + $trecaudo4libranza;

$trecaudo5final = $trecaudo5cllcenter + $trecaudo5conciliacion + $trecaudo5mora31 + $trecaudo5moraalta + $trecaudo5contados + $trecaudo5oficina + $trecaudo5castigada + $trecaudo5libranza;

$trecaudo6final = $trecaudo6cllcenter + $trecaudo6conciliacion + $trecaudo6mora31 + $trecaudo6moraalta + $trecaudo6contados + $trecaudo6oficina + $trecaudo6castigada + $trecaudo6libranza;

$trecaudo7final = $trecaudo7cllcenter + $trecaudo7conciliacion + $trecaudo7mora31 + $trecaudo7moraalta + $trecaudo7contados + $trecaudo7oficina + $trecaudo7castigada + $trecaudo7libranza;

$trecaudo8final = $trecaudo8cllcenter + $trecaudo8conciliacion + $trecaudo8mora31 + $trecaudo8moraalta + $trecaudo8contados + $trecaudo8oficina + $trecaudo8castigada + $trecaudo8libranza;

$trecaudo9final = $trecaudo9cllcenter + $trecaudo9conciliacion + $trecaudo9mora31 + $trecaudo9moraalta + $trecaudo9contados + $trecaudo9oficina + $trecaudo9castigada + $trecaudo9libranza;

$trecaudo10final = $trecaudo10cllcenter + $trecaudo10conciliacion + $trecaudo10mora31 + $trecaudo10moraalta + $trecaudo10contados + $trecaudo10oficina + $trecaudo10castigada + $trecaudo10libranza;

$trecaudo11final = $trecaudo11cllcenter + $trecaudo11conciliacion + $trecaudo11mora31 + $trecaudo11moraalta + $trecaudo11contados + $trecaudo11oficina + $trecaudo11castigada + $trecaudo11libranza;

$trecaudo12final = $trecaudo12cllcenter + $trecaudo12conciliacion + $trecaudo12mora31 + $trecaudo12moraalta + $trecaudo12contados + $trecaudo12oficina + $trecaudo12castigada + $trecaudo12libranza;

$trecaudo13final = $trecaudo13cllcenter + $trecaudo13conciliacion + $trecaudo13mora31 + $trecaudo13moraalta + $trecaudo13contados + $trecaudo13oficina + $trecaudo13castigada + $trecaudo13libranza;

$trecaudo14final = $trecaudo14cllcenter + $trecaudo14conciliacion + $trecaudo14mora31 + $trecaudo14moraalta + $trecaudo14contados + $trecaudo14oficina + $trecaudo14castigada + $trecaudo14libranza;

$trecaudo15final = $trecaudo15cllcenter + $trecaudo15conciliacion + $trecaudo15mora31 + $trecaudo15moraalta + $trecaudo15contados + $trecaudo15oficina + $trecaudo15castigada + $trecaudo15libranza;

$trecaudo16final = $trecaudo16cllcenter + $trecaudo16conciliacion + $trecaudo16mora31 + $trecaudo16moraalta + $trecaudo16contados + $trecaudo16oficina + $trecaudo16castigada + $trecaudo16libranza;

$trecaudo17final = $trecaudo17cllcenter + $trecaudo17conciliacion + $trecaudo17mora31 + $trecaudo17moraalta + $trecaudo17contados + $trecaudo17oficina + $trecaudo17castigada + $trecaudo17libranza;

$trecaudo18final = $trecaudo18cllcenter + $trecaudo18conciliacion + $trecaudo18mora31 + $trecaudo18moraalta + $trecaudo18contados + $trecaudo18oficina + $trecaudo18castigada + $trecaudo18libranza;

$trecaudo19final = $trecaudo19cllcenter + $trecaudo19conciliacion + $trecaudo19mora31 + $trecaudo19moraalta + $trecaudo19contados + $trecaudo19oficina + $trecaudo19castigada + $trecaudo19libranza;

$trecaudo20final = $trecaudo20cllcenter + $trecaudo20conciliacion + $trecaudo20mora31 + $trecaudo20moraalta + $trecaudo20contados + $trecaudo20oficina + $trecaudo20castigada + $trecaudo20libranza;

$trecaudo21final = $trecaudo21cllcenter + $trecaudo21conciliacion + $trecaudo21mora31 + $trecaudo21moraalta + $trecaudo21contados + $trecaudo21oficina + $trecaudo21castigada + $trecaudo21libranza;

$trecaudo22final = $trecaudo22cllcenter + $trecaudo22conciliacion + $trecaudo22mora31 + $trecaudo22moraalta + $trecaudo22contados + $trecaudo22oficina + $trecaudo22castigada + $trecaudo22libranza;

$trecaudo23final = $trecaudo23cllcenter + $trecaudo23conciliacion + $trecaudo23mora31 + $trecaudo23moraalta + $trecaudo23contados + $trecaudo23oficina + $trecaudo23castigada + $trecaudo23libranza;

$trecaudo24final = $trecaudo24cllcenter + $trecaudo24conciliacion + $trecaudo24mora31 + $trecaudo24moraalta + $trecaudo24contados + $trecaudo24oficina + $trecaudo24castigada + $trecaudo24libranza;

$trecaudo25final = $trecaudo25cllcenter + $trecaudo25conciliacion + $trecaudo25mora31 + $trecaudo25moraalta + $trecaudo25contados + $trecaudo25oficina + $trecaudo25castigada + $trecaudo25libranza;

$trecaudo26final = $trecaudo26cllcenter + $trecaudo26conciliacion + $trecaudo26mora31 + $trecaudo26moraalta + $trecaudo26contados + $trecaudo26oficina + $trecaudo26castigada + $trecaudo26libranza;

$trecaudo27final = $trecaudo27cllcenter + $trecaudo27conciliacion + $trecaudo27mora31 + $trecaudo27moraalta + $trecaudo27contados + $trecaudo27oficina + $trecaudo27castigada + $trecaudo27libranza;

$trecaudo28final = $trecaudo28cllcenter + $trecaudo28conciliacion + $trecaudo28mora31 + $trecaudo28moraalta + $trecaudo28contados + $trecaudo28oficina + $trecaudo28castigada + $trecaudo28libranza;

$trecaudo29final = $trecaudo29cllcenter + $trecaudo29conciliacion + $trecaudo29mora31 + $trecaudo29moraalta + $trecaudo29contados + $trecaudo29oficina + $trecaudo29castigada + $trecaudo29libranza;

$trecaudo30final = $trecaudo30cllcenter + $trecaudo30conciliacion + $trecaudo30mora31 + $trecaudo30moraalta + $trecaudo30contados + $trecaudo30oficina + $trecaudo30castigada + $trecaudo30libranza;

$trecaudo31final = $trecaudo31cllcenter + $trecaudo31conciliacion + $trecaudo31mora31 + $trecaudo31moraalta + $trecaudo31contados + $trecaudo31oficina + $trecaudo31castigada + $trecaudo31libranza;


$tcedulas1final = $tcedulas1cllcenter + $tcedulas1conciliacion + $tcedulas1mora31 + $tcedulas1moraalta + $tcedulas1contados + $tcedulas1oficina + $tcedulas1castigada + $tcedulas1libranza;

$tcedulas2final = $tcedulas2cllcenter + $tcedulas2conciliacion + $tcedulas2mora31 + $tcedulas2moraalta + $tcedulas2contados + $tcedulas2oficina + $tcedulas2castigada + $tcedulas2libranza;

$tcedulas3final = $tcedulas3cllcenter + $tcedulas3conciliacion + $tcedulas3mora31 + $tcedulas3moraalta + $tcedulas3contados + $tcedulas3oficina + $tcedulas3castigada + $tcedulas3libranza;

$tcedulas4final = $tcedulas4cllcenter + $tcedulas4conciliacion + $tcedulas4mora31 + $tcedulas4moraalta + $tcedulas4contados + $tcedulas4oficina + $tcedulas4castigada + $tcedulas4libranza;

$tcedulas5final = $tcedulas5cllcenter + $tcedulas5conciliacion + $tcedulas5mora31 + $tcedulas5moraalta + $tcedulas5contados + $tcedulas5oficina + $tcedulas5castigada + $tcedulas5libranza;

$tcedulas6final = $tcedulas6cllcenter + $tcedulas6conciliacion + $tcedulas6mora31 + $tcedulas6moraalta + $tcedulas6contados + $tcedulas6oficina + $tcedulas6castigada + $tcedulas6libranza;

$tcedulas7final = $tcedulas7cllcenter + $tcedulas7conciliacion + $tcedulas7mora31 + $tcedulas7moraalta + $tcedulas7contados + $tcedulas7oficina + $tcedulas7castigada + $tcedulas7libranza;

$tcedulas8final = $tcedulas8cllcenter + $tcedulas8conciliacion + $tcedulas8mora31 + $tcedulas8moraalta + $tcedulas8contados + $tcedulas8oficina + $tcedulas8castigada + $tcedulas8libranza;

$tcedulas9final = $tcedulas9cllcenter + $tcedulas9conciliacion + $tcedulas9mora31 + $tcedulas9moraalta + $tcedulas9contados + $tcedulas9oficina + $tcedulas9castigada + $tcedulas9libranza;

$tcedulas10final = $tcedulas10cllcenter + $tcedulas10conciliacion + $tcedulas10mora31 + $tcedulas10moraalta + $tcedulas10contados + $tcedulas10oficina + $tcedulas10castigada + $tcedulas10libranza;

$tcedulas11final = $tcedulas11cllcenter + $tcedulas11conciliacion + $tcedulas11mora31 + $tcedulas11moraalta + $tcedulas11contados + $tcedulas11oficina + $tcedulas11castigada + $tcedulas11libranza;

$tcedulas12final = $tcedulas12cllcenter + $tcedulas12conciliacion + $tcedulas12mora31 + $tcedulas12moraalta + $tcedulas12contados + $tcedulas12oficina + $tcedulas12castigada + $tcedulas12libranza;

$tcedulas13final = $tcedulas13cllcenter + $tcedulas13conciliacion + $tcedulas13mora31 + $tcedulas13moraalta + $tcedulas13contados + $tcedulas13oficina + $tcedulas13castigada + $tcedulas13libranza;

$tcedulas14final = $tcedulas14cllcenter + $tcedulas14conciliacion + $tcedulas14mora31 + $tcedulas14moraalta + $tcedulas14contados + $tcedulas14oficina + $tcedulas14castigada + $tcedulas14libranza;

$tcedulas15final = $tcedulas15cllcenter + $tcedulas15conciliacion + $tcedulas15mora31 + $tcedulas15moraalta + $tcedulas15contados + $tcedulas15oficina + $tcedulas15castigada + $tcedulas15libranza;

$tcedulas16final = $tcedulas16cllcenter + $tcedulas16conciliacion + $tcedulas16mora31 + $tcedulas16moraalta + $tcedulas16contados + $tcedulas16oficina + $tcedulas16castigada + $tcedulas16libranza;

$tcedulas17final = $tcedulas17cllcenter + $tcedulas17conciliacion + $tcedulas17mora31 + $tcedulas17moraalta + $tcedulas17contados + $tcedulas17oficina + $tcedulas17castigada + $tcedulas17libranza;

$tcedulas18final = $tcedulas18cllcenter + $tcedulas18conciliacion + $tcedulas18mora31 + $tcedulas18moraalta + $tcedulas18contados + $tcedulas18oficina + $tcedulas18castigada + $tcedulas18libranza;

$tcedulas19final = $tcedulas19cllcenter + $tcedulas19conciliacion + $tcedulas19mora31 + $tcedulas19moraalta + $tcedulas19contados + $tcedulas19oficina + $tcedulas19castigada + $tcedulas19libranza;

$tcedulas20final = $tcedulas20cllcenter + $tcedulas20conciliacion + $tcedulas20mora31 + $tcedulas20moraalta + $tcedulas20contados + $tcedulas20oficina + $tcedulas20castigada + $tcedulas20libranza;

$tcedulas21final = $tcedulas21cllcenter + $tcedulas21conciliacion + $tcedulas21mora31 + $tcedulas21moraalta + $tcedulas21contados + $tcedulas21oficina + $tcedulas21castigada + $tcedulas21libranza;

$tcedulas22final = $tcedulas22cllcenter + $tcedulas22conciliacion + $tcedulas22mora31 + $tcedulas22moraalta + $tcedulas22contados + $tcedulas22oficina + $tcedulas22castigada + $tcedulas22libranza;

$tcedulas23final = $tcedulas23cllcenter + $tcedulas23conciliacion + $tcedulas23mora31 + $tcedulas23moraalta + $tcedulas23contados + $tcedulas23oficina + $tcedulas23castigada + $tcedulas23libranza;

$tcedulas24final = $tcedulas24cllcenter + $tcedulas24conciliacion + $tcedulas24mora31 + $tcedulas24moraalta + $tcedulas24contados + $tcedulas24oficina + $tcedulas24castigada + $tcedulas24libranza;

$tcedulas25final = $tcedulas25cllcenter + $tcedulas25conciliacion + $tcedulas25mora31 + $tcedulas25moraalta + $tcedulas25contados + $tcedulas25oficina + $tcedulas25castigada + $tcedulas25libranza;

$tcedulas26final = $tcedulas26cllcenter + $tcedulas26conciliacion + $tcedulas26mora31 + $tcedulas26moraalta + $tcedulas26contados + $tcedulas26oficina + $tcedulas26castigada + $tcedulas26libranza;

$tcedulas27final = $tcedulas27cllcenter + $tcedulas27conciliacion + $tcedulas27mora31 + $tcedulas27moraalta + $tcedulas27contados + $tcedulas27oficina + $tcedulas27castigada + $tcedulas27libranza;

$tcedulas28final = $tcedulas28cllcenter + $tcedulas28conciliacion + $tcedulas28mora31 + $tcedulas28moraalta + $tcedulas28contados + $tcedulas28oficina + $tcedulas28castigada + $tcedulas28libranza;

$tcedulas29final = $tcedulas29cllcenter + $tcedulas29conciliacion + $tcedulas29mora31 + $tcedulas29moraalta + $tcedulas29contados + $tcedulas29oficina + $tcedulas29castigada + $tcedulas29libranza;

$tcedulas30final = $tcedulas30cllcenter + $tcedulas30conciliacion + $tcedulas30mora31 + $tcedulas30moraalta + $tcedulas30contados + $tcedulas30oficina + $tcedulas30castigada + $tcedulas30libranza;

$tcedulas31final = $tcedulas31cllcenter + $tcedulas31conciliacion + $tcedulas31mora31 + $tcedulas31moraalta + $tcedulas31contados + $tcedulas31oficina + $tcedulas31castigada + $tcedulas31libranza;




$tcedulasfinal = $tcedulascllcenter + $tcedulasconciliacion + $tcedulasmora31 + $tcedulasmoraalta + $tcedulascontados + $tcedulasoficina + $tcedulascastigada + $tcedulaslibranza;

$tvalorfinal = $tvalorcllcenter + $tvalorconciliacion + $tvalormora31 + $tvalormoraalta + $tvalorcontados + $tvaloroficina + $tvalorcastigada + $tvalorlibranza;

$tcedurealizadofinal = $tcedurealizadocllcenter + $tcedurealizadoconciliacion + $tcedurealizadomora31 + $tcedurealizadomoraalta + $tcedurealizadocontados + $tcedurealizadooficina + $tcedurealizadocastigada + $tcedurealizadolibranza;

$tvalorrealizadofinal = $tvalorrealizadocllcenter + $tvalorrealizadoconciliacion + $tvalorrealizadomora31 + $tvalorrealizadomoraalta + $tvalorrealizadocontados + $tvalorrealizadooficina + $tvalorrealizadocastigada + $tvalorrealizadolibranza;



	$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':BQ'.$i)->applyFromArray($styleArrayThree);
	$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('A'.$i, 'TOTAL FINAL')
    	->setCellValue('B'.$i, $tcedulasfinal)
		->setCellValue('C'.$i, $tvalorfinal)
		->setCellValue('D'.$i, $tcedurealizadofinal)
		->setCellValue('E'.$i, $tvalorrealizadofinal)
		->setCellValue('F'.$i, round($tporcencedulasfinal,1).' %')
		->setCellValue('G'.$i, round($tporcenvalorfinal,1).' %')
		->setCellValue('H'.$i, $tcedulas1final)
		->setCellValue('I'.$i, $trecaudo1final)
		->setCellValue('J'.$i, $tcedulas2final)
		->setCellValue('K'.$i, $trecaudo2final)	
		->setCellValue('L'.$i, $tcedulas3final)
		->setCellValue('M'.$i, $trecaudo3final)	
		->setCellValue('N'.$i, $tcedulas4final)
		->setCellValue('O'.$i, $trecaudo4final)	
		->setCellValue('P'.$i, $tcedulas5final)
		->setCellValue('Q'.$i, $trecaudo5final)	
		->setCellValue('R'.$i, $tcedulas6final)
		->setCellValue('S'.$i, $trecaudo6final)	
		->setCellValue('T'.$i, $tcedulas7final)
		->setCellValue('U'.$i, $trecaudo7final)	
		->setCellValue('V'.$i, $tcedulas8final)
		->setCellValue('W'.$i, $trecaudo8final)	
		->setCellValue('X'.$i, $tcedulas9final)
		->setCellValue('Y'.$i, $trecaudo9final)	
		->setCellValue('Z'.$i, $tcedulas10final)
		->setCellValue('AA'.$i, $trecaudo10final)	
		->setCellValue('AB'.$i, $tcedulas11final)
		->setCellValue('AC'.$i, $trecaudo11final)	
		->setCellValue('AD'.$i, $tcedulas12final)
		->setCellValue('AE'.$i, $trecaudo12final)	
		->setCellValue('AF'.$i, $tcedulas13final)
		->setCellValue('AG'.$i, $trecaudo13final)	
		->setCellValue('AH'.$i, $tcedulas14final)
		->setCellValue('AI'.$i, $trecaudo14final)	
		->setCellValue('AJ'.$i, $tcedulas15final)
		->setCellValue('AK'.$i, $trecaudo15final)	
		->setCellValue('AL'.$i, $tcedulas16final)
		->setCellValue('AM'.$i, $trecaudo16final)	
		->setCellValue('AN'.$i, $tcedulas17final)
		->setCellValue('AO'.$i, $trecaudo17final)	
		->setCellValue('AP'.$i, $tcedulas18final)
		->setCellValue('AQ'.$i, $trecaudo18final)	
		->setCellValue('AR'.$i, $tcedulas19final)
		->setCellValue('AS'.$i, $trecaudo19final)	
		->setCellValue('AT'.$i, $tcedulas20final)
		->setCellValue('AU'.$i, $trecaudo20final)	
		->setCellValue('AV'.$i, $tcedulas21final)
		->setCellValue('AW'.$i, $trecaudo21final)	
		->setCellValue('AX'.$i, $tcedulas22final)
		->setCellValue('AY'.$i, $trecaudo22final)	
		->setCellValue('AZ'.$i, $tcedulas23final)
		->setCellValue('BA'.$i, $trecaudo23final)	
		->setCellValue('BB'.$i, $tcedulas24final)
		->setCellValue('BC'.$i, $trecaudo24final)	
		->setCellValue('BD'.$i, $tcedulas25final)
		->setCellValue('BE'.$i, $trecaudo25final)	
		->setCellValue('BF'.$i, $tcedulas26final)
		->setCellValue('BG'.$i, $trecaudo26final)	
		->setCellValue('BH'.$i, $tcedulas27final)
		->setCellValue('BI'.$i, $trecaudo27final)	
		->setCellValue('BJ'.$i, $tcedulas28final)
		->setCellValue('BK'.$i, $trecaudo28final)	
		->setCellValue('BL'.$i, $tcedulas29final)
		->setCellValue('BM'.$i, $trecaudo29final)	
		->setCellValue('BN'.$i, $tcedulas30final)
		->setCellValue('BO'.$i, $trecaudo30final)	
		->setCellValue('BP'.$i, $tcedulas31final)
		->setCellValue('BQ'.$i, $trecaudo31final);	
	$i++;		
	
	
// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Como Vamos '.$fecha.'.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
?>