<?php
$r = '../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
?>
<!doctype html>
<html>
<head>
	<?php 
		require($r . 'incluir/src/head.php');
		require($r . 'incluir/src/head-form.php');
	?>
<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#form').validationEngine({
		showOneMessage:true,
		onValidationComplete: function(form, status){
			if(status == true) {
				carga();
				return true;
			}
		}
	});
	$('.btnconsulta').button({ icons: { primary: 'ui-icon ui-icon-search' }});
	$('#fecha').datepicker({
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		minDate: '+0D',	
	}).keypress(function(event) { event.preventDefault() });
});
</script>
<style type="text/css">
#form fieldset{ padding: 10px; display:block; width:400px; margin:20px auto }
#form legend{ font-weight: bold; margin-left:5px; padding:5px }
#form label { display:inline-block; width:50px; text-align:right; margin:0.3em 2% 0 0 }
#form p{ margin: 5px 0 }
</style>
</head>
<body>
<?php require($r.'incluir/src/login.php') ?>
<section id="principal">
<?php require($r.'incluir/src/cabeza.php') ?>
<?php require($r.'incluir/src/menu.php') ?>
<article id="cuerpo">
<article class="mapa">
<a href="#">Principal</a><div class="mapa_div"></div><a href="#">Cartera</a><div class="mapa_div"></div><a class="current">Reportes</a>
</article>
<article id="contenido">
<form id="form" name="form" action="lisprevisto.php" method="post">
<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
<legend class="ui-widget ui-widget-header ui-corner-all">Reporte previsto de cartera</legend>
<p>
<label for="promotor">Promotor:</label>
<select name="promotor" class="nombre validate[required] text-input">
<option value="">SELECCIONE</option>
<?php
$qry = $db->query("SELECT * FROM usuarios WHERE usupromotor = '1' ORDER BY usunombre");
while($row = $qry->fetch(PDO::FETCH_ASSOC))
    echo '<option value='.$row['usuid'].'>'.$row['usunombre'].'</option>';
?>
</select>
</p>
<p>
<label for="fecha">Fecha:</label>
<input type="text" id="fecha" name="fecha" class="fecha validate[required]" />
</p>
<p class="boton">
<button type="submit" class="btnconsulta">consultar</button>
</p>
</article>
</article>
<?php require($r.'incluir/src/pie.php') ?>
</section>
</body>
</html>