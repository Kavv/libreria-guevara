<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

$empresa = $_POST['empresa'];
$fecha1 = $_POST['fecha1'];
$fecha2 = $_POST['fecha2'];
$filtro = 'empresa=' . $empresa . '&fecha1=' . $fecha1 . '&fecha2=' . $fecha2;

// No se estan usando
$cartotal = 0;
$carcapital = 0;


$tlcarterafn = 0;
$numtlcarterafn = 0;
$carcastigada = 0;
$numcarcastigada = 0;
$venbrutageneral = 0;
$numvenbrutageneral = 0;
$credito = 0;
$numcredito = 0;
$contado = 0;
$numcontado = 0;
$finantotal = 0;
$numfinantotal = 0;
$ctaini = 0;
$numctaini = 0;
$totalcapital = 0;
$numtotalcapital = 0;
$carfinanciacionmul = 0;
$castigadapagada = 0;
$numcastigadapagada = 0;
$descapital = 0;
$numdescapital = 0;
$desfinanciacionmul = 0;
$ndebito = 0;
$numndebito = 0;
$ncredito = 0;
$numncredito = 0;
$devoluciones = 0;
$numdevoluciones = 0;

if ($empresa != 'TODAS') {

	$qry = $db->query("SELECT * FROM empresas WHERE empid = '$empresa'"); // row empresa


	/*AGREGADO POR MARIA DIAZ PARA DETERMINAR SI EL MES YA FUE CERRADO Y CON CUAL VALOR*/
	$anno = substr($fecha2, 0, 4);
	$mes = substr($fecha2, 5, 2);

	$qrysaldo = $db->query("SELECT * FROM saldocartera WHERE saldocarteramesano BETWEEN '" . $fecha1 . "' AND '" . $fecha2 . "' AND saldocarteraempresa = '$empresa'");
	$saldoAnterior = $qrysaldo->rowCount();

	$aux = $anno . "-" . $mes . "-01";
	$fechacorte = new DateTime($aux);
	$qrysaldocartera = $db->query("SELECT 
	sum(saldocarteravalor) as saldocarteravalor, 
	sum(saldocarteranumclientes) as saldocarteranumclientes, 
	sum(saldocarteracastigada) as saldocarteracastigada, 
	sum(saldocarteranumcastigada) as saldocarteranumcastigada 
	FROM saldocartera 
	WHERE saldocarteraempresa = '" . $empresa . "' AND saldocarteramesano = '" . $fechacorte->format('Y-m-d') . "'");
	$rowsaldocartera = $qrysaldocartera->fetch(PDO::FETCH_ASSOC);
} else {/* ESTAS CONSULTAS SON PARA LA SELECCION DE TODAS LAS EMPRESAS*/

	/*AGREGADO POR MARIA DIAZ PARA DETERMINAR SI EL MES YA FUE CERRADO Y CON CUAL VALOR*/
	$anno = substr($fecha2, 0, 4);
	$mes = substr($fecha2, 5, 2);

	$qrysaldo = $db->query("SELECT 
sum(saldocarteravalor) as saldocarteravalor, sum(saldocarteranumclientes) as saldocarteranumclientes, 
sum(saldoventasbrutas) as saldoventasbrutas, sum(saldoventasnumbrutas) as saldoventasnumbrutas,
sum(saldoventascredito) as saldoventascredito, sum(saldoventasnumcredito) as saldoventasnumcredito,
sum(saldoventascontado) as saldoventascontado, sum(saldoventasnumcontado) as saldoventasnumcontado,
sum(saldofinanciado) as saldofinanciado, sum(saldonumfinanciado) as saldonumfinanciado,
sum(saldocuotasiniciales) as saldocuotasiniciales, sum(saldonumcuotasiniciales) as saldonumcuotasiniciales,
sum(saldocarteracapital) as saldocarteracapital, sum(saldonumcarteracapital) as saldonumcarteracapital,
sum(saldocarterafinanciacion) as saldocarterafinanciacion,
sum(saldocastigadapagada) as saldocastigadapagada, sum(saldonumcastigadapagada) as saldonumcastigadapagada,
sum(saldodescuentoscapital) as saldodescuentoscapital, sum(saldonumdescuentoscapital) as saldonumdescuentoscapital,
sum(saldodescuentosfinanciacion) as saldodescuentosfinanciacion,
sum(saldonotasdebito) as saldonotasdebito, sum(saldonumnotasdebito) as saldonumnotasdebito,
sum(saldonotascredito) as saldonotascredito, sum(saldonumnotascredito) as saldonumnotascredito,
sum(saldodevoluciones) as saldodevoluciones, sum(saldonumdevoluciones) as saldonumdevoluciones
FROM saldocartera WHERE saldocarteramesano BETWEEN '" . $fecha1 . "' AND '" . $fecha2 . "'");
	$saldoAnterior = $qrysaldo->rowCount();


	$aux = $anno . "-" . $mes . "-01";
	$fechacorte = new DateTime($aux);
	$qrysaldocartera = $db->query("SELECT sum(saldocarteravalor) as saldocarteravalor, sum(saldocarteranumclientes) as saldocarteranumclientes, sum(saldocarteracastigada) as saldocarteracastigada, sum(saldocarteranumcastigada) as saldocarteranumcastigada FROM saldocartera WHERE saldocarteramesano = '" . $fechacorte->format('Y-m-d') . "'");
	$rowsaldocartera = $qrysaldocartera->fetch(PDO::FETCH_ASSOC);
}


if ($saldoAnterior > 0) {
	while ($rowsaldo = $qrysaldo->fetch(PDO::FETCH_ASSOC)) {
		$tlcarterafn = $rowsaldocartera['saldocarteravalor'];
		$numtlcarterafn = $numtlcarterafn + $rowsaldo['saldocarteranumclientes'];

		$carcastigada = $rowsaldocartera['saldocarteracastigada'];
		$numcarcastigada = $rowsaldocartera['saldocarteranumcastigada'];

		$venbrutageneral = $venbrutageneral + $rowsaldo['saldoventasbrutas'];
		$numvenbrutageneral = $numvenbrutageneral + $rowsaldo['saldoventasnumbrutas'];

		$credito = $credito + $rowsaldo['saldoventascredito'];
		$numcredito = $numcredito + $rowsaldo['saldoventasnumcredito'];

		$contado = $contado + $rowsaldo['saldoventascontado'];
		$numcontado = $numcontado + $rowsaldo['saldoventasnumcontado'];

		$finantotal = $finantotal + $rowsaldo['saldofinanciado'];
		$numfinantotal = $numfinantotal + $rowsaldo['saldonumfinanciado'];

		$ctaini = $ctaini + $rowsaldo['saldocuotasiniciales'];
		$numctaini = $numctaini + $rowsaldo['saldonumcuotasiniciales'];

		$totalcapital = $totalcapital + $rowsaldo['saldocarteracapital'];
		$numtotalcapital = $numtotalcapital + $rowsaldo['saldonumcarteracapital'];

		$carfinanciacionmul = $carfinanciacionmul + $rowsaldo['saldocarterafinanciacion'];

		$castigadapagada = $castigadapagada + $rowsaldo['saldocastigadapagada'];
		$numcastigadapagada = $numcastigadapagada + $rowsaldo['saldonumcastigadapagada'];

		$descapital = $descapital + $rowsaldo['saldodescuentoscapital'];
		$numdescapital = $numdescapital + $rowsaldo['saldonumdescuentoscapital'];

		$desfinanciacionmul = $desfinanciacionmul + $rowsaldo['saldodescuentosfinanciacion'];

		$ndebito = $ndebito + $rowsaldo['saldonotasdebito'];
		$numndebito = $numndebito + $rowsaldo['saldonumnotasdebito'];

		$ncredito = $ncredito + $rowsaldo['saldonotascredito'];
		$numncredito = $numncredito + $rowsaldo['saldonumnotascredito'];

		$devoluciones = $devoluciones + $rowsaldo['saldodevoluciones'];
		$numdevoluciones = $numdevoluciones + $rowsaldo['saldonumdevoluciones'];
	}
}

$row7 = $db->query("SELECT * FROM empresas WHERE empid = '$empresa'")->fetch(PDO::FETCH_ASSOC); // row Nombre de la empresa
$rowpar = $db->query("SELECT * FROM parametros")->fetch(PDO::FETCH_ASSOC); // row de parametros

?>
<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>

	<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
	<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#tabla').dataTable({
				'bPaginate': false,
				'bLengthChange': false,
				'bFilter': false,
				'bSort': false,
				'bInfo': false,
				'bAutoWidth': false,
				'bJQueryUI': true
			});
		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Cartera</a>
				<div class="mapa_div"></div><a class="current">Reportes</a>
			</article>
			<article id="contenido">
				<h2>Reporte resumen de cartera de <?php echo $fecha1 . ' al ' . $fecha2;
													if ($empresa != 'TODAS') {
														echo ' de la empresa ' . $row7['empnombre'];
													} else {
														echo ' de todas las empresas ';
													} ?></h2>
				<div class="reporte">
					<a href="pdfresumenactual.php?<?php echo $filtro ?>">
						<img src="<?php echo $r ?>imagenes/iconos/pdf.png" title="pdf" />
					</a> 
				</div>
				<table id="tabla">
					<thead>
						<tr>
							<th>Detalle</th>
							<th>Valor</th>
							<th>No. Clientes</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>VR. Ventas brutas entre el intervalo: </td>
							<td align="right"><?php echo number_format($venbrutageneral, 2) ?></td>
							<td> <?php echo $numvenbrutageneral ?> </td>
						</tr>
						<tr>
							<td>VR. Ventas a credito entre el intervalo: </td>
							<td align="right"><?php echo number_format($credito, 2) ?></td>
							<td> <?php echo $numcredito ?> </td>
						</tr>
						<tr>
							<td>VR. Contados entre el intervalo: </td>
							<td align="right"><?php echo number_format($contado, 2) ?></td>
							<td><?php echo number_format($numcontado, 2) ?></td>
						</tr>
						<tr>
							<td>VR. Financiado entre el intervalo: </td>
							<td align="right"><?php echo number_format($finantotal, 2) ?></td>
							<td> <?php echo $numfinantotal ?> </td>
						</tr>
						<tr>
							<td>VR. Cuotas iniciales entre el intervalo: </td>
							<td align="right"><?php echo number_format($ctaini, 2) ?></td>
							<td><?php echo number_format($numctaini, 2) ?></td>
						</tr>
						<tr>
							<td>VR. Cartera total al <?php echo $fecha2 ?>: </td>
							<td align="right">
								<?php
								if (($tlcarterafn - $carcastigada) < 0) {
									echo "0";
								} else {
									echo number_format($tlcarterafn - $carcastigada, 2); // CAMBIO REALIZADO EL 08 DE ENERO
								}
								?></td>
							<td>
								<?php
								if (($tlcarterafn - $carcastigada) < 0) {
									echo "0";
								} else {
									echo number_format($numtlcarterafn - $numcarcastigada, 2); // CAMBIO REALIZADO EL 08 DE ENERO
								}
								?>
							</td>
						</tr>
						<?php

						?>
						<tr>
							<td>VR. Cartera pagada entre el intervalo (CAPITAL): </td>
							<td align="right"><?php echo number_format($totalcapital, 2) ?></td>
							<td> <?php echo $numtotalcapital ?> </td>
						</tr>
						<tr>
							<td>VR. Cartera pagada entre el intervalo (FINANCIACION): </td>
							<td align="right"><?php echo number_format($carfinanciacionmul, 2) ?></td>
							<td> </td>
						</tr>
						<?php
						?>
						<tr>
							<td>VR. Cartera Castigada pagada entre el intervalo: </td>
							<td align="right"><?php echo number_format($castigadapagada, 2) ?></td>
							<td><?php echo $numcastigadapagada ?></td>
						</tr>
						<tr>
							<td>VR. Descuentos entre el intervalo (CAPITAL): </td>
							<td align="right"><?php echo number_format($descapital, 2) ?></td>
							<td> <?php echo $numdescapital ?> </td>
						</tr>
						<tr>
							<td>VR. Descuentos entre el intervalo (FINANCIACION): </td>
							<td align="right"><?php echo number_format($desfinanciacionmul, 2) ?></td>
							<td> </td>
						</tr>
						<tr>
							<td>VR. Notas debito entre el intervalo: </td>
							<td align="right"><?php echo number_format($ndebito, 2) ?></td>
							<td> <?php echo $numndebito ?> </td>
						</tr>
						<tr>
							<td>VR. Notas credito entre el intervalo: </td>
							<td align="right"><?php echo number_format($ncredito, 2) ?></td>
							<td> <?php echo $numncredito ?> </td>
						</tr>
						<tr>
							<td>VR. Devoluciones entre el intervalo: </td>
							<td align="right"><?php echo number_format($devoluciones, 2) ?></td>
							<td> <?php echo $numdevoluciones ?> </td>
						</tr>
						<tr>
							<td>VR. Cartera castigada activa al <?php echo $fecha2 ?>: </td>
							<td align="right"><?php echo number_format($carcastigada, 2) ?></td>
							<td> <?php echo $numcarcastigada ?> </td>
						</tr>
					</tbody>
				</table>
				<p class="boton">
					<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href='conresumen.php'">atras</button>
				</p>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
</body>

</html>