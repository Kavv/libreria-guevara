<?php
$r = '../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
?>
<!doctype html>
<html>
<head>
	<?php 
		require($r . 'incluir/src/head.php');
		require($r . 'incluir/src/head-form.php');
	?>
<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#form').validationEngine({
		showOneMessage:true,
		onValidationComplete: function(form, status){
        	if(status) {               
            	carga();
            	return true;
			}
		}
	});
	$('#fecha').datepicker({
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		maxDate: '+0D',
		beforeShowDay: function(date) {
			var day = date.getDate(); 
			return [(day != 2 && day != 3 && day != 4 && day != 5 && day != 6 && day != 7 && day != 8 && day != 9 && day != 10 && day != 11 && day != 12 && day != 13 && day != 14 && day != 15 && day != 16 && day != 17 && day != 18 && day != 19 && day != 20 && day != 21 && day != 22 && day != 23 && day != 24 && day != 25 && day != 26 && day != 27 && day != 28 && day != 29 && day != 30 && day != 31), ''];
		}
		
	}).keypress(function(event) { event.preventDefault() });
	$('.btnsiguiente').button({ icons: { secondary: 'ui-icon ui-icon-arrowthick-1-e' }});
	$('#dialog-message').dialog({
		height: 80,
		width: 'auto',
		modal: true
	});
});
</script>
<style type="text/css">
#form form{ width:680px }
#form fieldset{ padding:10px; display:block; width:300px; margin:20px auto }
#form legend{ font-weight: bold; margin-left:5px; padding:5px }
#form label{ display:inline-block; width:140px; text-align:right; float:left; margin:0.3em 2% 0 0 }
#form p{ margin:5px 0 }
</style>
</head>
<body>
<?php require($r.'incluir/src/login.php') ?>
<section id="principal">
<?php require($r.'incluir/src/cabeza.php') ?>
<?php require($r.'incluir/src/menu.php') ?>
<article id="cuerpo">
<article class="mapa">
<a href="#">Principal</a><div class="mapa_div"></div><a class="current">Cartera</a>
</article>
<article id="contenido">
<form id="form" name="form" action="convamos2.php" method="get">
<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
<legend class="ui-widget ui-widget-header ui-corner-all">Reporte Como Vamos 1</legend>
<p>
<label for="fecha">Fecha de reporte:</label>
<input type="text" id="fecha" name="fecha" class="fecha" />
</p>
<br>
<p class="boton">
<button type="submit" class="btnsiguiente" name="siguiente" value="siguiente">Siguiente</button>
</p>
</fieldset>
</form>
</article>
</article>
<?php require($r.'incluir/src/pie.php') ?>
</section>
<?php
if($_GET['error']) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>'.$_GET['error'].'</div>';
?>
</body>
</html>