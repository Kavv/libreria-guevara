<?php
$r = '../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
require($r.'incluir/funciones.php');
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
	<?php 
		require($r . 'incluir/src/head.php');
		require($r . 'incluir/src/head-form.php');
	?>	

<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#tabla').dataTable({
		'bJQueryUI': true,
		'bStateSave': true,
		'bSort': false,
		'sPaginationType': 'full_numbers',
		'oLanguage': {
			'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
		},
	});
	$('.btnatras').button({ icons: { primary: 'ui-icon ui-icon-arrowthick-1-w' }});
});
</script>
</head>
<body>
<?php require($r.'incluir/src/login.php') ?>
<section id="principal">
<?php require($r.'incluir/src/cabeza.php') ?>
<?php require($r.'incluir/src/menu.php') ?>
<article id="cuerpo">
<article class="mapa">
<a href="#">Principal</a><div class="mapa_div"></div><a href="#">Cartera</a><div class="mapa_div"></div><a class="current">Reportes</a>
</article>
<article id="contenido">
<h2>Reporte resumen morosidad</h2>
<div class="reporte">
<a href="pdfresmoro.php?<?php echo $filtro ?>" onClick="carga()"><img src="<?php echo $r ?>imagenes/iconos/pdf.png" title="pdf" /></a> <a href="excelresmoro.php?<?php echo $filtro ?>"><img src="<?php echo $r ?>imagenes/iconos/excel.png" title="csv" /></a>
</div>
<table id="tabla">
<thead>
<tr>
<th>Promotor</th>
<th>cuentas</th>
<th>Valor</th>
<th>1 - 30</th>
<th>1 - 30</th>
<th>31 - 60</th>
<th>31 - 60</th>
<th>61 - 90</th>
<th>61 - 90</th>
<th>91 - 120</th>
<th>91 - 120</th>
<th>121 - Mas</th>
<th>121 - Mas</th>
<th>Total mora</th>
<th>Total mora</th>
<th>% Mora</th>	
<th>% Mora</th>	
</tr>
</thead>
<tbody>
<?php
$qry = $db->query("SELECT * FROM usuarios WHERE usupromotor = '1'");
while($row = $qry->fetch(PDO::FETCH_ASSOC)){
	$tlcartera = 0;
	$mora30 = 0;
	$ttl30 = 0;
	$mora60 = 0;
	$ttl60 = 0;
	$mora90 = 0;
	$ttl90 = 0;
	$mora120 = 0;
	$ttl120 = 0;
	$moramas = 0;
	$ttlmas = 0;
	$ttlmora = 0;
	$ttlvalor = 0;
	$num = 0;
	$qryemp = $db->query("SELECT * FROM empresas");
	while($rowemp = $qryemp->fetch(PDO::FETCH_ASSOC)){
		$qry2 = $db->query("SELECT * FROM carteras WHERE carempresa = ".$rowemp['empid']." AND carpromotor = '".$row['usuid']."' AND (carestado = 'ACTIVA' OR carestado = 'CASTIGADA' OR carestado = 'RECOLECCION' OR carestado = 'PENDIENTE' OR carestado = 'MOROSO' OR carestado = 'RECOLECCION RAZONADA')");
		$num = $num + $qry2->rowCount();
		while($row2 = $qry2->fetch(PDO::FETCH_ASSOC)){
			$tlcartera = $tlcartera + $row2['carsaldo'];
			$qry3 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = ".$rowemp['empid']." AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota ASC");
			$num2 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = ".$rowemp['empid']." AND dcafactura = '".$row2['carfactura']."' AND dcafecha < now()  AND dcaestado = 'ACTIVA'")->rowCount();
			while($row3 = $qry3->fetch(PDO::FETCH_ASSOC)){
				if(date('Y-m-d') > $row3['dcafecha'] && $row3['dcaestado'] == 'ACTIVA'){
					$mora = fechaDif($row3['dcafecha'],date('Y-m-d'));
					if($mora > 120){
						$moramas = $moramas + 1;
						$ttlmas = $ttlmas + ($num2 * $row2['carcuota']);
						break;
					}elseif($mora > 90){
						$mora120 = $mora120 + 1;
						$ttl120 = $ttl120 + ($num2 * $row2['carcuota']);
						break;
					}elseif($mora > 60){
						$mora90 = $mora90 + 1;
						$ttl90 = $ttl90 + ($num2 * $row2['carcuota']);
						break;
					}elseif($mora > 30){
						$mora60 = $mora60 + 1;
						$ttl60 = $ttl60 + ($num2 * $row2['carcuota']);
						break;
					}elseif($mora > 0){
						$mora30 = $mora30 + 1;
						$ttl30 = $ttl30 + ($num2 * $row2['carcuota']);
						break;
					}
				}
			}
		}
		$ttlmora = $mora30 + $mora60 + $mora90 + $mora120 + $moramas;
		$ttlvalor = $ttl30 + $ttl60 + $ttl90 + $ttl120 + $ttlmas;
		if($ttlmora != 0) @$porcnta = round(($ttlmora / $num) * 100);
		else $porcnta = 0;
		if($ttlvalor != 0) @$porvalor = round(($ttlvalor / $tlcartera) * 100);
		else $porvalor = 0;
	}
?>
<tr>
<td title="<?php echo $row['usuid'] ?>"><?php echo $row['usunombre'] ?></td>
<td align="center"><?php echo $num ?></td>
<td align="right"><?php echo number_format($tlcartera,0,',','.') ?></td>
<td align="center"><?php echo $mora30 ?></td>
<td align="right"><?php echo number_format($ttl30,0,',','.') ?></td>
<td align="center"><?php echo $mora60 ?></td>
<td align="right"><?php echo number_format($ttl60,0,',','.') ?></td>
<td align="center"><?php echo $mora90 ?></td>
<td align="right"><?php echo number_format($ttl90,0,',','.') ?></td>
<td align="center"><?php echo $mora120 ?></td>
<td align="right"><?php echo number_format($ttl120,0,',','.') ?></td>
<td align="center"><?php echo $moramas ?></td>
<td align="right"><?php echo number_format($ttlmas,0,',','.') ?></td>
<td align="center"><?php echo $ttlmora ?></td>
<td align="right"><?php echo number_format($ttlvalor,0,',','.') ?></td>
<td align="center"><?php echo $porcnta ?> %</td>
<td align="center"><?php echo $porvalor ?> %</td>
</tr>
<?php
	$numcarfn = $numcarfn + $num;
	$tlcarterafn = $tlcarterafn + $tlcartera;
	$mora30fn = $mora30fn + $mora30;
	$ttl30fn = $ttl30fn + $ttl30;
	$mora60fn = $mora60fn + $mora60;
	$ttl60fn = $ttl60fn + $ttl60;
	$mora90fn = $mora90fn + $mora90;
	$ttl90fn = $ttl90fn + $ttl90;
	$mora120fn = $mora120fn + $mora120;
	$ttl120fn = $ttl120fn + $ttl120;
	$moramasfn = $moramasfn + $moramas;
	$ttlmasfn = $ttlmasfn + $ttlmas;
	$ttlmorafn = $ttlmorafn + $ttlmora;
	$ttlvalorfn = $ttlvalorfn + $ttlvalor;
	if($ttlmorafn != 0) @$porcntafn = round(($ttlmorafn / $numcarfn) * 100);
	else $porcntafn = 0;
	if($ttlvalorfn != 0) @$porvalorfn = round(($ttlvalorfn / $tlcarterafn) * 100);
	else $porvalorfn = 0;
}
?>
</tbody>
<tfoot>
<tr bgcolor="#D1CFCF">
<td></td>
<td align="center"><?php echo $numcarfn ?></td>
<td align="right"><?php echo number_format($tlcarterafn,0,',','.') ?></td>
<td align="center"><?php echo $mora30fn ?></td>
<td align="right"><?php echo number_format($ttl30fn,0,',','.') ?></td>
<td align="center"><?php echo $mora60fn ?></td>
<td align="right"><?php echo number_format($ttl60fn,0,',','.') ?></td>
<td align="center"><?php echo $mora90fn ?></td>
<td align="right"><?php echo number_format($ttl90fn,0,',','.') ?></td>
<td align="center"><?php echo $mora120fn ?></td>
<td align="right"><?php echo number_format($ttl120fn,0,',','.') ?></td>
<td align="center"><?php echo $moramasfn ?></td>
<td align="right"><?php echo number_format($ttlmasfn,0,',','.') ?></td>
<td align="center"><?php echo $ttlmorafn ?></td>
<td align="right"><?php echo number_format($ttlvalorfn,0,',','.') ?></td>
<td align="center"><?php echo $porcntafn ?> %</td>
<td align="center"><?php echo $porvalorfn ?> %</td>
</tr>
</tfoot>
</table>
</article>
</article>
<?php require($r.'incluir/src/pie.php') ?>
</section>
</body>
</html>