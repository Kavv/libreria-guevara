<?php
$r = '../../../';  
require($r.'incluir/session.php'); 
require($r.'incluir/connection.php');
$fecha1 = $_POST['fecha1'];
$fecha2 = $_POST['fecha2']; 
$empresa = $_POST['empresa']; 

$filtro = 'fecha1='.$fecha1.'&fecha2='.$fecha2.'&empresa='.$empresa;

$sql = "SELECT * FROM movimientos WHERE movprefijo IN ('RC') AND DATE(movfecha) BETWEEN '".$fecha1."' AND '".$fecha2."' AND movempresa = '".$empresa."' AND movsaldo > 1000000 order by movtercero, movdocumento ;";
$qry = $db->query($sql);

$qryempresa = $db->query("SELECT * FROM empresas WHERE empid = '".$empresa."';");
$rowempresa = $qryempresa->fetch(PDO::FETCH_ASSOC);

?>
<!doctype html>
<html>
<head>
	<?php 
		require($r . 'incluir/src/head.php');
		require($r . 'incluir/src/head-form.php');
	?>	

<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#tabla').dataTable({
		'bJQueryUI': true,
		'bStateSave': true,
		'bSort': false,
		'sPaginationType': 'full_numbers',
		'oLanguage': {
			'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
		},
	});
	$('.historico').click(function(){
		newHref = $(this).attr('data-rel');
		$('#dialog2').load(newHref).dialog({ modal:true, width: 800 }); 
    });
	$('.btnatras').button({ icons: { primary: 'ui-icon ui-icon-arrowthick-1-w' }});
});
</script>
</head>
<body>
<?php require($r.'incluir/src/login.php') ?>
<section id="principal">
<?php require($r.'incluir/src/cabeza.php') ?>
<?php require($r.'incluir/src/menu.php') ?>
<article id="cuerpo">
<article class="mapa">
<a href="#">Principal</a><div class="mapa_div"></div><a href="#">Cartera</a><div class="mapa_div"></div><a class="current">Reportes</a>
</article>
<article id="contenido">
<h2 style="text-align:center;"> CARTERA SUPERIOR - FORMATO 1008 - Medios magneticos entre el invervalo del <?php echo $fecha1.' al '.$fecha2 ?> Saldos mayores a 1.000.000 de la empresa <?php echo $rowempresa['empnombre'] ?></h2>
<div class="reporte">
<a href="pdfmediosmagneticos.php?<?php echo $filtro ?>"><img src="<?php echo $r ?>imagenes/iconos/pdf.png" title="pdf" /></a> <a href="excelmagnetico.php?<?php echo $filtro ?>"><img src="<?php echo $r ?>imagenes/iconos/excel.png" title="csv" /></a>
</div>
<table id="tabla">
<thead>
<tr>
<th>Concepto</th>
<th>Tipo de Documento</th>
<th>Numero de Identificacion</th>
<th title="Digito de Verificacion" >DV</th>
<th>1er Apellido</th>
<th>2do Apellido</th>
<th>1er Nombre</th>
<th>2do Nombre</th>
<th>Dirección</th>
<th>Razon Social</th>
<th>Pais</th>
<th title="Saldo Cliente" >Saldo Cliente</th>
<th>Financiacion</th>
</tr>
</thead>
<tbody>
<?php
$movtercero = '';
$movdocumento = '';

while($row = $qry->fetch(PDO::FETCH_ASSOC)){

if($movtercero == $row['movtercero'] && $movdocumento == $row['movdocumento'])
{
	///SALTA EL PASO
}
else
{
	$sqldv = "SELECT * FROM movimientos WHERE movtercero = '".$row['movtercero']."' AND movdocumento = '".$row['movdocumento']."' AND movprefijo = 'DV' "; // determinar si hay devolucion
	$qrydv = $db->query($sqldv);
	$rowdv = $qrydv->rowCount();
		if ($rowdv == 0)
		{
		
			$sqlx = "SELECT max(movnumero) as ultimo_pago FROM movimientos  WHERE movtercero = '".$row['movtercero']."' AND DATE(movfecha) BETWEEN '".$fecha1."' AND '".$fecha2."' ";
			$qryx = $db->query($sqlx);
			$rowx = $qryx->fetch(PDO::FETCH_ASSOC);
			
			$sql1 = "SELECT * FROM movimientos INNER JOIN clientes ON movtercero = cliid  WHERE movtercero = '".$row['movtercero']."' AND movnumero = '".$rowx['ultimo_pago']."' ";
			$qry1 = $db->query($sql1);
			$row1 = $qry1->fetch(PDO::FETCH_ASSOC);

			$valor_total = $row1['movsaldo'];
			
			if (!empty($row1['movfinan'])) { $financiacion = $row1['movfinan']; } else { $financiacion = 0; }
			
			//echo "<br><br><br><br>".$sql."<br><br>".$sqldv."<br><br>".$sqlx."<br><br>".$sql1."<br><br>".$row1['movsaldo']."<br><br><br><br><br><br>";

			
			$total_u = $total_u + $valor_total;
?>
<tr>
<td align="center" ><?php echo "1315"; ?></td>
<td align="center" ><?php if($row1['clitide'] == 1){ echo "13";} elseif($row1['clitide'] == 2) {echo "31";} ?></td>
<td align="center" ><?php if ($valor_total >= 1000000)  {echo $row1['cliid'];} elseif($valor_total < 1000000) {echo "2222222222";} ?></td>
<td align="center" ><?php echo $row1['clidigito'] ?></td>
<td align="center" ><?php echo $row1['cliape1'] ?></td>
<td align="center" ><?php echo $row1['cliape2'] ?></td>
<td align="center" ><?php if($row1['clitide'] == 1) {echo $row1['clinombre'];} ?></td>
<td align="center" ><?php echo $row1['clinom2'] ?></td>
<td align="center" ><?php echo $row1['clidirresidencia'] ?></td>
<td align="center" ><?php if($row1['clitide'] == 2) {echo $row1['clinombre'];} ?></td>
<td align="center" ><?php echo "169"; ?></td>
<td align="center" ><?php echo number_format($valor_total,0,',','.'); ?></td>
<td align="center" ><?php echo $financiacion ?></td>
</tr>
<?php
			
		}
}
$movtercero = $row['movtercero'];
$movdocumento = $row['movdocumento'];
}
$total_fin =  $total_u ;
?>
<tfoot>
<tr>
<td align="center" ></td>
<td align="center" ></td>
<td align="center" ></td>
<td align="center" ></td>
<td align="center" ></td>
<td align="center" ></td>
<td align="center" ></td>
<td align="center" ></td>
<td align="center" ></td>
<td align="center" ></td>
<td align="center" ></td>
<td align="center" ><?php echo number_format($total_fin,0,',','.'); ?></td>
<td align="center" ></td>
</tr>
</tfoot>
</tbody>
</table>
<p class="boton">
<button type="button" class="btnatras" onClick="carga(); location.href='conmediosmagneticos.php'">Atras</button>
</p>
</article>
</article>
<?php require($r.'incluir/src/pie.php') ?>
</section>
<div id="dialog2" title="Historico de solicitudes"  style="display:none"></div>
</body>
</html>