<?php
$r = '../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
include($r.'incluir/fpdf/fpdf.php');
$empresa = $_GET['empresa'];
$promotor = $_GET['promotor'];
$fecha = $_GET['fecha'];
class PDF extends FPDF{
	function Header(){
		global $fecha;
    	$this->SetFont('LucidaConsole','',8);
		$this->Cell(0,5,date('Y/m/d H:i:s'),0,1);
		$this->SetFont('LucidaConsole','',11);
		$this->Cell(0,5,'------------------------------------------------------------------------------------------------------------------------------------------------',0,1,'C');
		$this->Cell(0,5,'REPORTE DE PREVISTOS CARTERA AL '.$fecha,0,1,'C');
		$this->Cell(0,5,'------------------------------------------------------------------------------------------------------------------------------------------------',0,1,'C');
    	$this->Ln(1);
	}

	function Footer(){
    	$this->SetY(-15);
    	$this->SetFont('LucidaConsole','',11);
		$this->Cell(0,5,'------------------------------------------------------------------------------------------------------------------------------------------------',0,1,'C');
		$this->SetFont('LucidaConsole','',8);
    	$this->Cell(0,5,'Pagina '.$this->PageNo().'/{nb}',0,0,'R');
	}
}

$pdf = new PDF('L','mm','Legal');
$pdf->AddFont('LucidaConsole','','lucon.php');
$pdf->AliasNbPages();
$pdf->AddPage();
if($empresa != '') $qry = $db->query("SELECT * FROM empresas WHERE empid = $empresa");
else $qry = $db->query("SELECT * FROM empresas");
while($row = $qry->fetch(PDO::FETCH_ASSOC)){
	$pdf->SetFont('LucidaConsole','',13);
	$pdf->SetFillColor(250,250,250);
	$pdf->Ln(5);
	$pdf->SetFillColor(210,210,210);
	$pdf->Cell(0,10,$row['empnombre'],1,1,'C',true);
	if($promotor != '') $qry2 = $db->query("SELECT * FROM usuarios WHERE usuid = $promotor");
	else $qry2 = $db->query("SELECT * FROM usuarios WHERE usupromotor = '1'");
	while($row2 = $qry2->fetch(PDO::FETCH_ASSOC)){
		$qry3 = $db->query("SELECT * FROM detcarteras, carteras, clientes WHERE dcafactura = carfactura AND carcliente = cliid AND dcafecha < '$fecha' AND carempresa = '".$row['empid']."' AND carpromotor = '".$row2['usuid']."' AND (carestado = 'ACTIVA' OR carestado = 'CASTIGADA' OR carestado = 'RECOLECCION' OR carestado = 'PENDIENTE' OR carestado = 'MOROSO' OR carestado = 'RECOLECCION RAZONADA') GROUP BY dcafactura ORDER BY dcafactura");
		$num = $qry3->rowCount();
		if($num > 0){
			$pdf->SetFont('LucidaConsole','',10);
			$pdf->SetFillColor(220,220,220);
			$pdf->Cell(0,5,$row2['usunombre'],1,1,'C',true);
			$pdf->SetFillColor(230,230,230);
			$pdf->Cell(50.5,5,'CLIENTE',1,0,'C',true);
			$pdf->Cell(20,5,'FACTURA',1,0,'C',true);
			$pdf->Cell(30,5,'Ttl CREDITO',1,0,'C',true);
			$pdf->Cell(10,5,'C',1,0,'C',true);
			$pdf->Cell(27,5,'Vlr CTA.',1,0,'C',true);
			$pdf->Cell(10,5,'CP',1,0,'C',true);
			$pdf->Cell(27,5,'Vlr.',1,0,'C',true);
			$pdf->Cell(10,5,'CA',1,0,'C',true);
			$pdf->Cell(27,5,'Vlr.',1,0,'C',true);
			$pdf->Cell(10,5,'CF',1,0,'C',true);
			$pdf->Cell(27,5,'Vlr.',1,0,'C',true);
			$pdf->Cell(10,5,'CPre',1,0,'C',true);
			$pdf->Cell(27,5,'Vlr.',1,0,'C',true);
			$pdf->Cell(25,5,'FECHA',1,0,'C',true);
			$pdf->Cell(25,5,'Ult PAGO',1,1,'C',true);
			$tlvcredito = 0;
			$tlncuota = 0;
			$tlvcuota = 0;
			$tlncpag = 0;
			$tlvcpag = 0;
			$tlncatr = 0;
			$tlvcatr = 0;
			$tlncfal = 0;
			$tlvcfal = 0;
			$tlncpre = 0;
			$tlvcpre = 0;
			while($row3 = $qry3->fetch(PDO::FETCH_ASSOC)){
				$qry4 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = ".$row['empid']." AND dcafactura = '".$row3['carfactura']."'");
				$tlvcredito = $tlvcredito + $row3['cartotal'];
				$tlncuota = $tlncuota + $row3['carncuota'];
				$tlvcuota = $tlvcuota + $row3['carcuota'];
				$ncpag = 0;
				$vcpag = 0;
				$ncatr = 0;
				$vcatr = 0;
				$ultpago = '';
				$ncprev = 0;
				$vcpre = 0;
				while($row4 = $qry4->fetch(PDO::FETCH_ASSOC)){
					if($row4['dcaestado'] == 'CANCELADO'){
						$ncpag++;
						$vcpag = $vcpag + $row4['dcavalor'];
						$ultpago = $row4['dcafepag'];
					}
					if($row4['dcaestado'] == 'ACTIVA' && $row4['dcafecha'] < date('Y-m-d')){
						$ncatr++;
						$vcatr = $vcatr + $row3['carcuota'];
						$ncprev++;
						$vcpre = $vcpre + $row3['carcuota'];
					}
					if($row4['dcafecha'] > date('Y-m-d') && $row4['dcafecha'] <= $fecha){
						$ncprev++;
						$vcpre = $vcpre + $row3['carcuota'];
					}
				}
				$ncfal = $row3['carncuota'] - $ncpaga;
				$tlncpag = $tlncpag + $ncpag;
				$tlvcpag = $tlvcpag + $vcpag;
				$tlncatr = $tlncatr + $ncatr;
				$tlvcatr = $tlvcatr + $vcatr;
				$tlncfal = $tlncfal + $ncfal;
				$tlvcfal = $tlvcfal + $row3['carsaldo'];
				$tlncpre = $tlncpre + $ncprev;
				$tlvcpre = $tlvcpre + $vcpre;
				$pdf->SetFont('LucidaConsole','',8);
				if($i%2 == 0) $x = 255;
				else $x = 235;
				$pdf->SetFillColor($x,$x,$x);
				$pdf->Cell(50.5,5,$row3['clinombre'].' '.$row3['clinom2'].' '.$row3['cliape1'].' '.$row3['cliape2'],1,0,'',true);
				$pdf->Cell(20,5,$row3['carfactura'],1,0,'C',true);
				$pdf->Cell(30,5,number_format($row3['cartotal'],0,',','.'),1,0,'R',true);
				$pdf->Cell(10,5,$row3['carncuota'],1,0,'C',true);
				$pdf->Cell(27,5,number_format($row3['carcuota'],0,',','.'),1,0,'R',true);
				$pdf->Cell(10,5,$ncpag,1,0,'C',true);
				$pdf->Cell(27,5,number_format($vcpag,0,',','.'),1,0,'R',true);
				$pdf->Cell(10,5,$ncatr,1,0,'C',true);
				$pdf->Cell(27,5,number_format($vcatr,0,',','.'),1,0,'R',true);
				$pdf->Cell(10,5,$ncfal,1,0,'C',true);
				$pdf->Cell(27,5,number_format($row3['carsaldo'],0,',','.'),1,0,'R',true);
				$pdf->Cell(10,5,$ncprev,1,0,'C',true);
				$pdf->Cell(27,5,number_format($vcpre,0,',','.'),1,0,'R',true);
				$pdf->Cell(25,5,$row3['dcafecha'],1,0,'C',true);
				$pdf->Cell(25,5,$ultpago,1,1,'C',true);
				$i++;
			}
			$pdf->SetFillColor(255,255,255);
			$pdf->Cell(50.5,5,'',0,0,'',true);
			$pdf->SetFillColor(210,210,210);
			$pdf->Cell(20,5,$num,1,0,'C',true);
			$pdf->Cell(30,5,number_format($tlvcredito,0,',','.'),1,0,'R',true);
			$pdf->Cell(10,5,$tlncuota,1,0,'C',true);
			$pdf->Cell(27,5,number_format($tlvcuota,0,',','.'),1,0,'R',true);
			$pdf->Cell(10,5,$tlncpag,1,0,'C',true);
			$pdf->Cell(27,5,number_format($tlvcpag,0,',','.'),1,0,'R',true);
			$pdf->Cell(10,5,$tlncatr,1,0,'C',true);
			$pdf->Cell(27,5,number_format($tlvcatr,0,',','.'),1,0,'R',true);
			$pdf->Cell(10,5,$tlncfal,1,0,'C',true);
			$pdf->Cell(27,5,number_format($tlvcfal,0,',','.'),1,0,'R',true);
			$pdf->Cell(10,5,$tlncpre,1,0,'C',true);
			$pdf->Cell(27,5,number_format($tlvcpre,0,',','.'),1,0,'R',true);
			$pdf->SetFillColor(255,255,255);
			$pdf->Cell(50,5,'',0,1,'',true);
		}
	}
}
$pdf->Output('previsto.pdf','d');
?>