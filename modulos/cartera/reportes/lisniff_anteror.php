<?php
$r = '../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
require($r.'incluir/funciones.php');

$empresa = $_GET['empresa'];
$fecha1 = $_GET['fecha1'];
$fecha2 = $_GET['fecha2'];
$filtro = 'empresa='.$empresa.'&fecha1='.$fecha1.'&fecha2='.$fecha2;



$rowem = $db->query("SELECT * FROM empresas WHERE empid = '$empresa'")->fetch(PDO::FETCH_ASSOC); // row Nombre de la empresa

if ($empresa <> 'TODAS'){
	$anexEmpresa = "  AND carempresa = '$empresa' ";
}

?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
	<?php 
		require($r . 'incluir/src/head.php');
		require($r . 'incluir/src/head-form.php');
	?>	

<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#tabla').dataTable({
		'bPaginate': false,
        'bLengthChange': false,
        'bFilter': false,
		'bSort': false,
        'bInfo': false,
        'bAutoWidth': false,
		'bStateSave': true,
		'bJQueryUI': true
	});
	$('.btnatras').button({ icons: { primary: 'ui-icon ui-icon-arrowthick-1-w' }});
});
</script>
</head>
<body>
<?php require($r.'incluir/src/login.php') ?>
<section id="principal">
<?php require($r.'incluir/src/cabeza.php') ?>
<?php require($r.'incluir/src/menu.php') ?>
<article id="cuerpo">
<article class="mapa">
<a href="#">Principal</a><div class="mapa_div"></div><a href="#">Cartera</a><div class="mapa_div"></div><a class="current">Reportes</a>
</article>
<article id="contenido">
<h2>Reporte NIFF del <?php  echo $fecha1.' al '.$fecha2; if($empresa != '') {echo ' de la empresa '.$rowem['empnombre'].' ';} ?></h2>
<div class="reporte">
<a href="pdfniff.php?<?php echo $filtro ?>"><img src="<?php echo $r ?>imagenes/iconos/pdf.png" title="pdf" /></a> <a href="#"><img src="<?php echo $r ?>imagenes/iconos/excel.png" title="csv" /></a>
</div>
<table id="tabla">
<thead>
<tr>
<th></th>
<th>Saldo Cartera</th>
<th>Valor K</th>
<th>Valor Financiacion</th>
<th>Nro Clientes</th>
<th>Valor Deterioro</th>
</tr>
</thead>
<tbody>
<?php 
// al dia
$qry = $db->query("SELECT * FROM ((carteras INNER JOIN clientes ON carcliente = cliid) INNER JOIN empresas ON carempresa = empid) INNER JOIN usuarios ON carpromotor = usuid INNER JOIN departamentos ON  depid = clidepresidencia  WHERE (carestado = 'ACTIVA') $anexEmpresa");
	while($row = $qry->fetch(PDO::FETCH_ASSOC)){	
		$qry2 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row['carempresa']."' AND dcafactura = '".$row['carfactura']."' AND dcaestado = 'ACTIVA' ORDER BY dcacuota ASC");
		$num2 = $qry2->rowCount();
		$row2 = $qry2->fetch(PDO::FETCH_ASSOC);
		if($fecha2 <= $row2['dcafecha']){
			$clientesaldia++;
			$saldo1 = $saldo1 + $row['carsaldo'];
			$mora = 0;
			$valorkaldia = 0;
			$financiacionaldia = 0;
			
		}
	}
// 1 -30 
	$qry2 = $db->query("SELECT * FROM ((carteras INNER JOIN clientes ON carcliente = cliid) INNER JOIN empresas ON carempresa = empid) INNER JOIN usuarios ON carpromotor = usuid INNER JOIN departamentos ON  depid = clidepresidencia  WHERE (carestado = 'ACTIVA') $anexEmpresa");
	while($row2 = $qry2->fetch(PDO::FETCH_ASSOC)){
		$qry3 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota DESC");
		while($row3 = $qry3->fetch(PDO::FETCH_ASSOC)){	
			if($row3['dcafepag'] != ''){
				$ultpago = $row3['dcafepag'];
				break;
			}
		}
		$qry4 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota ASC");
		$ncmora = 0;
		$ncpend = 0;
		$residuo = 0;
		while($row4 = $qry4->fetch(PDO::FETCH_ASSOC)){	
			if(date('Y-m-d') > $row4['dcafecha'] && $row4['dcaestado'] == 'ACTIVA'){
				$ncmora = $ncmora + 1;
				$residuo = $residuo + ($row4['dcavalor'] + $row4['dcadescuento']);
			}
			if($row4['dcaestado'] == 'ACTIVA')
				$ncpend = $ncpend + 1;
		}
		$qry5 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota ASC");
		while($row5 = $qry5->fetch(PDO::FETCH_ASSOC)){
			if(date('Y-m-d') > $row5['dcafecha'] && $row5['dcaestado'] == 'ACTIVA'){
				$mora = fechaDif($row5['dcafecha'],$fecha2);
				if($mora >= 1 && $mora <= 30){
					
					$valork1_30 = $valork1_30 + (($ncmora * $row2['carcuota']) - $residuo);
					$financiacion1_30 = $valork1_30 * 0.01;
					$clientes1_30 = $clientes1_30 + 1;
					$deterioro1_30 = $valork1_30 * 0.01; 
				}
				break;
			}	
		}
	}
//31 - 60
	$qry2 = $db->query("SELECT * FROM ((carteras INNER JOIN clientes ON carcliente = cliid) INNER JOIN empresas ON carempresa = empid) INNER JOIN usuarios ON carpromotor = usuid INNER JOIN departamentos ON  depid = clidepresidencia  WHERE (carestado = 'ACTIVA') $anexEmpresa");
	while($row2 = $qry2->fetch(PDO::FETCH_ASSOC)){
		$qry3 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota DESC");
		while($row3 = $qry3->fetch(PDO::FETCH_ASSOC)){	
			if($row3['dcafepag'] != ''){
				$ultpago = $row3['dcafepag'];
				break;
			}
		}
		$qry4 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota ASC");
		$ncmora = 0;
		$ncpend = 0;
		$residuo = 0;
		while($row4 = $qry4->fetch(PDO::FETCH_ASSOC)){	
			if(date('Y-m-d') > $row4['dcafecha'] && $row4['dcaestado'] == 'ACTIVA'){
				$ncmora = $ncmora + 1;
				$residuo = $residuo + ($row4['dcavalor'] + $row4['dcadescuento']);
			}
			if($row4['dcaestado'] == 'ACTIVA')
				$ncpend = $ncpend + 1;
		}
		$qry5 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota ASC");
		while($row5 = $qry5->fetch(PDO::FETCH_ASSOC)){
			if(date('Y-m-d') > $row5['dcafecha'] && $row5['dcaestado'] == 'ACTIVA'){
				$mora = fechaDif($row5['dcafecha'],$fecha2);
				if($mora >= 31 && $mora <= 60){
					
					$valork31_60 = $valork31_60 + (($ncmora * $row2['carcuota']) - $residuo);
					$financiacion31_60 = $valork31_60 * 0.01;
					$clientes31_60 = $clientes31_60 + 1;
					$deterioro31_60 = $valork31_60 * 0.01; 
				}
				break;
			}	
		}
	}
// 61 - 90
	$qry2 = $db->query("SELECT * FROM ((carteras INNER JOIN clientes ON carcliente = cliid) INNER JOIN empresas ON carempresa = empid) INNER JOIN usuarios ON carpromotor = usuid INNER JOIN departamentos ON  depid = clidepresidencia  WHERE (carestado = 'ACTIVA') $anexEmpresa");
	while($row2 = $qry2->fetch(PDO::FETCH_ASSOC)){
		$qry3 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota DESC");
		while($row3 = $qry3->fetch(PDO::FETCH_ASSOC)){	
			if($row3['dcafepag'] != ''){
				$ultpago = $row3['dcafepag'];
				break;
			}
		}
		$qry4 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota ASC");
		$ncmora = 0;
		$ncpend = 0;
		$residuo = 0;
		while($row4 = $qry4->fetch(PDO::FETCH_ASSOC)){	
			if(date('Y-m-d') > $row4['dcafecha'] && $row4['dcaestado'] == 'ACTIVA'){
				$ncmora = $ncmora + 1;
				$residuo = $residuo + ($row4['dcavalor'] + $row4['dcadescuento']);
			}
			if($row4['dcaestado'] == 'ACTIVA')
				$ncpend = $ncpend + 1;
		}
		$qry5 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota ASC");
		while($row5 = $qry5->fetch(PDO::FETCH_ASSOC)){
			if(date('Y-m-d') > $row5['dcafecha'] && $row5['dcaestado'] == 'ACTIVA'){
				$mora = fechaDif($row5['dcafecha'],$fecha2);
				if($mora >= 61 && $mora <= 90){
					
					$valork61_90 = $valork61_90 + (($ncmora * $row2['carcuota']) - $residuo);
					$financiacion61_90 = $valork61_90 * 0.01;
					$clientes61_90 = $clientes61_90 + 1;
					$deterioro61_90 = $valork61_90 * 0.01; 
				}
				break;
			}	
		}
	}
// 91 -120
	$qry2 = $db->query("SELECT * FROM ((carteras INNER JOIN clientes ON carcliente = cliid) INNER JOIN empresas ON carempresa = empid) INNER JOIN usuarios ON carpromotor = usuid INNER JOIN departamentos ON  depid = clidepresidencia  WHERE (carestado = 'ACTIVA') $anexEmpresa");
	while($row2 = $qry2->fetch(PDO::FETCH_ASSOC)){
		$qry3 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota DESC");
		while($row3 = $qry3->fetch(PDO::FETCH_ASSOC)){	
			if($row3['dcafepag'] != ''){
				$ultpago = $row3['dcafepag'];
				break;
			}
		}
		$qry4 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota ASC");
		$ncmora = 0;
		$ncpend = 0;
		$residuo = 0;
		while($row4 = $qry4->fetch(PDO::FETCH_ASSOC)){	
			if(date('Y-m-d') > $row4['dcafecha'] && $row4['dcaestado'] == 'ACTIVA'){
				$ncmora = $ncmora + 1;
				$residuo = $residuo + ($row4['dcavalor'] + $row4['dcadescuento']);
			}
			if($row4['dcaestado'] == 'ACTIVA')
				$ncpend = $ncpend + 1;
		}
		$qry5 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota ASC");
		while($row5 = $qry5->fetch(PDO::FETCH_ASSOC)){
			if(date('Y-m-d') > $row5['dcafecha'] && $row5['dcaestado'] == 'ACTIVA'){
				$mora = fechaDif($row5['dcafecha'],$fecha2);
				if($mora >= 91 && $mora <= 120){
					
					$valork91_120 = $valork91_120 + (($ncmora * $row2['carcuota']) - $residuo);
					$financiacion91_120 = $valork91_120 * 0.01;
					$clientes91_120 = $clientes91_120 + 1;
					$deterioro91_120 = $valork91_120 * 0.032; 
				}
				break;
			}	
		}
	}
// 121 - 150

	$qry2 = $db->query("SELECT * FROM ((carteras INNER JOIN clientes ON carcliente = cliid) INNER JOIN empresas ON carempresa = empid) INNER JOIN usuarios ON carpromotor = usuid INNER JOIN departamentos ON  depid = clidepresidencia  WHERE (carestado = 'ACTIVA') $anexEmpresa");
	while($row2 = $qry2->fetch(PDO::FETCH_ASSOC)){
		$qry3 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota DESC");
		while($row3 = $qry3->fetch(PDO::FETCH_ASSOC)){	
			if($row3['dcafepag'] != ''){
				$ultpago = $row3['dcafepag'];
				break;
			}
		}
		$qry4 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota ASC");
		$ncmora = 0;
		$ncpend = 0;
		$residuo = 0;
		while($row4 = $qry4->fetch(PDO::FETCH_ASSOC)){	
			if(date('Y-m-d') > $row4['dcafecha'] && $row4['dcaestado'] == 'ACTIVA'){
				$ncmora = $ncmora + 1;
				$residuo = $residuo + ($row4['dcavalor'] + $row4['dcadescuento']);
			}
			if($row4['dcaestado'] == 'ACTIVA')
				$ncpend = $ncpend + 1;
		}
		$qry5 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota ASC");
		while($row5 = $qry5->fetch(PDO::FETCH_ASSOC)){
			if(date('Y-m-d') > $row5['dcafecha'] && $row5['dcaestado'] == 'ACTIVA'){
				$mora = fechaDif($row5['dcafecha'],$fecha2);
				if($mora >= 121 && $mora <= 150){
					
					$valork121_150 = $valork121_150 + (($ncmora * $row2['carcuota']) - $residuo);
					$financiacion121_150 = $valork121_150 * 0.01;
					$clientes121_150 = $clientes121_150 + 1;
					$deterioro121_150 = $valork121_150 * 0.032; 
				}
				break;
			}	
		}
	}
// 151 - 180

	$qry2 = $db->query("SELECT * FROM ((carteras INNER JOIN clientes ON carcliente = cliid) INNER JOIN empresas ON carempresa = empid) INNER JOIN usuarios ON carpromotor = usuid INNER JOIN departamentos ON  depid = clidepresidencia  WHERE (carestado = 'ACTIVA') $anexEmpresa");
	while($row2 = $qry2->fetch(PDO::FETCH_ASSOC)){
		$qry3 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota DESC");
		while($row3 = $qry3->fetch(PDO::FETCH_ASSOC)){	
			if($row3['dcafepag'] != ''){
				$ultpago = $row3['dcafepag'];
				break;
			}
		}
		$qry4 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota ASC");
		$ncmora = 0;
		$ncpend = 0;
		$residuo = 0;
		while($row4 = $qry4->fetch(PDO::FETCH_ASSOC)){	
			if(date('Y-m-d') > $row4['dcafecha'] && $row4['dcaestado'] == 'ACTIVA'){
				$ncmora = $ncmora + 1;
				$residuo = $residuo + ($row4['dcavalor'] + $row4['dcadescuento']);
			}
			if($row4['dcaestado'] == 'ACTIVA')
				$ncpend = $ncpend + 1;
		}
		$qry5 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota ASC");
		while($row5 = $qry5->fetch(PDO::FETCH_ASSOC)){
			if(date('Y-m-d') > $row5['dcafecha'] && $row5['dcaestado'] == 'ACTIVA'){
				$mora = fechaDif($row5['dcafecha'],$fecha2);
				if($mora >= 151 && $mora <= 180){
					
					$valork151_180 = $valork151_180 + (($ncmora * $row2['carcuota']) - $residuo);
					$financiacion151_180 = $valork151_180 * 0.01;
					$clientes151_180 = $clientes151_180 + 1;
					$deterioro151_180 = $valork151_180 * 0.032; 
				}
				break;
			}	
		}
	}
// 181 - 210 

	$qry2 = $db->query("SELECT * FROM ((carteras INNER JOIN clientes ON carcliente = cliid) INNER JOIN empresas ON carempresa = empid) INNER JOIN usuarios ON carpromotor = usuid INNER JOIN departamentos ON  depid = clidepresidencia  WHERE (carestado = 'ACTIVA') $anexEmpresa");
	while($row2 = $qry2->fetch(PDO::FETCH_ASSOC)){
		$qry3 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota DESC");
		while($row3 = $qry3->fetch(PDO::FETCH_ASSOC)){	
			if($row3['dcafepag'] != ''){
				$ultpago = $row3['dcafepag'];
				break;
			}
		}
		$qry4 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota ASC");
		$ncmora = 0;
		$ncpend = 0;
		$residuo = 0;
		while($row4 = $qry4->fetch(PDO::FETCH_ASSOC)){	
			if(date('Y-m-d') > $row4['dcafecha'] && $row4['dcaestado'] == 'ACTIVA'){
				$ncmora = $ncmora + 1;
				$residuo = $residuo + ($row4['dcavalor'] + $row4['dcadescuento']);
			}
			if($row4['dcaestado'] == 'ACTIVA')
				$ncpend = $ncpend + 1;
		}
		$qry5 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota ASC");
		while($row5 = $qry5->fetch(PDO::FETCH_ASSOC)){
			if(date('Y-m-d') > $row5['dcafecha'] && $row5['dcaestado'] == 'ACTIVA'){
				$mora = fechaDif($row5['dcafecha'],$fecha2);
				if($mora >= 181 && $mora <= 210){
					
					$valork181_210 = $valork181_210 + (($ncmora * $row2['carcuota']) - $residuo);
					$financiacion181_210 = $valork181_210 * 0.01;
					$clientes181_210 = $clientes181_210 + 1;
					$deterioro181_210 = $valork181_210 * 0.5; 
					
				}
				break;
			}	
		}
	}
// 211- 240
	$qry2 = $db->query("SELECT * FROM ((carteras INNER JOIN clientes ON carcliente = cliid) INNER JOIN empresas ON carempresa = empid) INNER JOIN usuarios ON carpromotor = usuid INNER JOIN departamentos ON  depid = clidepresidencia  WHERE (carestado = 'ACTIVA') $anexEmpresa");
	while($row2 = $qry2->fetch(PDO::FETCH_ASSOC)){
		$qry3 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota DESC");
		while($row3 = $qry3->fetch(PDO::FETCH_ASSOC)){	
			if($row3['dcafepag'] != ''){
				$ultpago = $row3['dcafepag'];
				break;
			}
		}
		$qry4 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota ASC");
		$ncmora = 0;
		$ncpend = 0;
		$residuo = 0;
		while($row4 = $qry4->fetch(PDO::FETCH_ASSOC)){	
			if(date('Y-m-d') > $row4['dcafecha'] && $row4['dcaestado'] == 'ACTIVA'){
				$ncmora = $ncmora + 1;
				$residuo = $residuo + ($row4['dcavalor'] + $row4['dcadescuento']);
			}
			if($row4['dcaestado'] == 'ACTIVA')
				$ncpend = $ncpend + 1;
		}
		$qry5 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota ASC");
		while($row5 = $qry5->fetch(PDO::FETCH_ASSOC)){
			if(date('Y-m-d') > $row5['dcafecha'] && $row5['dcaestado'] == 'ACTIVA'){
				$mora = fechaDif($row5['dcafecha'],$fecha2);
				if($mora >= 211 && $mora <= 240){
					
					$valork211_240 = $valork211_240 + (($ncmora * $row2['carcuota']) - $residuo);
					$financiacion211_240 = $valork211_240 * 0.01;
					$clientes211_240 = $clientes211_240 + 1;
					$deterioro211_240 = $valork211_240 * 0.5; 
				}
				break;
			}	
		}
	}
// 241-270
	$qry2 = $db->query("SELECT * FROM ((carteras INNER JOIN clientes ON carcliente = cliid) INNER JOIN empresas ON carempresa = empid) INNER JOIN usuarios ON carpromotor = usuid INNER JOIN departamentos ON  depid = clidepresidencia  WHERE (carestado = 'ACTIVA') $anexEmpresa");
	while($row2 = $qry2->fetch(PDO::FETCH_ASSOC)){
		$qry3 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota DESC");
		while($row3 = $qry3->fetch(PDO::FETCH_ASSOC)){	
			if($row3['dcafepag'] != ''){
				$ultpago = $row3['dcafepag'];
				break;
			}
		}
		$qry4 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota ASC");
		$ncmora = 0;
		$ncpend = 0;
		$residuo = 0;
		while($row4 = $qry4->fetch(PDO::FETCH_ASSOC)){	
			if(date('Y-m-d') > $row4['dcafecha'] && $row4['dcaestado'] == 'ACTIVA'){
				$ncmora = $ncmora + 1;
				$residuo = $residuo + ($row4['dcavalor'] + $row4['dcadescuento']);
			}
			if($row4['dcaestado'] == 'ACTIVA')
				$ncpend = $ncpend + 1;
		}
		$qry5 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota ASC");
		while($row5 = $qry5->fetch(PDO::FETCH_ASSOC)){
			if(date('Y-m-d') > $row5['dcafecha'] && $row5['dcaestado'] == 'ACTIVA'){
				$mora = fechaDif($row5['dcafecha'],$fecha2);
				if($mora >= 241 && $mora <= 270){
					
					$valork241_270 = $valork241_270 + (($ncmora * $row2['carcuota']) - $residuo);
					$financiacion241_270 = $valork241_270 * 0.01;
					$clientes241_270 = $clientes241_270 + 1;
					$deterioro241_270 = $valork241_270 * 0.5; 
				}
				break;
			}	
		}
	}
//271-300

	$qry2 = $db->query("SELECT * FROM ((carteras INNER JOIN clientes ON carcliente = cliid) INNER JOIN empresas ON carempresa = empid) INNER JOIN usuarios ON carpromotor = usuid INNER JOIN departamentos ON  depid = clidepresidencia  WHERE (carestado = 'ACTIVA') $anexEmpresa");
	while($row2 = $qry2->fetch(PDO::FETCH_ASSOC)){
		$qry3 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota DESC");
		while($row3 = $qry3->fetch(PDO::FETCH_ASSOC)){	
			if($row3['dcafepag'] != ''){
				$ultpago = $row3['dcafepag'];
				break;
			}
		}
		$qry4 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota ASC");
		$ncmora = 0;
		$ncpend = 0;
		$residuo = 0;
		while($row4 = $qry4->fetch(PDO::FETCH_ASSOC)){	
			if(date('Y-m-d') > $row4['dcafecha'] && $row4['dcaestado'] == 'ACTIVA'){
				$ncmora = $ncmora + 1;
				$residuo = $residuo + ($row4['dcavalor'] + $row4['dcadescuento']);
			}
			if($row4['dcaestado'] == 'ACTIVA')
				$ncpend = $ncpend + 1;
		}
		$qry5 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota ASC");
		while($row5 = $qry5->fetch(PDO::FETCH_ASSOC)){
			if(date('Y-m-d') > $row5['dcafecha'] && $row5['dcaestado'] == 'ACTIVA'){
				$mora = fechaDif($row5['dcafecha'],$fecha2);
				if($mora >= 271 && $mora <= 300){
					
					$valork271_300 = $valork271_300 + (($ncmora * $row2['carcuota']) - $residuo);
					$financiacion271_300 = $valork271_300 * 0.01;
					$clientes271_300 = $clientes271_300 + 1;
					$deterioro271_300 = $valork271_300 * 0.5; 
				}
				break;
			}	
		}
	}
// 301- 330
	$qry2 = $db->query("SELECT * FROM ((carteras INNER JOIN clientes ON carcliente = cliid) INNER JOIN empresas ON carempresa = empid) INNER JOIN usuarios ON carpromotor = usuid INNER JOIN departamentos ON  depid = clidepresidencia  WHERE (carestado = 'ACTIVA') $anexEmpresa");
	while($row2 = $qry2->fetch(PDO::FETCH_ASSOC)){
		$qry3 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota DESC");
		while($row3 = $qry3->fetch(PDO::FETCH_ASSOC)){	
			if($row3['dcafepag'] != ''){
				$ultpago = $row3['dcafepag'];
				break;
			}
		}
		$qry4 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota ASC");
		$ncmora = 0;
		$ncpend = 0;
		$residuo = 0;
		while($row4 = $qry4->fetch(PDO::FETCH_ASSOC)){	
			if(date('Y-m-d') > $row4['dcafecha'] && $row4['dcaestado'] == 'ACTIVA'){
				$ncmora = $ncmora + 1;
				$residuo = $residuo + ($row4['dcavalor'] + $row4['dcadescuento']);
			}
			if($row4['dcaestado'] == 'ACTIVA')
				$ncpend = $ncpend + 1;
		}
		$qry5 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota ASC");
		while($row5 = $qry5->fetch(PDO::FETCH_ASSOC)){
			if(date('Y-m-d') > $row5['dcafecha'] && $row5['dcaestado'] == 'ACTIVA'){
				$mora = fechaDif($row5['dcafecha'],$fecha2);
				if($mora >= 301 && $mora <= 330){
					
					$valork301_330 = $valork1_330 + (($ncmora * $row2['carcuota']) - $residuo);
					$financiacion301_330 = $valork301_330 * 0.01;
					$clientes301_330 = $clientes301_330 + 1;
					$deterioro301_330 = $valork301_330 * 0.5; 
				}
				break;
			}	
		}
	}
// 331 - 360
	$qry2 = $db->query("SELECT * FROM ((carteras INNER JOIN clientes ON carcliente = cliid) INNER JOIN empresas ON carempresa = empid) INNER JOIN usuarios ON carpromotor = usuid INNER JOIN departamentos ON  depid = clidepresidencia  WHERE (carestado = 'ACTIVA') $anexEmpresa");
	while($row2 = $qry2->fetch(PDO::FETCH_ASSOC)){
		$qry3 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota DESC");
		while($row3 = $qry3->fetch(PDO::FETCH_ASSOC)){	
			if($row3['dcafepag'] != ''){
				$ultpago = $row3['dcafepag'];
				break;
			}
		}
		$qry4 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota ASC");
		$ncmora = 0;
		$ncpend = 0;
		$residuo = 0;
		while($row4 = $qry4->fetch(PDO::FETCH_ASSOC)){	
			if(date('Y-m-d') > $row4['dcafecha'] && $row4['dcaestado'] == 'ACTIVA'){
				$ncmora = $ncmora + 1;
				$residuo = $residuo + ($row4['dcavalor'] + $row4['dcadescuento']);
			}
			if($row4['dcaestado'] == 'ACTIVA')
				$ncpend = $ncpend + 1;
		}
		$qry5 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota ASC");
		while($row5 = $qry5->fetch(PDO::FETCH_ASSOC)){
			if(date('Y-m-d') > $row5['dcafecha'] && $row5['dcaestado'] == 'ACTIVA'){
				$mora = fechaDif($row5['dcafecha'],$fecha2);
				if($mora >= 331 && $mora <= 360){
					
					$valork331_360 = $valork331_360 + (($ncmora * $row2['carcuota']) - $residuo);
					$financiacion331_360 = $valork331_360 * 0.01;
					$clientes331_360 = $clientes331_360 + 1;
					$deterioro331_360 = $valork331_360 * 1; 
				}
				break;
			}	
		}
	}
// castigada
	$qry2 = $db->query("SELECT * FROM ((carteras INNER JOIN clientes ON carcliente = cliid) INNER JOIN empresas ON carempresa = empid) INNER JOIN usuarios ON carpromotor = usuid INNER JOIN departamentos ON  depid = clidepresidencia  WHERE (carestado = 'CASTIGADA') $anexEmpresa");
	while($row2 = $qry2->fetch(PDO::FETCH_ASSOC)){
		$qry3 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota DESC");
		while($row3 = $qry3->fetch(PDO::FETCH_ASSOC)){	
			if($row3['dcafepag'] != ''){
				$ultpago = $row3['dcafepag'];
				break;
			}
		}
		$qry4 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota ASC");
		$ncmora = 0;
		$ncpend = 0;
		$residuo = 0;
		while($row4 = $qry4->fetch(PDO::FETCH_ASSOC)){	
			if(date('Y-m-d') > $row4['dcafecha'] && $row4['dcaestado'] == 'ACTIVA'){
				$ncmora = $ncmora + 1;
				$residuo = $residuo + ($row4['dcavalor'] + $row4['dcadescuento']);
			}
			if($row4['dcaestado'] == 'ACTIVA')
				$ncpend = $ncpend + 1;
		}
		$qry5 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota ASC");
		while($row5 = $qry5->fetch(PDO::FETCH_ASSOC)){
			if(date('Y-m-d') > $row5['dcafecha'] && $row5['dcaestado'] == 'ACTIVA'){
				$mora = fechaDif($row5['dcafecha'],$fecha2);
				if($mora >= 0){
					$valorkcastigada = $valorkcastigada + (($ncmora * $row2['carcuota']) - $residuo);
					$financiacioncastigada= $valorkcastigada * 0.01;
					$clientescastigada = $clientescastigada + 1;
				}
				break;
			}	
		}
	}
?>
<tr> <td>A</td> <td>AL DIA </td> <td align="center"><?php echo number_format($valorkaldia ,0,',','.') ?></td> <td align="center"><?php echo number_format($financiacionaldia ,0,',','.') ?></td> <td align="center"><?php echo number_format($clientesaldia ,0,',','.') ?></td> <td align="center"><?php echo number_format($no ,0,',','.') ?></td>  </tr>
<tr><td>A</td> <td>MORA 30 </td> <td align="center"><?php echo number_format($valork1_30 ,0,',','.') ?></td> <td align="center"><?php echo number_format($financiacion1_30 ,0,',','.') ?></td> <td align="center"><?php echo number_format($clientes1_30 ,0,',','.') ?></td> <td align="center"><?php echo number_format($deterioro1_30 ,0,',','.') ?></td>  </tr>
<tr><td>B</td> <td>MORA 60 </td> <td align="center"><?php echo number_format($valork31_60 ,0,',','.') ?></td> <td align="center"><?php echo number_format($financiacion31_60 ,0,',','.') ?></td> <td align="center"><?php echo number_format($clientes31_60 ,0,',','.') ?></td> <td align="center"><?php echo number_format($deterioro31_60 ,0,',','.') ?></td>  </tr>
<tr><td>B</td> <td>MORA 90 </td> <td align="center"><?php echo number_format($valork61_90 ,0,',','.') ?></td> <td align="center"><?php echo number_format($financiacion61_90 ,0,',','.') ?></td> <td align="center"><?php echo number_format($clientes61_90 ,0,',','.') ?></td> <td align="center"><?php echo number_format($deterioro61_90 ,0,',','.') ?></td>  </tr>
<tr><td>B</td> <td>MORA 120 </td> <td align="center"><?php echo number_format($valork91_120 ,0,',','.') ?></td> <td align="center"><?php echo number_format($financiacion91_120 ,0,',','.') ?></td> <td align="center"><?php echo number_format($clientes91_120 ,0,',','.') ?></td> <td align="center"><?php echo number_format($deterioro91_120 ,0,',','.') ?></td>  </tr>
<tr><td>C</td> <td>MORA 150 </td> <td align="center"><?php echo number_format($valork121_150 ,0,',','.') ?></td> <td align="center"><?php echo number_format($financiacion121_150 ,0,',','.') ?></td> <td align="center"><?php echo number_format($clientes121_150 ,0,',','.') ?></td> <td align="center"><?php echo number_format($deterioro121_150 ,0,',','.') ?></td>  </tr>
<tr><td>C</td> <td>MORA 180 </td> <td align="center"><?php echo number_format($valork151_180 ,0,',','.') ?></td> <td align="center"><?php echo number_format($financiacion151_180 ,0,',','.') ?></td> <td align="center"><?php echo number_format($clientes151_180 ,0,',','.') ?></td> <td align="center"><?php echo number_format($deterioro151_180 ,0,',','.') ?></td>  </tr>
<tr><td>C</td> <td>MORA 210 </td> <td align="center"><?php echo number_format($valork181_210 ,0,',','.') ?></td> <td align="center"><?php echo number_format($financiacion181_210 ,0,',','.') ?></td> <td align="center"><?php echo number_format($clientes181_210 ,0,',','.') ?></td> <td align="center"><?php echo number_format($deterioro181_210 ,0,',','.') ?></td>  </tr>
<tr> <td>D</td><td>MORA 240 </td> <td align="center"><?php echo number_format($valork211_240 ,0,',','.') ?></td> <td align="center"><?php echo number_format($financiacion211_240 ,0,',','.') ?></td> <td align="center"><?php echo number_format($clientes211_240 ,0,',','.') ?></td> <td align="center"><?php echo number_format($deterioro211_240 ,0,',','.') ?></td>  </tr>
<tr> <td>D</td><td>MORA 270 </td> <td align="center"><?php echo number_format($valork241_270 ,0,',','.') ?></td> <td align="center"><?php echo number_format($financiacion241_270 ,0,',','.') ?></td> <td align="center"><?php echo number_format($clientes241_270 ,0,',','.') ?></td> <td align="center"><?php echo number_format($deterioro241_270 ,0,',','.') ?></td>  </tr>
<tr> <td>D</td><td>MORA 300 </td> <td align="center"><?php echo number_format($valork271_300 ,0,',','.') ?></td> <td align="center"><?php echo number_format($financiacion271_300 ,0,',','.') ?></td> <td align="center"><?php echo number_format($clientes271_300 ,0,',','.') ?></td> <td align="center"><?php echo number_format($deterioro271_300 ,0,',','.') ?></td>  </tr>
<tr> <td>D</td><td>MORA 330 </td> <td align="center"><?php echo number_format($valork301_330 ,0,',','.') ?></td> <td align="center"><?php echo number_format($financiacion301_330 ,0,',','.') ?></td> <td align="center"><?php echo number_format($clientes301_330 ,0,',','.') ?></td> <td align="center"><?php echo number_format($deterioro301_330 ,0,',','.') ?></td>  </tr>
<tr> <td>D</td><td>MORA 360 </td> <td align="center"><?php echo number_format($valork331_360 ,0,',','.') ?></td> <td align="center"><?php echo number_format($financiacion331_360 ,0,',','.') ?></td> <td align="center"><?php echo number_format($clientes331_360 ,0,',','.') ?></td> <td align="center"><?php echo number_format($deterioro331_360 ,0,',','.') ?></td>  </tr>
<tr> <td>E</td><td>CARTERA CASTIGADA </td> <td align="center"><?php echo number_format($valorkcastigada ,0,',','.') ?></td> <td align="center"><?php echo number_format($financiacioncastigada ,0,',','.') ?></td> <td align="center"><?php echo number_format($clientescastigada ,0,',','.') ?></td> <td align="center"><?php echo number_format($no ,0,',','.') ?></td>  </tr>

<?php
$totalvalork = $valorkaldia + $valork1_30 + $valork31_60 + $valork61_90 + $valork91_120 + $valork121_150 + $valork151_180 + $valork181_210 + $valork211_240 + $valork241_270 + $valork271_300 + $valork301_330 + $valork331_360 + $valorkcastigada;
$totalfinanciacion = $financiacionaldia + $financiacion1_30 + $financiacion31_60 + $financiacion61_90 + $financiacion91_120 + $financiacion121_150 + $financiacion151_180 + $financiacion181_210 + $financiacion211_240 + $financiacion241_270 + $financiacion271_300 + $financiacion301_330 + $financiacion331_360 + $financiacioncastigada;
$totalclientes = $clientesaldia + $clientes1_30 + $clientes31_60 + $clientes61_90 + $clientes91_120 + $clientes121_150 + $clientes151_180 + $clientes181_210 + $clientes211_240 + $clientes241_270 + $clientes271_300 + $clientes301_330 + $clientes331_360 + $clientescastigada;
$totaldeterioro = $deterioro1_30 + $deterioro31_60 + $deterioro61_90 + $deterioro91_120 + $deterioro121_150 + $deterioro151_180 + $deterioro181_210 + $deterioro211_240 + $deterioro241_270 + $deterioro271_300 + $deterioro301_330 + $deterioro331_360;
?>
<tr> <td></td><td>TOTALES </td> <td align="center"><?php echo number_format($totalvalork ,0,',','.') ?></td> <td align="center"><?php echo number_format($totalfinanciacion ,0,',','.') ?></td> <td align="center"><?php echo number_format($totalclientes ,0,',','.') ?></td> <td align="center"><?php echo number_format($totaldeterioro ,0,',','.') ?></td>  </tr>

</tbody>
</table>
<p class="boton">
<button type="button" class="btnatras" onClick="carga(); location.href='conniff.php'">atras</button>
</p>
</article>
</article>
<?php require($r.'incluir/src/pie.php') ?>
</section>
</body>
</html>