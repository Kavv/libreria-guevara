<?php
$r = '../../../';
require($r.'incluir/connection.php');
require($r.'incluir/fpdf/fpdf.php');
require($r.'incluir/funciones.php');
class PDF extends FPDF{
	function Header(){
    	$this->SetFont('LucidaConsole','',8);
		$this->Cell(0,5,date('Y/m/d H:i:s'),0,1);
		$this->SetFont('LucidaConsole','',11);
		$this->Cell(0,5,'------------------------------------------------------------------------------------------------------------------------------------------------',0,1,'C');
		$this->Cell(0,5,'REPORTE RESUMEN DE MOROSOS DE  '.$row['empnombre'],0,1,'C');
		$this->Cell(0,5,'------------------------------------------------------------------------------------------------------------------------------------------------',0,1,'C');
    	$this->Ln(1);
	}

	function Footer(){
    	$this->SetY(-15);
    	$this->SetFont('LucidaConsole','',11);
		$this->Cell(0,5,'------------------------------------------------------------------------------------------------------------------------------------------------',0,1,'C');
		$this->SetFont('LucidaConsole','',8);
    	$this->Cell(0,5,'Pagina '.$this->PageNo().'/{nb}',0,0,'R');
	}
}

$pdf = new PDF('L','mm','Legal');
$pdf->AddFont('LucidaConsole','','lucon.php');
$pdf->AliasNbPages();
$pdf->AddPage();

$pdf->SetFont('LucidaConsole','',10);
$pdf->SetFillColor(255,255,255);
$pdf->Cell(45,5,'',0,0,'',true);
$pdf->Cell(10,5,'',0,0,'C',true);
$pdf->Cell(27,5,'',0,0,'C',true);
$pdf->SetFillColor(230,230,230);
$pdf->Cell(37,5,'1 - 30',1,0,'C',true);
$pdf->Cell(37,5,'61 - 90',1,0,'C',true);
$pdf->Cell(37,5,'VALOR',1,0,'C',true);
$pdf->Cell(37,5,'91 - 120',1,0,'C',true);
$pdf->Cell(37,5,'121 - MAS',1,0,'C',true);
$pdf->Cell(37,5,'MORA',1,0,'C',true);
$pdf->Cell(30,5,'PORCEN',1,1,'C',true);

$pdf->SetFillColor(230,230,230);
$pdf->Cell(45,5,'PROMOTOR',1,0,'',true);
$pdf->Cell(10,5,'CUEN',1,0,'C',true);
$pdf->Cell(27,5,'VALOR',1,0,'C',true);
$pdf->Cell(10,5,'CAN',1,0,'C',true);
$pdf->Cell(27,5,'VALOR',1,0,'C',true);
$pdf->Cell(10,5,'CAN',1,0,'C',true);
$pdf->Cell(27,5,'VALOR',1,0,'C',true);
$pdf->Cell(10,5,'CAN',1,0,'C',true);
$pdf->Cell(27,5,'VALOR',1,0,'C',true);
$pdf->Cell(10,5,'CAN',1,0,'C',true);
$pdf->Cell(27,5,'VALOR',1,0,'C',true);
$pdf->Cell(10,5,'CAN',1,0,'C',true);
$pdf->Cell(27,5,'VALOR',1,0,'C',true);
$pdf->Cell(10,5,'CAN',1,0,'C',true);
$pdf->Cell(27,5,'VALOR',1,0,'C',true);
$pdf->Cell(15,5,'% C',1,0,'C',true);
$pdf->Cell(15,5,'% V',1,1,'C',true);
		
$qry = $db->query("SELECT * FROM usuarios WHERE usupromotor = '1'");
while($row = $qry->fetch(PDO::FETCH_ASSOC)){
	$tlcartera = 0;
	$mora30 = 0;
	$ttl30 = 0;
	$mora60 = 0;
	$ttl60 = 0;
	$mora90 = 0;
	$ttl90 = 0;
	$mora120 = 0;
	$ttl120 = 0;
	$moramas = 0;
	$ttlmas = 0;
	$ttlmora = 0;
	$ttlvalor = 0;
	$num = 0;
	$qryemp = $db->query("SELECT * FROM empresas");
	while($rowemp = $qryemp->fetch(PDO::FETCH_ASSOC)){
		$qry2 = $db->query("SELECT * FROM carteras WHERE carempresa = ".$rowemp['empid']." AND carpromotor = '".$row['usuid']."' AND (carestado = 'ACTIVA' OR carestado = 'CASTIGADA' OR carestado = 'RECOLECCION' OR carestado = 'PENDIENTE' OR carestado = 'MOROSO' OR carestado = 'RECOLECCION RAZONADA')");
		$num = $num + $qry2->rowCount();
		while($row2 = $qry2->fetch(PDO::FETCH_ASSOC)){
			$tlcartera = $tlcartera + $row2['carsaldo'];
			$qry3 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = ".$rowemp['empid']." AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota ASC");
			$num2 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = ".$rowemp['empid']." AND dcafactura = '".$row2['carfactura']."' AND dcafecha < now()  AND dcaestado = 'ACTIVA'")->rowCount();
			while($row3 = $qry3->fetch(PDO::FETCH_ASSOC)){
				if(date('Y-m-d') > $row3['dcafecha'] && $row3['dcaestado'] == 'ACTIVA'){
					$mora = fechaDif($row3['dcafecha'],date('Y-m-d'));
					if($mora > 120){
						$moramas = $moramas + 1;
						$ttlmas = $ttlmas + ($num2 * $row2['carcuota']);
						break;
					}elseif($mora > 90){
						$mora120 = $mora120 + 1;
						$ttl120 = $ttl120 + ($num2 * $row2['carcuota']);
						break;
					}elseif($mora > 60){
						$mora90 = $mora90 + 1;
						$ttl90 = $ttl90 + ($num2 * $row2['carcuota']);
						break;
					}elseif($mora > 30){
						$mora60 = $mora60 + 1;
						$ttl60 = $ttl60 + ($num2 * $row2['carcuota']);
						break;
					}elseif($mora > 0){
						$mora30 = $mora30 + 1;
						$ttl30 = $ttl30 + ($num2 * $row2['carcuota']);
						break;
					}
				}
			}
		}
		$ttlmora = $mora30 + $mora60 + $mora90 + $mora120 + $moramas;
		$ttlvalor = $ttl30 + $ttl60 + $ttl90 + $ttl120 + $ttlmas;
		if($ttlmora != 0) @$porcnta = round(($ttlmora / $num) * 100);
		else $porcnta = 0;
		if($ttlvalor != 0) @$porvalor = round(($ttlvalor / $tlcartera) * 100);
		else $porvalor = 0;
	}		
	$pdf->SetFont('LucidaConsole','',8);
	if($i%2 == 0) $x = 255;
	else $x = 235;
	$pdf->SetFillColor($x,$x,$x);
	$pdf->Cell(45,5,$row['usunombre'],1,0,'',true);
	$pdf->Cell(10,5,$num,1,0,'C',true);
	$pdf->Cell(27,5,number_format($tlcartera,0,',','.'),1,0,'R',true);
	$pdf->Cell(10,5,$mora30,1,0,'C',true);
	$pdf->Cell(27,5,number_format($ttl30,0,',','.'),1,0,'R',true);
	$pdf->Cell(10,5,$mora60,1,0,'C',true);
	$pdf->Cell(27,5,number_format($ttl60,0,',','.'),1,0,'R',true);
	$pdf->Cell(10,5,$mora90,1,0,'C',true);
	$pdf->Cell(27,5,number_format($ttl90,0,',','.'),1,0,'R',true);
	$pdf->Cell(10,5,$mora120,1,0,'C',true);
	$pdf->Cell(27,5,number_format($ttl120,0,',','.'),1,0,'R',true);
	$pdf->Cell(10,5,$moramas,1,0,'C',true);
	$pdf->Cell(27,5,number_format($ttlmas,0,',','.'),1,0,'R',true);
	$pdf->Cell(10,5,$ttlmora,1,0,'C',true);
	$pdf->Cell(27,5,number_format($ttlvalor,0,',','.'),1,0,'R',true);
	$pdf->Cell(15,5,$porcnta.' %',1,0,'C',true);
	$pdf->Cell(15,5,$porvalor.' %',1,1,'C',true);
	$i++;	
	$numcarfn = $numcarfn + $num;
	$tlcarterafn = $tlcarterafn + $tlcartera;
	$mora30fn = $mora30fn + $mora30;
	$ttl30fn = $ttl30fn + $ttl30;
	$mora60fn = $mora60fn + $mora60;
	$ttl60fn = $ttl60fn + $ttl60;
	$mora90fn = $mora90fn + $mora90;
	$ttl90fn = $ttl90fn + $ttl90;
	$mora120fn = $mora120fn + $mora120;
	$ttl120fn = $ttl120fn + $ttl120;
	$moramasfn = $moramasfn + $moramas;
	$ttlmasfn = $ttlmasfn + $ttlmas;
	$ttlmorafn = $ttlmorafn + $ttlmora;
	$ttlvalorfn = $ttlvalorfn + $ttlvalor;
	if($ttlmorafn != 0) $porcntafn = round(($ttlmorafn / $numcarfn) * 100);
	else $porcntafn = 0;
	if($ttlvalorfn != 0) $porvalorfn = round(($ttlvalorfn / $tlcarterafn) * 100);
	else $porvalorfn = 0;
}
$pdf->SetFillColor(255,255,255);
$pdf->Cell(45,5,'',0,0,'',true);
$pdf->SetFillColor(220,220,220);
$pdf->Cell(10,5,$numcarfn,1,0,'C',true);
$pdf->Cell(27,5,number_format($tlcarterafn,0,',','.'),1,0,'R',true);
$pdf->Cell(10,5,$mora30fn,1,0,'C',true);
$pdf->Cell(27,5,number_format($ttl30fn,0,',','.'),1,0,'R',true);
$pdf->Cell(10,5,$mora60fn,1,0,'C',true);
$pdf->Cell(27,5,number_format($ttl60fn,0,',','.'),1,0,'R',true);
$pdf->Cell(10,5,$mora90fn,1,0,'C',true);
$pdf->Cell(27,5,number_format($ttl90fn,0,',','.'),1,0,'R',true);
$pdf->Cell(10,5,$mora120fn,1,0,'C',true);
$pdf->Cell(27,5,number_format($ttl120fn,0,',','.'),1,0,'R',true);
$pdf->Cell(10,5,$moramasfn,1,0,'C',true);
$pdf->Cell(27,5,number_format($ttlmasfn,0,',','.'),1,0,'R',true);
$pdf->Cell(10,5,$ttlmorafn,1,0,'C',true);
$pdf->Cell(27,5,number_format($ttlvalorfn,0,',','.'),1,0,'R',true);
$pdf->Cell(15,5,$porcntafn.' %',1,0,'C',true);
$pdf->Cell(15,5,$porvalorfn.' %',1,1,'C',true);
$pdf->Output('morosidad.pdf','d');
?>