<?php
$r = '../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
require($r.'incluir/funciones.php');
$fecha = $_POST['fecha'];
$promotor = $_POST['promotor'];
$filtro = 'promotor='.$promotor.'&fecha='.$fecha;
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
	<?php 
		require($r . 'incluir/src/head.php');
		require($r . 'incluir/src/head-form.php');
	?>	

<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('.tabla').dataTable({
		'bJQueryUI': true,
		'bStateSave': true,
		'bSort': false,
		'sPaginationType': 'full_numbers',
		'oLanguage': {
			'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
		},
	});
	$('.btnatras').button({ icons: { primary: 'ui-icon ui-icon-arrowthick-1-w' }});
});
</script>
</head>
<body>
<?php require($r.'incluir/src/login.php') ?>
<section id="principal">
<?php require($r.'incluir/src/cabeza.php') ?>
<?php require($r.'incluir/src/menu.php') ?>
<article id="cuerpo">
<article class="mapa">
<a href="#">Principal</a><div class="mapa_div"></div><a href="#">Cartera</a><div class="mapa_div"></div><a class="current">Reportes</a>
</article>
<article id="contenido">
<h2>Reporte detallado morosos</h2>
<div class="reporte">
<a href="pdfresmoro.php?<?php echo $filtro ?>" onClick="carga()"><img src="<?php echo $r ?>imagenes/iconos/pdf.png" title="pdf" /></a> <a href="exceldetmoro.php?<?php echo $filtro ?>"><img src="<?php echo $r ?>imagenes/iconos/excel.png" title="csv" /></a>
</div>
<?php
$row = $db->query("SELECT * FROM usuarios WHERE usuid = '$promotor'")->fetch(PDO::FETCH_ASSOC);
?>
<h2><?php echo $row['usunombre'] ?></h2>
<br />
<h3><b>RANGO: Al dia</b></h3><br />
<table class="tabla">
<thead>
<tr>
<th>Empresa</th>
<th>cuenta</th>
<th>Cliente</th>
<th>C. pacta.</th>
<th>Valor cuota</th>
<th>D. mora</th>
<th>C. mora</th>
<th>C. pend.</th>
<th>Vlr. mora</th>
<th>Saldo actual</th>	
<th>Ult. comp.</th>
</tr>
</thead>
<tbody>
<?php
$qry2 = $db->query("SELECT * FROM (carteras INNER JOIN clientes ON carcliente = cliid) INNER JOIN empresas ON carempresa = empid WHERE carpromotor = '$promotor' AND (carestado = 'ACTIVA' OR carestado = 'CASTIGADA' OR carestado = 'RECOLECCION' OR carestado = 'PENDIENTE' OR carestado = 'MOROSO' OR carestado = 'RECOLECCION RAZONADA')");
$num2 = $qry2->rowCount();
$cta1 = 0;
$vlr1 = 0;
$saldo1 = 0;
while($row2 = $qry2->fetch(PDO::FETCH_ASSOC)){
	$qry3 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' AND dcafecha < '$fecha' AND dcaestado = 'ACTIVA' ORDER BY dcacuota DESC");
	$row3 = $qry3->fetch(PDO::FETCH_ASSOC);
	$fpago = $row3['dcafepag'];
	$ncmora = $qry->RowCount();
	$qry4 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota ASC");
	$ncmora = 0;
	$ncpend = 0;
	while($row4 = $qry4->fetch(PDO::FETCH_ASSOC)){	
		if($fecha <= $row4['dcafecha'] && $row4['dcaestado'] == 'ACTIVA')
			$ncmora = 0;
			$ncpend = $ncpend + 1;
	}
	$qry5 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota ASC");
	while($row5 = $qry5->fetch(PDO::FETCH_ASSOC)){
		if($fecha <= $row5['dcafecha'] && $row5['dcaestado'] == 'ACTIVA'){
			echo '<tr><td title="'.$row2['empid'].'">'.$row2['empnombre'].'</td><td align="center">'.$row2['carfactura'].'</td><td>'.$row2['clinombre'].' '.$row2['clinom2'].' '.$row2['cliape1'].' '.$row2['cliape2'].'</td><td align="center">'.$row2['carncuota'].'</td><td align="right">'.number_format($row2['carcuota'],0,',','.').'</td><td align="center">'.$mora.'</td><td align="center">'.$ncmora.'</td><td align="center">'.$ncpend.'</td><td align="right">'.number_format($ncmora * $row2['carcuota'],0,',','.').'</td><td align="right">'.number_format($row2['carsaldo'],0,',','.').'</td><td align="center">'.$fpago.'</td></tr>';
			$cta1 = $cta1 + 1;
			$vlr1 = $vlr1 + ($ncmora * $row2['carcuota']);
			$saldo1 = $saldo1 + $row2['carsaldo'];
			break;
		}	
	}
}
?>
</tbody>
<tfoot>
<tr><td></td><td align="center" bgcolor="#D1CFCF"><?php echo $cta1 ?></td><td colspan="6"></td><td align="right" bgcolor="#D1CFCF"><?php echo number_format($vlr1,0,',','.') ?></td><td align="right" bgcolor="#D1CFCF"><?php echo number_format($saldo1,0,',','.') ?></td><td></td></tr>
</tfoot>
</table>
<br />
<h3><b>RANGO: 1 - 30</b></h3><br />
<table class="tabla">
<thead>
<tr>
<th>Empresa</th>
<th>cuenta</th>
<th>Cliente</th>
<th>C. pacta.</th>
<th>Valor cuota</th>
<th>D. mora</th>
<th>C. mora</th>
<th>C. pend.</th>
<th>Vlr. mora</th>
<th>Saldo actual</th>	
<th>Ult. pago</th>
</tr>
</thead>
<tbody>
<?php
$qry2 = $db->query("SELECT * FROM (carteras INNER JOIN clientes ON carcliente = cliid) INNER JOIN empresas ON carempresa = empid WHERE carpromotor = '$promotor' AND (carestado = 'ACTIVA' OR carestado = 'CASTIGADA' OR carestado = 'RECOLECCION' OR carestado = 'PENDIENTE' OR carestado = 'MOROSO' OR carestado = 'RECOLECCION RAZONADA')");
$num2 = $qry2->rowCount();
$cta1 = 0;
$vlr1 = 0;
$saldo1 = 0;
while($row2 = $qry2->fetch(PDO::FETCH_ASSOC)){
	$qry3 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota DESC");
	while($row3 = $qry3->fetch(PDO::FETCH_ASSOC)){	
		if($row3['dcafepag'] != ''){
			$ultpago = $row3['dcafepag'];
			break;
		}
	}
	$qry4 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota ASC");
	$ncmora = 0;
	$ncpend = 0;
	while($row4 = $qry4->fetch(PDO::FETCH_ASSOC)){	
		if($fecha > $row4['dcafecha'] && $row4['dcaestado'] == 'ACTIVA')
			$ncmora = $ncmora + 1;
		if($row4['dcaestado'] == 'ACTIVA')
			$ncpend = $ncpend + 1;
	}
	$qry5 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota ASC");
	while($row5 = $qry5->fetch(PDO::FETCH_ASSOC)){
		if($fecha > $row5['dcafecha'] && $row5['dcaestado'] == 'ACTIVA'){
			$mora = fechaDif($row5['dcafecha'],$fecha);
			if($mora > 0 && $mora < 31){
				echo '<tr><td title="'.$row2['empid'].'">'.$row2['empnombre'].'</td><td align="center">'.$row2['carfactura'].'</td><td>'.$row2['clinombre'].' '.$row2['clinom2'].' '.$row2['cliape1'].' '.$row2['cliape2'].'</td><td align="center">'.$row2['carncuota'].'</td><td align="right">'.number_format($row2['carcuota'],0,',','.').'</td><td align="center">'.$mora.'</td><td align="center">'.$ncmora.'</td><td align="center">'.$ncpend.'</td><td align="right">'.number_format($ncmora * $row2['carcuota'],0,',','.').'</td><td align="right">'.number_format($row2['carsaldo'],0,',','.').'</td><td align="center">'.$ultpago.'</td></tr>';
				$cta1 = $cta1 + 1;
				$vlr1 = $vlr1 + ($ncmora * $row2['carcuota']);
				$saldo1 = $saldo1 + $row2['carsaldo'];
			}
			break;
		}	
	}
}
?>
</tbody>
<tfoot>
<tr><td></td><td align="center" bgcolor="#D1CFCF"><?php echo $cta1 ?></td><td colspan="6"></td><td align="right" bgcolor="#D1CFCF"><?php echo number_format($vlr1,0,',','.') ?></td><td align="right" bgcolor="#D1CFCF"><?php echo number_format($saldo1,0,',','.') ?></td><td></td></tr>
</tfoot>
</table>
<br />
<h3><b>RANGO: 31 - 60</b></h3><br />
<table class="tabla">
<thead>
<tr>
<th>Empresa</th>
<th>cuenta</th>
<th>Cliente</th>
<th>C. pacta.</th>
<th>Valor cuota</th>
<th>D. mora</th>
<th>C. mora</th>
<th>C. pend.</th>
<th>Vlr. mora</th>
<th>Saldo actual</th>	
<th>Ult. pago</th>
</tr>
</thead>
<tbody>
<?php
$qry2 = $db->query("SELECT * FROM (carteras INNER JOIN clientes ON carcliente = cliid) INNER JOIN empresas ON carempresa = empid WHERE carpromotor = '$promotor' AND (carestado = 'ACTIVA' OR carestado = 'CASTIGADA' OR carestado = 'RECOLECCION' OR carestado = 'PENDIENTE' OR carestado = 'MOROSO' OR carestado = 'RECOLECCION RAZONADA')");
$num2 = $qry2->rowCount();
$cta1 = 0;
$vlr1 = 0;
$saldo1 = 0;
while($row2 = $qry2->fetch(PDO::FETCH_ASSOC)){
	$qry3 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota DESC");
	while($row3 = $qry3->fetch(PDO::FETCH_ASSOC)){	
		if($row3['dcafepag'] != ''){
			$ultpago = $row3['dcafepag'];
			break;
		}
	}
	$qry4 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota ASC");
	$ncmora = 0;
	$ncpend = 0;
	while($row4 = $qry4->fetch(PDO::FETCH_ASSOC)){	
		if($fecha > $row4['dcafecha'] && $row4['dcaestado'] == 'ACTIVA')
			$ncmora = $ncmora + 1;
		if($row4['dcaestado'] == 'ACTIVA')
			$ncpend = $ncpend + 1;
	}
	$qry5 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota ASC");
	while($row5 = $qry5->fetch(PDO::FETCH_ASSOC)){
		if($fecha > $row5['dcafecha'] && $row5['dcaestado'] == 'ACTIVA'){
			$mora = fechaDif($row5['dcafecha'],$fecha);
			if($mora > 30 && $mora < 61){
				echo '<tr><td title="'.$row2['empid'].'">'.$row2['empnombre'].'</td><td align="center">'.$row2['carfactura'].'</td><td>'.$row2['clinombre'].' '.$row2['clinom2'].' '.$row2['cliape1'].' '.$row2['cliape2'].'</td><td align="center">'.$row2['carncuota'].'</td><td align="right">'.number_format($row2['carcuota'],0,',','.').'</td><td align="center">'.$mora.'</td><td align="center">'.$ncmora.'</td><td align="center">'.$ncpend.'</td><td align="right">'.number_format($ncmora * $row2['carcuota'],0,',','.').'</td><td align="right">'.number_format($row2['carsaldo'],0,',','.').'</td><td align="center">'.$ultpago.'</td></tr>';
				$cta1 = $cta1 + 1;
				$vlr1 = $vlr1 + ($ncmora * $row2['carcuota']);
				$saldo1 = $saldo1 + $row2['carsaldo'];
			}
			break;
		}	
	}
}
?>
</tbody>
<tfoot>
<tr><td></td><td align="center" bgcolor="#D1CFCF"><?php echo $cta1 ?></td><td colspan="6"></td><td align="right" bgcolor="#D1CFCF"><?php echo number_format($vlr1,0,',','.') ?></td><td align="right" bgcolor="#D1CFCF"><?php echo number_format($saldo1,0,',','.') ?></td><td></td></tr>
</tfoot>
</table>
<br />
<h3><b>RANGO: 61 - 90</b></h3><br />
<table class="tabla">
<thead>
<tr>
<th>Empresa</th>
<th>cuenta</th>
<th>Cliente</th>
<th>C. pacta.</th>
<th>Valor cuota</th>
<th>D. mora</th>
<th>C. mora</th>
<th>C. pend.</th>
<th>Vlr. mora</th>
<th>Saldo actual</th>	
<th>Ult. pago</th>
</tr>
</thead>
<tbody>
<?php
$qry2 = $db->query("SELECT * FROM (carteras INNER JOIN clientes ON carcliente = cliid) INNER JOIN empresas ON carempresa = empid WHERE carpromotor = '$promotor' AND (carestado = 'ACTIVA' OR carestado = 'CASTIGADA' OR carestado = 'RECOLECCION' OR carestado = 'PENDIENTE' OR carestado = 'MOROSO' OR carestado = 'RECOLECCION RAZONADA')");
$num2 = $qry2->rowCount();
$cta1 = 0;
$vlr1 = 0;
$saldo1 = 0;
while($row2 = $qry2->fetch(PDO::FETCH_ASSOC)){
	$qry3 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota DESC");
	while($row3 = $qry3->fetch(PDO::FETCH_ASSOC)){	
		if($row3['dcafepag'] != ''){
			$ultpago = $row3['dcafepag'];
			break;
		}
	}
	$qry4 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota ASC");
	$ncmora = 0;
	$ncpend = 0;
	while($row4 = $qry4->fetch(PDO::FETCH_ASSOC)){	
		if($fecha > $row4['dcafecha'] && $row4['dcaestado'] == 'ACTIVA')
			$ncmora = $ncmora + 1;
		if($row4['dcaestado'] == 'ACTIVA')
			$ncpend = $ncpend + 1;
	}
	$qry5 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota ASC");
	while($row5 = $qry5->fetch(PDO::FETCH_ASSOC)){
		if($fecha > $row5['dcafecha'] && $row5['dcaestado'] == 'ACTIVA'){
			$mora = fechaDif($row5['dcafecha'],$fecha);
			if($mora > 60 && $mora < 91){
				echo '<tr><td title="'.$row2['empid'].'">'.$row2['empnombre'].'</td><td align="center">'.$row2['carfactura'].'</td><td>'.$row2['clinombre'].' '.$row2['clinom2'].' '.$row2['cliape1'].' '.$row2['cliape2'].'</td><td align="center">'.$row2['carncuota'].'</td><td align="right">'.number_format($row2['carcuota'],0,',','.').'</td><td align="center">'.$mora.'</td><td align="center">'.$ncmora.'</td><td align="center">'.$ncpend.'</td><td align="right">'.number_format($ncmora * $row2['carcuota'],0,',','.').'</td><td align="right">'.number_format($row2['carsaldo'],0,',','.').'</td><td align="center">'.$ultpago.'</td></tr>';
				$cta1 = $cta1 + 1;
				$vlr1 = $vlr1 + ($ncmora * $row2['carcuota']);
				$saldo1 = $saldo1 + $row2['carsaldo'];
			}
			break;
		}	
	}
}
?>
</tbody>
<tfoot>
<tr><td></td><td align="center" bgcolor="#D1CFCF"><?php echo $cta1 ?></td><td colspan="6"></td><td align="right" bgcolor="#D1CFCF"><?php echo number_format($vlr1,0,',','.') ?></td><td align="right" bgcolor="#D1CFCF"><?php echo number_format($saldo1,0,',','.') ?></td><td></td></tr>
</tfoot>
</table>
<br />
<h3><b>RANGO: 91 - 120</b></h3><br />
<table class="tabla">
<thead>
<tr>
<th>Empresa</th>
<th>cuenta</th>
<th>Cliente</th>
<th>C. pacta.</th>
<th>Valor cuota</th>
<th>D. mora</th>
<th>C. mora</th>
<th>C. pend.</th>
<th>Vlr. mora</th>
<th>Saldo actual</th>	
<th>Ult. pago</th>
</tr>
</thead>
<tbody>
<?php
$qry2 = $db->query("SELECT * FROM (carteras INNER JOIN clientes ON carcliente = cliid) INNER JOIN empresas ON carempresa = empid WHERE carpromotor = '$promotor' AND (carestado = 'ACTIVA' OR carestado = 'CASTIGADA' OR carestado = 'RECOLECCION' OR carestado = 'PENDIENTE' OR carestado = 'MOROSO' OR carestado = 'RECOLECCION RAZONADA')");
$num2 = $qry2->rowCount();
$cta1 = 0;
$vlr1 = 0;
$saldo1 = 0;
while($row2 = $qry2->fetch(PDO::FETCH_ASSOC)){
	$qry3 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota DESC");
	while($row3 = $qry3->fetch(PDO::FETCH_ASSOC)){	
		if($row3['dcafepag'] != ''){
			$ultpago = $row3['dcafepag'];
			break;
		}
	}
	$qry4 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota ASC");
	$ncmora = 0;
	$ncpend = 0;
	while($row4 = $qry4->fetch(PDO::FETCH_ASSOC)){	
		if($fecha > $row4['dcafecha'] && $row4['dcaestado'] == 'ACTIVA')
			$ncmora = $ncmora + 1;
		if($row4['dcaestado'] == 'ACTIVA')
			$ncpend = $ncpend + 1;
	}
	$qry5 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota ASC");
	while($row5 = $qry5->fetch(PDO::FETCH_ASSOC)){
		if($fecha > $row5['dcafecha'] && $row5['dcaestado'] == 'ACTIVA'){
			$mora = fechaDif($row5['dcafecha'],$fecha);
			if($mora > 90 && $mora < 121){
				echo '<tr><td title="'.$row2['empid'].'">'.$row2['empnombre'].'</td><td align="center">'.$row2['carfactura'].'</td><td>'.$row2['clinombre'].' '.$row2['clinom2'].' '.$row2['cliape1'].' '.$row2['cliape2'].'</td><td align="center">'.$row2['carncuota'].'</td><td align="right">'.number_format($row2['carcuota'],0,',','.').'</td><td align="center">'.$mora.'</td><td align="center">'.$ncmora.'</td><td align="center">'.$ncpend.'</td><td align="right">'.number_format($ncmora * $row2['carcuota'],0,',','.').'</td><td align="right">'.number_format($row2['carsaldo'],0,',','.').'</td><td align="center">'.$ultpago.'</td></tr>';
				$cta1 = $cta1 + 1;
				$vlr1 = $vlr1 + ($ncmora * $row2['carcuota']);
				$saldo1 = $saldo1 + $row2['carsaldo'];
			}
			break;
		}	
	}
}
?>
</tbody>
<tfoot>
<tr><td></td><td align="center" bgcolor="#D1CFCF"><?php echo $cta1 ?></td><td colspan="6"></td><td align="right" bgcolor="#D1CFCF"><?php echo number_format($vlr1,0,',','.') ?></td><td align="right" bgcolor="#D1CFCF"><?php echo number_format($saldo1,0,',','.') ?></td><td></td></tr>
</tfoot>
</table>
<br />
<h3><b>RANGO: 121 - 150</b></h3><br />
<table class="tabla">
<thead>
<tr>
<th>Empresa</th>
<th>cuenta</th>
<th>Cliente</th>
<th>C. pacta.</th>
<th>Valor cuota</th>
<th>D. mora</th>
<th>C. mora</th>
<th>C. pend.</th>
<th>Vlr. mora</th>
<th>Saldo actual</th>	
<th>Ult. pago</th>
</tr>
</thead>
<tbody>
<?php
$qry2 = $db->query("SELECT * FROM (carteras INNER JOIN clientes ON carcliente = cliid) INNER JOIN empresas ON carempresa = empid WHERE carpromotor = '$promotor' AND (carestado = 'ACTIVA' OR carestado = 'CASTIGADA' OR carestado = 'RECOLECCION' OR carestado = 'PENDIENTE' OR carestado = 'MOROSO' OR carestado = 'RECOLECCION RAZONADA')");
$num2 = $qry2->rowCount();
$cta1 = 0;
$vlr1 = 0;
$saldo1 = 0;
while($row2 = $qry2->fetch(PDO::FETCH_ASSOC)){
	$qry3 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota DESC");
	while($row3 = $qry3->fetch(PDO::FETCH_ASSOC)){	
		if($row3['dcafepag'] != ''){
			$ultpago = $row3['dcafepag'];
			break;
		}
	}
	$qry4 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota ASC");
	$ncmora = 0;
	$ncpend = 0;
	while($row4 = $qry4->fetch(PDO::FETCH_ASSOC)){	
		if($fecha > $row4['dcafecha'] && $row4['dcaestado'] == 'ACTIVA')
			$ncmora = $ncmora + 1;
		if($row4['dcaestado'] == 'ACTIVA')
			$ncpend = $ncpend + 1;
	}
	$qry5 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota ASC");
	while($row5 = $qry5->fetch(PDO::FETCH_ASSOC)){
		if($fecha > $row5['dcafecha'] && $row5['dcaestado'] == 'ACTIVA'){
			$mora = fechaDif($row5['dcafecha'],$fecha);
			if($mora > 120 && $mora < 151){
				echo '<tr><td title="'.$row2['empid'].'">'.$row2['empnombre'].'</td><td align="center">'.$row2['carfactura'].'</td><td>'.$row2['clinombre'].' '.$row2['clinom2'].' '.$row2['cliape1'].' '.$row2['cliape2'].'</td><td align="center">'.$row2['carncuota'].'</td><td align="right">'.number_format($row2['carcuota'],0,',','.').'</td><td align="center">'.$mora.'</td><td align="center">'.$ncmora.'</td><td align="center">'.$ncpend.'</td><td align="right">'.number_format($ncmora * $row2['carcuota'],0,',','.').'</td><td align="right">'.number_format($row2['carsaldo'],0,',','.').'</td><td align="center">'.$ultpago.'</td></tr>';
				$cta1 = $cta1 + 1;
				$vlr1 = $vlr1 + ($ncmora * $row2['carcuota']);
				$saldo1 = $saldo1 + $row2['carsaldo'];
			}
			break;
		}	
	}
}
?>
</tbody>
<tfoot>
<tr><td></td><td align="center" bgcolor="#D1CFCF"><?php echo $cta1 ?></td><td colspan="6"></td><td align="right" bgcolor="#D1CFCF"><?php echo number_format($vlr1,0,',','.') ?></td><td align="right" bgcolor="#D1CFCF"><?php echo number_format($saldo1,0,',','.') ?></td><td></td></tr>
</tfoot>
</table>
<br />
<h3><b>RANGO: 151 - 180</b></h3><br />
<table class="tabla">
<thead>
<tr>
<th>Empresa</th>
<th>cuenta</th>
<th>Cliente</th>
<th>C. pacta.</th>
<th>Valor cuota</th>
<th>D. mora</th>
<th>C. mora</th>
<th>C. pend.</th>
<th>Vlr. mora</th>
<th>Saldo actual</th>	
<th>Ult. pago</th>
</tr>
</thead>
<tbody>
<?php
$qry2 = $db->query("SELECT * FROM (carteras INNER JOIN clientes ON carcliente = cliid) INNER JOIN empresas ON carempresa = empid WHERE carpromotor = '$promotor' AND (carestado = 'ACTIVA' OR carestado = 'CASTIGADA' OR carestado = 'RECOLECCION' OR carestado = 'PENDIENTE' OR carestado = 'MOROSO' OR carestado = 'RECOLECCION RAZONADA')");
$num2 = $qry2->rowCount();
$cta1 = 0;
$vlr1 = 0;
$saldo1 = 0;
while($row2 = $qry2->fetch(PDO::FETCH_ASSOC)){
	$qry3 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota DESC");
	while($row3 = $qry3->fetch(PDO::FETCH_ASSOC)){	
		if($row3['dcafepag'] != ''){
			$ultpago = $row3['dcafepag'];
			break;
		}
	}
	$qry4 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota ASC");
	$ncmora = 0;
	$ncpend = 0;
	while($row4 = $qry4->fetch(PDO::FETCH_ASSOC)){	
		if($fecha > $row4['dcafecha'] && $row4['dcaestado'] == 'ACTIVA')
			$ncmora = $ncmora + 1;
		if($row4['dcaestado'] == 'ACTIVA')
			$ncpend = $ncpend + 1;
	}
	$qry5 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota ASC");
	while($row5 = $qry5->fetch(PDO::FETCH_ASSOC)){
		if($fecha > $row5['dcafecha'] && $row5['dcaestado'] == 'ACTIVA'){
			$mora = fechaDif($row5['dcafecha'],$fecha);
			if($mora > 150 && $mora < 181){
				echo '<tr><td title="'.$row2['empid'].'">'.$row2['empnombre'].'</td><td align="center">'.$row2['carfactura'].'</td><td>'.$row2['clinombre'].' '.$row2['clinom2'].' '.$row2['cliape1'].' '.$row2['cliape2'].'</td><td align="center">'.$row2['carncuota'].'</td><td align="right">'.number_format($row2['carcuota'],0,',','.').'</td><td align="center">'.$mora.'</td><td align="center">'.$ncmora.'</td><td align="center">'.$ncpend.'</td><td align="right">'.number_format($ncmora * $row2['carcuota'],0,',','.').'</td><td align="right">'.number_format($row2['carsaldo'],0,',','.').'</td><td align="center">'.$ultpago.'</td></tr>';
				$cta1 = $cta1 + 1;
				$vlr1 = $vlr1 + ($ncmora * $row2['carcuota']);
				$saldo1 = $saldo1 + $row2['carsaldo'];
			}
			break;
		}	
	}
}
?>
</tbody>
<tfoot>
<tr><td></td><td align="center" bgcolor="#D1CFCF"><?php echo $cta1 ?></td><td colspan="6"></td><td align="right" bgcolor="#D1CFCF"><?php echo number_format($vlr1,0,',','.') ?></td><td align="right" bgcolor="#D1CFCF"><?php echo number_format($saldo1,0,',','.') ?></td><td></td></tr>
</tfoot>
</table>
<br />
<h3><b>RANGO: 181 - mas</b></h3><br />
<table class="tabla">
<thead>
<tr>
<th>Empresa</th>
<th>cuenta</th>
<th>Cliente</th>
<th>C. pacta.</th>
<th>Valor cuota</th>
<th>D. mora</th>
<th>C. mora</th>
<th>C. pend.</th>
<th>Vlr. mora</th>
<th>Saldo actual</th>	
<th>Ult. pago</th>
</tr>
</thead>
<tbody>
<?php
$qry2 = $db->query("SELECT * FROM (carteras INNER JOIN clientes ON carcliente = cliid) INNER JOIN empresas ON carempresa = empid WHERE carpromotor = '$promotor' AND (carestado = 'ACTIVA' OR carestado = 'CASTIGADA' OR carestado = 'RECOLECCION' OR carestado = 'PENDIENTE' OR carestado = 'MOROSO' OR carestado = 'RECOLECCION RAZONADA')");
$num2 = $qry2->rowCount();
$cta1 = 0;
$vlr1 = 0;
$saldo1 = 0;
while($row2 = $qry2->fetch(PDO::FETCH_ASSOC)){
	$qry3 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota DESC");
	while($row3 = $qry3->fetch(PDO::FETCH_ASSOC)){	
		if($row3['dcafepag'] != ''){
			$ultpago = $row3['dcafepag'];
			break;
		}
	}
	$qry4 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota ASC");
	$ncmora = 0;
	$ncpend = 0;
	while($row4 = $qry4->fetch(PDO::FETCH_ASSOC)){	
		if($fecha > $row4['dcafecha'] && $row4['dcaestado'] == 'ACTIVA')
			$ncmora = $ncmora + 1;
		if($row4['dcaestado'] == 'ACTIVA')
			$ncpend = $ncpend + 1;
	}
	$qry5 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota ASC");
	while($row5 = $qry5->fetch(PDO::FETCH_ASSOC)){
		if($fecha > $row5['dcafecha'] && $row5['dcaestado'] == 'ACTIVA'){
			$mora = fechaDif($row5['dcafecha'],$fecha);
			if($mora > 180){
				echo '<tr><td title="'.$row2['empid'].'">'.$row2['empnombre'].'</td><td align="center">'.$row2['carfactura'].'</td><td>'.$row2['clinombre'].' '.$row2['clinom2'].' '.$row2['cliape1'].' '.$row2['cliape2'].'</td><td align="center">'.$row2['carncuota'].'</td><td align="right">'.number_format($row2['carcuota'],0,',','.').'</td><td align="center">'.$mora.'</td><td align="center">'.$ncmora.'</td><td align="center">'.$ncpend.'</td><td align="right">'.number_format($ncmora * $row2['carcuota'],0,',','.').'</td><td align="right">'.number_format($row2['carsaldo'],0,',','.').'</td><td align="center">'.$ultpago.'</td></tr>';
				$cta1 = $cta1 + 1;
				$vlr1 = $vlr1 + ($ncmora * $row2['carcuota']);
				$saldo1 = $saldo1 + $row2['carsaldo'];
			}
			break;
		}	
	}
}
?>
</tbody>
<tfoot>
<tr><td></td><td align="center" bgcolor="#D1CFCF"><?php echo $cta1 ?></td><td colspan="6"></td><td align="right" bgcolor="#D1CFCF"><?php echo number_format($vlr1,0,',','.') ?></td><td align="right" bgcolor="#D1CFCF"><?php echo number_format($saldo1,0,',','.') ?></td><td></td></tr>
</tfoot>
</table>
<p class="boton">
<button type="button" class="btnatras" onClick="carga(); location.href='conprevisto.php'">atras</button>
</p>
</article>
</article>
<?php require($r.'incluir/src/pie.php') ?>
</section>
</body>
</html>