<?php
$r = '../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
require($r.'incluir/funciones.php');


	$fecha1 = $_POST['fecha1'];
	$fecha2 = $_POST['fecha2'];


$filtro = "fecha1=".$fecha1."&fecha2=".$fecha2;

?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
	<?php 
		require($r . 'incluir/src/head.php');
		require($r . 'incluir/src/head-form.php');
	?>	

<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#tabla').dataTable({
		'bJQueryUI': true,
		'bStateSave': true,
		'bSort': false,
		'sPaginationType': 'full_numbers',
		'oLanguage': {
			'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
		},
	});
	$('.btnatras').button({ icons: { primary: 'ui-icon ui-icon-arrowthick-1-w' }});
});
</script>
</head>
<body>
<?php require($r.'incluir/src/login.php') ?>
<section id="principal">
<?php require($r.'incluir/src/cabeza.php') ?>
<?php require($r.'incluir/src/menu.php') ?>
<article id="cuerpo">
<article class="mapa">
<a href="#">Principal</a><div class="mapa_div"></div><a href="#">Cartera</a><div class="mapa_div"></div><a class="current">Reportes</a>
</article>
<article id="contenido">
<h2>Reporte calificaciones de productos desde <?php echo "$fecha1 hasta $fecha2"; ?></h2>
<div class="reporte">
<a href="pdfcalificacion.php?<?php echo $filtro ?>" onClick="carga()"><img src="<?php echo $r ?>imagenes/iconos/pdf.png" title="pdf" /></a> <a href=""><img src="<?php echo $r ?>imagenes/iconos/excel.png" title="csv" /></a>
</div>
<table id="tabla">
<thead>
<tr>
<th>Promotor</th>
<th>cuentas</th>
<th>Gestiones</th>
<th>Calificados</th>
<th>No Calificados</th>
<th title="Porcnetaje de Calificados">P.C</th>
<th title="Porcnetaje de NO Calificados">P.N.C</th>
<th>No Aplica</th>
<th>Insuficiente</th>
<th>Malo</th>
<th>Aceptable</th>
<th>Bueno</th>
<th>Excelente</th>
</tr>
</thead>
<tbody>
<?php
$qry = $db->query("SELECT * FROM usuarios WHERE usupromotor = '1'");
while($row = $qry->fetch(PDO::FETCH_ASSOC)){

	$cuentas = 0;
	$gestiones = 0;
	$calificados = 0;
	$nocalificados = 0;
	$porcentaje = 0;
	$noaplica = 0;
	$insuficiente = 0;
	$malo = 0;
	$aceptable = 0;
	$bueno = 0;
	$excelente = 0;

	$qry2 = $db->query("SELECT * FROM hiscarteras as T1 
	INNER JOIN carteras ON carfactura = hiscuenta AND carempresa = hisempresa
	INNER JOIN detCarteras ON dcafactura = hiscuenta AND dcaempresa = hisempresa
	WHERE (carestado = 'ACTIVA' or (dcafepag < '$fecha2'
	AND dcaestado = 'CANCELADO')) AND hisusuario = '".$row['usuid']."' AND DATE(hisfecha) BETWEEN '$fecha1' and '$fecha2'
	GROUP BY hiscuenta,hisempresa order by hiscuenta,hisid desc");
	$contgestiones = $qry2->rowCount();
	$qrycu = $db->query("SELECT * FROM hiscarteras as T1 
	INNER JOIN carteras ON carfactura = hiscuenta AND carempresa = hisempresa
	INNER JOIN detCarteras ON dcafactura = hiscuenta AND dcaempresa = hisempresa
	WHERE (carestado = 'ACTIVA' or (dcafepag < '$fecha2'
	AND dcaestado = 'CANCELADO')) AND hisusuario = '".$row['usuid']."' AND DATE(hisfecha) BETWEEN '$fecha1' and '$fecha2'
	GROUP BY hiscuenta,hisempresa order by hiscuenta,hisid desc");
	$contcuentas = $qrycu->rowCount();
	while($row2 = $qry2->fetch(PDO::FETCH_ASSOC)){
				
		if ($row2['hiscalificacion'] == 6 ){
			$noaplica = $noaplica + 1; 
		}
		if ($row2['hiscalificacion'] == 1){
			$insuficiente = $insuficiente + 1;
		}
		if ($row2['hiscalificacion'] == 2){
			$malo = $malo + 1;
		}
		if ($row2['hiscalificacion'] == 3){
			$aceptable = $aceptable + 1;
		}
		if ($row2['hiscalificacion'] == 4){
			$bueno = $bueno + 1;
		}
		if ($row2['hiscalificacion'] == 5){
			$excelente = $excelente + 1;
		}
		if($row2['hiscalificacion'] == NULL){
			$nocalificados = $nocalificados + 1;
		}
		if ($row2['hiscalificacion'] != NULL){
			$calificados = $calificados + 1;
		}
		
	}
	$gestiones = $gestiones + $contgestiones;
	$cuentas = $cuentas + $contcuentas;
	
	$porcentajecalificados = round((100 * $calificados) / $gestiones);
	$porcentajenocalificados = round((100 * $nocalificados) / $gestiones);

	
?>
<tr>
<td title="<?php echo $row['usuid'] ?>"><?php echo $row['usunombre'] ?></td>
<td align="center"><?php echo $cuentas ?></td>
<td align="center"><?php echo $gestiones ?></td>
<td align="center"><?php echo $calificados ?></td>
<td align="center"><?php echo $nocalificados ?></td>
<td align="center">% <?php echo $porcentajecalificados ?></td>
<td align="center">% <?php echo $porcentajenocalificados ?></td>
<td align="center"><?php echo $noaplica ?></td>
<td align="center"><?php echo $insuficiente ?></td>
<td align="center"><?php echo $malo ?></td>
<td align="center"><?php echo $aceptable ?></td>
<td align="center"><?php echo $bueno ?></td>
<td align="center"><?php echo $excelente ?></td>

</tr>
<?php
	$totalcuentas = $totalcuentas + $cuentas;
	$totalgestiones = $totalgestiones + $gestiones;
	$totalcalificados = $totalcalificados + $calificados;
	$totalnocalificados = $totalnocalificados + $nocalificados;
	$totalnoaplica = $totalnoaplica + $noaplica;
	$totalinsuficiente = $totalinsuficiente + $insuficiente;
	$totalmalo = $totalmalo + $malo;
	$totalaceptable = $totalaceptable + $aceptable;
	$totalbueno = $totalbueno + $bueno;
	$totalexcelente = $totalexcelente + $excelente;
}
?>
</tbody>
<tfoot>
<tr bgcolor="#D1CFCF">
<td></td>
<td align="center"><?php echo $totalcuentas ?></td>
<td align="center"><?php echo $totalgestiones ?></td>
<td align="center"><?php echo $totalcalificados ?></td>
<td align="center"><?php echo $totalnocalificados ?></td>
<td align="center"></td>
<td align="center"></td>
<td align="center"><?php echo $totalnoaplica ?></td>
<td align="center"><?php echo $totalinsuficiente ?></td>
<td align="center"><?php echo $totalmalo ?></td>
<td align="center"><?php echo $totalaceptable ?></td>
<td align="center"><?php echo $totalbueno ?></td>
<td align="center"><?php echo $totalexcelente ?></td>
</tr>
</tfoot>
</table>
<br>
<p class="boton">
<button type="button" class="btnatras" onClick="carga(); location.href = 'concalificacion.php'">Atras</button>
</p>
</article>
</article>
<?php require($r.'incluir/src/pie.php') ?>
</section>
</body>
</html>