<?php
$r = '../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');

$empresa = $_POST['empresa'];
$fecha1 = $_POST['fecha1'];
$fecha2 = $_POST['fecha2'];
$filtro = 'empresa='.$empresa.'&fecha1='.$fecha1.'&fecha2='.$fecha2;
$credito = 0;
$contado = 0;
$ctaini = 0;
$finantotal = 0;
$devoluciones = 0;
$ndebito = 0;
$ncredito = 0;
$cartotal = 0;
$carcapital = 0;
$carcastigada = 0;
$descapital = 0;

if($empresa != 'TODAS') {$qry = $db->query("SELECT * FROM empresas WHERE empid = $empresa"); // row empresa


/*AGREGADO POR MARIA DIAZ PARA DETERMINAR SI EL MES YA FUE CERRADO Y CON CUAL VALOR*/
$anno = substr($fecha2, 0, 4); 
$mes = substr($fecha2, 5, 2);  

$qrysaldo = $db->query("SELECT * FROM saldocartera WHERE saldocarteramesano = '".$mes."-".$anno."' AND saldocarteraempresa = $empresa");
$saldoAnterior = $qrysaldo->rowCount();

$sqlgeneral = "SELECT * FROM solicitudes INNER JOIN movimientos ON solfactura = movnumero  WHERE movempresa = '".$empresa."' AND movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'FV' AND movestado <> 'ANULADA'"; // qry general ventas bruto

$sqlventascredito = "SELECT * FROM solicitudes INNER JOIN movimientos ON solfactura = movnumero  WHERE movempresa = '".$empresa."' AND movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'FV' AND  solncuota > 1 AND movestado <> 'ANULADA' ;"; // qry ventas a credito
$sqlventascontado = "SELECT * FROM solicitudes INNER JOIN movimientos ON solfactura = movnumero  WHERE movempresa = '".$empresa."' AND movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'FV' AND  solncuota IN ('1','0') AND movestado <> 'ANULADA';"; // qry ventas de contado
$sqlfinanciado = "SELECT * FROM solicitudes INNER JOIN movimientos ON solfactura = movnumero  WHERE movempresa = '".$empresa."' AND movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'FV' AND  movfinan > 0 AND movestado <> 'ANULADA';"; // qry financiado

$sqlcuotasini =  "SELECT * FROM solicitudes INNER JOIN movimientos ON solfactura = movnumero  WHERE movempresa = '".$empresa."' AND movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'FV' AND  solcuota > 0 AND movestado = 'FACTURADO';"; // qry cuotas ini

/*
$sqlcarterafecha = "
SELECT * FROM  detcarteras D
INNER JOIN carteras on carfactura = D.dcafactura 
WHERE D.dcaempresa = '".$empresa."' 
AND D.dcafepag >= '".$fecha2."' 
AND D.dcafecha < '".$fecha2."' 
order by D.dcafactura, D.dcacuota desc ; 
";

$sqlnumcarterafecha = " 
SELECT * FROM  detcarteras D
INNER JOIN carteras on carfactura = D.dcafactura 
WHERE D.dcaempresa = '".$empresa."' 
AND D.dcafepag >= '".$fecha2."' OR D.dcafepag IS NULL
AND D.dcafecha < '".$fecha2."'  GROUP BY dcafactura ; 
";
*/

$sqlcarcapital = "SELECT * FROM detcarteras inner join solicitudes on solfactura = dcafactura WHERE solncuota > 1 and dcaempresa = '".$empresa."' AND dcafepag BETWEEN '".$fecha1."' AND '".$fecha2."' AND dcaestado = 'CANCELADO'"; //qry descuentos  
$sqlnumcarcapital = "SELECT * FROM detcarteras inner join solicitudes on solfactura = dcafactura WHERE solncuota > 1 and dcaempresa = '".$empresa."' AND dcafepag BETWEEN '".$fecha1."' AND '".$fecha2."' AND dcaestado = 'CANCELADO' group by dcafactura"; //qry descuentos  numero de clientes agrupados por factura
$sqldescapital = "SELECT * FROM detcarteras WHERE dcaempresa = '".$empresa."' AND dcafepag BETWEEN '".$fecha1."' AND '".$fecha2."' AND dcaestado = 'CANCELADO' and dcadescuento > 0"; //qry descuentos 
$sqlnumdescapital = "SELECT * FROM detcarteras WHERE dcaempresa = '".$empresa."' AND dcafepag BETWEEN '".$fecha1."' AND '".$fecha2."' AND dcaestado = 'CANCELADO' and dcadescuento > 0 group by dcafactura"; //qry descuentos  numero de clientes agrupados por factura
$sqlNC = "SELECT * FROM  movimientos  WHERE movempresa = '".$empresa."' AND movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'NC' AND movestado <> 'ANULADA'"; // qry NC = notas credito
$sqlND = "SELECT * FROM  movimientos  WHERE movempresa = '".$empresa."' AND movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'ND' AND movestado <> 'ANULADA'"; // qry ND = notas debito
$sqlDV = "SELECT * FROM  movimientos  WHERE movempresa = '".$empresa."' AND movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'DV' AND movestado <> 'ANULADA'"; // qry DV = devoluciones
$sqlcarcastigada = " SELECT * FROM  detcarteras INNER JOIN carteras on carfactura = dcafactura WHERE dcaempresa = '".$empresa."' AND dcafecha <= '".$fecha2."' AND dcaestado = 'ACTIVA' AND carestado <> 'CANCELADO' AND carestado = 'CASTIGADA' order by dcafecha desc; "; // qry Cartera Castigada 
$sqlnumcarcastigada = " SELECT * FROM  detcarteras INNER JOIN carteras on carfactura = dcafactura WHERE dcaempresa = '".$empresa."' AND dcafecha <= '".$fecha2."' AND dcaestado = 'ACTIVA' AND (carestado <> 'CANCELADO' AND carestado = 'CASTIGADA') group by dcafactura order by dcafecha desc; "; // qry Cartera Castigada numero de clientes

}else{/* ESTAS CONSULTAS SON PARA LA SELECCION DE TODAS LAS EMPRESAS*/

/*AGREGADO POR MARIA DIAZ PARA DETERMINAR SI EL MES YA FUE CERRADO Y CON CUAL VALOR*/
$anno = substr($fecha2, 0, 4); 
$mes = substr($fecha2, 5, 2);  

$qrysaldo = $db->query("SELECT SUM(saldocarteravalor) AS saldocarteravalor, SUM(saldocarteranumclientes) AS saldocarteranumclientes FROM saldocartera WHERE saldocarteramesano = '".$mes."-".$anno."'");
$saldoAnterior = $qrysaldo->rowCount();


$sqlgeneral = "SELECT * FROM solicitudes INNER JOIN movimientos ON solfactura = movnumero  WHERE movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'FV' AND movestado <> 'ANULADA'"; // qry general ventas bruto

$sqlventascredito = "SELECT * FROM solicitudes INNER JOIN movimientos ON solfactura = movnumero  WHERE movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'FV' AND  solncuota > 1 AND movestado <> 'ANULADA' ;"; // qry ventas a credito
$sqlventascontado = "SELECT * FROM solicitudes INNER JOIN movimientos ON solfactura = movnumero  WHERE movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'FV' AND  solncuota IN ('1','0') AND movestado <> 'ANULADA';"; // qry ventas de contado
$sqlfinanciado = "SELECT * FROM solicitudes INNER JOIN movimientos ON solfactura = movnumero  WHERE movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'FV' AND  movfinan > 0 AND movestado <> 'ANULADA';"; // qry financiado

$sqlcuotasini =  "SELECT * FROM solicitudes INNER JOIN movimientos ON solfactura = movnumero  WHERE movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'FV' AND  solcuota > 0 AND movestado = 'FACTURADO';"; // qry cuotas ini

/*
$sqlcarterafecha = "
SELECT * FROM  detcarteras D
INNER JOIN carteras on carfactura = D.dcafactura 
WHERE D.dcafepag >= '".$fecha2."' 
AND D.dcafecha < '".$fecha2."' 
order by D.dcafactura, D.dcacuota desc ; 
";

$sqlnumcarterafecha = " 
SELECT * FROM  detcarteras D
INNER JOIN carteras on carfactura = D.dcafactura 
WHERE D.dcafepag >= '".$fecha2."' OR D.dcafepag IS NULL
AND D.dcafecha < '".$fecha2."'  GROUP BY dcafactura ; 
";
*/

$sqlcarcapital = "SELECT * FROM detcarteras inner join solicitudes on solfactura = dcafactura WHERE solncuota > 1 AND dcafepag BETWEEN '".$fecha1."' AND '".$fecha2."' AND dcaestado = 'CANCELADO'"; //qry descuentos  
$sqlnumcarcapital = "SELECT * FROM detcarteras inner join solicitudes on solfactura = dcafactura WHERE solncuota > 1 AND dcafepag BETWEEN '".$fecha1."' AND '".$fecha2."' AND dcaestado = 'CANCELADO' group by dcafactura"; //qry descuentos  numero de clientes agrupados por factura
$sqldescapital = "SELECT * FROM detcarteras WHERE dcafepag BETWEEN '".$fecha1."' AND '".$fecha2."' AND dcaestado = 'CANCELADO' and dcadescuento > 0"; //qry descuentos 
$sqlnumdescapital = "SELECT * FROM detcarteras WHERE dcafepag BETWEEN '".$fecha1."' AND '".$fecha2."' AND dcaestado = 'CANCELADO' and dcadescuento > 0 group by dcafactura"; //qry descuentos  numero de clientes agrupados por factura
$sqlNC = "SELECT * FROM  movimientos  WHERE movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'NC' AND movestado <> 'ANULADA'"; // qry NC = notas credito
$sqlND = "SELECT * FROM  movimientos  WHERE movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'ND' AND movestado <> 'ANULADA'"; // qry ND = notas debito
$sqlDV = "SELECT * FROM  movimientos  WHERE movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'DV' AND movestado <> 'ANULADA'"; // qry DV = devoluciones
$sqlcarcastigada = " SELECT * FROM  detcarteras INNER JOIN carteras on carfactura = dcafactura WHERE dcafecha <= '".$fecha2."' AND dcaestado = 'ACTIVA' AND carestado <> 'CANCELADO' AND carestado = 'CASTIGADA' order by dcafecha desc; "; // qry Cartera Castigada 
$sqlnumcarcastigada = " SELECT * FROM  detcarteras INNER JOIN carteras on carfactura = dcafactura WHERE dcafecha <= '".$fecha2."' AND dcaestado = 'ACTIVA' AND (carestado <> 'CANCELADO' AND carestado = 'CASTIGADA') group by dcafactura order by dcafecha desc; "; // qry Cartera Castigada numero de clientes

}




$qrygeneral = $db->query($sqlgeneral); // consulta general ventas bruto
while($rowgeneral = $qrygeneral->fetch(PDO::FETCH_ASSOC)) {
$venbrutageneral = $venbrutageneral + $rowgeneral['movvalor'];
$numgeneral = $qrygeneral->rowCount();
}

$qryventascredito = $db->query($sqlventascredito); // consulta ventas a credito 
while($rowventascredito = $qryventascredito->fetch(PDO::FETCH_ASSOC)) { 
$credito = $credito + $rowventascredito['movvalor'];	
$numventascredito = $qryventascredito->rowCount();
}

$qryventascontado = $db->query($sqlventascontado); // consulta ventas de contado
while($rowventascontado = $qryventascontado->fetch(PDO::FETCH_ASSOC)) { 
$contado = $contado + $rowventascontado['movvalor'];	
$numventascontado = $qryventascontado->rowCount();
}

$qryfinanciado = $db->query($sqlfinanciado); // consulta financiado
while($rowfinanciado = $qryfinanciado->fetch(PDO::FETCH_ASSOC)) { 
$finantotal = $finantotal + $rowfinanciado['movfinan'];
$numfinanciado = $qryfinanciado->rowCount();
}

$qrycuotasini = $db->query($sqlcuotasini); // consulta cuotas iniciales
while($rowcuotasini = $qrycuotasini->fetch(PDO::FETCH_ASSOC)) { 
$ctaini = $ctaini + $rowcuotasini['solcuota'];
$numcuotasini = $qrycuotasini->rowCount();
}

if($saldoAnterior > 0)
{
	$rowsaldo = $qrysaldo->fetch(PDO::FETCH_ASSOC);
	$tlcarterafn = $rowsaldo['saldocarteravalor'];
	$numcarfn = $rowsaldo['saldocarteranumclientes'];
}
else
{
echo "<br><br><br><br><br>";
	// Consulta Para Cartera Total a la fecha y Numero de clientes
	$qry = $db->query("SELECT * FROM usuarios WHERE usupromotor = '1'");
	while($row = $qry->fetch(PDO::FETCH_ASSOC)){
		$tlcartera = 0;
		$num = 0;
		if ($empresa != 'TODAS'){
		$qryemp = $db->query("SELECT * FROM empresas where empid = $empresa");
		} else {
		$qryemp = $db->query("SELECT * FROM empresas");
		}
		$contadore++;
		while($rowemp = $qryemp->fetch(PDO::FETCH_ASSOC)){
			//echo $contadore."  SELECT * FROM carteras WHERE carempresa = ".$rowemp['empid']." AND carpromotor = '".$row['usuid']."' AND (carestado = 'ACTIVA' OR carestado = 'CASTIGADA') AND carfecha <= '$fecha2' <br>";
			$qry2 = $db->query("SELECT * FROM carteras WHERE carempresa = ".$rowemp['empid']." AND carpromotor = '".$row['usuid']."' AND (carestado = 'ACTIVA' OR carestado = 'CASTIGADA') AND carfecha <= '$fecha2'");
			$num = $num + $qry2->rowCount();
			while($row2 = $qry2->fetch(PDO::FETCH_ASSOC)){
				$tlcartera = $tlcartera + $row2['cartotal'];
			}
		}
		
		$numcarfn = $numcarfn + $num; // numero de clientes de cartera a la fecha
		$tlcarterafn = $tlcarterafn + $tlcartera;
	}
}

$qrycarcapital = $db->query($sqlcarcapital); // cartera Pagada Capital
$qrynumcarcapital = $db->query($sqlnumcarcapital); // numero de clientes agrupados por facturas
while($rowcarcapital = $qrycarcapital->fetch(PDO::FETCH_ASSOC)) { 
 $carcapital = $carcapital + $rowcarcapital['dcavalor'];
$numcarcapital = $qrynumcarcapital->rowCount();
}

$qrydescapital = $db->query($sqldescapital); // Descuentos Capital
$qrynumdescapital = $db->query($sqlnumdescapital); // numero de clientes agrupados por facturas
while($rowdescapital = $qrydescapital->fetch(PDO::FETCH_ASSOC)) { 
$descapital = $descapital + $rowdescapital['dcadescuento'];
$numdescapital = $qrynumdescapital->rowCount();
}

$qryNC = $db->query($sqlNC); // consulta para NOTAS CREDITO
while($rowNC = $qryNC->fetch(PDO::FETCH_ASSOC)) { 
$ncredito = $ncredito + $rowNC['movvalor'];
$numNC = $qryNC->rowCount();
}

$qryND = $db->query($sqlND); // consulta para NOTAS DEBITO
while($rowND = $qryND->fetch(PDO::FETCH_ASSOC)) { 
$ndebito = $ndebito  + $rowND['movvalor'];
$numND = $qryND->rowCount();
}

$qryDV = $db->query($sqlDV); // consulta para DEVOLUCIONES
while($rowDV = $qryDV->fetch(PDO::FETCH_ASSOC)) { 
$devoluciones = $devoluciones  + $rowDV['movvalor'];
$numDV = $qryDV->rowCount();
}

$qrycarcastigada = $db->query($sqlcarcastigada); // consulta para Cartera Castigada
$numqrycarcastigada = $db->query($sqlnumcarcastigada); //numero de clientes con cartera castigada agrupados por factura
while($rowcarcastigada = $qrycarcastigada->fetch(PDO::FETCH_ASSOC)) { 
$carcastigada = $carcastigada  + $rowcarcastigada['carsaldo'];
$numcarcastigada = $numqrycarcastigada->rowCount();
}

/* COMO ESTA LA CARTERA CASTIGADA SI NO ESTÁ EN LA BASE DE DATOS SE COLOCA LA SIGUIENTE LÍNEA CON EL FIN DE QUE QUEDE EL VALOR CORRECTO */
if($saldoAnterior > 0)
{
	$tlcarterafn = $tlcarterafn + $carcastigada;
}

$row7 = $db->query("SELECT * FROM empresas WHERE empid = '$empresa'")->fetch(PDO::FETCH_ASSOC); // row Nombre de la empresa
$rowpar = $db->query("SELECT * FROM parametros")->fetch(PDO::FETCH_ASSOC); // row de parametros

?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
	<?php 
		require($r . 'incluir/src/head.php');
		require($r . 'incluir/src/head-form.php');
	?>	

<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#tabla').dataTable({
		'bPaginate': false,
        'bLengthChange': false,
        'bFilter': false,
		'bSort': false,
        'bInfo': false,
        'bAutoWidth': false,
		'bStateSave': true,
		'bJQueryUI': true
	});
	$('.btnatras').button({ icons: { primary: 'ui-icon ui-icon-arrowthick-1-w' }});
});
</script>
</head>
<body>
<?php require($r.'incluir/src/login.php') ?>
<section id="principal">
<?php require($r.'incluir/src/cabeza.php') ?>
<?php require($r.'incluir/src/menu.php') ?>
<article id="cuerpo">
<article class="mapa">
<a href="#">Principal</a><div class="mapa_div"></div><a href="#">Cartera</a><div class="mapa_div"></div><a class="current">Reportes</a>
</article>
<article id="contenido">
<h2>Reporte resumen de cartera de <?php  echo $fecha1.' al '.$fecha2; if($empresa != 'TODAS') {echo ' de la empresa '.$row7['empnombre'];}else { echo ' de todas las empresas ';} ?></h2>
<div class="reporte">
<a href="pdfresumen.php?<?php echo $filtro ?>"><img src="<?php echo $r ?>imagenes/iconos/pdf.png" title="pdf" /></a> <a href="#"><img src="<?php echo $r ?>imagenes/iconos/excel.png" title="csv" /></a>
</div>
<table id="tabla">
<thead>
<tr>
<th>Detalle</th>
<th>Valor</th>
<th>No. Clientes</th>
</tr>
</thead>
<tbody>
<tr><td>VR. Ventas brutas entre el intervalo: </td><td align="right"><?php echo number_format($venbrutageneral,0,',','.') ?></td> <td> <?php echo $numgeneral ?> </td> </tr>
<tr><td>VR. Ventas a credito entre el intervalo: </td><td align="right"><?php echo number_format($credito,0,',','.') ?></td> <td> <?php echo $numventascredito ?> </td> </tr>
<tr><td>VR. Financiado entre el intervalo: </td><td align="right"><?php echo number_format($finantotal,0,',','.') ?></td> <td> <?php echo $numfinanciado ?> </td> </tr>
<tr><td>VR. Saldos iniciales y contados entre el intervalo: </td><td align="right"><?php echo number_format($ctaini + $contado,0,',','.') ?></td><td><?php echo number_format($numcuotasini + $numventascontado,0,',','.')?></td></tr>
<tr><td>VR. Cartera total al <?php echo $fecha2 ?>: </td><td align="right"><?php echo number_format($tlcarterafn - $carcastigada,0,',','.') ?></td> <td> <?php echo number_format($numcarfn - $numcarcastigada,0,',','.') ?> </td> </tr>
<?php
$carfinanciacionmul = $carcapital * $rowpar['parfinanciacion'];
?>
<tr><td>VR. Cartera pagada entre el intervalo (CAPITAL): </td><td align="right"><?php echo number_format($carcapital,0,',','.') ?></td> <td> <?php echo $numcarcapital ?> </td> </tr>
<tr><td>VR. Cartera pagada entre el intervalo (FINANCIACION): </td><td align="right"><?php echo number_format($carfinanciacionmul,0,',','.') ?></td> <td> </td> </tr>
<?php
$desfinanciacionmul = $descapital * $rowpar['parfinanciacion'];
?>
<tr><td>VR. Descuentos entre el intervalo (CAPITAL): </td><td align="right"><?php echo number_format($descapital,0,',','.') ?></td> <td> <?php echo $numdescapital ?> </td> </tr>
<tr><td>VR. Descuentos entre el intervalo (FINANCIACION): </td><td align="right"><?php echo number_format($desfinanciacionmul,0,',','.') ?></td> <td> </td> </tr>
<tr><td>VR. Notas debito entre el intervalo: </td><td align="right"><?php echo number_format($ndebito,0,',','.') ?></td> <td> <?php echo $numND ?> </td> </tr>
<tr><td>VR. Notas credito entre el intervalo: </td><td align="right"><?php echo number_format($ncredito,0,',','.') ?></td> <td> <?php echo $numNC ?> </td> </tr>
<tr><td>VR. Devoluciones entre el intervalo: </td><td align="right"><?php echo number_format($devoluciones,0,',','.') ?></td> <td> <?php echo $numDV ?> </td> </tr>
<tr><td>VR. Cartera castigada activa al <?php echo $fecha2 ?>: </td><td align="right"><?php echo number_format($carcastigada,0,',','.') ?></td> <td> <?php echo $numcarcastigada ?> </td> </tr>
</tbody>
</table>
<p class="boton">
<button type="button" class="btnatras" onClick="carga(); location.href='conresumen.php'">atras</button>
</p>
</article>
</article>
<?php require($r.'incluir/src/pie.php') ?>
</section>
</body>
</html>