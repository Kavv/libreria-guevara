<?php
$r = '../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
require($r.'incluir/funciones.php');
include($r.'incluir/phpexcel/Classes/PHPExcel.php');
$promotor = $_GET['promotor'];
$promotor = array_recibe($promotor);
$objPHPExcel = new PHPExcel();
$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN
		)
	)
);
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:O1');
$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'REPORTE DETALLADOS MOROSOS');
$i = 2;
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$i.':O'.$i);
$objPHPExcel->getActiveSheet()->getStyle('A'.$i)->applyFromArray($styleArray);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$i, 'AL DIA');
$i++;
$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':O'.$i)->applyFromArray($styleArray);
$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A'.$i, 'EMPRESA')
   	        ->setCellValue('B'.$i, 'cuenta')
   	        ->setCellValue('C'.$i, 'CEDULA')
			->setCellValue('D'.$i, 'CLIENTE')
			->setCellValue('E'.$i, 'PROMOTOR')
   	        ->setCellValue('F'.$i, 'C PACTA')
			->setCellValue('G'.$i, 'VLR CUOTA')
   	        ->setCellValue('H'.$i, 'D MORA')
   	        ->setCellValue('I'.$i, 'C MORA')
			->setCellValue('J'.$i, 'C PEND')
			->setCellValue('K'.$i, 'VLR MORA')
			->setCellValue('L'.$i, 'SALDO')
			->setCellValue('M'.$i, 'ULT PAGO')
			->setCellValue('N'.$i, 'FECHA PROX PAGO')
			->setCellValue('O'.$i, 'CORREO ELECTRONICO');
$cta1 = 0;
$vlr1 = 0;
$saldo1 = 0;
foreach ($promotor as $promo=>$elemento){
	$i++;
	$qry = $db->query("SELECT * FROM ((carteras INNER JOIN clientes ON carcliente = cliid) INNER JOIN empresas ON carempresa = empid) INNER JOIN usuarios ON carpromotor = usuid WHERE carpromotor = '$elemento' AND (carestado = 'ACTIVA' OR carestado = 'CASTIGADA'  OR carestado = 'RECOLECCION' OR carestado = 'PENDIENTE' OR carestado = 'MOROSO' OR carestado = 'RECOLECCION RAZONADA')");
	while($row = $qry->fetch(PDO::FETCH_ASSOC)){	
		$qry2 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row['carempresa']."' AND dcafactura = '".$row['carfactura']."' AND dcaestado = 'ACTIVA' ORDER BY dcacuota ASC");
		$num2 = $qry2->rowCount();
		$row2 = $qry2->fetch(PDO::FETCH_ASSOC);
		if(date('Y-m-d') <= $row2['dcafecha']){
			$cta1++;
			$saldo1 = $saldo1 + $row['carsaldo'];
			$mora = 0;
			$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':O'.$i)->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('B'.$i)->getNumberFormat();
			$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue('A'.$i, $row['empnombre'])
						->setCellValue('B'.$i, $row['carfactura'])
						->setCellValue('C'.$i, $row['cliid'])
						->setCellValue('D'.$i, $row['clinombre'].' '.$row['clinom2'].' '.$row['cliape1'].' '.$row['cliape2'])
						->setCellValue('E'.$i, $row['usunombre'])
						->setCellValue('F'.$i, $row['carncuota'])
						->setCellValue('G'.$i, $row['carcuota'])
						->setCellValue('H'.$i, $mora)
						->setCellValue('I'.$i, '0')
						->setCellValue('J'.$i, $ncpend)
						->setCellValue('K'.$i, $ncmora * $row['carcuota'])
						->setCellValue('L'.$i, $row['carsaldo'])
						->setCellValue('M'.$i, $ultpago)
						->setCellValue('N'.$i, $row2['dcafecha'])
						->setCellValue('O'.$i, $row['cliemail']);
			$cta1 = $cta1 + 1;
			$vlr1 = $vlr1 + ($ncmora * $row['carcuota']);
			$saldo1 = $saldo1 + $row['carsaldo'];
			$i++;
			
		}
	}
}
$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':O'.$i)->applyFromArray($styleArray);
$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('B'.$i, $cta1)
  			->setCellValue('I'.$i, $vlr1)
			->setCellValue('K'.$i, $saldo1);
$i++;
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$i.':O'.$i);
$objPHPExcel->getActiveSheet()->getStyle('A'.$i)->applyFromArray($styleArray);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$i, '1 - 30');
$i++;
$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A'.$i, 'EMPRESA')
   	        ->setCellValue('B'.$i, 'cuenta')
			->setCellValue('C'.$i, 'CEDULA')
   	        ->setCellValue('D'.$i, 'CLIENTE')
			->setCellValue('E'.$i, 'PROMOTOR')
   	        ->setCellValue('F'.$i, 'C PACTA')
			->setCellValue('G'.$i, 'VLR CUOTA')
   	        ->setCellValue('H'.$i, 'D MORA')
   	        ->setCellValue('I'.$i, 'C MORA')
			->setCellValue('J'.$i, 'C PEND')
			->setCellValue('K'.$i, 'VLR MORA')
			->setCellValue('L'.$i, 'SALDO')
			->setCellValue('M'.$i, 'ULT PAGO')
			->setCellValue('N'.$i, 'FECHA PROX PAGO')
			->setCellValue('O'.$i, 'CORREO ELECTRONICO');
$cta1 = 0;
$vlr1 = 0;
$saldo1 = 0;
foreach ($promotor as $promo=>$elemento){
	$i++;
	$qry2 = $db->query("SELECT * FROM ((carteras INNER JOIN clientes ON carcliente = cliid) INNER JOIN empresas ON carempresa = empid) INNER JOIN usuarios ON carpromotor = usuid WHERE carpromotor = '$elemento' AND (carestado = 'ACTIVA' OR carestado = 'CASTIGADA' OR carestado = 'RECOLECCION' OR carestado = 'PENDIENTE' OR carestado = 'MOROSO' OR carestado = 'RECOLECCION RAZONADA')");
	while($row2 = $qry2->fetch(PDO::FETCH_ASSOC)){
		$qry3 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota DESC");
		while($row3 = $qry3->fetch(PDO::FETCH_ASSOC)){	
			if($row3['dcafepag'] != ''){
				$ultpago = $row3['dcafepag'];
				break;
			}
		}
		$qry4 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota ASC");
		$ncmora = 0;
		$ncpend = 0;
		$residuo = 0;
		while($row4 = $qry4->fetch(PDO::FETCH_ASSOC)){	
			if(date('Y-m-d') > $row4['dcafecha'] && $row4['dcaestado'] == 'ACTIVA'){
				$ncmora = $ncmora + 1;
				$residuo = $residuo + ($row4['dcavalor'] + $row4['dcadescuento']);
			}
			if($row4['dcaestado'] == 'ACTIVA')
				$ncpend = $ncpend + 1;
		}
		$qry5 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota ASC");
		while($row5 = $qry5->fetch(PDO::FETCH_ASSOC)){
			if(date('Y-m-d') > $row5['dcafecha'] && $row5['dcaestado'] == 'ACTIVA'){
				$mora = fechaDif($row5['dcafecha'],date('Y-m-d'));
				if($mora >=1  && $mora <= 30){
					$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':O'.$i)->applyFromArray($styleArray);
					$objPHPExcel->getActiveSheet()->getStyle('B'.$i)->getNumberFormat();
					$objPHPExcel->setActiveSheetIndex(0)
							->setCellValue('A'.$i, $row2['empnombre'])
							->setCellValue('B'.$i, $row2['carfactura'])
							->setCellValue('C'.$i, $row2['cliid'])
							->setCellValue('D'.$i, $row2['clinombre'].' '.$row2['clinom2'].' '.$row2['cliape1'].' '.$row2['cliape2'])
							->setCellValue('E'.$i, $row2['usunombre'])
							->setCellValue('F'.$i, $row2['carncuota'])
							->setCellValue('G'.$i, $row2['carcuota'])
							->setCellValue('H'.$i, $mora)
							->setCellValue('I'.$i, $ncmora)
							->setCellValue('J'.$i, $ncpend)
							->setCellValue('K'.$i, ($ncmora * $row2['carcuota']) - $residuo)
							->setCellValue('L'.$i, $row2['carsaldo'])
							->setCellValue('M'.$i, $ultpago)
							->setCellValue('N'.$i, $row5['dcafecha'])
							->setCellValue('O'.$i, $row['cliemail']);
					$cta1 = $cta1 + 1;
					$vlr1 = $vlr1 + (($ncmora * $row2['carcuota']) - $residuo);
					$saldo1 = $saldo1 + $row2['carsaldo'];
					$i++;
				}
				break;
			}	
		}
	}
}
	$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':O'.$i)->applyFromArray($styleArray);
	$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('B'.$i, $cta1)
   				->setCellValue('I'.$i, $vlr1)
				->setCellValue('K'.$i, $saldo1);
	$i++;
	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$i.':O'.$i);
	$objPHPExcel->getActiveSheet()->getStyle('A'.$i)->applyFromArray($styleArray);
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$i, '31 - 90');
	$i++;
	$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A'.$i, 'EMPRESA')
    	        ->setCellValue('B'.$i, 'cuenta')
				->setCellValue('C'.$i, 'CEDULA')
    	        ->setCellValue('D'.$i, 'CLIENTE')
				->setCellValue('E'.$i, 'PROMOTOR')
    	        ->setCellValue('F'.$i, 'C PACTA')
				->setCellValue('G'.$i, 'VLR CUOTA')
    	        ->setCellValue('H'.$i, 'D MORA')
    	        ->setCellValue('I'.$i, 'C MORA')
				->setCellValue('I'.$i, 'C PEND')
				->setCellValue('K'.$i, 'VLR MORA')
				->setCellValue('L'.$i, 'SALDO')
				->setCellValue('M'.$i, 'ULT PAGO')
				->setCellValue('N'.$i, 'FECHA PROX PAGO')
				->setCellValue('O'.$i, 'CORREO ELECTRONICO');
$cta1 = 0;
$vlr1 = 0;
$saldo1 = 0;
foreach ($promotor as $promo=>$elemento){
	$i++;
	$qry2 = $db->query("SELECT * FROM ((carteras INNER JOIN clientes ON carcliente = cliid) INNER JOIN empresas ON carempresa = empid) INNER JOIN usuarios ON carpromotor = usuid WHERE carpromotor = '$elemento' AND (carestado = 'ACTIVA' OR carestado = 'CASTIGADA' OR carestado = 'RECOLECCION' OR carestado = 'PENDIENTE' OR carestado = 'MOROSO' OR carestado = 'RECOLECCION RAZONADA')");
	while($row2 = $qry2->fetch(PDO::FETCH_ASSOC)){
		$qry3 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota DESC");
		while($row3 = $qry3->fetch(PDO::FETCH_ASSOC)){	
			if($row3['dcafepag'] != ''){
				$ultpago = $row3['dcafepag'];
				break;
			}
		}
		$qry4 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota ASC");
		$ncmora = 0;
		$ncpend = 0;
		$residuo = 0;
		while($row4 = $qry4->fetch(PDO::FETCH_ASSOC)){	
			if(date('Y-m-d') > $row4['dcafecha'] && $row4['dcaestado'] == 'ACTIVA'){
				$ncmora = $ncmora + 1;
				$residuo = $residuo + ($row4['dcavalor'] + $row4['dcadescuento']);
			}
			if($row4['dcaestado'] == 'ACTIVA')
				$ncpend = $ncpend + 1;
		}
		$qry5 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota ASC");
		while($row5 = $qry5->fetch(PDO::FETCH_ASSOC)){
			if(date('Y-m-d') > $row5['dcafecha'] && $row5['dcaestado'] == 'ACTIVA'){
				$mora = fechaDif($row5['dcafecha'],date('Y-m-d'));
				if($mora >= 31 && $mora <= 90){
					$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':O'.$i)->applyFromArray($styleArray);
					$objPHPExcel->getActiveSheet()->getStyle('B'.$i)->getNumberFormat();
					$objPHPExcel->setActiveSheetIndex(0)
							->setCellValue('A'.$i, $row2['empnombre'])
							->setCellValue('B'.$i, $row2['carfactura'])
							->setCellValue('C'.$i, $row2['cliid'])
							->setCellValue('D'.$i, $row2['clinombre'].' '.$row2['clinom2'].' '.$row2['cliape1'].' '.$row2['cliape2'])
							->setCellValue('E'.$i, $row2['usunombre'])
							->setCellValue('F'.$i, $row2['carncuota'])
							->setCellValue('G'.$i, $row2['carcuota'])
							->setCellValue('H'.$i, $mora)
							->setCellValue('I'.$i, $ncmora)
							->setCellValue('J'.$i, $ncpend)
							->setCellValue('K'.$i, ($ncmora * $row2['carcuota']) - $residuo)
							->setCellValue('L'.$i, $row2['carsaldo'])
							->setCellValue('M'.$i, $ultpago)
							->setCellValue('N'.$i, $row5['dcafecha'])
							->setCellValue('O'.$i, $row2['cliemail']);
					$cta1 = $cta1 + 1;
					$vlr1 = $vlr1 + (($ncmora * $row2['carcuota']) - $residuo);
					$saldo1 = $saldo1 + $row2['carsaldo'];
					$i++;
				}
				break;
			}	
		}
	}
}



// -------------------------------------

	$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':O'.$i)->applyFromArray($styleArray);
	$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('B'.$i, $cta1)
   				->setCellValue('I'.$i, $vlr1)
				->setCellValue('K'.$i, $saldo1);
	$i++;
	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$i.':O'.$i);
	$objPHPExcel->getActiveSheet()->getStyle('A'.$i)->applyFromArray($styleArray);
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$i, '91 - 180');
	$i++;
	$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A'.$i, 'EMPRESA')
    	        ->setCellValue('B'.$i, 'cuenta')
				->setCellValue('C'.$i, 'CEDULA')
    	        ->setCellValue('D'.$i, 'CLIENTE')
				->setCellValue('E'.$i, 'PROMOTOR')
    	        ->setCellValue('F'.$i, 'C PACTA')
				->setCellValue('G'.$i, 'VLR CUOTA')
    	        ->setCellValue('H'.$i, 'D MORA')
    	        ->setCellValue('I'.$i, 'C MORA')
				->setCellValue('I'.$i, 'C PEND')
				->setCellValue('K'.$i, 'VLR MORA')
				->setCellValue('L'.$i, 'SALDO')
				->setCellValue('M'.$i, 'ULT PAGO')
				->setCellValue('N'.$i, 'FECHA PROX PAGO')
				->setCellValue('O'.$i, 'CORREO ELECTRONICO');
$cta1 = 0;
$vlr1 = 0;
$saldo1 = 0;
foreach ($promotor as $promo=>$elemento){
	$i++;
	$qry2 = $db->query("SELECT * FROM ((carteras INNER JOIN clientes ON carcliente = cliid) INNER JOIN empresas ON carempresa = empid) INNER JOIN usuarios ON carpromotor = usuid WHERE carpromotor = '$elemento' AND (carestado = 'ACTIVA' OR carestado = 'CASTIGADA' OR carestado = 'RECOLECCION' OR carestado = 'PENDIENTE' OR carestado = 'MOROSO' OR carestado = 'RECOLECCION RAZONADA')");
	while($row2 = $qry2->fetch(PDO::FETCH_ASSOC)){
		$qry3 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota DESC");
		while($row3 = $qry3->fetch(PDO::FETCH_ASSOC)){	
			if($row3['dcafepag'] != ''){
				$ultpago = $row3['dcafepag'];
				break;
			}
		}
		$qry4 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota ASC");
		$ncmora = 0;
		$ncpend = 0;
		$residuo = 0;
		while($row4 = $qry4->fetch(PDO::FETCH_ASSOC)){	
			if(date('Y-m-d') > $row4['dcafecha'] && $row4['dcaestado'] == 'ACTIVA'){
				$ncmora = $ncmora + 1;
				$residuo = $residuo + ($row4['dcavalor'] + $row4['dcadescuento']);
			}
			if($row4['dcaestado'] == 'ACTIVA')
				$ncpend = $ncpend + 1;
		}
		$qry5 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota ASC");
		while($row5 = $qry5->fetch(PDO::FETCH_ASSOC)){
			if(date('Y-m-d') > $row5['dcafecha'] && $row5['dcaestado'] == 'ACTIVA'){
				$mora = fechaDif($row5['dcafecha'],date('Y-m-d'));
				if($mora >= 91 && $mora <= 180){
					$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':O'.$i)->applyFromArray($styleArray);
					$objPHPExcel->getActiveSheet()->getStyle('B'.$i)->getNumberFormat();
					$objPHPExcel->setActiveSheetIndex(0)
							->setCellValue('A'.$i, $row2['empnombre'])
							->setCellValue('B'.$i, $row2['carfactura'])
							->setCellValue('C'.$i, $row2['cliid'])
							->setCellValue('D'.$i, $row2['clinombre'].' '.$row2['clinom2'].' '.$row2['cliape1'].' '.$row2['cliape2'])
							->setCellValue('E'.$i, $row2['usunombre'])
							->setCellValue('F'.$i, $row2['carncuota'])
							->setCellValue('G'.$i, $row2['carcuota'])
							->setCellValue('H'.$i, $mora)
							->setCellValue('I'.$i, $ncmora)
							->setCellValue('J'.$i, $ncpend)
							->setCellValue('K'.$i, ($ncmora * $row2['carcuota']) - $residuo)
							->setCellValue('L'.$i, $row2['carsaldo'])
							->setCellValue('M'.$i, $ultpago)
							->setCellValue('N'.$i, $row5['dcafecha'])
							->setCellValue('O'.$i, $row2['cliemail']);
					$cta1 = $cta1 + 1;
					$vlr1 = $vlr1 + (($ncmora * $row2['carcuota']) - $residuo);
					$saldo1 = $saldo1 + $row2['carsaldo'];
					$i++;
				}
				break;
			}	
		}
	}
}


// ------------------------------------------


	$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':O'.$i)->applyFromArray($styleArray);
	$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('B'.$i, $cta1)
   				->setCellValue('I'.$i, $vlr1)
				->setCellValue('K'.$i, $saldo1);
	$i++;
	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$i.':O'.$i);
	$objPHPExcel->getActiveSheet()->getStyle('A'.$i)->applyFromArray($styleArray);
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$i, '181 - MAS');
	$i++;
	$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A'.$i, 'EMPRESA')
    	        ->setCellValue('B'.$i, 'cuenta')
				->setCellValue('C'.$i, 'CEDULA')
    	        ->setCellValue('D'.$i, 'CLIENTE')
				->setCellValue('E'.$i, 'PROMOTOR')
    	        ->setCellValue('F'.$i, 'C PACTA')
				->setCellValue('G'.$i, 'VLR CUOTA')
    	        ->setCellValue('H'.$i, 'D MORA')
    	        ->setCellValue('I'.$i, 'C MORA')
				->setCellValue('J'.$i, 'C PEND')
				->setCellValue('K'.$i, 'VLR MORA')
				->setCellValue('L'.$i, 'SALDO')
				->setCellValue('M'.$i, 'ULT PAGO')
				->setCellValue('N'.$i, 'FECHA PROX PAGO')
				->setCellValue('O'.$i, 'CORREO ELECTRONICO');
$cta1 = 0;
$vlr1 = 0;
$saldo1 = 0;
foreach ($promotor as $promo=>$elemento){
	$i++;
	$qry2 = $db->query("SELECT * FROM ((carteras INNER JOIN clientes ON carcliente = cliid) INNER JOIN empresas ON carempresa = empid) INNER JOIN usuarios ON carpromotor = usuid WHERE carpromotor = '$elemento' AND (carestado = 'ACTIVA' OR carestado = 'CASTIGADA' OR carestado = 'RECOLECCION' OR carestado = 'PENDIENTE' OR carestado = 'MOROSO' OR carestado = 'RECOLECCION RAZONADA')");
	while($row2 = $qry2->fetch(PDO::FETCH_ASSOC)){
		$qry3 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota DESC");
		while($row3 = $qry3->fetch(PDO::FETCH_ASSOC)){	
			if($row3['dcafepag'] != ''){
				$ultpago = $row3['dcafepag'];
				break;
			}
		}
		$qry4 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota ASC");
		$ncmora = 0;
		$ncpend = 0;
		$residuo = 0;
		while($row4 = $qry4->fetch(PDO::FETCH_ASSOC)){	
			if(date('Y-m-d') > $row4['dcafecha'] && $row4['dcaestado'] == 'ACTIVA'){
				$ncmora = $ncmora + 1;
				$residuo = $residuo + ($row4['dcavalor'] + $row4['dcadescuento']);
			}
			if($row4['dcaestado'] == 'ACTIVA')
				$ncpend = $ncpend + 1;
		}
		$qry5 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row2['carempresa']."' AND dcafactura = '".$row2['carfactura']."' ORDER BY dcacuota ASC");
		while($row5 = $qry5->fetch(PDO::FETCH_ASSOC)){
			if(date('Y-m-d') > $row5['dcafecha'] && $row5['dcaestado'] == 'ACTIVA'){
				$mora = fechaDif($row5['dcafecha'],date('Y-m-d'));
				if($mora >180){
					$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':O'.$i)->applyFromArray($styleArray);
					$objPHPExcel->getActiveSheet()->getStyle('B'.$i)->getNumberFormat();
					$objPHPExcel->setActiveSheetIndex(0)
							->setCellValue('A'.$i, $row2['empnombre'])
							->setCellValue('B'.$i, $row2['carfactura'])
							->setCellValue('C'.$i, $row2['cliid'])
							->setCellValue('D'.$i, $row2['clinombre'].' '.$row2['clinom2'].' '.$row2['cliape1'].' '.$row2['cliape2'])
							->setCellValue('E'.$i, $row2['usunombre'])
							->setCellValue('F'.$i, $row2['carncuota'])
							->setCellValue('G'.$i, $row2['carcuota'])
							->setCellValue('H'.$i, $mora)
							->setCellValue('I'.$i, $ncmora)
							->setCellValue('J'.$i, $ncpend)
							->setCellValue('K'.$i, ($ncmora * $row2['carcuota']) - $residuo)
							->setCellValue('L'.$i, $row2['carsaldo'])
							->setCellValue('M'.$i, $ultpago)
							->setCellValue('N'.$i, $row5['dcafecha'])
							->setCellValue('O'.$i, $row2['cliemail']);
					$cta1 = $cta1 + 1;
					$vlr1 = $vlr1 + (($ncmora * $row2['carcuota']) - $residuo);
					$saldo1 = $saldo1 + $row2['carsaldo'];
					$i++;
				}
				break;
			}	
		}
	}
}
	$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':O'.$i)->applyFromArray($styleArray);
	$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('B'.$i, $cta1)
   				->setCellValue('I'.$i, $vlr1)
				->setCellValue('K'.$i, $saldo1);
	$i++;

// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="edad_mora.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:O0:O0 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
?>