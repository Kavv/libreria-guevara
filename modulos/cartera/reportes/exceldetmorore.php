<?php
$r = '../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
require($r.'incluir/funciones.php');
include($r.'incluir/phpexcel/Classes/PHPExcel.php');
$promotor = $_GET['promotor'];
$promotor = array_recibe($promotor);
$objPHPExcel = new PHPExcel();
$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN
		)
	)
);
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:N1');
$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'REPORTE ');
$i = 2;
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$i.':N'.$i);
$objPHPExcel->getActiveSheet()->getStyle('A'.$i)->applyFromArray($styleArray);
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$i, 'REPORTE');
$i++;
$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':N'.$i)->applyFromArray($styleArray);
$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A'.$i, 'EMPRESA')
   	        ->setCellValue('B'.$i, 'cuenta')
   	        ->setCellValue('C'.$i, 'CEDULA')
			->setCellValue('D'.$i, 'CLIENTE')
			->setCellValue('E'.$i, 'TELEFONOS')
   	        ->setCellValue('F'.$i, 'C PACTA')
			->setCellValue('G'.$i, 'TOTAL')
   	        ->setCellValue('H'.$i, 'D MORA')
   	        ->setCellValue('I'.$i, 'ESTADO')
			->setCellValue('J'.$i, 'C PEND')
			->setCellValue('K'.$i, 'VLR MORA')
			->setCellValue('L'.$i, 'SALDO')
			->setCellValue('M'.$i, 'DEPARTAMENTO')
			->setCellValue('N'.$i, 'FECHA PROX PAGO');
$cta1 = 0;
$vlr1 = 0;
$saldo1 = 0;

	$i++;
$qry = $db->query("SELECT * FROM ((carteras INNER JOIN clientes ON carcliente = cliid) INNER JOIN empresas ON carempresa = empid) INNER JOIN departamentos ON  depid = clidepresidencia  WHERE carempresa = 900487926 ");
	while($row = $qry->fetch(PDO::FETCH_ASSOC)){	
		$qry2 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$row['carempresa']."' AND dcafactura = '".$row['carfactura']."' AND dcaestado = 'ACTIVA' ORDER BY dcacuota ASC");
		$num2 = $qry2->rowCount();
		$row2 = $qry2->fetch(PDO::FETCH_ASSOC);

			$cta1++;
			$saldo1 = $saldo1 + $row['carsaldo'];
			if ($row['carestado'] == 'CANCELADO'){
			$mora = 0;

			} else {
			$mora = fechaDif($row2['dcafecha'],date('2015-04-30'));	
			}
			$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':N'.$i)->applyFromArray($styleArray);
			$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue('A'.$i, $row['empnombre'])
						->setCellValue('B'.$i, $row['carfactura'])
						->setCellValue('C'.$i, $row['cliid'])
						->setCellValue('D'.$i, $row['clinombre'].' '.$row['clinom2'].' '.$row['cliape1'].' '.$row['cliape2'])
						->setCellValue('E'.$i, $row['clicelular'].' / '.$row['clitelresidencia'].' / '.$row['clitelcomercio'])
						->setCellValue('F'.$i, $row['carncuota'])
						->setCellValue('G'.$i, $row['cartotal'])
						->setCellValue('H'.$i, $mora)
						->setCellValue('I'.$i, $row['carestado'])
						->setCellValue('J'.$i, $ncpend)
						->setCellValue('K'.$i, $ncmora * $row['carcuota'])
						->setCellValue('L'.$i, $row['carsaldo'])
						->setCellValue('M'.$i, $row['depnombre'])
						->setCellValue('N'.$i, $row2['dcafecha']);
			$cta1 = $cta1 + 1;
			$vlr1 = $vlr1 + ($ncmora * $row['carcuota']);
			$saldo1 = $saldo1 + $row['carsaldo'];
			$i++;
			
		
	}

$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':N'.$i)->applyFromArray($styleArray);
$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('B'.$i, $cta1)
  			->setCellValue('I'.$i, $vlr1)
			->setCellValue('K'.$i, $saldo1);
$i++;


// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="edad_mora.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
?>