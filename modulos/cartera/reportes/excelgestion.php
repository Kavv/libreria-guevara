<?php
$r = '../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
include($r.'incluir/phpexcel/Classes/PHPExcel.php');
$promotor = $_GET['promotor'];
$fecha = $_GET['fecha'];

$usuario = $db->query("SELECT * FROM usuarios where usuid = $promotor;");
$rowusuario = $usuario->fetch(PDO::FETCH_ASSOC);

$gestiondia = $db->query("SELECT * FROM hiscarteras INNER JOIN  carteras on (hiscuenta = carfactura AND hisempresa = carempresa) WHERE DATE(hisfecha) = '$fecha' and hisusuario = $promotor ;");

$objPHPExcel = new PHPExcel();
$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN
		)
	)
);
$objPHPExcel->getActiveSheet()->getStyle('A1:D2')->applyFromArray($styleArray);
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:D1');

	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'REPORTE RESUMEN DE GESTION DIARIA DEL '.$rowusuario['usunombre'].' EL '.$fecha );	
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A2', 'CLIENTE')
            ->setCellValue('B2', 'cuenta')
            ->setCellValue('C2', 'FECHA')
            ->setCellValue('D2', 'NOTA');
$i = 3;
while($rowgestiondia = $gestiondia->fetch(PDO::FETCH_ASSOC)){	
	$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':D'.$i)->applyFromArray($styleArray);
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$i, $rowgestiondia['carcliente'])->setCellValue('B'.$i, $rowgestiondia['hiscuenta'])->setCellValue('C'.$i, $rowgestiondia['hisfecha']);
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$i, $rowgestiondia['hisnota']);	
	$i++;
}

// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Gestion dia '.$fecha.'.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
?>