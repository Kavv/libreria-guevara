<?php
$r = '../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
require($r.'incluir/funciones.php');
$fecha = $_POST['fecha'];
$promotor = $_POST['promotor'];

$filtro = 'fecha='.$fecha.'&promotor='.$promotor;
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
	<?php 
		require($r . 'incluir/src/head.php');
		require($r . 'incluir/src/head-form.php');
	?>	

<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('.tabla').dataTable({
		'bJQueryUI': true,
		'bStateSave': true,
		'bSort': false,
		'sPaginationType': 'full_numbers',
		'oLanguage': {
			'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
		},
	});
	$('.btnatras').button({ icons: { primary: 'ui-icon ui-icon-arrowthick-1-w' }});
});
</script>
</head>
<body>
<?php require($r.'incluir/src/login.php') ?>
<section id="principal">
<?php require($r.'incluir/src/cabeza.php') ?>
<?php require($r.'incluir/src/menu.php') ?>
<article id="cuerpo">
<article class="mapa">
<a href="#">Principal</a><div class="mapa_div"></div><a href="#">Cartera</a><div class="mapa_div"></div><a class="current">Reportes</a>
</article>
<article id="contenido">
<?php
$usuario = $db->query("SELECT * FROM usuarios where usuid = $promotor;");
$rowusuario = $usuario->fetch(PDO::FETCH_ASSOC);

$gestiondia = $db->query("SELECT * FROM hiscarteras INNER JOIN  carteras on (hiscuenta = carfactura and hisempresa = carempresa) WHERE DATE(hisfecha) = '$fecha' and hisusuario = $promotor ;");
?>
<h2>Reporte resumen de gestion diaria del  <?php echo $rowusuario['usunombre'].' el '.$fecha ?>  </h2>
</br>
<div class="reporte">
<a href="#"><img src="<?php echo $r ?>imagenes/iconos/pdf.png" title="pdf" /></a> <a href="excelgestion.php?<?php echo $filtro ?>"><img src="<?php echo $r ?>imagenes/iconos/excel.png" title="csv" /></a>
</div>
<table class="tabla">
<thead>
<tr>
<th>Empresa</th>
<th>Cliente</th>
<th>cuenta</th>
<th>Fecha</th>
<th>Nota</th>
</tr>
</thead>
<tbody>
<?php 
while($rowgestiondia = $gestiondia->fetch(PDO::FETCH_ASSOC)){	

$empresa = $db->query("SELECT * FROM empresas where empid = '".$rowgestiondia['hisempresa']."';");
$empresanom = $empresa->fetch(PDO::FETCH_ASSOC);

echo "<tr> <td>".$empresanom['empnombre']."</td> <td> ".$rowgestiondia['carcliente']." </td> <td> ".$rowgestiondia['hiscuenta']." </td> <td> ".$rowgestiondia['hisfecha']." </td> <td> ".$rowgestiondia['hisnota']." </td> </tr>";
} //cierre while gestiondia
?>
</tbody>
</table>
<?php  ?>
<p class="boton">
<button type="button" class="btnatras" onClick="carga(); location.href='congestion.php'">atras</button>
</p>
</article>
</article>
<?php require($r.'incluir/src/pie.php') ?>
</section>
</body>
</html>