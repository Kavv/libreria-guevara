<?php
$r = '../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
require($r.'incluir/funciones.php');  
require($r.'incluir/fpdf/fpdf.php');

$fecha1 = $_GET['fecha1'];
$fecha2 = $_GET['fecha2'];
$empresa = $_GET['empresa'];

$filtro = 'fecha1='.$fecha1.'&fecha2='.$fecha2.'&empresa='.$empresa;

$sql = "SELECT * FROM movimientos WHERE movprefijo IN ('RC') AND DATE(movfecha) BETWEEN '".$fecha1."' AND '".$fecha2."' AND movempresa = '".$empresa."' AND movsaldo > 1000000 order by movtercero, movdocumento ;";
$qry = $db->query($sql);

$qryempresa = $db->query("SELECT * FROM empresas WHERE empid = '".$empresa."';");
$rowempresa = $qryempresa->fetch(PDO::FETCH_ASSOC);

$nombre_empresa = $rowempresa['empnombre'];

class PDF extends FPDF{
	function Header(){
		global $fecha1;
		global $fecha2;
		global $empresa;
		global $nombre_empresa;
    	$this->SetFont('LucidaConsole','',6);
		$this->Cell(50,5,date('Y/m/d'),0,1);
		$this->SetFont('LucidaConsole','',8);
		$this->Cell(280,5,'--------------------------------------------------------------------------------------------------------------------------------------------------------------------',0,1,'C');
    	$this->Cell(280,5,'MEDIOS MAGNETICOS ENTRE EL INVERVALO DEL '.$fecha1.' AL '.$fecha2.' SALDOS MAYORES A UN MILLON DE LA EMPRESA '.$nombre_empresa.' ',0,1,'C');
		$this->Cell(280,5,'--------------------------------------------------------------------------------------------------------------------------------------------------------------------',0,1,'C');
    	$this->Ln(5);
	}

	function Footer(){
    	$this->SetY(-15);
    	$this->SetFont('LucidaConsole','',8);
		$this->Cell(0,5,'----------------------------------------------------------------------------------------------------------------------------------------------------------------------',0,1,'C');
		$this->SetFont('LucidaConsole','',8);
    	$this->Cell(0,5,'Pagina '.$this->PageNo().'/{nb}',0,0,'R');
	}
}

$pdf = new PDF();
$pdf->AddFont('LucidaConsole','','lucon.php');
$pdf->AliasNbPages();
$pdf->AddPage('L');

$pdf->SetFont('LucidaConsole','',8);
$pdf->SetFillColor(220,220,220);
$pdf->SetX(8);
// Suma total 260
$pdf->Cell(25,5,'CONCEPTO',1,0,'C',true);
$pdf->Cell(25,5,'TIPO DOCU',1,0,'C',true);
$pdf->Cell(20,5,'NUM IDEN',1,0,'C',true);
$pdf->Cell(27,5,'DV',1,0,'C',true);
$pdf->Cell(25,5,'1 APELLIDO',1,0,'C',true);
$pdf->Cell(25,5,'2 APELLIDO',1,0,'C',true);
$pdf->Cell(25,5,'1 NOMBRE',1,0,'C',true);
$pdf->Cell(25,5,'2 NOMBRE',1,0,'C',true);
$pdf->Cell(25,5,'RAZON SOCIAL',1,0,'C',true);
$pdf->Cell(15,5,'PAIS',1,0,'C',true);
$pdf->Cell(20,5,'I.R',1,0,'C',true);
$pdf->Cell(25,5,'FINANCIACION',1,1,'C',true);

$i = 1;
$movtercero = '';
$movdocumento = '';

while($row = $qry->fetch(PDO::FETCH_ASSOC)){

if($movtercero == $row['movtercero'] && $movdocumento == $row['movdocumento'])
{
	///SALTA EL PASO
}
else
{
	$sqldv = "SELECT * FROM movimientos WHERE movtercero = '".$row['movtercero']."' AND movdocumento = '".$row['movdocumento']."' AND movprefijo = 'DV' "; // determinar si hay devolucion
	$qrydv = $db->query($sqldv);
	$rowdv = $qrydv->rowCount();
		if ($rowdv == 0)
		{
		
			$sqlx = "SELECT max(movnumero) as ultimo_pago FROM movimientos  WHERE movtercero = '".$row['movtercero']."' AND DATE(movfecha) BETWEEN '".$fecha1."' AND '".$fecha2."' ";
			$qryx = $db->query($sqlx);
			$rowx = $qryx->fetch(PDO::FETCH_ASSOC);
			
			$sql1 = "SELECT * FROM movimientos INNER JOIN clientes ON movtercero = cliid  WHERE movtercero = '".$row['movtercero']."' AND movnumero = '".$rowx['ultimo_pago']."' ";
			$qry1 = $db->query($sql1);
			$row1 = $qry1->fetch(PDO::FETCH_ASSOC);

			$valor_total = $row1['movsaldo'];
			
			if (!empty($row1['movfinan'])) { $financiacion = $row1['movfinan']; } else { $financiacion = 0; }
			
			//echo "<br><br><br><br>".$sql."<br><br>".$sqldv."<br><br>".$sqlx."<br><br>".$sql1."<br><br>".$row1['movsaldo']."<br><br><br><br><br><br>";

			if ($valor_total >= 1000000){
			$total_u = $total_u + $valor_total;
	if($row1['clitide'] == 1){ $tipo = "13";} elseif($row1['clitide'] == 2) {$tipo = "31";}
	if($row1['clitide'] == 1) {$nombre_re = $row1['clinombre'];}
	if($row1['clitide'] == 2) {$razon_social = $row1['clinombre'];}
	$pdf->SetFont('LucidaConsole','',7);
	if($i%2 == 0) $x = 255;
	else $x = 235;
	$pdf->SetFillColor($x,$x,$x);
	$pdf->SetX(8);
	$pdf->Cell(25,5,'4001',1,0,'L',true);
	$pdf->Cell(25,5,$tipo,1,0,'C',true);
	$pdf->Cell(20,5,$row1['cliid'],1,0,'C',true);
	$pdf->Cell(27,5,$row1['clidigito'],1,0,'C',true);
	$pdf->Cell(25,5,$row1['cliape1'],1,0,'C',true);
	$pdf->Cell(25,5,$row1['cliape2'],1,0,'C',true);
	$pdf->Cell(25,5,$nombre_re,1,0,'C',true);
	$pdf->Cell(25,5,$row1['clinom2'],1,0,'C',true);
	$pdf->Cell(25,5,$razon_social,1,0,'L',true);
	$pdf->Cell(15,5,'169',1,0,'C',true);
	$pdf->Cell(20,5,number_format($valor_total,0,',','.'),1,0,'C',true);
	$pdf->Cell(25,5,$financiacion,1,1,'C',true);
	$i++;
			}
		}
}
$movtercero = $row['movtercero'];
$movdocumento = $row['movdocumento'];
}
$total_fin =  $total_u ;

	
	$pdf->SetFont('LucidaConsole','',7);
	if($i%2 == 0) $x = 255;
	else $x = 235;
	$pdf->SetFillColor($x,$x,$x);
	$pdf->SetX(8);
	$pdf->Cell(25,5,'',1,0,'L',true);
	$pdf->Cell(25,5,'',1,0,'C',true);
	$pdf->Cell(20,5,'',1,0,'C',true);
	$pdf->Cell(27,5,'',1,0,'C',true);
	$pdf->Cell(25,5,'',1,0,'C',true);
	$pdf->Cell(25,5,'',1,0,'C',true);
	$pdf->Cell(25,5,'',1,0,'C',true);
	$pdf->Cell(25,5,'',1,0,'C',true);
	$pdf->Cell(25,5,'',1,0,'C',true);
	$pdf->Cell(15,5,'',1,0,'C',true);
	$pdf->Cell(20,5,number_format($total_fin,0,',','.'),1,0,'C',true);
	$pdf->Cell(25,5,'',1,1,'C',true);
	$i++;


$pdf->Output('Medios magneticos entre el invervalo del '.$fecha1.' a '.$fecha2.' Mayores a un millon.pdf','d');
?>