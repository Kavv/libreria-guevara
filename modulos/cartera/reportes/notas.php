<?php
$r = '../../../';
require($r.'incluir/connection.php');
$empresa = $_GET['empresa'];
$id = $_GET['id'];
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>. : : S o p h y a : : .</title>
<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#tabla2').dataTable({
		'bJQueryUI': true,
		'bStateSave': true,
		'bFilter': false,
		'bLengthChange': false,
		'bSort': false,
		'sPaginationType': 'full_numbers',
		'oLanguage': {
			'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
		},
	});
});
</script>
</head>
<body>
<table id="tabla2">
<thead>
<tr>
<th>Fecha</th>
<th>Usuario</th>
<th>Nota</th>
</tr>
</thead>
<tbody>
<?php
$qry = $db->query("SELECT * FROM hissolicitudes INNER JOIN usuarios ON hsousuario = usuid WHERE hsoempresa = $empresa AND hsosolicitud = '$id' ORDER BY hsofecha DESC");
while($row2 = $qry->fetch(PDO::FETCH_ASSOC)){
?>
<tr>
<td align="center"><?php echo $row2['hsofecha'] ?></td>
<td align="center"><?php echo $row2['usunombre'] ?></td>
<td><?php echo $row2['hsonota'] ?></td>
</tr>
<?php
}
?>
</tbody>
</table>
</p>
</body>
</html>