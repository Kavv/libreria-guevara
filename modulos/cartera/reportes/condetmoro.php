<?php
$r = '../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
?>
<!doctype html>
<html>
<head>
	<?php 
		require($r . 'incluir/src/head.php');
		require($r . 'incluir/src/head-form.php');
	?>
<script type="text/javascript">
$(document).ready(function(){
	$('#form').validationEngine({
		showOneMessage:true,
		onValidationComplete: function(form, status){
			if(status == true) {
				carga();
				return true;
			}
		}
	});
	$('.btnconsulta').button({ icons: { primary: 'ui-icon ui-icon-search' }});
	 $('#checkAll').click(function () {    
     	$('input:checkbox').prop('checked', this.checked);    
 });
});
</script>
<style type="text/css">
#form fieldset{ padding: 10px; display:block; width:410px; margin:20px auto }
#form legend{ font-weight: bold; margin-left:5px; padding:5px }
#form label{ display: inline-block; width:60px; margin:1px }
#form p{ margin: 5px 0 }
</style>
</head>
<body>
<?php require($r.'incluir/src/login.php') ?>
<section id="principal">
<?php require($r.'incluir/src/cabeza.php') ?>
<?php require($r.'incluir/src/menu.php') ?>
<article id="cuerpo">
<article class="mapa">
<a href="#">Principal</a><div class="mapa_div"></div><a href="#">Cartera</a><div class="mapa_div"></div><a class="current">Reportes</a>
</article>
<article id="contenido">
<form id="form" name="form" action="lisdetmoro.php" method="post">
<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
<legend class="ui-widget ui-widget-header ui-corner-all">Reporte morosidad detallado</legend>
<p style="margin-left:30px">
<input type="checkbox" id="checkAll" name="promotor" /> TODOS<br />
<?php
$qry = $db->query("SELECT * FROM usuarios WHERE usupromotor = '1' AND usuperfil <> '56' ORDER BY usunombre"); //Todos los promotores exepto prejuridico
while($row = $qry->fetch(PDO::FETCH_ASSOC))
    echo '<input type="checkbox" name="promotor[]" value='.$row['usuid'].'> '.$row['usunombre'].'<br />';
?>
</p>
<p class="boton">
<button type="submit" name="tipo" value="totalizado" class="btnconsulta">consultar</button>
</p>
</fieldset>
</form>
</article>
</article>
<?php require($r.'incluir/src/pie.php') ?>
</section>
</body>
</html>