<?php
$r = '../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
?>
<!doctype html>
<html>
<head>
	<?php 
		require($r . 'incluir/src/head.php');
		require($r . 'incluir/src/head-form.php');
	?>
<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#form').validationEngine({
		showOneMessage:true,
		onValidationComplete: function(form, status){
			if(status == true) {
				carga();
				return true;
			}
		}
	});
	$('#fecha1').datepicker({
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		maxDate: '+0D',
		minDate: '2016-01-01',
		onClose: function( selectedDate ) {
			$('#fecha2').datepicker('option', 'minDate', selectedDate);
		}
	}).keypress(function(event) { event.preventDefault() });
	$('#fecha2').datepicker({
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		maxDate: '+0D',
		onClose: function( selectedDate ) {
			$('#fecha1').datepicker('option', 'maxDate', selectedDate);
		}
	}).keypress(function(event) { event.preventDefault() });
	$('.btnconsulta').button({ icons: { primary: 'ui-icon ui-icon-search' }});
	$('.btnconsulta').button({ icons: { primary: 'ui-icon ui-icon-search' }});
		$('#dialog-message').dialog({
		height: 80,
		width: 'auto',
		modal: true
	});
});
</script>
<style type="text/css">
#form fieldset{ padding: 10px; display:block; width:410px; margin:20px auto }
#form legend{ font-weight: bold; margin-left:5px; padding:5px }
#form label{ display: inline-block; width:60px; margin:1px }
#form p{ margin: 5px 0 }
</style>
</head>
<body>
<?php require($r.'incluir/src/login.php') ?>
<section id="principal">
<?php require($r.'incluir/src/cabeza.php') ?>
<?php require($r.'incluir/src/menu.php') ?>
<article id="cuerpo">
<article class="mapa">
<a href="#">Principal</a><div class="mapa_div"></div><a href="#">Cartera</a><div class="mapa_div"></div><a class="current">Reportes</a>
</article>
<article id="contenido">
<form id="form" name="form" action="lisniffpagos.php" method="post">
<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
<legend class="ui-widget ui-widget-header ui-corner-all">Reporte Niff Pagos</legend>
<p>
<label for="empresa">Empresa:</label>
<select name="empresa" class="empresa" class="validate[required]">
<option class="validate[required]" value='TODAS'>TODAS</option>
<?php
$qry = $db->query("SELECT * FROM empresas ORDER BY empnombre");
while($row = $qry->fetch(PDO::FETCH_ASSOC))
    echo '<option value='.$row['empid'].'>'.$row['empnombre'].'</option>';
?>
</select>
</p>
<p>
<label for="fechas">Fechas:</label>
<input type="text" name="fecha1" id="fecha1" class="fecha validate[required]" /> - <input type="text" name="fecha2" id="fecha2" class="fecha validate[required]" />
</p>
<p class="boton">
<button type="submit" class="btnconsulta">Consultar</button>
</p>
</article>
</article>
<?php require($r.'incluir/src/pie.php') ?>
</section>
<?php
if($_GET['error']) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>'.$_GET['error'].'</div>';
?>
</body>
</html>