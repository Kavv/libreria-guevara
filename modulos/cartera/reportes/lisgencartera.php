<?php
$r = '../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
require($r.'incluir/funciones.php'); 
$fecha1 = $_POST['fecha1'];
$fecha2 = $_POST['fecha2'];


$qryinser1 = $db->query(" INSERT INTO saldocartera (saldocarteraempresa, saldocarteramesano) VALUES ('900051528', '$fecha1');"); //IDENCORP
$qryinser2 = $db->query(" INSERT INTO saldocartera (saldocarteraempresa, saldocarteramesano) VALUES ('900306685', '$fecha1');"); //COOPERATIVA MULTIACTIVA
$qryinser3 = $db->query(" INSERT INTO saldocartera (saldocarteraempresa, saldocarteramesano) VALUES ('900813686', '$fecha1');"); //SOPHYA
$qryinser4 = $db->query(" INSERT INTO saldocartera (saldocarteraempresa, saldocarteramesano) VALUES ('900487926', '$fecha1');"); //ESCUELA DE DESARROLLO

$qryuliden = $db->query("SELECT * FROM saldocartera WHERE saldocarteraempresa = 900051528 order by idsaldocartera desc LIMIT 1"); 
$rowuliden = $qryuliden->fetch(PDO::FETCH_ASSOC);
$ultimoiden = $rowuliden['idsaldocartera'];

$qryulcoopin = $db->query("SELECT * FROM saldocartera WHERE saldocarteraempresa = 900306685 order by idsaldocartera desc LIMIT 1"); 
$rowulcoopin = $qryulcoopin->fetch(PDO::FETCH_ASSOC);
$ultimocoopin = $rowulcoopin['idsaldocartera'];

$qryulsophya = $db->query("SELECT * FROM saldocartera WHERE saldocarteraempresa = 900813686 order by idsaldocartera desc LIMIT 1"); 
$rowulsophya = $qryulsophya->fetch(PDO::FETCH_ASSOC);
$ultimosophya = $rowulsophya['idsaldocartera'];

$qryulescuela = $db->query("SELECT * FROM saldocartera WHERE saldocarteraempresa = 900487926 order by idsaldocartera desc LIMIT 1"); 
$rowulescuela = $qryulescuela->fetch(PDO::FETCH_ASSOC);
$ultimoescuela = $rowulescuela['idsaldocartera'];

#Ventas
$qryventas = $db->query("SELECT movempresa,SUM(movvalor) as ValorT , count(movempresa) as Numero FROM solicitudes 
INNER JOIN movimientos ON solfactura = movnumero AND movempresa = solempresa 
WHERE movfecha BETWEEN '$fecha1' AND '$fecha2' 
AND movprefijo = 'FV' 
AND movestado = 'FACTURADO'
GROUP BY movempresa "); 

while($rowventas = $qryventas->fetch(PDO::FETCH_ASSOC)){
	if ($rowventas['movempresa'] == 900051528){
		$qryupdateventas = $db->query(" UPDATE saldocartera SET saldoventasbrutas='".$rowventas['ValorT']."' , saldoventasnumbrutas='".$rowventas['Numero']."' WHERE idsaldocartera='$ultimoiden'; "); 
	}elseif ($rowventas['movempresa'] == 900306685){
		$qryupdateventas = $db->query(" UPDATE saldocartera SET saldoventasbrutas='".$rowventas['ValorT']."' , saldoventasnumbrutas='".$rowventas['Numero']."' WHERE idsaldocartera='$ultimocoopin'; "); 
	}elseif ($rowventas['movempresa'] == 900813686){
		$qryupdateventas = $db->query(" UPDATE saldocartera SET saldoventasbrutas='".$rowventas['ValorT']."' , saldoventasnumbrutas='".$rowventas['Numero']."' WHERE idsaldocartera='$ultimosophya'; "); 
	}elseif ($rowventas['movempresa'] == 900487926){
		$qryupdateventas = $db->query(" UPDATE saldocartera SET saldoventasbrutas='".$rowventas['ValorT']."' , saldoventasnumbrutas='".$rowventas['Numero']."' WHERE idsaldocartera='$ultimoescuela'; "); 
	}
}




#Ventas a Credito
$qryventacre = $db->query("SELECT movempresa,SUM(movvalor) as ValorT , count(movempresa) as Numero FROM solicitudes 
INNER JOIN movimientos ON solfactura = movnumero AND movempresa = solempresa 
WHERE movfecha BETWEEN '$fecha1' AND '$fecha2' 
AND movprefijo = 'FV' 
AND solncuota > 1 
AND movestado = 'FACTURADO'
GROUP BY movempresa"); 

while($rowventacre = $qryventacre->fetch(PDO::FETCH_ASSOC)){
	if ($rowventacre['movempresa'] == 900051528){
		$qryupdateventacre = $db->query("UPDATE saldocartera SET saldoventascredito='".$rowventacre['ValorT']."' , saldoventasnumcredito='".$rowventacre['Numero']."' WHERE idsaldocartera='$ultimoiden';"); 
	}elseif ($rowventacre['movempresa'] == 900306685){
		$qryupdateventacre = $db->query("UPDATE saldocartera SET saldoventascredito='".$rowventacre['ValorT']."' , saldoventasnumcredito='".$rowventacre['Numero']."' WHERE idsaldocartera='$ultimocoopin';"); 
	}elseif ($rowventacre['movempresa'] == 900813686){
		$qryupdateventacre = $db->query("UPDATE saldocartera SET saldoventascredito='".$rowventacre['ValorT']."' , saldoventasnumcredito='".$rowventacre['Numero']."' WHERE idsaldocartera='$ultimosophya';"); 
	}elseif ($rowventacre['movempresa'] == 900487926){
		$qryupdateventacre = $db->query("UPDATE saldocartera SET saldoventascredito='".$rowventacre['ValorT']."' , saldoventasnumcredito='".$rowventacre['Numero']."' WHERE idsaldocartera='$ultimoescuela';"); 
	}
}

#Ventas a Contado
$qryventacon = $db->query("SELECT movempresa,SUM(movvalor) as ValorT , count(movempresa) as Numero FROM solicitudes 
INNER JOIN movimientos ON solfactura = movnumero AND movempresa = solempresa 
WHERE movfecha BETWEEN '$fecha1' AND '$fecha2' 
AND movprefijo = 'FV' 
AND solncuota <= 1 
AND movestado = 'FACTURADO'
GROUP BY movempresa"); 

while($rowventacon = $qryventacon->fetch(PDO::FETCH_ASSOC)){ 
	if ($rowventacon['movempresa'] == 900051528){
		$qryupdateventacon = $db->query("UPDATE saldocartera SET saldoventascontado='".$rowventacon['ValorT']."' , saldoventasnumcontado='".$rowventacon['Numero']."' WHERE idsaldocartera='$ultimoiden';"); 
	}elseif ($rowventacon['movempresa'] == 900306685){
		$qryupdateventacon = $db->query("UPDATE saldocartera SET saldoventascontado='".$rowventacon['ValorT']."' , saldoventasnumcontado='".$rowventacon['Numero']."' WHERE idsaldocartera='$ultimocoopin';"); 
	}elseif ($rowventacon['movempresa'] == 900813686){
		$qryupdateventacon = $db->query("UPDATE saldocartera SET saldoventascontado='".$rowventacon['ValorT']."' , saldoventasnumcontado='".$rowventacon['Numero']."' WHERE idsaldocartera='$ultimosophya';"); 
	}elseif ($rowventacon['movempresa'] == 900487926){
		$qryupdateventacon = $db->query("UPDATE saldocartera SET saldoventascontado='".$rowventacon['ValorT']."' , saldoventasnumcontado='".$rowventacon['Numero']."' WHERE idsaldocartera='$ultimoescuela';"); 
	}
}

#Financiación
$qryfinancia = $db->query("SELECT movempresa,SUM(movfinan) as ValorT , count(movempresa) as Numero FROM solicitudes 
INNER JOIN movimientos ON solfactura = movnumero AND movempresa = solempresa 
WHERE movfecha BETWEEN '$fecha1' AND '$fecha2' 
AND movprefijo = 'FV' 
AND movestado = 'FACTURADO'
GROUP BY movempresa"); 

while($rowfinancia = $qryfinancia->fetch(PDO::FETCH_ASSOC)){ // 
	if ($rowfinancia['movempresa'] == 900051528){
		$qryupdatefinancia = $db->query("UPDATE saldocartera SET saldofinanciado='".$rowfinancia['ValorT']."' , saldonumfinanciado='".$rowfinancia['Numero']."' WHERE idsaldocartera='$ultimoiden';"); 
	}elseif ($rowfinancia['movempresa'] == 900306685){
		$qryupdatefinancia = $db->query("UPDATE saldocartera SET saldofinanciado='".$rowfinancia['ValorT']."' , saldonumfinanciado='".$rowfinancia['Numero']."' WHERE idsaldocartera='$ultimocoopin';"); 
	}elseif ($rowfinancia['movempresa'] == 900813686){
		$qryupdatefinancia = $db->query("UPDATE saldocartera SET saldofinanciado='".$rowfinancia['ValorT']."' , saldonumfinanciado='".$rowfinancia['Numero']."' WHERE idsaldocartera='$ultimosophya';"); 
	}elseif ($rowfinancia['movempresa'] == 900487926){
		$qryupdatefinancia = $db->query("UPDATE saldocartera SET saldofinanciado='".$rowfinancia['ValorT']."' , saldonumfinanciado='".$rowfinancia['Numero']."' WHERE idsaldocartera='$ultimoescuela';"); 
	}
}


#Cuotas Iniciales
$qrycuotasin = $db->query("SELECT movempresa, SUM(solcuota) as ValorT , count(movempresa) as Numero FROM solicitudes 
INNER JOIN movimientos ON solfactura = movnumero  
WHERE movfecha BETWEEN '$fecha1' AND '$fecha2' 
AND movprefijo = 'FV' 
AND  solcuota > 0 
AND movestado = 'FACTURADO'
GROUP BY movempresa"); 

while($rowcuotasin = $qrycuotasin->fetch(PDO::FETCH_ASSOC)){ 
	if ($rowcuotasin['movempresa'] == 900051528){
		$qryupdatecuotasin = $db->query("UPDATE saldocartera SET saldocuotasiniciales='".$rowcuotasin['ValorT']."' , saldonumcuotasiniciales='".$rowcuotasin['Numero']."' WHERE idsaldocartera='$ultimoiden';"); 
	}elseif ($rowcuotasin['movempresa'] == 900306685){
		$qryupdatecuotasin = $db->query("UPDATE saldocartera SET saldocuotasiniciales='".$rowcuotasin['ValorT']."' , saldonumcuotasiniciales='".$rowcuotasin['Numero']."' WHERE idsaldocartera='$ultimocoopin';"); 
	}elseif ($rowcuotasin['movempresa'] == 900813686){
		$qryupdatecuotasin = $db->query("UPDATE saldocartera SET saldocuotasiniciales='".$rowcuotasin['ValorT']."' , saldonumcuotasiniciales='".$rowcuotasin['Numero']."' WHERE idsaldocartera='$ultimosophya';"); 
	}elseif ($rowcuotasin['movempresa'] == 900487926){
		$qryupdatecuotasin = $db->query("UPDATE saldocartera SET saldocuotasiniciales='".$rowcuotasin['ValorT']."' , saldonumcuotasiniciales='".$rowcuotasin['Numero']."' WHERE idsaldocartera='$ultimoescuela';"); 
	}
}


#RC
$qrycarcapital = $db->query("SELECT movempresa, SUM(movvalor) as ValorT FROM movimientos 
WHERE movfecha BETWEEN '$fecha1' AND '$fecha2' 
AND movprefijo = 'RC' 
AND movestado = 'FINALIZADO'
GROUP BY movempresa"); 

while($rowcarcapital = $qrycarcapital->fetch(PDO::FETCH_ASSOC)){ 
	if ($rowcarcapital['movempresa'] == 900051528){
		$financia = $rowcarcapital['ValorT'] * 0.01;
		$qrycarvali = $db->query("SELECT sum(movvalor) as valorRC FROM movimientos
		WHERE movfecha BETWEEN '$fecha1' AND '$fecha2' 
		AND movprefijo = 'RC'
		AND movempresa = '900051528'
		AND movnumero IN (SELECT movnumero FROM solicitudes 
		INNER JOIN movimientos ON solfactura = movnumero AND movempresa = solempresa 
		WHERE movfecha BETWEEN '$fecha1' AND '$fecha2' 
		AND movprefijo = 'FV' 
		AND solncuota <= 1 
		AND movestado = 'FACTURADO'
		);"); 
		$rowcarvali = $qrycarvali->fetch(PDO::FETCH_ASSOC);
		$numvali = $qrycarvali->rowCount();
		$carteracapital = $rowcarcapital['ValorT'] - $financia;
		if ($num > 0){
			$carteracapital = $carteracapital - $rowcarvali['valorRC'];
		} 
		$qryupdatecarcapital = $db->query("UPDATE saldocartera SET saldocarteracapital='".$carteracapital."', saldocarterafinanciacion='".$financia."' WHERE idsaldocartera='$ultimoiden';"); 
	}elseif ($rowcarcapital['movempresa'] == 900306685){
		$financia = $rowcarcapital['ValorT'] * 0.01;
		$qrycarvali = $db->query("SELECT sum(movvalor) as valorRC FROM movimientos
		WHERE movfecha BETWEEN '$fecha1' AND '$fecha2' 
		AND movprefijo = 'RC'
		AND movempresa = '900306685'
		AND movnumero IN (SELECT movnumero FROM solicitudes 
		INNER JOIN movimientos ON solfactura = movnumero AND movempresa = solempresa 
		WHERE movfecha BETWEEN '$fecha1' AND '$fecha2' 
		AND movprefijo = 'FV' 
		AND solncuota <= 1 
		AND movestado = 'FACTURADO'
		);"); 
		$rowcarvali = $qrycarvali->fetch(PDO::FETCH_ASSOC);
		$numvali = $qrycarvali->rowCount();
		$carteracapital = $rowcarcapital['ValorT'] - $financia;
		if ($num > 0){
			$carteracapital = $carteracapital - $rowcarvali['valorRC'];
		} 
		$qryupdatecarcapital = $db->query("UPDATE saldocartera SET saldocarteracapital='".$carteracapital."', saldocarterafinanciacion='".$financia."' WHERE idsaldocartera='$ultimocoopin';"); 
	}elseif ($rowcarcapital['movempresa'] == 900813686){
		$financia = $rowcarcapital['ValorT'] * 0.01;
		$qrycarvali = $db->query("SELECT sum(movvalor) as valorRC FROM movimientos
		WHERE movfecha BETWEEN '$fecha1' AND '$fecha2' 
		AND movprefijo = 'RC'
		AND movempresa = '900813686'
		AND movnumero IN (SELECT movnumero FROM solicitudes 
		INNER JOIN movimientos ON solfactura = movnumero AND movempresa = solempresa 
		WHERE movfecha BETWEEN '$fecha1' AND '$fecha2' 
		AND movprefijo = 'FV' 
		AND solncuota <= 1 
		AND movestado = 'FACTURADO'
		);"); 
		$rowcarvali = $qrycarvali->fetch(PDO::FETCH_ASSOC);
		$numvali = $qrycarvali->rowCount();
		$carteracapital = $rowcarcapital['ValorT'] - $financia;
		if ($num > 0){
			$carteracapital = $carteracapital - $rowcarvali['valorRC'];
		} 
		$qryupdatecarcapital = $db->query("UPDATE saldocartera SET saldocarteracapital='".$carteracapital."', saldocarterafinanciacion='".$financia."' WHERE idsaldocartera='$ultimosophya';"); 
	}elseif ($rowcarcapital['movempresa'] == 900487926){
		$financia = $rowcarcapital['ValorT'] * 0.01;
		$qrycarvali = $db->query("SELECT sum(movvalor) as valorRC FROM movimientos
		WHERE movfecha BETWEEN '$fecha1' AND '$fecha2' 
		AND movprefijo = 'RC'
		AND movempresa = '900487926'
		AND movnumero IN (SELECT movnumero FROM solicitudes 
		INNER JOIN movimientos ON solfactura = movnumero AND movempresa = solempresa 
		WHERE movfecha BETWEEN '$fecha1' AND '$fecha2' 
		AND movprefijo = 'FV' 
		AND solncuota <= 1 
		AND movestado = 'FACTURADO'
		);"); 
		$rowcarvali = $qrycarvali->fetch(PDO::FETCH_ASSOC);
		$numvali = $qrycarvali->rowCount();
		$carteracapital = $rowcarcapital['ValorT'] - $financia;
		if ($num > 0){
			$carteracapital = $carteracapital - $rowcarvali['valorRC'];
		} 
		$qryupdatecarcapital = $db->query("UPDATE saldocartera SET saldocarteracapital='".$carteracapital."', saldocarterafinanciacion='".$financia."' WHERE idsaldocartera='$ultimoescuela';"); 
	}
}


#Descuentos
$qrydescufinancia = $db->query("SELECT movempresa, SUM(movdescuento) as ValorT , count(movempresa) as Numero FROM movimientos
WHERE movfecha BETWEEN '$fecha1' AND '$fecha2' 
AND movprefijo = 'RC'
GROUP BY movempresa"); 

while($rowdescufinancia = $qrydescufinancia->fetch(PDO::FETCH_ASSOC)){ 
	if ($rowdescufinancia['movempresa'] == 900051528){
		$descufinancia = $rowdescufinancia['ValorT'] * 0.01;
		$totaldescuen = $rowdescufinancia['ValorT'] - $descufinancia;
		$qryupdatedescufinancia = $db->query("UPDATE saldocartera SET saldodescuentoscapital='".$totaldescuen."',  saldodescuentosfinanciacion='".$descufinancia."'  , saldonumdescuentoscapital='".$rowdescufinancia['Numero']."' WHERE idsaldocartera='$ultimoiden';"); 
	}elseif ($rowdescufinancia['movempresa'] == 900306685){
		$descufinancia = $rowdescufinancia['ValorT'] * 0.01;
		$totaldescuen = $rowdescufinancia['ValorT'] - $descufinancia;
		$qryupdatedescufinancia = $db->query("UPDATE saldocartera SET saldodescuentoscapital='".$totaldescuen."',  saldodescuentosfinanciacion='".$descufinancia."' , saldonumdescuentoscapital='".$rowdescufinancia['Numero']."' WHERE idsaldocartera='$ultimocoopin';"); 
	}elseif ($rowdescufinancia['movempresa'] == 900813686){
		$descufinancia = $rowdescufinancia['ValorT'] * 0.01;
		$totaldescuen = $rowdescufinancia['ValorT'] - $descufinancia;
		$qryupdatedescufinancia = $db->query("UPDATE saldocartera SET saldodescuentoscapital='".$totaldescuen."',  saldodescuentosfinanciacion='".$descufinancia."' , saldonumdescuentoscapital='".$rowdescufinancia['Numero']."' WHERE idsaldocartera='$ultimosophya';"); 
	}elseif ($rowdescufinancia['movempresa'] == 900487926){
		$descufinancia = $rowdescufinancia['ValorT'] * 0.01;
		$totaldescuen = $rowdescufinancia['ValorT'] - $descufinancia;
		$qryupdatedescufinancia = $db->query("UPDATE saldocartera SET saldodescuentoscapital='".$totaldescuen."',  saldodescuentosfinanciacion='".$descufinancia."' , saldonumdescuentoscapital='".$rowdescufinancia['Numero']."' WHERE idsaldocartera='$ultimoescuela';"); 
	}
}


#ND
$qrynotasde = $db->query("SELECT movempresa, SUM(movvalor) as ValorT , count(movempresa) as Numero FROM movimientos 
WHERE movfecha BETWEEN '$fecha1' AND '$fecha2' 
AND movprefijo = 'ND' 
AND movestado = 'FINALIZADO'
GROUP BY movempresa"); 

while($rownotasde = $qrynotasde->fetch(PDO::FETCH_ASSOC)){ 
	if ($rownotasde['movempresa'] == 900051528){
		$qryupdatenotasde = $db->query("UPDATE saldocartera SET saldonotasdebito='".$rownotasde['ValorT']."' , saldonumnotasdebito='".$rownotasde['Numero']."' WHERE idsaldocartera='$ultimoiden';"); 
	}elseif ($rownotasde['movempresa'] == 900306685){
		$qryupdatenotasde = $db->query("UPDATE saldocartera SET saldonotasdebito='".$rownotasde['ValorT']."' , saldonumnotasdebito='".$rownotasde['Numero']."' WHERE idsaldocartera='$ultimocoopin';"); 
	}elseif ($rownotasde['movempresa'] == 900813686){
		$qryupdatenotasde = $db->query("UPDATE saldocartera SET saldonotasdebito='".$rownotasde['ValorT']."' , saldonumnotasdebito='".$rownotasde['Numero']."' WHERE idsaldocartera='$ultimosophya';"); 
	}elseif ($rownotasde['movempresa'] == 900487926){
		$qryupdatenotasde = $db->query("UPDATE saldocartera SET saldonotasdebito='".$rownotasde['ValorT']."' , saldonumnotasdebito='".$rownotasde['Numero']."' WHERE idsaldocartera='$ultimoescuela';"); 
	}
}


#NC
$qrynotascred = $db->query("SELECT movempresa, SUM(movvalor) as ValorT , count(movempresa) as Numero FROM movimientos 
WHERE movfecha BETWEEN '$fecha1' AND '$fecha2' 
AND movprefijo = 'NC' 
AND movestado = 'FINALIZADO'
GROUP BY movempresa"); 

while($rownotascred = $qrynotascred->fetch(PDO::FETCH_ASSOC)){ 
	if ($rownotascred['movempresa'] == 900051528){
		$qryupdatenotascred = $db->query("UPDATE saldocartera SET saldonotascredito='".$rownotascred['ValorT']."' , saldonumnotascredito='".$rownotascred['Numero']."' WHERE idsaldocartera='$ultimoiden';"); 
	}elseif ($rownotascred['movempresa'] == 900306685){
		$qryupdatenotascred = $db->query("UPDATE saldocartera SET saldonotascredito='".$rownotascred['ValorT']."' , saldonumnotascredito='".$rownotascred['Numero']."' WHERE idsaldocartera='$ultimocoopin';"); 
	}elseif ($rownotascred['movempresa'] == 900813686){
		$qryupdatenotascred = $db->query("UPDATE saldocartera SET saldonotascredito='".$rownotascred['ValorT']."' , saldonumnotascredito='".$rownotascred['Numero']."' WHERE idsaldocartera='$ultimosophya';"); 
	}elseif ($rownotascred['movempresa'] == 900487926){
		$qryupdatenotascred = $db->query("UPDATE saldocartera SET saldonotascredito='".$rownotascred['ValorT']."' , saldonumnotascredito='".$rownotascred['Numero']."' WHERE idsaldocartera='$ultimoescuela';"); 
	}
}


#DV
$qrydevoluci = $db->query("SELECT movempresa, SUM(movvalor) as ValorT , count(movempresa) as Numero FROM movimientos 
WHERE movfecha BETWEEN '$fecha1' AND '$fecha2' 
AND movprefijo = 'DV' 
AND movestado = 'FINALIZADO'
GROUP BY movempresa"); 

while($rowdevoluci = $qrydevoluci->fetch(PDO::FETCH_ASSOC)){ 
	if ($rowdevoluci['movempresa'] == 900051528){
		$qryupdatedevoluci = $db->query("UPDATE saldocartera SET saldodevoluciones='".$rowdevoluci['ValorT']."' , saldonumdevoluciones='".$rowdevoluci['Numero']."' WHERE idsaldocartera='$ultimoiden';"); 
	}elseif ($rowdevoluci['movempresa'] == 900306685){
		$qryupdatedevoluci = $db->query("UPDATE saldocartera SET saldodevoluciones='".$rowdevoluci['ValorT']."'  , saldonumdevoluciones='".$rowdevoluci['Numero']."' WHERE idsaldocartera='$ultimocoopin';"); 
	}elseif ($rowdevoluci['movempresa'] == 900813686){
		$qryupdatedevoluci = $db->query("UPDATE saldocartera SET saldodevoluciones='".$rowdevoluci['ValorT']."'  , saldonumdevoluciones='".$rowdevoluci['Numero']."' WHERE idsaldocartera='$ultimosophya';"); 
	}elseif ($rowdevoluci['movempresa'] == 900487926){
		$qryupdatedevoluci = $db->query("UPDATE saldocartera SET saldodevoluciones='".$rowdevoluci['ValorT']."'  , saldonumdevoluciones='".$rowdevoluci['Numero']."' WHERE idsaldocartera='$ultimoescuela';"); 
	}
}


#Saldo Cartera total Iden
$qrycarteraiden = $db->query("SELECT carempresa,SUM(carsaldo) as ValorT, COUNT(carsaldo) as ClientesT 
FROM carteras
WHERE carfecha <= '$fecha2' 
AND carsaldo > 0
AND carempresa NOT IN ('900487926', '900306685','900813686')"); 

while($rowcarteraiden = $qrycarteraiden->fetch(PDO::FETCH_ASSOC)){ 
	$qryupdatecarteraiden = $db->query("UPDATE saldocartera SET saldocarteravalor='".$rowcarteraiden['ValorT']."', saldocarteranumclientes='".$rowcarteraiden['ClientesT']."' WHERE idsaldocartera='$ultimoiden';"); 
}



#Saldo Cartera total las otras
$qrycarteraotras = $db->query("SELECT carempresa,SUM(carsaldo) as ValorT, COUNT(carsaldo) as ClientesT 
FROM carteras
WHERE carfecha <= '$fecha2' 
AND carsaldo > 0
AND carempresa IN ('900487926', '900306685','900813686')
GROUP BY carempresa"); 

while($rowcarteraotras = $qrycarteraotras->fetch(PDO::FETCH_ASSOC)){ 
	if ($rowcarteraotras['carempresa'] == 900306685){
		$qryupdatecarteraotras = $db->query("UPDATE saldocartera SET saldocarteravalor='".$rowcarteraotras['ValorT']."', saldocarteranumclientes='".$rowcarteraotras['ClientesT']."' WHERE idsaldocartera='$ultimocoopin';"); 
	}elseif ($rowcarteraotras['carempresa'] == 900813686){
		$qryupdatecarteraotras = $db->query("UPDATE saldocartera SET saldocarteravalor='".$rowcarteraotras['ValorT']."', saldocarteranumclientes='".$rowcarteraotras['ClientesT']."' WHERE idsaldocartera='$ultimosophya';"); 
	}elseif ($rowcarteraotras['carempresa'] == 900487926){
		$qryupdatecarteraotras = $db->query("UPDATE saldocartera SET saldocarteravalor='".$rowcarteraotras['ValorT']."', saldocarteranumclientes='".$rowcarteraotras['ClientesT']."' WHERE idsaldocartera='$ultimoescuela';"); 
	}
}



#CASTIGADA IdenCorp
$qrycarcastiiden = $db->query("SELECT carempresa,SUM(carsaldo) as ValorTotal , COUNT(carsaldo) as ClientesTotal
FROM carteras
WHERE carfecha <= '$fecha2' 
AND carestado = 'CASTIGADA'
AND carsaldo > 0
AND carempresa NOT IN ('900487926', '900306685','900813686')"); 

$rowcarcastiiden= $qrycarcastiiden->fetch(PDO::FETCH_ASSOC);
$qryupdatecarcastiiden = $db->query("UPDATE saldocartera SET saldocarteracastigada='".$rowcarcastiiden['ValorTotal']."', saldocarteranumcastigada='".$rowcarcastiiden['ClientesTotal']."' WHERE idsaldocartera='$ultimoiden';"); 
 


#CASTIGADA las otras
$qrycastiotras = $db->query("SELECT carempresa,SUM(carsaldo) as ValorT, COUNT(carsaldo) as ClientesT 
FROM carteras
WHERE carfecha <= '$fecha2' 
AND carestado = 'CASTIGADA'
AND carsaldo > 0
AND carempresa IN ('900487926', '900306685','900813686')
GROUP BY carempresa"); 

while($rowcastiotras = $qrycastiotras->fetch(PDO::FETCH_ASSOC)){ 
	if ($rowcastiotras['carempresa'] == 900306685){
		$qryupdatecastiotras = $db->query("UPDATE saldocartera SET saldocarteracastigada='".$rowcastiotras['ValorT']."', saldocarteranumcastigada='".$rowcastiotras['ClientesT']."' WHERE idsaldocartera='$ultimocoopin';"); 
	}elseif ($rowcastiotras['carempresa'] == 900813686){
		$qryupdatecastiotras = $db->query("UPDATE saldocartera SET saldocarteracastigada='".$rowcastiotras['ValorT']."', saldocarteranumcastigada='".$rowcastiotras['ClientesT']."' WHERE idsaldocartera='$ultimosophya';"); 
	}elseif ($rowcastiotras['carempresa'] == 900487926){
		$qryupdatecastiotras = $db->query("UPDATE saldocartera SET saldocarteracastigada='".$rowcastiotras['ValorT']."', saldocarteranumcastigada='".$rowcastiotras['ClientesT']."' WHERE idsaldocartera='$ultimoescuela';"); 
	}
}


#Castigada Pagada --- Mes anterior VS ACTUAL
$mespasado = strtotime ( '-1 month' , strtotime ( $fecha1 ) ) ; // Resta un mes a la fecha 1
$mespasado = date ( 'Y-m-d' , $mespasado );
$qrycarcastigadapagada = $db->query("SELECT * FROM saldocartera WHERE saldocarteramesano = '$mespasado' order by idsaldocartera desc "); 
while($rowcarcastigadapagada = $qrycarcastigadapagada->fetch(PDO::FETCH_ASSOC)){
	if ($rowcarcastigadapagada['saldocarteraempresa'] == 900051528){
		$castigadamespasado = $rowcarcastigadapagada['saldocarteracastigada'];
		$qryconultimacastigada = $db->query("SELECT * from saldocartera WHERE idsaldocartera = '$ultimoiden';"); 
		$rowconultimacastigada = $qryconultimacastigada->fetch(PDO::FETCH_ASSOC);
		$castigadadelmes = $rowconultimacastigada['saldocarteracastigada'];
		$totalpagadadelmes = $castigadamespasado - $castigadadelmes;
		$qryupdatecastiotras = $db->query("UPDATE saldocartera SET saldocastigadapagada='".$totalpagadadelmes."' WHERE idsaldocartera='$ultimoiden';"); 
		
	} elseif ($rowcarcastigadapagada['saldocarteraempresa'] == 900306685){	
		$castigadamespasado = $rowcarcastigadapagada['saldocarteracastigada'];
		$qryconultimacastigada = $db->query("SELECT * from saldocartera WHERE idsaldocartera = '$ultimocoopin';"); 
		$rowconultimacastigada = $qryconultimacastigada->fetch(PDO::FETCH_ASSOC);
		$castigadadelmes = $rowconultimacastigada['saldocarteracastigada'];
		$totalpagadadelmes = $castigadamespasado - $castigadadelmes;
		$qryupdatecastiotras = $db->query("UPDATE saldocartera SET saldocastigadapagada='".$totalpagadadelmes."' WHERE idsaldocartera='$ultimocoopin';"); 
	} elseif ($rowcarcastigadapagada['saldocarteraempresa'] == 900813686){
		$castigadamespasado = $rowcarcastigadapagada['saldocarteracastigada'];
		$qryconultimacastigada = $db->query("SELECT * from saldocartera WHERE idsaldocartera = '$ultimosophya';"); 
		$rowconultimacastigada = $qryconultimacastigada->fetch(PDO::FETCH_ASSOC);
		$castigadadelmes = $rowconultimacastigada['saldocarteracastigada'];
		$totalpagadadelmes = $castigadamespasado - $castigadadelmes;
		$qryupdatecastiotras = $db->query("UPDATE saldocartera SET saldocastigadapagada='".$totalpagadadelmes."' WHERE idsaldocartera='$ultimosophya';"); 
	} elseif ($rowcarteraiden['saldocarteraempresa'] == 900487926){
		$castigadamespasado = $rowcarcastigadapagada['saldocarteracastigada'];
		$qryconultimacastigada = $db->query("SELECT * from saldocartera WHERE idsaldocartera = '$ultimoescuela';"); 
		$rowconultimacastigada = $qryconultimacastigada->fetch(PDO::FETCH_ASSOC);
		$castigadadelmes = $rowconultimacastigada['saldocarteracastigada'];
		$totalpagadadelmes = $castigadamespasado - $castigadadelmes;
		$qryupdatecastiotras = $db->query("UPDATE saldocartera SET saldocastigadapagada='".$totalpagadadelmes."' WHERE idsaldocartera='$ultimoescuela';"); 	
	}
}
	


?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
	<?php 
		require($r . 'incluir/src/head.php');
		require($r . 'incluir/src/head-form.php');
	?>	

<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('.tabla').dataTable({
		'bJQueryUI': true,
		'bStateSave': true,
		'bSort': false,
		'sPaginationType': 'full_numbers',
		'oLanguage': {
			'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
		},
	});
	$('.btnatras').button({ icons: { primary: 'ui-icon ui-icon-arrowthick-1-w' }});
});
</script>
</head>
<body>
<?php require($r.'incluir/src/login.php') ?>
<section id="principal">
<?php require($r.'incluir/src/cabeza.php') ?>
<?php require($r.'incluir/src/menu.php') ?>
<article id="cuerpo">
<article class="mapa">
<a href="#">Principal</a><div class="mapa_div"></div><a href="#">Cartera</a><div class="mapa_div"></div><a class="current">Reportes</a>
</article>
<article id="contenido">

<h2>Reporte para generar cartera del  <?php echo $fecha1." - ".$fecha2 ?>  </h2>
<p><?php echo "Se ha realizado Todo Correctamente"; ?></p>
<p class="boton">
<button type="button" class="btnatras" onClick="carga(); location.href='congencartera.php'">Atras</button>
</p>
</article>
</article>
<?php require($r.'incluir/src/pie.php') ?>
</section>
</body>
</html>