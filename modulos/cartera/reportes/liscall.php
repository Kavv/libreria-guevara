<?php
$r = '../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
require($r.'incluir/funciones.php');


	$fecha1 = "2015-05-01";
	$fecha2 = "2015-07-01";


$filtro = "fecha1=".$fecha1."&fecha2=".$fecha2;

?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
	<?php 
		require($r . 'incluir/src/head.php');
		require($r . 'incluir/src/head-form.php');
	?>	

<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#tabla').dataTable({
		'bJQueryUI': true,
		'bStateSave': true,
		'bSort': false,
		'sPaginationType': 'full_numbers',
		'oLanguage': {
			'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
		},
	});
	$('.btnatras').button({ icons: { primary: 'ui-icon ui-icon-arrowthick-1-w' }});
});
</script>
</head>
<body>
<?php require($r.'incluir/src/login.php') ?>
<section id="principal">
<?php require($r.'incluir/src/cabeza.php') ?>
<?php require($r.'incluir/src/menu.php') ?>
<article id="cuerpo">
<article class="mapa">
<a href="#">Principal</a><div class="mapa_div"></div><a href="#">Cartera</a><div class="mapa_div"></div><a class="current">Reportes</a>
</article>
<article id="contenido">
<h2>Reporte calificaciones de productos desde <?php echo "$fecha1 hasta $fecha2"; ?></h2>
<div class="reporte">
<a href="pdfcalificacion.php?<?php echo $filtro ?>" onClick="carga()"><img src="<?php echo $r ?>imagenes/iconos/pdf.png" title="pdf" /></a> <a href=""><img src="<?php echo $r ?>imagenes/iconos/excel.png" title="csv" /></a>
</div>
<table id="tabla">
<thead>
<tr>
<th>CEDULA</th>
<th>USUARIO</th>
<th>REGISTROS BARRIDOS</th>
<th>NO CONTESTA</th>
<th>SE DEJA MENSAJE EN CONTESTADORA</th>
<th>NO REPICA E INGRESA DIRECTO A BUZON</th>
<th>OTROS TONOS</th>
<th>NO REPICA SE QUEDA MUDO</th>
<th>DATOS DE TELEFONOS IMCOMPLETOS</th>
<th>REGISTRO YA GESTIONADO</th>
<th>CONTACTO</th>
<th>CASA DE FAMILIA</th>
<th>PROSPECTO YA GESTIONADO</th>
<th>SEGUIMIENTO CITA CORPORATIVA</th>
<th>NO EXISTE</th>
</tr>
</thead>
<tbody>
<?php
$qry = $db->query("SELECT * FROM usuarios WHERE usucallcenter = '1' AND usuperfil <> 99");
while($row = $qry->fetch(PDO::FETCH_ASSOC)){



	$qry2 = $db->query("SELECT * FROM hisProspectos WHERE hisprousuario = '".$row['usuid']."' AND DATE(hisprofechahora) BETWEEN '$fecha1' and '$fecha2' ");
	$registrosbarridos = $qry2->rowCount();
	$qrycu = $db->query("SELECT * FROM hisProspectos WHERE hisprousuario = '".$row['usuid']."' AND DATE(hisprofechahora) BETWEEN '$fecha1' and '$fecha2' GROUP BY hisproprospectos ");
	$prospectos = $qrycu->rowCount();
	while($row2 = $qry2->fetch(PDO::FETCH_ASSOC)){
		
		$nocontesta = 0;
		$sedejamensajeencontestadora = 0;
		$norepicabuzon = 0;
		$otrostonos = 0;
		$norepicamudo = 0;
		$telimcompleto = 0;
		$regyagestionado = 0;
		$contacto = 0;
		$casadefamilia = 0;
		$prospectogestionado = 0;
		$seguimientocitacorp = 0;
		$noexiste = 0;
		
		
		if ($row2['hisproresultadollamada'] == 2 ){
			$nocontesta = $nocontesta + 1; 
		}
		if ($row2['hisproresultadollamada'] == 7){
			$sedejamensajeencontestadora = $sedejamensajeencontestadora + 1;
		}
		if ($row2['hisproresultadollamada'] == 9){
			$norepicabuzon = $norepicabuzon + 1;
		}
		if ($row2['hisproresultadollamada'] == 10){
			$otrostonos = $otrostonos + 1;
		}
		if ($row2['hisproresultadollamada'] == 11){
			$norepicamudo = $norepicamudo + 1;
		}
		if ($row2['hisproresultadollamada'] == 12){
			$telimcompleto = $telimcompleto + 1;
		}
		if ($row2['hisproresultadollamada'] == 14){
			$regyagestionado = $regyagestionado + 1;
		}
		if ($row2['hisproresultadollamada'] == 15){
			$contacto = $contacto + 1;
		}
		if ($row2['hisproresultadollamada'] == 16){
			$casadefamilia = $casadefamilia + 1;
		}
		if ($row2['hisproresultadollamada'] == 17){
			$prospectogestionado = $prospectogestionado + 1;
		}
		if ($row2['hisproresultadollamada'] == 18){
			$seguimientocitacorp = $seguimientocitacorp + 1;
		}
		if ($row2['hisproresultadollamada'] == 19){
			$noexiste = $noexiste + 1;
		}
		
		
	}
	
echo "<br><br><br><br><br>SELECT * FROM hisProspectos WHERE hisprousuario = '".$row['usuid']."' AND DATE(hisprofechahora) BETWEEN '$fecha1' and '$fecha2'<br><br><br><br>";
	
?>
<tr>
<td><?php echo $row['usuid'] ?></td>
<td title="<?php echo $row['usuid'] ?>"><?php echo $row['usunombre'] ?></td>
<td align="center"><?php echo $registrosbarridos ?></td>
<!-- RESULTADOS DE LLAMADA -->
<td align="center"><?php echo $nocontesta ?></td>
<td align="center"><?php echo $sedejamensajeencontestadora ?></td>
<td align="center"><?php echo $norepicabuzon ?></td>
<td align="center"><?php echo $otrostonos ?></td>
<td align="center"><?php echo $norepicamudo ?></td>
<td align="center"><?php echo $telimcompleto ?></td>
<td align="center"><?php echo $regyagestionado ?></td>
<td align="center"><?php echo $contacto ?></td>
<td align="center"><?php echo $casadefamilia ?></td>
<td align="center"><?php echo $prospectogestionado ?></td>
<td align="center"><?php echo $seguimientocitacorp ?></td>
<td align="center"><?php echo $noexiste ?></td>


</tr>
<?php

}
?>
</tbody>

</table>
<br>
<p class="boton">
<button type="button" class="btnatras" onClick="carga(); location.href = 'concalificacion.php'">Atras</button>
</p>
</article>
</article>
<?php require($r.'incluir/src/pie.php') ?>
</section>
</body>
</html>