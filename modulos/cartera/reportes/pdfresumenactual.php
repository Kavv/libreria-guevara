<?php
$r = '../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
include($r.'incluir/fpdf/fpdf.php');
$empresa = $_GET['empresa'];
$fecha1 = $_GET['fecha1'];
$fecha2 = $_GET['fecha2'];
$filtro = 'empresa='.$empresa.'&fecha1='.$fecha1.'&fecha2='.$fecha2;
$credito = 0;
$contado = 0;
$ctaini = 0;
$finantotal = 0;
$devoluciones = 0;
$ndebito = 0;
$ncredito = 0;
$cartotal = 0;
$carcapital = 0;
$carcastigada = 0;
$descapital = 0;


if($empresa != 'TODAS') {$qry = $db->query("SELECT * FROM empresas WHERE empid = $empresa"); // row empresa


/*AGREGADO POR MARIA DIAZ PARA DETERMINAR SI EL MES YA FUE CERRADO Y CON CUAL VALOR*/
$anno = substr($fecha2, 0, 4); 
$mes = substr($fecha2, 5, 2);  

$qrysaldo = $db->query("SELECT * FROM saldocartera WHERE saldocarteramesano BETWEEN '".$fecha1."' AND '".$fecha2."' AND saldocarteraempresa = $empresa");
$saldoAnterior = $qrysaldo->rowCount();

$aux = $anno."-".$mes."-01";
$fechacorte = new DateTime($aux);
$qrysaldocartera = $db->query("SELECT sum(saldocarteravalor) as saldocarteravalor, sum(saldocarteranumclientes) as saldocarteranumclientes, sum(saldocarteracastigada) as saldocarteracastigada, sum(saldocarteranumcastigada) as saldocarteranumcastigada FROM saldocartera WHERE saldocarteraempresa = '".$empresa."' AND saldocarteramesano = '".$fechacorte->format('Y-m-d')."'");
$rowsaldocartera = $qrysaldocartera->fetch(PDO::FETCH_ASSOC);

}else{/* ESTAS CONSULTAS SON PARA LA SELECCION DE TODAS LAS EMPRESAS*/

/*AGREGADO POR MARIA DIAZ PARA DETERMINAR SI EL MES YA FUE CERRADO Y CON CUAL VALOR*/
$anno = substr($fecha2, 0, 4); 
$mes = substr($fecha2, 5, 2);  

$qrysaldo = $db->query("SELECT 
sum(saldocarteravalor) as saldocarteravalor, sum(saldocarteranumclientes) as saldocarteranumclientes, 
sum(saldoventasbrutas) as saldoventasbrutas, sum(saldoventasnumbrutas) as saldoventasnumbrutas,
sum(saldoventascredito) as saldoventascredito, sum(saldoventasnumcredito) as saldoventasnumcredito,
sum(saldoventascontado) as saldoventascontado, sum(saldoventasnumcontado) as saldoventasnumcontado,
sum(saldofinanciado) as saldofinanciado, sum(saldonumfinanciado) as saldonumfinanciado,
sum(saldocuotasiniciales) as saldocuotasiniciales, sum(saldonumcuotasiniciales) as saldonumcuotasiniciales,
sum(saldocarteracapital) as saldocarteracapital, sum(saldonumcarteracapital) as saldonumcarteracapital,
sum(saldocarterafinanciacion) as saldocarterafinanciacion,
sum(saldocastigadapagada) as saldocastigadapagada, sum(saldonumcastigadapagada) as saldonumcastigadapagada,
sum(saldodescuentoscapital) as saldodescuentoscapital, sum(saldonumdescuentoscapital) as saldonumdescuentoscapital,
sum(saldodescuentosfinanciacion) as saldodescuentosfinanciacion,
sum(saldonotasdebito) as saldonotasdebito, sum(saldonumnotasdebito) as saldonumnotasdebito,
sum(saldonotascredito) as saldonotascredito, sum(saldonumnotascredito) as saldonumnotascredito,
sum(saldodevoluciones) as saldodevoluciones, sum(saldonumdevoluciones) as saldonumdevoluciones
FROM saldocartera WHERE saldocarteramesano BETWEEN '".$fecha1."' AND '".$fecha2."'");
$saldoAnterior = $qrysaldo->rowCount();


$aux = $anno."-".$mes."-01";
$fechacorte = new DateTime($aux);
$qrysaldocartera = $db->query("SELECT sum(saldocarteravalor) as saldocarteravalor, sum(saldocarteranumclientes) as saldocarteranumclientes, sum(saldocarteracastigada) as saldocarteracastigada, sum(saldocarteranumcastigada) as saldocarteranumcastigada FROM saldocartera WHERE saldocarteramesano = '".$fechacorte->format('Y-m-d')."'");
$rowsaldocartera = $qrysaldocartera->fetch(PDO::FETCH_ASSOC);
}


if($saldoAnterior > 0)
{
	while($rowsaldo = $qrysaldo->fetch(PDO::FETCH_ASSOC)){
	$tlcarterafn = $rowsaldocartera['saldocarteravalor'];
	$numtlcarterafn = $numtlcarterafn + $rowsaldo['saldocarteranumclientes'];
	
	$carcastigada = $rowsaldocartera['saldocarteracastigada'];
	$numcarcastigada = $rowsaldocartera['saldocarteranumcastigada'];

	$venbrutageneral = $venbrutageneral + $rowsaldo['saldoventasbrutas'];
	$numvenbrutageneral = $numvenbrutageneral + $rowsaldo['saldoventasnumbrutas'];
	
	$credito = $credito + $rowsaldo['saldoventascredito'];
	$numcredito = $numcredito + $rowsaldo['saldoventasnumcredito'];
	
	$contado = $contado + $rowsaldo['saldoventascontado'];
	$numcontado = $numcontado + $rowsaldo['saldoventasnumcontado'];
	
	$finantotal = $finantotal + $rowsaldo['saldofinanciado'];
	$numfinantotal = $numfinantotal + $rowsaldo['saldonumfinanciado'];
	
	$ctaini = $ctaini + $rowsaldo['saldocuotasiniciales'];
	$numctaini = $numctaini + $rowsaldo['saldonumcuotasiniciales'];
	
	$totalcapital = $totalcapital + $rowsaldo['saldocarteracapital'];
	$numtotalcapital = $numtotalcapital + $rowsaldo['saldonumcarteracapital'];
	
	$carfinanciacionmul = $carfinanciacionmul + $rowsaldo['saldocarterafinanciacion'];
	
	$castigadapagada = $castigadapagada + $rowsaldo['saldocastigadapagada'];
	$numcastigadapagada = $numcastigadapagada + $rowsaldo['saldonumcastigadapagada'];	
	
	$descapital = $descapital + $rowsaldo['saldodescuentoscapital'];
	$numdescapital = $numdescapital + $rowsaldo['saldonumdescuentoscapital'];
	
	$desfinanciacionmul = $desfinanciacionmul + $rowsaldo['saldodescuentosfinanciacion'];
	
	$ndebito = $ndebito + $rowsaldo['saldonotasdebito'];
	$numndebito = $numndebito + $rowsaldo['saldonumnotasdebito'];	
	
	$ncredito = $ncredito+ $rowsaldo['saldonotascredito'];	
	$numncredito = $numncredito + $rowsaldo['saldonumnotascredito'];
	
	$devoluciones = $devoluciones + $rowsaldo['saldodevoluciones'];
	$numdevoluciones = $numdevoluciones + $rowsaldo['saldonumdevoluciones'];
	}
}

$row7 = $db->query("SELECT * FROM empresas WHERE empid = '$empresa'")->fetch(PDO::FETCH_ASSOC); // row Nombre de la empresa
$rowpar = $db->query("SELECT * FROM parametros")->fetch(PDO::FETCH_ASSOC); // row de parametros


if( ($tlcarterafn-$carcastigada)<0)
{
$totalCartera = '0';
$totalNumCartera = '0';
}else
{
$totalCartera = $tlcarterafn-$carcastigada;
$totalNumCartera = $numtlcarterafn-$numcarcastigada;
}

$pdf = new FPDF();
$pdf->AddFont('LucidaConsole','','lucon.php');
$pdf->AddPage('P');
$pdf->SetFont('LucidaConsole','',8);
$pdf->Cell(0,5,date('Y/m/d'),0,1);
$pdf->SetFont('LucidaConsole','',11);
$pdf->Cell(0,5,'---------------------------------------------------------------------------------',0,1,'C');
$pdf->Cell(0,5,'REPORTE DE CARTERA DETALLADAS DE '.$fecha1.' AL '.$fecha2,0,1,'C');
if($empresa != 'TODAS') { $pdf->Cell(0,5,'DE LA EMPRESA '.$row7['empnombre'],0,1,'C'); }else { $pdf->Cell(0,5,' DE TODAS LAS EMPRESAS ',0,1,'C'); }
$pdf->Cell(0,5,'---------------------------------------------------------------------------------',0,1,'C');
$pdf->Ln(5);
$pdf->SetFont('LucidaConsole','',8);
$pdf->SetFillColor(220,220,220);
$pdf->SetX(20);
$pdf->Cell(110,5,'VR. VENTAS BRUTAS ENTRE EL INTERVALO DE FECHAS: ',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(40,5,number_format($venbrutageneral,0,',','.'),1,0,'R',true);
$pdf->Cell(20,5,number_format($numvenbrutageneral,0,',','.'),1,1,'R',true);
$pdf->SetFillColor(220,220,220);
$pdf->SetX(20);
$pdf->Cell(110,5,'VR. VENTAS A CREDITO ENTRE EL INTERVALO DE FECHAS: ',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(40,5,number_format($credito,0,',','.'),1,0,'R',true);
$pdf->Cell(20,5,number_format($numcredito,0,',','.'),1,1,'R',true);
$pdf->SetFillColor(220,220,220);
$pdf->SetX(20);
$pdf->Cell(110,5,'VR. CONTADOS ENTRE EL INTERVALO DE FECHAS: ',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(40,5,number_format($contado,0,',','.'),1,0,'R',true);
$pdf->Cell(20,5,number_format($numcontado,0,',','.'),1,1,'R',true);
$pdf->SetFillColor(220,220,220);
$pdf->SetX(20);
$pdf->Cell(110,5,'VR. FINANCIADO ENTRE EL INTERVALO DE FECHAS: ',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(40,5,number_format($finantotal,0,',','.'),1,0,'R',true);
$pdf->Cell(20,5,number_format($numfinantotal,0,',','.'),1,1,'R',true);
$pdf->SetFillColor(220,220,220);
$pdf->SetX(20);
$pdf->Cell(110,5,'VR. CUOTAS INICIALES ENTRE EL INTERVALO DE FECHAS: ',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(40,5,number_format($ctaini,0,',','.'),1,0,'R',true);
$pdf->Cell(20,5,number_format($numctaini,0,',','.'),1,1,'R',true);
$pdf->SetFillColor(220,220,220);
$pdf->SetX(20);
$pdf->Cell(110,5,'VR. CARTERA TOTAL AL '.$fecha2.': ',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(40,5,number_format($totalCartera,0,',','.'),1,0,'R',true);
$pdf->Cell(20,5,number_format($totalNumCartera,0,',','.'),1,1,'R',true);




$pdf->SetFillColor(220,220,220);
$pdf->SetX(20);
$pdf->Cell(110,5,'VR. CARTERA PAGADA ENTRE EL INTERVALO (CAPITAL): ',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(40,5,number_format($totalcapital,0,',','.'),1,0,'R',true);
$pdf->Cell(20,5,number_format($numtotalcapital,0,',','.'),1,1,'R',true);
$pdf->SetFillColor(220,220,220);
$pdf->SetX(20);
$pdf->Cell(110,5,'VR. CARTERA PAGADA ENTRE EL INTERVALO (FINANCIACION): ',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(40,5,number_format($carfinanciacionmul,0,',','.'),1,0,'R',true);
$pdf->Cell(20,5,'---',1,1,'R',true);
$pdf->SetFillColor(220,220,220);
$pdf->SetX(20); 
$pdf->Cell(110,5,'VR. CARTERA CASTIGADA PAGADA ENTRE EL INTERVALO:',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(40,5,number_format(0,0,',','.'),1,0,'R',true);
$pdf->Cell(20,5,'---',1,1,'R',true);
$pdf->SetFillColor(220,220,220);
$pdf->SetX(20);
$pdf->Cell(110,5,'VR. DESCUENTOS ENTRE EL INTERVALO DE FECHAS (CAPITAL): ',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(40,5,number_format($descapital,0,',','.'),1,0,'R',true);
$pdf->Cell(20,5,number_format($numdescapital,0,',','.'),1,1,'R',true);
$pdf->SetFillColor(220,220,220);
$pdf->SetX(20);
$pdf->Cell(110,5,'VR. DESCUENTOS ENTRE EL INTERVALO DE FECHAS (FINANCIACION): ',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(40,5,number_format($desfinanciacionmul,0,',','.'),1,0,'R',true);
$pdf->Cell(20,5,'---',1,1,'R',true);
$pdf->SetFillColor(220,220,220);
$pdf->SetX(20);
$pdf->Cell(110,5,'VR. NOTAS DEBITO ENTRE EL INTERVALO DE FECHAS: ',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(40,5,number_format($ndebito,0,',','.'),1,0,'R',true);
$pdf->Cell(20,5,number_format($numndebito,0,',','.'),1,1,'R',true);
$pdf->SetFillColor(220,220,220);
$pdf->SetX(20);
$pdf->Cell(110,5,'VR. NOTAS CREDITO ENTRE EL INTERVALO DE FECHAS: ',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(40,5,number_format($ncredito,0,',','.'),1,0,'R',true);
$pdf->Cell(20,5,number_format($numncredito,0,',','.'),1,1,'R',true);
$pdf->SetFillColor(220,220,220);
$pdf->SetX(20);
$pdf->Cell(110,5,'VR. DEVOLUCIONES ENTRE EL INTERVALO DE FECHAS:  ',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(40,5,number_format($devoluciones,0,',','.'),1,0,'R',true);
$pdf->Cell(20,5,number_format($numdevoluciones,0,',','.'),1,1,'R',true);
$pdf->SetFillColor(220,220,220);
$pdf->SetX(20);
$pdf->Cell(110,5,'VR. CARTERA CASTIGADA ACTIVA AL '.$fecha2.': ',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(40,5,number_format($carcastigada,0,',','.'),1,0,'R',true);
$pdf->Cell(20,5,number_format($numcarcastigada,0,',','.'),1,1,'R',true);
$pdf->SetFillColor(220,220,220);
$pdf->Ln(5);
$pdf->SetFont('LucidaConsole','',11);
$pdf->Cell(0,5,'---------------------------------------------------------------------------------',0,1,'C');
$pdf->Output('Resumen_cartera.pdf','d');
?>