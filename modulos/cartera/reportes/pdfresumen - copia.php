<?php
$r = '../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
include($r.'incluir/fpdf/fpdf.php');
$empresa = $_GET['empresa'];
$fecha1 = $_GET['fecha1'];
$fecha2 = $_GET['fecha2'];
$filtro = 'empresa='.$empresa.'&fecha1='.$fecha1.'&fecha2='.$fecha2;
$credito = 0;
$contado = 0;
$ctaini = 0;
$finantotal = 0;
$devoluciones = 0;
$ndebito = 0;
$ncredito = 0;
$cartotal = 0;
$carcapital = 0;
$carcastigada = 0;
$descapital = 0;

if($empresa != 'TODAS') {$qry = $db->query("SELECT * FROM empresas WHERE empid = $empresa"); // row empresa


/*AGREGADO POR MARIA DIAZ PARA DETERMINAR SI EL MES YA FUE CERRADO Y CON CUAL VALOR*/
$anno = substr($fecha2, 0, 4); 
$mes = substr($fecha2, 5, 2);  

$qrysaldo = $db->query("SELECT * FROM saldocartera WHERE saldocarteramesano = '".$mes."-".$anno."' AND saldocarteraempresa = $empresa");
$saldoAnterior = $qrysaldo->rowCount();

$sqlgeneral = "SELECT * FROM solicitudes INNER JOIN movimientos ON solfactura = movnumero  WHERE movempresa = '".$empresa."' AND movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'FV' AND movestado <> 'ANULADA'"; // qry general ventas bruto

$sqlventascredito = "SELECT * FROM solicitudes INNER JOIN movimientos ON solfactura = movnumero  WHERE movempresa = '".$empresa."' AND movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'FV' AND  solncuota > 1 AND movestado <> 'ANULADA' ;"; // qry ventas a credito
$sqlventascontado = "SELECT * FROM solicitudes INNER JOIN movimientos ON solfactura = movnumero  WHERE movempresa = '".$empresa."' AND movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'FV' AND  solncuota IN ('1','0') AND movestado <> 'ANULADA';"; // qry ventas de contado
$sqlfinanciado = "SELECT * FROM solicitudes INNER JOIN movimientos ON solfactura = movnumero  WHERE movempresa = '".$empresa."' AND movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'FV' AND  movfinan > 0 AND movestado <> 'ANULADA';"; // qry financiado

$sqlcuotasini =  "SELECT * FROM solicitudes INNER JOIN movimientos ON solfactura = movnumero  WHERE movempresa = '".$empresa."' AND movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'FV' AND  solcuota > 0 AND movestado = 'FACTURADO';"; // qry cuotas ini

/*
$sqlcarterafecha = "
SELECT * FROM  detcarteras D
INNER JOIN carteras on carfactura = D.dcafactura 
WHERE D.dcaempresa = '".$empresa."' 
AND D.dcafepag >= '".$fecha2."' 
AND D.dcafecha < '".$fecha2."' 
order by D.dcafactura, D.dcacuota desc ; 
";

$sqlnumcarterafecha = " 
SELECT * FROM  detcarteras D
INNER JOIN carteras on carfactura = D.dcafactura 
WHERE D.dcaempresa = '".$empresa."' 
AND D.dcafepag >= '".$fecha2."' OR D.dcafepag IS NULL
AND D.dcafecha < '".$fecha2."'  GROUP BY dcafactura ; 
";
*/

$sqlcarcapital = "SELECT * FROM detcarteras inner join solicitudes on solfactura = dcafactura WHERE solncuota > 1 and dcaempresa = '".$empresa."' AND dcafepag BETWEEN '".$fecha1."' AND '".$fecha2."' AND dcaestado = 'CANCELADO'"; //qry descuentos  
$sqlnumcarcapital = "SELECT * FROM detcarteras inner join solicitudes on solfactura = dcafactura WHERE solncuota > 1 and dcaempresa = '".$empresa."' AND dcafepag BETWEEN '".$fecha1."' AND '".$fecha2."' AND dcaestado = 'CANCELADO' group by dcafactura"; //qry descuentos  numero de clientes agrupados por factura
$sqldescapital = "SELECT * FROM detcarteras WHERE dcaempresa = '".$empresa."' AND dcafepag BETWEEN '".$fecha1."' AND '".$fecha2."' AND dcaestado = 'CANCELADO' and dcadescuento > 0"; //qry descuentos 
$sqlnumdescapital = "SELECT * FROM detcarteras WHERE dcaempresa = '".$empresa."' AND dcafepag BETWEEN '".$fecha1."' AND '".$fecha2."' AND dcaestado = 'CANCELADO' and dcadescuento > 0 group by dcafactura"; //qry descuentos  numero de clientes agrupados por factura
$sqlNC = "SELECT * FROM  movimientos  WHERE movempresa = '".$empresa."' AND movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'NC' AND movestado <> 'ANULADA'"; // qry NC = notas credito
$sqlND = "SELECT * FROM  movimientos  WHERE movempresa = '".$empresa."' AND movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'ND' AND movestado <> 'ANULADA'"; // qry ND = notas debito
$sqlDV = "SELECT * FROM  movimientos  WHERE movempresa = '".$empresa."' AND movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'DV' AND movestado <> 'ANULADA'"; // qry DV = devoluciones
$sqlcarcastigada = " SELECT * FROM  detcarteras INNER JOIN carteras on carfactura = dcafactura WHERE dcaempresa = '".$empresa."' AND dcafecha <= '".$fecha2."' AND dcaestado = 'ACTIVA' AND carestado <> 'CANCELADO' AND carestado = 'CASTIGADA' order by dcafecha desc; "; // qry Cartera Castigada 
$sqlnumcarcastigada = " SELECT * FROM  detcarteras INNER JOIN carteras on carfactura = dcafactura WHERE dcaempresa = '".$empresa."' AND dcafecha <= '".$fecha2."' AND dcaestado = 'ACTIVA' AND (carestado <> 'CANCELADO' AND carestado = 'CASTIGADA') group by dcafactura order by dcafecha desc; "; // qry Cartera Castigada numero de clientes

}else{/* ESTAS CONSULTAS SON PARA LA SELECCION DE TODAS LAS EMPRESAS*/

/*AGREGADO POR MARIA DIAZ PARA DETERMINAR SI EL MES YA FUE CERRADO Y CON CUAL VALOR*/
$anno = substr($fecha2, 0, 4); 
$mes = substr($fecha2, 5, 2);  

$qrysaldo = $db->query("SELECT SUM(saldocarteravalor) AS saldocarteravalor, SUM(saldocarteranumclientes) AS saldocarteranumclientes FROM saldocartera WHERE saldocarteramesano = '".$mes."-".$anno."'");
$saldoAnterior = $qrysaldo->rowCount();


$sqlgeneral = "SELECT * FROM solicitudes INNER JOIN movimientos ON solfactura = movnumero  WHERE movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'FV' AND movestado <> 'ANULADA'"; // qry general ventas bruto

$sqlventascredito = "SELECT * FROM solicitudes INNER JOIN movimientos ON solfactura = movnumero  WHERE movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'FV' AND  solncuota > 1 AND movestado <> 'ANULADA' ;"; // qry ventas a credito
$sqlventascontado = "SELECT * FROM solicitudes INNER JOIN movimientos ON solfactura = movnumero  WHERE movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'FV' AND  solncuota IN ('1','0') AND movestado <> 'ANULADA';"; // qry ventas de contado
$sqlfinanciado = "SELECT * FROM solicitudes INNER JOIN movimientos ON solfactura = movnumero  WHERE movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'FV' AND  movfinan > 0 AND movestado <> 'ANULADA';"; // qry financiado

$sqlcuotasini =  "SELECT * FROM solicitudes INNER JOIN movimientos ON solfactura = movnumero  WHERE movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'FV' AND  solcuota > 0 AND movestado = 'FACTURADO';"; // qry cuotas ini

/*
$sqlcarterafecha = "
SELECT * FROM  detcarteras D
INNER JOIN carteras on carfactura = D.dcafactura 
WHERE D.dcafepag >= '".$fecha2."' 
AND D.dcafecha < '".$fecha2."' 
order by D.dcafactura, D.dcacuota desc ; 
";

$sqlnumcarterafecha = " 
SELECT * FROM  detcarteras D
INNER JOIN carteras on carfactura = D.dcafactura 
WHERE D.dcafepag >= '".$fecha2."' OR D.dcafepag IS NULL
AND D.dcafecha < '".$fecha2."'  GROUP BY dcafactura ; 
";
*/

$sqlcarcapital = "SELECT * FROM detcarteras inner join solicitudes on solfactura = dcafactura WHERE solncuota > 1 AND dcafepag BETWEEN '".$fecha1."' AND '".$fecha2."' AND dcaestado = 'CANCELADO'"; //qry descuentos  
$sqlnumcarcapital = "SELECT * FROM detcarteras inner join solicitudes on solfactura = dcafactura WHERE solncuota > 1 AND dcafepag BETWEEN '".$fecha1."' AND '".$fecha2."' AND dcaestado = 'CANCELADO' group by dcafactura"; //qry descuentos  numero de clientes agrupados por factura
$sqldescapital = "SELECT * FROM detcarteras WHERE dcafepag BETWEEN '".$fecha1."' AND '".$fecha2."' AND dcaestado = 'CANCELADO' and dcadescuento > 0"; //qry descuentos 
$sqlnumdescapital = "SELECT * FROM detcarteras WHERE dcafepag BETWEEN '".$fecha1."' AND '".$fecha2."' AND dcaestado = 'CANCELADO' and dcadescuento > 0 group by dcafactura"; //qry descuentos  numero de clientes agrupados por factura
$sqlNC = "SELECT * FROM  movimientos  WHERE movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'NC' AND movestado <> 'ANULADA'"; // qry NC = notas credito
$sqlND = "SELECT * FROM  movimientos  WHERE movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'ND' AND movestado <> 'ANULADA'"; // qry ND = notas debito
$sqlDV = "SELECT * FROM  movimientos  WHERE movfecha BETWEEN '".$fecha1."' AND '".$fecha2."' AND movprefijo = 'DV' AND movestado <> 'ANULADA'"; // qry DV = devoluciones
$sqlcarcastigada = " SELECT * FROM  detcarteras INNER JOIN carteras on carfactura = dcafactura WHERE dcafecha <= '".$fecha2."' AND dcaestado = 'ACTIVA' AND carestado <> 'CANCELADO' AND carestado = 'CASTIGADA' order by dcafecha desc; "; // qry Cartera Castigada 
$sqlnumcarcastigada = " SELECT * FROM  detcarteras INNER JOIN carteras on carfactura = dcafactura WHERE dcafecha <= '".$fecha2."' AND dcaestado = 'ACTIVA' AND (carestado <> 'CANCELADO' AND carestado = 'CASTIGADA') group by dcafactura order by dcafecha desc; "; // qry Cartera Castigada numero de clientes

}




$qrygeneral = $db->query($sqlgeneral); // consulta general ventas bruto
while($rowgeneral = $qrygeneral->fetch(PDO::FETCH_ASSOC)) {
$venbrutageneral = $venbrutageneral + $rowgeneral['movvalor'];
$numgeneral = $qrygeneral->rowCount();
}

$qryventascredito = $db->query($sqlventascredito); // consulta ventas a credito 
while($rowventascredito = $qryventascredito->fetch(PDO::FETCH_ASSOC)) { 
$credito = $credito + $rowventascredito['movvalor'];	
$numventascredito = $qryventascredito->rowCount();
}

$qryventascontado = $db->query($sqlventascontado); // consulta ventas de contado
while($rowventascontado = $qryventascontado->fetch(PDO::FETCH_ASSOC)) { 
$contado = $contado + $rowventascontado['movvalor'];	
$numventascontado = $qryventascontado->rowCount();
}

$qryfinanciado = $db->query($sqlfinanciado); // consulta financiado
while($rowfinanciado = $qryfinanciado->fetch(PDO::FETCH_ASSOC)) { 
$finantotal = $finantotal + $rowfinanciado['movfinan'];
$numfinanciado = $qryfinanciado->rowCount();
}

$qrycuotasini = $db->query($sqlcuotasini); // consulta cuotas iniciales
while($rowcuotasini = $qrycuotasini->fetch(PDO::FETCH_ASSOC)) { 
$ctaini = $ctaini + $rowcuotasini['solcuota'];
$numcuotasini = $qrycuotasini->rowCount();
}

if($saldoAnterior > 0)
{
	$rowsaldo = $qrysaldo->fetch(PDO::FETCH_ASSOC);
	$tlcarterafn = $rowsaldo['saldocarteravalor'];
	$numcarfn = $rowsaldo['saldocarteranumclientes'];
}
else
{
	// Consulta Para Cartera Total a la fecha y Numero de clientes
	$qry = $db->query("SELECT * FROM usuarios WHERE usupromotor = '1'");
	while($row = $qry->fetch(PDO::FETCH_ASSOC)){
		$tlcartera = 0;
		$num = 0;
		if ($empresa != 'TODAS'){
		$qryemp = $db->query("SELECT * FROM empresas where empid = $empresa");
		} else {
		$qryemp = $db->query("SELECT * FROM empresas");
		}
		while($rowemp = $qryemp->fetch(PDO::FETCH_ASSOC)){
			$qry2 = $db->query("SELECT * FROM carteras WHERE carempresa = ".$rowemp['empid']." AND carpromotor = '".$row['usuid']."' AND (carestado = 'ACTIVA' OR carestado = 'CASTIGADA') AND carfecha <= '$fecha2'");
			$num = $num + $qry2->rowCount();
			while($row2 = $qry2->fetch(PDO::FETCH_ASSOC)){
				$tlcartera = $tlcartera + $row2['cartotal'];
			}
		}
		
		$numcarfn = $numcarfn + $num; // numero de clientes de cartera a la fecha
		$tlcarterafn = $tlcarterafn + $tlcartera;
	}
}

$qrycarcapital = $db->query($sqlcarcapital); // cartera Pagada Capital
$qrynumcarcapital = $db->query($sqlnumcarcapital); // numero de clientes agrupados por facturas
while($rowcarcapital = $qrycarcapital->fetch(PDO::FETCH_ASSOC)) { 
 $carcapital = $carcapital + $rowcarcapital['dcavalor'];
$numcarcapital = $qrynumcarcapital->rowCount();
}

$qrydescapital = $db->query($sqldescapital); // Descuentos Capital
$qrynumdescapital = $db->query($sqlnumdescapital); // numero de clientes agrupados por facturas
while($rowdescapital = $qrydescapital->fetch(PDO::FETCH_ASSOC)) { 
$descapital = $descapital + $rowdescapital['dcadescuento'];
$numdescapital = $qrynumdescapital->rowCount();
}

$qryNC = $db->query($sqlNC); // consulta para NOTAS CREDITO
while($rowNC = $qryNC->fetch(PDO::FETCH_ASSOC)) { 
$ncredito = $ncredito + $rowNC['movvalor'];
$numNC = $qryNC->rowCount();
}

$qryND = $db->query($sqlND); // consulta para NOTAS DEBITO
while($rowND = $qryND->fetch(PDO::FETCH_ASSOC)) { 
$ndebito = $ndebito  + $rowND['movvalor'];
$numND = $qryND->rowCount();
}

$qryDV = $db->query($sqlDV); // consulta para DEVOLUCIONES
while($rowDV = $qryDV->fetch(PDO::FETCH_ASSOC)) { 
$devoluciones = $devoluciones  + $rowDV['movvalor'];
$numDV = $qryDV->rowCount();
}

$qrycarcastigada = $db->query($sqlcarcastigada); // consulta para Cartera Castigada
$numqrycarcastigada = $db->query($sqlnumcarcastigada); //numero de clientes con cartera castigada agrupados por factura
while($rowcarcastigada = $qrycarcastigada->fetch(PDO::FETCH_ASSOC)) { 
$carcastigada = $carcastigada  + $rowcarcastigada['carsaldo'];
$numcarcastigada = $numqrycarcastigada->rowCount();
}

$row7 = $db->query("SELECT * FROM empresas WHERE empid = '$empresa'")->fetch(PDO::FETCH_ASSOC); // row Nombre de la empresa
$rowpar = $db->query("SELECT * FROM parametros")->fetch(PDO::FETCH_ASSOC); // row de parametros


$pdf = new FPDF();
$pdf->AddFont('LucidaConsole','','lucon.php');
$pdf->AddPage('P');
$pdf->SetFont('LucidaConsole','',8);
$pdf->Cell(0,5,date('Y/m/d'),0,1);
$pdf->SetFont('LucidaConsole','',11);
$pdf->Cell(0,5,'---------------------------------------------------------------------------------',0,1,'C');
$pdf->Cell(0,5,'REPORTE DE CARTERA DETALLADAS DE '.$fecha1.' AL '.$fecha2,0,1,'C');
if($empresa != 'TODAS') { $pdf->Cell(0,5,'DE LA EMPRESA '.$row7['empnombre'],0,1,'C'); }else { $pdf->Cell(0,5,' DE TODAS LAS EMPRESAS ',0,1,'C'); }
$pdf->Cell(0,5,'---------------------------------------------------------------------------------',0,1,'C');
$pdf->Ln(5);
$pdf->SetFont('LucidaConsole','',8);
$pdf->SetFillColor(220,220,220);
$pdf->SetX(20);
$pdf->Cell(110,5,'VR. VENTAS BRUTAS ENTRE EL INTERVALO DE FECHAS: ',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(40,5,number_format($venbrutageneral,0,',','.'),1,0,'R',true);
$pdf->Cell(20,5,number_format($numgeneral,0,',','.'),1,1,'R',true);
$pdf->SetFillColor(220,220,220);
$pdf->SetX(20);
$pdf->Cell(110,5,'VR. VENTAS A CREDITO ENTRE EL INTERVALO DE FECHAS: ',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(40,5,number_format($credito,0,',','.'),1,0,'R',true);
$pdf->Cell(20,5,number_format($numventascredito,0,',','.'),1,1,'R',true);
$pdf->SetFillColor(220,220,220);
$pdf->SetX(20);
$pdf->Cell(110,5,'VR. FINANCIADO ENTRE EL INTERVALO DE FECHAS: ',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(40,5,number_format($finantotal,0,',','.'),1,0,'R',true);
$pdf->Cell(20,5,number_format($numfinanciado,0,',','.'),1,1,'R',true);
$pdf->SetFillColor(220,220,220);
$pdf->SetX(20);
$pdf->Cell(110,5,'VR. SALDOS INICIALES Y CONTADOS ENTRE EL INTERVALO DE FECHAS: ',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(40,5,number_format($ctaini + $contado,0,',','.'),1,0,'R',true);
$pdf->Cell(20,5,number_format($numcuotasini + $numventascontado,0,',','.'),1,1,'R',true);
$pdf->SetFillColor(220,220,220);
$pdf->SetX(20);
$pdf->Cell(110,5,'VR. CARTERA TOTAL AL '.$fecha2.': ',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(40,5,number_format($tlcarterafn - $carcastigada,0,',','.'),1,0,'R',true);
$pdf->Cell(20,5,number_format($numcarfn - $numcarcastigada,0,',','.'),1,1,'R',true);
$carfinanciacionmul = $carcapital * $rowpar['parfinanciacion'];
$pdf->SetFillColor(220,220,220);
$pdf->SetX(20);
$pdf->Cell(110,5,'VR. CARTERA PAGADA ENTRE EL INTERVALO (CAPITAL): ',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(40,5,number_format($carcapital,0,',','.'),1,0,'R',true);
$pdf->Cell(20,5,number_format($numcarcapital,0,',','.'),1,1,'R',true);
$pdf->SetFillColor(220,220,220);
$pdf->SetX(20);
$pdf->Cell(110,5,'VR. CARTERA PAGADA ENTRE EL INTERVALO (FINANCIACION): ',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(40,5,number_format($carfinanciacionmul,0,',','.'),1,0,'R',true);
$pdf->Cell(20,5,'---',1,1,'R',true);
$desfinanciacionmul = $descapital * $rowpar['parfinanciacion'];
$pdf->SetFillColor(220,220,220);
$pdf->SetX(20);
$pdf->Cell(110,5,'VR. DESCUENTOS ENTRE EL INTERVALO DE FECHAS (CAPITAL): ',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(40,5,number_format($descapital,0,',','.'),1,0,'R',true);
$pdf->Cell(20,5,number_format($numdescapital,0,',','.'),1,1,'R',true);
$pdf->SetFillColor(220,220,220);
$pdf->SetX(20);
$pdf->Cell(110,5,'VR. DESCUENTOS ENTRE EL INTERVALO DE FECHAS (FINANCIACION): ',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(40,5,number_format($desfinanciacionmul,0,',','.'),1,0,'R',true);
$pdf->Cell(20,5,'---',1,1,'R',true);
$pdf->SetFillColor(220,220,220);
$pdf->SetX(20);
$pdf->Cell(110,5,'VR. NOTAS DEBITO ENTRE EL INTERVALO DE FECHAS: ',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(40,5,number_format($ndebito,0,',','.'),1,0,'R',true);
$pdf->Cell(20,5,number_format($numND,0,',','.'),1,1,'R',true);
$pdf->SetFillColor(220,220,220);
$pdf->SetX(20);
$pdf->Cell(110,5,'VR. NOTAS CREDITO ENTRE EL INTERVALO DE FECHAS: ',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(40,5,number_format($ncredito,0,',','.'),1,0,'R',true);
$pdf->Cell(20,5,number_format($numNC,0,',','.'),1,1,'R',true);
$pdf->SetFillColor(220,220,220);
$pdf->SetX(20);
$pdf->Cell(110,5,'VR. DEVOLUCIONES ENTRE EL INTERVALO DE FECHAS:  ',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(40,5,number_format($devoluciones,0,',','.'),1,0,'R',true);
$pdf->Cell(20,5,number_format($numDV,0,',','.'),1,1,'R',true);
$pdf->SetFillColor(220,220,220);
$pdf->SetX(20);
$pdf->Cell(110,5,'VR. CARTERA CASTIGADA ACTIVA AL '.$fecha2.': ',1,0,'',true);
$pdf->SetFillColor(250,250,250);
$pdf->Cell(40,5,number_format($carcastigada,0,',','.'),1,0,'R',true);
$pdf->Cell(20,5,number_format($numcarcastigada,0,',','.'),1,1,'R',true);
$pdf->SetFillColor(220,220,220);
$pdf->Ln(5);
$pdf->SetFont('LucidaConsole','',11);
$pdf->Cell(0,5,'---------------------------------------------------------------------------------',0,1,'C');
$pdf->Output('Resumen_cartera.pdf','d');
?>