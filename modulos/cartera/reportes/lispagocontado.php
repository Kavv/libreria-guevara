<?php
$r = '../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
$fecha1 = $_POST['fecha1'];
$fecha2 = $_POST['fecha2'];
$filtro = 'fecha1='.$fecha1.'&fecha2='.$fecha2;
?>
<!doctype html>
<html>
<head>
	<?php 
		require($r . 'incluir/src/head.php');
		require($r . 'incluir/src/head-form.php');
	?>	

<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#tabla').dataTable({
		'bJQueryUI': true,
		'bStateSave': true,
		'bSort': false,
		'sPaginationType': 'full_numbers',
		'oLanguage': {
			'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
		},
	});
	$('.historico').click(function(){
		newHref = $(this).attr('data-rel');
		$('#dialog2').load(newHref).dialog({ modal:true, width: 800 }); 
    });
	$('.btnatras').button({ icons: { primary: 'ui-icon ui-icon-arrowthick-1-w' }});
});
</script>
</head>
<body>
<?php require($r.'incluir/src/login.php') ?>
<section id="principal">
<?php require($r.'incluir/src/cabeza.php') ?>
<?php require($r.'incluir/src/menu.php') ?>
<article id="cuerpo">
<article class="mapa">
<a href="#">Principal</a><div class="mapa_div"></div><a href="#">Cartera</a><div class="mapa_div"></div><a class="current">Reportes</a>
</article>
<article id="contenido">
<h2>Clientes con pago de contado entre el  <?php echo $fecha1.' al '.$fecha2 ?></h2>
<div class="reporte">
<a href="pdfpagocontado.php?<?php echo $filtro ?>"><img src="<?php echo $r ?>imagenes/iconos/pdf.png" title="pdf" /></a> <a href="excelpagocontado.php?<?php echo $filtro ?>"><img src="<?php echo $r ?>imagenes/iconos/excel.png" title="csv" /></a>
</div>
<table id="tabla">
<thead>
<tr>
<th>Cliente</th>
<th>Solicitud</th>
<th>Factura</th>
<th>Estado</th>
<th>Fecha Screen</th>
<th>Fecha Facturacion</th>
<th></th>
</tr>
</thead>
<tbody>
<?php
$qry = $db->query("SELECT * FROM solicitudes INNER JOIN clientes on cliid = solcliente WHERE solposcontado = 1 AND DATE(solfechscreen) BETWEEN '".$fecha1."' AND '".$fecha2."' ORDER BY solfechscreen DESC;");
while($row = $qry->fetch(PDO::FETCH_ASSOC)){
?>
<tr>
<td title="<?php echo $row['cliid'] ?>" ><?php echo $row['clinombre']." ".$row['clinom2']." ".$row['cliape1']." ".$row['cliape2'] ?></td>
<td align="center" ><?php echo $row['solid'] ?></td>
<td align="center" ><?php echo $row['solfactura'] ?></td>
<?php
$qry1 = $db->query("SELECT * FROM carteras INNER JOIN detcarteras ON carfactura = dcafactura WHERE carfactura = '".$row['solfactura']."' AND dcacuota = 1;");
$row1 = $qry1->fetch(PDO::FETCH_ASSOC);
?>
<td align="center" ><?php echo $row['solestado'] ?></td>
<td align="center" ><?php echo substr($row['solfechscreen'], 0, 10) ?></td>
<td align="center" ><?php echo substr($row['solfechafac'], 0, 10) ?></td>
<td><img src="<?php echo $r ?>imagenes/iconos/comments.png" class="historico" data-rel="notas.php?empresa=<?php echo $row['solempresa'].'&id='.$row['solid'] ?>" title="Historia de la solicitud" /></td>
</tr>
<?php
}
?>
</tbody>
</table>
<p class="boton">
<button type="button" class="btnatras" onClick="carga(); location.href='conpagocontado.php'">Atras</button>
</p>
</article>
</article>
<?php require($r.'incluir/src/pie.php') ?>
</section>
<div id="dialog2" title="Historico de solicitudes"  style="display:none"></div>
</body>
</html>