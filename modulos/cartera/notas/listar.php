<?php
	$r = '../../../';
	require($r . 'incluir/session.php');
	require($r . 'incluir/connection.php');

	$empresa = $prefijo = $numero = $estado = $fecha1 = $fecha2 = "";
	if (isset($_POST['consultar'])) {
		$empresa = $_POST['empresa'];
		$prefijo = $_POST['prefijo'];
		$numero = $_POST['numero'];
		$estado = $_POST['estado'];
		$fecha1 = $_POST['fecha1'];
		$fecha2 = $_POST['fecha2'];
	} elseif (isset($_GET['empresa'])) {
		$empresa = $_GET['empresa'];
		$prefijo = $_GET['prefijo'];
		$numero = $_GET['numero'];
		$estado = $_GET['estado'];
		$fecha1 = $_GET['fecha1'];
		$fecha2 = $_GET['fecha2'];
	}

	$filtro = 'empresa=' . $empresa . '&prefijo=' . $prefijo . '&numero=' . $numero . '&estado=' . $estado . '&fecha1=' . $fecha1 . '&fecha2=' . $fecha2;
	$con = 'SELECT * FROM movimientos INNER JOIN empresas ON movempresa = empid';
	$ord = ' ORDER BY movfecha DESC, movnumero DESC';

	/* Los parametros de la consulta sql se genera dinamicamente 
		en base a los datos recibidos para delimitar los resultados */
	$parameters = [];
	
	array_push($parameters, "(movprefijo = 'NC' OR movprefijo = 'ND')");
	if ($empresa != "")
		array_push($parameters, "movempresa LIKE '%$empresa%'");
	if ($prefijo != "")
		array_push($parameters, "movprefijo = '$prefijo'");
	if ($numero != "")
		array_push($parameters, "movnumero LIKE '%$numero%'");
	if ($estado != "")
		array_push($parameters, "movestado = '$estado'");
	if ($fecha1 != "")
		array_push($parameters, "movfecha BETWEEN '$fecha1' AND '$fecha2'");

	// Consulta base
	$sql = $con;
	foreach ($parameters as $index => $parameter) {
		// Se agregan los parametros del WHERE
		if ($index == 0)
			$sql .= " WHERE " . $parameter;
		else
			$sql .= " AND " . $parameter;
	}
	// Se completa la consulta
	$sql .= $ord;
	$qry = $db->query($sql);
?>
<!doctype html>
<html>

<head>

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>

	<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
	<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#tabla').dataTable({
				'bJQueryUI': true,
				'sPaginationType': 'full_numbers',
				'oLanguage': {
					'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
				},
				'bSort': false,
			});
			$('.pdf').click(function() {
				newSrc = $(this).attr('data-rel');
				if (validation_file(newSrc) == 200)
					$('#modal').html("<iframe src='" + newSrc + "' width='100%' height='100%'></iframe>");
				else
					$('#modal').html("<spam>El PDF no fue encontrado</spam>");
				$('#modal').dialog({
					modal: true,
					width: '600',
					height: '800',
					title: 'PDF del documento'
				});
			});
		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Carteras</a>
				<div class="mapa_div"></div><a class="current">Notas</a>
			</article>
			<article id="contenido">
				<h2>Listado de notas</h2>
				<table id="tabla">
					<thead>
						<th>Empresa</th>
						<th>Numero</th>
						<th>Documento</th>
						<th>Tercero</th>
						<th>Total</th>
						<th>Fecha</th>
						<th>Estado</th>
						<th></th>
						<th></th>
						</tr>
					</thead>
					<tbody>
						<?php
						while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
						?>
							<tr>
								<td title="<?php echo $row['movempresa'] ?>"><?php echo $row['empnombre'] ?></td>
								<td align="center"><?php echo $row['movprefijo'] ?>-<?php echo $row['movnumero'] ?></td>
								<td align="center"><?php echo $row['movdocumento'] ?></td>
								<td><?php echo $row['movtercero'] ?></td>
								<td align="right"><?php echo number_format($row['movvalor'], 2) ?></td>
								<td align="center"><img src="<?php echo $r ?>imagenes/iconos/date.png" title="<?php echo $row['movfecha'] ?>" /></td>
								<td><?php echo $row['movestado'] ?></td>

								<?php
								if ($row['movestado'] == 'PROCESO') {
									if ($row['movprefijo'] == 'NC') {
										echo '<td align="center"><a href="NC/nota.php?empresa=' . $row['movempresa'] . '&numero=' . $row['movnumero'] . '&documento=' . $row['movdocumento'] . '" title="modificar" onClick="carga()"><img src="' . $r . 'imagenes/iconos/lapiz.png" class="grayscale" /></a></td>';
										echo '<td align="center"><img src="' . $r . 'imagenes/iconos/pdf.png" class="pdf" data-rel="NC/pdf.php?empresa=' . $row['movempresa'] . '&prefijo=' . $row['movprefijo'] . '&numero=' . $row['movnumero'] . '" /></td>';
									} elseif ($row['movprefijo'] == 'ND') {
										echo '<td align="center"><a href="ND/nota.php?empresa=' . $row['movempresa'] . '&numero=' . $row['movnumero'] . '&documento=' . $row['movdocumento'] . '" title="modificar" onClick="carga()"><img src="' . $r . 'imagenes/iconos/lapiz.png" class="grayscale" /></a></td>';
										echo '<td align="center"><img src="' . $r . 'imagenes/iconos/pdf.png" class="pdf" data-rel="ND/pdf.php?empresa=' . $row['movempresa'] . '&prefijo=' . $row['movprefijo'] . '&numero=' . $row['movnumero'] . '" /></td>';
									}
								} else {
									if ($row['movprefijo'] == 'NC') {
										echo '<td align="center"><img src="' . $r . 'imagenes/iconos/lapiz.png" class="gray" /></td>';
										echo '<td align="center"><img src="' . $r . 'imagenes/iconos/pdf.png" class="pdf" data-rel="NC/pdf.php?empresa=' . $row['movempresa'] . '&prefijo=' . $row['movprefijo'] . '&numero=' . $row['movnumero'] . '" /></td>';
									} elseif ($row['movprefijo'] == 'ND') {
										echo '<td align="center"><img src="' . $r . 'imagenes/iconos/lapiz.png" class="gray" /></td>';
										echo '<td align="center"><img src="' . $r . 'imagenes/iconos/pdf.png" class="pdf" data-rel="ND/pdf.php?empresa=' . $row['movempresa'] . '&prefijo=' . $row['movprefijo'] . '&numero=' . $row['movnumero'] . '" /></td>';
									}
								}
								?>
							</tr>
						<?php } ?>
					</tbody>
				</table>
				<p class="boton">
					<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href = 'consultar.php'">atras</button>
				</p>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<div id="modal" style="display:none"></div>
</body>

</html>