<?php
$r = '../../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

$con = 'SELECT * FROM carteras INNER JOIN empresas on empid = carempresa INNER JOIN clientes ON cliid = carcliente WHERE carestado = "ACTIVA" OR carestado = "CASTIGADA" OR carestado = "RECOLECCION" OR carestado = "PENDIENTE" OR carestado = "MOROSO" OR carestado = "RECOLECCION RAZONADA"';

$qry = $db->query($con);
?>
<!doctype html>
<html>

<head>

    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>

    <link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
    <script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#tabla').dataTable({
                'bJQueryUI': true,
                'bStateSave': true,
                'sPaginationType': 'full_numbers',
                'oLanguage': {
                    'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
                },
                'bSort': false,
            });
        });
    </script>
</head>

<body>
    <?php require($r . 'incluir/src/login.php') ?>
    <section id="principal">
        <?php require($r . 'incluir/src/cabeza.php') ?>
        <?php require($r . 'incluir/src/menu.php') ?>
        <article id="cuerpo">
            <article class="mapa">
                <a href="#">Principal</a>
                <div class="mapa_div"></div><a href="#">Carteras</a>
                <div class="mapa_div"></div><a class="current">Notas</a>
            </article>
            <article id="contenido">
                <h2>Listado de notas</h2>
                <table id="tabla">
                    <thead>
                        <th>Empresa</th>
                        <th>Factura</th>
                        <th>Cliente</th>
                        <th>Total</th>
                        <th>Cuota</th>
                        <th>Saldo</th>
                        <th>#Cuota</th>
                        <th>Estado</th>
                        <th>Fecha</th>
                        <th>Nota</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
                            $nombre_cliente = $row['clinombre'] ." ". $row['clinom2'] ." ". $row['cliape1'] ." ". $row['cliape2'];
                            $parametros = "factura=".$row['carfactura']."&empresa=".$row['carempresa'];
                        ?>
                            <tr>
                                <td title="<?php echo $row['carempresa'] ?>"><?php echo $row['empnombre'] ?></td>
                                <td align="center"><?php echo $row['carfactura'] ?></td>
                                <td  title="<?php echo $row['carTercero'] ?>"><?php echo $nombre_cliente ?></td>
                                <td align="right"><?php echo number_format($row['cartotal'],2) ?></td>
                                <td align="right"><?php echo number_format($row['carcuota'],2) ?></td>
                                <td align="right"><?php echo number_format($row['carsaldo'],2) ?></td>
                                <td align="center"><?php echo $row['carncuota'] ?></td>
                                <td align="center"><?php echo $row['carestado'] ?></td>
                                <td align="center"><img src="<?php echo $r ?>imagenes/iconos/date.png" title="<?php echo $row['carfecha'] ?>" /></td>
                                <td align="center"><a href="principal.php?<?php echo $parametros ?>"><img src="<?php echo $r ?>imagenes/iconos/add.png" class="nota" title="Agregar nota" /></a></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <p class="boton">
                    <button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href = 'consultar.php'">atras</button>
                </p>
            </article>
        </article>
        <?php require($r . 'incluir/src/pie.php') ?>
    </section>
	<?php
	if (isset($_GET['error'])) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['error'] . '</div>';
	elseif (isset($_GET['mensaje'])) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['mensaje'] . '</div>';
	?>
</body>

</html>