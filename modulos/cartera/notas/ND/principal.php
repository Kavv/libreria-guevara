<?php
$r = '../../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

$empresa_id = $_GET['empresa'];
$factura = $_GET['factura'];

$query = "SELECT * FROM empresas WHERE empid = '$empresa_id'";
$qry = $db->query($query);

$rowmax = $db->query("SELECT MAX(movnumero)+1 AS ultimo FROM movimientos WHERE movempresa = '$empresa_id' AND movprefijo = 'ND'")->fetch(PDO::FETCH_ASSOC);
if ($rowmax['ultimo'] == '')
	$ultimo = 1;
else
	$ultimo = $rowmax['ultimo'];

?>
<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>

	</script>

</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Cartera</a>
				<div class="mapa_div"></div><a href="#">Notas</a>
				<div class="mapa_div"></div><a class="current">Nota credito</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="nota.php" method="post">
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">Nota Debito</legend>
						<p>
							<label for="empresa">Empresa:</label>
							<select id="empresa" name="empresa" class="validate[required]">
								<?php
								while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
									echo '<option value=' . $row['empid'] . '>' . $row['empnombre'] . '</option>';
								}
								?>
							</select>
						</p>
						<p>
							<label for="documento">Factura:</label>
							<input type="text" class="documento" value="FV" readonly />
							<input readonly id="documento" type="text" class="validate[required, custom[onlyNumberSp]] consecutivo" name="documento" value="<?php echo $factura?>"/>
						</p>
						<p>
							<label for="numero">Numero:</label>
							<input type="text" id="prefijo" class="documento" name="prefijo" value="ND" readonly />
							<input type="text" name="numero" id="numero" class="consecutivo" readonly value="<?php echo $ultimo?>"/>
						</p>
						<p class="boton">
							<button type="submit" class="btn btn-primary btningresar" name="ingresar" value="ingresar">Continuar</button>
							<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href = 'listar_cartera.php'">atras</button>
						</p>
					</fieldset>
				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<?php
	if (isset($_GET['error'])) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['error'] . '</div>';
	elseif (isset($_GET['mensaje'])) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['mensaje'] . '</div>';
	?>
</body>

</html>