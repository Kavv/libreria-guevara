<?php
	$r = '../../../../';
	require($r . 'incluir/session.php');
	require($r . 'incluir/connection.php');
	if(isset($_POST['empresa']))
	{
		$empresa = $_POST['empresa'];
		// Consecutivo del movimiendo NC (nota de credito)
		$numero = $_POST['numero'];
		// # Factura correspondiente
		$documento = $_POST["documento"];
		// Valor de la cuota a ingresar
		$valor = $_POST['valor'];
		// Nueva cuota
		$nuecuota = $_POST['nuecuota'];
		// Nuevo saldo
		$nsaldo = $_POST['nsaldo'];
		// Numero de cuotas
		$ncuota = $_POST['ncuota'];
		$texto = trim($_POST['texto']);
		$qry = $db->query("UPDATE movimientos SET movfecha = NOW(), movvalor = $valor, movsaldo = $nsaldo, movestado = 'FINALIZADO', movtexto = '$texto' WHERE movempresa = '$empresa' AND movprefijo = 'NC' AND movnumero = '$numero'");
	
		// Obtenemos la cantidad de detalles de cartera cancelado
		if($valor != 0)
		{
			if($num < 0)
				$num = 1;
			else
				$num += 1;
		
			$qry = $db->query("UPDATE detcarteras set dcaestado = 'CANCELADO', dcavalor = $valor, dcasaldo = $nsaldo WHERE dcaempresa = '$empresa' AND dcafactura = '$documento' AND dcacuota = $num AND dcaestado = 'ACTIVA'")->fetch(PDO::FETCH_ASSOC);
		}
		// Query original, se desconoce si la implementacion es corecta 04/11/20
		/* Evaluar en otro momento */
		//$qry = $db->query("UPDATE detcarteras set dcaestado = 'CANCELADO' WHERE dcaempresa = '$empresa' AND dcafactura = '$documento' AND dcavalor + dcadescuento > 0 AND dcaestado = 'ACTIVA'")->fetch(PDO::FETCH_ASSOC);
		
		// Cuotas canceladas + cuotas restantes
		$numcuotas = $num + $ncuota;
		if ($nsaldo == 0) {
			$qry = $db->query("UPDATE carteras SET carcuota = $nuecuota, carsaldo = $nsaldo, carncuota = $numcuotas, carestado = 'CANCELADO' WHERE carempresa = '$empresa' AND carfactura = '$documento'");
		} else {
			$qry = $db->query("UPDATE carteras SET carcuota = $nuecuota, carsaldo = $nsaldo, carncuota = $numcuotas WHERE carempresa = '$empresa' AND carfactura = '$documento'");
		}
		
		// Elimina los detalles con estado 'Activa'  de una cartera especifica
		$qry = $db->query("DELETE FROM detcarteras WHERE dcaempresa = '$empresa' AND dcafactura = '$documento' AND dcaestado = 'ACTIVA'");
		$row2 = $db->query("SELECT * FROM solicitudes WHERE solempresa = '$empresa' AND solfactura = '$documento'")->fetch(PDO::FETCH_ASSOC);
		$fecha = $row2['solcompromiso'];
	
		$i = 1;	
		while ($i <= $numcuotas) {
			if($i > $num)
				$qry = $db->query("INSERT INTO detcarteras (dcaempresa, dcafactura, dcacuota, dcafecha, dcaestado) VALUES ('$empresa', '$documento', '$i', '$fecha', 'ACTIVA')");
			$fecha = date('Y-m-d', strtotime($fecha . ' next month'));
			$i++;
		}
		header("location:finalizar.php?empresa=" . $empresa . "&numero=" . $numero);
		exit();
	}
	if(isset($_GET['empresa']))
	{
		$empresa = $_GET['empresa'];
		// Consecutivo del movimiendo NC (nota de credito)
		$numero = $_GET['numero'];
	}

?>
<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>

</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Cartera</a>
				<div class="mapa_div"></div><a href="#">Notas</a>
				<div class="mapa_div"></div><a class="current">Notas credito</a>
			</article>
			<article id="contenido">
				<p align="center">
					<iframe src="pdf.php?empresa=<?php echo $empresa . '&numero=' . $numero ?>" width="800" height="550"></iframe>
				</p>
				<p class="boton">
					<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href = 'listar_cartera.php'">atras</button>
				</p>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
</body>

</html>