<?php
$r = '../../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
if (isset($_POST['ingresar'])) {
	$empresa = $_POST['empresa'];
	$numero = $_POST['numero'];
	$documento = $_POST['documento'];
	// Verificamos que la cartera exista
	$qry = $db->query("SELECT * FROM carteras WHERE carempresa = '$empresa' AND carfactura = '$documento'");
	$num = $qry->rowCount();
	if ($num > 0) {
		// Verificamos que la cartera este ACTIVA o CASTIGADA
		$row = $qry->fetch(PDO::FETCH_ASSOC);
		if ($row['carestado'] == 'ACTIVA' or $row['carestado'] == 'CASTIGADA' or $row['carestado'] =='RECOLECCION' or $row['carestado'] == 'PENDIENTE' or $row['carestado'] == 'MOROSO' or $row['carestado'] == 'RECOLECCION RAZONADA') {
			// Verificamos que no exista un movimiento en PROCESO relacionado a la empresa y la factura
			$num = $db->query("SELECT * FROM movimientos WHERE movempresa = '$empresa' AND movnumero = '$numero' AND movestado = 'PROCESO'")->rowCount();
			if ($num < 1) {
				$qry = $db->query("INSERT INTO movimientos(movempresa, movprefijo, movnumero, movtercero, movdocumento, movfecha, movestado) VALUES ('$empresa', 'NC', '$numero', '" . $row['carcliente'] . "', '$documento', NOW(), 'PROCESO')");
				header('Location: nota.php?empresa=' . $empresa . '&numero=' . $numero . '&documento=' . $documento);
				exit();
			} else $error = 'Hay una movimiento que no se ha finalizado para esta cuenta.';
		} else $error = 'La cuenta ya se encuentra cancelada.';
	} else $error = 'No existe la cuenta.';
	if ($error) {
		header('Location: principal.php?error=' . $error);
		exit();
	}
} else {
	$empresa = $_GET['empresa'];
	$numero = $_GET['numero'];
	$documento = $_GET['documento'];
}
$row = $db->query("SELECT * FROM (carteras INNER JOIN clientes ON carcliente = cliid) INNER JOIN empresas ON carempresa = empid WHERE carempresa = '$empresa' AND carfactura = '$documento'")->fetch(PDO::FETCH_ASSOC);
$row2 = $db->query("SELECT * FROM parametros")->fetch(PDO::FETCH_ASSOC);
$num = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '$empresa' AND dcafactura = '$documento' AND dcaestado = 'ACTIVA'")->rowCount();
?>
<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<script type="text/javascript">
		$(document).ready(function() {

			$('#valor, #ncuota').change(function(event) {
				var nsaldo = parseFloat($('#saldo').val()) - parseFloat($('#valor').val());
				$('#nsaldo').val(nsaldo.toFixed(2));
				var n_cuota = parseFloat($('#ncuota').find(':selected').val());
				if (n_cuota > 0)
					var nuecuota = nsaldo / n_cuota;
				else
					var nuecuota = nsaldo;
				$('#nuecuota').val(nuecuota.toFixed(2));
			});
			$('#valor, #ncuota').change();
		});
	</script>

</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Cartera</a>
				<div class="mapa_div"></div><a href="#">Notas</a>
				<div class="mapa_div"></div><a class="current">Nota credito</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="finalizar.php" method="post">
					<fieldset id="field" class="ui-widget ui-widget-content ui-corner-all col-md-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">Datos de la cuenta</legend>
						<p align="center">
							<label>Empresa: </label>
							<input type="hidden" name="empresa" value="<?php echo $empresa ?>" />
							<label style="font-weight: bold;"><?php echo $row['empnombre'] ?></label>
							<label>Cliente: </label>
							<label style="font-weight: bold;"><?php echo $row['clinombre'] . ' ' . $row['clinom2'] . ' ' . $row['cliape1'] . ' ' . $row['cliape2'] . ' - ' . $row['cliid'] ?></label>
						</p>
						<p align="center">
							<label>Documento: </label>
							<input type="text" class="documento" name="prefijo" value="NC" readonly />
							<input type="text" class="consecutivo" name="numero" value="<?php echo $numero ?>" readonly />
							<label>Fecha: </label>
							<input type="text" id="fecha" class="fecha" name="fecha" value="<?php echo date('Y-m-d') ?>" readonly />
							<label>Factura: </label>
							<input type="text" class="documento" value="FV" readonly />
							<input type="text" class="consecutivo" name="documento" value="<?php echo $documento ?>" readonly />
						</p>
						<fieldset id="field" class="ui-widget ui-widget-content ui-corner-all">
							<legend class="ui-widget ui-widget-header ui-corner-all">Datos actuales</legend>
							<p align="center">
								<label>Total cuenta: </label>
								<input type="text" id="totalfac" name="totalfac" class="valor" value="<?php echo $row['cartotal'] ?>" readonly />
								<label>Saldo actual: </label>
								<input type="text" id="saldo" name="saldo" class="valor" value="<?php echo $row['carsaldo'] ?>" readonly />
								<label>Cuotas faltantes: </label>

								<div class="input-group">
									<input type="text" id="ncf" name="ncf" class="cantidad col-md-5 not-w" value="<?php echo $num ?>" readonly />
									<div class="input-group-append col-md-7 p-0">
										<span class="input-group-text col-md-3">De</span>
										<input type="text" id="nc" name="nc" class="cantidad col-md-9 not-w" value="<?php echo $row['carncuota'] ?>" readonly />
									</div>
								</div>

								<label>Valor de la cuota: </label>
								<input type="text" id="cuota" name="cuota" class="valor" value="<?php echo $row['carcuota'] ?>" readonly />
							</p>
						</fieldset>
						<br>
						<fieldset id="field" class="ui-widget ui-widget-content ui-corner-all">
							<legend class="ui-widget ui-widget-header ui-corner-all">Datos nota credito</legend>
							<p align="center">
								<label>Pago de cuota: </label>
								<input type="text" id="valor" name="valor" class="valor validate[required, custom[onlyNumberSp]]" value="<?php echo $row['carcuota'] ?>" />
								<label>Numero de cuotas restantes:</label>
								<select id="ncuota" name="ncuota" class="validate[required]">
									<option value=""></option>
									<?php
									if($siguiente_cuota != 0)
									$siguiente_cuota = $num - 1;
									for ($i = 0; $i <= $row2['parncuota']; $i++) {
										if ($siguiente_cuota != $i)
											echo '<option value="' . $i . '">' . $i . '</option>';
										else
											echo '<option value="' . $i . '" selected>' . $i . '</option>';
									}
									?>
								</select>
								<label>Nuevo saldo: </label>
								<input type="text" id="nsaldo" name="nsaldo" class="valor validate[min[0]]" readonly />
								<label>Nueva cuota: </label>
								<input type="text" id="nuecuota" name="nuecuota" class="valor" readonly />
							</p>
							<p align="center">
								<label for="texto">Nota: </label>
								<textarea name="texto" class="form-control" rows="3"></textarea>
							</p>
							<p class="boton">
								<button type="button" class="btn btn-danger btnatras" onClick="carga(); location.href = 'cancelar.php?empresa=<?php echo $empresa . '&numero=' . $numero ?>'">cancelar</button>
								<button type="submit" class="btn btn-primary btnfinalizar" name="finalizar" value="finalizar">finalizar</button>
							</p>
						</fieldset>
					</fieldset>
				</form>
			</article>
		</article>
	</section>
	<?php
	if (isset($_GET['error'])) echo "<div id='dialog-message' title='Error'><span class='ui-icon ui-icon-circle-close' style='float:left; margin:3px 7px 7px 0;'></span>" . $_GET["error"] . "</div>";
	elseif (isset($_GET['mensaje'])) echo "<div id='dialog-message' title='Correcto'><span class='ui-icon ui-icon-circle-check' style='float:left; margin:3px 7px 7px 0;'></span>" . $_GET["mensaje"] . "</div>";
	?>
</body>

</html>