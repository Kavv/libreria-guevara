<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
?>
<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {

			$('#fecha1').datepicker({
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
				maxDate: '+0D',
				onClose: function(selectedDate) {
					$('#fecha2').datepicker('option', 'minDate', selectedDate);
				}
			}).keypress(function(event) {
				event.preventDefault()
			});

			$('#fecha2').datepicker({
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
				maxDate: '+0D',
				onClose: function(selectedDate) {
					$('#fecha1').datepicker('option', 'maxDate', selectedDate);
				}
			}).keypress(function(event) {
				event.preventDefault()
			});


			/* Valida que ambas fechas sean especificadas */
			$('#fecha1').change(function(){
				both_date();
			});
			
			$('#fecha2').change(function(){
				both_date();
			});
			function both_date()
			{
				if($('#fecha2').val() != "" || $('#fecha1').val() != "")
				{
					$('#fecha1').attr("required",true);
					$('#fecha2').attr("required",true);
				}
				else
				{
					$('#fecha1').removeAttr("required",false);
					$('#fecha2').removeAttr("required",false);
				}
			}
		});
	</script>

</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Cartera</a>
				<div class="mapa_div"></div><a class="current">Notas</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="listar.php" method="post">
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">Consulta de notas</legend>
						<p>
							<label for="empresa">Empresa:</label>
							<select name="empresa">
								<option value="">TODAS</option>
								<?php
								$qry = $db->query("SELECT * FROM empresas ORDER BY empnombre");
								while ($row = $qry->fetch(PDO::FETCH_ASSOC))
									echo '<option value=' . $row['empid'] . '>' . $row['empnombre'] . '</option>';
								?>
							</select>
						</p>
						<p>
							<label for="prefijo">Documento:</label>
							<select id="prefijo" name="prefijo">
								<option value="">TODOS</option>
								<option value="NC">NOTAS CREDITO</option>
								<option value="ND">NOTAS DEBITO</option>
							</select>
						</p>
						<p>
							<label for="numero">Numero:</label>
							<input type="text" name="numero" class="consecutivo" />
						</p>
						<p>
							<label for="estado">Estado:</label>
							<select name="estado">
								<option value="">TODOS</option>
								<option value="PROCESO">PROCESO</option>
								<option value="FINALIZADO">FINALIZADO</option>
							</select>
						</p>
						<p>
							<label for="fecha">Fechas:</label>
							<input type="text" id="fecha1" name="fecha1" class="fecha" /> 
							<label for="">-</label>
							<input type="text" id="fecha2" name="fecha2" class="fecha" />
						</p>
						<div class="row">
							<div class="col-md-12 col-lg-6">
								<button type="submit" class="btnconsulta btn btn-primary btn-block mt-2" name="consultar" value="Buscar">consultar</button> <!-- BOTON CONSULTAR -->
							</div>
							<div class="col-md-12 col-lg-6">
								<button type="submit" class="btnconsulta btn btn-primary btn-block mt-2" name="show-all" value="Buscar">Mostrar Todo</button> </div>
						</div>
					</fieldset>
				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<?php
	if (isset($_GET['error'])) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['error'] . '</div>';
	?>
</body>

</html>