<?php
$r = '../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
require($r . 'incluir/funciones.php');
$empresa = $_GET['id1'];
$numero = $_GET['id2'];


$filtro = 'empresa=' . $_GET['empresa'] . '&prefijo=' . $_GET['prefijo'] . '&numero=' . $_GET['numero'] . '&cliente=' . $_GET['cliente'] . '&fecha1=' . $_GET['fecha1'] . '&fecha2=' . $_GET['fecha2'];

if (isset($_GET['adinota'])) {
	$nota = trim($_GET['nota']);
	$calificacion = $_GET['calificacion'];
	$estado = $_GET['estado'];
	$subestado = $_GET['subestado'];
	$qry = $db->query("INSERT INTO hiscarteras (hisempresa, hiscuenta, hisfecha, hisusuario, hisnota, hiscalificacion, hisestado, hissubestado) VALUES ('$empresa', '$numero', NOW(), '" . $_SESSION['id'] . "', '$nota', '$calificacion', '$estado', '$subestado')");
	header("location:historial.php?".$filtro."&id1=".$empresa."&id2=".$numero);
	exit();
}

if (isset($_GET['soporte'])) {
	$fecha_hora_soporte = $_GET['fecha'] . " " . $_GET['hora'];
	$comentarios = trim($_GET['comentarios']);
	$solicitudsoporte = $_GET['solicitudsoporte'];
	$clientesoporte = $_GET['clientesoporte'];
	$qry = $db->query("INSERT INTO soportes (soporfactura, soporsolicitud, soporempresa, soporcliente, soporfechasoporte, soporcomentarios, soporusuario) VALUES ('$numero', '$solicitudsoporte', '$empresa', '$clientesoporte', '$fecha_hora_soporte', '$comentarios', '" . $_SESSION['id'] . "');");
	header("location:historial.php?".$filtro."&id1=".$empresa."&id2=".$numero);
	exit();
}

$row = $db->query("SELECT * FROM (((((carteras INNER JOIN empresas ON carempresa = empid) INNER JOIN clientes ON carcliente = cliid) INNER JOIN solicitudes ON (carempresa = solempresa AND carfactura = solfactura)) INNER JOIN departamentos ON depid = soldepcobro) INNER JOIN ciudades ON (soldepcobro = ciudepto AND solciucobro = ciuid)) LEFT JOIN usuarios ON carpromotor = usuid WHERE carempresa = '$empresa' AND carfactura = '$numero'")->fetch(PDO::FETCH_ASSOC);
$row2 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '$empresa' AND dcafactura = '$numero' AND dcaestado = 'ACTIVA' ORDER BY dcacuota ASC")->fetch(PDO::FETCH_ASSOC);

$mora = 0;
if($row2)
{
	if ((date('Y-m-d') > $row2['dcafecha']) && $row['carsaldo'] != 0)
		$mora = fechaDif($row2['dcafecha'], date('Y-m-d'));
}


?>
<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>

	<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
	<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>

	<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {

			$("#tabla").dataTable({
				'bJQueryUI': true,
				'sPaginationType': 'full_numbers',
				'oLanguage': {
					'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
				},
				'bSort': false,
			});

			$('.nota').click(function() {
				$('#campo').dialog({
					height: 500,
					width: 680,
					modal: true,
					position: {
						at:'top'
					}
				});
			});

			$('.soporte').click(function() {
				$('#soporte').dialog({
					height: 500,
					width: 680,
					modal: true
				});
			});

			$('.pdf').click(function() {
				newSrc = $(this).attr('data-rel');
				if (validation_file(newSrc) == 200)
					$('#modal').html("<iframe src='" + newSrc + "' width='100%' height='100%'></iframe>");
				else
					$('#modal').html("<spam>El PDF no fue encontrado</spam>");
				$('#modal').dialog({
					modal: true,
					width: '600',
					height: '800',
					title: 'PDF de la solicitud'
				});
			});

			$('#fecha').datepicker({
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
			});
		});
	</script>
	<style type="text/css">
		#detalle span {
			margin-right: 10px;
		}

		#detalle label {
			border-bottom: 1px solid #000;
		}
	</style>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a class="current">Cartera</a>
			</article>
			<article id="contenido">
				<h2>Historial de la cuenta</h2>
				<div id="detalle" class="row">
					<div class="col-md-6">
						<div class="row">
							<span><strong>Empresa:</strong></span>
							<label class="not-w"><?php echo $row['empnombre'] ?><br /></label>
						</div>
						<div class="row">
							<span><strong>Solicitud:</strong></span>
							<label class="not-w"><?php echo $row['solid'] ?><br /></label>
						</div>
						<div class="row">
							<span><strong>Factura:</strong></span>
							<label class="not-w"><?php echo $row['carfactura'] ?><br /></label>
						</div>
						<div class="row">
							<span><strong>Cliente:</strong></span>
							<label class="not-w"><?php echo $row['clinombre'] . ' ' . $row['clinom2'] . ' ' . $row['cliape1'] . ' ' . $row['cliape2'] ?><br /></label>
						</div>
						<div class="row">
							<span><strong>Numero Identi:</strong></span>
							<label class="not-w"><?php echo $row['cliid'] ?><br /></label>
						</div>
						<div class="row">
							<span><strong>Email:</strong></span>
							<label class="not-w"><?php echo $row['cliemail'] ?><br /></label>
						</div>
						<div class="row">
							<span><strong>Promotor:</strong></span>
							<label class="not-w"><?php echo $row['usunombre'] ?><br /></label>
						</div>
						<div class="row">
							<span><strong>Ubicacion:</strong></span>
							<label class="not-w"><?php echo $row['depnombre'] . ' / ' . $row['ciunombre'] ?><br /></label>
						</div>
						<div class="row">
							<span><strong>Dirección:</strong></span>
							<label class="not-w"><?php echo $row['solbarcobro'] . ', ' . $row['solcobro'] ?><br /></label>
						</div>
					</div>
					<div class="col-md-6">

						<div class="row">
							<span><strong>Contacto Directo:</strong></span>
							<label class="not-w"><?php echo $row['clicelular'] ?><br /></label>
						</div>
						<div class="row">
							<span><strong>Teléfono Comercio:</strong></span>
							<label class="not-w"><?php echo $row['clitelcomercio'] ?><br /></label>
						</div>
						<div class="row">
							<span><strong>Número Adicional:</strong></span>
							<label class="not-w"><?php echo $row['clitelresidencia'] ?><br /></label>
						</div>
						<div class="row">
							<span><strong>Cuota inicial:</strong></span>
							<label class="not-w"><?php echo number_format($row['solcuota'], 2) ?><br /></label>
						</div>
						<div class="row">
							<span><strong>Total credito:</strong></span>
							<label class="not-w"><?php echo number_format($row['cartotal'], 2) ?><br /></label>
						</div>
						<div class="row">
							<span><strong>Valor cuota:</strong></span>
							<label class="not-w"><?php echo number_format($row['carcuota'], 2) ?><br /></label>
						</div>
						<div class="row">
							<span><strong>Valor pagado:</strong></span>
							<label class="not-w"><?php echo number_format($row['cartotal'] - $row['carsaldo'], 2) ?><br /></label>
						</div>
						<div class="row">
							<span><strong>Saldo:</strong></span>
							<label class="not-w"><?php echo number_format($row['carsaldo'], 2) ?><br /></label>
						</div>
						<div class="row">
							<span><strong>Dias en mora:</strong></span>
							<label class="not-w"><?php echo $mora; ?></label>
						</div>
					</div>
				</div>
				<div class="reporte">
					<img src="<?php echo $r ?>imagenes/iconos/computer.png" class="soporte" style="cursor:pointer" title="Agentar Soporte" />
					<img style="margin:0 3px;" src="<?php echo $r ?>imagenes/iconos/pdf.png" class="pdf" data-rel="<?php echo $r . 'pdf/solicitudes/' . $row['solempresa'] . '/' . $row['solid'] . '.pdf' ?>" title="PDF de la Solicitud" />
					<a href="detalle.php?id1=<?php echo $empresa . '&id2=' . $numero . '&' . $filtro ?>" onClick="carga()"><img src="<?php echo $r ?>imagenes/iconos/briefcase.png" title="Cartera" /></a> 
					<a href="movimientos.php?id1=<?php echo $empresa . '&id2=' . $numero . '&' . $filtro ?>" onClick="carga()"><img src="<?php echo $r ?>imagenes/iconos/application_side_list.png" title="Movimientos" /></a> <img src="<?php echo $r ?>imagenes/iconos/add.png" class="nota" title="Agregar nota" />
				</div>
				<table id="tabla">
					<thead>
						<tr>
							<th>fecha</th>
							<th>Usuario</th>
							<th>Nota</th>
							<th>Estado</th>
							<th>Subestado</th>
							<th>Calificacion</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$qry = $db->query("SELECT * FROM hiscarteras INNER JOIN usuarios ON hisusuario = usuid LEFT JOIN calificaciones ON hiscalificacion = califiid LEFT JOIN estadoscartera ON hisestado = estaid LEFT JOIN subestadoscartera ON hissubestado = subesid WHERE hisempresa = '" . $row['carempresa'] . "' AND hiscuenta = '" . $row['carfactura'] . "' ORDER BY hisfecha DESC");
						while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
						?>
							<tr>
								<td align="center"><?php echo $row['hisfecha'] ?></td>
								<td><?php echo $row['usunombre'] ?></td>
								<td><?php echo $row['hisnota'] ?></td>
								<td><?php echo $row['estadescripcion'] ?></td>
								<td><?php echo $row['subesdescripcion'] ?></td>
								<td><?php echo $row['califinombre']  ?></td>
							</tr>
						<?php
						}
						?>
					</tbody>
				</table>
				<p class="boton">
					<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href = 'detalle.php?id1=<?php echo $empresa . '&id2=' . $numero . '&' . $filtro ?>'">atras</button>
				</p>
			</article>
		</article>
	</section>

	<div id="soporte" title="Agendar Soporte" style="display:none">
		<?php
		$row4 = $db->query("SELECT * FROM 
		(
			(
				(
					(
						(
							carteras INNER JOIN empresas ON carempresa = empid
						) 
						INNER JOIN clientes ON carcliente = cliid
					) 
					INNER JOIN solicitudes ON (
						carempresa = solempresa AND carfactura = solfactura
					)
				) INNER JOIN departamentos ON depid = soldepcobro
			) INNER JOIN ciudades ON (
				soldepcobro = ciudepto AND solciucobro = ciuid
			)
		) LEFT JOIN usuarios ON carpromotor = usuid WHERE carempresa = '$empresa' AND carfactura = '$numero'")->fetch(PDO::FETCH_ASSOC);
		
		?> 
		<form id="soporte" name="soporte" action="historial.php?<?php echo $filtro ?>" method="get">
			<input type="hidden" name="id1" value="<?php echo $empresa ?>">
			<input type="hidden" name="id2" value="<?php echo $numero ?>">
			<input type="hidden" name="clientesoporte" value="<?php echo $row4['cliid'] ?>">
			<input type="hidden" name="solicitudsoporte" value="<?php echo $row4['solid'] ?>">
			</br>
			<p>
				<label style="width:160px;" for="hora"> Hora de soporte: </label>
				<input type="time" name="hora" id="hora" value="" max="23:59:59" min="00:00:01" class="validate[required]" step="1">
			</p>
			<br>
			<p>
				<label style="width:160px;" for="fecha"> Fecha de soporte: </label>
				<input type="text" name="fecha" id="fecha" class="validate[required] text-input fecha" />
			</p>
			<br>
			<label for="">Comentario</label>
			<center>
				<textarea name="comentarios" placeholder="Comentarios...." rows="3" class="form-control text-input" required></textarea>
				</br></br>
				<button type="submit" class="btn btn-success btninsertar" name="soporte" value="soporte">Validar Soporte</button>
			</center>
		</form>
	</div>


	<div id="campo" title="Agregar historico" style="display:none">
		<form id="form" name="form" action="historial.php?<?php echo $filtro ?>" method="get">
			<input type="hidden" name="id1" value="<?php echo $empresa ?>">
			<input type="hidden" name="id2" value="<?php echo $numero ?>">
		

			<label for="calificacion">Calificacion:</label>
			<select name="calificacion" required>
				<option value="">SELECCIONE </option>
				<?php
				$qrycal = $db->query("SELECT * FROM calificaciones WHERE califiestado = '1' ORDER BY califinombre");
				while ($rowcal = $qrycal->fetch(PDO::FETCH_ASSOC))
					echo '<option value=' . $rowcal['califiid'] . '>' . $rowcal['califinombre'] . '</option>';
				?>
			</select>
			</br></br>

			<label for="estado">Estado:</label>
			<select name="estado" required>
				<option value="">SELECCIONE </option>
				<?php
				$qrycal = $db->query("SELECT * FROM estadoscartera WHERE estaestado = 1 ORDER BY estadescripcion");
				while ($rowcal = $qrycal->fetch(PDO::FETCH_ASSOC))
					echo '<option value=' . $rowcal['estaid'] . '>' . $rowcal['estadescripcion'] . '</option>';
				?>
			</select>
			</br></br>

			<label for="subestado">Subestado:</label>
			<select name="subestado" required>
				<option value="">SELECCIONE </option>
				<?php
				$qrycal = $db->query("SELECT * FROM subestadoscartera WHERE subesestado = 1 ORDER BY subesdescripcion");
				while ($rowcal = $qrycal->fetch(PDO::FETCH_ASSOC))
					echo '<option value=' . $rowcal['subesid'] . '>' . $rowcal['subesdescripcion'] . '</option>';
				?>
			</select>
			</br></br>
			<center>
				<label>Comentario:</label>
				<textarea name="nota" id="" class="form-control" rows="3" required></textarea>
				</br></br>
				<button type="submit" class="btn btn-success btninsertar" name="adinota" value="adinota">Insertar Nota</button>
			</center>
		</form>
	</div>
	<div id="modal" style="display:none"></div>
	<div id="dialog2" title="Historico de Solicitudes" style="display:none"></div>
	
	<?php require($r . 'incluir/src/pie.php') ?>
</body>

</html>