<?php
	$r = '../../';
	require($r . 'incluir/session.php');
	require($r . 'incluir/connection.php');
	require($r . 'incluir/funciones.php');
	$empresa = $_GET['id1'];
	$numero = $_GET['id2'];
	$filtro = 'empresa=' . @$_GET['empresa'] . '&prefijo=' . @$_GET['prefijo'] . '&numero=' . @$_GET['numero'] . '&cliente=' . @$_GET['cliente'] . '&fecha1=' . $_GET['fecha1'] . '&fecha2=' . $_GET['fecha2'];
	$row = $db->query("SELECT * FROM (((((carteras INNER JOIN empresas ON carempresa = empid) INNER JOIN clientes ON carcliente = cliid) INNER JOIN solicitudes ON (carempresa = solempresa AND carfactura = solfactura)) INNER JOIN departamentos ON depid = soldepcobro) INNER JOIN ciudades ON (soldepcobro = ciudepto AND solciucobro = ciuid)) LEFT JOIN usuarios ON carpromotor = usuid WHERE carempresa = '$empresa' AND carfactura = '$numero'")->fetch(PDO::FETCH_ASSOC);
	$row2 = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '$empresa' AND dcafactura = '$numero' AND dcaestado = 'ACTIVA' ORDER BY dcacuota ASC")->fetch(PDO::FETCH_ASSOC);
	
	$mora = 0;
	if($row2)
	{
		if ((date('Y-m-d') > $row2['dcafecha']) && $row['carsaldo'] != 0)
			$mora = fechaDif($row2['dcafecha'], date('Y-m-d'));
	}

?>
<!doctype html>
<html>

<head>
	<?php
		require($r . 'incluir/src/head.php');
		require($r . 'incluir/src/head-form.php');
	?>

	<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
	<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#tabla').dataTable({
				'bPaginate': false,
				'bLengthChange': false,
				'bFilter': false,
				'bSort': false,
				'bInfo': false,
				'bAutoWidth': false,
				'bJQueryUI': true
			});
			
			$('.pdf').click(function() {
				newSrc = $(this).attr('data-rel');
				if (validation_file(newSrc) == 200)
					$('#modal').html("<iframe src='" + newSrc + "' width='100%' height='100%'></iframe>");
				else
					$('#modal').html("<spam>El PDF no fue encontrado</spam>");
				$('#modal').dialog({
					modal: true,
					width: '600',
					height: '800',
					title: 'PDF del detalle'
				});
			});

			$('.historico').click(function() {
				newHref = $(this).attr('data-rel');
				$('#dialog2').load(newHref).dialog({
					modal: true,
					width: 800, 
					draggable: false,
				});

			});

		});
	</script>
	<style>
		#detalle span{
			margin-right: 10px;
		}
		#detalle label{
			border-bottom: 1px solid #000;
		}
	</style>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a class="current">Cartera</a>
			</article>
			<article id="contenido">
				<h2>Detalle de la cuenta</h2>
				<div id="detalle" class="row">
					<div class="col-md-6">
						<div class="row">
							<span><strong>Empresa:</strong></span> 
							<label class="not-w"><?php echo $row['empnombre'] ?><br /></label>
						</div>
						<div class="row">
							<span><strong>Solicitud:</strong></span> 
							<label class="not-w"><?php echo $row['solid'] ?><br /></label>
						</div>
						<div class="row">
							<span><strong>Factura:</strong></span> 
							<label class="not-w"><?php echo $row['carfactura'] ?><br /></label>
						</div>
						<div class="row">
							<span><strong>Cliente:</strong></span> 
							<label class="not-w"><?php echo $row['clinombre'] . ' ' . $row['clinom2'] . ' ' . $row['cliape1'] . ' ' . $row['cliape2'] ?><br /></label>
						</div>
						<div class="row">
							<span><strong>Numero Identi:</strong></span> 
							<label class="not-w"><?php echo $row['cliid'] ?><br /></label>
						</div>
						<div class="row">
							<span><strong>Email:</strong></span> 
							<label class="not-w"><?php echo $row['cliemail'] ?><br /></label>
						</div>
						<div class="row">
							<span><strong>Promotor:</strong></span> 
							<label class="not-w"><?php echo $row['usunombre'] ?><br /></label>
						</div>
						<div class="row">
							<span><strong>Ubicacion:</strong></span> 
							<label class="not-w"><?php echo $row['depnombre'] . ' / ' . $row['ciunombre'] ?><br /></label>
						</div>
						<div class="row">
							<span><strong>Dirección:</strong></span> 
							<label class="not-w"><?php echo $row['solbarcobro'] . ', ' . $row['solcobro'] ?><br /></label>
						</div>
					</div>
					<div class="col-md-6">
						
						<div class="row">
							<span><strong>Contacto Directo:</strong></span> 
							<label class="not-w"><?php echo $row['clicelular'] ?><br /></label>
						</div>
						<div class="row">
							<span><strong>Teléfono Comercio:</strong></span> 
							<label class="not-w"><?php echo $row['clitelcomercio'] ?><br /></label>
						</div>
						<div class="row">
							<span><strong>Número Adicional:</strong></span> 
							<label class="not-w"><?php echo $row['clitelresidencia'] ?><br /></label>
						</div>
						<div class="row">
							<span><strong>Cuota inicial:</strong></span> 
							<label class="not-w"><?php echo number_format($row['solcuota'], 2) ?><br /></label>
						</div>
						<div class="row">
							<span><strong>Total credito:</strong></span> 
							<label class="not-w"><?php echo number_format($row['cartotal'], 2) ?><br /></label>
						</div>
						<div class="row">
							<span><strong>Valor cuota:</strong></span> 
							<label class="not-w"><?php echo number_format($row['carcuota'], 2) ?><br /></label>
						</div>
						<div class="row">
							<span><strong>Valor pagado:</strong></span> 
							<label class="not-w"><?php echo number_format($row['cartotal'] - $row['carsaldo'], 2) ?><br /></label>
						</div>
						<div class="row">
							<span><strong>Saldo:</strong></span> 
							<label class="not-w"><?php echo number_format($row['carsaldo'], 2) ?><br /></label>
						</div>
						<div class="row">
							<span><strong>Dias en mora:</strong></span> 
							<label class="not-w"><?php echo $mora; ?></label>
						</div>
					</div>
				</div>
				</br>
				<?php
				if ($row['solposcontado'] == '1') {
					echo "<strong style ='color:red;'>ES UN CLIENTE CON POSIBLE PAGO DE CONTADO</strong>";
				} elseif ($row['carestado'] == 'CASTIGADA') {
					echo "<strong style ='color:blue;'>ES UNA CARTERA CASTIGADA</strong>";
				} elseif ($row['carventacartera'] == 1) {
					echo "<strong style ='color:#D816F1;'>ES UNA VENTA DE CARTERA</strong>";
				}
				?>
				</br>
				<div class="reporte">
					<!-- PDF de la solicitud -->
					<img style="margin:0 3px;" src="<?php echo $r ?>imagenes/iconos/pdf.png" class="pdf" data-rel="<?php echo $r . 'pdf/solicitudes/' . $row['solempresa'] . '/' . $row['solid'] . '.pdf' ?>" title="PDF de la Solicitud" />
					<!-- Comentarios realizados al momento de realizar la solicitud -->
					<img src="<?php echo $r ?>imagenes/iconos/comments.png" class="historico" data-rel="notas.php?id1=<?php echo $row['solempresa'] . '&id=' . $row['solid'] ?>" title="Historia de la solicitud" />
					<!-- Movimientos -->
					<a href="movimientos.php?<?php echo 'id1=' . $empresa . '&id2=' . $numero . '&' . $filtro ?>" onClick="carga()"><img src="<?php echo $r ?>imagenes/iconos/application_side_list.png" title="Movimientos" /></a> 
					<a href="historial.php?<?php echo 'id1=' . $empresa . '&id2=' . $numero . '&' . $filtro ?>" onClick="carga()"><img src="<?php echo $r ?>imagenes/iconos/application_edit.png" title="Historial" /></a>
					<img src="<?php echo $r ?>imagenes/iconos/pdf.png" class="pdf" data-rel="pdfdetalle.php?<?php echo 'id1=' . $empresa . '&id2=' . $numero ?>" title="Pdf del detalle" />
				</div>
				<table id="tabla">
					<thead>
						<tr>
							<th>N.Cuota</th>
							<th>F. Obligacion</th>
							<th>F. Pago</th>
							<th>Cuota</th>
							<th>Descuento</th>
							<th>Saldo</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$qry = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '" . $row['carempresa'] . "' AND dcafactura = '" . $row['carfactura'] . "' ORDER BY dcacuota");
						while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
							if ($row['dcaestado'] == 'CANCELADO') {
								echo '<tr style="background-color:#e1dfdf">
								<td align="center">' . $row['dcacuota'] . '</td>
								<td align="center">' . $row['dcafecha'] . '</td>
								<td align="center">' . $row['dcafepag'] . '</td>
								<td align="right">' . number_format($row['dcavalor'], 2) . '</td>
								<td align="right">' . number_format($row['dcadescuento'], 2) . '</td>
								<td align="right">' . number_format($row['dcasaldo'], 2) . '</td></tr>';
							} elseif ($row['dcafecha'] < date('Y-m-d')) {
								echo '<tr style="background-color:#f27070">
								<td align="center">' . $row['dcacuota'] . '</td>
								<td align="center">' . $row['dcafecha'] . '</td>
								<td align="center">' . $row['dcafepag'] . '</td>
								<td align="right">' . number_format($row['dcavalor'], 2) . '</td>
								<td align="right">' . number_format($row['dcadescuento'], 2) . '</td>
								<td align="right">' . number_format($row['dcasaldo'], 2) . '</td></tr>';
							} else {
								echo '<tr>
								<td align="center">' . $row['dcacuota'] . '</td>
								<td align="center">' . $row['dcafecha'] . '</td>
								<td align="center">' . $row['dcafepag'] . '</td>
								<td align="right">' . number_format($row['dcavalor'], 2) . '</td>
								<td align="right">' . number_format($row['dcadescuento'], 2) . '</td>
								<td align="right">' . number_format($row['dcasaldo'], 2) . '</td></tr>';
							}
						}
						?>
					</tbody>
				</table>
				<p class="boton">
					<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href = 'listar.php?<?php echo $filtro;?>'">atras</button>
				</p>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<div id="modal" style="display:none"></div>
	<div id="dialog2" title="Historico de Solicitudes" style="display:none; "></div>
</body>

</html>