<?php
$r = '../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
$empresa = $numero = $cliente = $fecha1 = $fecha2 = $prefijo = "";
if (isset($_POST['consultar'])) {
	$empresa = $_POST['empresa'];
	$numero = $_POST['numero'];
	$cliente = $_POST['cliente'];
	$fecha1 = $_POST['fecha1'];
	$fecha2 = $_POST['fecha2'];
	$prefijo = $_POST['prefijo'];
	//$covinet = $_POST['covinet'];
} elseif(isset($_GET['empresa'])) {
	$empresa = $_GET['empresa'];
	$numero = $_GET['numero'];
	$cliente = $_GET['cliente'];
	$fecha1 = $_GET['fecha1'];
	$fecha2 = $_GET['fecha2'];
	$prefijo = $_GET['prefijo'];
	//$covinet = $_GET['covinet'];
}

$filtro = 'empresa=' . $empresa . '&prefijo=' . $prefijo . '&numero=' . $numero . '&cliente=' . $cliente . '&fecha1=' . $fecha1 . '&fecha2=' . $fecha2;
$con = 'SELECT * FROM (((carteras INNER JOIN empresas ON carempresa = empid) INNER JOIN clientes ON carcliente = cliid) INNER JOIN solicitudes ON (solempresa = carempresa AND solfactura = carfactura)) LEFT JOIN usuarios ON usuid = carpromotor';
$ord = ' ORDER BY carfecha DESC, carfactura DESC';

/* if ($covinet == 1) {
	$ord = ' AND carcovinet = 1  ORDER BY carfecha DESC, carfactura DESC';
} else {
	$ord = 'ORDER BY carfecha DESC, carfactura DESC';
} */


    /* Los parametros de la consulta sql se genera dinamicamente 
    en base a los datos recibidos para delimitar los resultados */
    $parameters = [];
    if($empresa != "")
        array_push($parameters, "carempresa LIKE '%$empresa%'" );
    if($numero != "")
        array_push($parameters, "carfactura = '$numero'" );
    if($cliente != "")
        array_push($parameters, "carcliente LIKE '%$cliente%'" );
    if($fecha1 != "")
        array_push($parameters, "carfecha BETWEEN '$fecha1' AND '$fecha2'" );

    // Consulta base
    $sql = $con;
    foreach ($parameters as $index => $parameter) {
        // Se agregan los parametros del WHERE
        if($index == 0)
            $sql .= " WHERE " . $parameter;
        else
            $sql .= " AND " . $parameter;
    }
    // Se completa la consulta
	$sql .= $ord;
	
$qry = $db->query($sql);

?>
<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>

	<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
	<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#tabla').dataTable({
				'bJQueryUI': true,
				'sPaginationType': 'full_numbers',
				'oLanguage': {
					'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
				},
				'bSort': false,
			});

		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a class="current">Cartera</a>
			</article>
			<article id="contenido">
				<h2>Listado de cartera <?php  ?></h2>
				<table id="tabla">
					<thead>
						<tr>
							<th>Empresa</th>
							<th>Factura</th>
							<th>Solicitud</th>
							<th>Cliente</th>
							<th>Promotor</th>
							<th>Fecha</th>
							<th>Cuota ini.</th>
							<th>T/credito</th>
							<th>Saldo</th>
							<th>Estado</th>
							<th>F. Castigo</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php
						while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
						?>
							<tr>
								<td title="<?php echo $row['empid'] . ' ' . $row['empdigito'] ?>"><?php echo $row['empnombre'] ?></td>
								<td align="center"><?php echo $row['carfactura'] ?></td>
								<td align="center"><?php echo $row['solid'] ?></td>
								<td title="<?php echo $row['cliid'] . ' ' . $row['clidigito'] ?>"><?php echo $row['clinombre'] . ' ' . $row['clinom2'] . ' ' . $row['cliape1'] . ' ' . $row['cliape2'] ?></td>
								<td title="<?php echo $row['usuid'] ?>"><?php echo $row['usunombre'] ?></td>
								<td align="center"><img src="<?php echo $r ?>imagenes/iconos/date.png" title="<?php echo $row['carfecha'] ?>" /></td>
								<td align="right"><?php echo number_format($row['solcuota'], 2) ?></td>
								<td align="right"><?php echo number_format($row['cartotal'], 2) ?></td>
								<td align="right"><?php echo number_format($row['carsaldo'], 2) ?></td>
								<td align="center"><?php echo $row['carestado'] ?></td>
								<?php 
								if($row['carfcastigo'] != "") {
									echo '<td align="center"><img src="' . $r . 'imagenes/iconos/date.png" title="'. $row['carfcastigo']. '" /></td>';
								} else {
									echo '<td align="center">-</td>';
								}
								?>
								
								<td align="center"><a href="<?php echo $r ?>modulos/cartera/detalle.php?id1=<?php echo $row['carempresa'] . '&id2=' . $row['carfactura'] . '&solid=' . $row['solid'] . '&' . $filtro ?>" onClick="carga();" title="Detalle"><img src="<?php echo $r ?>imagenes/iconos/page_white_find.png" class="grayscale" /></a></td>
							</tr>
						<?php
						}
						?>
					</tbody>
				</table>
				<p class="boton">
					<button type="button" class="btn btn-primary btnatras" onClick=" carga(); location.href = 'consultar.php'">atras</button>
				</p>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<?php
	if (isset($_GET['mensaje'])) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['mensaje'] . '</div>';
	?>
</body>

</html>