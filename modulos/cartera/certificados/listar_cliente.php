<?php

    $r = '../../../';
    require($r . 'incluir/session.php');
    require($r . 'incluir/connection.php');
    
    $id = $nombre = "";
    if(isset($_POST['id']))
    {
        $id = $_POST['id'];
        $nombre = $_POST['nombre'];
    }
    elseif(isset($_GET['id']))
    {
        $id = $_GET['id'];
        $nombre = $_GET['nombre'];
    }

    $filtro = "id=".$id."&nombre=".$nombre;

    $con = "SELECT * FROM (clientes LEFT JOIN departamentos ON clidepresidencia = depid) LEFT JOIN ciudades ON (clidepresidencia = ciudepto AND cliciuresidencia = ciuid) INNER JOIN carteras ON carcliente = cliid";
    /* Los parametros de la consulta sql se genera dinamicamente 
    en base a los datos recibidos para delimitar los resultados */
    $parameters = [];
    if($nombre != "")
        array_push($parameters, "clinombre LIKE '%$nombre%'" );
    if($id != "")
        array_push($parameters, "cliid LIKE '%$id%'" );

    // Consulta base
    $sql = $con;
    foreach ($parameters as $index => $parameter) {
        // Se agregan los parametros del WHERE
        if($index == 0)
            $sql .= " WHERE " . $parameter;
        else
            $sql .= " AND " . $parameter;
    }
    $qry = $db->query($sql);
?>
<!doctype html>
<html>

<head>

    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>
    <link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
    <script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
    <script>
        $(document).ready(function() {
            $('#tabla').dataTable({ //QRY PARA MANEJO Y VISUALIZACION DE TABLAS
                'bJQueryUI': true,
                'bStateSave': true,
                'sPaginationType': "full_numbers",
                'oLanguage': {
                    'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
                },
                'aoColumnDefs': [{
                    'bSortable': false,
                    'aTargets': [5]
                }],
            });

        });
    </script>
</head>

<body>
    <?php require($r . 'incluir/src/login.php') ?>
    <!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
    <section id="principal">
        <?php require($r . 'incluir/src/cabeza.php') ?>
        <!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
        <?php require($r . 'incluir/src/menu.php') ?>
        <!-- INCLUIMOS MENU PRINCIPAL -->
        <article id="cuerpo">
            <article class="mapa">
                <a href="#">Principal</a>
                <div class="mapa_div"></div><a href="#">Administracion</a>
                <div class="mapa_div"></div><a class="current">Clientes</a>
            </article>
            <article id="contenido">
                <h2>Listado de clientes</h2>
                <div class="reporte">

                </div>
                <!-- INICIO DE TABLA -->
                <table id="tabla">
                    <thead>
                        <tr>
                            <th>Identificacion</th>
                            <th>Nombre</th>
                            <th>Departamento</th>
                            <th>Municipio</th>
                            <th title="Consultar">Paz y Salvo</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {  //INFORMACION DE LA CONSULTA
                        ?>
                            <tr>
                                <td><?php echo $row['cliid'] . ' ' . $row['clidigito'] ?></td>
                                <td><?php echo $row['clinombre'] . ' ' . $row['clinom2'] . ' ' . $row['cliape1'] . ' ' . $row['cliape2'] ?></td>
                                <td><?php echo $row['depnombre'] ?></td>
                                <td><?php echo $row['ciunombre'] ?></td>
                                <td align="center"><a href="pazysalvo.php?<?php echo 'cliente=' . $row['cliid'] . '&' . $filtro ?>" onClick="carga()" title="Consultar"><img src="<?php echo $r ?>imagenes/iconos/eye.png" class="grayscale" /></a></td><!-- ENVIA POR GET LAS VARIABLES NECESARIAS PARA MODIFICAR UN CLIENTE -->
                            </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                </table> <!-- FIN DE LA TABLA -->
                <p class="boton">
                    <button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href='consultar.php'">Atras</button>
                </p>
            </article>
        </article>
        <?php require($r . 'incluir/src/pie.php') ?>
        <!-- BOTON PARA VOLVER A LA PARTE SUPERIOR  -->
    </section>
    <?php
    if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>'; // VENTANA MODAL ERROR
    elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>'; // VENTANA MODAL EXITOSO
    ?>
</body>

</html>