<?php
/* OBSOLETO BY: KAVV */
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
?>
<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#fecha1').datepicker({
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
				maxDate: '+0D',
				onClose: function(selectedDate) {
					$('#fecha2').datepicker('option', 'minDate', selectedDate);
				}
			}).keypress(function(event) {
				event.preventDefault()
			});
			$('#fecha2').datepicker({
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
				maxDate: '+0D',
				onClose: function(selectedDate) {
					$('#fecha1').datepicker('option', 'maxDate', selectedDate);
				}
			}).keypress(function(event) {
				event.preventDefault()
			});

		});
	</script>

</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a class="current">Cartera</a>
				<div class="mapa_div"></div><a class="current">Solicitar Certificado Paz y Salvo</a>
			</article>
			<article id="contenido">

				<form id="form" name="form" action="solicitarcertificado.php" method="get">
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">Cartera - Solicitar Certificado, Paz y Salvo</legend>
						<p>
							<label for="idcliente">Cliente:</label>
							<input type="text" name="idcliente" class="id validate[required] text-input" title="Digite la identificaci&oacute;n del cliente" onkeypress="return isNumberKey(event)" />
						</p>
						<p class="boton">
							<button type="submit" class="btn btn-primary btnconsulta" name="consultar" value="consultar" onclick="carga()">consultar</button>
						</p>
						<?php
						if(isset($_GET['idcliente']))
						{
							$idcliente = $_GET['idcliente'];
							if ($idcliente != NULL) {
								include("buscador_certificado.php");
							}
						}
						?>
					</fieldset>
				</form>

			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
</body>

</html>