<?php
    $r = '../../../';
    require($r . 'incluir/session.php');
    require($r . 'incluir/connection.php');
    include($r . 'incluir/fpdf/fpdf.php');

    $cliente = $_GET['idcliente'];
    $empresa = $_GET['empresa'];
    $factura = $_GET['factura'];
    $filtro = "id=" . $_GET['id'] . "&nombre=" . $_GET['nombre'];

    if (isset($_GET['log'])) {
        $redirect = "ver_archivo_pdf.php?idcliente=" . $cliente . "&empresa=" . $empresa . "&factura=" . $factura . "&" . $filtro;

        //Log de acciones realizadas por usuario en la BD	
        $qrylogsregister = $db->query("SELECT * FROM usuarios INNER JOIN perfiles ON usuperfil = perid WHERE usuid = '" . $_SESSION['id'] . "'"); //verificacion usuario por ID de sesion
        $rowlogsregister = $qrylogsregister->fetch(PDO::FETCH_ASSOC);
        $qrylogsregister = $db->query("INSERT INTO logs ( usuario, id_usuario , ip_usuario , accion_usuario, fecha_accion) VALUES ( '" . $rowlogsregister['usunombre'] . "', '" . $_SESSION['id'] . "', '" . $_SERVER['REMOTE_ADDR'] . "' , 'GENERO CERTIFICADO DE PAZ Y SALVO A NOMBRE DE: " . $nombre_completo_cliente . " CON NUMERO DE IDENTIFICACION " . $row["cliid"] . " CON REFERENCIA LA FACTURA NUMERO " . $row["carfactura"] . "'  , '" . date("Y-m-d H:i:s") . "' );"); //anexo de informacion de accion a la BD tabla LOGS
        header("location:" . $redirect);
        exit();
    }



    $sql = " SELECT * FROM (((carteras INNER JOIN empresas ON carempresa = empid) INNER JOIN clientes ON carcliente = cliid) INNER JOIN solicitudes ON (solempresa = carempresa AND solfactura = carfactura))  where cliid = '$cliente' AND carempresa = '$empresa' AND carfactura = '$factura'; ";
    

    class PDF extends FPDF
    {
        // Cabecera de página
        function Header()
        {
            // Logo
            //$this->Image('../../../imagenes/logos/plantillaecu.jpg',0,0,215,300);
            // Arial 
            $this->SetFont('Arial', '', 25);

            // Título
            $this->SetTextColor(220, 50, 50);
            $this->Cell(0, 20, 'Paz y Salvo', 0, 0, 'C');
            // Salto de línea
            $this->Ln(25);
        }

        // Pie de página
        function Footer()
        {
            // Posición: a 1,5 cm del final
            $this->SetY(-15);
            // Arial italic 8
            $this->SetFont('Arial', 'I', 8);
            // Número de página
            $this->Cell(0, 10, 'Nicaragua, Managua. Club Terraza, 350 metros al Norte. (505) 2227-0716 / 8213-6506', 0, 0, 'C'); //.$this->PageNo().'/{nb}',0,0,'C');
        }
    }

    //nuevo pdf y seleccion de fuente y tamaño a usar
    $pdf = new PDF();
    $pdf->SetFont('Times', '', 13);


    $qry = $db->query($sql);
    while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {

        $pdf->AliasNbPages();
        $pdf->AddPage();

        $nombre_completo_cliente = $row["clinombre"] . " " . $row["clinom2"] . " " . $row["cliape1"] . " " . $row["cliape2"];

        $pdf->MultiCell(0, 6, utf8_decode('Certifica que el Señor o señora ' . $row["clinombre"] . " " . $row["clinom2"] . " " . $row["cliape1"] . " " . $row["cliape2"] . ' con numero de identificacion ' . $row["cliid"] . ' ha cancelado la suma adeudada.'), 0, 6);
        $pdf->Ln(5);
        $pdf->Cell(0, 6, 'Solicitud de Pedido: ' . $row['solid'], 0, 6, '');
        $pdf->Cell(0, 6, 'Factura numero: ' . $row['carfactura'], 0, 6, '');
        $pdf->Cell(0, 6, 'Total deuda: ' . $row['cartotal'], 0, 6, '');
        $pdf->Cell(0, 6, 'Saldo pendiente: ' . $row['carsaldo'], 0, 6, '');
        $pdf->Cell(0, 6, 'Total de cuotas: ' . $row['carncuota'], 0, 6, '');
        $pdf->Cell(0, 6, 'Estado de la cartera: ' . $row['carestado'], 0, 6, '');

        
    }

    $arrayMeses = array(
        'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
        'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'
    );

    $arrayDias = array(
        'Domingo', 'Lunes', 'Martes',
        'Miercoles', 'Jueves', 'Viernes', 'Sabado'
    );


    $pdf->Ln(5);
    $pdf->Cell(0, 6, 'Este paz y salvo no exime al girador de otros titulos pendientes. ', 0, 6, 'L');
    $pdf->Ln(5);
    $pdf->Cell(0, 6, 'El documento se expide el ' . $arrayDias[date('w')] . ", " . date('d') . " de " . $arrayMeses[date('m') - 1] . " de " . date('Y') . '.', 0, 6);
    $pdf->Ln(30);
    $pdf->Cell(0, 6, 'Cordialmente: ', 0, 6);
    $pdf->Ln(10);
    $pdf->Image('../../../imagenes/logos/firma.jpg', 5, 190, 80);
    $pdf->Cell(0, 6, 'Nombre completo ', 0, 6);
    $pdf->Cell(0, 6, 'Cargo ', 0, 6);
    $pdf->Cell(0, 6, 'Numero de contacto', 0, 6);
    $pdf->Cell(0, 6, 'Correos', 0, 6);
    $pdf->Output('Paz y Salvo_' . $cliente . '.pdf', 'i');


?>