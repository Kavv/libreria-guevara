<?php


    $r = '../../../';
    require($r . 'incluir/session.php');
    require($r . 'incluir/connection.php');
    $id = $_GET['cliente'];
    
    $filtro = "id=".$_GET['id']."&nombre=".$_GET['nombre'];

	$sql = " SELECT * FROM (((carteras INNER JOIN empresas ON carempresa = empid) INNER JOIN clientes ON carcliente = cliid) INNER JOIN solicitudes ON (solempresa = carempresa AND solfactura = carfactura)) where cliid = '$id'; ";
	$qry = $db->query($sql);
	
?>
<!doctype html>
<html>

<head>

    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>
    <style>
        .cartera-data{
            border-radius: 15px;
            background-color: #cce4ee;
        }
        .salvo-no {
            font-weight: bold;
            color: #d13535;
        }
        label{
            width: 100%;
        }
    </style>
</head>

<body>
    <?php require($r . 'incluir/src/login.php') ?>
    <!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
    <section id="principal">
        <?php require($r . 'incluir/src/cabeza.php') ?>
        <!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
        <?php require($r . 'incluir/src/menu.php') ?>
        <!-- INCLUIMOS MENU PRINCIPAL -->
        <article id="cuerpo">
            <article class="mapa">
                <a href="#">Principal</a>
                <div class="mapa_div"></div><a href="#">Administracion</a>
                <div class="mapa_div"></div><a class="current">Clientes</a>
            </article>
            <article id="contenido">
                <h3>Resultados del cliente con identificacion <?php echo $id ?> </h3>
                <div class="row">
                    <?php while ($row = $qry->fetch(PDO::FETCH_ASSOC)) { ?>
                        <div class="col-md-6">
                            <div class="p-3 m-2 cartera-data">
                                <h5 class="text-center">Cartera asociada a la factura # <?php echo $row["carfactura"]; ?></h5>
                                <?php if ($row["carsaldo"] > 0) { ?>
            
                                <label>El cliente: <?php echo $row["clinombre"] . " " . $row["clinom2"] . " " . $row["cliape1"] . " " . $row["cliape2"] ?> </label>  
                                <label class="salvo-no">No se encuentra a Paz y Salvo actualmente posee un saldo pendiente de <?php echo number_format($row["carsaldo"],2)?></label>
                                
                                <?php } else { ?>
                                    <label>Nombre del cliente: <?php echo $row["clinombre"] . " " . $row["clinom2"] . " " . $row["cliape1"] . " " . $row["cliape2"] ?> </label>
                                    <label>Empresa del proceso:  <?php echo $row["carempresa"]?> </label>
                                    <label>Estado de la cartera:  <?php echo $row["carestado"]?> </label>
                                    <label>Saldo del Cliente: <?php echo $row["carsaldo"]?> </label>
                                    <label>Factura: <?php echo $row["carfactura"]?>  </label>
                                    <label>Email: <?php echo $row["cliemail"]?>  </label>
                                    <label>Numero Celular: <?php echo $row["clicelular"]?>  </label>
                                    <label>Direccion personal del cliente: <?php echo $row["clibarresidencia"] . " , " . $row["clidirresidencia"]?>  </label>
                                    <br>
                                    <div class="row d-flex justify-content-center">
                                        <?php 
                                            $redirect = "ver_archivo_pdf.php?idcliente=" . $id . "&empresa=" . $row["carempresa"] . "&factura=" . $row["carfactura"] . "&log=1&" . $filtro;
                                        ?>
                                        <a class="btn btn-info" target='_BLANK' href='<?php echo $redirect ?>'><img src='<?php echo $r ?>imagenes/iconos/pdf.png' id='pdf' title='Pdf del detalle' /> Ver Certificado PDF </a>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>

                    <?php } ?>
                    
                    <?php if($qry->rowCount() == 0) { ?>
                        <div class="col-md-12 mt-3">
                            <div class="text-center p-5 cartera-data">
                                <h4 for="">No posee ninguna cartera</h4>
                                <label for="">Esto puede ser por que no ha realizado ninguna facturacion o por que ha pagado sus facturas de contado.</label>
                            </div>
                        </div>
                    <?php } ?>
                
                </div>
                <p class="boton mt-3">
                    <button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href='listar_cliente.php?<?php echo $filtro?>'">Atras</button>
                </p>

            </article>
        </article>
        <?php require($r . 'incluir/src/pie.php') ?>
    </section>
</body>

</html>