<?php
	$r = '../../../../';
	require($r . 'incluir/session.php');
	require($r . 'incluir/connection.php');
	$numero = $exacta = $cliente = "";
	if (isset($_POST['consultar'])) {
		$numero = $_POST['numero'];
		if (isset($_POST['exacta']))
			$exacta = $_POST['exacta'];
		$cliente = $_POST['cliente'];
	}
	if (isset($_GET['numero'])) {
		$numero = $_GET['numero'];
		$exacta = $_GET['exacta'];
		$cliente = $_GET['cliente'];
	}
	$filtro = 'numero=' . $numero . '&exacta=' . $exacta . '&cliente=' . $cliente;


	$con = "SELECT * FROM (carteras INNER JOIN empresas ON carempresa = empid) INNER JOIN clientes ON carcliente = cliid";

	/* Los parametros de la consulta sql se genera dinamicamente 
		en base a los datos recibidos para delimitar los resultados */
	$parameters = [];
	array_push($parameters, "(carestado = 'ACTIVA' OR carestado = 'CASTIGADA' OR carestado = 'RECOLECCION' OR carestado = 'PENDIENTE' OR carestado = 'MOROSO' OR carestado = 'RECOLECCION RAZONADA')");

	if ($cliente != "")
		array_push($parameters, "carcliente LIKE '%$cliente%'");
	if ($numero != "" && $exacta == "1")
		array_push($parameters, "carfactura = '$numero'");
	if ($numero != "" && $exacta != "1")
		array_push($parameters, "carfactura LIKE '%$numero%'");

	// Consulta base
	$sql = $con;
	foreach ($parameters as $index => $parameter) {
		// Se agregan los parametros del WHERE
		if ($index == 0)
			$sql .= " WHERE " . $parameter;
		else
			$sql .= " AND " . $parameter;
	}

	$qry = $db->query($sql);


?>
<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>

	<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
	<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#tabla').dataTable({
				'bStateSave': true,
				'bJQueryUI': true,
				// 'aaSorting': [ [0,'desc'], [1,'desc'] ],
				'sPaginationType': 'full_numbers',
				'oLanguage': {
					'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
				},
				'aoColumnDefs': [{
					'bSortable': false,
					'aTargets': [4]
				}]
			});

		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Cartera</a>
				<div class="mapa_div"></div><a class="current">Pagos</a>
			</article>
			<article id="contenido">
				<h2>Listado de cartera para aplicar pago</h2>
				<table id="tabla">
					<thead>
						<tr>
							<th>Empresa</th>
							<th>Cliente</th>
							<th>Factura</th>
							<th>Saldo</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php
						while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
						?>
							<tr>
								<td title="<?php echo $row['carempresa'] ?>"><?php echo $row['empnombre'] ?></td>
								<td title="<?php echo $row['carcliente'] ?>"><?php echo $row['clinombre'] . ' ' . $row['clinom2'] . ' ' . $row['cliape1'] . ' ' . $row['cliape2'] ?></td>
								<td align="center"><?php echo $row['carfactura'] ?></td>
								<td align="right"><?php echo $row['carsaldo'] ?></td>
								<td align="center"><a href="datos.php?id1=<?php echo $row['carempresa'] . '&id2=' . $row['carfactura'] . '&' . $filtro ?>" onClick="carga()"><img src="<?php echo $r ?>imagenes/iconos/tick.png" class="grayscale" title="Aplicar" /></a></td>
							</tr>
						<?php
						}
						?>
					</tbody>
				</table>
				<p class="boton">
					<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href = 'consultar.php'">atras</button>
				</p>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<?php
	if (isset($_GET['error'])) echo "<div id='dialog-message' title='Error'><span class='ui-icon ui-icon-circle-close' style='float:left; margin:3px 7px 7px 0;'></span>" . $_GET['error'] . "</div>";
	?>
</body>

</html>