<?php
$r = '../../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
$empresa = $_GET['id1'];
$numero = $_GET['id2'];
$cuota = $_GET['cuota'];
$filtro = 'numero=' . $_GET['numero'] . '&exacta=' . $_GET['exacta'] . '&cliente=' . $_GET['cliente'];
if ($cuota > 1) {
	$row = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '$empresa' AND dcafactura = '$numero' AND dcacuota = $cuota - 1")->fetch(PDO::FETCH_ASSOC);
	if ($row['dcaestado'] == 'ACTIVA') {
		$error = 'La cuota anterior no ha sido cancelada';
		header('Location:datos.php?id1=' . $empresa . '&id2=' . $numero . '&' . $filtro . '&error=' . $error);
		exit();
	}
	$row = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '$empresa' AND dcafactura = '$numero' AND dcacuota = $cuota")->fetch(PDO::FETCH_ASSOC);
	if ($row['dcaestado'] == 'CANCELADO') {
		$error = 'La cuota actual ya habia sido pagada';
		header('Location:datos.php?id1=' . $empresa . '&id2=' . $numero . '&' . $filtro . '&error=' . $error);
		exit();
	}
}
$row = $db->query("SELECT * FROM (((((carteras INNER JOIN empresas ON carempresa = empid) INNER JOIN clientes ON carcliente = cliid) INNER JOIN solicitudes ON (carempresa = solempresa AND carfactura = solfactura)) INNER JOIN departamentos ON depid = soldepcobro) INNER JOIN ciudades ON (soldepcobro = ciudepto AND solciucobro = ciuid)) INNER JOIN detcarteras ON (dcaempresa = carempresa AND dcafactura = carfactura) WHERE dcaempresa = '$empresa' AND dcafactura = '$numero' AND dcacuota = $cuota")->fetch(PDO::FETCH_ASSOC);
?>
<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<script type="text/javascript">
		$(document).ready(function() {

			$('#descuento').change(function(event) {
				var salnuevo = parseInt($("#saldo").val()) - (parseInt($("#vaplicar").val()) + parseInt($("#descuento").val()));
				$("#salnue").val(salnuevo);
			});
			$('#vaplicar').change(function(event) {
				var salnuevo = parseInt($("#saldo").val()) - (parseInt($("#vaplicar").val()) + parseInt($("#descuento").val()));
				$("#salnue").val(salnuevo);
			});

			$('#fecha').datepicker({
				dateFormat: 'yy-mm-dd',
				changeYear: true,
				changeMonth: true,
				maxDate: '+0D',
			}).keypress(function(event) {
				event.preventDefault()
			});

		});
	</script>

</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Cartera</a>
				<div class="mapa_div"></div><a class="current">Pagos</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="finalizar.php?<?php echo $filtro ?>" method="post">
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">Detalle para pago</legend>
						<p>
							<label for="empresa">Empresa:</label>
							<input type="hidden" name="id1" value="<?php echo $row['carempresa'] ?>" /><?php echo $row['empnombre'] ?>
						</p>
						<p>
							<label for="id">Solicitud:</label>
							<input type="text" name="id" class="pedido" value="<?php echo $row['solid'] ?>" readonly />
						</p>
						<p>
							<label for="factura">Factura:</label>
							<input type="text" name="id2" class="consecutivo" value="<?php echo $row['carfactura'] ?>" readonly />
						</p>
						<p>
							<label for="cliente">Cliente:</label>
							<input type="hidden" name="cliente" value="<?php echo $row['cliid'] ?>" /><?php echo $row['clinombre'] . ' ' . $row['clinom2'] . ' ' . $row['cliape1'] . ' ' . $row['cliape2'] ?>
						</p>
						<p>
							<label for="email">Email:</label>
							<input type="text" name="email" class="email validate[custom[email]] text-input" value="<?php echo $row['cliemail'] ?>" />
						</p>
						<p>
							<label for="depto">Ubicacion:</label>
							<label><?php echo $row['depnombre'] . ' / ' . $row['ciunombre'] ?></label>
						</p>
						<p>
							<label for="direccion">Direccion:</label>
							<label><?php echo $row['solbarcobro'] . ', ' . $row['solcobro'] ?></label>
						</p>
						<p>
							<label for="telefono">Telefonos:</label>
							<label><?php echo $row['clicelular'] . ' / ' . $row['clitelresidencia'] . ' / ' . $row['clitelcomercio'] ?></label>
						</p>
						<p>
							<label for="feccom">F. de compromiso:</label>
							<label><?php echo $row['dcafecha'] ?></label>
						</p>
						<p>
							<label for="fecpago">Fecha de pago:</label>
							<input type="text" id="fecha" name="fecpago" class="fecha validate[required] text-input" value="<?php echo date('Y-m-d') ?>" />
						</p>
						<p>
							<label for="saldo">Saldo:</label>
							<input type="text" id="saldo" class="valor" name="saldo" value="<?php echo $row['carsaldo'] ?>" readonly />
						</p>
						<p>
							<label for="vcuota">Valor cuota:</label>
							<input type="text" class="valor" name="vcuota" value="<?php echo $row['carcuota'] ?>" readonly />
						</p>
						<p>
							<label for="vaplicar">Valor a aplicar:</label>
							<input type="text" id="vaplicar" class="valor validate[required, custom[onlyNumberSp]] text-input" name="vaplicar" value="<?php echo $row['carcuota'] ?>" />
						</p>
						<p>
							<label for="descuento">Descuento:</label>
							<input type="text" id="descuento" class="valor validate[required, custom[onlyNumberSp]] text-input" name="descuento" value="0" />
						</p>
						<p>
							<label for="salnue">Saldo nuevo:</label>
							<input type="text" id="salnue" class="valor validate[min[0]]" name="salnue" value="<?php echo $row['carsaldo'] - $row['carcuota'] ?>" readonly />
						</p>


						<div class="input-group my-2">
							<label for="salnue">La cuota actual es la:</label>
							<input type="text" name="cuota" class="cantidad col-md-5 not-w" value="<?php echo $row["dcacuota"] ?>" readonly />
							<div class="input-group-append col-md-7 p-0">
								<span class="input-group-text col-md-3">De</span>
								<input type="text" name="cantidad" class="cantidad col-md-9 not-w" value="<?php echo $row['carncuota'] ?>" readonly />
							</div>
						</div>


						<p>
							<label for="fpago">Forma de pago:</label>
							<select name="fpago" class="validate[required] text-input">
								<option value="">SELECCIONE</option>
								<?php
								$qry = $db->query("SELECT * FROM fpagos ORDER BY fpanombre");
								while ($row = $qry->fetch(PDO::FETCH_ASSOC))
									echo '<option value="' . $row['fpaid'] . '">' . $row['fpanombre'] . '</option>';
								?>
							</select>
						</p>
						<p class="boton">
							<button type="button" class="btn btn-primary btnatras" name="atras" onClick="carga(); location.href = 'datos.php?id1=<?php echo $empresa . '&id2=' . $numero . '&' . $filtro ?>'">atras</button>
							<button type="submit" class="btn btn-primary btnfinalizar" name="pagar" value="pagar">pagar</button>
						</p>
					</fieldset>
				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<?php
	if (isset($_GET['error'])) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0"></span>' . $_GET['error'] . '</div>';
	elseif (isset($_GET['mensaje'])) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0"></span>' . $_GET['mensaje'] . '</div>';
	?>
</body>

</html>