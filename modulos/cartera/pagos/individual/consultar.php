<?php
$r = '../../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
?>
<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>

</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Cartera</a>
				<div class="mapa_div"></div><a class="current">Pagos</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="listar.php" method="post">
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">Consulta de cartera a pagar</legend>
						<p>
							<label for="cliente">Cliente:</label>
							<input type="text" name="cliente" class="id validate[custom[onlyNumberSp]]" />
						</p>
						<p>
							<label for="numero">Factura:</label>
							<input type="text" name="numero" class="consecutivo validate[custom[onlyNumberSp]]" />
							<em style="font-size:14px;">Busqueda exacta  </em><input type="checkbox" name="exacta" value="1" title="exacta" />
						</p>
						<div class="row">
							<div class="col-md-12 col-lg-6">
								<button type="submit" class="btnconsulta btn btn-primary btn-block mt-2" name="consultar" value="Buscar">consultar</button> <!-- BOTON CONSULTAR -->
							</div>
							<div class="col-md-12 col-lg-6">
								<button type="submit" class="btnconsulta btn btn-primary btn-block mt-2" name="show-all" value="Buscar">Mostrar Todo</button> </div>
						</div>
					</fieldset>
				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<?php
	if (isset($_GET["error"])) echo "<div id='dialog-message' title='Error'><span class='ui-icon ui-icon-circle-close' style='float:left; margin:3px 7px 7px 0;'></span>" . $_GET["error"] . "</div>";
	elseif (isset($_GET["mensaje"])) echo "<div id='dialog-message' title='Correcto'><span class='ui-icon ui-icon-circle-check' style='float:left; margin:3px 7px 7px 0;'></span>" . $_GET["mensaje"] . "</div>";
	?>
</body>

</html>