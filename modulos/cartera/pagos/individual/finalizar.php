<?php
	$r = '../../../../';
	require($r . 'incluir/session.php');
	require($r . 'incluir/connection.php');
	require($r . 'incluir/funciones.php');

	$filtro = 'numero=' . $_GET['numero'] . '&exacta=' . $_GET['exacta'] . '&cliente=' . $_GET['cliente'];
	if(isset($_POST['id']))
	{
		$solicitud = $_POST['id'];
		$empresa = $_POST['id1'];
		$numero = $_POST['id2'];
		$cliente = $_POST['cliente'];
		$saldo = $_POST['saldo'];
		$vcuota = $_POST['vcuota'];
		$cuota = $_POST['cuota'];
		$vaplicar = $_POST['vaplicar'];
		$descuento = $_POST['descuento'];
		// Valor original
		$vaplicar_org = $vaplicar;
		// Descuento original
		$descuento_org = $descuento;
		
		$fecpago = $_POST['fecpago'];
		$fpago = $_POST['fpago'];

		
		$num = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '$empresa' AND dcafactura = '$numero' AND dcaestado = 'ACTIVA'")->rowCount();
		// numeroaplica = Redondear hacia arriba((valor a pagarse + descuento)/ valor de la cuota)
		$numapli = ceil(($vaplicar + $descuento) / $vcuota);
		// Si la cantidad de cuotas activas es inferior al numeroaplica entonces retorna como un error
		if ($num < $numapli) {
			$error = 'No se puede aplicar por que '.($vaplicar + $descuento).' es mayor al saldo faltante';
			header('Location:cuota.php?id1=' . $empresa . '&id2=' . $numero . '&cuota=' . $cuota . '&' . $filtro . '&error=' . $error);
			exit();
		}
		$qry = $db->query("UPDATE clientes SET cliemail = '" . $_POST['email'] . "' WHERE cliid = '$cliente'");
		$qry = $db->query("UPDATE carteras SET carsaldo = carsaldo - ($vaplicar + $descuento) WHERE carempresa = '$empresa' AND carfactura = '$numero'");
		$rmax = $db->query("SELECT MAX(movnumero)+1 AS ultimo FROM movimientos WHERE movempresa = '$empresa' AND movprefijo = 'RC'")->fetch(PDO::FETCH_ASSOC);
		if ($rmax['ultimo'] == '')
			$rmax['ultimo'] = 1;
		$recibo = $rmax['ultimo'];
		while ($vaplicar >= 0.01) {
			$rowdet = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '$empresa' AND dcafactura = '$numero' AND dcacuota = $cuota")->fetch(PDO::FETCH_ASSOC);
			$acumulado = $rowdet['dcavalor'] + $rowdet['dcadescuento'];
			if ($acumulado == 0) {
				if ($vaplicar >= $vcuota) {
					$saldo = $saldo - $vcuota;
					$db->query("UPDATE detcarteras SET dcavalor = $vcuota, dcasaldo = $saldo, dcafepag = '$fecpago', dcapromotor = '" . $_SESSION['id'] . "', dcaestado = 'CANCELADO' WHERE dcaempresa = '$empresa' AND dcafactura = '$numero' AND dcacuota = $cuota");
					$vaplicar = $vaplicar - $vcuota;
					$cuota++;
				} else {
					$saldo = $saldo - $vaplicar;
					$db->query("UPDATE detcarteras SET dcavalor = $vaplicar, dcasaldo = $saldo, dcafepag = '$fecpago', dcapromotor = '" . $_SESSION['id'] . "' WHERE dcaempresa = '$empresa' AND dcafactura = '$numero' AND dcacuota = $cuota");
					$vaplicar = 0;
				}
			} else {
				if ($vaplicar >= ($vcuota - $acumulado)) {
					$saldo = $saldo - ($vcuota - $acumulado);
					$db->query("UPDATE detcarteras SET dcavalor = $vcuota - dcadescuento, dcasaldo = $saldo, dcafepag = '$fecpago', dcapromotor = '" . $_SESSION['id'] . "', dcaestado = 'CANCELADO' WHERE dcaempresa = '$empresa' AND dcafactura = '$numero' AND dcacuota = $cuota");
					$vaplicar = $vaplicar - ($vcuota - $acumulado);
					$cuota++;
				} else {
					$saldo = $saldo - $vaplicar;
					$db->query("UPDATE detcarteras SET dcavalor = $vaplicar, dcasaldo = $saldo, dcafepag = '$fecpago', dcapromotor = '" . $_SESSION['id'] . "' WHERE dcaempresa = '$empresa' AND dcafactura = '$numero' AND dcacuota = $cuota");
					$vaplicar = 0;
				}
			}
		}

		while ($descuento > 0) {
			$rowdet = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '$empresa' AND dcafactura = '$numero' AND dcacuota = $cuota")->fetch(PDO::FETCH_ASSOC);
			$acumulado = $rowdet['dcavalor'] + $rowdet['dcadescuento'];
			if ($acumulado == 0) {
				if ($descuento >= $vcuota) {
					$saldo = $saldo - $vcuota;
					$db->query("UPDATE detcarteras SET dcasaldo = $saldo, dcafepag = '$fecpago', dcadescuento = $vcuota, dcapromotor = '" . $_SESSION['id'] . "', dcaestado = 'CANCELADO' WHERE dcaempresa = '$empresa' AND dcafactura = '$numero' AND dcacuota = $cuota");
					$descuento = $descuento - $vcuota;
					$cuota++;
				} else {
					$saldo = $saldo - $descuento;
					$db->query("UPDATE detcarteras SET dcasaldo = $saldo, dcafepag = '$fecpago', dcadescuento = $descuento, dcapromotor = '" . $_SESSION['id'] . "' WHERE dcaempresa = '$empresa' AND dcafactura = '$numero' AND dcacuota = $cuota");
					$descuento = 0;
				}
			} else {
				if ($descuento >= ($vcuota - $acumulado)) {
					$saldo = $saldo - ($vcuota - $acumulado);
					$db->query("UPDATE detcarteras SET dcasaldo = $saldo, dcafepag = '$fecpago', dcadescuento = $vcuota - dcavalor, dcapromotor = '" . $_SESSION['id'] . "', dcaestado = 'CANCELADO' WHERE dcaempresa = '$empresa' AND dcafactura = '$numero' AND dcacuota = $cuota");
					$descuento = $descuento - ($vcuota - $acumulado);
					$cuota++;
				} else {
					$saldo = $saldo - $descuento;
					$db->query("UPDATE detcarteras SET dcasaldo = $saldo, dcafepag = '$fecpago', dcadescuento = dcadescuento + $descuento, dcapromotor = '" . $_SESSION['id'] . "' WHERE dcaempresa = '$empresa' AND dcafactura = '$numero' AND dcacuota = $cuota");
					$descuento = 0;
				}
			}
		}

		$db->query("INSERT INTO movimientos (movempresa, movprefijo, movnumero, movtercero, movdocumento, movfecha, movvalor, movdescuento, movsaldo, movfpago, movestado) VALUES ('$empresa', 'RC', '$recibo', '$cliente', '$numero', '$fecpago', $vaplicar_org, $descuento_org, $saldo, '$fpago', 'FINALIZADO')");
		if ($saldo < 1) {
			$db->query("UPDATE carteras SET carestado = 'CANCELADO' WHERE carempresa = '$empresa' AND carfactura = '$numero'");
		}

		$rowsoli = $db->query("SELECT * FROM solicitudes where solid ='$solicitud' AND solempresa = '$empresa' ")->fetch(PDO::FETCH_ASSOC);
		$asesorsoli = $rowsoli['solasesor'];
		$empresasoli = $rowsoli['solempresa'];

		$rowsoli = $db->query("SELECT * FROM usuarios WHERE usuid = '$asesorsoli'")->fetch(PDO::FETCH_ASSOC);
		$qry = $db->query("UPDATE solicitudes SET solcomision = '" . $rowsoli['usucomision'] . "', solcomision1 = '" . $rowsoli['usucomision1'] . "', solcomision2 = '" . $rowsoli['usucomision2'] . "', solcomision3 = '" . $rowsoli['usucomision3'] . "', solover1 = '" . $rowsoli['usuover1'] . "', solpover1 = '" . $rowsoli['usupover1'] . "', solover2 = '" . $rowsoli['usuover2'] . "', solpover2 = '" . $rowsoli['usupover2'] . "', solover3 = '" . $rowsoli['usuover3'] . "', solpover3 = '" . $rowsoli['usupover3'] . "', solover4 = '" . $rowsoli['usuover4'] . "', solpover4 = '" . $rowsoli['usupover4'] . "' WHERE solempresa = '$empresasoli' AND solid = '$solicitud'");

		
		//Log de informacion que se le envia a American Logistic 
		$qrylogsregister = $db->query("SELECT * FROM usuarios INNER JOIN perfiles ON usuperfil = perid WHERE usuid = '".$_SESSION['id']."'"); //verificacion usuario por ID de sesion
		$rowlogsregister = $qrylogsregister->fetch(PDO::FETCH_ASSOC);	
		$qrylogsregister = $db->query("INSERT INTO logs ( usuario, id_usuario , ip_usuario , accion_usuario, fecha_accion) VALUES ( '".$rowlogsregister['usunombre']."', '".$_SESSION['id']."', '".$_SERVER['REMOTE_ADDR']."' , ' SE GENERO RECIBO DE CAJA NUMERO  ".$recibo." - ".$empresa." A LA cuenta/FACTURA ".$row['movdocumento']." A NOMBRE DEL CLIENTE ".$row['clinombre']." ".$row['clinom2']." ".$row['cliape1']." ".$row['cliape2']." POR UN VALOR DE ".$row['movvalor']." QUEDANDO UN SALDO DE ".$row['movsaldo']."  ' , '".date("Y-m-d H:i:s")."' );"); //anexo de informacion de accion a la BD tabla LOGS

		header("location:finalizar.php?".$filtro."&recibo=".$recibo."&empresa=".$empresa.'&activas='.$num);
		exit();
	}

	if(isset($_GET['recibo']))
	{
		$recibo = $_GET['recibo'];
		$empresa = $_GET['empresa'];
		$activas = $_GET['activas'];
	}


?>
<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Cartera</a>
				<div class="mapa_div"></div><a class="current">Pagos</a>
			</article>
			<article id="contenido">
				<p align="center">
					<iframe src="pdf.php?id1=<?php echo $empresa . '&id2=' . $recibo . "&activas=" . $activas ?>" width="600" height="830"></iframe>
				</p>
				<p class="boton">
					<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href = 'listar.php?<?php echo $filtro ?>'">atras</button>
				</p>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
</body>

</html>