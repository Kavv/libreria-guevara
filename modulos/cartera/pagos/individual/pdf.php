<?php
$r = '../../../../';
require($r.'incluir/connection.php');
require($r.'incluir/fpdf/fpdf.php');
$empresa = $_GET['id1'];
$recibo = $_GET['id2'];
$ex_activas = $_GET['activas'];
$row = $db->query("SELECT * FROM ((movimientos INNER JOIN empresas ON movempresa = empid) INNER JOIN clientes ON movtercero = cliid) INNER JOIN fpagos ON movfpago = fpaid WHERE movempresa = '$empresa' AND movprefijo = 'RC' AND movnumero = '$recibo'")->fetch(PDO::FETCH_ASSOC);
$activas = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '$empresa' AND dcafactura = '".$row['movdocumento']."' AND dcaestado = 'ACTIVA'")->rowCount();

$c_pagadas = $ex_activas - $activas;

$pdf = new FPDF('P','mm',array(140,110));
$pdf->AddFont('LucidaConsole','','lucon.php');
$pdf->AddPage();
$pdf->SetFont('LucidaConsole','',8);
$pdf->Ln(10);
$pdf->Cell(0,5,'RECIBO DE CAJA NO:'.$recibo,0,1,'C');
$pdf->Ln(5);
$pdf->Cell(0,5,'Fecha: '.$row['movfecha'],0,1);
$pdf->Cell(0,5,'Empresa: '.substr($row['empnombre'],0,42),0,1);
$pdf->Cell(0,5,'Cliente: '.substr($row['clinombre'].' '.$row['clinom2'].' '.$row['cliape1'].' '.$row['cliape2'],0,40),0,1);
$pdf->Cell(0,5,'Factura #: '.$row['movdocumento'],0,1);
$saldo = $row['movsaldo'] + $row['movvalor'] + $row['movdescuento'];
$pdf->Cell(0,5,'Saldo anterior: '.number_format($saldo,2),0,1);
$pdf->Cell(0,5,'Valor a pagar: '.number_format($row['movvalor'],2),0,1);
$pdf->Cell(0,5,'Descuento: '.number_format($row['movdescuento'],2),0,1);
$pdf->Cell(0,5,'Saldo actual: '.number_format($row['movsaldo'],2),0,1);
$row = $db->query("SELECT * FROM fpagos WHERE fpaid = '".$row['movfpago']."'")->fetch(PDO::FETCH_ASSOC);
$pdf->Cell(0,5,'Forma de pago: '.$row['fpanombre'],0,1);
$pdf->Cell(0,5,'Cubre: '.$c_pagadas. ' cuota/s',0,1);
$pdf->Ln(10);
$pdf->SetFont('LucidaConsole','',6);
//$pdf->MultiCell(0,3,"NOTA: En caso de que una o varias cuotas del credito,aqui mencionado hayan sido canceladas con cheque o con tarjeta de credito, este recibo solo tendra validez una vez sean confirmados los documentos mencionados.");
$pdf->Output();


?>