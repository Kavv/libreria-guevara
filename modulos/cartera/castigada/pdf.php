<?php
	$r = '../../../';
	require($r . 'incluir/session.php');
	require($r . 'incluir/connection.php');
	include($r.'incluir/fpdf/fpdf.php');
$nit = $_GET["nit"];
$numero = $_GET["numero"];
$cedula = $_GET["cedula"];
$fecha1 = $_GET["fecha1"];
$fecha2 = $_GET["fecha2"];


$con = "SELECT * FROM (((carteras INNER JOIN empresas ON carempresa = empid) INNER JOIN clientes ON carcliente = cliid) INNER JOIN solicitudes ON (solempresa = carempresa AND solfactura = carfactura)) LEFT JOIN usuarios ON usuid = carpromotor";
$ord = " ORDER BY carfecha";
/* Los parametros de la consulta sql se genera dinamicamente 
	en base a los datos recibidos para delimitar los resultados */
$parameters = [];
array_push($parameters, "(carestado = 'CASTIGADA' OR carestado = 'RECUPERADA')");
if ($nit != "")
	array_push($parameters, "carempresa LIKE '%$nit%'");
if ($numero != "")
	array_push($parameters, "carfactura LIKE '%$numero%'");
if ($cedula != "")
	array_push($parameters, "carcliente LIKE '%$cedula%'");
if ($fecha1 != "")
	array_push($parameters, "carfcastigo BETWEEN '" . $fecha1 . "' AND '" . $fecha2 . "'");


// Consulta base
$sql = $con;
foreach ($parameters as $index => $parameter) {
	// Se agregan los parametros del WHERE
	if ($index == 0)
		$sql .= " WHERE " . $parameter;
	else
		$sql .= " AND " . $parameter;
}
// Se completa la consulta
$sql .= $ord;

$qry = $db->query($sql);

class PDF extends FPDF{
	function Header(){
		global $fecha1;
		global $fecha2;
    	$this->SetFont('LucidaConsole','',8);
		$this->Cell(0,5,date('Y/m/d H:i:s'),0,1);
		$this->SetFont('LucidaConsole','',11);
		$this->Cell(0,5,'------------------------------------------------------------------------------------------------------------------------------------------------',0,1,'C');
		if($fecha1 != "") $this->Cell(0,5,'LISTADO DE CARTERA CASTIGADA DEL '.$fecha1.' AL '.$fecha2,0,1,'C');
    	else $this->Cell(0,5,'LISTADO DE CARTERA CASTIGADA',0,1,'C');
		$this->Cell(0,5,'------------------------------------------------------------------------------------------------------------------------------------------------',0,1,'C');
    	$this->Ln(5);
	}
	function Footer(){
    	$this->SetY(-15);
    	$this->SetFont('LucidaConsole','',11);
		$this->Cell(0,5,'------------------------------------------------------------------------------------------------------------------------------------------------',0,1,'C');
		$this->SetFont('LucidaConsole','',8);
    	$this->Cell(0,5,'Pagina '.$this->PageNo().'/{nb}',0,0,'R');
	}
}

$pdf = new PDF('L','mm','Legal');
$pdf->AddFont('LucidaConsole','','lucon.php');
$pdf->AliasNbPages();
$pdf->AddPage();

$pdf->SetFont('LucidaConsole','',10);
$pdf->SetFillColor(220,220,220);
//$pdf->setX(35);
$pdf->Cell(50,5,'EMPRESA',1,0,'C',true);
$pdf->Cell(25,5,'SOLICITUD',1,0,'C',true);
$pdf->Cell(25,5,'FACTURA',1,0,'C',true);
$pdf->Cell(25,5,'FECHA',1,0,'C',true);
$pdf->Cell(60,5,'CLIENTE',1,0,'C',true);
$pdf->Cell(30,5,'TOTAL CREDITO',1,0,'C',true);
$pdf->Cell(30,5,'SALDO',1,0,'C',true);
$pdf->Cell(25,5,'ESTADO',1,0,'C',true);
$pdf->Cell(25,5,'F.CASTIGO',1,0,'C',true);
$pdf->Cell(25,5,'F.RECUPE',1,1,'C',true);
$i = 1;
$pdf->SetFont('LucidaConsole','',8);
while($row = $qry->fetch(PDO::FETCH_ASSOC)){

	$row2 = $db->query(
		"SELECT movfecha FROM movimientos WHERE 
		movprefijo='NR' AND movempresa='".$row['carempresa']."' AND movdocumento = '".$row['carfactura']."'
		ORDER BY movfecha DESC LIMIT 1")->fetch(PDO::FETCH_ASSOC);
	if(!$row2)
		$row2["movfecha"] = "";

	if($i%2 == 0) $x = 235;
	else $x = 250;
	$pdf->SetFillColor($x,$x,$x);
	//$pdf->setX(35);
	$pdf->Cell(50,5,$row["empnombre"],1,0,'',true);
	$pdf->Cell(25,5,$row["solid"],1,0,'C',true);
	$pdf->Cell(25,5,$row["carfactura"],1,0,'C',true);
	$pdf->Cell(25,5,$row["carfecha"],1,0,'C',true);
	$pdf->Cell(60,5,$row["clinombre"].' '.$row["clinom2"].' '.$row["cliape1"].' '.$row["cliape2"],1,0,'',true);
	$pdf->Cell(30,5,number_format($row["cartotal"],2),1,0,'R',true);
	$pdf->Cell(30,5,number_format($row["carsaldo"],2),1,0,'R',true);
	$pdf->Cell(25,5,$row["carestado"],1,0,'C',true);
	$pdf->Cell(25,5,$row["carfcastigo"],1,0,'C',true);
	$pdf->Cell(25,5,$row2["movfecha"],1,1,'C',true);
	$i++;
}

$pdf->Output("facturas.pdf","d");
?>