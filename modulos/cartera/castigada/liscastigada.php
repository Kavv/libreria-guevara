<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

$nit = $numero = $cedula = $fecha1 = $fecha2 = "";
if (isset($_POST["consultar"])) {
	$nit = $_POST["nit"];
	$numero = $_POST["numero"];
	$cedula = $_POST["cedula"];
	$fecha1 = $_POST["fecha1"];
	$fecha2 = $_POST["fecha2"];
} elseif (isset($_GET["nit"])) {
	$nit = $_GET["nit"];
	$numero = $_GET["numero"];
	$cedula = $_GET["cedula"];
	$fecha1 = $_GET["fecha1"];
	$fecha2 = $_GET["fecha2"];
}
$filtro = "nit=$nit&numero=$numero&cedula=$cedula&fecha1=$fecha1&fecha2=$fecha2";

$con = "SELECT * FROM (((carteras INNER JOIN empresas ON carempresa = empid) INNER JOIN clientes ON carcliente = cliid) INNER JOIN solicitudes ON (solempresa = carempresa AND solfactura = carfactura)) LEFT JOIN usuarios ON usuid = carpromotor";
$ord = " ORDER BY carfecha";
/* Los parametros de la consulta sql se genera dinamicamente 
	en base a los datos recibidos para delimitar los resultados */
$parameters = [];
array_push($parameters, "(carestado = 'CASTIGADA' OR carestado = 'RECUPERADA')");
if ($nit != "")
	array_push($parameters, "carempresa LIKE '%$nit%'");
if ($numero != "")
	array_push($parameters, "carfactura LIKE '%$numero%'");
if ($cedula != "")
	array_push($parameters, "carcliente LIKE '%$cedula%'");
if ($fecha1 != "")
	array_push($parameters, "carfcastigo BETWEEN '" . $fecha1 . "' AND '" . $fecha2 . "'");


// Consulta base
$sql = $con;
foreach ($parameters as $index => $parameter) {
	// Se agregan los parametros del WHERE
	if ($index == 0)
		$sql .= " WHERE " . $parameter;
	else
		$sql .= " AND " . $parameter;
}
// Se completa la consulta
$sql .= $ord;


$qry = $db->query($sql);
?>
<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
	<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>

	<script type="text/javascript">
		$(document).ready(function() {
			$("#tabla").dataTable({
				"bStateSave": true,
				"bJQueryUI": true,
				// "aaSorting": [ [0,'desc'], [1,'desc'] ],
				"sPaginationType": "full_numbers",
				"oLanguage": {
					'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
				},
				"aoColumnDefs": [{
					"bSortable": false,
					"aTargets": [3, 5, 6, 8, 9]
				}]
			});
		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="<?php echo $r ?>index.php">Principal</a>
				<div class="mapa_div"></div><a href="#">Cartera</a>
				<div class="mapa_div"></div><a class="current">Cartera castigada</a>
			</article>
			<article id="contenido">
				<h2>Listado cartera castigada y recuperada <?php if ($fecha1 != '') echo ' del ' . $fecha1 . ' al ' . $fecha2 ?></h2>
				<div class="reporte">
					<a href="pdf.php?<?php echo $filtro ?>">
						<img src="<?php echo $r ?>imagenes/iconos/pdf.png" title="pdf" />
					</a> 
				</div>
				<table id="tabla">
					<thead>
						<tr>
							<th>EMPRESA</th>
							<th>SOLICITUD</th>
							<th>FACTURA</th>
							<th>FECHA</th>
							<th>CLIENTE</th>
							<th>T/CREDITO</th>
							<th>SALDO</th>
							<th>ESTADO</th>
							<th title="Fecha Castigada">F.CAS.</th>
							<th title="Fecha Recuperada">F.REC.</th>
						</tr>
					</thead>
					<tbody>
						<?php
						while ($rowfac = $qry->fetch(PDO::FETCH_ASSOC)) {
							$row2 = $db->query(
								"SELECT movfecha FROM movimientos WHERE 
								movprefijo='NR' AND movempresa='".$rowfac['carempresa']."' AND movdocumento = '".$rowfac['carfactura']."'
								ORDER BY movfecha DESC LIMIT 1")->fetch(PDO::FETCH_ASSOC);
							if(!$row2)
								$row2["movfecha"] = "";
						?>
							<tr>
								<td title="<?php echo $rowfac["carempresa"] ?>"><?php echo $rowfac["empnombre"] ?></td>
								<td align="center"><?php echo $rowfac["solid"] ?></td>
								<td align="center"><?php echo $rowfac["carfactura"] ?></td>
								<td align="center"><img src="<?php echo $r ?>imagenes/iconos/date.png" title="<?php echo $rowfac["carfecha"] ?>" /></td>
								<td title="<?php echo $rowfac["carcliente"] ?>"><?php echo $rowfac["clinombre"] . ' ' . $rowfac["clinom2"] . ' ' . $rowfac["cliape1"] . ' ' . $rowfac["cliape2"] ?></td>
								<td align="right"><?php echo number_format($rowfac["cartotal"], 2) ?></td>
								<td align="right"><?php echo number_format($rowfac["carsaldo"], 2) ?></td>
								<td><?php echo $rowfac["carestado"] ?></td>
								<td align="center"><img src="<?php echo $r ?>imagenes/iconos/date.png" title="<?php echo $rowfac["carfcastigo"] ?>" /></td>
								<td align="center"><img src="<?php echo $r ?>imagenes/iconos/date.png" title="<?php echo $row2["movfecha"] ?>" /></td>
							</tr>
						<?php
						}
						?>
					</tbody>
				</table>
				<p class="boton">
					<button type="button" class="btn btn-primary btnatras" onClick="location.href = 'concastigada.php'" style="margin-top:5px" onclick="Carga()">atras</button>
				</p>
			</article>
		</article>
	</section>
</body>

</html>