<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
?>
<!doctype html>
<html>

<head>

	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="<?php echo $r ?>index.php">Principal</a>
				<div class="mapa_div"></div><a href="#">Cartera</a>
				<div class="mapa_div"></div><a class="current">Castigada</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="lisrecuperar.php" method="post">
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">Consultar cartera a recuperar</legend>
						<p>
							<label for="nit">Empresa:</label>
							<select name="nit">
								<option value="">TODAS</option>
								<?php
								$qry = $db->query("SELECT * FROM empresas");
								while ($row = $qry->fetch(PDO::FETCH_ASSOC))
									echo '<option value="' . $row['empid'] . '">' . $row['empnombre'] . '</option>';
								?>
							</select>
						</p>
						<p>
							<label for="numero">Numero de factura:</label>
							<input type="text" name="numero" class="consecutivo" />
						</p>
						<p>
							<label for="cedula">Cedula del cliente:</label>
							<input type="text" name="cedula" class="cedula" /></td>
						</p>
						<p class="boton">
							<button type="submit" class="btn btn-primary btnconsulta" name="consultar" value="consultar" onclick="Carga()">consultar</button>
						</p>
					</fieldset>
				</form>
			</article>
		</article>
	</section>
	<?php
	if (isset($_GET["error"]))
		echo "<div id='dialog-message' title='Error'><span class='ui-icon ui-icon-circle-close' style='float:left; margin:3px 7px 7px 0;'></span>" . $_GET["error"] . "</div>";
	elseif (isset($_GET["mensaje"]))
		echo "<div id='dialog-message' title='Correcto'><span class='ui-icon ui-icon-circle-check' style='float:left; margin:3px 7px 7px 0;'></span>" . $_GET["mensaje"] . "</div>";
	?>
</body>

</html>