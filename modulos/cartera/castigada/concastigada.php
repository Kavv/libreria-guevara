<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
?>
<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<script type="text/javascript" src="<?php echo $r ?>jquery/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$("#fecha1").datepicker({
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
				maxDate: "+0D",
				onClose: function(selectedDate) {
					$("#fecha2").datepicker("option", "minDate", selectedDate);
				}
			});
			$("#fecha2").datepicker({
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
				maxDate: "+0D",
				onClose: function(selectedDate) {
					$("#fecha1").datepicker("option", "maxDate", selectedDate);
				}
			});
		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="<?php echo $r ?>index.php">Principal</a>
				<div class="mapa_div"></div><a class="current">Cartera</a>
				<div class="mapa_div"></div><a class="current">Cartera castigada</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="liscastigada.php" method="post">
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">Consulta de cartera castigada y recuperada</legend>
						<p>
							<label for="nit">Empresa:</label>
							<select name="nit">
								<option value="">TODAS</option>
								<?php
								$qry = $db->query("SELECT * FROM empresas");
								while ($row = $qry->fetch(PDO::FETCH_ASSOC))
									echo '<option value="' . $row['empid'] . '">' . $row['empnombre'] . '</option>';
								?>
							</select>
						</p>
						<p>
							<label for="numero">Numero de factura:</label>
							<input type="text" name="numero" class="" />
						</p>
						<p>
							<label for="cedula">Cedula del cliente:</label>
							<input type="text" name="cedula" class="nit" /></td>
						</p>
						<p>
							<label for="fechas">Intervalo de fechas:</label>
							<input type="text" class="fecha" id="fecha1" name="fecha1" /> 
							- 
							<input type="text" id="fecha2" class="fecha" name="fecha2" />
						</p>
						<p class="boton">
							<button type="submit" class="btn btn-primary btnconsulta" name="consultar" value="consultar" onclick="Carga()">consultar</button>
						</p>
					</fieldset>
				</form>
			</article>
		</article>
	</section>
	<?php
	if (isset($_GET["error"]))
		echo "<div id='dialog-message' title='Error'><span class='ui-icon ui-icon-circle-close' style='float:left; margin:3px 7px 7px 0;'></span>" . $_GET["error"] . "</div>";
	elseif (isset($_GET["mensaje"]))
		echo "<div id='dialog-message' title='Correcto'><span class='ui-icon ui-icon-circle-check' style='float:left; margin:3px 7px 7px 0;'></span>" . $_GET["mensaje"] . "</div>";
	?>

</body>

</html>