<?php
	$r = '../../../';
	require($r . 'incluir/session.php');
	require($r . 'incluir/connection.php');

	$nit = $numero = $cedula = "";

	if (isset($_POST["consultar"])) {
		$nit = $_POST["nit"];
		$numero = $_POST["numero"];
		$cedula = $_POST["cedula"];
	} elseif (isset($_GET["nit"])) {
		$nit = $_GET["nit"];
		$numero = $_GET["numero"];
		$cedula = $_GET["cedula"];
	}
	$filtro = "nit=$nit&numero=$numero&cedula=$cedula";

	$con = "SELECT * FROM (((carteras INNER JOIN empresas ON carempresa = empid) INNER JOIN clientes ON carcliente = cliid) INNER JOIN solicitudes ON (solempresa = carempresa AND solfactura = carfactura)) LEFT JOIN usuarios ON usuid = carpromotor";
	$ord = " ORDER BY carfecha";
	/* Los parametros de la consulta sql se genera dinamicamente 
		en base a los datos recibidos para delimitar los resultados */
	$parameters = [];
	array_push($parameters, "carestado = 'ACTIVA'");
	if ($nit != "")
		array_push($parameters, "carempresa LIKE '%$nit%'");
	if ($numero != "")
		array_push($parameters, "carfactura LIKE '%$numero%'");
	if ($cedula != "")
		array_push($parameters, "carcliente LIKE '%$cedula%'");


	// Consulta base
	$sql = $con;
	foreach ($parameters as $index => $parameter) {
		// Se agregan los parametros del WHERE
		if ($index == 0)
			$sql .= " WHERE " . $parameter;
		else
			$sql .= " AND " . $parameter;
	}
	// Se completa la consulta
	$sql .= $ord;

	$qry = $db->query($sql);
?>
<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
	<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
	
	<script type="text/javascript">
		$(document).ready(function() {
			$("#tabla").dataTable({
				"bStateSave": true,
				"bJQueryUI": true,
				// "aaSorting": [ [0,'desc'], [1,'desc'] ],
				"sPaginationType": "full_numbers",
				"oLanguage": {
                    'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
				},
				"aoColumnDefs": [{
					"bSortable": false,
					"aTargets": [7]
				}]
			});
		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="<?php echo $r ?>index.php">Principal</a>
				<div class="mapa_div"></div><a href="#">Cartera</a>
				<div class="mapa_div"></div><a class="current">Castigada</a>
			</article>
			<article id="contenido">
				<h2>Listado de cartera a castigar</h2>
				<table id="tabla">
					<thead>
						<tr>
							<th>EMPRESA</th>
							<th>SOLICITUD</th>
							<th>FACTURA</th>
							<th>FECHA</th>
							<th>CLIENTE</th>
							<th>TOTAL CREDITO</th>
							<th>SALDO</th>
							<th>ESTADO</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php
						while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
						?>
							<tr>
								<td title="<?php echo $row["carempresa"] ?>"><?php echo $row["empnombre"] ?></td>
								<td align="center"><?php echo $row["solid"] ?></td>
								<td align="center"><?php echo $row["carfactura"] ?></td>
								<td align="center"><img src="<?php echo $r ?>imagenes/iconos/date.png" title="<?php echo $row["carfecha"] ?>" /></td>
								<td title="<?php echo $row["carcliente"] ?>"><?php echo $row["clinombre"] . ' ' . $row["clinom2"] . ' ' . $row["cliape1"] . ' ' . $row["cliape2"] ?></td>
								<td align="right"><?php echo number_format($row["cartotal"], 2) ?></td>
								<td align="right"><?php echo number_format($row["carsaldo"], 2) ?></td>
								<td align="center"><?php echo $row["carestado"] ?></td>
								<td class="text-center">
									<a href="castigar.php?empresa=<?php echo $row["carempresa"] ?>&factura=<?php echo $row["carfactura"] ?>&<?php echo $filtro;?>" title="Castigar">
										<img src="<?php echo $r ?>imagenes/iconos/punish.png"/>
									</a>
								</td>
							</tr>
						<?php
						}
						?>
					</tbody>
				</table>
				<p class="boton">
					<button type="button" class="btn btn-primary btnatras" onClick="location.href = 'consultar.php'" onclick="Carga()">atras</button>
				</p>
			</article>
		</article>
	</section>
	<?php
	if (isset($_GET["error"]))
		echo "<div id='dialog-message' title='Error'><span class='ui-icon ui-icon-circle-close' style='float:left; margin:3px 7px 7px 0;'></span>" . $_GET["error"] . "</div>";
	elseif (isset($_GET["mensaje"]))
		echo "<div id='dialog-message' title='Correcto'><span class='ui-icon ui-icon-circle-check' style='float:left; margin:3px 7px 7px 0;'></span>" . $_GET["mensaje"] . "</div>";
	?>
</body>

</html>