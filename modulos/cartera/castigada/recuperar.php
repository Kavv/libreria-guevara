<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

$filtro = "nit=" . $_GET['nit'] . "&numero=" . $_GET['numero'] . "&cedula=" . $_GET['cedula'];

if (isset($_POST['recuperar'])) {
	$nit = $_POST["nit"];
	$documento = $_POST["numero"];
	$fecha = $_POST["fecha"];
	$cliente = $_POST['cliente'];
	$saldo = $_POST['saldo'];
	$valor = $_POST['valor'];
	if ($saldo > $valor) {
		$nsaldo = $saldo - $valor;
		$db->query("UPDATE carteras SET carsaldo = $nsaldo WHERE carempresa = '$nit' AND carfactura = '$documento'");
		$rmax = $db->query("SELECT MAX(movnumero)+1 AS ultimo FROM movimientos WHERE movempresa = '$nit' AND movprefijo = 'NR'")->fetch(PDO::FETCH_ASSOC);
		if ($rmax["ultimo"] == "")
			$rmax["ultimo"] = 1;
		$numero = $rmax["ultimo"];
		$db->query("INSERT INTO movimientos(movempresa, movprefijo, movnumero, movtercero, movdocumento, movfecha, movvalor, movsaldo, movestado) VALUES ('$nit', 'NR', '$numero', '$cliente', '$documento', '$fecha', $valor, $nsaldo, 'FINALIZADO')");
		echo $mensaje = "Se abono al valor del castigo correctamente.";
		header("Location:lisrecuperar.php?mensaje=$mensaje"."&$filtro");
		exit();
	} elseif ($saldo == $valor) {
		$db->query("UPDATE carteras SET carestado = 'RECUPERADA', carsaldo = 0 WHERE carempresa = '$nit' AND carfactura = '$documento'");
		$rmax = $db->query("SELECT MAX(movnumero)+1 AS ultimo FROM movimientos WHERE movempresa = '$nit' AND movprefijo = 'NR'")->fetch(PDO::FETCH_ASSOC);
		if ($rmax["ultimo"] == "")
			$rmax["ultimo"] = 1;
		$numero = $rmax["ultimo"];
		$db->query("INSERT INTO movimientos(movempresa, movprefijo, movnumero, movtercero, movdocumento, movfecha, movvalor, movsaldo, movestado) VALUES ('$nit', 'NR', '$numero', '$cliente', '$documento', '$fecha', '$valor', 0, 'FINALIZADO')");
		echo $mensaje = "Se recupero cartera correctamente.";
		header("Location:lisrecuperar.php?mensaje=$mensaje"."&$filtro");
		exit();
	} else {
		echo $error = "El pago no puede ser mayor al saldo.";
		header("Location:recuperar.php?error=$error"."&empresa=$nit"."&factura=$numero"."&$filtro");
		exit();
	}
} elseif (isset($_GET["empresa"])) {
	$nit = $_GET["empresa"];
	$numero = $_GET["factura"];
}
$qry = $db->query("SELECT * FROM (carteras INNER JOIN empresas ON carempresa = empid) INNER JOIN clientes ON carcliente = cliid WHERE carempresa = '$nit' AND carfactura = '$numero'");
$row = $qry->fetch(PDO::FETCH_ASSOC)
?>
<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<script type="text/javascript">
		$(document).ready(function() {
			$("#fecha").datepicker({
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
				maxDate: "+0D",
			}).keypress(function(event) {
				event.preventDefault()
			});
		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="<?php echo $r ?>index.php">Principal</a>
				<div class="mapa_div"></div><a href="#">Cartera</a>
				<div class="mapa_div"></div><a href="#">Castigada</a>
				<div class="mapa_div"></div><a class="current">Castigar</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="recuperar.php?<? echo $filtro ?>" method="post">
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">Recuperar cartera</legend>
						<p>
							<label for="empresa">Empresa:</label>
							<input type="hidden" name="nit" value="<?php echo $row['carempresa'] ?>" />
							<?php echo $row['empnombre'] ?>
						</p>
						<p>
							<label for="cliente">Cliente:</label>
							<input type="hidden" name="cliente" value="<?php echo $row['cliid'] ?>" />
							<?php echo $row['clinombre'] . ' ' . $row['clinom2'] . ' ' . $row['cliape1'] . ' ' . $row['cliape2'] ?>
						</p>
						<p>
							<label for="numero">Factura:</label>
							<input type="text" name="numero" class="solicitud" value="<?php echo $row['carfactura'] ?>" readonly />
						</p>
						<p>
							<label for="numero">Valor castigado:</label>
							<input type="text" name="saldo" class="numero" value="<?php echo $row['carsaldo'] ?>" readonly />
						</p>
						<p>
							<label for="numero">Valor a pagar:</label>
							<input type="text" name="valor" class="numero" onKeyUp="miles(this)" required />
						</p>
						<p>
							<label for="fechas">Fecha de recuperación:</label>
							<input type="text" name="fecha" id="fecha" class="fecha" required />
						</p>
						<p align="center" class="mt-3">
							<button type="button" class="btn btn-primary btnatras" onClick="location.href = 'lisrecuperar.php?<?php echo $filtro ?>'" onclick="Carga()">Atras</button>
							<button class="btn btn-primary btnfinalizar" type="submit" name="recuperar" value="recuperar" onclick="Carga()">Recuperar</button>
						</p>
					</fieldset>
				</form>
			</article>
		</article>
	</section>
	<?php
	if (isset($_GET["error"]))
		echo "<div id='dialog-message' title='Error'><span class='ui-icon ui-icon-circle-close' style='float:left; margin:3px 7px 7px 0;'></span>" . $_GET["error"] . "</div>";
	?>
</body>

</html>