<?php
$r = "../../";
include($r."include/session.class.php");
include($r."include/mysql.class.php");
$session = new Session;
$mysql = new MySql;
$session->iniciar($r);
$mysql->conectar();
$mysql->base();
$rowcre = mysql_fetch_assoc($db->query("SELECT * FROM ((((carteras INNER JOIN empresas ON carempresa = empid) INNER JOIN clientes ON carcliente = cliid) INNER JOIN solicitudes ON (carempresa = solempresa AND carfactura = solfactura)) INNER JOIN departamentos ON depCodigo = soldepcobro) INNER JOIN ciudades ON (soldepcobro = ciudepto AND solciucobro = ciuCodigo) WHERE carempresa = '".$_GET["nit"]."' AND carfactura = '".$_GET["numero"]."'"));
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>.:: IdenCorp ::.</title>
<link type="text/css" rel="stylesheet" href="<?php echo $r ?>estilo/estilo.css" />
<link type="text/css" rel="stylesheet" href="<?php echo $r ?>jquery/css/base/jquery-ui-1.10.2.custom.css">
<script type="text/javascript" src="<?php echo $r ?>jquery/js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="<?php echo $r ?>jquery/js/jquery-ui-1.10.2.custom.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$(".btnatras").button({ icons: { primary: "ui-icon ui-icon-arrowthick-1-w" }});
	$(".grilla:odd").css("background-color","#EAF2D3");
  	$( "#dialog-message" ).dialog({
		height: 80,
		width: 'auto',
		modal: true
	});
	$(".newWindow").click(function (event){
		var url = $(this).attr("href");
		var windowName = "popUp";
		var windowSize = "width=600,height=800";
		window.open(url, windowName, windowSize);
		event.preventDefault();
	});
});
</script>
</head>
<body>
<?php include($r.'include/encabezado.php') ?>
<div class="contenedor">
<?php include($r.'include/menu.php') ?>
<div class="mapa">
<a href="<?php echo $r ?>index.php">Principal</a><div class="mapa_div"></div><a href="#">Cartera</a><div class="mapa_div"></div><a href="consultar.php">Pagos</a><div class="mapa_div"></div><a href="#">Listado</a><div class="mapa_div"></div><a class="current">Detalle</a>
</div>
<div class="cuerpo centrado">
<table>
<thead>
<tr><th colspan="6" align="center"><h1>DETALLE cuenta</h1></th></tr>
</thead>
<tbody>
<tr><td class="alt" colspan="2">EMPRESA </td><td colspan="4"><?php echo $rowcre["empnombre"] ?></td></tr>
<tr><td class="alt" colspan="2">SOLICITUD</td><td colspan="4"><?php echo $rowcre["solid"] ?></td></tr>
<tr><td class="alt" colspan="2">FACTURA</td><td colspan="4"><?php echo $rowcre["carfactura"] ?></td></tr>
<tr><td class="alt" colspan="2">CLIENTE: </td><td colspan="4"><?php echo $rowcre["clinombre"].' '.$rowcre["clinom2"].' '.$rowcre["cliape1"].' '.$rowcre["cliape2"] ?></td></tr>
<tr><td class="alt" colspan="2">DEPARTAMENTO / CIUDAD: </td><td colspan="4"><?php echo $rowcre["depnombre"].' / '.$rowcre["ciunombre"] ?></td></tr>
<tr><td class="alt" colspan="2">DIRECCION: </td><td colspan="4"><?php echo $rowcre["solbarcobro"].' - '.$rowcre["solcobro"] ?></td></tr>
<tr><td class="alt" colspan="2">TELEFONOS: </td><td colspan="4"><?php echo $rowcre["clicelular"] ?> - <?php echo $rowcre["clitelresidencia"] ?> - <?php echo $rowcre["clitelcomercio"] ?></td></tr>
<tr><td class="alt" colspan="2">TOTAL CREDITO: </td><td colspan="4" align="right"><?php echo number_format($rowcre["cartotal"],0,',','.') ?></td></tr>
<tr><td class="alt" colspan="2">CUOTAS DE: </td><td colspan="4" align="right"><?php echo number_format($rowcre["carcuota"],0,',','.') ?></td></tr>
<tr><td class="alt" colspan="2">SALDO: </td><td colspan="4" align="right"><?php echo number_format($rowcre["carsaldo"],0,',','.') ?></td></tr>
<tr align="center"><th>NC</th><th>OBLIGACION</th><th>F. PAGO</th><th>CUOTA</th><th>DESCUENTO</th><th>SALDO</th></tr>
<?php
$qry = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '".$rowcre["carempresa"]."' AND dcafactura = '".$rowcre["carfactura"] ."' ORDER BY dcacuota");
while($row = $qry->fetch(PDO::FETCH_ASSOC)){
	echo '<tr class="grilla">';
	if($row["dcaestado"] === 'CANCELADO'){
		echo '<td align="center">'.$row["dcacuota"].'</td><td align="center">'.$row["dcafecha"].'</td><td align="center">'.$row["dcafepag"].'</td><td align="right">'.number_format($row["dcavalor"],0,',','.').'</td><td align="right">'.number_format($row["dcadescuento"],0,',','.').'</td><td align="right">'.number_format($row["dcasaldo"],0,',','.').'</td>';
	}elseif($row["dcafecha"] < date("Y-m-d")){
		echo '<td align="center"><a href="cuota.php?nit='.$_GET["nit"].'&numero='.$_GET["numero"].'&cuota='.$row["dcacuota"].'">'.$row["dcacuota"].'</a></td><td align="center" style="background-color:#F00">'.$row["dcafecha"].'</td><td align="center" style="background-color:#F00">'.$row["dcafepag"].'</td><td align="right" style="background-color:#F00">'.number_format($row["dcavalor"],0,',','.').'</td><td align="right" style="background-color:#F00">'.number_format($row["dcadescuento"],0,',','.').'</td><td align="right" style="background-color:#F00">'.number_format($row["dcasaldo"],0,',','.').'</td>';
	}else{
		echo '<td align="center"><a href="cuota.php?nit='.$_GET["nit"].'&numero='.$_GET["numero"].'&cuota='.$row["dcacuota"].'">'.$row["dcacuota"].'</a></td><td align="center">'.$row["dcafecha"].'</td><td align="center">'.$row["dcafepag"].'</td><td align="right">'.number_format($row["dcavalor"],0,',','.').'</td><td align="right">'.number_format($row["dcadescuento"],0,',','.').'</td><td align="right">'.number_format($row["dcasaldo"],0,',','.').'</td>';
	}
	echo '</tr>';
}
?>
</tbody>
</table>
<button type="button" class="btnatras" onClick="location.href = 'consultar.php'" style="margin-top:5px">atras</button>
<?php
if($_GET["error"])
	echo "<div id='dialog-message' title='Error'><span class='ui-icon ui-icon-circle-close' style='float:left; margin:3px 7px 7px 0;'></span>".$_GET["error"]."</div>";
elseif($_GET["mensaje"])
	echo "<div id='dialog-message' title='Correcto'><span class='ui-icon ui-icon-circle-check' style='float:left; margin:3px 7px 7px 0;'></span>".$_GET["mensaje"]."</div>";
?>
</div>
</div>
</body>
</html>