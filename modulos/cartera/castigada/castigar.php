<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

$filtro = "nit=" . $_GET['nit'] . "&numero=" . $_GET['numero'] . "&cedula=" . $_GET['cedula'];

if (isset($_POST["castigar"])) {
	$nit = $_POST["nit"];
	$numero = $_POST["numero"];
	$fecha = $_POST["fecha"];
	$db->query("UPDATE carteras SET carestado = 'CASTIGADA', carfcastigo = '" . $fecha . "' WHERE carempresa = '" . $nit . "' AND carfactura = '" . $numero . "'");
	$mensaje = "Se castigo la cuenta correctamente.";
	header("Location:listar.php?$filtro"."&mensaje=$mensaje");
	exit();
} elseif (isset($_GET["empresa"])) {
	$nit = $_GET["empresa"];
	$numero = $_GET["factura"];
}

$qry = $db->query("SELECT * FROM (carteras INNER JOIN empresas ON carempresa = empid) INNER JOIN clientes ON carcliente = cliid WHERE carempresa = '" . $nit . "' AND carfactura = '" . $numero . "'");
$row = $qry->fetch(PDO::FETCH_ASSOC)
?>
<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<script type="text/javascript" src="<?php echo $r ?>jquery/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$("#fecha").datepicker({
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
				maxDate: "+0D",
			});
		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="<?php echo $r ?>index.php">Principal</a>
				<div class="mapa_div"></div><a href="#">Cartera</a>
				<div class="mapa_div"></div><a href="#">Castigada</a>
				<div class="mapa_div"></div><a class="current">Castigar</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="castigar.php?<?php echo $filtro?>" method="post">
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">CASTIGAR CARTERA</legend>
						<p>
							<label for="empresa">Empresa:</label>
							<input type="hidden" name="nit" value="<?php echo $row['carempresa'] ?>" /><?php echo $row['empnombre'] ?>
						</p>
						<p>
							<label for="cliente">Cliente:</label>
							<?php echo $row['clinombre'] . ' ' . $row['clinom2'] . ' ' . $row['cliape1'] . ' ' . $row['cliape2'] ?>
						</p>
						<p>
							<label for="numero">Factura:</label>
							<input type="text" name="numero" class="solicitud" value="<?php echo $row['carfactura'] ?>" readonly />
						</p>
						<p>
							<label for="fechas">Fecha de castigo:</label>
							<input type="text" name="fecha" id="fecha" class="fecha" required />
						</p>
						<p align="center">
							<button type="button" class="btn btn-primary btnatras" onClick="location.href = 'listar.php?<?php echo $filtro;?>'" style="margin-top:5px" onclick="Carga()">Atras</button>
							<button class="btn btn-danger btnfinalizar" type="submit" name="castigar" value="castigar" style="margin-top:5px">castigar</button>
						</p>
				</form>
			</article>
		</article>
		<?php
		if (isset($_GET["error"]))
			echo "<div id='dialog-message' title='Error'><span class='ui-icon ui-icon-circle-close' style='float:left; margin:3px 7px 7px 0;'></span>" . $_GET["error"] . "</div>";
		?>
	</section>
</body>

</html>