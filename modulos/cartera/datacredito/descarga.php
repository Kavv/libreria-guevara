<?php
$r = '../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
if($_GET['archivo']){
	unlink($_GET['archivo']);
}
?>
<!doctype html>
<html>
<head>
	<?php 
		require($r . 'incluir/src/head.php');
		require($r . 'incluir/src/head-form.php');
	?>
<script type="text/javascript">
$(document).ready(function(){
	$('.btndescarga').button({ icons: { primary: 'ui-icon ui-icon-circle-arrow-s' }, text: false });
	$('.btneliminar').button({ icons: { primary: 'ui-icon ui-icon-circle-minus' }, text: false });
});
</script>
<style type="text/css">
#form fieldset{ padding:10px; display:block; width:250px; margin:20px auto }
#form legend{ font-weight: bold; margin-left:5px; padding:5px }
#form label{ display:inline-block; width:110px; text-align:right; margin:0.3em 2% 0 0 }
#form p{ margin:5px 0 }
</style>
</head>
<body>
<?php require($r.'incluir/src/login.php') ?>
<section id="principal">
<?php require($r.'incluir/src/cabeza.php') ?>
<?php require($r.'incluir/src/menu.php') ?>
<article id="cuerpo">
<article class="mapa">
<a href="#">Principal</a><div class="mapa_div"></div><a href="#">Cartera</a><div class="mapa_div"></div><a class="current">Datacredito</a>
</article>
<article id="contenido">
<?php
$path = $r.'modulos/cartera/datacredito/';
$no_mostrar = Array('','.php','.','..','.exe','.ini');
$dir_handle = opendir($path) or die('No se pudo abrir $path');
while ($file = readdir($dir_handle)) {
	$pos = strrpos($file,'.');
	$extension = substr($file, $pos);
	if(!in_array($extension, $no_mostrar)) {
		echo '<div style="padding-bottom:5px">'.$file.'&nbsp;<a href="descarga2.php?archivo='.$file.'"><button type="button" class="btndescarga">x</button></a>&nbsp;<a href="descarga.php?archivo='.$file.'"><button type="button" class="btneliminar">x</button></a></div>';
	}
}
closedir($dir_handle);
?>
</article>
</article>
<?php require($r.'incluir/src/pie.php') ?>
</section>
</body>
</html>