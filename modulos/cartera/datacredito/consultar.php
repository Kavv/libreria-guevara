<?php
$r = "../../../";
include($r."include/session.class.php");
include($r."include/mysql.class.php");
$session = new Session;
$mysql = new MySql;
$session->iniciar($r);
$mysql->conectar();
$mysql->base();
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>.:: IdenCorp ::.</title>
<link type="text/css" rel="stylesheet" href="<?php echo $r ?>estilo/estilo.css" />
<link type="text/css" rel="stylesheet" href="<?php echo $r ?>jquery/css/base/jquery-ui-1.10.2.custom.css">
<script type="text/javascript" src="<?php echo $r ?>jquery/js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="<?php echo $r ?>jquery/js/jquery-ui-1.10.2.custom.js"></script>
<script type="text/javascript" src="<?php echo $r ?>jquery/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$(".btnconsulta").button({ icons: { primary: "ui-icon ui-icon-search" }});
	$("#fecha1").datepicker({
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		maxDate: "+0D",
		onClose: function( selectedDate ) {
			$("#fecha2").datepicker( "option", "minDate", selectedDate );
		}
	}).keypress(function(event) { event.preventDefault() });
	$("#fecha2").datepicker({
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		maxDate: "+0D",
		onClose: function( selectedDate ) {
			$("#fecha1").datepicker( "option", "maxDate", selectedDate );
		}
	}).keypress(function(event) { event.preventDefault() });
	$( "#dialog-message" ).dialog({
		height: 80,
		width: 'auto',
		modal: true
	});
});
</script>
<style type="text/css">
#form form{ width: 600px }
#form fieldset{ padding:10px; display:block; width:600px; margin:20px auto }
#form legend{ font-weight: bold; margin-left:5px; padding:5px }
#form label{ display:inline-block; width:165px; text-align:right; margin:0.3em 2% 0 0 }
#form p{ margin:5px 0 }
</style>
</head>
<body>
<?php include($r.'include/encabezado.php') ?>
<div class="contenedor">
<?php include($r.'include/menu.php') ?>
<div class="mapa">
<a href="<?php echo $r ?>index.php">Principal</a><div class="mapa_div"></div><a href="#">Cartera</a><div class="mapa_div"></div><a class="current">Datacredito</a>
</div>
<div class="ui-widget">
<form id="form" name="form" action="listar.php" method="post">
<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
<legend class="ui-widget ui-widget-header ui-corner-all">Consulta de cartera en base a Datacrédito</legend>
<p>
<label for="nit">Empresa:</label>
<select name="nit">
<option value="">- TODAS -</option>
<?php
$qry = mysql_query("SELECT * FROM empresas");
while($row = mysql_fetch_assoc($qry))
	echo '<option value="'.$row['empid'].'">'.$row['empnombre'].'</option>';
?>
</select>
</p>
<p>
<label for="numero">Numero de cuenta:</label>
<input type="text" name="numero" class="consecutivo" />
</p>
<p>
<label for="cedula">Cedula del cliente:</label>
<input type="text" name="cedula" class="nit" /></td>
</p>
<p>
<label for="fechas">Intervalo de fechas:</label>
<input type="text" class="fecha" id="fecha1" name="fecha1" /> - <input type="text" id="fecha2" class="fecha" name="fecha2" />
</p>
<p class="boton">
<button type="submit" class="btnconsulta" name="consultar" value="consultar" onclick="Carga()">consultar</button>
</p>
</form>
</div>
</div>
<?php
if($_GET["error"])
	echo "<div id='dialog-message' title='Error'><span class='ui-icon ui-icon-circle-close' style='float:left; margin:3px 7px 7px 0;'></span>".$_GET["error"]."</div>";
elseif($_GET["mensaje"])
	echo "<div id='dialog-message' title='Correcto'><span class='ui-icon ui-icon-circle-check' style='float:left; margin:3px 7px 7px 0;'></span>".$_GET["mensaje"]."</div>";
?>
</body>
</html>