<?php
$r = '../../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');
?>
<!doctype html>
<html>
<head>
	<?php 
		require($r . 'incluir/src/head.php');
		require($r . 'incluir/src/head-form.php');
	?>
<script type="text/javascript">
function LastDayOfMonth(Year, Month){
	return new Date(new Date(Year, Month+1,1)-1).getDate();
}
$(document).ready(function(){
	$('#form').validationEngine({
		showOneMessage:true,
		onValidationComplete: function(form, status){
        	if(status) {               
            	carga();
            	return true;
			}
		}
	});
	$('.btngenera').button({ icons: { primary: 'ui-icon ui-icon-gear' }});
	$('.fecha').datepicker({
		dateFormat: 'yy-mm-dd',
		changeMonth: true,
		changeYear: true,
		maxDate: '+0D',
		beforeShowDay: function (date) {
        //getDate() returns the day (0-31)
        if (date.getDate() == LastDayOfMonth(date.getFullYear(),date.getMonth())) {
            return [true, ''];
        }
        return [false, ''];
    }
	});
	$('#dialog-message').dialog({
		height: 80,
		width: 'auto',
		modal: true
	});
});
</script>
<style type="text/css">
#form fieldset{ padding:10px; display:block; width:250px; margin:20px auto }
#form legend{ font-weight: bold; margin-left:5px; padding:5px }
#form label{ display:inline-block; width:110px; text-align:right; margin:0.3em 2% 0 0 }
#form p{ margin:5px 0 }
</style>
</head>
<body>
<?php require($r.'incluir/src/login.php') ?>
<section id="principal">
<?php require($r.'incluir/src/cabeza.php') ?>
<?php require($r.'incluir/src/menu.php') ?>
<article id="cuerpo">
<article class="mapa">
<a href="#">Principal</a><div class="mapa_div"></div><a href="#">Cartera</a><div class="mapa_div"></div><a class="current">Datacredito</a>
</article>
<article id="contenido">
<form id="form" name="form" class="form" action="generar1.php" method="post">
<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
<legend class="ui-widget ui-widget-header ui-corner-all">Archivo datacredito IdenCorp</legend>
<p>
<label for="fecha">Fecha de corte: </label>
<input type="text" name="fecha" class="fecha validate[required] text-input" />
</p>
<p class="boton">
<button type="submit" class="btngenera" name="consultar" value="consultar">generar</button>
</p>
</fieldset>
</form>
</article>
</article>
<?php require($r.'incluir/src/pie.php') ?>
</section>
</body>
</html>