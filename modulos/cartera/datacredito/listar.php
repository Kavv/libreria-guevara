<?php
$r = "../../";
include($r."include/session.class.php");
include($r."include/mysql.class.php");
$session = new Session;
$mysql = new MySql;
$session->iniciar($r);
$mysql->conectar();
$mysql->base();
if($_POST["consultar"]){
	$nit = $_POST["nit"];
	$numero = $_POST["numero"];
	$cedula = $_POST["cedula"];
	$fecha1 = $_POST["fecha1"];
	$fecha2 = $_POST["fecha2"];
}else{
	$nit = $_GET["nit"];
	$numero = $_GET["numero"];
	$cedula = $_GET["cedula"];
	$fecha1 = $_GET["fecha1"];
	$fecha2 = $_GET["fecha2"];
}
if($nit != "" && $numero == "") $sql = "SELECT * FROM (carteras INNER JOIN empresas ON carempresa = empid) INNER JOIN clientes ON carcliente = cliid WHERE carempresa = '$nit' AND carestado <> 'CANCELADO' AND carestado <> 'RECUPERADA' ORDER BY carfecha";
elseif($nit == "" && $numero != "") $sql = "SELECT * FROM (carteras INNER JOIN empresas ON carempresa = empid) INNER JOIN clientes ON carcliente = cliid WHERE carfactura LIKE '%$numero%' carestado <> 'CANCELADO' OR carestado <> 'RECUPERADA' ORDER BY carfecha";
elseif($nit != "" && $numero != "") $sql = "SELECT * FROM (carteras INNER JOIN empresas ON carempresa = empid) INNER JOIN clientes ON carcliente = cliid WHERE carempresa = '$nit' AND carfactura LIKE '%$numero%' carestado <> 'CANCELADO' OR carestado <> 'RECUPERADA' ORDER BY carfecha";
elseif($cedula != "") $sql = "SELECT * FROM (carteras INNER JOIN empresas ON carempresa = empid) INNER JOIN clientes ON carcliente = cliid WHERE carcliente = '$cedula' carestado <> 'CANCELADO' OR carestado <> 'RECUPERADA' ORDER BY carfecha";
elseif($fecha1 != "") $sql = "SELECT * FROM (carteras INNER JOIN empresas ON carempresa = empid) INNER JOIN clientes ON carcliente = cliid WHERE carfecha BETWEEN '$fecha1' AND '$fecha2' carestado <> 'CANCELADO' OR carestado <> 'RECUPERADA' ORDER BY carfecha";
else $sqlfac = "SELECT * FROM (carteras INNER JOIN empresas ON carempresa = empid) INNER JOIN clientes ON carcliente = cliid WHERE carestado <> 'CANCELADO' OR carestado <> 'RECUPERADA' ORDER BY carfecha";
$qry = mysql_query($sql);
$filtro = "nit=$nit&prefijo=$prefijo&numero=$numero&cedula=$cedula&fecha1=$fecha1&fecha2=$fecha2";
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>.:: IdenCorp ::.</title>
<link type="text/css" rel="stylesheet" href="<?php echo $r ?>estilo/estilo.css" />
<link type="text/css" rel="stylesheet" href="<?php echo $r ?>jquery/css/base/jquery-ui-1.10.2.custom.css">
<link type="text/css" rel="stylesheet" href="<?php echo $r ?>include/datatables/media/css/jquery.dataTables_themeroller.css" />
<script type="text/javascript" src="<?php echo $r ?>jquery/js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="<?php echo $r ?>jquery/js/jquery-ui-1.10.2.custom.js"></script>
<script type="text/javascript" src="<?php echo $r ?>include/datatables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$("#tabla").dataTable({
		"bStateSave": true,
		"bJQueryUI": true,
		// "aaSorting": [ [0,'desc'], [1,'desc'] ],
		"sPaginationType": "full_numbers",
		"oLanguage": {
			"sUrl": "../../include/datatables/datatables.es.txt",
		},
		"aoColumnDefs": [
			{ "bSortable": false, "aTargets": [ 5 ] }
    	]
	});
	$(".btnatras").button({ icons: { primary: "ui-icon ui-icon-arrowthick-1-w" }});
});
</script>
</head>
<body>
<?php include($r.'include/encabezado.php') ?>
<div class="contenedor">
<?php include($r.'include/menu.php') ?>
<div class="mapa">
<a href="<?php echo $r ?>index.php">Principal</a><div class="mapa_div"></div><a href="#">Cartera</a><div class="mapa_div"></div><a class="current">Datacredito</a>
</div>
<div id="demo" style="padding:15px">
<h2>Listado de cartera a modificar para Datacrédito</h2>
<div class="export"><a href="#"><img src="<?php echo $r ?>imagenes/pdf.png" title="pdf" /></a> <a href="#"><img src="<?php echo $r ?>imagenes/excel.png" title="csv" /></a></div>
<table id="tabla">
<thead>
<tr>
<th>EMPRESA</th>
<th>FACTURA</th>
<th>FECHA</th>
<th>CLIENTE</th>
<th>ESTADO</th>
<th></th>
</tr>
</thead>
<tbody>
<?php
while($row = mysql_fetch_assoc($qry)){
?>
<tr>
<td title="<?php echo $row["carempresa"] ?>"><?php echo $row["empnombre"] ?></td>
<td align="center"><?php echo $row["carfactura"] ?></td>
<td align="center" width="30px"><img src="<?php echo $r ?>imagenes/fecha.png" title="<?php echo $row["carfecha"] ?>" /></td>
<td title="<?php echo $row["carcliente"] ?>"><?php echo $row["clinombre"].' '.$row["clinom2"].' '.$row["cliape1"].' '.$row["cliape2"] ?></td>
<td><?php echo $row["carestado"] ?></td>
<td align="center">
<?php
if ($row["cardata"] == '0')
	echo '<input type="checkbox" value="" />';
else echo '<input type="checkbox" value="1" checked />';
?>
</td>
</tr>
<?php
}
?>
</tbody>
</table>
<p class="boton">
<button type="button" class="btnatras" onClick="location.href = 'consultar.php'">atras</button>
</p>
</div>
</div>
</body>
</html>