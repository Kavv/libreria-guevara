<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
$empresa = $_GET['id1'];
$numero = $_GET['id2'];
$filtro = 'numero=' . $_GET['numero'] . '&exacta=' . $_GET['exacta'];
$row = $db->query("SELECT * FROM (((((carteras INNER JOIN empresas ON carempresa = empid) INNER JOIN clientes ON carcliente = cliid) INNER JOIN solicitudes ON (carempresa = solempresa AND carfactura = solfactura)) INNER JOIN departamentos ON depid = soldepcobro) INNER JOIN ciudades ON (soldepcobro = ciudepto AND solciucobro = ciuid)) LEFT JOIN usuarios ON carpromotor = usuid WHERE carempresa = '$empresa' AND carfactura = '$numero'")->fetch(PDO::FETCH_ASSOC);
?>
<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>

	<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
	<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#tabla').dataTable({
				'bPaginate': false,
				'bLengthChange': false,
				'bFilter': false,
				'bSort': false,
				'bInfo': false,
				'bAutoWidth': false,
				'bJQueryUI': true
			});

		});
	</script>
	<style type="text/css">
		#detalle span {
			margin-right: 10px;
		}

		#detalle label {
			border-bottom: 1px solid #000;
		}
	</style>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Cartera</a>
				<div class="mapa_div"></div><a class="current">Fechas</a>
			</article>
			<article id="contenido">
				<h2>Detalle de cuenta</h2>
				<div id="detalle" class="row">
					<div class="col-md-6">
						<div class="row">
							<span><strong>Empresa:</strong></span> 
							<label class="not-w"><?php echo $row['empnombre'] ?><br /></label>
						</div>
						<div class="row">
							<span><strong>Solicitud:</strong></span> 
							<label class="not-w"><?php echo $row['solid'] ?><br /></label>
						</div>
						<div class="row">
							<span><strong>Factura:</strong></span> 
							<label class="not-w"><?php echo $row['carfactura'] ?><br /></label>
						</div>
						<div class="row">
							<span><strong>Cliente:</strong></span> 
							<label class="not-w"><?php echo $row['clinombre'] . ' ' . $row['clinom2'] . ' ' . $row['cliape1'] . ' ' . $row['cliape2'] ?><br /></label>
						</div>
						<div class="row">
							<span><strong>Promotor:</strong></span> 
							<label class="not-w"><?php echo $row['usunombre'] ?><br /></label>
						</div>
						<div class="row">
							<span><strong>Ubicacion:</strong></span> 
							<label class="not-w"><?php echo $row['depnombre'] . ' / ' . $row['ciunombre'] ?><br /></label>
						</div>
						
					</div>
					<div class="col-md-6">

						<div class="row">
							<span><strong>Contacto Directo:</strong></span>
							<label class="not-w"><?php echo $row['solbarcobro'] . ', ' . $row['solcobro'] ?><br /></label>
						</div>
						<div class="row">
							<span><strong>Teléfono Comercio:</strong></span>
							<label class="not-w"><?php echo $row['clicelular'] . ' / ' . $row['clitelresidencia'] . ' / ' . $row['clitelcomercio'] ?><br /></label>
						</div>
						<div class="row">
							<span><strong>Número Adicional:</strong></span>
							<label class="not-w"><?php echo number_format($row['solcuota'], 2) ?><br /></label>
						</div>
						<div class="row">
							<span><strong>Cuota inicial:</strong></span>
							<label class="not-w"><?php echo number_format($row['cartotal'], 2) ?><br /></label>
						</div>
						<div class="row">
							<span><strong>Total credito:</strong></span>
							<label class="not-w"><?php echo number_format($row['carcuota'], 2) ?><br /></label>
						</div>
						<div class="row">
							<span><strong>Valor cuota:</strong></span>
							<label class="not-w"><?php echo number_format($row['carsaldo'], 2) ?></label>
						</div>
						
					</div>
				</div>
				<br>
				<table id="tabla">
					<thead>
						<tr>
							<th>N.Cuota</th>
							<th>F. Obligacion</th>
							<th>F. Pago</th>
							<th>Cuota</th>
							<th>Descuento</th>
							<th>Saldo</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$qry = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '" . $row['carempresa'] . "' AND dcafactura = '" . $row['carfactura'] . "' ORDER BY dcacuota");
						while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
							if ($row['dcaestado'] == 'CANCELADO') {
								echo '<tr style="background-color:#e1dfdf">
								<td align="center">' . $row['dcacuota'] . '</td>
								<td align="center">' . $row['dcafecha'] . '</td>
								<td align="center">' . $row['dcafepag'] . '</td>
								<td align="right">' . number_format($row['dcavalor'], 2) . '</td>
								<td align="right">' . number_format($row['dcadescuento'], 2) . '</td>
								<td align="right">' . number_format($row['dcasaldo'], 2) . '</td></tr>';
							} else {
								if ($row['dcafecha'] < date('Y-m-d'))
									echo '<tr style="background-color:#f27070">';
								else
									echo '<tr>';
								echo '<td align="center">' . $row['dcacuota'] . '</td>
								<td align="center"><a href="fecha.php?id1=' . $empresa . '&id2=' . $numero . '&id3=' . $row['dcacuota'] . '&id4=' . $row['dcafecha'] . '&' . $filtro .'" onClick="carga()">' . $row['dcafecha'] . '</a></td>
								<td align="center">' . $row['dcafepag'] . '</td>
								<td align="right">' . number_format($row['dcavalor'], 2) . '</td>
								<td align="right">' . number_format($row['dcadescuento'], 2) . '</td>
								<td align="right">' . number_format($row['dcasaldo'], 2) . '</td></tr>';
							} 
						}
						?>
					</tbody>
				</table>
				<p class="boton">
					<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href = 'listar.php?<?php echo $filtro?>'">atras</button>
				</p>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<?php
	if (isset($_GET['error'])) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0"></span>' . $_GET['error'] . '</div>';
	elseif (isset($_GET['mensaje'])) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0"></span>' . $_GET['mensaje'] . '</div>';
	?>
</body>

</html>