<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
$empresa = $_GET['id1'];
$numero = $_GET['id2'];
$cuota = $_GET['id3'];
$fecha = $_GET['id4'];

$filtro = 'numero=' . $_GET['numero'] . '&exacta=' . $_GET['exacta'];

if (isset($_POST['cambiar'])) {
	$empresa = $_POST['empresa'];
	$numero = $_POST['numero'];
	$cuota = $_POST['cuota'];
	$fecha1 = $_POST['fecha1'];
	$fecha2 = $_POST['fecha2'];
	if ($_POST['solo'] == '1') {
		$row = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '$empresa' AND dcafactura = '$numero' AND dcacuota = $cuota + 1")->fetch(PDO::FETCH_ASSOC);
		if ($fecha2 >= $row['dcafecha']) {
			$error = 'Las fecha es mayor o igual a la proxima cuota.';
			header('Location:datos.php?id1=' . $empresa . '&id2=' . $numero . '&error=' . $error. "&" . $filtro);
			exit();
		}
		$qry = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '$empresa' AND dcafactura = '$numero' AND dcacuota = $cuota");
	} 
	else 
		$qry = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '$empresa' AND dcafactura = '$numero' AND dcacuota >= $cuota");
	
	while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
		$db->query("UPDATE detcarteras SET dcafecha = '$fecha2' WHERE dcaempresa = '$empresa' AND dcafactura = '$numero' AND dcacuota = " . $row['dcacuota']);
		$fecha2 = date('Y-m-d', strtotime($fecha2 . ' next month'));
		// LOG DE ACCIONES REALIZADAS POR EL USUARIO
		$qrylogsregister = $db->query("SELECT * FROM usuarios INNER JOIN perfiles ON usuperfil = perid WHERE usuid = '" . $_SESSION['id']."'"); // VERIFICACION USUARIO POR ID DE SESION
		$rowlogsregister = $qrylogsregister->fetch(PDO::FETCH_ASSOC);
		$qrylogsregister = $db->query("INSERT INTO logs ( usuario, id_usuario , ip_usuario , accion_usuario, fecha_accion) VALUES ( '" . $rowlogsregister['usunombre'] . "', '" . $_SESSION['id'] . "', '" . $_SERVER['REMOTE_ADDR'] . "' , 'SE REALIZO UN CAMBIO DE FECHA DE PAGO EN LA cuenta  NUMERO " . $numero . " DEL DIA " . $fecha1 . " AL " . $fecha2 . " EN LA CUOTA NUMERO " . $cuota . " DE LA EMPRESA " . $empresa . " '  , '" . date("Y-m-d H:i:s") . "' );"); //ANEXO DE INFORMACION DE ACCION A LA BD TABLA LOGS
	}
	$mensaje = 'Las fechas fueron cambiadas correctamente.';
	header('Location:datos.php?id1=' . $empresa . '&id2=' . $numero . '&mensaje=' . $mensaje. "&" . $filtro);
	exit();
}
?>
<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#fecha2').datepicker({
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
				beforeShowDay: function(date) {
					var day = date.getDate();
					return [(day != 31), ''];
				}
			});
			$("#solo").change(function(){
				if($("#solo").prop("checked"))
					$("#alert-check").hide();
				else
					$("#alert-check").show();
			});
		});
	</script>

</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Cartera</a>
				<div class="mapa_div"></div><a class="current">Fechas</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="fecha.php?id1=<?php echo $empresa . '&id2=' . $numero . "&" . $filtro?>" method="post">
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">Cambiar fecha</legend>
						<input type="hidden" name="empresa" value="<?php echo $empresa ?>" readonly />
						<p>
							<label for="numero">Factura: </label>
							<input type="text" name="numero" value="<?php echo $numero ?>" readonly />
						</p>
						<p>
							<label for="cuota">Cuota: </label>
							<input type="text" class="cantidad" name="cuota" value="<?php echo $cuota ?>" readonly /> 
							<em style="font-size:14px;">Solo esta cuota  </em><input  id="solo" checked type="checkbox" name="solo" value="1" title="solo esta cuota" />
							<label id="alert-check" style="display:none;"><em style="font-size:14px;">Se ajustarán todas las fechas de las cuotas faltantes</em></label>
						</p>
						<p>
							<label for="fecha">Día actual: </label>
							<input type="text" id="fecha1" name="fecha1" class="fecha" value="<?php echo $fecha ?>" readonly /></p>
						<p>
							<label for="fecha">Nuevo día: </label>
							<input type="text" id="fecha2" name="fecha2" class="fecha validate[required]" value="<?php echo $fecha ?>" />
						</p>
						<p class="boton">
							<button type="button" class="btn btn-primary btnatras" name="atras" onClick="carga(); location.href = 'datos.php?id1=<?php echo $empresa . '&id2=' . $numero ?>'">atras</button>
							<button type="submit" class="btn btn-primary btnmodificar" name="cambiar" value="cambiar">cambiar</button>
						</p>
					</fieldset>
				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<?php
	if (isset($_GET["error"])) echo "<div id='dialog-message' title='Error'><span class='ui-icon ui-icon-circle-close' style='float:left; margin:3px 7px 7px 0;'></span>" . $_GET["error"] . "</div>";
	elseif (isset($_GET["mensaje"])) echo "<div id='dialog-message' title='Correcto'><span class='ui-icon ui-icon-circle-check' style='float:left; margin:3px 7px 7px 0;'></span>" . $_GET["mensaje"] . "</div>";
	?>
</body>

</html>