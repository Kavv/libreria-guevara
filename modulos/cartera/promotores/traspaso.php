<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
if (isset($_POST['traspasar'])) {
	$num = $db->query("UPDATE carteras SET carpromotor = '" . $_POST['a'] . "'  WHERE carpromotor = '" . $_POST['de']. "'");
	$num = $num->rowCount();
	if($num > 0)
	{
		$mensaje = 'Se traspasarón '.$num.' carteras';
		header("location:../listar.php?mensaje=" . $mensaje);
		exit();
	}
	else
	{
		$mensaje = 'El promotor seleccionado no tenia asignada ninguna cartera';
		header("location:traspaso.php?mensaje=" . $mensaje);
		exit();
	}
}
?>
<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css"/>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
	<style>
		.filter-option {
			box-shadow: 0 0 3px 0px black!important;
		}
	</style>
	
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Cartera</a>
				<div class="mapa_div"></div><a class="current">Promotores</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="" method="post">
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">Traspaso de carteras</legend>
						<p>
							<label for="de">Carteras de:</label>
							<select id="de" name="de" class="selectpicker" required 
							data-live-search="true" title="Seleccione un usuario" data-width="100%">
								<?php
								$qry = $db->query("SELECT * FROM usuarios WHERE usupromotor = '1' ORDER BY usunombre");
								while ($row = $qry->fetch(PDO::FETCH_ASSOC))
									echo '<option value="' . $row['usuid'] . '">' . $row['usunombre'] . '</option>';
								?>
							</select>
						</p>
						<p>
							<label for="a">Trasnferirlas a:</label>
							<select id="a" name="a" class="selectpicker" required 
							data-live-search="true" title="Seleccione un usuario" data-width="100%">
								<?php
								$qry = $db->query("SELECT * FROM usuarios WHERE usupromotor = '1' ORDER BY usunombre");
								while ($row = $qry->fetch(PDO::FETCH_ASSOC))
									echo '<option value="' . $row['usuid'] . '">' . $row['usunombre'] . '</option>';
								?>
							</select>
						</p>
						<p class="boton">
							<button class="btn btn-primary btntraspasar" type="submit" name="traspasar" value="traspasar">traspasar</button>
						</p>
					</fieldset>
				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<?php
	if (isset($_GET['mensaje'])) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['mensaje'] . '</div>';
	?>
</body>

</html>