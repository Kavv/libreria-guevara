<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
include($r . 'incluir/phpexcel/Classes/PHPExcel.php');
require($r . 'incluir/funciones.php');


if (isset($_GET['numero'])) {
	$numero = $_GET['numero'];
	$exacta = $_GET['exacta'];
	$cliente = $_GET['cliente'];
}


$con = "SELECT * FROM ((((carteras INNER JOIN empresas ON carempresa = empid) INNER JOIN clientes ON carcliente = cliid) INNER JOIN solicitudes ON (carempresa = solempresa AND carfactura = solfactura)) INNER JOIN departamentos ON depid = soldepcobro) INNER JOIN ciudades ON (soldepcobro = ciudepto AND solciucobro = ciuid) ";

/* Los parametros de la consulta sql se genera dinamicamente 
	en base a los datos recibidos para delimitar los resultados */
$parameters = [];
array_push($parameters, "carpromotor IS NULL");

if ($cliente != "")
	array_push($parameters, "carcliente LIKE '%$cliente%'");
if ($numero != "" && $exacta == "1")
	array_push($parameters, "carfactura = '$numero'");
if ($numero != "" && $exacta != "1")
	array_push($parameters, "carfactura LIKE '%$numero%'");



// Consulta base
$sql = $con;
foreach ($parameters as $index => $parameter) {
	// Se agregan los parametros del WHERE
	if ($index == 0)
		$sql .= " WHERE " . $parameter;
	else
		$sql .= " AND " . $parameter;
}

$qry = $db->query($sql);



$objPHPExcel = new PHPExcel();
$styleArray = array(
	'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN
		)
	)
);
$objPHPExcel->getActiveSheet()->getStyle('A1:N2')->applyFromArray($styleArray);
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:N1');
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'Cartera - Promotores - Asignar');
$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A2', 'EMPRESA')
	->setCellValue('B2', 'FACTURA')
	->setCellValue('C2', 'CLIENTE')
	->setCellValue('D2', 'IDENTIFICACION CLIENTE')
	->setCellValue('E2', 'TELEFONO CONTACTO')
	->setCellValue('F2', 'EMAIL')
	->setCellValue('G2', 'SIGUIENTE PAGO')
	->setCellValue('H2', 'DEPARTAMENTO')
	->setCellValue('I2', 'CIUDAD')
	->setCellValue('J2', 'CUOTAS PACTADAS')
	->setCellValue('K2', 'DIAS DE MORA')
	->setCellValue('L2', 'VALOR CUOTA')
	->setCellValue('M2', 'TOTAL')
	->setCellValue('N2', 'SALDO PENDIENTE');

$i = 3;
while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
	$row2  = $db->query("SELECT * FROM detcarteras WHERE dcafactura = '" . $row['carfactura'] . "' AND dcaempresa = '" . $row['carempresa'] . "' AND dcaestado='ACTIVA' ORDER BY dcacuota ASC")->fetch(PDO::FETCH_ASSOC);

	$objPHPExcel->getActiveSheet()->getStyle('A' . $i . ':N' . $i)->applyFromArray($styleArray);

	$mora = 0;
	if($row2)
	{
		if ((date('Y-m-d') > $row2['dcafecha']) && $row['carsaldo'] != 0)
			$mora = fechaDif($row2['dcafecha'], date('Y-m-d'));
	}

	$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('A' . $i, $row['empnombre'])
		->setCellValue('B' . $i, $row['carfactura'])
		->setCellValue('C' . $i, $row['clinombre'] . ' ' . $row['clinom2'] . ' ' . $row['cliape1'] . ' ' . $row['cliape2'])
		->setCellValue('D' . $i, $row['cliid'])
		->setCellValue('E' . $i, $row['clicelular'])
		->setCellValue('F' . $i, $row['cliemail'])
		->setCellValue('G' . $i, $row2['dcafecha'])
		->setCellValue('H' . $i, $row['depnombre'])
		->setCellValue('I' . $i, $row['ciunombre'])
		->setCellValue('J' . $i, $row['carncuota'])
		->setCellValue('K' . $i, $mora)
		->setCellValue('L' . $i, $row['carcuota'])
		->setCellValue('M' . $i, $row['cartotal'])
		->setCellValue('N' . $i, $row['carsaldo']);

	$i++;
}


// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Cartera_Promotores_Asignar.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
