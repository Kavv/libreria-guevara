<?php
	$r = '../../../';
	require($r . 'incluir/session.php');
	require($r . 'incluir/connection.php');
	$numero = $exacta = $cliente = "";
	if (isset($_POST['consultar'])) {
		$numero = $_POST['numero'];
		if(isset($_POST['exacta']))
			$exacta = $_POST['exacta'];
		$cliente = $_POST['cliente'];
	} 
	if (isset($_GET['numero'])) {
		$numero = $_GET['numero'];
		$exacta = $_GET['exacta'];
		$cliente = $_GET['cliente'];
	}
	$filtro = 'numero=' . $numero . '&exacta=' . $exacta . '&cliente=' . $cliente;

	if (isset($_POST['asignar'])) {
		$row = $db->query("SELECT * FROM usuarios WHERE usuid = '" . $_POST['promotor'] . "'")->fetch(PDO::FETCH_ASSOC);
		$qry = $db->query("UPDATE carteras SET carpromotor = '" . $row['usuid'] . "', cargrupo = '" . $row['usuGrupo'] . "'  WHERE carempresa = '" . $_POST['empresa'] . "' AND carfactura = '" . $_POST['numero'] . "'");
		$mensaje = 'Se asigno el promotor';
		header("location:asignar.php?".$filtro."&mensaje=".$mensaje);
		exit();
	}
	if(isset($_GET['mensaje']))
		$mensaje = $_GET['mensaje'];



	$con = "SELECT * FROM ((((carteras INNER JOIN empresas ON carempresa = empid) INNER JOIN clientes ON carcliente = cliid) INNER JOIN solicitudes ON (carempresa = solempresa AND carfactura = solfactura)) INNER JOIN departamentos ON depid = soldepcobro) INNER JOIN ciudades ON (soldepcobro = ciudepto AND solciucobro = ciuid) ";

	/* Los parametros de la consulta sql se genera dinamicamente 
		en base a los datos recibidos para delimitar los resultados */
	$parameters = [];
	array_push($parameters, "carpromotor IS NULL");
	
	if ($cliente != "")
		array_push($parameters, "carcliente LIKE '%$cliente%'");
	if ($numero != "" && $exacta == "1")
		array_push($parameters, "carfactura = '$numero'");
	if ($numero != "" && $exacta != "1")
		array_push($parameters, "carfactura LIKE '%$numero%'");



	// Consulta base
	$sql = $con;
	foreach ($parameters as $index => $parameter) {
		// Se agregan los parametros del WHERE
		if ($index == 0)
			$sql .= " WHERE " . $parameter;
		else
			$sql .= " AND " . $parameter;
	}

	$qry = $db->query($sql);

?>
<!doctype html>
<html>

<head>
	<meta charset="utf-8">
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>

	<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
	<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#tabla').dataTable({
				'bStateSave': true,
				'bJQueryUI': true,
				// 'aaSorting': [ [0,'desc'], [1,'desc'] ],
				'sPaginationType': 'full_numbers',
				'oLanguage': {
					'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
				},
				'aoColumnDefs': [{
					'bSortable': false,
					'aTargets': [2, 6, 7]
				}]
			});
		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Cartera</a>
				<div class="mapa_div"></div><a class="current">Promotores</a>
			</article>
			<article id="contenido">
				<h2>Cartera por asignar</h2>
				<div class="reporte">
					<a href="excelpromotoresasignar.php?<?php echo $filtro;?>"><img src="<?php echo $r ?>imagenes/iconos/excel.png" title="csv" /></a>
				</div>
				<table id="tabla">
					<thead>
						<tr>
							<th>Empresa</th>
							<th>cuenta</th>
							<th>Cliente</th>
							<th>Siguiente Pago</th>
							<th>Departamento</th>
							<th>Ciudad</th>
							<th>Direccion</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php
						while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
							$row2  = $db->query("SELECT * FROM detcarteras WHERE dcafactura = '". $row['carfactura'] ."' AND dcaempresa = '". $row['carempresa'] ."' AND dcaestado='ACTIVA' ORDER BY dcacuota ASC")->fetch(PDO::FETCH_ASSOC);
						?>
							<tr>
								<td title="<?php echo $row['carempresa'] ?>"><?php echo $row['empnombre'] ?></td>
								<td align="center"><?php echo $row['carfactura'] ?></td>
								<td title="<?php echo $row['carcliente'] ?>"><?php echo $row['clinombre'] . ' ' . $row['clinom2'] . ' ' . $row['cliape1'] . ' ' . $row['cliape2'] ?></td>
								<td><?php echo $row2['dcafecha'] ?></td>
								<td><?php echo $row['depnombre'] ?></td>
								<td><?php echo $row['ciunombre'] ?></td>
								<td><?php echo $row['solbarcobro'] . ', ' . $row['solcobro'] ?></td>
								<td align="center"><a href="asignacion.php?empresa=<?php echo $row['carempresa'] . '&cuenta=' . $row['carfactura'] . '&' . $filtro ?>" title="Asignar" onClick="carga()"><img src="<?php echo $r ?>imagenes/iconos/tick.png" class="grayscale" title="Asignar" /></a></td>
							</tr>
						<?php
						}
						?>
					</tbody>
				</table>
				<p class="boton">
					<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href = 'conasignar.php'">atras</button>
				</p>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<?php
	if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>';
	elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>';
	?>
</body>

</html>