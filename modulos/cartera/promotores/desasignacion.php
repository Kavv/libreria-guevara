<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
$filtro = 'numero=' . $_GET['numero'] . '&exacta=' . $_GET['exacta'] . '&cliente=' . $_GET['cliente'];
$row = $db->query("SELECT * FROM (((((carteras INNER JOIN empresas ON carempresa = empid) INNER JOIN clientes ON carcliente = cliid) INNER JOIN solicitudes ON (carempresa = solempresa AND carfactura = solfactura)) INNER JOIN departamentos ON depid = soldepcobro) INNER JOIN ciudades ON (soldepcobro = ciudepto AND solciucobro = ciuid)) INNER JOIN usuarios ON carpromotor = usuid WHERE carempresa = '" . $_GET['empresa'] . "' AND carfactura = '" . $_GET['cuenta'] . "'")->fetch(PDO::FETCH_ASSOC);
?>
<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>

</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Cartera</a>
				<div class="mapa_div"></div><a class="current">Promotores</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="desasignar.php?<?php echo $filtro ?>" method="post">
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">Desasignacion de promotor</legend>
						<p>
							<label for="cliente">Cliente:</label>
							<label><?php echo $row['clinombre'] . ' ' . $row['clinom2'] . ' ' . $row['cliape1'] . ' ' . $row['cliape2'] ?></label>
						</p>
						<p>
							<label for="empresa">Empresa:</label>
							<input type="hidden" name="empresa" value="<?php echo $row['carempresa'] ?>">
							<label><?php echo $row['empnombre'] ?></label>
						</p>
						<p>
							<label for="direccion">Direccion de cobro:</label>
							<label><?php echo $row['solbarcobro'] . ', ' . $row['solcobro'] . ' - (' . $row['depnombre'] . ' / ' . $row['ciunombre'] . ')' ?></label>
						</p>
						<p>
							<label for="cuenta">Factura:</label>
							<input type="text" name="numero" class="consecutivo" value="<?php echo $row['carfactura'] ?>" readonly>
						</p>
						<p>
							<label for="promotor">Promotor:</label>
							<label><?php echo $row['usunombre'] ?></label>
						</p>
						<p class="boton">
							<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href = 'desasignar.php?<?php echo $filtro ?>'">atras</button>
							<button type="submit" class="btn btn-primary btndesasignar" name="desasignar" value="desasignar">desasignar</button>
						</p>
					</fieldset>
				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
</body>

</html>