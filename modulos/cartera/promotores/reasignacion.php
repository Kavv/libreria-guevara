<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
$filtro = 'numero=' . $_GET['numero'] . '&exacta=' . $_GET['exacta'] . '&cliente=' . $_GET['cliente'];
$row = $db->query("SELECT * FROM (((((carteras INNER JOIN empresas ON carempresa = empid) INNER JOIN clientes ON carcliente = cliid) INNER JOIN solicitudes ON (carempresa = solempresa AND carfactura = solfactura)) INNER JOIN departamentos ON depid = soldepcobro) INNER JOIN ciudades ON (soldepcobro = ciudepto AND solciucobro = ciuid)) INNER JOIN usuarios ON carpromotor = usuid WHERE carempresa = '" . $_GET['empresa'] . "' AND carfactura = '" . $_GET['cuenta'] . "'")->fetch(PDO::FETCH_ASSOC);
?>
<!doctype html>
<html>

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css"/>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
	<style>
		.filter-option {
			box-shadow: 0 0 3px 0px black!important;
		}
	</style>

</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="#">Principal</a>
				<div class="mapa_div"></div><a href="#">Cartera</a>
				<div class="mapa_div"></div><a class="current">Promotores</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="reasignar.php?<?php echo $filtro ?>" method="post">
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
						<legend class="ui-widget ui-widget-header ui-corner-all">Reasignacion de promotor</legend>
						<p>
							<label for="cliente">Cliente:</label>
							<label><?php echo $row['clinombre'] . ' ' . $row['clinom2'] . ' ' . $row['cliape1'] . ' ' . $row['cliape2'] ?></label>
						</p>
						<p>
							<label for="empresa">Empresa:</label>
							<label><input type="hidden" name="empresa" value="<?php echo $row['carempresa'] ?>"><?php echo $row['empnombre'] ?></label>
						</p>
						<p>
							<label for="direccion">Direccion de cobro:</label>
							<label><?php echo $row['solbarcobro'] . ', ' . $row['solcobro'] . ' - (' . $row['depnombre'] . ' / ' . $row['ciunombre'] . ')' ?></label>
						</p>
						<p>
							<label for="cuenta">cuenta:</label>
							<input type="text" name="numero" class="consecutivo" value="<?php echo $row['carfactura'] ?>" readonly>
						</p>
						<p>
							<label for="cliente">Promotor:</label>
							<select id="promotor" name="promotor" class="selectpicker" required 
							data-live-search="true" title="Seleccione un usuario" data-width="100%">
								<?php
								$qry = $db->query("SELECT * FROM usuarios WHERE usupromotor = '1' ORDER BY usunombre");
								while ($row2 = $qry->fetch(PDO::FETCH_ASSOC))
								if($row2['usuid'] != $row['usuid'])
									echo '<option value="' . $row2['usuid'] . '">' . $row2['usunombre'] . '</option>';
								else
									echo '<option value="' . $row2['usuid'] . '" selected>' . $row2['usunombre'] . '</option>';
								?>
							</select>
						</p>
						<p class="boton mt-3">
							<button type="button" class="btn btn-primary btnatras" onClick="carga(); location.href = 'reasignar.php?<?php echo $filtro;?>'">atras</button>
							<button type="submit" class="btn btn-primary btnreasignar" name="reasignar" value="reasignar">reasignar</button>
						</p>
					</fieldset>
				</form>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
</body>

</html>