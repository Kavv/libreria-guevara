<?php
    
unlink('../../Modulos/Cartera/archivos_csv/DeterioDetalladoCartera.csv');
$r = '../../';
require($r.'incluir/session.php');
require($r.'incluir/connection.php');

$empresa = $_POST['empresa'];
$fecha1 = $_POST['fecha1'];
$fecha2 = $_POST['fecha2'];
$TipoConsult = $_POST['TipoConsult'];

$mes = $_POST['mes'];
$annio = $_POST['annio'];

if ($TipoConsult == null)
{
    $error = 'Debe selecionar una empresa y un tipo de consulta';
    header('Location:deterioro.php?error='.$error);
    exit();
}
else if($TipoConsult == '1')
{
    if($mes == '' || $mes == '' ){
        $error = 'Debe selecionar una empresa, mes y a�o';
        header('Location:deterioro.php?error='.$error);
        exit();
    }
    else
    {
        
        $formato = $annio.'-'.$mes.'-15';
        $fecha = new DateTime($formato);
        $fecha1 =  $fecha->modify('first day of this month')->format('Y-m-d');
        $fecha2 =  $fecha->modify('last day of this month')->format('Y-m-d');
    }
}
else if ($TipoConsult == '2')
{
    if($fecha1 == '' || $fecha2 == '' ){
        $error = 'Debe selecionar una empresa y un rango de fechas';
        header('Location:deterioro.php?error='.$error);
        exit();
    }
}


$stmt = $db->prepare("CALL obtenerDeterioroCarteraResumen(:empresa,:fecha1,:fecha2)");
$stmt->bindParam(':empresa', $empresa, PDO::PARAM_STR, 50);    
$stmt->bindParam(':fecha1', $fecha1, PDO::PARAM_STR, 20);
$stmt->bindParam(':fecha2', $fecha2, PDO::PARAM_STR, 20);

$stmt->execute();

$results = array();
do 
{
    $results[] = $stmt->fetchAll();
} 
while ($stmt->nextRowset());

$datosConsulta = $results[0][0];

$nombreEmp = '';

if ($empresa != '0')
{
    $qry = $db->query("SELECT * FROM empresas WHERE empid = $empresa LIMIT 1");
    $col = $qry->fetch(PDO::FETCH_ASSOC);
	$nombreEmp = 'la empresa : '.$col['empnombre'];
}
else
{
	$nombreEmp = 'todas las empresas';
}



?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>. : : I d e n C o r p : : .</title>
    <meta charset="utf-8" />
    <link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/css/normalize.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/jquery/css/smoothness/jquery-ui-1.10.3.custom.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/css/css.css" type="text/css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/css/menu.css" type="text/css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
    <script type="text/javascript" src="<?php echo $r ?>incluir/jquery/js/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="<?php echo $r ?>incluir/jquery/js/jquery-ui-1.10.3.custom.js"></script>
    <script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
</head>
<body>
    <?php require($r.'incluir/src/login.php') ?>
    <section id="principal">
        <?php require($r.'incluir/src/cabeza.php') ?>
        <?php require($r.'incluir/src/menu.php') ?>
        <article id="cuerpo">
            <article class="mapa">
                <a href="#">Principal</a><div class="mapa_div"></div><a class="current">Cartera</a>
            </article>
            <article id="content">
                <div  class="reporte" >
                    <e id="divExcel"></e>
                    <a style="text-align:right" href="../../Modulos/Cartera/archivos_csv/INSTRUCTIVO DETERIORO.pdf" title="Ayuda" target="_blank"><img src="<?php echo $r ?>imagenes/iconos/help.png" title="pdf" /></a>
                    <a href="#" id="btnImprime" class="col-md-3"><img src="<?php echo $r ?>imagenes/iconos/pdf.png" title="pdf" /></a> 
                    <a href="#" id="createCsv" class="col-md-3"><img src="<?php echo $r ?>imagenes/iconos/excel.png" title="csv" /></a>
                </div>
                <br />
                <table style="width:100%; text-align:center">
                    <tr>
                         <td>Deterioro de cartera de <?php echo $nombreEmp; ?> con cuotas que se encuentran entre los dias 1 al 390 de mora</td>
                    </tr>
                    <tr>
                         <td>Este infome tiene como fecha de inicio <u><?php echo $fecha1; ?></u> y fecha final  <u><?php echo $fecha2; ?></u></td>
                    </tr>
                </table>
                <br />
                <br />
                <table border="0" style="width:30%">
                    <tr>
                        <td>TOTAL CLIENTES</td>
                        <td><?php echo number_format($datosConsulta[0]); ?></td>
                    </tr>
                    <tr>
                        <td>TOTAL FACTURAS</td>
                        <td><?php echo number_format($datosConsulta[1]); ?></td>
                    </tr>
                    <tr>
                        <td>N TOTAL CUOTAS</td>
                        <td><?php echo number_format($datosConsulta[20]); ?></td>
                    </tr>
                </table>
                <br />
                <table class="tabla" border="1" style="border-collapse:collapse;width:100%;font-size:11px">
                    <thead>
                        <tr>
                            <th>categoria</th>
                            <th>RANGO DE DIAS</th>
                            <th>PORCENTAJE</th>
                            <th>NUMERO DETERIOROS</th>
                            <th>BASE CARTERA</th>
                            <th>VALOR DETERIORO</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>categoria A</td>
                            <td>1 -30</td>
                            <td>1 %</td>
                            <td style="text-align:right"><?php echo number_format($datosConsulta[3]); ?></td>
                            <td style="text-align:right"><?php echo number_format($datosConsulta[9]); ?></td>
                            <td style="text-align:right"><?php echo number_format($datosConsulta[15]); ?></td>
                        </tr>
                        <tr>
                            <td>categoria B</td>
                            <td>31 -90</td>
                            <td>3.2 %</td>
                            <td style="text-align:right"><?php echo number_format($datosConsulta[4]); ?></td>
                            <td style="text-align:right"><?php echo number_format($datosConsulta[10]); ?></td>
                            <td style="text-align:right"><?php echo number_format($datosConsulta[16]); ?></td>
                        </tr>
                        <tr>
                            <td>categoria C</td>
                            <td>91 -180</td>
                            <td>20 %</td>
                            <td style="text-align:right"><?php echo number_format($datosConsulta[5]); ?></td>
                            <td style="text-align:right"><?php echo number_format($datosConsulta[11]); ?></td>
                            <td style="text-align:right"><?php echo number_format($datosConsulta[17]); ?></td>
                        </tr>
                         <tr>
                            <td>categoria D</td>
                            <td>181 -360</td>
                            <td>50 %</td>
                            <td style="text-align:right"><?php echo number_format($datosConsulta[6]); ?></td>
                            <td style="text-align:right"><?php echo number_format($datosConsulta[12]); ?></td>
                            <td style="text-align:right"><?php echo number_format($datosConsulta[18]); ?></td>
                        </tr>
                        <tr>
                            <td>categoria E</td>
                            <td>361 -390</td>
                            <td>100 %</td>
                            <td style="text-align:right"><?php echo number_format($datosConsulta[7]); ?></td>
                            <td style="text-align:right"><?php echo number_format($datosConsulta[13]); ?></td>
                            <td style="text-align:right"><?php echo number_format($datosConsulta[19]); ?></td>
                        </tr>
                         <tr>
                            <td>TOTALES</td>
                            <td>-</td>
                            <td>-</td>
                            <td style="text-align:right"><?php echo number_format($datosConsulta[2]); ?></td>
                            <td style="text-align:right"><?php echo number_format($datosConsulta[8]); ?></td>
                            <td style="text-align:right"><?php echo number_format($datosConsulta[14]); ?></td>
                        </tr>
                    </tbody>
                </table>
                <br />
                <hr>
                <p class="boton">
                    <button type="button" class="btnatras" onclick=" carga(); location.href = 'deterioro.php'">atras</button>
                </p>
            </article>
        </article>
        <?php require($r.'incluir/src/pie.php') ?>
    </section>
</body>
</html>
<script>
    $(document).ready(function(){
        $('.tabla').dataTable({
            'bPaginate': false,
            'bLengthChange': false,
            'bFilter': false,
            'bSort': false,
            'bInfo': false,
            'bAutoWidth': false,
            'bStateSave': true,
            'bJQueryUI': true
        });
        $('.btnatras').button({ icons: { primary: 'ui-icon ui-icon-arrowthick-1-w' } });
       
    });
    
    $('#createCsv').click(function() {
        CreteExcel();
    });
    
    function CreteExcel() {
        $('#divExcel').empty();       
        $('#divExcel').append('Espere un momento...');
        var dataObject = { 
            'empresa': '<?php echo $empresa; ?>',            
            'fecha1': '<?php echo $fecha1; ?>',
            'fecha2': '<?php echo $fecha2; ?>'
        };
        //envio de info al controlador
        $.ajax({
            type: 'POST',
            url: '../../modulos/cartera/excelDeterioro.php',
            data: dataObject,
            success: function (res) {
                $('#divExcel').empty();       
                $('#divExcel').append('<a href="../../modulos/cartera/archivos_csv/DeterioDetalladoCartera.csv" target="_blank"><b>DESCARGUE EL INFORME</b></a>');
            },
            error: function () {
                console.log('error POST listarCasos');
            }
        });
    }

    $('#btnImprime').click(function () {
        $('.reporte').hide();
        imprimir();
    });

    function imprimir() {
        //$('#contenido').
        var objeto = document.getElementById('content');  //obtenemos el objeto a imprimir
        var ventana = window.open('', '_blank');  //abrimos una ventana vac�a nueva
        ventana.document.writeln(objeto.innerHTML);  //imprimimos el HTML del objeto en la nueva ventana
        ventana.document.close();  //cerramos el documento
        ventana.print();  //imprimimos la ventana
        ventana.close();  //cerramos la ventana
        $('.reporte').show();
    }
    
</script>