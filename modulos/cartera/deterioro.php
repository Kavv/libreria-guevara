<?php

    unlink('../../Modulos/Cartera/archivos_csv/DeterioDetalladoCartera.csv');
    $r = '../../';
    require($r.'incluir/session.php');
    require($r.'incluir/connection.php');
    /*PRUEBA*/

    $actual = intval(date('Y'));
    $inicial = 2015;
    $meses = array('enero','febrero','marzo','abril','mayo','junio','julio', 'agosto','septiembre','octubre','noviembre','diciembre');

?>
<!doctype html>
<html>
<head>
    <title>. : : I d e n C o r p : : .</title>
    <meta charset="utf-8" />
    <link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/css/normalize.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/jquery/css/smoothness/jquery-ui-1.10.3.custom.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/css/css.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/css/menu.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/validation/css/validationEngine.jquery.css" />
    <script type="text/javascript" src="<?php echo $r ?>incluir/jquery/js/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="<?php echo $r ?>incluir/jquery/js/jquery-ui-1.10.3.custom.js"></script>
    <script type="text/javascript" src="<?php echo $r ?>incluir/validation/js/languages/jquery.validationEngine-es.js"></script>
    <script type="text/javascript" src="<?php echo $r ?>incluir/validation/js/jquery.validationEngine.js"></script>
    <script type="text/javascript" src="<?php echo $r ?>incluir/jquery/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>
    
</head>
<body>
    <?php require($r.'incluir/src/login.php') ?>
    <section id="principal">
        <?php require($r.'incluir/src/cabeza.php') ?>
        <?php require($r.'incluir/src/menu.php') ?>
        <article id="cuerpo">
            <article class="mapa">
                <a href="#">Principal</a><div class="mapa_div"></div><a class="current">Cartera</a>
            </article>
            <article id="contenido">
                <form id="form" name="form" action="listDeterioro.php" method="post">
                    <fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
                        <legend class="ui-widget ui-widget-header ui-corner-all">Consulta del deterioro de cartera menor a 390 dias</legend>
                        <div>
                            <label for="empresa">Empresa:</label>
                            <select name="empresa">
                                    <option value="0">Todas:</option>
                                    <?php
                                            $qry = $db->query("SELECT * FROM empresas ORDER BY empnombre");
                                            while($row = $qry->fetch(PDO::FETCH_ASSOC)){
                                                echo '<option value='.$row['empid'].'>'.$row['empnombre'].'</option>';
                                            }
                                    ?>
                            </select>
                        </div>
                        <br />
                        <div>
                            <label for="fechas">Tipo de consulta</label>
                            <div>
                                Por mes <input type="radio" name="TipoConsult" class="tipoConsulta" value="1"/>
                                Por rango <input type="radio" name="TipoConsult" class="tipoConsulta" value="2"/>
                            </div> 
                        </div>
                        <br />
                        <br />
                        <div id="porMes" style="display:none">
                            <div>
                                <label for="fechas">A&ntildeo:</label>
                                <select name="annio" id="annio">
                                    <option value="">Seleccione:</option>
                                    <?php 
                                    for ($i = $inicial; $i <= $actual; $i++){
                                        echo '<option value='.$i.'>'.$i.'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <br />
                            <div>
                                <label for="fechas">Mes:</label>
                                <select name="mes" id="mes">
                                    <option value="">Seleccione:</option>
                                    <?php 
                                        $array = $meses; 
                                        for ($i=0; $i<sizeof($array); $i++){ 
                                            $ind = $i+1;
                                            echo "<option value='$ind'>". $array[$i] . '</option>'; 
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div id="porRango" style="display:none">
                            <label for="fechas">Fechas:</label>
                            <input type="text" class="fecha" id="fecha1" name="fecha1" /> <input type="text" id="fecha2" class="fecha" name="fecha2" />
                        </div>
                        <?php
                        $qryval = $db->query("SELECT * FROM usuarios INNER JOIN perfiles ON usuperfil = perid WHERE usuid = ".$_SESSION['id']); //verificacion usuario por ID de sesion
                        $rowval = $qryval->fetch(PDO::FETCH_ASSOC);

                        if ($rowval['usuperfil'] == 100){
                            echo "<input type='hidden' name='covinet' value='1' />";
                        }
                        ?>
                        <p class="boton">
                            <button type="submit" class="btnconsulta" name="consultar" value="consultar" onclick="carga()">consultar</button>
                        </p>
                    </fieldset>
                </form>
            </article>
        </article>
        <?php require($r.'incluir/src/pie.php') ?>
    </section>
    <?php
    if($_GET['error']) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>'.$_GET['error'].'</div>';
    ?>
</body>
</html>
<script type="text/javascript">
    $(document).ready(function () {
        $('#form').validationEngine({
            showOneMessage: true,
            onValidationComplete: function (form, status) {
                if (status) {
                    carga();
                    return true;
                }
            }
        });
        $('#fecha1').datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
            maxDate: '+0D',
            onClose: function (selectedDate) {
                $('#fecha2').datepicker('option', 'minDate', selectedDate);
            }
        }).keypress(function (event) { event.preventDefault() });
        $('#fecha2').datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
            maxDate: '+0D',
            onClose: function (selectedDate) {
                $('#fecha1').datepicker('option', 'maxDate', selectedDate);
            }
        }).keypress(function (event) { event.preventDefault() });
        $('.btnconsulta').button({ icons: { primary: 'ui-icon ui-icon-search' } });
        $('#dialog-message').dialog({
            height: 80,
            width: 'auto',
            modal: true
        });
    });

    $('.tipoConsulta').change(function() {
        if ($(this).val() == '1') {
            $('#porMes').show();
            $('#porRango').hide();
            $('#fecha1').val('');
            $('#fecha2').val('');
        }
        else if ($(this).val() == '2') {
            $('#porRango').show();
            $('#porMes').hide();
            $('#annio').val('');
            $('#mes').val('');
        }
    });

</script>
<style type="text/css">
    #form form {
        width: 690px;
    }

    #form fieldset {
        padding: 10px;
        display: block;
        width: 690px;
        margin: 20px auto;
    }

    #form legend {
        font-weight: bold;
        margin-left: 5px;
        padding: 5px;
    }

    #form label {
        display: inline-block;
        width: 100px;
        text-align: right;
        float: left;
        margin: 0.3em 2% 0 0;
    }

    #form p {
        margin: 5px 0;
    }
</style>