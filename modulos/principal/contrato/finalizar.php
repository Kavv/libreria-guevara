<?php
    $r = '../../../';
    require($r . 'incluir/session.php');
    require($r . 'incluir/connection.php');
    $contrato = $_GET['solid'];
?>
<!doctype html>
<html lang="es">
<head>
    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>


</head>
<body>
<?php require($r . 'incluir/src/login.php') ?>
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <?php require($r . 'incluir/src/menu.php') ?>
    <article id="cuerpo">
        <article class="mapa">
            <a href="#">Principal</a>
            <div class="mapa_div"></div>
            <a href="#">Bodega</a>
            <div class="mapa_div"></div>
            <a href="#">Documentos</a>
            <div class="mapa_div"></div>
            <a class="current">Entrada</a>
        </article>
        <article id="contenido">
            <p align="center">
                <iframe src="pdf.php?solid=<?php echo $contrato?>" width="800" height="550"></iframe>
            </p>
            <p class="boton">
                <button class="btn btn-warning" onClick="carga(); location.href = 'facturar.php?solid=<?php echo $contrato?>'">Editar esta Factura</button>
                <button type="button" class="btn btn-primary btnatras"
                        onClick="carga(); location.href = 'consultar.php'">
                    Ver otros contratos
                </button>
            </p>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php') ?>
</section>
</body>
</html>