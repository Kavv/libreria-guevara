<?php
    $r = '../../../';
    require($r . 'incluir/session.php');
    require($r . 'incluir/connection.php');
    require($r . 'incluir/funciones.php');

    $contrato = $_GET['solid'];
    $data = $db->query("SELECT * FROM solicitudes 
    INNER JOIN clientes ON cliid = solcliente 
    LEFT JOIN departamentos ON depid = soldepentrega 
    LEFT JOIN ciudades ON (ciuid = solciuentrega AND ciudepto = soldepentrega)
    LEFT JOIN movimientos on (movdocumento = solfactura AND movprefijo = 'FV') 
    LEFT JOIN carteras on carfactura = solfactura 
    WHERE solid = '$contrato'; ")->fetch(PDO::FETCH_ASSOC);

    $full_name = $data['clinombre'];
    if( $data['clinom2'] != "" )
        $full_name .= " " . $data['clinom2'];
    if( $data['cliape1'] != "" )
        $full_name .= " " . $data['cliape1'];
    if( $data['cliape2'] != "" )
        $full_name .= " " . $data['cliape2'];

    // Cliente
    $cedula = strupperEsp(str_replace("'", "", $data['solcliente']));
    $nombre = strupperEsp(str_replace("'", "", $full_name));
    $telefono1 = str_replace("'", "", $data['soltelentrega']);
    $telefono2 = str_replace("'", "", $data['soltelcobro']);
    $departamento = strupperEsp(str_replace("'", "", $data['soldepentrega']));

    $ciudad = $data['solciuentrega'];
    $full_ciudad = str_replace("'", "", $data['soldepentrega'].$ciudad);

    $trabajo = strupperEsp(str_replace("'", "", $data['cliempresa']));
    $direccion = strupperEsp(str_replace("'", "", $data['solentrega']));
    $conyugue = strupperEsp(str_replace("'", "", $data['clinomfamiliar']));
    $conyugue_telefono = str_replace("'", "", $data['clitelfamiliar']);
    $conyugue_trabajo = strupperEsp(str_replace("'", "", $data['cliempfamiliar']));
    $referencia1 = strupperEsp(str_replace("'", "", $data['clirefnombre1']));
    $ref1_tel1 = str_replace("'", "", $data['clireftelefono1']);
    $ref1_tel2 = str_replace("'", "", $data['clirefcelular1']);
    $referencia2 = strupperEsp(str_replace("'", "", $data['clirefnombre2']));
    $ref2_tel1 = str_replace("'", "", $data['clireftelefono2']);
    $ref2_tel2 = str_replace("'", "", $data['clirefcelular2']);

    // Solicitud
    $empresa = "16027020274133";
    //$contrato = strupperEsp(str_replace("'", "", $data['solid']));
    $fecha_contrato = str_replace("'", "", $data['solfecha']);
    $estudiante = strupperEsp(str_replace("'", "", $data['estudiante']));
    $escuela = strupperEsp(str_replace("'", "", $data['escuela']));
    $grado = strupperEsp(str_replace("'", "", $data['grado']));
    $observaciones = strupperEsp(str_replace(["'","<",">"], "", $data['solobservacion']));

    $razonamientos = $db->query("SELECT hissolicitudes.* FROM solicitudes INNER JOIN hissolicitudes ON hsosolicitud = solid WHERE solid='$contrato' ORDER BY hsoid ASC");
    
    if(isset($data['razonamientos']))
    $razonamientos = $data['razonamientos'];
    $cantidad_razonamiento = $razonamientos->rowCount();
    
    
    $asesor = $data['solasesor'];
    $entregador = $data['solenvio'];
    $cobrador = $data['solrelacionista'];
    
    // Por defecto
    $clase = $data['clase'];
    $salida = $data['salida'];
    $cuenta = $data['cuenta'];
    $digitador = $data['digitador'];

    // solicitud-detalle

    $productos = $db->query("SELECT detproducto, detcantidad, detprecio, pronombre FROM solicitudes INNER JOIN detsolicitudes ON detsolicitud = solid INNER JOIN productos on proid = detproducto WHERE solid = '$contrato';");
    $cantidad_productos = $productos->rowCount();

    // Información de pago
    $fentrega = $data['solfechdescarga'];
    $fcobro = $data['solcompromiso'];
    $cobrar = $data['cobro'];
    $monto_contrato = $data['soltotal'];
    $prima = $data['solcuota'];
    $neto = $data['cartotal'];
    $descuento = $data['movdescuento'];
    $ncuotas = $data['carncuota'];
    $cuota = $data['carcuota'];
    $saldo = $data['carsaldo'];
    $cartera_estado = $data['carestado'];




?>


<!doctype html>
<html>

<head>
    <title>VER CONTRATO <?php echo $contrato ?></title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-not-form.php');
    ?>
    <style>
        #form input {
            text-transform: uppercase;
        }
        .divisor {
            margin-left: 5px;
            margin-right: 5px;
            margin-top: 15px;
            margin-bottom: 15px;
            padding-top: 10px;
            padding-bottom: 2px;
            background-color: #004593;
            color: #fff;
            border-radius: 8px;
        }
        .divisor h6 {
            font-weight: bold; 
        }
    </style>
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

	<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            info_extra();
        });
        
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="<?php echo $r ?>index.php">Principal</a>
				<div class="mapa_div"></div><a>Contrato</a>
				<div class="mapa_div"></div><a class="current">Detalle</a>
			</article>
			<article id="contenido">
                
                <div id="form">
                    <div id="carouselExampleControls" class="carousel slide" data-ride="false" >
                        <div class="carousel-inner">
                            <div class="carousel-item active" id="carousel1">
                                <?php require('detalle_carousel1.php') ?>
                                <div class="row d-flex justify-content-center mt-5">
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-primary btn-block" onclick="CambioPag();">INFORMACIÓN DE PAGO</button>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item" id="carousel2">
                                <?php require('detalle_carousel2.php') ?>
                                <div class="row mt-3">
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-info btn-block" onclick="CambioPag();">REGRESAR A CONTRATO</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <!-- Modal para ver razonamiento-->
                    <div class="modal fade" id="ver-razonamiento" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Ver Razonamientos</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <table class="table table-hover">
                                        <thead class="text-center">
                                            <th></th>
                                        </thead>
                                        <tbody id="t-razonamiento">
                                            <?php 
                                                while($razonamiento = $razonamientos->fetch(PDO::FETCH_ASSOC))
                                                {
                                                    echo "<tr>
                                                        <td><textarea readonly type='text' class='form-control'>".$razonamiento['hsonota']."</textarea></td>
                                                    </tr>";
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal para agregar razonamiento-->
                    <div class="modal fade" id="add-razonamiento" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Agregar Razonamiento</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <label for="">Describa el razonamiento</label>
                                    <textarea id="razonamiento" class="form-control not-w" rows="3"></textarea>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-success" data-dismiss="modal" onclick="add_razonamiento();">Guardar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </article>
    </section>
</body>
<script>

    function CambioPag()
    {
        if($("#carousel1").hasClass('active'))
        {
            $("#carousel1").removeClass("active");
            $("#carousel2").addClass("active");
        }
        else
        {
            $("#carousel2").removeClass("active");
            $("#carousel1").addClass("active");
        }
    }
    $("fieldset").prop('disabled',true)

</script>