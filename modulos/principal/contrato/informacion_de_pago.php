<?php
    $r = '../../../';
    require($r . 'incluir/session.php');
    require($r . 'incluir/connection.php');
    require($r . 'incluir/funciones.php');
    require('funciones_contrato.php');
    // Cliente
    $cedula = strupperEsp(str_replace("'", "", $_POST['cedula']));
    $nombre = strupperEsp(str_replace("'", "", $_POST['nombre']));
    $telefono1 = str_replace("'", "", $_POST['telefono1']);
    $telefono2 = str_replace("'", "", $_POST['telefono2']);
    $departamento = strupperEsp(str_replace("'", "", $_POST['departamento']));
    $full_ciudad = str_replace("'", "", $_POST['ciudad']);
    $ciudad = substr($full_ciudad,-2);
    $trabajo = strupperEsp(str_replace("'", "", $_POST['trabajo']));
    $direccion = strupperEsp(str_replace("'", "", $_POST['direccion']));
    $conyugue = strupperEsp(str_replace("'", "", $_POST['conyugue']));
    $conyugue_telefono = str_replace("'", "", $_POST['conyugue_telefono']);
    $conyugue_trabajo = strupperEsp(str_replace("'", "", $_POST['conyugue_trabajo']));
    $referencia1 = strupperEsp(str_replace("'", "", $_POST['referencia1']));
    $ref1_tel1 = str_replace("'", "", $_POST['ref1_tel1']);
    $ref1_tel2 = str_replace("'", "", $_POST['ref1_tel2']);
    $referencia2 = strupperEsp(str_replace("'", "", $_POST['referencia2']));
    $ref2_tel1 = str_replace("'", "", $_POST['ref2_tel1']);
    $ref2_tel2 = str_replace("'", "", $_POST['ref2_tel2']);

    // Solicitud
    $empresa = "16027020274133";
    $contrato = strupperEsp(str_replace("'", "", $_POST['contrato']));
    $fecha_contrato = str_replace("'", "", $_POST['fecha-contrato']);
    $estudiante = strupperEsp(str_replace("'", "", $_POST['estudiante']));
    $escuela = strupperEsp(str_replace("'", "", $_POST['escuela']));
    $grado = strupperEsp(str_replace("'", "", $_POST['grado']));
    $observaciones = strupperEsp(str_replace(["'","<",">"], "", $_POST['observaciones']));
    $razonamientos = [];
    if(isset($_POST['razonamientos']))
    $razonamientos = $_POST['razonamientos'];
    $cantidad_razonamiento = count($razonamientos);
    $asesor = $_POST['asesor'];
    $entregador = strupperEsp($_POST['entregador']);
    $cobrador = $_POST['cobrador'];
    // Por defecto
    $clase = 1;
    $cuenta = 1;
    $digitador = $_SESSION['id'];

    // solicitud-detalle
    $producto_codigo = [];
    $producto_nombre = [];
    $producto_precio = [];
    $producto_cantidad = [];
    
    if(isset($_POST['producto-codigo']))
        $producto_codigo = $_POST['producto-codigo'];
    if(isset($_POST['producto-nombre']))
        $producto_nombre = $_POST['producto-nombre'];
    if(isset($_POST['producto-precio']))
        $producto_precio = $_POST['producto-precio'];
    if(isset($_POST['producto-cantidad']))
        $producto_cantidad = $_POST['producto-cantidad'];
        
    $cantidad_productos = count($producto_codigo);

    $tel_aux = "";
    if($telefono1 != "")
        $tel_aux .= $telefono1;
    if($telefono2 != "")
    {
        if($tel_aux != "")
            $tel_aux .= " / ".$telefono2;
        else
            $tel_aux .= $telefono2;
    }

    // Ajuste del nombre
    $nombre1 = "";
    $nombre2 = "";
    $apellido1 = "";
    $apellido2 = "";
    asignar_nombre($nombre);

    if(isset($_POST['guardar-contrato']))
    {
    
        $aux_cliente = $db->query("SELECT * FROM clientes WHERE cliid = '$cedula'")->fetch(PDO::FETCH_ASSOC);
        // Si no hay ningun cliente con el numero de cedula entonces
        if(!$aux_cliente)
        {
            // Generamos la insercion del cliente
            $db->query("INSERT INTO clientes(
            clase, cliid, clinombre, clinom2, cliape1, cliape2, 
            clicelular, clitelresidencia, clidepresidencia, cliciuresidencia, 
            cliempresa, clidirresidencia, clinomfamiliar, clitelfamiliar, 
            cliempfamiliar, clirefnombre1, clireftelefono1, clirefcelular1, 
            clirefnombre2, clireftelefono2 ,clirefcelular2) 
            VALUES ($clase, '$cedula', '$nombre1', '$nombre2', '$apellido1', '$apellido2', 
            '$telefono1', '$telefono2', '$departamento', '$ciudad', 
            '$trabajo', '$direccion', '$conyugue', '$conyugue_telefono', 
            '$conyugue_trabajo', '$referencia1', '$ref1_tel1', '$ref1_tel2', 
            '$referencia2', '$ref2_tel1', '$ref2_tel2' ); ") or die($db->errorInfo()[2]);
        }
        else
        {
            actualizar_cliente($aux_cliente);
        }


        
        $aux_solicitud = $db->query("SELECT solid FROM solicitudes WHERE solid='$contrato';")->fetch(PDO::FETCH_ASSOC);
        if(!$aux_solicitud)
        {
            $db->query("INSERT INTO solicitudes
            (solempresa, solid, soltipo, solfecha, solcliente, solasesor, solrelacionista, 
            soldepentrega, solciuentrega, solentrega, soltelentrega, 
            soldepcobro, solciucobro, solcobro, soltelcobro, 
            solestado, solfechreg, solfechafac, 
            solenvio, solfisico, solobservacion, digitador, cuenta, 
            estudiante, escuela, grado )
            VALUES 
            ('$empresa', '$contrato', 'NORMAL', '$fecha_contrato', '$cedula', '$asesor', '$cobrador', 
            '$departamento', '$ciudad', '$direccion', '$telefono1', 
            '$departamento', '$ciudad', '$direccion', '$telefono2', 
            'BORRADOR', NOW(), NOW(), 
            '$entregador', 0, '$observaciones', '$digitador', '$cuenta', 
            '$estudiante', '$escuela', '$grado');") or die($db->errorInfo()[2]);

            $ultima_factura = $db->query("SELECT solfactura FROM solicitudes where solid = '$contrato'")->fetch(PDO::FETCH_ASSOC);
            if($ultima_factura)
            {
                $factura = $ultima_factura['solfactura'];
                $mov_numero = $factura;
            }
            else
            {
                echo 'Error critico al crear el nuevo contrato contactar inmediatamente a Kevin Valverde <a href="tel:50582449347">8244-9347</a>';
                exit();
            }
    
            
            // En caso de existir razonamiento se agrega como historial de la solicitud
            if($cantidad_razonamiento > 0)
            {
                for($i = 0; $i < $cantidad_razonamiento; $i++)
                {
                    $aux_razonamiento = strupperEsp(str_replace(["'","<",">"], "", $razonamientos[$i]));
                    
                    $db->query("INSERT INTO hissolicitudes(hsoempresa, hsosolicitud, hsofecha, hsousuario, hsonota) 
                    VALUES ('$empresa', '$contrato', NOW(), '$digitador', '$aux_razonamiento');") or die($db->errorInfo()[2]);
                }
            }

            $db->query("INSERT INTO movimientos(movempresa, movprefijo, movnumero, movtercero, movdocumento, movfecha, movestado) 
            VALUES ('$empresa', 'FV', '$mov_numero', '$cedula', '$factura', NOW(), 'PROCESO');") or die($db->errorInfo()[2]);
        
            
            //$db->query("DELETE FROM detsolicitudes WHERE detempresa='$empresa' AND detsolicitud='$contrato'");
            //$db->query("DELETE FROM detmovimientos WHERE dmoempresa='$empresa' AND dmoprefijo = 'FV' AND dmonumero='$numero'");
            
            if($cantidad_productos > 0)
            {
                for($i = 0; $i < $cantidad_productos; $i++)
                {
                    $aux_codigo = $producto_codigo[$i];
                    $aux_cantidad = $producto_cantidad[$i];
                    $aux_precio = $producto_precio[$i];
                    $aux_costo = $db->query("SELECT procosto from productos WHERE proid = '$aux_codigo'");
                    $aux_costo = $aux_costo->fetch(PDO::FETCH_ASSOC);
                    if($aux_costo)
                        $aux_costo = $aux_costo['procosto'];
                    else
                        $aux_costo = 0;
                        
                    $total_costo = $aux_costo * $aux_cantidad;
                    $total_precio = $aux_precio * $aux_cantidad;
        
                    $db->query("INSERT INTO detsolicitudes (detsolicitud, detempresa, detproducto, detcantidad, detprecio) 
                    VALUES ('$contrato', '$empresa', '$aux_codigo', $aux_cantidad, $aux_precio);") or die($db->errorInfo()[2]);
                    
                    $db->query("INSERT INTO detmovimientos (dmoempresa, dmoprefijo, dmonumero, dmoproducto, dmocantidad, dmounitario, dmototal, dmoprecio, dmoptotal) VALUES 
                    ('$empresa', 'FV', '$mov_numero', '$aux_codigo', $aux_cantidad, $aux_costo, $total_costo, $aux_precio, $total_precio );") or die($db->errorInfo()[2]);
                }
            }
            
            $qrylogsregister = $db->query("SELECT * FROM usuarios WHERE usuid = '".$_SESSION['id']."'"); //verificacion usuario por ID de sesion
            $rowlogsregister = $qrylogsregister->fetch(PDO::FETCH_ASSOC);	
            // Log de la acción realizada
            $db->query("INSERT INTO logs ( usuario, id_usuario , ip_usuario , accion_usuario, fecha_accion) VALUES ( '".$rowlogsregister['usunombre']."', '".$_SESSION['id']."', '".$_SERVER['REMOTE_ADDR']."' , 'SE CREO LA 1ER PARTE DEL CONTRATO: $contrato' , '".date("Y-m-d H:i:s")."' );"); 
        
            
            $db->query("INSERT INTO carteras(carempresa, carfactura, carcliente, cartotal, carcuota, carsaldo, carncuota, carfecha, carestado, created_at)
            VALUES ('$empresa', '$factura', '$cedula', 0, 0, 0, 1, NOW(), 'ACTIVA', NOW() );");
        }

    
    }

    if(isset($_POST['informacion-pago']))
    {

        // Agregar el cliente en caso de haber modificado la cedula y ser una cedula nueva
        $aux_cliente = $db->query("SELECT * FROM clientes WHERE cliid = '$cedula'")->fetch(PDO::FETCH_ASSOC);
        // Si no hay ningun cliente con el numero de cedula entonces
        if(!$aux_cliente)
        {
            // Generamos la insercion del cliente
            $db->query("INSERT INTO clientes(clase, cliid, clinombre, clinom2, cliape1, cliape2, clicelular, clitelresidencia, clidepresidencia, cliciuresidencia, cliempresa, clidirresidencia, clinomfamiliar, clitelfamiliar, cliempfamiliar, clirefnombre1, clireftelefono1, clirefcelular1, clirefnombre2, clireftelefono2 ,clirefcelular2) 
            VALUES ($clase, '$cedula', '$nombre1', '$nombre2', '$apellido1', '$apellido2', '$telefono1', '$telefono2', '$departamento', '$ciudad', '$trabajo', '$direccion', '$conyugue', '$conyugue_telefono', '$conyugue_trabajo', '$referencia1', '$ref1_tel1', '$ref1_tel2', '$referencia2', '$ref2_tel1', '$ref2_tel2' ); ") or die($db->errorInfo()[2]);
        }
        else
        {
            actualizar_cliente($aux_cliente);
        }

        // Datos de la cartera a agenerarse
        $contrato_original = $_POST['contrato-original'];
        $entrega = $_POST['entrega'];
        $fecha_cobro = $_POST['fecha-cobro'];
        $cobro = $_POST['cobro'];
        $monto_contrato = floatval($_POST['monto-contrato']);
        $prima = floatval($_POST['prima']);
        $cuenta = $_POST['cuenta'];
        if($cuenta == "")
            $cuenta = 1;
        $salida = $_POST['salida'];
        if($salida == "")
            $salida = 0;

        $neto = floatval($_POST['neto']);
        $descuento = floatval($_POST['descuento']);
        $ncuotas = $_POST['ncuotas'];
        $cuota = floatval($_POST['cuota']);
        $saldo = round($neto - $descuento, 2);

        // Datos de lo pagos abonados
        $detalle_recibo = [];
        $detalle_fpago = [];
        $detalle_monto = [];
        $detalle_observaciones = [];
        if(isset($_POST['detalle-recibo']))
            $detalle_recibo = $_POST['detalle-recibo'];
        if(isset($_POST['detalle-fpago']))
            $detalle_fpago = $_POST['detalle-fpago'];
        if(isset($_POST['detalle-monto']))
            $detalle_monto = $_POST['detalle-monto'];
        if(isset($_POST['detalle-observaciones']))
            $detalle_observaciones = $_POST['detalle-observaciones'];
        if(isset($_POST['detalle-cobrador']))
            $detalle_cobrador = $_POST['detalle-cobrador'];

        
        $aux_solicitud = $db->query("SELECT solfactura FROM solicitudes WHERE solid='$contrato_original';")->fetch(PDO::FETCH_ASSOC);
        $factura  = $aux_solicitud['solfactura'];

		$db->query("UPDATE solicitudes SET 
        
        solid = '$contrato', solfecha = '$fecha_contrato', 
        solcliente = '$cedula', solasesor = '$asesor', solrelacionista = '$cobrador', 
        soldepentrega = '$departamento', solciuentrega = '$ciudad', solentrega = '$direccion', soltelentrega = '$telefono1', 
        soldepcobro = '$departamento', solciucobro = '$ciudad', solcobro = '$direccion', soltelcobro = '$telefono2', 
        solenvio = '$entregador', solobservacion = '$observaciones', 
        estudiante = '$estudiante', escuela = '$escuela', grado = '$grado',

        soltotal = $monto_contrato, solbase = $monto_contrato, solcuota = $prima, solncuota = $ncuotas, 
        solcompromiso = '$fecha_cobro', cobro = '$cobro', solestado = 'ENTREGADO', 
        solfechafac = NOW(), solfechdespacho = '$entrega', solfechdescarga = '$entrega', 
        cuenta = '$cuenta', salida = '$salida'
        WHERE solempresa = '$empresa' AND solid = '$contrato_original'")
        or die($db->errorInfo()[2]);


        //Eliminamos los razonamientos existentes
        $db->query("DELETE FROM hissolicitudes WHERE hsoempresa = '$empresa' AND hsosolicitud = '$contrato_original'");

        // En caso de existir razonamiento se agrega como historial de la solicitud
        if($cantidad_razonamiento > 0)
        {
            for($i = 0; $i < $cantidad_razonamiento; $i++)
            {
                $aux_razonamiento = strupperEsp(str_replace(["'","<",">"], "", $razonamientos[$i]));
                
                $db->query("INSERT INTO hissolicitudes(hsoempresa, hsosolicitud, hsofecha, hsousuario, hsonota) 
                VALUES ('$empresa', '$contrato', NOW(), '$digitador', '$aux_razonamiento');") or die($db->errorInfo()[2]);
            }
        }

        // Movimiento
        $db->query("UPDATE movimientos SET 
        movtercero = '$cedula', movestado = 'FACTURADO',
        movvalor = '$monto_contrato', movdescuento = '$descuento', movsaldo = '$saldo'  
        WHERE movempresa = '$empresa' AND movdocumento = '$factura' AND movprefijo = 'FV';")
        or die($db->errorInfo()[2]);

        // Detalle movimiento
        $db->query("DELETE FROM detsolicitudes WHERE detempresa = '$empresa' AND detsolicitud = '$contrato_original'");
        $db->query("DELETE FROM detmovimientos WHERE dmoempresa = '$empresa' AND dmonumero = '$factura' AND dmoprefijo = 'FV'");
        if($cantidad_productos > 0)
        {
            for($i = 0; $i < $cantidad_productos; $i++)
            {
                $aux_codigo = $producto_codigo[$i];
                $aux_cantidad = $producto_cantidad[$i];
                $aux_precio = $producto_precio[$i];
                $aux_costo = $db->query("SELECT procosto from productos WHERE proid = '$aux_codigo'");
                $aux_costo = $aux_costo->fetch(PDO::FETCH_ASSOC);
                if($aux_costo)
                    $aux_costo = $aux_costo['procosto'];
                else
                    $aux_costo = 0;
                    
                $total_costo = $aux_costo * $aux_cantidad;
                $total_precio = $aux_precio * $aux_cantidad;
    
                $db->query("INSERT INTO detsolicitudes (detsolicitud, detempresa, detproducto, detcantidad, detprecio) 
                VALUES ('$contrato', '$empresa', '$aux_codigo', $aux_cantidad, $aux_precio);") or die($db->errorInfo()[2]);
                // aquí cambiamos mov_numero por factura porque igual sigue siendo el mismo dato
                $db->query("INSERT INTO detmovimientos (dmoempresa, dmoprefijo, dmonumero, dmoproducto, dmocantidad, dmounitario, dmototal, dmoprecio, dmoptotal) VALUES 
                ('$empresa', 'FV', '$factura', '$aux_codigo', $aux_cantidad, $aux_costo, $total_costo, $aux_precio, $total_precio );") or die($db->errorInfo()[2]);
            }
        }

        // SEGUNDA PARTE

        // Creación de cartera
        // En caso de que el # de cuotas sea mayor a 0 entonces se genera una cartera
        if($ncuotas > 0)
        {
            $aux_cartera = $db->query("SELECT * FROM carteras WHERE carempresa='$empresa' AND carfactura='$factura'")->fetch(PDO::FETCH_ASSOC);
            if($aux_cartera)
            {
                $db->query("DELETE FROM carteras WHERE carempresa='$empresa' AND carfactura='$factura'");
                $db->query("DELETE FROM detcarteras WHERE dcaempresa='$empresa' AND dcafactura='$factura'");
            }

            $estado = "ACTIVA";
            $db->query("INSERT INTO carteras(carempresa, carfactura, carcliente, cartotal, carcuota, carsaldo, carncuota, carfecha, carestado, created_at)
            VALUES ('$empresa', '$factura', '$cedula', $neto, $cuota, $saldo, $ncuotas, NOW(), '$estado', NOW() );")
            or die($db->errorInfo()[2]);
        
            
            $fechacom = $fecha_cobro;
            // Creación de detalle cartera
            for($i = 1; $i <= $ncuotas; $i++)
            {
                $db->query("INSERT INTO detcarteras (dcaempresa, dcafactura, dcacuota, dcafecha, dcaestado) 
                VALUES ('$empresa', '$factura', $i, '$fechacom', 'ACTIVA')")
                or die($db->errorInfo()[2]);
				$fechacom = date('Y-m-d', strtotime($fechacom.' next month'));
            }

            // Aplicamos los pagos en caso de existir
            $cantidad_pagos = count($detalle_monto);
            $movimiento_nuevo = true;
            $comision = true;

            
            
            
            for($i = 0; $i < $cantidad_pagos; $i++)
            {
                // Numero del recibo
                $num_recibo = $detalle_recibo[$i];
                if($num_recibo == "" || $num_recibo == 0)
                {
                    // Obtenemos el ultimo recibo con numero negativo para poder asignar a todos aquellos pagos sin numero de recibo
                    $recibo_automatico = $db->query("select nextval('sec_factura_negativa') as factura")->fetch(PDO::FETCH_ASSOC);
                    if($recibo_automatico)
                        $recibo_automatico = $recibo_automatico['factura'];
                    else 
                        $recibo_automatico = -1;
                        
                    $num_recibo = $recibo_automatico;
                }

                $digitador = $_SESSION['id'];
                $comentario = "";
                $db->query("INSERT INTO infoextra 
                (comentario, digitador)
                VALUES ('$comentario', '$digitador');") or die($db->errorInfo()[2]);
                
                $id_info = $db->lastInsertId();

                $aux_cobrador = strtoupper($detalle_cobrador[$i]);
                pagar($db, $contrato, $empresa, $factura, $cedula,
                $cuota, 1, $detalle_monto[$i], 0, $detalle_fpago[$i], 1, $aux_cobrador, 
                $nombre, $num_recibo, $id_info,
                $movimiento_nuevo, $comision);
                
                $comision = false;
            }
        }
        $msj = "¡Contrato ".$contrato." creado exitosamente!";
        header("location:insertar.php?msj=$msj");
        exit();
    }
    
?>


<!doctype html>
<html>

<head>
    <title>AGREGAR CONTRATOS</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-not-form.php');
    ?>
    <style>
        #form input {
            text-transform: uppercase;
        }
        .divisor {
            margin-left: 5px;
            margin-right: 5px;
            margin-top: 15px;
            margin-bottom: 15px;
            padding-top: 10px;
            padding-bottom: 2px;
            background-color: #004593;
            color: #fff;
            border-radius: 8px;
        }
        .divisor h6 {
            font-weight: bold; 
        }
    </style>
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
    
    <link rel="stylesheet" href="<?php echo $r.'incluir/librerias/fontawesome/css/all.css'?>" />
    <script src="<?php echo $r.'incluir/librerias/fontawesome/js/all.js'?>"></script>

	<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>
    <script type="text/javascript">
        show_carga_modal = false;
        var $select_cobradores = "";
		$(document).ready(function() {
            input_fecha();
            $("input").prop("autocomplete","off");

            $('#form').validationEngine({
                onValidationComplete: function(form, status) {
                    if (status) {
                        return true;
                    }
                }
            });
            var ruta = 'ajax/cobradores_asesores.php';
            $select_cobradores += "<select class='sin-inicializar detalle-cobrador form-control validate[required] selectpicker' name='detalle-cobrador[]'  data-live-search='true' title='Seleccione un cobrador' >";
            $select_cobradores += "<option value=''></option>";
            $.get(ruta, function(res){
                res = JSON.parse(res);
                res.forEach(function(data){
                    nombre = "";
                    if(data.usunombre != "")
                        nombre += "/" + data.usunombre;
                    $select_cobradores += "<option value='"+ data.usuid +"'> "+ data.usuid + nombre +"</option>";
                });
                $select_cobradores += "</select>";
            });
        });
        function input_fecha()
        {
			$(".fecha").datepicker({
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
            });
        }
        
        
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="<?php echo $r ?>index.php">Principal</a>
				<div class="mapa_div"></div><a href="#">Cartera</a>
				<div class="mapa_div"></div><a href="#">Castigada</a>
				<div class="mapa_div"></div><a class="current">Castigar</a>
			</article>
			<article id="contenido">
				<form id="form" name="form-detalle" action="informacion_de_pago.php" method="post">
                    <div id="carouselExampleControls" class="carousel slide" data-ride="false" >
                        <div class="carousel-inner">
                            <div class="carousel-item" id="carousel1">
                                <?php require('carousel1.php') ?>
                                
                                <div class="row d-flex justify-content-center mt-5">
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-primary btn-block" onclick="CambioPag();">INFORMACIÓN DE PAGO</button>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item active" id="carousel2">
                                <?php require('carousel2.php') ?>
                                <div class="row mt-3">
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-info btn-block" onclick="CambioPag();">REGRESAR A CONTRATO</button>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="submit" class="btn btn-success btn-block" name="informacion-pago">GUARDAR</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <!-- Modal para ver razonamiento-->
                    <div class="modal fade" id="ver-razonamiento" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Ver Razonamientos</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <table class="table table-hover">
                                        <thead class="text-center">
                                            <th>Razonamiento</th>
                                            <th></th>
                                        </thead>
                                        <tbody id="t-razonamiento">
                                            <?php 
                                                for($i = 0; $i < $cantidad_razonamiento; $i++)
                                                {
                                                    echo "<tr>
                                                        <td><input type='text' name=razonamientos[] value='".$razonamientos[$i]."'></td>
                                                        <td><button type='button' class='btn btn-danger' onclick='remove_razonamiento(this);'>Eliminar</button></td>
                                                    </tr>";
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal para agregar razonamiento-->
                    <div class="modal fade" id="add-razonamiento" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Agregar Razonamiento</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <label for="">Describa el razonamiento</label>
                                    <textarea id="razonamiento" class="form-control not-w" rows="3"></textarea>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-success" data-dismiss="modal" onclick="add_razonamiento();">Guardar</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>
            </article>
        </article>
    </section>
</body>
<script>
    var prueba;

    function check(e) {
        tecla = (document.all) ? e.keyCode : e.which;

        //Tecla de retroceso para borrar, siempre la permite
        if (tecla == 8) {
            return true;
        }

        // Patron de entrada, en este caso solo acepta numeros y letras
        patron = /[A-Za-z0-9]/;
        tecla_final = String.fromCharCode(tecla);
        return patron.test(tecla_final);
    }


    function CambioPag()
    {
        if($("#carousel1").hasClass('active'))
        {
            if(!first_step_validation())
            {
                buscar_contrato();
            }
        }
        else
        {
            $("#carousel2").removeClass("active");
            $("#carousel1").addClass("active");
        }
    }

    $("#form").submit(function(e){
        
        if(incompleto == true)
        {
            $("#modal-recibos").modal("show");
            e.preventDefault();
        }
        else
        {
            if($("#form").validationEngine('validate'))
                carga();
        }
        
    });
    
    function buscar_contrato()
    {
        var original = $("#contrato-original").val();
        var contrato = $("#contrato").val();
        ruta = "ajax/contrato.php?contrato="+contrato+"&original="+original;
        $.get(ruta, function(res){
            if(JSON.parse(res))
            {
                update_total();
                $("#carousel1").removeClass("active");
                $("#carousel2").addClass("active");
            }
            else
            {
                $("#contrato").validationEngine('validate')
                $("#contrato").focus();
            }
        });
    }

    function first_step_validation()
    {
        var inputs = ["#cedula", "#fecha-contrato", "#nombre", "#asesor", ".producto-cantidad", ".producto-precio"];
        var inputs_ajax = ["#contrato"];
        for(var i = 0; i < inputs.length; i++)
        {
            cant_inputs = $(inputs[i]).length;
            //Si la cantidad de inputs es igual a 1 entonces
            if(cant_inputs == 1)
            {
                // ejecutamos la validacion individual
                if( $(inputs[i]).validationEngine('validate') )
                {
                    $(inputs[i]).focus();
                    return true;
                }
            }
            else
            {
                // Significa que hay mas de 1 input del mismo tipo
                for(j = 0; j < cant_inputs; j++)
                {
                    
                    if( $(inputs[i]).eq(j).validationEngine('validate') )
                    {
                        $(inputs[i]).eq(j).focus();
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    
    function update_total()
    {
        var cantidad_productos = $('.producto-condigo').length;
        var total = 0;
        for(var i = 0; i < cantidad_productos; i++)
        {
            var precio = $(".producto-precio").eq(i).val();
            var cantidad = $(".producto-cantidad").eq(i).val();
            total += (precio*cantidad);
        }
        $("#monto-contrato").val(total);
        calcular();
    }
    

</script>