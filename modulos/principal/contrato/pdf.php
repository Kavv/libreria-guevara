<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
require($r . 'incluir/fpdf/fpdf.php');
$contrato = $_GET['solid'];

$sql = "SELECT solicitudes.* , clientes.*, departamentos.*, ciudades.*, movimientos.movdescuento, infoextra.comentario FROM solicitudes
LEFT JOIN clientes ON cliid = solcliente
LEFT JOIN departamentos ON depid = soldepentrega
LEFT JOIN ciudades ON (ciuid = solciuentrega AND ciudepto = soldepentrega)
INNER JOIN movimientos ON (movprefijo = 'FV' AND movdocumento = solfactura)
LEFT JOIN infoextra on movinfo = infoextra.id
WHERE solid = '$contrato';";

$row = $db->query($sql)->fetch(PDO::FETCH_ASSOC);


// Obtenemos la moneda correspondiente
$moneda = $db->query("SELECT * FROM monedas WHERE espordefecto = true")->fetch(PDO::FETCH_ASSOC);
$simbolo = $moneda['simbolo'];


$fecha = $row['solfechafac'];
$fechaComoEntero = strtotime($fecha);

$year = date("y", $fechaComoEntero);
$month = date("m", $fechaComoEntero);
$day = date("d", $fechaComoEntero);

$nombre = "";
if($row['clinombre'] != "")
    $nombre .= $row['clinombre'];
if($row['clinom2'] != "")
    $nombre .= " " . $row['clinom2'];
if($row['cliape1'] != "")
    $nombre .= " " . $row['cliape1'];
if($row['cliape2'] != "")
    $nombre .= " " . $row['cliape2'];



$nombre = strupperEsp($nombre);

$pdf = new FPDF('P','mm',array(210,158));
$pdf->AddFont('LucidaConsole', '', 'lucon.php');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetTitle("Factura del contrato ".$contrato);
#Establecemos los márgenes izquierda, arriba y derecha:
$pdf->SetMargins(5,0);

$w_codigo = 16;
$w_cantidad = 16;
$w_producto = 64;
$w_precio = 18;
$w_total = 18;
$max_w = $w_codigo + $w_cantidad + $w_producto + $w_precio + $w_total;

//$margen_r = 8;
$margen_r = 0.1;

$font_size = 8;
$border = 0;

$pdf->SetFont('LucidaConsole', '', $font_size);
$pdf->ln(7);
// Día Mes Año
$pdf->Cell($margen_r, 5, '', 0, 0);
$pdf->Cell($max_w, 5, $day ."       " . $month ."       " . $year . "  ", $border, 1, 'R');

// Nombre y Cedula
$pdf->ln(7);
$pdf->Cell($margen_r, 5, '', 0, 0);
$pdf->Cell($max_w*0.66, 5, utf8_decode($nombre), $border, 0);
$pdf->Cell($max_w*0.33, 5, $row['solcliente'], $border, 1, 'R');

// Dir y Tel
$pdf->ln(6);
$pdf->Cell($margen_r, 5, '', 0, 0);
$pdf->Cell($max_w*0.66, 5, utf8_decode($row['depnombre']." / ".$row['ciunombre']), $border, 0);
$pdf->Cell($max_w*0.33, 5, utf8_decode($row['soltelentrega']), $border, 1, 'R');

// Direccion
$pdf->ln(1);
$direccion1 = substr($row['solentrega'],0, 76);
$direccion2 = substr($row['solentrega'],76, 76);
$direccion3 = substr($row['solentrega'],152, 76);

$pdf->Cell($margen_r, 4, '', 0, 0);
$pdf->Cell($max_w, 4, utf8_decode($direccion1), $border, 1);
$pdf->Cell($margen_r, 4, '', 0, 0);
$pdf->Cell($max_w, 4, utf8_decode($direccion2), $border, 1);
$pdf->Cell($margen_r, 4, '', 0, 0);
$pdf->Cell($max_w, 4, utf8_decode($direccion3), $border, 1);



$pdf->Cell($margen_r, 5, '', 0, 0);
$pdf->Cell(18, 5, '', 0, 0, 'C');
$pdf->Cell(18, 5, '', 0, 0, 'C');
$pdf->Cell(60, 5, '', 0, 0, 'C');
$pdf->Cell(18, 5, '', 0, 0, 'C');
$pdf->Cell(18, 5, '', 0, 1, 'C');

$productos = $db->query("SELECT detproducto, detcantidad, detprecio, pronombre FROM solicitudes INNER JOIN detsolicitudes ON detsolicitud = solid INNER JOIN productos on proid = detproducto WHERE solid = '$contrato';");
$cantidad_productos = $productos->rowCount();
$index_fila = 0;
$max_filas = 20;
//$pdf->SetFont('LucidaConsole', '', 8);
$pdf->SetFont('LucidaConsole', '', $font_size);



$subtotal = 0;
$truncar = false;
while($producto = $productos->fetch(PDO::FETCH_ASSOC))
{
    $p_id = $producto['detproducto'];
    $p_nombre = substr($producto['pronombre'], 0, 35);
    $p_cantidad = $producto['detcantidad'];
    $p_precio = $producto['detprecio'];
    $p_total = round(($p_precio * $p_cantidad), 2);

    if(!$truncar)
    {
        $pdf->Cell($margen_r, 4, '', 0, 0);
        $pdf->Cell($w_codigo, 4, $p_id, $border, 0, 'R');
        $pdf->Cell($w_cantidad, 4, $p_cantidad, $border, 0, 'R');
        $pdf->Cell($w_producto, 4, utf8_decode($p_nombre), $border, 0);
        $pdf->Cell($w_precio, 4, $simbolo . $p_precio, $border, 0, 'R');
        $pdf->Cell($w_total, 4, $simbolo . $p_total, $border, 1, 'R');
        $index_fila++;
    }
    $subtotal += $p_total;
    if($index_fila == $max_filas)
    {
        $truncar = true;
    }
}
while($index_fila < $max_filas)
{
    $pdf->Cell($margen_r, 4, '', 0, 0);
    $pdf->Cell($w_codigo, 4, "", $border, 0, 'C');
    $pdf->Cell($w_cantidad, 4, "", $border, 0, 'C');
    $pdf->Cell($w_producto, 4, "", $border, 0);
    $pdf->Cell($w_precio, 4, "", $border, 0, 'R');
    $pdf->Cell($w_total, 4, "", $border, 1, 'R');
    $index_fila++;
}
/* $pdf->Cell($margen_r, 4, '', 0, 0);
$pdf->Cell($w_codigo, 4, '100', $border, 0, 'R');
$pdf->Cell($w_cantidad, 4, '20', $border, 0, 'R');
$pdf->Cell($w_producto, 4, 'PRODUCTO FINAL', $border, 0);
$pdf->Cell($w_precio, 4, '200', $border, 0, 'R');
$pdf->Cell($w_total, 4, '400', $border, 1, 'R'); */

$pdf->SetFont('LucidaConsole', '', $font_size);
// Observaciones

$pdf->ln(2);
$observacion1 = substr($row['comentario'],0, 40);
$observacion2 = substr($row['comentario'],40, 55);
$observacion3 = substr($row['comentario'],95, 55);
$observacion4 = substr($row['comentario'],150, 55);

$w_observacion = $w_codigo + $w_cantidad + $w_producto;

$pdf->Cell($margen_r, 5, '', 0, 0);
$pdf->Cell($w_observacion, 5, "               " . utf8_decode($observacion1), $border, 0);
$pdf->Cell($w_precio, 5, "", $border, 0);
$pdf->Cell($w_total, 5, $simbolo. "$subtotal", $border, 1, 'R');

$descuento = $row['movdescuento'];
$pdf->Cell($margen_r, 5, '', 0, 0);
$pdf->Cell($w_observacion, 5, utf8_decode($observacion2), $border, 0);
$pdf->Cell($w_precio, 5, "", $border, 0);
$pdf->Cell($w_total, 5, $simbolo. "$descuento", $border, 1, 'R');

$iva = round(($subtotal-$descuento) * 0.15, 2);
$pdf->Cell($margen_r, 5, '', 0, 0);
$pdf->Cell($w_observacion, 5, utf8_decode($observacion3), $border, 0);
$pdf->Cell($w_precio, 5, "", $border, 0);
$pdf->Cell($w_total, 5, $simbolo. $iva, $border, 1, 'R');

$gran_total = round($subtotal - $descuento + $iva, 2);
$pdf->Cell($margen_r, 5, '', 0, 0);
$pdf->Cell($w_observacion, 5, utf8_decode($observacion4), $border, 0);
$pdf->Cell($w_precio, 5, "", $border, 0);
$pdf->Cell($w_total, 5, $simbolo. "$gran_total", $border, 1, 'R');


$detalle = $contado = $mayor = $credito = "";
if($row['solfpago'] == 1)
    $detalle = 4;
if($row['solfpago'] == 2)
    $contado = 4;
if($row['solfpago'] == 3)
    $mayor = 4;
if($row['solfpago'] == 4)
    $credito = 4;

$pdf->SetFont('ZapfDingbats','', $font_size);

$pdf->Cell($margen_r, 9, '', 0, 0);
$pdf->Cell($w_codigo, 9, "", $border, 0, 'C');
// METODO DE PAGO "AL DETALLE"
$pdf->Cell($w_cantidad, 9, $detalle, $border, 0, 'C');
// METODO DE PAGO "CONTADO"
$pdf->Cell($w_producto, 9, $contado, $border, 1, 'C');


$pdf->Cell($margen_r, 8, '', 0, 0);
$pdf->Cell($w_codigo, 8, "", $border, 0, 'C');
// METODO DE PAGO "AL POR MAYO"
$pdf->Cell($w_cantidad, 8, $mayor, $border, 0, 'C');
// METODO DE PAGO "CREDITO"
$pdf->Cell($w_producto, 8, $credito, $border, 1, 'C');


$pdf->Output("Factura del contrato ".$contrato , 'I');
?>