<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

?>

<!doctype html>
<html lang="es">

<head>
    <title>AGREGAR CONTRATOS</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
    ?>
    <style>
        #form input {
            text-transform: uppercase;
        }
        .divisor {
            margin-left: 5px;
            margin-right: 5px;
            margin-top: 15px;
            margin-bottom: 15px;
            padding-top: 10px;
            padding-bottom: 2px;
            background-color: #004593;
            color: #fff;
            border-radius: 8px;
        }
        .divisor h6 {
            font-weight: bold; 
        }
        .input-disabled {
            background :#e0e0e0;
            pointer-events: none;
        }
    </style>
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

	<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$(".fecha").datepicker({
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
            });
            
            $("input").prop("autocomplete","off");
		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="<?php echo $r ?>index.php">Principal</a>
				<div class="mapa_div"></div><a>Contrato</a>
				<div class="mapa_div"></div><a class="current">Agregar Nuevo</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="informacion_de_pago.php" method="post">
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12">
						<legend class="ui-widget ui-widget-header ui-corner-all">AGREGAR CONTRATO</legend>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <p for="" class="text-right">Estado: <span style="color: #004593;" class="font-weight-bold">ACTIVA</span></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label for="">Contrato</label>
                                <input id="contrato" type="text" name="contrato" class="form-control not-w validate[required, maxSize[15], ajax[ajaxContrato]]" onkeypress="return check(event)">
                            </div>
                            <div class="col-md-4">
                                <label for="">Cédula</label>
                                <div class="input-group mb-3">
                                    <input id="cedula" type="text" name="cedula" class="form-control not-w validate[required]" placeholder="000-000000-0000X" >
                                    <div class="input-group-append">
                                        <button class="btn btn-outline-primary" type="button" id="button-addon2" onclick="clientes();">Buscar</button>
                                    </div>
                                </div>    
                            </div>
                            <div class="col-md-4">
                                <label for="">Fecha Contrato</label>
							    <input type="text" class="form-control not-w fecha validate[required, custom[date]]" id="fecha-contrato" name="fecha-contrato" > 
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label for="">Nombre Completo</label>
                                <input id="nombre" type="text" name="nombre" class="form-control not-w validate[required]">
                            </div>
                            <div class="col-md-3">
                                <label for="">Teléfono 1</label>
                                <input id="telefono1" type="text" name="telefono1" class="form-control not-w validate[maxSize[40]]">
                            </div>
                            <div class="col-md-3">
                                <label for="">Teléfono 2</label>
                                <input id="telefono2" type="text" name="telefono2" class="form-control not-w validate[maxSize[40]]">
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-3">
                                <label for="">Departamento</label>
                                <select id="departamento" name="departamento" class="selectpicker validate[required]" data-live-search="true" title="Seleccione un departamento" data-width="100%">
                                <?php
                                    $query = $db->query("SELECT * FROM departamentos ORDER BY depnombre");
                                    while($row = $query->fetch(PDO::FETCH_ASSOC))
                                    {
                                        echo "<option value='". $row['depid'] ."'>". $row['depnombre'] ."</option>";
                                    }

                                ?>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label for="">Ciudad</label>
                                <select id="ciudad" name="ciudad" class="selectpicker validate[required]" data-live-search="true" title="Seleccione una ciurdad" data-width="100%">
                                <?php
                                    $query = $db->query("SELECT * FROM ciudades ORDER BY ciunombre");
                                    while($row = $query->fetch(PDO::FETCH_ASSOC))
                                    {
                                        echo "<option value='". $row['ciudepto'] . $row['ciuid'] ."'>". $row['ciunombre'] ."</option>";
                                    }

                                ?>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label for="">Lugar de trabajo</label>
                                <input id="trabajo" type="text" name="trabajo" class="form-control not-w validate[maxSize[250]]">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label for="">Dirección</label>
                                <textarea id="direccion" name="direccion" class="form-control not-w validate[maxSize[399]]" rows="2"></textarea>
                            </div>
                        </div>

                        <div class="row divisor">
                            <div class="col-md-12 text-center">
                                <h6>DATOS DEL CONYUGUE</h6>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <label for="">Nombre del Conyugue</label>
                                <input id="conyugue" type="text" name="conyugue" class="form-control not-w validate[maxSize[100]]">
                            </div>
                            <div class="col-md-3">
                                <label for="">Teléfono</label>
                                <input id="conyugue_tel" type="text" name="conyugue_telefono" class="form-control not-wv validate[maxSize[40]]">
                            </div>
                            <div class="col-md-3">
                                <label for="">Lugar de trabajo</label>
                                <input id="conyugue_trabajo" type="text" name="conyugue_trabajo" class="form-control not-w validate[maxSize[100]]">
                            </div>
                        </div>

                        <div class="row divisor">
                            <div class="col-md-12 text-center">
                                <h6>REFERENCIAS</h6>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label for="">Nombre Referencia 1</label>
                                <input id="referencia1" type="text" name="referencia1" class="form-control not-w validate[maxSize[100]]">
                            </div>
                            <div class="col-md-3">
                                <label for="">Teléfono 1</label>
                                <input id="ref1_tel1" type="text" name="ref1_tel1" class="form-control not-w validate[maxSize[15]]">
                            </div>
                            <div class="col-md-3">
                                <label for="">Teléfono 2</label>
                                <input id="ref1_tel2" type="text" name="ref1_tel2" class="form-control not-w validate[maxSize[10]]">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label for="">Nombre Referencia 2</label>
                                <input id="referencia2" type="text" name="referencia2" class="form-control not-w validate[maxSize[100]]">
                            </div>
                            <div class="col-md-3">
                                <label for="">Teléfono 1</label>
                                <input id="ref2_tel1" type="text" name="ref2_tel1" class="form-control not-w validate[maxSize[15]]">
                            </div>
                            <div class="col-md-3">
                                <label for="">Teléfono 2</label>
                                <input id="ref2_tel2" type="text" name="ref2_tel2" class="form-control not-w validate[maxSize[10]]">
                            </div>
                        </div>
                        <div class="row divisor">
                            <div class="col-md-12 text-center">
                                <h6>DATOS DEL ESTUDIANTE</h6>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <label for="">Nombre del estudiante</label>
                                <input type="text" name="estudiante" class="form-control not-w validate[maxSize[60]]">
                            </div>
                            <div class="col-md-3">
                                <label for="">Escuela</label>
                                <input type="text" name="escuela" class="form-control not-w validate[maxSize[50]]">
                            </div>
                            <div class="col-md-3">
                                <label for="">Grado</label>
                                <input type="text" name="grado" class="form-control not-w validate[maxSize[50]]">
                            </div>
                        </div>

                        <div class="row divisor">
                            <div class="col-md-12 text-center">
                                <h6>DETALLES</h6>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <label for="">Observaciones</label>
                                <input type="text" name="observaciones" class="form-control not-w">
                            </div>
                            <div class="col-md-6">
                                <label for="">Razonamiento</label>
                                <div class="btn-group col-md-12" role="group" aria-label="Basic example">
                                    <button type="button" class="btn btn-info btn-block mt-2" data-toggle="modal" data-target="#ver-razonamiento">Ver</button>
                                    <button type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#add-razonamiento">Agregar</button>
                                </div>
                            </div>
                        </div>

                        <div class="row divisor">
                            <div class="col-md-12 text-center">
                                <h6>PERSONAL ASIGNADO</h6>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <label for="">Asesor de Ventas</label>
                                <select id="asesor" name="asesor" class="selectpicker validate[required]" data-live-search="true" title="Seleccione un asesor" data-width="100%" >
                                <?php
                                    $query = $db->query("SELECT usuid, usunombre FROM usuarios WHERE usuasesor = 1 ORDER BY usunombre");
                                    while($row = $query->fetch(PDO::FETCH_ASSOC))
                                    {
                                        $codigo = $row['usuid'];
                                        $nombre = "";
                                        if(TRIM($row['usunombre']) != "")
                                            $nombre .= "/" . $row['usunombre'];
                                        echo "<option value='". $row['usuid'] ."'>$codigo $nombre</option>";
                                    }
                                ?>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label for="">Entregador</label>
                                <input  id="entregador" name="entregador" type="text" class="form-control validate[maxSize[30]]" placeholder="Ingrese al entregador">

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label for="">Cobrador</label>
                                <select id="cobrador" name="cobrador" class="selectpicker" data-live-search="true" title="Seleccione un cobrador" data-width="100%">
                                <?php
                                    $query = $db->query("SELECT usuid, usunombre FROM usuarios WHERE usurelacionista = 1 ORDER BY usunombre");
                                    while($row = $query->fetch(PDO::FETCH_ASSOC))
                                    {
                                        $codigo = $row['usuid'];
                                        $nombre = "";
                                        if(TRIM($row['usunombre']) != "")
                                            $nombre .= "/" . $row['usunombre'];
                                        echo "<option value='". $row['usuid'] ."'>$codigo $nombre</option>";
                                    }
                                ?>
                                </select>
                            </div>
                        </div>

                        <div class="row divisor">
                            <div class="col-md-12 text-center">
                                <h6>PRODUCTOS ENTREGADOS</h6>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-8">
                                <label for="">Lista de Productos</label>
                                <select id="producto" class="selectpicker mb-1 pb-3" data-live-search="true" title="Seleccione un producto" data-width="100%">
                                    <option value="">SELECCIONE</option>
                                    <?php
                                    //$qry = $db->query("SELECT * FROM productos WHERE prodesactivado = '0' AND proid NOT IN (SELECT detproducto FROM detsolicitudes WHERE detsolicitud = '$id' AND detempresa = '$empresa') AND protipo = 'PT' ORDER BY TRIM(pronombre)");
                                    $qry = $db->query("SELECT proid, pronombre, proprecio FROM productos WHERE prodesactivado = '0' AND protipo = 'PT'  ORDER BY proid;");
                                    while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
                                        $codigo = $row['proid'];
                                        $nombre = "";
                                        $precio = $row['proprecio'];
                                        $nombre_aux = $row['pronombre'];
                                        if($row['pronombre']!="")
                                            $nombre = "-" . $row['pronombre'];
                                        echo "<option value='$codigo' data-nombre='$nombre_aux' data-precio='$precio'>$codigo $nombre</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-4">

                                <div class="input-group mb-3">
                                    <label for="" style="visibility:hidden;">Agregar producto</label>
                                    <button type="button" class="btn btn-success" name="adiproducto" value="adiproducto" onclick="add_product();">Agregar a la lista</button>
                                </div>
                            </div>

                        </div>
                        <div class="row text-center mb-2">
                            <div class="col-md-12">
                                <h6><strong>Lista de productos entregados</strong></h6>
                            </div>
                        </div>
                        
                        <div id="lista-productos">
                        </div>
                    </fieldset>

                    <!-- Modal para ver razonamiento-->
                    <div class="modal fade" id="ver-razonamiento" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Ver Razonamientos</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <table class="table table-hover">
                                        <thead class="text-center">
                                            <th>Razonamiento</th>
                                            <th></th>
                                        </thead>
                                        <tbody id="t-razonamiento">

                                        </tbody>
                                    </table>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal para agregar razonamiento-->
                    <div class="modal fade" id="add-razonamiento" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Agregar Razonamiento</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <label for="">Describa el razonamiento</label>
                                    <textarea id="razonamiento" class="form-control not-w" rows="3"></textarea>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-success" data-dismiss="modal" onclick="add_razonamiento();">Guardar</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row d-flex justify-content-center">
                        <div class="col-md-6">
                            <button type="submit" name="guardar-contrato" class="btn btn-primary btn-block">GUARDAR</button>
                        </div>
                    </div>
                </form>
            </article>
        </article>
    </section>
	<?php require($r . 'incluir/src/loading.php'); ?>
    
	<?php
	if (isset($_GET['msj'])) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['msj'] . '</div>'; //VENTANA MODAL EXITOSO
	?>
</body>
<script>
    var prueba;

    function clientes()
    {
        var cedula = $("#cedula").val();
        var ruta = "ajax/cliente.php?cedula="+cedula;
        $("#loading").css("display", "block");
        $.get(ruta, function(res){
            res = JSON.parse(res);
            limpiar();
            if(res != 0)
            {
                var nombre = "";
                if(res.clinombre != "")
                    nombre += res.clinombre;
                if(res.clinom2 != "")
                    nombre += " " + res.clinom2;
                if(res.cliape1 != "")
                    nombre += " " + res.cliape1;
                if(res.cliape2 != "")
                    nombre += " " + res.cliape2;

                var codigo_ciudad = res.clidepresidencia.toString() + res.cliciuresidencia.toString(); 
                $("#nombre").val(nombre);
                $("#telefono1").val(res.clicelular);
                $("#telefono2").val(res.clitelresidencia);
                $("#departamento").val(res.clidepresidencia);
                $("#departamento").change();
                $("#ciudad").val(codigo_ciudad);
                $("#ciudad").change();
                $("#trabajo").val(res.cliempresa);
                $("#direccion").val(res.clidirresidencia);
                $("#conyugue").val(res.clinomfamiliar);
                $("#conyugue_tel").val(res.clitelfamiliar);
                $("#conyugue_trabajo").val(res.cliempfamiliar);
                $("#referencia1").val(res.clirefnombre1);
                $("#ref1_tel1").val(res.clireftelefono1);
                $("#ref1_tel2").val(res.clirefcelular1);
                $("#referencia2").val(res.clirefnombre2);
                $("#ref2_tel1").val(res.clireftelefono2);
                $("#ref2_tel2").val(res.clirefcelular2);
            }
            $("#loading").css("display", "none");
        });
    }
    function limpiar()
    {
        $("#nombre").val("");
        $("#telefono1").val("");
        $("#telefono2").val("");
        $("#departamento").val("");
        $("#ciudad").val("");
        $("#trabajo").val("");
        $("#direccion").val("");
        $("#conyugue").val("");
        $("#conyugue_tel").val("");
        $("#conyugue_trabajo").val("");
        $("#referencia1").val("");
        $("#ref1_tel").val("");
        $("#referencia2").val("");
        $("#ref2_tel").val("");
    }
    function add_razonamiento()
    {
        var razonamiento = $("#razonamiento").val();
        var tabla = $("#t-razonamiento");
        var lista = $("#lista-razonamiento");
        if(razonamiento != "")
        {
            var fila = 
            "<tr>" +
                "<td><input type='text' name=razonamientos[] value='"+ razonamiento +"'></td>" +
                "<td><button type='button' class='btn btn-danger' onclick='remove_razonamiento(this);'>Eliminar</button></td>" +
            "</tr>";
            tabla.append(fila);
            $("#ver-razonamiento").modal("show");
        }
    }
    function remove_razonamiento(element)
    {
        var fila = $(element);
        fila.parents("tr").remove();
    }
    function add_product()
    {
        var producto = $("#producto option:selected");
        prueba = producto;

        var codigo = producto.val();
        if($("#producto-"+codigo).length == 0)
        {
            var nombre = producto.data().nombre;
            var precio = producto.data().precio;

            var ruta = "ajax/add_producto.php?codigo="+codigo+"&nombre="+nombre+"&precio="+precio;
            $.get(ruta, function(res){
                var html = res;
                $("#lista-productos").append(html);
            }); 
        }
        else
        {
            alert("No se puede agregar porque ya esta agregado a la lista");
        }
    }
    function remove_product(element)
    {
        var fila = $(element);
        prueba = fila;
        fila.parents(".producto-item").remove();
    }
    function check(e) {
        tecla = (document.all) ? e.keyCode : e.which;

        //Tecla de retroceso para borrar, siempre la permite
        if (tecla == 8) {
            return true;
        }

        // Patron de entrada, en este caso solo acepta numeros y letras
        patron = /[A-Za-z0-9]/;
        tecla_final = String.fromCharCode(tecla);
        return patron.test(tecla_final);
    }

    $('#add-razonamiento').on('show.bs.modal', function (e) {
        $("#razonamiento").val("");
    });
    $('#add-razonamiento').on('shown.bs.modal', function (e) {
        $("#razonamiento").focus();
    });
</script>