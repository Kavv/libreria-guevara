
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12" id="fieldset-contrato">
						<legend class="ui-widget ui-widget-header ui-corner-all">CONTRATO</legend>
                        <div class="row mb-3">
                            <div class="col-md-12 justify-content-end d-flex">
                                <p class="mr-2">Estado:</p> 
                                <select class="form-control not-w col-md-4" name="cartera-estado">
                                    <?php 
                                        $estados = $db->query("SELECT * FROM estadoscartera");
                                        $exito = false;
                                        while($row_estado = $estados->fetch(PDO::FETCH_ASSOC))
                                        {
                                            if($cartera_estado == $row_estado['estadescripcion'])
                                            {
                                                echo '<option selected value="'. $row_estado['estadescripcion'] .'">'. $row_estado['estadescripcion'] .'</option>';
                                                $exito = true;
                                            }
                                            else
                                            {
                                                echo '<option value="'. $row_estado['estadescripcion'] .'">'. $row_estado['estadescripcion'] .'</option>';
                                            }
                                        }
                                        if($cartera_estado != "" && !$exito)
                                        {
                                            echo '<option selected value="'. $cartera_estado .'">'. $cartera_estado .'</option>';
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label for="">Contrato</label>
                                <input value="<?php echo $contrato ;?>" id="contrato-original" type="hidden" name="contrato-original">
                                <input value="<?php echo $factura ;?>" id="factura" type="hidden" name="factura">
                                <input value="<?php echo $contrato ;?>" id="contrato" type="text" name="contrato" class="form-control not-w validate[required, ajax[ajaxContrato]]" onkeypress="return check(event)">
                            </div>
                            <div class="col-md-4">
                                <label for="">Cédula</label>
                                <div class="input-group mb-3">
                                    <input value="<?php echo $cedula ;?>" id="cedula" type="text" name="cedula" class="form-control not-w validate[required]" placeholder="000-000000-0000X" >
                                    <div class="input-group-append">
                                        <button class="btn btn-outline-primary" type="button" id="button-addon2" onclick="clientes();">Buscar</button>
                                    </div>
                                </div>    
                            </div>
                            <div class="col-md-4">
                                <label for="">Fecha Contrato</label>
							    <input value="<?php echo $fecha_contrato ;?>" type="text" class="form-control not-w fecha validate[required]" id="fecha-contrato" name="fecha-contrato"/> 
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label for="">Nombre Completo</label>
                                <input value="<?php echo $nombre ;?>" id="nombre" type="text" name="nombre" class="form-control not-w validate[required]">
                            </div>
                            <div class="col-md-3">
                                <label for="">Teléfono 1</label>
                                <input value="<?php echo $telefono1 ;?>" id="telefono1" type="text" name="telefono1" class="form-control not-w">
                            </div>
                            <div class="col-md-3">
                                <label for="">Teléfono 2</label>
                                <input value="<?php echo $telefono2 ;?>" id="telefono2" type="text" name="telefono2" class="form-control not-w">
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-3">
                                <label for="">Departamento</label>
                                <select id="departamento" name="departamento" class="selectpicker" data-live-search="true" title="Seleccione un departamento" data-width="100%">
                                    <?php
                                        $query = $db->query("SELECT * FROM departamentos ORDER BY depnombre");
                                        while($row = $query->fetch(PDO::FETCH_ASSOC))
                                        {
                                            if($row['depid'] != $departamento)
                                                echo "<option value='". $row['depid'] ."'>". $row['depnombre'] ."</option>";
                                            else
                                                echo "<option selected value='". $row['depid'] ."'>". $row['depnombre'] ."</option>";
                                        }

                                    ?>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label for="">Ciudad</label>
                                <select id="ciudad" name="ciudad" class="selectpicker" data-live-search="true" title="Seleccione una ciurdad" data-width="100%">
                                    <?php
                                        $query = $db->query("SELECT * FROM ciudades ORDER BY ciunombre");
                                        while($row = $query->fetch(PDO::FETCH_ASSOC))
                                        {
                                            $code_full = $row['ciudepto'] . $row['ciuid'];
                                            if($code_full != $full_ciudad)
                                                echo "<option value='". $code_full ."'>". $row['ciunombre'] ."</option>";
                                            else
                                                echo "<option selected value='". $code_full ."'>". $row['ciunombre'] ."</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label for="">Lugar de trabajo</label>
                                <input value="<?php echo $trabajo ;?>" id="trabajo" type="text" name="trabajo" class="form-control not-w">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label for="">Dirección</label>
                                <textarea id="direccion" name="direccion" class="form-control not-w" rows="2"><?php echo $direccion ;?></textarea>
                            </div>
                        </div>

                        <div class="row divisor">
                            <div class="col-md-12 text-center">
                                <h6>DATOS DEL CONYUGUE</h6>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <label for="">Nombre del Conyugue</label>
                                <input value="<?php echo $conyugue ;?>" id="conyugue" type="text" name="conyugue" class="form-control not-w">
                            </div>
                            <div class="col-md-3">
                                <label for="">Teléfono</label>
                                <input value="<?php echo $conyugue_telefono ;?>" id="conyugue_tel" type="text" name="conyugue_telefono" class="form-control not-w">
                            </div>
                            <div class="col-md-3">
                                <label for="">Lugar de trabajo</label>
                                <input value="<?php echo $conyugue_trabajo ;?>" id="conyugue_trabajo" type="text" name="conyugue_trabajo" class="form-control not-w">
                            </div>
                        </div>

                        
                        <div class="row divisor">
                            <div class="col-md-12 text-center">
                                <h6>REFERENCIAS</h6>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label for="">Nombre Referencia 1</label>
                                <input value="<?php echo $referencia1 ;?>" id="referencia1" type="text" name="referencia1" class="form-control not-w">
                            </div>
                            <div class="col-md-3">
                                <label for="">Teléfono 1</label>
                                <input value="<?php echo $ref1_tel1 ;?>" id="ref1_tel1" type="text" name="ref1_tel1" class="form-control not-w">
                            </div>
                            <div class="col-md-3">
                                <label for="">Teléfono 2</label>
                                <input value="<?php echo $ref1_tel2 ;?>" id="ref1_tel2" type="text" name="ref1_tel2" class="form-control not-w">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label for="">Nombre Referencia 2</label>
                                <input value="<?php echo $referencia2 ;?>" id="referencia2" type="text" name="referencia2" class="form-control not-w">
                            </div>
                            <div class="col-md-3">
                                <label for="">Teléfono 1</label>
                                <input value="<?php echo $ref2_tel1 ;?>" id="ref2_tel1" type="text" name="ref2_tel1" class="form-control not-w">
                            </div>
                            <div class="col-md-3">
                                <label for="">Teléfono 2</label>
                                <input value="<?php echo $ref2_tel2 ;?>" id="ref2_tel2" type="text" name="ref2_tel2" class="form-control not-w">
                            </div>
                        </div>

                        <div class="row divisor">
                            <div class="col-md-12 text-center">
                                <h6>DATOS DEL ESTUDIANTE</h6>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <label for="">Nombre del estudiante</label>
                                <input value="<?php echo $estudiante ;?>" type="text" name="estudiante" class="form-control not-w">
                            </div>
                            <div class="col-md-3">
                                <label for="">Escuela</label>
                                <input value="<?php echo $escuela ;?>" type="text" name="escuela" class="form-control not-w">
                            </div>
                            <div class="col-md-3">
                                <label for="">Grado</label>
                                <input value="<?php echo $grado ;?>" type="text" name="grado" class="form-control not-w">
                            </div>
                        </div>

                        <div class="row divisor">
                            <div class="col-md-12 text-center">
                                <h6>DETALLES</h6>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <label for="">Observaciones</label>
                                <input value="<?php echo $observaciones ;?>" type="text" name="observaciones" class="form-control not-w">
                            </div>
                            <div class="col-md-6">
                                <label for="">Razonamiento</label>
                                <div class="btn-group col-md-12" role="group" aria-label="Basic example">
                                    <button type="button" class="btn btn-info btn-block mt-2" data-toggle="modal" data-target="#ver-razonamiento">Ver</button>
                                    <button type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#add-razonamiento">Agregar</button>
                                </div>
                            </div>
                        </div>

                        <div class="row divisor">
                            <div class="col-md-12 text-center">
                                <h6>PERSONAL ASIGNADO</h6>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <label for="">Asesor de Ventas</label>
                                <select id="asesor" name="asesor" class="selectpicker validate[required]" data-live-search="true" title="Seleccione un asesor" data-width="100%">
                                <?php
                                    $query = $db->query("SELECT usuid, usunombre FROM usuarios WHERE usuasesor = 1 ORDER BY usunombre");
                                    while($row = $query->fetch(PDO::FETCH_ASSOC))
                                    {
                                        $codigo = $row['usuid'];
                                        $nombre = "";
                                        if(TRIM($row['usunombre']) != "")
                                            $nombre .= "/" . $row['usunombre'];

                                        if($row['usuid'] != $asesor)
                                            echo "<option value='". $row['usuid'] ."'>$codigo $nombre</option>";
                                        else
                                            echo "<option selected value='". $row['usuid'] ."'>$codigo $nombre</option>";
                                    }
                                ?>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label for="">Entregador</label>
                                <input  id="entregador" name="entregador" type="text" class="form-controll" placeholder="Ingrese al entregador" value="<?php echo $entregador ?>">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label for="">Cobrador</label>
                                <select id="cobrador" name="cobrador" class="selectpicker" data-live-search="true" title="Seleccione un cobrador" data-width="100%">
                                <?php
                                    $query = $db->query("SELECT usuid, usunombre FROM usuarios WHERE usurelacionista = 1 ORDER BY usunombre");
                                    while($row = $query->fetch(PDO::FETCH_ASSOC))
                                    {
                                        $codigo = $row['usuid'];
                                        $nombre = "";
                                        if(TRIM($row['usunombre']) != "")
                                            $nombre .= "/" . $row['usunombre'];
                                            
                                        if($row['usuid'] != $cobrador)
                                            echo "<option value='". $row['usuid'] ."'>$codigo $nombre</option>";
                                        else
                                            echo "<option selected value='". $row['usuid'] ."'>$codigo $nombre</option>";
                                    }
                                ?>
                                </select>
                            </div>
                        </div>

                        <div class="row divisor">
                            <div class="col-md-12 text-center">
                                <h6>PRODUCTOS ENTREGADOS</h6>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-8">
                                <label for="">Lista de Productos</label>
                                <select id="producto" class="selectpicker mb-1 pb-3" data-live-search="true" title="Seleccione un producto" data-width="100%">
                                    <option value="">SELECCIONE</option>
                                    <?php
                                    //$qry = $db->query("SELECT * FROM productos WHERE prodesactivado = '0' AND proid NOT IN (SELECT detproducto FROM detsolicitudes WHERE detsolicitud = '$id' AND detempresa = '$empresa') AND protipo = 'PT' ORDER BY TRIM(pronombre)");
                                    $qry = $db->query("SELECT proid, pronombre, proprecio FROM productos WHERE prodesactivado = '0' AND protipo = 'PT'  ORDER BY proid;");
                                    while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
                                        $codigo = $row['proid'];
                                        $nombre = "";
                                        $precio = $row['proprecio'];
                                        $nombre_aux = $row['pronombre'];
                                        if($row['pronombre']!="")
                                            $nombre = "-" . $row['pronombre'];
                                        echo "<option value='$codigo' data-nombre='$nombre_aux' data-precio='$precio'>$codigo $nombre</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-4">

                                <div class="input-group mb-3">
                                    <label for="" style="visibility:hidden;">Agregar producto</label>
                                    <button type="button" class="btn btn-success" name="adiproducto" value="adiproducto" onclick="add_product();">Agregar a la lista</button>
                                </div>
                            </div>

                        </div>
                        <div class="row text-center mb-2">
                            <div class="col-md-12">
                                <h6><strong>Lista de productos entregados</strong></h6>
                            </div>
                        </div>
                        
                        <div id="lista-productos">
                            
                        <?php 
                            while($producto = $productos->fetch(PDO::FETCH_ASSOC))
                            {
                            ?>
                                <div class="row producto-item" id="producto-<?php echo $producto['detproducto'];?>">
                                    <div class="col-md-2">
                                        <label for="">Código</label>
                                        <input name="producto-codigo[]" type="text" class="producto-condigo form-control" value="<?php echo $producto['detproducto'];?>" readonly />
                                    </div>
                                    <div class="col-md-4">
                                        <label for="">Producto</label>
                                        <input name="producto-nombre[]" type="text" class="producto-nombre form-control" value="<?php echo $producto['pronombre'];?>" readonly />
                                    </div>
                                    <div class="col-md-3">
                                        <label for="">Precio Unidad</label>
                                        <input name="producto-precio[]" type="text" class="input-disabled producto-precio form-control validate[min[0], custom[number], required]" value="<?php echo $producto['detprecio'];?>" />
                                    </div>
                                    <div class="col-md-2">
                                        <label for="">Cantidad</label>
                                        <input name="producto-cantidad[]" type="text" class="producto-cantidad form-control validate[min[0], custom[integer],required]" value="<?php echo $producto['detcantidad'];?>" />
                                    </div>
                                    <div class="col-md-1">
                                        <label for=""></label>
                                        <button type="button" class="btn btn-danger" onclick="remove_product(this);">-</button>
                                    </div>
                                </div>
                            <?php
                            }
                            ?>
                        </div>
                        
                    </fieldset>

<script>
                        
    function clientes()
    {
        var cedula = $("#cedula").val();
        var ruta = "ajax/cliente.php?cedula="+cedula;
        $.get(ruta, function(res){
            res = JSON.parse(res);
            limpiar();
            if(res != 0)
            {
                var nombre = "";
                if(res.clinombre != "")
                    nombre += res.clinombre;
                if(res.clinom2 != "")
                    nombre += " " + res.clinom2;
                if(res.cliape1 != "")
                    nombre += " " + res.cliape1;
                if(res.cliape2 != "")
                    nombre += " " + res.cliape2;

                var codigo_ciudad = res.clidepresidencia.toString() + res.cliciuresidencia.toString(); 
                $("#nombre").val(nombre);
                $("#telefono1").val(res.clicelular);
                $("#telefono2").val(res.clitelresidencia);
                $("#departamento").val(res.clidepresidencia);
                $("#departamento").change();
                $("#ciudad").val(codigo_ciudad);
                $("#ciudad").change();
                $("#trabajo").val(res.cliempresa);
                $("#direccion").val(res.clidirresidencia);
                $("#conyugue").val(res.clinomfamiliar);
                $("#conyugue_tel").val(res.clitelfamiliar);
                $("#conyugue_trabajo").val(res.cliempfamiliar);
                $("#referencia1").val(res.clirefnombre1);
                $("#ref1_tel1").val(res.clireftelefono1);
                $("#ref1_tel2").val(res.clirefcelular1);
                $("#referencia2").val(res.clirefnombre2);
                $("#ref2_tel1").val(res.clireftelefono2);
                $("#ref2_tel2").val(res.clirefcelular2);
            }
        });
    }
    function limpiar()
    {
        $("#nombre").val("");
        $("#telefono1").val("");
        $("#telefono2").val("");
        $("#departamento").val("");
        $("#ciudad").val("");
        $("#trabajo").val("");
        $("#direccion").val("");
        $("#conyugue").val("");
        $("#conyugue_tel").val("");
        $("#conyugue_trabajo").val("");
        $("#referencia1").val("");
        $("#ref1_tel").val("");
        $("#referencia2").val("");
        $("#ref2_tel").val("");
    }
    function add_razonamiento()
    {
        var razonamiento = $("#razonamiento").val();
        var tabla = $("#t-razonamiento");
        var lista = $("#lista-razonamiento");
        if(razonamiento != "")
        {
            var fila = 
            "<tr>" +
                "<td><input type='text' name=razonamientos[] value='"+ razonamiento +"'></td>" +
                "<td><button type='button' class='btn btn-danger' onclick='remove_razonamiento(this);'>Eliminar</button></td>" +
            "</tr>";
            tabla.append(fila);
            $("#ver-razonamiento").modal("show");
        }
    }
    function remove_razonamiento(element)
    {
        var fila = $(element);
        fila.parents("tr").remove();
    }
    function add_product()
    {
        var producto = $("#producto option:selected");
        prueba = producto;

        var codigo = producto.val();
        if($("#producto-"+codigo).length == 0)
        {
            var nombre = producto.data().nombre;
            var precio = producto.data().precio;

            var ruta = "ajax/add_producto.php?codigo="+codigo+"&nombre="+nombre+"&precio="+precio;
            $.get(ruta, function(res){
                var html = res;
                $("#lista-productos").append(html);
            }); 
        }
        else
        {
            alert("No se puede agregar porque ya esta agregado a la lista");
        }
    }
    function remove_product(element)
    {
        var fila = $(element);
        prueba = fila;
        fila.parents(".producto-item").remove();
    }
</script>