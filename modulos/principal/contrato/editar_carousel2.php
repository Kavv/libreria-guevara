
<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12" id="fieldset-infopago">
    <legend class="ui-widget ui-widget-header ui-corner-all">INFORMACIÓN DE PAGO</legend>
    <div class="row">
        <div class="col-md-4">
            <label for=""><spam class="text-danger">*</spam>Fecha de entrega</label>
            <input value="<?php echo $fentrega;?>" type="text" class="calcular form-control not-w fecha validate[required, custom[date]]" id="entrega" name="entrega"/> 
        </div>
        <div class="col-md-4">
            <label for=""><spam class="text-danger">*</spam>Fecha de cobro</label>
            <input value="<?php echo $fcobro;?>" type="text" class="calcular form-control not-w fecha validate[required, custom[date]]" id="fecha-cobro" name="fecha-cobro"/> 
        </div>
        <div class="col-md-4">
            <label for=""><spam class="text-danger">*</spam>Fecha (cobrar)</label>
            <input value="<?php echo $cobrar;?>" type="text" class="calcular form-control not-w validate[required, custom[integer, required]]" id="cobro" name="cobro"/> 
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <label for=""><spam class="text-danger">*</spam>Monto del contrato</label>
            <input value="<?php echo $monto_contrato;?>" type="text" class="calcular form-control not-w validate[required, custom[number, required]]" id="monto-contrato" name="monto-contrato" value="<?php echo $monto_contrato; ?>"/> 
        </div>
        <div class="col-md-3">
            <label for="">Prima</label>
            <input value="<?php echo $prima;?>" type="text" class="calcular form-control not-w validate[custom[number]]" id="prima" name="prima"/> 
        </div>
        <div class="col-md-3">
            <label for=""><spam class="text-danger">*</spam>Neto</label>
            <input value="<?php echo $neto;?>" type="text" class="calcular form-control not-w validate[required, custom[number]]" id="neto" name="neto" value="<?php echo $monto_contrato; ?>"/> 
        </div>
        <div class="col-md-3">
            <label for="">Descuento</label>
            <input value="<?php echo $descuento;?>" type="text" class="calcular form-control not-w validate[custom[number]]" id="descuento" name="descuento"/> 
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <label for=""><spam class="text-danger">*</spam>Número de cuotas</label>
            <input value="<?php echo $ncuotas;?>" type="text" class="calcular form-control not-w validate[required, custom[integer], min[1]]" id="ncuotas" name="ncuotas"/> 
        </div>
        <div class="col-md-3">
            <label for=""><spam class="text-danger">*</spam>Monto cuotas</label>
            <div class="input-group mb-3">
            <input value="<?php echo $cuota;?>" type="text" class="form-control not-w validate[custom[number], required]" id="cuota" name="cuota"/> 
                <div class="input-group-append">
                    <button type="button" class="btn btn-primary" onclick="calcular(true)" style="z-index: 0;"><i class="fas fa-calculator"></i></button>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <label for="">Saldo pendiente</label>
            <input value="<?php echo $saldo;?>" type="text" class="form-control not-w" id="saldo" name="saldo" readonly/> 
        </div>
        <div class="col-md-3">
            <label for="" class="text-danger">Mora acumulada 5% x mes</label>
            <input type="text" class="form-control not-w" readonly id="mora" name="mora"/> 
        </div>
    </div>

    <div class="row mt-3">
        <div class="col-md-8">
            <div class="row" style="visibility: hidden;height: 0;">
                <div class="col-md-4">
                    <label for=""><spam class="text-danger">*</spam>Cuenta</label>
                    <select class="form-control validate[required, custom[number], min[0]]" value="1" id="cuenta" name="cuenta" id="">
                        <?php 
                            $v_cuestas = ["A","B","C"];
                            $limit_cuenta = count($v_cuestas)+2;
                            for($index_cuenta = 2; $index_cuenta < $limit_cuenta; $index_cuenta++)
                            {
                                $letra = $v_cuestas[$index_cuenta-2];
                                if($index_cuenta == $cuenta)
                                    echo "<option selected value='$index_cuenta'>$letra</option>";
                                else
                                    echo "<option value='$index_cuenta'>$letra</option>";

                            }
                        ?>
                    </select>
                </div>
                <div class="col-md-4">
                    <label for="">Salida</label>
                    <input value="<?php echo $salida;?>" type="text" class="form-control validate[custom[number]]" id="salida" name="salida">
                </div>
            </div>
            <div class="row mt-4">
                <button type="button" class="btn btn-info btn-block" data-toggle="modal" data-target="#modal-recibos">VER/AGREGAR RECIBOS PAGADOS</button>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row">
                <div class="col-md-12">
                    <label for="">Abonado en recibos</label>
                    <input readonly type="text" class="form-control not-w" id="abonado-recibo" value="0"/> 
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label for="">Hoy es</label>
                    <input readonly type="text" class="form-control fecha not-w" id="hoy" value="<?php echo date("Y/m/d");?>"/> 
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label for="">Los días desde firmado</label>
                    <input readonly type="text" class="form-control not-w" id="dias-firmado"/> 
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label for="">Su contrato es por días</label>
                    <input readonly type="text" class="form-control not-w" id="duracion-contrato"/> 
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label for="">Su cuota por dia es de</label>
                    <input readonly type="text" class="form-control not-w" id="dia-cuota"/> 
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label for="">Su ultimo pago será</label>
                    <input readonly type="text" class="form-control not-w" id="ultimo-pago"/> 
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label for="">Los días que ha pagado son</label>
                    <input readonly type="text" class="form-control not-w" id="dias-pagados"/> 
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label for="">Los días que tiene atrasado son</label>
                    <input readonly type="text" class="form-control not-w" id="dias-atrasados"/> 
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label for="">La cuota para estar al día es</label>
                    <input readonly type="text" class="form-control not-w" id="aldia"/> 
                </div>
            </div>
            
        </div>
    </div>

</fieldset>


<!-- Modal para agregar recibos-->
<div class="modal fade" id="modal-recibos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document" style="max-width: 95%;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Recibos Cobrados</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="overflow: auto;  min-height: 35em;">

                <table id="tabla-recibos" class="table table-hover" style="min-width: 700px;">
                    <thead>
                        <tr>
                            <th style="width:5%; text-align:center;">#</th>
                            <th style="width:15%;"><spam class="text-danger">*</spam>Recibo</th>
                            <th style="width:20%;"><spam class="text-danger">*</spam>F. Pago</th>
                            <th style="width:20%;"><spam class="text-danger">*</spam>Monto</th>
                            <th style="width:20%;"><spam class="text-danger">*</spam>Cobrador</th>
                            <th style="width:10%;"></th>
                        </tr>
                    </thead>
                    <tbody id="tb-recibo">
                        <!-- AGREGAR LOS RECIBOS PAGADOS -->
                        <?php 
                            $pagos = $db->query("SELECT * FROM solicitudes INNER JOIN movimientos ON (movprefijo = 'RC' AND movdocumento = solfactura) WHERE solid = '$contrato' ORDER BY movfecha asc");
                            $index_pago = 1;
                            while($pago = $pagos->fetch(PDO::FETCH_ASSOC))
                            {
                                echo 
                                "<tr class='recibo-item'>" .
                                    "<td style='text-align:center;'>$index_pago</td>" .
                                    "<td>
                                        <input value='". $pago['movnumero'] ."' type='hidden' name='detalle-original[]'>
                                        <input value='". $pago['movnumero'] ."' class='num_recibo form-control validate[custom[integer], ajax[ajaxRecibo]]' name='detalle-recibo[]' onkeypress='return check(event)'>
                                    </td>" .
                                    "<td><input value='". $pago['movfecha'] ."' class='detalle-fpago form-control fecha validate[required, custom[date]]' name='detalle-fpago[]'></td>" .
                                    "<td><input value='". $pago['movvalor'] ."' class='monto-recibo form-control validate[required, custom[number], min[0]]' name='detalle-monto[]'></td>" .
                                    "<td class='cobrador-existente'>".
                                        "<input type='hidden' class='codigo-cobrador-existente' value='" . $pago['movcobrador'] ."'>" .
                                    "</td>" .
                                    "<td></td>" .
                                "</tr>";
                                $index_pago++;
                            }
                        ?>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" onclick="add_recibo();">AGREGAR PAGO</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">LISTO</button>
            </div>
        </div>
    </div>
</div>


<script>
    function info_extra()
    {
        // Nota siempre redonder a 2 decimales

        // Los días que han pasado desde la firma 
        // (Se utiliza la fecha de cobro(solcompromiso) como punto de inicio y la fecha actual fecha de fin)
        var diff = dias();
        // carcuotas
        var ncuota = $("#ncuotas").val();
         
        // Duración total en días del contrato
        var duracion = 30.5 * ncuota;
        // cartotal
        var neto = $("#neto").val();
        var cuota_dia = 0;
        // Total de lo que se ha pagado (sumatoria de detcarteras o sumatoria de mov RC o 
        // tambien podria ser soltotal - carsaldo)
        var saldo_pagado = $("#abonado-recibo").val();
        var dias_pagados = 0;
        if(duracion > 0)
        {
            // Lo que se pagaria por día
            cuota_dia = neto / duracion;
            // Los días que se han pagado
            dias_pagados = saldo_pagado / cuota_dia;
        }

        var aux_fcobro = $("#fecha-cobro").val().replace("-", "/");
        var f_cobro = new Date(aux_fcobro);
        // Se le suman los días de duración del contrato a la fecha de cobro que se utiliza como fecha de inicio
        var aux = new Date(f_cobro.setDate(f_cobro.getDate() + duracion));
        var ultimo_pago = "";
        // Fecha del ultimo pago
        if(aux > 0)
            ultimo_pago = aux.getFullYear()+ "-" + (aux.getMonth() +1) + "-" + aux.getDate();
        

        // Ajuste
        cuota_dia = (cuota_dia.toFixed(2) * 1);

        $("#dias-firmado").val(diff);
        $("#duracion-contrato").val(duracion);
        $("#dia-cuota").val(cuota_dia);
        $("#ultimo-pago").val(ultimo_pago);
        $("#dias-pagados").val(dias_pagados.toFixed(2));

        // Días que lleva atrasado
        var dias_atrasados = diff - dias_pagados;
        if(dias_atrasados < 0)
            dias_atrasados = 0;
        $("#dias-atrasados").val(dias_atrasados.toFixed(2));

        var aldia = dias_atrasados * cuota_dia;

        $("#aldia").val(aldia.toFixed(2)*1);

        var saldo = $("#saldo").val(); 
        // Mora en base al saldo actual (carsaldo)
        var mora = ((dias_atrasados/30.5) * (saldo * 0.05)).toFixed(2) * 1;
        $("#mora").val(mora);
    }
    function dias()
    {
        var f0 = $("#fecha-cobro").val().replace("-", "/");
        var f1 = new Date(f0);
        var fecha_contrato = new Date (f1.getFullYear()+ "/" + (f1.getMonth() +1) + "/" + f1.getDate());
        fecha_contrato = fecha_contrato.getTime();
        var diff = 0;
        if(fecha_contrato > 0)
        {
            var hoy = new Date($("#hoy").val()).getTime();
    
            diff = hoy - fecha_contrato;
            diff = diff/(1000*60*60*24);
            
            //ajuste
            if(diff >= 0)
                diff += 1;
        }
        return diff;
    }
    $(".calcular").keyup(function(){
        console.log("keyup");
        calcular();
        
    });

    $(".calcular").change(function(){
        console.log("change");
        calcular();
    });

    function calcular(calcular_cuota = false)
    {
        
        var monto = $("#monto-contrato").val();
        var prima = $("#prima").val(); 
        var descuento = $("#descuento").val();
        var ncuotas = $("#ncuotas").val(); 

        var $neto = $("#neto"); 
        var $cuota = $("#cuota"); 
        var $saldo = $("#saldo"); 

        var neto = (monto - prima);
        $neto.val(neto);
        if(ncuotas != "")
        {
            var cuota = 0;
            if(ncuotas > 0)
            {
                var total_final = neto - descuento;
                // Si quieres que se aplique la formula restando el descuento cambiar neto por total_final
                //cuota = (neto/ncuotas).toFixed(2);
                cuota = (total_final/ncuotas).toFixed(2);
            }
            else
                cuota = neto.toFixed(2); 
            
            // En caso de no querer calcular automaticamente la cuota debe comentariar la sig linea
            if(calcular_cuota)
                $cuota.val(cuota);
        }

        var abonado = $("#abonado-recibo").val();
        var aux_saldo = (neto - descuento - abonado);
        $saldo.val(aux_saldo);
        info_extra();

    }

    function add_recibo()
    {
        html = "<tr class='recibo-item'>" +
            "<td></td>" +
            "<td><input class='num_recibo form-control validate[custom[integer], ajax[ajaxRecibo]]' name='detalle-recibo[]' onkeypress='return check(event)' autocomplete='off'></td>" +
            "<td><input class='detalle-fpago form-control fecha validate[required, custom[date]]' name='detalle-fpago[]' autocomplete='off'></td>" +
            "<td><input class='monto-recibo form-control validate[required, custom[number], min[0]]' name='detalle-monto[]' autocomplete='off'></td>" +
            "<td>" +
                $select_cobradores +
            "</td>" +

            "<td><button type='button' class='btn btn-danger' onclick='remove_recibo(this);'>Eliminar</button></td>" +
        "</tr>";

        $("#tb-recibo").append(html);
        input_fecha();
        $(".sin-inicializar").selectpicker("refresh");
        $(".sin-inicializar").removeClass("sin-inicializar");
    } 

    function remove_recibo(element)
    {
        var fila = $(element);
        prueba = fila;
        fila.parents(".recibo-item").remove();
    }
    var base_abonado = parseFloat($("#abonado-recibo").val());
    var incompleto = false;
    var numero_recibos = [];
    function actualizar_saldo()
    {
        var total = 0;
        var valor = 0;
        var cant = $(".monto-recibo").length;
        
        incompleto = false;
        numero_recibos = [];
        for (i = 0; i<cant; i++)
        {
            valor = parseFloat($(".monto-recibo").eq(i).val());
            var num_recibo = $(".num_recibo").eq(i).val();
            var fpago = $(".detalle-fpago").eq(i).val();
            var detalle_cobrador = $("select.detalle-cobrador").eq(i).val();

            // Llenamos un arreglo con los # de recibos para verificar si hay alguno repetido
            numero_recibos.push(num_recibo);

            if( (valor >= 0) && (fpago != "") && (detalle_cobrador != "" || first_time) )
                total += valor;
            else
            {
                incompleto = true;
                
                $(".num_recibo").eq(i).validationEngine('validate');
                $(".detalle-fpago").eq(i).validationEngine('validate');
                $(".monto-recibo").eq(i).validationEngine('validate');
                $("select.detalle-cobrador").eq(i).validationEngine('validate');
            }
        }
        abonado = base_abonado + total;
        $("#abonado-recibo").val(abonado);
        calcular();
    }
    var validation_modal_status = false;
    var modal_preventdefault = true;
    
    const factura = $("#factura").val();
    $('#modal-recibos').on('hide.bs.modal', function (e) {

        carga();
        var inputs_recibo = $(".num_recibo");
        var cantidad = inputs_recibo.length;
        actualizar_saldo();
        if(cantidad > 0)
        {
            // Verificamos que no existan # de recibos repetidos en la interfaz
            if(!recibo_repetido())
            {
                if(!validation_modal_status)
                {
                    console.log("se ejecuta pre");
                    e.preventDefault();
                    modal_preventdefault = true;
                }
                else
                {
                    modal_preventdefault = false;
                    console.log("No ejecuta pre");
    
                }
                if(modal_preventdefault)
                {
                    
                    if(incompleto == true)
                    {
                        e.preventDefault();
                        $("#carga").dialog("close");
                    }
                    else
                    {
                        var good = true;
                        var repetido = false;
                        var ajax_index = 0;
                        
                        for(i = 0; i < cantidad; i++)
                        {
                            var recibo = inputs_recibo.eq(i).val();
                            ruta = "ajax/recibo.php?recibo="+recibo+"&factura="+factura;
                            $.get(ruta, function(res){
                                console.log("iteración: "+ajax_index);
                                //Si es false entonces el recibo no es valido
                                if(!JSON.parse(res))
                                {
                                    validation_modal_status = false;
                                    good = false;
                                    console.log("Se deberia quedar");
                                    $(".num_recibo").eq(ajax_index).validationEngine('validate');
                                    
                                }
                                else
                                {
                                    console.log("Se deberia ocultar- cantidad:"+cantidad+"-index:"+ajax_index+"good"+good);
                                    if(ajax_index == (cantidad-1) && good)
                                    {
                                        if(modal_preventdefault)
                                        {
                                            console.log("entra");
                                            validation_modal_status = true;
                                            console.log("Entra en la cosa esa");
                                            $("#modal-recibos").modal("hide");
                                            $("#carga").dialog("close");
                                        }
                                    }
                                }
                                if(ajax_index == (cantidad-1) && !good)
                                    $("#carga").dialog("close");
                                ajax_index++;
                            });
                        }
                    }
                }
            }
            else
            {
                e.preventDefault();
                $("#carga").dialog("close");
                alert("Error, hay núrmeros de recibos repetidos.")
            }
        }
        else
        {
            $("#carga").dialog("close");
        }
    });

    $('#modal-recibos').on('show.bs.modal', function (e) {
        validation_modal_status = false;
    });

    function recibo_repetido()
    {
        var duplicate = false;
        var cantidad = numero_recibos.length;
        for(var i = 0; i < cantidad - 1; i++)
        {
            for(var j = i + 1; j < cantidad; j++ )
            {
                if(numero_recibos[i] == numero_recibos[j])
                {
                    if(numero_recibos[i] != "")
                    {
                        duplicate = true;
                        break;
                    }
                }
            }
            if(duplicate == true)
                break;
        }
        return duplicate;
    }

</script>