
<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12" id="fieldset-infopago">
    <legend class="ui-widget ui-widget-header ui-corner-all">INFORMACIÓN DE PAGO</legend>
    <div class="row">
        <div class="col-md-4">
            <label for=""><spam class="text-danger">*</spam>Fecha de entrega</label>
            <input value="<?php echo $fentrega;?>" type="text" class="calcular form-control not-w fecha validate[required]" id="entrega" name="entrega"/> 
        </div>
        <div class="col-md-4">
            <label for=""><spam class="text-danger">*</spam>Fecha de cobro</label>
            <input value="<?php echo $fcobro;?>" type="text" class="calcular form-control not-w fecha validate[required]" id="fecha-cobro" name="fecha-cobro"/> 
        </div>
        <div class="col-md-4">
            <label for=""><spam class="text-danger">*</spam>Fecha (cobrar)</label>
            <input value="<?php echo $cobrar;?>" type="text" class="calcular form-control not-w validate[required, custom[integer]]" id="cobro" name="cobro"/> 
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <label for=""><spam class="text-danger">*</spam>Monto del contrato</label>
            <input value="<?php echo $monto_contrato;?>" type="text" class="calcular form-control not-w validate[required, custom[number]]" id="monto-contrato" name="monto-contrato" value="<?php echo $monto_contrato; ?>"/> 
        </div>
        <div class="col-md-3">
            <label for="">Prima</label>
            <input value="<?php echo $prima;?>" type="text" class="calcular form-control not-w validate[custom[number]]" id="prima" name="prima"/> 
        </div>
        <div class="col-md-3">
            <label for=""><spam class="text-danger">*</spam>Neto</label>
            <input value="<?php echo $neto;?>" type="text" class="calcular form-control not-w validate[required, custom[number]]" id="neto" name="neto" value="<?php echo $monto_contrato; ?>"/> 
        </div>
        <div class="col-md-3">
            <label for="">Descuento</label>
            <input value="<?php echo $descuento;?>" type="text" class="calcular form-control not-w validate[custom[number]]" id="descuento" name="descuento"/> 
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <label for=""><spam class="text-danger">*</spam>Número de cuotas</label>
            <input value="<?php echo $ncuotas;?>" type="text" class="calcular form-control not-w validate[required, custom[integer], min[1]]" id="ncuotas" name="ncuotas"/> 
        </div>
        <div class="col-md-3">
            <label for=""><spam class="text-danger">*</spam>Monto cuotas</label>
            <input value="<?php echo $cuota;?>" type="text" class="form-control not-w validate[custom[number]]" id="cuota" name="cuota"/> 
        </div>
        <div class="col-md-3">
            <label for="">Saldo pendiente</label>
            <input value="<?php echo $saldo;?>"  type="text" class="form-control not-w" id="saldo" name="saldo" readonly/> 
        </div>
        <div class="col-md-3">
            <label for="" class="text-danger">Mora acumulada 5% x mes</label>
            <input type="text" class="form-control not-w" readonly id="mora" name="mora"/> 
        </div>
    </div>

    <div class="row mt-3">
        <div class="col-md-8">
            <div class="row" style="visibility: hidden;height: 0;">
                <div class="col-md-4">
                    <label for=""><spam class="text-danger">*</spam>Cuenta</label>
                    <select class="form-control validate[required, custom[number], min[0]]" value="1" id="cuenta" name="cuenta" id="">
                        <?php 
                            $v_cuestas = ["A","B","C"];
                            $limit_cuenta = count($v_cuestas)+2;
                            for($index_cuenta = 2; $index_cuenta < $limit_cuenta; $index_cuenta++)
                            {
                                $letra = $v_cuestas[$index_cuenta-2];
                                if($index_cuenta == $cuenta)
                                    echo "<option selected value='$index_cuenta'>$letra</option>";
                                else
                                    echo "<option value='$index_cuenta'>$letra</option>";

                            }
                        ?>
                    </select>
                </div>
                <div class="col-md-4">
                    <label for="">Salida</label>
                    <input value="<?php echo $salida;?>" type="text" class="form-control validate[custom[number]]" id="salida" name="salida">
                </div>
            </div>
        </div>
    </div>
</fieldset>
    <div class="row mt-4">
        <div class="col-md-8 mt-4">
            <button type="button" class="btn btn-info btn-block" data-toggle="modal" data-target="#modal-recibos">VER RECIBOS PAGADOS</button>
        </div>
        <div class="col-md-4">
            <div class="row">
                <div class="col-md-12">
                    <label for="">Abonado en recibos</label>
                    <input readonly type="text" class="form-control not-w" id="abonado-recibo" value="0"/> 
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label for="">Hoy es</label>
                    <input readonly type="text" class="form-control fecha not-w" id="hoy" value="<?php echo date("Y/m/d");?>"/> 
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label for="">Los días desde firmado</label>
                    <input readonly type="text" class="form-control not-w" id="dias-firmado"/> 
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label for="">Su contrato es por días</label>
                    <input readonly type="text" class="form-control not-w" id="duracion-contrato"/> 
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label for="">Su cuota por dia es de</label>
                    <input readonly type="text" class="form-control not-w" id="dia-cuota"/> 
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label for="">Su ultimo pago será</label>
                    <input readonly type="text" class="form-control not-w" id="ultimo-pago"/> 
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label for="">Los días que ha pagado son</label>
                    <input readonly type="text" class="form-control not-w" id="dias-pagados"/> 
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label for="">Los días que tiene atrasado son</label>
                    <input readonly type="text" class="form-control not-w" id="dias-atrasados"/> 
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label for="">La cuota para estar al día es</label>
                    <input readonly type="text" class="form-control not-w" id="aldia"/> 
                </div>
            </div>
            
        </div>
    </div>

</fieldset>


<!-- Modal para agregar razonamiento-->
<div class="modal fade" id="modal-recibos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document" style="max-width: 95%;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Recibos Cobrados</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="overflow: auto;">
    
                <table id="tabla-recibos" class="table table-hover" style="min-width: 700px;">
                    <thead>
                        <tr>
                            <th style="width:5%; text-align:center;">#</th>
                            <th style="width:15%;"><spam class="text-danger">*</spam>Recibo</th>
                            <th style="width:20%;"><spam class="text-danger">*</spam>F. Pago</th>
                            <th style="width:20%;"><spam class="text-danger">*</spam>Monto</th>
                        </tr>
                    </thead>
                    <tbody id="tb-recibo">
                        <!-- AGREGAR LOS RECIBOS PAGADOS -->
                        <?php 
                            $pagos = $db->query("SELECT * FROM solicitudes INNER JOIN movimientos ON (movprefijo = 'RC' AND movdocumento = solfactura) WHERE solid = '$contrato' ORDER BY movimientos.created_at ASC");
                            $index_pago = 1;
                            while($pago = $pagos->fetch(PDO::FETCH_ASSOC))
                            {
                                echo 
                                "<tr class='recibo-item'>" .
                                    "<td style='text-align: center;'>$index_pago</td>".
                                    "<td><input readonly class='num_recibo form-control' value='". $pago['movnumero'] ."'></td>" .
                                    "<td><input readonly class='detalle-fpago form-control' value='". $pago['movfecha'] ."'></td>" .
                                    "<td><input readonly class='monto-recibo form-control' value='". $pago['movvalor'] ."'></td>" .
                                "</tr>";
                                $index_pago++;
                            }
                        ?>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>


<script>
    function info_extra()
    {
        var diff = dias();
        var ncuota = $("#ncuotas").val(); 
        var duracion = 30.5 * ncuota;
        var neto = $("#neto").val();
        var cuota_dia = 0;
        var saldo_pagado = $("#abonado-recibo").val();
        var dias_pagados = 0;

        actualizar_saldo(); 

        if(duracion > 0)
        {
            cuota_dia = neto / duracion;
            dias_pagados = saldo_pagado / cuota_dia;
        }

        var aux_fcobro = $("#fecha-cobro").val().replace("-", "/");
        var f_cobro = new Date(aux_fcobro);
        var aux = new Date(f_cobro.setDate(f_cobro.getDate() + duracion));
        var ultimo_pago = "";
        if(aux > 0)
            ultimo_pago = aux.getFullYear()+ "-" + (aux.getMonth() +1) + "-" + aux.getDate();
        

        // Ajuste
        cuota_dia = (cuota_dia.toFixed(2) * 1);

        $("#dias-firmado").val(diff);
        $("#duracion-contrato").val(duracion);
        $("#dia-cuota").val(cuota_dia);
        $("#ultimo-pago").val(ultimo_pago);
        $("#dias-pagados").val(dias_pagados);
        var dias_atrasados = diff - dias_pagados;
        if(dias_atrasados < 0)
            dias_atrasados = 0;
        $("#dias-atrasados").val(dias_atrasados);

        var dias_atrasados = $("#dias-atrasados").val();
        var aldia = 0;
        if(dias_atrasados > duracion)
        {
            aldia = duracion * cuota_dia;
        }
        else
            aldia = dias_atrasados * cuota_dia;
        $("#aldia").val(aldia.toFixed(2)*1);

        var saldo = $("#saldo").val(); 
        var mora = ((dias_atrasados/30.5) * (saldo * 0.05)).toFixed(2) * 1;
        $("#mora").val(mora);
    }
    function dias()
    {
        var f0 = $("#fecha-cobro").val().replace("-", "/");
        var f1 = new Date(f0);
        var fecha_contrato = new Date (f1.getFullYear()+ "/" + (f1.getMonth() +1) + "/" + f1.getDate());
        fecha_contrato = fecha_contrato.getTime();
        var diff = 0;
        if(fecha_contrato > 0)
        {
            var hoy = new Date($("#hoy").val()).getTime();
    
            diff = hoy - fecha_contrato;
            diff = diff/(1000*60*60*24);
            
            //ajuste
            if(diff >= 0)
                diff += 1;
        }
        return diff;
    }
    


    var base_abonado = parseFloat($("#abonado-recibo").val());
    var incompleto = false;
    function actualizar_saldo()
    {
        var total = 0;
        var valor = 0;
        var cant = $(".monto-recibo").length;
        
        incompleto = false;
        for (i = 0; i<cant; i++)
        {
            valor = parseFloat($(".monto-recibo").eq(i).val());
            var num_recibo = $(".num_recibo ").eq(i).val();
            var fpago = $(".detalle-fpago ").eq(i).val();
            if( (valor >= 0) && (num_recibo != "" && (fpago != "")) )
                total += valor;
        }
        var abonado = base_abonado + total;
        $("#abonado-recibo").val(abonado);
    }
    
</script>