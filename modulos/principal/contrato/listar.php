<?php
    $r = '../../../';
    require($r . 'incluir/session.php');
    require($r . 'incluir/connection.php');
    $numero = $cliente = $fecha1 = $fecha2 = $estado = "";
    if (isset($_POST['consultar'])) {
        $contrato = strupperEsp(str_replace("'", "", $_POST['contrato']));
        $cliente = strupperEsp(str_replace("'", "", $_POST['cliente']));
        $fecha1 = $_POST['fecha1'];
		$fecha2 = $_POST['fecha2'];
		$estado = $_POST['estado'];
		$nombre = strupperEsp($_POST['nombre']);

		$dep = $_POST['departamento'];
		$ciu = $_POST['ciudad'];
		$asesor = $_POST['asesor'];
		$cobrador = $_POST['cobrador'];
		$entregador = $_POST['entregador'];
		$telefono = $_POST['telefono'];
		$direccion = strupperEsp(str_replace("'", "", $_POST['direccion']));
		$referencia = strupperEsp(str_replace("'", "", $_POST['referencia']));
		$fecha_cobro = $_POST['cobro'];

    } elseif(isset($_GET['contrato'])) {
        @$contrato = strupperEsp(str_replace("'", "", $_GET['contrato']));
        @$cliente = strupperEsp(str_replace("'", "", $_GET['cliente']));
        @$fecha1 = $_GET['fecha1'];
        @$fecha2 = $_GET['fecha2'];
		@$estado = $_GET['estado'];
		@$nombre = strupperEsp($_GET['nombre']);
		@$dep = $_GET['departamento'];
		@$ciu = $_GET['ciudad'];
		@$asesor = $_GET['asesor'];
		@$cobrador = $_GET['cobrador'];
		@$entregador = $_GET['entregador'];
		@$telefono = $_GET['telefono'];
		@$direccion = strupperEsp(str_replace("'", "", $_GET['direccion']));
		@$referencia = strupperEsp(str_replace("'", "", $_GET['referencia']));
		@$fecha_cobro = $_GET['cobro'];
    }

    $filtro = 'contrato=' . $contrato . '&cliente=' . $cliente . '&fecha1=' . $fecha1 . '&fecha2=' . $fecha2 . '&estado=' . $estado . 
	'&departamento=' . $dep . '&ciudad=' . $ciu . '&asesor=' . $asesor . '&cobrador=' . $cobrador . '&entregador=' . $entregador.
	'&telefono='. $telefono. '&direccion=' . $direccion . '&referencia=' . $referencia  . '&cobro=' . $fecha_cobro ;
    
	$con = 'SELECT * FROM solicitudes INNER JOIN clientes ON cliid = solcliente LEFT JOIN departamentos ON depid = soldepentrega LEFT JOIN ciudades ON (ciuid = solciuentrega AND ciudepto = soldepentrega) LEFT JOIN carteras ON carfactura = solfactura';
    $ord = ' ORDER BY solfecha';

    /* Los parametros de la consulta sql se genera dinamicamente 
    en base a los datos recibidos para delimitar los resultados */
    $parameters = [];
    if($contrato != "")
        array_push($parameters, "solid LIKE '%$contrato%'" );
    if($cliente != "")
        array_push($parameters, "solcliente LIKE '%$cliente%'" );
    if($fecha1 != "")
        array_push($parameters, "solfecha BETWEEN '$fecha1' AND '$fecha2'" );
	if($estado != "")
		array_push($parameters, "carestado = '$estado'" );
	if($nombre != "")
		array_push($parameters, "upper(concat(cliNombre, ' ', cliNom2, ' ', cliApe1, ' ', cliApe2)) LIKE '%$nombre%'" );
	
	if($dep != "")
		array_push($parameters, "soldepentrega = '$dep'" );
	if($ciu != "")
		array_push($parameters, "solciuentrega = '$ciu'" );
	if($asesor != "")
		array_push($parameters, "solasesor = '$asesor'" );
	if($cobrador != "")
		array_push($parameters, "solrelacionista = '$cobrador'" );
	if($entregador != "")
		array_push($parameters, "solenvio = '$entregador'" );
	
	if($telefono != "")
		array_push($parameters, "soltelentrega iLIKE '%$telefono%' OR soltelcobro iLIKE '%$telefono%'" );
	
	if($direccion != "")
		array_push($parameters, "solentrega like '%$direccion%'" );
	
	if($referencia != "")
		array_push($parameters, "clirefnombre1 ILIKE '%$referencia%' OR clirefnombre2 ILIKE '%$referencia%'" );
	
	if($fecha_cobro != "")
		array_push($parameters, "cobro = $fecha_cobro" );
	
	
	if(count($parameters) == 0)
	{
		header('location:consultar.php?error=Debe llenar almenos un campo');
		exit();
	}
	
    // Consulta base
    $sql = $con;
    foreach ($parameters as $index => $parameter) {
        // Se agregan los parametros del WHERE
        if($index == 0)
            $sql .= " WHERE " . $parameter;
        else
            $sql .= " AND " . $parameter;
    }
    // Se completa la consulta
	$sql .= $ord;
	
$qry = $db->query($sql);
$cantidad_contrato = $qry->rowCount();

?>
<!doctype html>
<html>

<head>
    <title>LISTA DE CONTRATOS</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-form.php');
	?>
	<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
	<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>
	
	<style>
	
		.dataTable
        {
            max-height: 30em!important;
        }
	</style>
    
	<script type="text/javascript">
        var table;
		$(document).ready(function() {
			table = $('#tabla').dataTable({
				'bJQueryUI': true,
				'sPaginationType': 'full_numbers',
				'oLanguage': {
					'sUrl': '<?php echo $r ?>incluir/datatables/datatables.es.txt',
				},
            });
            
            
			table.on("draw", function(){
				console.log("hola");
            	$('#loading').dialog('close');
			});
			
			$('.pdf').click(function() {
				newSrc = $(this).attr('data-rel');
				if (validation_file(newSrc) == 200)
					$('#modal').html("<iframe src='" + newSrc + "' width='100%' height='100%'></iframe>");
				else
					$('#modal').html("<spam>El PDF no fue encontrado</spam>");
				$('#modal').dialog({
					modal: true,
					width: '900',
					height: '500',
					title: 'PDF de la solicitud'
				});
			});
        });
        
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
        <?php require($r . 'incluir/src/menu.php') ?>
        

		<div id="loading" title="Procesando..."><span class="" style="float:left; margin:3px 7px 7px 0;"></span>Este proceso puede demorar <br>Se estan cargando <span style="color:red"><?php echo $cantidad_contrato?></span> resgistros.</div>
        <script>
			$('#loading').dialog({
					autoOpen:false,
					modal:true,
					draggable: false,
					resizable:false
				}).parent('.ui-dialog').find('.ui-dialog-titlebar-close').remove();
			$('#loading').dialog('open');
		</script>
		<article id="cuerpo">
			<article class="mapa">
				<a href="<?php echo $r ?>index.php">Principal</a>
				<div class="mapa_div"></div><a>Contrato</a>
				<div class="mapa_div"></div><a class="current">Ver contratos</a>
			</article>
			<article id="contenido">
				<h2>Listado de contratos <?php  ?></h2>
				<div class="row" style="overflow:auto;">
					<table id="tabla" style="width:100%!important">
						<thead>
							<tr>
								<th></th>
								<?php if ($rowlog['percontratoedit'] == '1') { ?>
									<th></th>
								<?php } ?>

								<?php if (@$rowlog['percontratofac'] == '1') { ?>
									<th></th>
									<th></th>
								<?php } ?>

								<th>Contrato</th>
								<th>Estado</th>
								<th>Fecha Contrato</th>
								<th>Cedula</th>
								<th>Nombre</th>
								<th>Departa</th>
								<th>Ciudad</th>
								<th>Asesor</th>
								<th>Cobrador</th>
								<th>Entregador</th>
							</tr>
						</thead>
						<tbody >
							<?php
							while ($row = $qry->fetch(PDO::FETCH_ASSOC)) {
							
								$full_name = $row['clinombre'];
								if( $row['clinom2'] != "" )
									$full_name .= " " . $row['clinom2'];
								if( $row['cliape1'] != "" )
									$full_name .= " " . $row['cliape1'];
								if( $row['cliape2'] != "" )
									$full_name .= " " . $row['cliape2'];
								
								$ruta = "solid=".$row['solid'];
							?>
								<tr>
									<td><a target="_blank" href="detalle.php?solid=<?php echo $row['solid']."&".$filtro?>" title="Ver más"><img src="../../../imagenes/iconos/eye.png" class="grayscale"></a></td>
									<?php if ($rowlog['percontratoedit'] == '1') { ?>
										<td>
											<a target="_blank" href="editar.php?solid=<?php echo $row['solid']."&".$filtro?>"  title="Editar"><img src="../../../imagenes/iconos/lapiz.png" class="grayscale"></a>
										</td>
									<?php } ?>
									<?php if (@$rowlog['percontratofac'] == '1') { ?>
										<td>
											<a target="_blank" href="facturar.php?solid=<?php echo $row['solid']."&".$filtro?>"  title="Facturar"><img src="../../../imagenes/iconos/money_add.png" class="grayscale"></a>
										</td>
										
										<td>
											<img style="margin:0 3px;" src="<?php echo $r ?>imagenes/iconos/pdf.png" class="pdf" data-rel="<?php echo 'pdf.php?solid=' . $row['solid'] ?>" title="PDF Factura" />
										</td>
									<?php } ?>
									<td><?php echo $row['solid'] ?></td>
									<td><?php echo $row['carestado'] ?></td>
									<td><?php echo $row['solfecha'] ?></td>
									<td><?php echo $row['solcliente'] ?></td>
									<td><?php echo $full_name ?></td>
									<td><?php echo $row['depnombre'] ?></td>
									<td><?php echo $row['ciunombre'] ?></td>
									<td><?php echo $row['solasesor'] ?></td>
									<td><?php echo $row['solrelacionista'] ?></td>
									<td><?php echo $row['solenvio'] ?></td>
									
									</td>
								</tr>
							<?php
							}
							?>
						</tbody>
					</table>
				</div>
				<p class="boton">
					<button type="button" class="btn btn-primary btnatras" onClick=" carga(); location.href = 'consultar.php'">atras</button>
				</p>
			</article>
		</article>
		<?php require($r . 'incluir/src/pie.php') ?>
	</section>
	<div id="modal" style="display:none"></div>

	<?php
	if (isset($_GET['mensaje'])) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['mensaje'] . '</div>';
	?>
</body>

</html>