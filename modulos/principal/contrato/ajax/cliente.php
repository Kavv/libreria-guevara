<?php

$r = '../../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

$cedula = strtoupper($_GET['cedula']);
if($cedula != "")
{
    $qry = $db->prepare(
        "SELECT clinombre, clinom2, cliape1, cliape2, clicelular, clitelresidencia, clidepresidencia, cliciuresidencia, clidirresidencia, cliempresa, 
        clinomfamiliar, clitelfamiliar, cliempfamiliar, 
        clirefnombre1, clireftelefono1, clirefcelular1, clirefnombre2, clireftelefono2, clirefcelular2 
        FROM clientes WHERE cliid = :cedula");
    $qry->bindParam(':cedula', $cedula);
    $qry->execute();
    
    $cliente = $qry->fetch(PDO::FETCH_ASSOC);
    
    if($cliente)
    {
        echo json_encode($cliente);
    }
    else
    {
        echo 0;
        exit();
    }
    
}
else{
    echo 0;
    exit();
}

?>