<?php 

$codigo = $_GET['codigo'];
$nombre = $_GET['nombre'];
$precio = $_GET['precio'];

?>
<div class="row producto-item" id="producto-<?php echo $codigo;?>">
    <div class="col-md-2">
        <label for="">Código</label>
        <input name="producto-codigo[]" type="text" class="producto-condigo form-control" value="<?php echo $codigo;?>" readonly />
    </div>
    <div class="col-md-4">
        <label for="">Producto</label>
        <input name="producto-nombre[]" type="text" class="producto-nombre form-control" value="<?php echo $nombre;?>" readonly />
    </div>
    <div class="col-md-3">
        <label for="">Precio Unidad</label>
        <input name="producto-precio[]" type="text" class="input-disabled producto-precio form-control validate[min[0], custom[number], required]" value="<?php echo $precio;?>" />
    </div>
    <div class="col-md-2">
        <label for="">Cantidad</label>
        <input name="producto-cantidad[]" type="text" class="producto-cantidad form-control validate[min[0], custom[integer],required]" value="1" />
    </div>
    <div class="col-md-1">
        <label for=""></label>
        <button type="button" class="btn btn-danger" onclick="remove_product(this);">-</button>
    </div>
</div>