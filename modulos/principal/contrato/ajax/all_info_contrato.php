<?php

$r = '../../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

$contrato = strupperEsp($_GET['contrato']);

if($contrato != "NULL" && $contrato != "")
{
    $consulta = "SELECT solicitudes.*, solicitudes.created_at as d_contrato, clientes.*, departamentos.*, ciudades.*, movimientos.*, carteras.*, 
    a.usuid as id_asesor, a.usunombre as asesor, c.usuid as id_cobrador, c.usunombre as cobrador FROM solicitudes 
    INNER JOIN clientes ON cliid = solcliente 
    LEFT JOIN departamentos ON depid = soldepentrega 
    LEFT JOIN ciudades ON (ciuid = solciuentrega AND ciudepto = soldepentrega)
    LEFT JOIN movimientos on (movdocumento = solfactura AND movprefijo = 'FV') 
    LEFT JOIN carteras on carfactura = solfactura 
    LEFT JOIN usuarios as a ON a.usuid = solasesor
    LEFT JOIN usuarios as c ON c.usuid = solrelacionista
    WHERE solid = '$contrato';";

    $data = $db->query($consulta)->fetch(PDO::FETCH_ASSOC);
    if(!$data)
    {
        $html = 
        '<div class="alert alert-danger" alert-dismissible fade show" role="alert">'.
            'No se encontro el contrato ' . $contrato . 
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'.
                '<span aria-hidden="true">&times;</span>'.
           ' </button>'.
        '</div>';
        echo $html;
        exit();
    }
    
    $full_name = $data['clinombre'];
    if( $data['clinom2'] != "" )
        $full_name .= " " . $data['clinom2'];
    if( $data['cliape1'] != "" )
        $full_name .= " " . $data['cliape1'];
    if( $data['cliape2'] != "" )
        $full_name .= " " . $data['cliape2'];

    // Cliente
    $cedula = strupperEsp(str_replace("'", "", $data['solcliente']));
    $nombre = strupperEsp(str_replace("'", "", $full_name));
    $telefono1 = str_replace("'", "", $data['clicelular']);
    $telefono2 = str_replace("'", "", $data['clitelresidencia']);
    $departamento = strupperEsp(str_replace("'", "", $data['soldepentrega']));

    $ciudad = $data['solciuentrega'];
    $full_ciudad = str_replace("'", "", $data['soldepentrega'].$ciudad);

    $trabajo = strupperEsp(str_replace("'", "", $data['cliempresa']));
    $direccion = strupperEsp(str_replace("'", "", $data['solentrega']));
    $conyugue = strupperEsp(str_replace("'", "", $data['clinomfamiliar']));
    $conyugue_telefono = str_replace("'", "", $data['clitelfamiliar']);
    $conyugue_trabajo = strupperEsp(str_replace("'", "", $data['cliempfamiliar']));
    $referencia1 = strupperEsp(str_replace("'", "", $data['clirefnombre1']));
    $ref1_tel1 = str_replace("'", "", $data['clireftelefono1']);
    $ref1_tel2 = str_replace("'", "", $data['clirefcelular1']);
    $referencia2 = strupperEsp(str_replace("'", "", $data['clirefnombre2']));
    $ref2_tel1 = str_replace("'", "", $data['clireftelefono2']);
    $ref2_tel2 = str_replace("'", "", $data['clirefcelular2']);

    // Solicitud
    $empresa = "16027020274133";
    //$contrato = strupperEsp(str_replace("'", "", $data['solid']));
    $fecha_contrato = str_replace("'", "", $data['solfecha']);
    $estudiante = strupperEsp(str_replace("'", "", $data['estudiante']));
    $escuela = strupperEsp(str_replace("'", "", $data['escuela']));
    $grado = strupperEsp(str_replace("'", "", $data['grado']));
    $observaciones = strupperEsp(str_replace(["'","<",">"], "", $data['solobservacion']));

    $razonamientos = $db->query("SELECT hissolicitudes.* FROM solicitudes INNER JOIN hissolicitudes ON hsosolicitud = solid WHERE solid='$contrato' ORDER BY hsoid ASC");
    
    if(isset($data['razonamientos']))
    $razonamientos = $data['razonamientos'];
    $cantidad_razonamiento = $razonamientos->rowCount();
    
    
    $asesor = $data['solasesor'];
    $entregador = $data['solenvio'];
    $cobrador = $data['solrelacionista'];
    
    // Por defecto
    $clase = $data['clase'];
    $salida = $data['salida'];
    $cuenta = $data['cuenta'];
    $digitador = $data['digitador'];

    // solicitud-detalle

    $productos = $db->query("SELECT detproducto, detcantidad, detprecio, pronombre FROM solicitudes INNER JOIN detsolicitudes ON detsolicitud = solid INNER JOIN productos on proid = detproducto WHERE solid = '$contrato' ORDER BY detsolicitudes.created_at ASC;");
    $cantidad_productos = $productos->rowCount();

    // Información de pago
    $fentrega = $data['solfechdescarga'];
    $fcobro = $data['solcompromiso'];
    $cobrar = $data['cobro'];
    $monto_contrato = $data['soltotal'];
    $prima = $data['solcuota'];
    $neto = $data['cartotal'];
    $descuento = $data['movdescuento'];
    $ncuotas = $data['carncuota'];
    $cuota = $data['carcuota'];
    $saldo = $data['carsaldo'];
    $factura = $data['solfactura'];
    $cartera_estado = $data['carestado'];


    
    $nombre_asesor = "";
    if(TRIM($data['asesor']) != "")
        $nombre_asesor .= " / " . $data['asesor'];
    $nombre_cobrador = "";
    if(TRIM($data['cobrador']) != "")
        $nombre_cobrador .= " / " . $data['cobrador'];

    $contato_digitado = $data['d_contrato'];
    $contato_digitado = date("Y-m-d h:i:s A", strtotime($contato_digitado));
}
else
{
    $html = 
    '<div class="alert alert-danger" alert-dismissible fade show" role="alert">'.
        'No ha especificado el contrato que desea buscar' .
        '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'.
            '<span aria-hidden="true">&times;</span>'.
       ' </button>'.
    '</div>';
    echo $html;
    exit();
}

?>


    <div class="row divisor">
        <div class="col-md-12 text-center">
            <h3 class="">CONTRATO #<?php echo $contrato .' / ESTADO: '.$cartera_estado;?></h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <label class="bold" for="">Digitado por: </label>
            <label for="" class="sub-rayado"><?php echo $data['digitador'];?></label>
        </div>
        <div class="col-md-3">
            <label class="bold" for="">F. Digitado: </label>
            <label for="" class="sub-rayado"><?php echo $contato_digitado;?></label>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <label class="bold" for="">Cédula: </label>
            <label for="" class="sub-rayado"><?php echo $cedula;?></label>
        </div>
        <div class="col-md-3">
            <label class="bold" for="">Fecha Contrato: </label>
            <label for="" class="sub-rayado"><?php echo $fecha_contrato;?></label>
        </div>
        <div class="col-md-3">
            <label class="bold" for="">Teléfono 1: </label>
            <label for="" class="sub-rayado"><?php echo $telefono1; ?> </label>
        </div>
        <div class="col-md-3">
            <label class="bold" for="">Teléfono 2: </label>
            <label for="" class="sub-rayado"><?php echo $telefono2; ?> </label>
        </div>
    </div>
              
    <div class="row">
        <div class="col-md-6">
            <label class="bold" for="">Nombre Completo:</label>
            <label for="" class="sub-rayado"><?php echo $nombre; ?> </label>
        </div>
        <div class="col-md-3">
            <label class="bold" for="">Departamento:</label>
            <label for="" class="sub-rayado"><?php echo $data['depnombre']; ?> </label>
        </div>
        <div class="col-md-3">
            <label class="bold" for="">Ciudad:</label>
            <label for="" class="sub-rayado"><?php echo $data['ciunombre']; ?> </label>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <label class="bold" for="">Lugar de trabajo:</label>
            <label for="" class="sub-rayado"><?php echo $trabajo; ?> </label>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <label class="bold" for="">Dirección:</label>
            <label for="" class="sub-rayado"><?php echo $direccion; ?> </label>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="row divisor">
                <div class="col-md-12 text-center">
                    <h6>DATOS DEL CONYUGUE</h6>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-8">
                    <label class="bold" for="">Nombre del Conyugue:</label>
                    <label for="" class="sub-rayado"><?php echo $conyugue; ?> </label>
                </div>
                <div class="col-md-4">
                    <label class="bold" for="">Teléfono:</label>
                    <label for="" class="sub-rayado"><?php echo $conyugue_telefono; ?> </label>
                </div>
                <div class="col-md-12">
                    <label class="bold" for="">Lugar de trabajo:</label>
                    <label for="" class="sub-rayado"><?php echo $conyugue_trabajo; ?> </label>
                </div>
            </div>

            <div class="row divisor">
                <div class="col-md-12 text-center">
                    <h6>DATOS DEL ESTUDIANTE</h6>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <label class="bold" for="">Nombre del estudiante:</label>
                    <label for="" class="sub-rayado"><?php echo $estudiante; ?> </label>
                </div>
                <div class="col-md-12">
                    <label class="bold" for="">Escuela: </label>
                    <label for="" class="sub-rayado"><?php echo $escuela; ?> </label>
                </div>
                <div class="col-md-12">
                    <label class="bold" for="">Grado: </label>
                    <label for="" class="sub-rayado"><?php echo $grado; ?> </label>
                </div>
            </div>
    
        </div>
        <div class="col-md-6">

            <div class="row divisor">
                <div class="col-md-12 text-center">
                    <h6>DATOS DE LAS REFERENCIAS</h6>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label class="bold" for="">Nombre Referencia 1:</label>
                    <label for="" class="sub-rayado"><?php echo $referencia1; ?> </label>
                </div>
                <div class="col-md-12">
                    <label class="bold" for="">Teléfono1 Referencia1:</label>
                    <label for="" class="sub-rayado"><?php echo $ref1_tel1; ?> </label>
                </div>
                <div class="col-md-12">
                    <label class="bold" for="">Teléfono2 Referencia1:</label>
                    <label for="" class="sub-rayado"><?php echo $ref1_tel2; ?> </label>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <label class="bold" for="">Nombre Referencia 2:</label>
                    <label for="" class="sub-rayado"><?php echo $referencia2; ?> </label>
                </div>
                <div class="col-md-12">
                    <label class="bold" for="">Teléfono1 Referencia2:</label>
                    <label for="" class="sub-rayado"><?php echo $ref2_tel1; ?> </label>
                </div>
                <div class="col-md-12">
                    <label class="bold" for="">Teléfono2 Referencia2:</label>
                    <label for="" class="sub-rayado"><?php echo $ref2_tel2; ?> </label>
                </div>
            </div>
        </div>

    </div>


    <div class="row">
        <div class="col-md-6">
            <div class="row divisor">
                <div class="col-md-12 text-center">
                    <h6>DETALLES</h6>
                </div>
            </div>
            <div class="col-md-12">
                <label class="bold" for="">Observaciones: </label>
                <label for="" class="sub-rayado"><?php echo $observaciones; ?> </label>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row divisor">
                <div class="col-md-12 text-center">
                    <h6>PERSONAL ASIGNADO</h6>
                </div>
            </div>
            <div class="col-md-12">
                <label class="bold" for="">Asesor de Ventas: </label>
                <label for="" class="sub-rayado"><?php echo $data['id_asesor'].$nombre_asesor; ?> </label>
            </div>
            <div class="col-md-12">
                <label class="bold" for="">Entregador: </label>
                <label for="" class="sub-rayado"><?php echo $entregador; ?> </label>
            </div>
            <div class="col-md-12">
                <label class="bold" for="">Cobrador: </label>
                <label for="" class="sub-rayado"><?php echo $data['id_cobrador'].$nombre_cobrador; ?> </label>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
        
            <div class="row divisor">
                <div class="col-md-12 text-center">
                    <h6>RAZONAMIENTOS</h6>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-12" style="overflow-y:auto; max-height: 20em;">
                    <table class="table table-hover">
                        <thead class="">
                            <th style='width:60%'>Razonamientos</th>
                            <th style='width:40%'>Digitador</th>
                        </thead>
                        <tbody id="t-razonamiento">
                            <?php 
                                while($razonamiento = $razonamientos->fetch(PDO::FETCH_ASSOC))
                                {
                                    echo "<tr> 
                                        <td style='width:60%'><label>" . $razonamiento['hsonota'] . "</label></td>
                                        <td style='width:40%'><label>" . $razonamiento['hsousuario'] . "</label></td>
                                    </tr>";
                                    
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


        <div class="col-md-6">

            <div class="row divisor">
                <div class="col-md-12 text-center">
                    <h6>PRODUCTOS ENTREGADOS</h6>
                </div>
            </div>

            <div id="lista-productos">
                <div class="row">
                    <div class="col-md-12" style="overflow-y:auto; max-height: 20em;">
                        <table class="table table-hover">
                            <thead class="text-center">
                                <th style='width:20%;'>Código</th>
                                <th style='width:60%;'>Producto</th>
                                <th style='width:10%;'>Precio</th>
                                <th style='width:10%;'>Cantidad</th>
                            </thead>
                            <tbody id="t-razonamiento">
                                <?php 
                                    while($producto = $productos->fetch(PDO::FETCH_ASSOC))
                                    {
                                        echo "<tr> 
                                            <td style='width:20%; text-align:end;'><label>" . $producto['detproducto'] . "</label></td>
                                            <td style='width:60%;'><label>" . $producto['pronombre'] . "</label></td>
                                            <td style='width:10%;'><label>" . $producto['detprecio'] . "</label></td>
                                            <td style='width:10%;'><label>" . $producto['detcantidad'] . "</label></td>
                                        </tr>";
                                        
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="row divisor">
                <div class="col-md-12 text-center">
                    <h6>INFORMACIÓN DE PAGO</h6>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12" style="overflow-y:auto; max-height: 20em;">
                    <table id="tabla-recibos" class="table table-hover" style="min-width: 700px;">
                        <thead>
                            <tr>
                                <th style="width:20px; text-align:center;">#</th>
                                <th style="width:40px;"><spam class="text-danger">*</spam>Recibo</th>
                                <th style="width:40px;"><spam class="text-danger">*</spam>F. Pago</th>
                                <th style="width:40px;"><spam class="text-danger">*</spam>Monto</th>
                                <th style="width:40px;"><spam class="text-danger">*</spam>Cobrador</th>
                                <th style="width:30px;">Digitador</th>
                                <th style="width:30px;">Digitación</th>
                            </tr>
                        </thead>
                        <tbody id="tb-recibo">
                            <!-- AGREGAR LOS RECIBOS PAGADOS -->
                            <?php 
                                $pagos = $db->query("SELECT movimientos.*, infoextra.digitador as d_cobro FROM solicitudes 
                                INNER JOIN movimientos ON (movprefijo = 'RC' AND movdocumento = solfactura)
                                LEFT JOIN infoextra ON movinfo = infoextra.id 
                                WHERE solid = '$contrato' ORDER BY movfecha asc");
                                $index_pago = 1;
                                $valor_pagado = 0;
                                $cantidad_pagos = $pagos->rowCount();
                                while($pago = $pagos->fetch(PDO::FETCH_ASSOC))
                                {
                                    $originalDate = $pago['created_at'];
                                    $newDate = date("Y-m-d h:i:s A", strtotime($originalDate));
                                    echo 
                                    "<tr class='recibo-item'>" .
                                        "<td style='text-align:center;'>$index_pago</td>" .
                                        "<td> <label style=''>". $pago['movnumero'] . "</label></td>" .
                                        "<td> <label style='min-width:90px;'>". $pago['movfecha'] . "</label></td>" .
                                        "<td> <label style=''>". $pago['movvalor'] . "</label></td>" .
                                        "<td> <label style='min-width:130px;'>". $pago['movcobrador'] . "</label></td>" .
                                        "<td> <label style='min-width:130px;'>". $pago['d_cobro'] . "</label></td>" .
                                        "<td> <label style='min-width:130px;'>". $newDate . "</label></td>" .
                                    "</tr>";
                                    $valor_pagado += $pago['movvalor'];
                                    $index_pago++;
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row divisor">
                <div class="col-md-12 text-center">
                    <h6>DETALLE DE LA CARTERA</h6>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <label class="bold" for="">Fecha de Entrega: </label>
                    <label for="" class="sub-rayado" style="font-size:17px!important;"><?php echo $fentrega; ?> </label>
                </div>
                <div class="col-md-6">
                    <label class="bold" for="">Fecha de Cobro: </label>
                    <label for="" class="sub-rayado" style="font-size:17px!important;"><?php echo $fcobro; ?> </label>
                </div>
                <div class="col-md-4">
                    <label class="bold" for="">Fecha (Cobrar): </label>
                    <label for="" class="sub-rayado" style="font-size:17px!important;"><?php echo $cobrar; ?> </label>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6" style="padding:0;">
                    <div class="col-md-12">
                        <label class="bold" for="">Monto contrato: </label>
                        <label for="" class="sub-rayado" style="font-size:17px!important;"><?php echo $monto_contrato; ?> </label>
                    </div>
                    <div class="col-md-12">
                        <label class="bold" for="">Prima: </label>
                        <label for="" class="sub-rayado" style="font-size:17px!important;"><?php echo $prima; ?> </label>
                    </div>
                    <div class="col-md-12">
                        <label class="bold" for="">Neto: </label>
                        <label for="" class="sub-rayado" style="font-size:17px!important;"><?php echo $neto; ?> </label>
                    </div>
                    <div class="col-md-12">
                        <label class="bold" for="">Descuento: </label>
                        <label for="" class="sub-rayado" style="font-size:17px!important;"><?php echo $descuento; ?> </label>
                    </div>
                </div>
                <div class="col-md-6" style="padding:0;">
                    <div class="col-md-12">
                        <label class="bold" for="">Numero de cuotas: </label>
                        <label for="" class="sub-rayado" style="font-size:17px!important;"><?php echo $ncuotas; ?> </label>
                    </div>
                    <div class="col-md-12">
                        <label class="bold" for="">Monto de cuotas: </label>
                        <label for="" class="sub-rayado" style="font-size:17px!important;"><?php echo $cuota; ?> </label>
                    </div>
                    <div class="col-md-12">
                        <label class="bold" for="">Saldo pendiente: </label>
                        <label for="" class="sub-rayado" style="font-size:17px!important;"><?php echo $saldo; ?> </label>
                    </div>
                    <div class="col-md-12" style="visibility: hidden;height: 0;">
                        <label class="bold" for="">Cuenta: </label>
                        <label for="" class="sub-rayado"><?php echo $cuenta; ?> </label>
                    </div>
                    <div class="col-md-12" style="visibility: hidden;height: 0;">
                        <label class="bold" for="">Salida: </label>
                        <label for="" class="sub-rayado" style="font-size:17px!important;"><?php echo $salida; ?> </label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <label class="bold" for="">Cantidad de pagos: </label>
                    <label for="" class="sub-rayado" style="font-size:17px!important;"><?php echo $cantidad_pagos; ?> </label>
                </div>
                <div class="col-md-6">
                    <label class="bold" for="">Total pagadó: </label>
                    <label for="" class="sub-rayado" style="font-size:17px!important;"><?php echo $valor_pagado; ?> </label>
                </div>
            </div>
        
        </div>
    </div>