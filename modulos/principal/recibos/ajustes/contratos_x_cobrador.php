<?php
$r = '../../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');


$departamento = $_GET['dep'];
$ciudad = $_GET['ciudad'];
$cobrador = $_GET['cobrador'];
$fecha_cobro = $_GET['cobro'];

$con = "SELECT s.solid, depnombre, ciunombre, s.cobro, s.digitador, s.solobservacion, solrelacionista, carsaldo, carestado  
FROM solicitudes as s 
LEFT JOIN departamentos ON depid = s.soldepentrega
LEFT JOIN ciudades ON (ciudepto = s.soldepentrega AND ciuid = s.solciuentrega)
LEFT JOIN carteras ON carfactura = s.solfactura";

if($cobrador == "")
{
    echo 1;
    exit();
}

$parameters = [];
array_push($parameters, "s.solrelacionista = '$cobrador'" );
array_push($parameters, "carsaldo > '0'" );
array_push($parameters, "carestado = 'ACTIVA'" );
if($departamento != "")
    array_push($parameters, "s.soldepentrega = '$departamento'" );
if($ciudad != "" && $departamento != "")
    array_push($parameters, "s.solciuentrega = '$ciudad'" );
if($fecha_cobro != "")
    array_push($parameters, "s.cobro = '$fecha_cobro'" );


if(count($parameters) == 0)
{
    echo 1;
    exit();
}

// Consulta base
$sql = $con;
foreach ($parameters as $index => $parameter) {
    // Se agregan los parametros del WHERE
    if($index == 0)
        $sql .= " WHERE " . $parameter;
    else
        $sql .= " AND " . $parameter;
}


$ord = " ORDER BY s.solfecha ASC";
$sql .= $ord;

$contratos = $db->query($sql);
$contratos = $contratos->fetchAll(PDO::FETCH_ASSOC);

echo json_encode($contratos);

?>
