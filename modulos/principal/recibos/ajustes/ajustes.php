<?php
$r = '../../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

?>

<!doctype html>
<html lang="es">

<head>
    <title>AJUSTE DE CONTRATOS</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-not-form.php');
    ?>
    <style>
        #form input {
            text-transform: uppercase;
        }
        .divisor {
            margin-left: 5px;
            margin-right: 5px;
            margin-top: 15px;
            margin-bottom: 15px;
            padding-top: 10px;
            padding-bottom: 2px;
            background-color: #004593;
            color: #fff;
            border-radius: 8px;
        }
        .divisor h6 {
            font-weight: bold; 
        }
        tr td {
            position: relative;
        }
        .f-size-15 {
            font-size: 15px;
        }
        .nota-importante{
            border: #000 solid 2px;
            padding: 10px;
            border-radius: 18px;
            text-align: center;
            background: #00205f;
            color: white;
        }
        .div-fixed-start {
            position: fixed;
            z-index: 10;
            right: 10px;
            bottom: 10px;
        }
        .div-fixed-end {
            position: fixed;
            z-index: 10;
            right: 10px;
            bottom: 50px;
        }
        .excluidos {
            background: #ffe2fd;
        }
        .dataTables_scrollBody
        {
            max-height: 30em!important;
        }
    </style>
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
    <link rel="stylesheet" href="<?php echo $r.'incluir/librerias/fontawesome/css/all.css'?>" />


    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
    <script src="<?php echo $r.'incluir/librerias/fontawesome/js/all.js'?>"></script>

	<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>


    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css" />
    <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>   
     
    <script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>

    <script src="<?php echo $r.'incluir/kavv_js/kavvdt.js'?>" ></script>

	<script type="text/javascript">
        var $ciudades = [];
		$(document).ready(function() {
			$(".fecha").datepicker({
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
            });
            
            $("input").prop("autocomplete","off"); 

            $("#form").keypress(function(e) {
                if (e.which == 13) {
                    consultar_contratos();
                    return false;
                }
            });
            $("#form2").keypress(function(e) {
                if (e.which == 13) {
                    contratos_x_cobrador();
                    return false;
                }
            });

            // Almacenar las ciudades
            ruta = '../ajax/ciudades.php';
            $.get(ruta, function(res){
                res = JSON.parse(res);
                res.forEach(function(data){

                    var dep = data.ciudepto;
                    var ciu = data.ciuid;
                    var nombre = data.ciunombre;
                    // Si es diferente a null significa que el arreglo en el indice x ya posee datos
                    if($ciudades[dep] != null)
                    {
                        $ciudades[dep].push([ciu, nombre]);
                    }
                    else
                    {
                        // Si es la primera vez se asigna el primer dato como un arreglo
                        $ciudades[dep] = [[ciu, nombre]];
                    }
                });
            });
		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="<?php echo $r ?>index.php">Principal</a>
				<div class="mapa_div"></div><a>Contrato</a>
				<div class="mapa_div"></div><a>Reporte</a>
				<div class="mapa_div"></div><a class="current">Ajustes</a>
			</article>
			<article id="contenido">
            <div class="row">
                <div class="col-md-4">
                    <form id="form" name="form" class="form-style" action="#" method="post">

                        <fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12">
                            <legend class="ui-widget ui-widget-header ui-corner-all">Cambiar fechas de cobro en los contratos</legend>
                            <p>
                                <label for="Departamento">Departamento</label>
                                <select name="dep" id="departamento" class="form-control selectpicker validate[required]" data-live-search="true" title="Selecciona un departamento"  data-width="100%">
                                    <option value=""></option>
                                    <?php
                                    $departamentos = $db->query("select  * from departamentos order by depnombre;");
                                    while ($dep = $departamentos->fetch(PDO::FETCH_ASSOC)) { ?>
                                        <option value="<?php echo $dep['depid'] ?>"><?php echo $dep['depnombre'] ?></option>
                                    <?php } ?>
                                </select>
                            </p>
                            <p>
                                <label for="Departamento">Ciudad (OPCIONAL)</label>
                                <select id="ciudad" name="ciudad" class="selectpicker" data-live-search="true" title="Selecciona una ciudad" data-width="100%">
                                    <option value=""></option>
                                </select>
                            </p>
                            <p>
                                <label for="">Fecha de cobro <strong class="text-danger">ORIGINAL</strong></label>
                                <input type="text" class="form-control validate[required, custom[integer] ]" name='fco' id='fco'>
                            </p>
                            <p>
                                <label for="">Fecha de cobro <strong class="text-danger">NUEVA</strong></label>
                                <input type="text" class="form-control validate[required, custom[integer]]" name='fcn' id='fcn'>
                            </p>
                            <button onclick="consultar_contratos();" type="button" class="btnconsulta btn btn-primary btn-block my-2" name="consultar" value="Buscar">
                                Consultar
                            </button>
                        </fieldset>
                        
                    </form>
                </div>
                <div class="col-md-4">
                    <form id="form2" name="form2" class="form-style" action="#" method="post">


                        <fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12">
                            <legend class="ui-widget ui-widget-header ui-corner-all">Reasignar cartera a otro cobrador</legend>
                            
                            <p>
                                <label for="">Cobrador <strong class="text-danger">ORIGINAL</strong></label>
                                <select id="cobrador" name="cobrador" class="selectpicker validate[required]" data-live-search="true" title="Seleccione un cobrador" data-width="100%">
                                    <option value=""></option>
                                    <?php
                                        $query = $db->query("SELECT usuid, usunombre FROM usuarios WHERE usurelacionista = 1 ORDER BY usunombre");
                                        while($row = $query->fetch(PDO::FETCH_ASSOC))
                                        {
                                            $codigo = $row['usuid'];
                                            $nombre = "";
                                            if(TRIM($row['usunombre']) != "")
                                                $nombre .= "/" . $row['usunombre'];
                                            echo "<option value='". $row['usuid'] ."'>$codigo $nombre</option>";
                                        }
                                    ?>
                                </select>
                            </p>
                            <p>
                                <label for="">Cobrador <strong class="text-danger">NUEVO</strong></label>
                                <select id="cobrador_nuevo" name="cobrador_nuevo" class="selectpicker validate[required]" data-live-search="true" title="Cobrador que para asignar" data-width="100%">
                                    <option value=""></option>
                                    <?php
                                        $query = $db->query("SELECT usuid, usunombre FROM usuarios WHERE usurelacionista = 1 ORDER BY usunombre");
                                        while($row = $query->fetch(PDO::FETCH_ASSOC))
                                        {
                                            $codigo = $row['usuid'];
                                            $nombre = "";
                                            if(TRIM($row['usunombre']) != "")
                                                $nombre .= "/" . $row['usunombre'];
                                            echo "<option value='". $row['usuid'] ."'>$codigo $nombre</option>";
                                        }
                                    ?>
                                </select>
                            </p>
                            <p>
                                <label for="Departamento">Departamento</label>
                                <select name="dep" id="departamento2" class="form-control selectpicker" data-live-search="true" title="Selecciona un departamento"  data-width="100%">
                                    <option value=""></option>
                                    <?php
                                    $departamentos = $db->query("select  * from departamentos order by depnombre;");
                                    while ($dep = $departamentos->fetch(PDO::FETCH_ASSOC)) { ?>
                                        <option value="<?php echo $dep['depid'] ?>"><?php echo $dep['depnombre'] ?></option>
                                    <?php } ?>
                                </select>
                            </p>
                            <p>
                                <label for="Departamento">Ciudad (OPCIONAL)</label>
                                <select id="ciudad2" name="ciudad" class="selectpicker" data-live-search="true" title="Selecciona una ciudad" data-width="100%">
                                    <option value=""></option>
                                </select>
                            </p>
                            <p>
                                <label for="">Fecha de cobro (OPCIONAL)</label>
                                <input type="text" class="form-control validate[custom[integer] ]" name='cobro' id='cobro'>
                            </p>
                            <button onclick="contratos_x_cobrador();" type="button" class="btnconsulta btn btn-primary btn-block my-2" name="consultar" value="Buscar">
                                Consultar
                            </button>
                        </fieldset>
                        
                    </form>
                </div>
                <div class="col-md-4 pt-5">
                    <label for="">Debe tener en cuenta que estas funcionalidades son <strong>altamente peligrosas</strong> debido a la cantidad 
                    de cambios que puede realizar a los datos ya guardados, como <strong>por ejemplo</strong>  <strong class="text-danger">remover toda una cartera de un 
                    cobrador por error</strong>, esto es un proceso que muy dificilmente se podra revertir de una manera sistematizada, 
                    otro ejemplo seria <strong class="text-danger">cambiarle la fecha de cobro a los contratos equivocados</strong>,
                    esto de igual modo sería complicado desacerlo de una manera sistematizada. <br><strong>Por ellos siempre se le muestra 
                    una lista de los registros que seran afectados</strong> al usted confirmar la accion dando click en el boton 
                    <strong class="text-danger">ROJO</strong> que se encuentra en la parte inferior de la tabla</label>
                </div>
            </div>
            <div id="msj"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <table id="tabla-contratos" class="table table-hover" style="min-width: 1260px; max-height: 20em;">
                            <thead>
                                <tr>
                                    <th style="width:3%;">#</th>
                                    <th style="width:7%;">Contrato</th>
                                    <th style="width:15%;">Departamento</th>
                                    <th style="width:15%;">Ciudad</th>
                                    <th style="width:7%;">Fecha Cobro</th>
                                    <th style="width:10%;">Digitador</th>
                                    <th style="width:20%;">Observacion</th>
                                </tr>
                            </thead>
                            <tbody id="tb-contratos">
                            </tbody>
                        </table>
                        <table id="tabla-contratos-cobrador" class="table table-hover" style="min-width: 1260px; max-height: 20em;">
                            <thead>
                                <tr>
                                    <th style="width:3%;">#</th>
                                    <th style="width:7%;">Contrato</th>
                                    <th style="width:15%;">Departamento</th>
                                    <th style="width:15%;">Ciudad</th>
                                    <th style="width:7%;">Fecha Cobro</th>
                                    <th style="width:10%;">Cobrador</th>
                                    <th style="width:10%;">Digitador</th>
                                    <th style="width:20%;">Observacion</th>
                                </tr>
                            </thead>
                            <tbody id="tb-contratos-cobrador">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <button class="btn btn-block btn-danger" onclick="cambiar_fecha_cobro()" id="btn-cambiar"  style="display:none;">CAMBIAR FECHA DE COBRO PARA TODOS ESTOS CONTRATOS</button>
                <button class="btn btn-block btn-danger" onclick="cambiar_cobrador()" id="btn-cambiar-cartera"  style="display:none;">CAMBIAR EL COBRADOR PARA TODOS ESTOS CONTRATOS</button>
            </div>
            </article>
        </article>
    </section>
	<?php
	if (isset($_GET['error'])) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['error'] . '</div>';
	elseif (isset($_GET['mensaje'])) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['mensaje'] . '</div>';
	?>

    <!-- Modal -->
    <div class="modal fade" id="mensaje" data-backdrop="static" data-keyboard='false' tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <label for="">Este proceso puede demorar <strong>varios segundos</strong> <br>No recargue la página <br>Espere pacientemente</label>
                </div>
            </div>
        </div>
    </div>
    
	<?php require($r . 'incluir/src/loading.php'); ?>
</body>
<script>
    var dt, dt2;
    function cambiar_fecha_cobro()
    {
        var departamento = $("#departamento").val();
        var ciudad = $("#ciudad").val();
        var depname = $("#departamento option:selected").text();
        var ciuname = $("#ciudad option:selected").text();
        var fecha_cobro_original = $("#fco").val();
        var fecha_cobro_nueva = $("#fcn").val();

        var form = $("#form").validationEngine('validate');
        var ruta  = "cambiar_fecha_cobro.php?";
        ruta += $("#form").serialize();
        
        if(form && departamento != "" && fecha_cobro_nueva != "" && fecha_cobro_original != "" )
        {
            $("#loading").css("display","block");
            $.get(ruta, function(res){
                res = JSON.parse(res);
                if(res != 1)
                {
                    if(dt != null)
                    {
                        dt.destroy();
                        dt = null;
                    }
                    $('#tb-contratos').empty();
                    dt = createdt($("#tabla-contratos"),{buttons: bootstrapButtons, dom: bootstrapDesingSlimButtons});

                    var msj = 'Se actualizó la fecha de cobro para todos contratos que tenian fecha de '+fecha_cobro_original+' en  '+ depname + '/' + ciuname;
                    make_alert({'message':msj});
                    $("#btn-cambiar").css('display', 'none');
                    $("#loading").css("display","none");
                }
                else
                {
                    make_alert({'type':'danger', 'message':'Ocurrio un error al momento de actualizar algunos contratos'});
                    $("#loading").css("display","none");
                }
                $("#loading").css("display","none");
            });
        }
    }
    var g_dep, g_ciu, g_cobro, g_cobro_nuevo;
    function consultar_contratos()
    {
        var departamento = $("#departamento").val();
        var ciudad = $("#ciudad").val();
        var depname = $("#departamento option:selected").text();
        var ciuname = $("#ciudad option:selected").text();
        var fecha_cobro_original = $("#fco").val();
        var fecha_cobro_nueva = $("#fcn").val();
        

        var form = $("#form").validationEngine('validate');
        var ruta  = "consultar_contratos.php?";
        ruta += $("#form").serialize();
        
        /* g_dep = departamento;
        g_ciu =  ciudad;
        g_cobro = fecha_cobro_original;
        g_cobro_nuevo = fecha_cobro_nueva; */

        if(form && departamento != "" && fecha_cobro_nueva != "" && fecha_cobro_original != "" )
        {
            $("#loading").css("display","block");
            $.get(ruta, function(res){
                res = JSON.parse(res);
                if(res != 1)
                {
                    var msj = 'Esta lista contiene todos los contratos que tienen fecha de cobro '+fecha_cobro_original+' en  '+ depname + '/' + ciuname;
                    make_alert({'type':'info', 'message':msj});
                    
                    if(dt != null)
                    {
                        dt.destroy();
                        dt = null;
                    }
                    if(dt2 != null)
                    {
                        dt2.destroy();
                        dt2 = null;
                    }
                    
                    $("#tabla-contratos").css('display','block');
                    $("#tabla-contratos-cobrador").css('display','none');
                    $("#btn-cambiar-cartera").css('display', 'none');
                    $('#tb-contratos').empty();
                    var index = 1;
                    res.forEach(function(data){
                        if(data.digitador == null)
                            var digitador = "-";
                        if(data.observaciones == null)
                            var digitador = "-";
                        var html = "<tr>" +
                            "<td>"+index+"</td>"+
                            "<td>"+data.solid+"</td>"+
                            "<td>"+data.depnombre+"</td>"+
                            "<td>"+data.ciunombre+"</td>"+
                            "<td>"+data.cobro+"</td>"+
                            "<td>"+data.digitador+"</td>"+
                            "<td>"+data.solobservacion+"</td>"+
                        "</tr>";

                        index++;
                        $("#tb-contratos").append(html);
                    });
                    var pos = $("#tabla-contratos").offset().top;
                    $('html, body').animate({scrollTop: pos}, 'slow');
                    dt = createdt($("#tabla-contratos"),{buttons: bootstrapButtons, dom: bootstrapDesingSlimButtons});
                    if(index > 1)
                        $("#btn-cambiar").css('display', 'block');
                    $("#loading").css("display","none");
                }
                else
                {
                    make_alert({'type':'danger', 'message':'Ocurrio un error al momento de consultar los contratos'});
                    $("#loading").css("display","none");
                }
                $("#loading").css("display","none");
                
            });
        }
    }
    $("#departamento").change(function(){
        $("#ciudad").empty();
        // Por defecto
        var html = '<option value=""></option>';
        $("#ciudad").append(html);

        dep = $(this).val();
		if(dep == "")
			cant = 0;
		else
        cant = $ciudades[dep].length;
        for(var i = 0; i < cant; i++)
        {
            html = "<option value='" + $ciudades[dep][i][0] + "'>" + $ciudades[dep][i][1] + "</option>";
            $("#ciudad").append(html);
        }
        $('#ciudad').selectpicker('refresh');
    });

    $("#departamento2").change(function(){
        $("#ciudad2").empty();
        // Por defecto
        var html = '<option value=""></option>';
        $("#ciudad2").append(html);

        dep = $(this).val();
		if(dep == "")
			cant = 0;
		else
        cant = $ciudades[dep].length;
        for(var i = 0; i < cant; i++)
        {
            html = "<option value='" + $ciudades[dep][i][0] + "'>" + $ciudades[dep][i][1] + "</option>";
            $("#ciudad2").append(html);
        }
        $('#ciudad2').selectpicker('refresh');
    });
    function make_alert({object = $('#msj'), type = 'success', message = ''})
    {
        object.slideUp();
        object.empty();

        var alerta = 
        '<div class="alert alert-'+ type +' alert-dismissible fade show" role="alert">'+
            message +
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
                '<span aria-hidden="true">&times;</span>'+
            '</button>'+
        '</div>';

        object.append(alerta);
        object.slideDown();
    }
    
    function contratos_x_cobrador()
    {
        var departamento = $("#departamento2").val();
        var ciudad = $("#ciudad2").val();
        var depname = $("#departamento2 option:selected").text();
        var ciuname = $("#ciudad2 option:selected").text();
        var cobrador = $("#cobrador").val();
        

        var form = $("#form2").validationEngine('validate');
        var ruta  = "contratos_x_cobrador.php?";
        ruta += $("#form2").serialize();

        if(ciudad != "" && departamento == "")
        {
            alert("Al especificar una ciudad, es necesario que especifique tambien el departamento");
            return 1;
        }

        if(form && cobrador != "" )
        {
            $("#loading").css("display","block");
            $.get(ruta, function(res) {
                res = JSON.parse(res);
                if(res != 1)
                {
                    var msj = 'Esta lista contiene todos los contratos asignados al cobrador '+cobrador+' en  '+ depname + '/' + ciuname;
                    make_alert({'type':'info', 'message':msj});
                    
                    if(dt2 != null)
                    {
                        dt2.destroy();
                        dt2 = null;
                    }
                    if(dt != null)
                    {
                        dt.destroy();
                        dt = null;
                    }
                    
                    $("#tabla-contratos").css('display','none');
                    $("#tabla-contratos-cobrador").css('display','block');
                    $("#btn-cambiar").css('display', 'none');
                    $('#tb-contratos-cobrador').empty();
                    var index = 1;
                    res.forEach(function(data) {
                        if(data.digitador == null)
                            var digitador = "-";
                        if(data.observaciones == null)
                            var digitador = "-";
                        var html = "<tr>" +
                            "<td>"+index+"</td>"+
                            "<td>"+data.solid+"</td>"+
                            "<td>"+data.depnombre+"</td>"+
                            "<td>"+data.ciunombre+"</td>"+
                            "<td>"+data.cobro+"</td>"+
                            "<td>"+data.solrelacionista+"</td>"+
                            "<td>"+data.digitador+"</td>"+
                            "<td>"+data.solobservacion+"</td>"+
                        "</tr>";

                        index++;
                        $("#tb-contratos-cobrador").append(html);
                    });
                    var pos = $("#tabla-contratos-cobrador").offset().top;
                    $('html, body').animate({scrollTop: pos}, 'slow');
                    dt2 = createdt($("#tabla-contratos-cobrador"),{buttons: bootstrapButtons, dom: bootstrapDesingSlimButtons});
                    if(index > 1)
                        $("#btn-cambiar-cartera").css('display', 'block');
                    $("#loading").css("display","none");
                }
                else
                {
                    make_alert({'type':'danger', 'message':'Ocurrio un error al momento de consultar los contratos'});
                    $("#loading").css("display","none");
                }
                $("#loading").css("display","none");
                
            });
        }
    }
    function cambiar_cobrador ()
    {
        
        var departamento = $("#departamento2").val();
        var ciudad = $("#ciudad2").val();
        var depname = $("#departamento2 option:selected").text();
        var ciuname = $("#ciudad2 option:selected").text();
        var cobrador = $("#cobrador").val();
        var cobrador_name = $("#cobrador option:selected").text();
        var fecha_cobro = $("#cobro").val();
        

        var form = $("#form2").validationEngine('validate');
        var ruta  = "cambiar_cobrador.php?";
        ruta += $("#form2").serialize();
        
        if(form && cobrador != "" )
        {
            $("#loading").css("display","block");
            $.get(ruta, function(res){
                res = JSON.parse(res);
                if(res != 1)
                {
                    if(dt2 != null)
                    {
                        dt2.destroy();
                        dt2 = null;
                    }
                    $('#tb-contratos-cobrador').empty();
                    dt2 = createdt($("#tabla-contratos-cobrador"),{buttons: bootstrapButtons, dom: bootstrapDesingSlimButtons});

                    var msj = 'Se actualizó el cobrador a todos los contrato del ex-cobrador '+cobrador_name+' en  '+ depname + '/' + ciuname;
                    if(fecha_cobro != "")
                        msj += " con fecha de "+fecha_cobro;

                    make_alert({'message':msj});
                    $("#btn-cambiar-cartera").css('display', 'none');
                    $("#loading").css("display","none");
                }
                else
                {
                    make_alert({'type':'danger', 'message':'Ocurrio un error al momento de modificar el cobrador'});
                    $("#loading").css("display","none");
                }
                $("#loading").css("display","none");
            });
        }
    }
</script>