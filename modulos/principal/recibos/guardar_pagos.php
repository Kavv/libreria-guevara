<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
require($r . 'incluir/funciones.php');
require('funciones_contrato.php');

$contratos = [];
$recibos = [];
$montos = [];
$fecha_pagos = [];
$cobradores = [];
$contratos_fail = "";
if(isset($_POST['detalle-contrato']))
    $contratos = $_POST['detalle-contrato'];
if(isset($_POST['detalle-recibo']))
    $recibos = $_POST['detalle-recibo'];
if(isset($_POST['detalle-monto']))
    $montos = $_POST['detalle-monto'];
if(isset($_POST['detalle-fpago']))
    $fecha_pagos = $_POST['detalle-fpago'];
if(isset($_POST['detalle-codigo']))
    $cobradores = $_POST['detalle-codigo'];

// Aplicamos los pagos en caso de existir
$cantidad_pagos = count($contratos);
$movimiento_nuevo = true;

if($cantidad_pagos == 0)
{
    $html = 
    '<div class="alert alert-danger" alert-dismissible fade show" role="alert">'.
        'NO DIGITO NINGÚN PAGO' .
        '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'.
            '<span aria-hidden="true">&times;</span>'.
       ' </button>'.
    '</div>';
    echo $html;
    exit();
}

for($i = 0; $i < $cantidad_pagos; $i++)
{
    $contrato_aux = strupperEsp($contratos[$i]);
    $data = $db->query("SELECT 
    solempresa, solfactura, solcliente, carcuota, clinombre, clinom2, cliape1, cliape2 
    FROM solicitudes
    INNER JOIN carteras ON carfactura = solfactura
    INNER JOIN clientes ON cliid = solcliente 
    WHERE solid = '$contrato_aux' ")->fetch(PDO::FETCH_ASSOC);
    if($data)
    {
        $empresa = $data['solempresa'];
        $factura = $data['solfactura'];
        $cedula = $data['solcliente'];
        $cuota = $data['carcuota'];
        // La siguiente cuota
        $detcuota = $db->query("SELECT dcacuota, dcavalor FROM detcarteras 
        WHERE dcafactura = $factura AND dcaestado = 'ACTIVA' 
        ORDER BY dcacuota ASC LIMIT 1")->fetch(PDO::FETCH_ASSOC);
        if(!$detcuota)
        {
            $detcuota = $db->query("SELECT dcacuota, dcavalor FROM detcarteras 
            WHERE dcafactura = $factura 
            ORDER BY dcacuota DESC LIMIT 1")->fetch(PDO::FETCH_ASSOC);
        }

        $num_cuota = $detcuota['dcacuota'];
        $dcavalor = $detcuota['dcavalor'];

        
        // Si num_cuota es mayor a 1 o dcavalor es diferente a 0 entonces significa que no es la primer cuota de este contrato
        if($num_cuota > 1 || $dcavalor != 0)
            $comision = false;
        else
            $comision = true;

        // completar
        $nombre = $data['clinombre'];
        if( $data['clinom2'] != "" )
            $nombre .= " " . $data['clinom2'];
        if( $data['cliape1'] != "" )
            $nombre .= " " . $data['cliape1'];
        if( $data['cliape2'] != "" )
            $nombre .= " " . $data['cliape2'];
            

        $numero_recibo = $recibos[$i];
        if($numero_recibo == "")
        {
            // Obtenemos el ultimo recibo con numero negativo para poder asignar a todos aquellos pagos sin numero de recibo
            $recibo_automatico = $db->query("select nextval('sec_factura_negativa') as factura")->fetch(PDO::FETCH_ASSOC);
            if($recibo_automatico)
                $numero_recibo = $recibo_automatico['factura'];
            else 
                $numero_recibo = -1;
        }
        $digitador = $_SESSION['id'];
        $comentario = "";
        $db->query("INSERT INTO infoextra 
        (comentario, digitador)
        VALUES ('$comentario', '$digitador');") or die($db->errorInfo()[2]);
        
        $id_info = $db->lastInsertId();

        pagar($db, $contrato_aux, $empresa, $factura, $cedula,
        $cuota, $num_cuota, $montos[$i], 0, $fecha_pagos[$i], 1, $cobradores[$i], 
        $nombre, $numero_recibo, $id_info,
        $movimiento_nuevo, $comision, 0);
        
    }
    else
    {
        $contratos_fail = " " . $contrato_aux . ",";
    }
}

if($contratos_fail != "")
{
    
    $html = 
    '<div class="alert alert-danger" alert-dismissible fade show" role="alert">'.
        '¡ERROR! El contrato '.$contrato_aux.' no fue encontrado y sus pagos no se han registrado' .
        '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'.
            '<span aria-hidden="true">&times;</span>'.
       ' </button>'.
    '</div>';
    echo $html;
    exit();
}
else
{
    $html = 
    '<div class="alert alert-success" alert-dismissible fade show" role="alert">'.
        '¡Guardo Exitoso!' .
        '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'.
            '<span aria-hidden="true">&times;</span>'.
       ' </button>'.
    '</div>';
    echo $html;
    exit();
}


?>