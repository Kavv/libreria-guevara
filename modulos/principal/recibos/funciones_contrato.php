<?php
    // Toma el nombre completo y lo segmenta en max 4 partes
    function asignar_nombre($nombre_completo)
    {
        $nombre_completo = trim($nombre_completo);
        global  $nombre1, $nombre2, $apellido1, $apellido2;
        $partes = explode(" ", $nombre_completo);
        for($i = 0; $i < count($partes); $i++)
        {
            if($i == 0)
                $nombre1 = $partes[$i];
            if($i == 1)
                $nombre2 = $partes[$i];
            if($i == 2)
                $apellido1 = $partes[$i];
            if($i == 3)
                $apellido2 = $partes[$i];
            if($i > 3)
                $apellido2 .= " ".$partes[$i];
        }
    }
    
    // Gestiona el pago realizado por 1 recibo
    function pagar($db, $contrato, $empresa, $factura, $cedula, 
    $vcuota, $num_cuota, $vaplicar, $descuento, $fecpago, $fpago, $cobrador, 
    $nombre, $recibo, $infoextra,
    $movimiento_nuevo, $comision, $msj_tipo)
    {
		// Valor original
		$vaplicar_org = $vaplicar;
		// Descuento original
		$descuento_org = $descuento;
		
        // Obtenemos el saldo de la cartera en el momento actual (ya que este cambia por las iteraciones)
        $aux_cartera = $db->query("SELECT carsaldo FROM carteras WHERE carempresa = '$empresa' AND carfactura = '$factura'");
        $aux_cartera = $aux_cartera->fetch(PDO::FETCH_ASSOC);
        $saldo = $aux_cartera['carsaldo'];

        $nuevo_saldo = round($saldo-($vaplicar + $descuento), 2);
		$db->query("UPDATE carteras SET carsaldo = $nuevo_saldo WHERE carempresa = '$empresa' AND carfactura = '$factura'") or die($db->errorInfo()[2]);;
        
        while ($vaplicar >= 0.01) 
        {
            $overflow = false;
            //echo "SELECT * FROM detcarteras WHERE dcaempresa = '$empresa' AND dcafactura = '$numero' AND dcacuota = $cuota"."<br>";
            $rowdet = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '$empresa' AND dcafactura = '$factura' AND dcacuota = $num_cuota")->fetch(PDO::FETCH_ASSOC);
            if(!$rowdet)
            {
                // Si entra aqui es por que no encontro la cuota, por lo tanto significa que aun hay un valor por aplicar superitor a 1
                // Esto indica un que lo que se pago es mayor al valor total a pagarse
                // Se forzara una edicion en la ultima cuota dejandola con valores negativos que demuestren el exceso de cobro
                $num_cuota--;
                $overflow = true;
                $rowdet = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '$empresa' AND dcafactura = '$factura' AND dcacuota = $num_cuota")->fetch(PDO::FETCH_ASSOC);
                if($rowdet)
                {
                    $temp_valor = $rowdet['dcavalor'];
                    $temp_saldo = $rowdet['dcasaldo'];
                    $overflow_saldo = round($temp_saldo - $vaplicar,2);
                    $overflow_valor = round($temp_valor + $vaplicar, 2);
                    $db->query("UPDATE detcarteras SET dcavalor = $overflow_valor , dcasaldo = $overflow_saldo, dcafepag = '$fecpago', dcapromotor = '$cobrador', dcaestado = 'CANCELADO' WHERE dcaempresa = '$empresa' AND dcafactura = '$factura' AND dcacuota = $num_cuota") or die("ocurrio un error 1 - detcartera");
                    $vaplicar = 0;
                    $saldo = $overflow_saldo;
                    break;
                }
            }
            if(!$overflow)
            {
                $acumulado = $rowdet['dcavalor'] + $rowdet['dcadescuento'];
                if ($acumulado == 0) {
                    if ($vaplicar >= $vcuota) {
                        $saldo = round($saldo - $vcuota, 2);
                        //echo "$vaplicar >= $vcuota";
                        //echo "<br>";
                        $db->query("UPDATE detcarteras SET dcavalor = $vcuota, dcasaldo = $saldo, dcafepag = '$fecpago', dcapromotor = '$cobrador', dcaestado = 'CANCELADO' WHERE dcaempresa = '$empresa' AND dcafactura = '$factura' AND dcacuota = $num_cuota") or die("ocurrio un error 1 - detcartera");
                        $vaplicar = $vaplicar - $vcuota;
                        $num_cuota++;
                    } else {
                        $saldo = round($saldo - $vaplicar, 2);
                        //echo "$vaplicar < $vcuota";
                        //echo "<br>";
                        $db->query("UPDATE detcarteras SET dcavalor = $vaplicar, dcasaldo = $saldo, dcafepag = '$fecpago', dcapromotor = '$cobrador' WHERE dcaempresa = '$empresa' AND dcafactura = '$factura' AND dcacuota = $num_cuota") or die("ocurrio un error 2 - detcartera");
                        $vaplicar = 0;
                    }
                } else {
                    if ($vaplicar >= ($vcuota - $acumulado)) {
                        //echo "$vaplicar >= $vcuota - $acumulado";
                        $saldo = round($saldo - ($vcuota - $acumulado), 2);
                        //echo "<br>";
                        $db->query("UPDATE detcarteras SET dcavalor = $vcuota - dcadescuento, dcasaldo = $saldo, dcafepag = '$fecpago', dcapromotor = '$cobrador', dcaestado = 'CANCELADO' WHERE dcaempresa = '$empresa' AND dcafactura = '$factura' AND dcacuota = $num_cuota") or die("ocurrio un error 3 - detcartera");
                        $vaplicar = $vaplicar - ($vcuota - $acumulado);
                        $num_cuota++;
                    } else {
                        $saldo = round($saldo - $vaplicar, 2);
                        //echo "$vaplicar < $vcuota";
                        //echo "<br>";
                        $db->query("UPDATE detcarteras SET dcavalor = dcavalor + $vaplicar, dcasaldo = $saldo, dcafepag = '$fecpago', dcapromotor = '$cobrador' WHERE dcaempresa = '$empresa' AND dcafactura = '$factura' AND dcacuota = $num_cuota") or die("ocurrio un error 4 - detcartera");
                        $vaplicar = 0;
                    }
                }
            }
        }
        $mensaje = ""; 
        if($movimiento_nuevo)
        {
            // Creamos el movimiento correspondiente al recibo
            $aux_movimiento = $db->query("SELECT * FROM movimientos WHERE movempresa = '$empresa' AND movprefijo = 'RC' AND movNumero = '$recibo'")->fetch(PDO::FETCH_ASSOC);
            if(!$aux_movimiento)
            {
                $db->query("INSERT INTO movimientos (movempresa, movprefijo, movnumero, movtercero, movdocumento, movfecha, movvalor, movdescuento, movsaldo, movfpago, movestado, movcobrador, movinfo) VALUES ('$empresa', 'RC', '$recibo', '$cedula', '$factura', '$fecpago', $vaplicar_org, $descuento_org, $saldo, '$fpago', 'FINALIZADO', '$cobrador', '$infoextra')") or die($db->errorInfo()[2]);
                $mensaje = $_SESSION['id']." GENERO RECIBO NUMERO  $recibo - $empresa AL CONTRATO $contrato A NOMBRE DEL CLIENTE $nombre POR UN VALOR DE $vaplicar_org QUEDANDO UN SALDO DE $saldo";
            }
        }
        else
        {
            if($msj_tipo == 1)
                $mensaje = $_SESSION['id']." MODIFICACIÓN DEL RECIBO $recibo - $empresa DEL CONTRATO $contrato POR UN VALOR DE $vaplicar_org QUEDANDO UN SALDO DE $saldo";
        }
        // Si el saldo es inferior a 1 (por error de redondeo no es 0), damos por cancelada la cartera
        if ($saldo < 1) {
			$db->query("UPDATE carteras SET carestado = 'CANCELADO' WHERE carempresa = '$empresa' AND carfactura = '$factura'") or die($db->errorInfo()[2]);;
		}
        // Obtenemos la solicitud con la que estamos trabajando
		$rowsoli = $db->query("SELECT * FROM solicitudes where solid ='$contrato' AND solempresa = '$empresa' ")->fetch(PDO::FETCH_ASSOC);
		$asesorsoli = $rowsoli['solasesor'];
		$empresasoli = $rowsoli['solempresa'];

        if($comision)
            comision($db, $asesorsoli, $empresasoli, $contrato);
		
		$qrylogsregister = $db->query("SELECT * FROM usuarios WHERE usuid = '".$_SESSION['id']."'"); //verificacion usuario por ID de sesion
        $rowlogsregister = $qrylogsregister->fetch(PDO::FETCH_ASSOC);	
        // Log de la acción realizada
        if($mensaje != "")
            $db->query("INSERT INTO logs ( usuario, id_usuario , ip_usuario , accion_usuario, fecha_accion) VALUES 
            ( '".$rowlogsregister['usunombre']."', '".$_SESSION['id']."', '".$_SERVER['REMOTE_ADDR']."' , 
            '$mensaje' , '".date("Y-m-d H:i:s")."' );") or die($db->errorInfo()[2]);
	
    }
    // Gestiona la inserción correcta de la comisión.
    function comision($db, $asesorsoli, $empresasoli, $contrato)
    {
        $rowsoli = $db->query("SELECT * FROM usuarios WHERE usuid = '$asesorsoli'")->fetch(PDO::FETCH_ASSOC);
        
        if($rowsoli['usucomision'] == "")
            $rowsoli['usucomision'] = 0;
        if($rowsoli['usucomision1'] == "")
            $rowsoli['usucomision1'] = 0;
        if($rowsoli['usucomision2'] == "")
            $rowsoli['usucomision2'] = 0;
        if($rowsoli['usucomision3'] == "")
            $rowsoli['usucomision3'] = 0;

        if($rowsoli['usupover1'] == "")
            $rowsoli['usupover1'] = 0;
        if($rowsoli['usupover2'] == "")
            $rowsoli['usupover2'] = 0;
        if($rowsoli['usupover3'] == "")
            $rowsoli['usupover3'] = 0;
        if($rowsoli['usupover4'] == "")
            $rowsoli['usupover4'] = 0;

        // Asignamos el valor por comision del vendedor
        $db->query("UPDATE solicitudes SET 
        solcomision = " . $rowsoli['usucomision'] . ", solcomision1 = " . $rowsoli['usucomision1'] . ", 
        solcomision2 = " . $rowsoli['usucomision2'] . ", solcomision3 = " . $rowsoli['usucomision3'] . ", 
        solover1 = '" . $rowsoli['usuover1'] . "', solpover1 = " . $rowsoli['usupover1'] . ", 
        solover2 = '" . $rowsoli['usuover2'] . "', solpover2 = " . $rowsoli['usupover2'] . ", 
        solover3 = '" . $rowsoli['usuover3'] . "', solpover3 = " . $rowsoli['usupover3'] . ", 
        solover4 = '" . $rowsoli['usuover4'] . "', solpover4 = " . $rowsoli['usupover4'] . " 
        WHERE solempresa = '$empresasoli' AND solid = '$contrato'") or die($db->errorInfo()[2]);
    }

    // Define que valores son diferentes al original y solo actualiza esos
    function actualizar_cliente($cliente)
    {
        global $db, $cedula, $nombre1, $nombre2, $apellido1, $apellido2, $telefono1, $telefono2, $departamento, $ciudad, $trabajo, $direccion, $conyugue, $conyugue_telefono, $conyugue_trabajo, $referencia1, $ref1_tel1, $ref1_tel2, $referencia2, $ref2_tel1, $ref2_tel2;
        $query = [];
        if($cliente['clinombre'] != $nombre1 && $nombre1!="")
            array_push($query, "clinombre = '$nombre1'");
        if($cliente['clinom2'] != $nombre2 && $nombre2!="")
            array_push($query, "clinom2 = '$nombre2'");
        if($cliente['cliape1'] != $apellido1 && $apellido1!="")
            array_push($query, "cliape1 = '$apellido1'");
        if($cliente['cliape2'] != $apellido2 && $apellido2!="")
            array_push($query, "cliape2 = '$apellido2'");
        if($cliente['clicelular'] != $telefono1 && $telefono1!="")
            array_push($query, "clicelular = '$telefono1'");
        if($cliente['clitelresidencia'] != $telefono2 && $telefono2!="")
            array_push($query, "clitelresidencia = '$telefono2'");
        if($cliente['clidepresidencia'] != $departamento && $departamento!="")
            array_push($query, "clidepresidencia = '$departamento'");
        if($cliente['cliciuresidencia'] != $ciudad && $ciudad!="")
            array_push($query, "cliciuresidencia = '$ciudad'");
        if($cliente['cliempresa'] != $trabajo && $trabajo!="")
            array_push($query, "cliempresa = '$trabajo'");
        if($cliente['clidirresidencia'] != $direccion && $direccion!="")
            array_push($query, "clidirresidencia = '$direccion'");
        if($cliente['clinomfamiliar'] != $conyugue && $conyugue!="")
            array_push($query, "clinomfamiliar = '$conyugue'");
        if($cliente['clitelfamiliar'] != $conyugue_telefono && $conyugue_telefono!="")
            array_push($query, "clitelfamiliar = '$conyugue_telefono'");
        if($cliente['cliempfamiliar'] != $conyugue_trabajo && $conyugue_trabajo!="")
            array_push($query, "cliempfamiliar = '$conyugue_trabajo'");
        if($cliente['clirefnombre1'] != $referencia1 && $referencia1!="")
            array_push($query, "clirefnombre1 = '$referencia1'");
        if($cliente['clireftelefono1'] != $ref1_tel1 && $ref1_tel1!="")
            array_push($query, "clireftelefono1 = '$ref1_tel1'");
        if($cliente['clirefcelular1'] != $ref1_tel2 && $ref1_tel2!="")
            array_push($query, "clirefcelular1 = '$ref1_tel2'");
        if($cliente['clirefnombre2'] != $referencia2 && $referencia2!="")
            array_push($query, "clirefnombre2 = '$referencia2'");
        if($cliente['clireftelefono2'] != $ref2_tel1 && $ref2_tel1!="")
            array_push($query, "clireftelefono2 = '$ref2_tel1'");
        if($cliente['clirefcelular2'] != $ref2_tel2 && $ref2_tel2!="")
            array_push($query, "clirefcelular2 = '$ref2_tel2'");

        // Si es igual 0 significa que todos los datos del cliente son iguales
        if(count($query) > 0)
        {
            $sql = "UPDATE clientes SET";
            foreach ($query as $index => $parameter) {
                // Se agregan los parametros del WHERE
                if ($index == 0)
                    $sql .= " " . $parameter;
                else
                    $sql .= ", " . $parameter;
            }
            // Se completa la consulta
            $sql .= " WHERE cliid = '$cedula'";
    
            $db->query($sql);
        }
    }

?>