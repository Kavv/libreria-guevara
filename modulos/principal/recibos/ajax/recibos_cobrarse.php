<?php

$r = '../../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');


    $fecha = $_GET['fecha'];
    $cobrador = $_GET['cobrador'];
    $departamento = $_GET['departamento'];
    $ciudad = $_GET['ciudad'];
    $desde = $_GET['desde'];
    $hasta = $_GET['hasta'];
    
    $parameters = [];
    array_push($parameters, "s.cobro = '$fecha'");
    array_push($parameters, "s.solrelacionista <> 'CALLCENTER'");
    array_push($parameters, "ca.carestado = 'ACTIVA'");
    if($cobrador != "")
        array_push($parameters, "s.solrelacionista = '$cobrador'" );
    if($departamento != "")
        array_push($parameters, "s.soldepentrega = '$departamento'" );
    if($ciudad != "")
        array_push($parameters, "s.solciuentrega = '$ciudad'" );
    if($desde != "" && $hasta != "")
        array_push($parameters, "solfecha BETWEEN '$desde' AND '$hasta'" );

    if(!isset($_GET['status']))
    {
        // Consulta base recibos
        $consulta_base = "SELECT s.solfactura, s.solid, s.solrelacionista, ca.carcuota, s.soldepcobro, d.depnombre, c.ciunombre
        FROM solicitudes as s
        INNER JOIN carteras as ca on ca.carfactura = s.solfactura
        LEFT JOIN departamentos as d on d.depid = s.soldepcobro
        LEFT JOIN ciudades as c on (c.ciuid = s.solciucobro and c.ciudepto = s.soldepcobro)";
        
        $group = "";
        $ord = " ORDER BY c.ciunombre, s.solid ASC";
    }

    if(isset($_GET['status']))
    {
        if($fecha != "")
        {
            // Consulta base cobradores con contratos asignados
            $consulta_base = "SELECT u.usuid, u.usunombre
            FROM solicitudes as s
            INNER JOIN carteras as ca on ca.carfactura = s.solfactura
            INNER JOIN usuarios as u on u.usuid = s.solrelacionista";
        
            $group = " GROUP BY u.usuid, u.usunombre";
            $ord = " ORDER BY u.usunombre ASC";
        }
        else
        {
            // En caso de que la fecha no sea establecida se retornará todos los cobradores
            $cobradores = $db->query("SELECT usuid, usunombre FROM usuarios where usurelacionista = 1 ORDER BY usunombre ASC");

            $cobradores = $cobradores->fetchAll(PDO::FETCH_ASSOC);
            echo json_encode($cobradores);
            exit();
        }

    }

    $sql = $consulta_base;
    foreach ($parameters as $index => $parameter) {
        // Se agregan los parametros del WHERE
        if($index == 0)
            $sql .= " WHERE " . $parameter;
        else
            $sql .= " AND " . $parameter;
    }
    $sql .= $group;
    $sql .= $ord;

    $recibos = $db->query($sql);

$recibos = $recibos->fetchAll(PDO::FETCH_ASSOC);
echo json_encode($recibos);

?>