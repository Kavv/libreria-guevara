<?php

$r = '../../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

require($r . 'incluir/funciones.php');
require('../funciones_contrato.php');

$factura = $_POST['factura'];
$contrato_orignal = $_POST['contrato_orignal'];
$contrato_edit = $_POST['contrato_edit'];
$recibo_original = $_POST['recibo_original'];
$recibo_edit = $_POST['recibo_edit'];
$monto_edit = $_POST['monto_recibo'];
$fecha_edit = $_POST['detalle_fpago'];
$cobrador_edit = $_POST['cobrador'];


$contrato_aux = $db->query("SELECT solfactura, solcliente FROM solicitudes WHERE solid = '$contrato_edit'")->fetch(PDO::FETCH_ASSOC);
if($contrato_aux)
{
    $factura_edit = $contrato_aux['solfactura']; 
    $cliente_edit = $contrato_aux['solcliente'];
}
else
{
    echo "No se encontro el contrato($contrato_edit)";
    exit();
}

// Obtenemos el ultimo recibo con numero negativo para poder asignar a todos aquellos pagos sin numero de recibo
$recibo_automatico = $db->query("SELECT movnumero FROM movimientos WHERE movprefijo = 'RC' AND movnumero < 0 ORDER BY movnumero ASC LIMIT 1")->fetch(PDO::FETCH_ASSOC);
if($recibo_automatico)
    $recibo_automatico = $recibo_automatico['movnumero'] - 1;
else 
    $recibo_automatico = -1;

if($recibo_edit == "")
{
    $recibo_edit = $recibo_automatico;
    $recibo_automatico--;
}

// Obtener el movimiento que se esta editando
//$movimiento = $db->query("SELECT * FROM movimientos WHERE movprefijo = 'RC' AND movnumero = '$recibo_original'")->fetch(PDO::FETCH_ASSOC);

//$detcarteras = $db->query("SELECT * FROM detcarteras WHERE dcafactura = '$factura'");

$db->query("UPDATE detcarteras SET dcavalor = 0, dcasaldo = 0, dcapromotor = null, dcafepag = null, dcaestado = 'ACTIVA' WHERE dcafactura = '$factura'") or die("Detalle cartera / ".$db->errorInfo()[2]);
$saldo_aux = $db->query("SELECT movdescuento, movsaldo from movimientos WHERE movdocumento = '$factura' AND movprefijo = 'FV'")->fetch(PDO::FETCH_ASSOC);
// Error
//$saldo_original = round(($saldo_aux['movsaldo'] - $saldo_aux['movdescuento']), 2);
// En teoria movsaldo cuando es FV es equivalente al total-prima-descuento
$saldo_original = $saldo_aux['movsaldo'];

$movimientos = $db->query("UPDATE carteras SET carsaldo = $saldo_original, carestado = 'ACTIVA' WHERE carfactura = '$factura'") or die("cartera / ".$db->errorInfo()[2]);

$movimientos = $db->query("SELECT movimientos.* , carteras.carcuota FROM movimientos INNER JOIN carteras ON carfactura = movdocumento WHERE movprefijo = 'RC' AND movdocumento = '$factura' ORDER BY movfecha ASC");
$total = 0;
$actualizar_siguientes = false;
$comision = true;
$ultimo_saldo = 0;
$movimiento_nuevo = false;
$actualizar_otra_cartera = false;
while ($movimiento = $movimientos->fetch(PDO::FETCH_ASSOC)) {
        
    $param_recibo = $movimiento['movnumero'];
    $param_documento = $movimiento['movdocumento'];
    $param_cliente = $movimiento['movtercero'];
    $param_fecha = $movimiento['movfecha'];
    $param_monto = $movimiento['movvalor'];
    $param_saldo = $movimiento['movsaldo'];
    $param_cobrador = $movimiento['movcobrador'];
    $actualizado = false;
    if($param_recibo == $recibo_original)
    {
        if($param_recibo != $recibo_edit)
        {
            $exist_recibo = $db->query("SELECT * FROM movimientos WHERE movnumero = '$recibo_edit' AND movprefijo = 'RC';")->fetch(PDO::FETCH_ASSOC);
            if($exist_recibo)
            {
                echo "¡El # de Recibo ($recibo_edit) ya lo tiene asignado otro pago!";
                exit();
            }
            else
            {
                $param_recibo = $recibo_edit;
                $actualizado = true;
            }
        }
        if($param_fecha != $fecha_edit)
        {
            $param_fecha = $fecha_edit;
            $actualizado = true;
        }
        if($param_monto != $monto_edit)
        {
            // Obtenemos el saldo sobre el cual estaran siendo actualizados los siguientes movimientos a este
            $ultimo_saldo = $movimiento['movvalor'] + $movimiento['movsaldo'];

            $param_monto = $monto_edit;
            $actualizado = true;
        }

        if($param_cobrador != $cobrador_edit)
        {
            $param_cobrador = $cobrador_edit;
            $actualizado = true;
        }


        if($param_documento != $factura_edit)
        {   
            $param_documento = $factura_edit;
            $param_cliente = $cliente_edit;
            // En caso de que se cambie de contrato/factura debemos resetiar 
            // el detalle de la cartera del contrato al que se esta siendo asignado el pago
            $actualizar_otra_cartera = true;
            // resetiamos detalle de la cartera del contrato al que se esta siendo asignado el pago
            //$db->query("UPDATE detcarteras SET dcavalor = 0, dcasaldo = 0, dcapromotor = null, dcafepag = null, dcaestado = 'ACTIVA' WHERE dcafactura = '$param_documento'");
            
            //
            $actualizado = true;
        }
        
        $actualizar_siguientes = true;
        $db->query("UPDATE movimientos SET 
        movnumero = '$param_recibo', movtercero = '$param_cliente', movdocumento = '$param_documento', movfecha = '$param_fecha',  
        movvalor = '$param_monto', movsaldo = 0, movcobrador = '$param_cobrador'
        WHERE movnumero = '$recibo_original' AND movprefijo = 'RC'") or die("Linea 108".$db->errorInfo()[2]);
        
        if(!$actualizar_otra_cartera)
        {
            $msj = 0;
            if($actualizado)
                $msj = 1;
            pagar($db, $contrato_orignal, $empresa, $param_documento, $param_cliente,
            $cuota, 1, $param_monto, 0, $param_fecha, 1, $param_cobrador, 
            $nombre, $param_recibo, null,
            $movimiento_nuevo, $comision, $msj);

            if($comision)
                $comision = false;
        }
    }
    else
    {
        
        $msj = 0;
        // Trabajamos el detalle de la cartera pero sin crear un movimiento nuevo
        $empresa = '16027020274133';
        $cuota = $movimiento['carcuota'];
        $nombre = '';
        if($actualizar_siguientes)
        {
            // El nuevo saldo
            $saldo_edit = $ultimo_saldo - $param_monto;
            $param_saldo = $saldo_edit;
            // cambiamos el estado del msj para que se genere un msj a guardar como log
            $msj = 1;

            $db->query("UPDATE movimientos SET 
            movsaldo = '$param_saldo'
            WHERE movnumero = '$recibo_original' AND movprefijo = 'RC'") or die("Line 139".$db->errorInfo()[2]);
        }
        
        pagar($db, $contrato_orignal, $empresa, $param_documento, $param_cliente,
        $cuota, 1, $param_monto, 0, $param_fecha, 1, $param_cobrador, 
        $nombre, $param_recibo, null,
        $movimiento_nuevo, $comision, $msj);

        if($comision)
            $comision = false;
    }

}
if($actualizar_otra_cartera)
    actualizar_cartera($db, $factura_edit, $recibo_edit, $contrato_edit);

echo true;
exit();

function actualizar_cartera($db, $factura, $recibo, $contrato)
{
    $db->query("UPDATE detcarteras SET dcavalor = 0, dcasaldo = 0, dcapromotor = null, dcafepag = null, dcaestado = 'ACTIVA' WHERE dcafactura = '$factura'") or die("Line 160".$db->errorInfo()[2]);
    // Obtenemos el saldo con el que arranca originalmente la carteras (total de contrato - descuento - prima)
    $saldo_aux = $db->query("SELECT movdescuento, movsaldo from movimientos WHERE movdocumento = '$factura' AND movprefijo = 'FV'")->fetch(PDO::FETCH_ASSOC);
    // Error
    // $saldo_original = round(($saldo_aux['movsaldo'] - $saldo_aux['movdescuento']), 2);
    // En teoria movsaldo cuando es FV es equivalente al total-prima-descuento
    $saldo_original = $saldo_aux['movsaldo'];
    $db->query("UPDATE carteras SET carsaldo = $saldo_original, carestado = 'ACTIVA' WHERE carfactura = '$factura'") or die("Line 165".$db->errorInfo()[2]);

    $saldo_temp = $saldo_original;
    $movimientos = $db->query("SELECT movimientos.*, carteras.carcuota FROM movimientos INNER JOIN carteras ON carfactura = movdocumento WHERE movprefijo = 'RC' AND movdocumento = '$factura' ORDER BY movfecha ASC");

    $actualizar_siguientes = false;
    $movimiento_nuevo = false;
    $comision = true;
    $msj = 0;
    while ($movimiento = $movimientos->fetch(PDO::FETCH_ASSOC)) {
        
        $param_recibo = $movimiento['movnumero'];
        $param_documento = $movimiento['movdocumento'];
        $param_cliente = $movimiento['movtercero'];
        $param_fecha = $movimiento['movfecha'];
        $param_monto = $movimiento['movvalor'];
        $param_saldo = $movimiento['movsaldo'];
        $param_cobrador = $movimiento['movcobrador'];

        $saldo_temp = round(($saldo_temp - $param_monto), 2);

        if($param_recibo == $recibo)
        {
            $actualizar_siguientes = true;
        }
        
        if($actualizar_siguientes)
        {
            // cambiamos el estado del msj para que se genere un msj a guardar como log
            $msj = 1;

            $db->query("UPDATE movimientos SET 
            movsaldo = '$saldo_temp'
            WHERE movnumero = '$recibo' AND movprefijo = 'RC'") or die("Line 198".$db->errorInfo()[2]);
        }
        // Trabajamos el detalle de la cartera pero sin crear un movimiento nuevo
        $empresa = '16027020274133';
        $cuota = $movimiento['carcuota'];
        $nombre = '';

        pagar($db, $contrato, $empresa, $param_documento, $param_cliente,
        $cuota, 1, $param_monto, 0, $param_fecha, 1, $param_cobrador, 
        $nombre, $param_recibo, null,
        $movimiento_nuevo, $comision, $msj);

        if($comision)
            $comision = false;
    }
}
// Del movimiento sacar el # de factura
// Buscar todos los detcarteras relacionados a la factura, modificar a 0 o null dcavalor, dcafepag, dcasaldo, dcpromotor, dcaestado.
// Tomar cada movimiento con movprefijo RC y con movdocument igual a la factura (ordenado por movfecha) y hacer uso de la funcion pagar()
