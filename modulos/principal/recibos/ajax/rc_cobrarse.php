<?php

$r = '../../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');


    $fecha_cobrarse = $_GET['fecha'];
    $cobrador = $_GET['cobrador'];
    $departamento = $_GET['departamento'];
    $ciudad = $_GET['ciudad'];
    
    $parameters = [];
    array_push($parameters, "r.cobrarse = '$fecha_cobrarse'");
    if($cobrador != "")
        array_push($parameters, "r.asignado = '$cobrador'" );
    if($departamento != "")
        array_push($parameters, "s.soldepentrega = '$departamento'" );
    if($ciudad != "")
        array_push($parameters, "s.solciuentrega = '$ciudad'" );

    if(!isset($_GET['status']))
    {
        // Consulta base recibos
        $consulta_base = "SELECT r.id, r.recibo, r.monto, s.solfactura, r.contrato, r.asignado, s.soldepcobro, d.depnombre, c.ciunombre, r.observaciones
        FROM recibos_a_cobrarse as r
        LEFT JOIN solicitudes as s ON s.solid = r.contrato
        LEFT JOIN departamentos as d on d.depid = s.soldepcobro
        LEFT JOIN ciudades as c on (c.ciuid = s.solciucobro and c.ciudepto = s.soldepcobro)";
        
        // $group = " GROUP BY r.numero, s.solfactura, r.contrato, r.asignado, s.soldepcobro, d.depnombre, c.ciunombre, r.observaciones";
        $group = "";
        $ord = " ORDER BY c.ciunombre, r.contrato ASC";
    }

    $sql = $consulta_base;
    foreach ($parameters as $index => $parameter) {
        // Se agregan los parametros del WHERE
        if($index == 0)
            $sql .= " WHERE " . $parameter;
        else
            $sql .= " AND " . $parameter;
    }
    $sql .= $group;
    $sql .= $ord;
    $recibos = $db->query($sql);

$recibos = $recibos->fetchAll(PDO::FETCH_ASSOC);
echo json_encode($recibos);

?>