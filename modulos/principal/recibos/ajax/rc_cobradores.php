<?php

$r = '../../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

$fecha_cobrarse = $_GET['fecha_cobrarse'];

if($fecha_cobrarse != "")
{
    $sql = "SELECT u.usuid, u.usunombre FROM recibos_a_cobrarse 
    INNER JOIN usuarios as u ON asignado = u.usuid 
    WHERE cobrarse = '$fecha_cobrarse'
    GROUP BY u.usuid, u.usunombre
    ORDER BY u.usuid ASC";
}
else
{
    $sql = "SELECT u.usuid, u.usunombre FROM usuarios as u
    WHERE u.usurelacionista = 1
    GROUP BY u.usuid, u.usunombre
    ORDER BY u.usuid ASC";
}

$cobradores_aux = $db->query($sql);

$cobradores = $cobradores_aux->fetchAll(PDO::FETCH_ASSOC);
echo json_encode($cobradores);