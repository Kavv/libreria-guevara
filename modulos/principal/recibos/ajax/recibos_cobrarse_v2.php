<?php

$r = '../../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');


    $cobro_start = $_GET['cobro_start'];
    $cobro_end = $_GET['cobro_end'];
    $cobrador = $_GET['cobrador'];
    $departamento = $_GET['departamento'];
    $ciudad = $_GET['ciudad'];
    $desde = $_GET['desde'];
    $hasta = $_GET['hasta'];
    $fechacobrarse = $_GET['fechacobrarse'];
    
    $parameters = [];
    array_push($parameters, "s.cobro BETWEEN '$cobro_start' AND '$cobro_end'");
    array_push($parameters, "s.solrelacionista <> 'CALLCENTER'");
    array_push($parameters, "ca.carestado = 'ACTIVA'");
    array_push($parameters, "ca.carncuota <> 0");
    array_push($parameters, "ca.cartotal <> 0");

    if($cobrador != "")
        array_push($parameters, "s.solrelacionista = '$cobrador'" );
    if($departamento != "")
        array_push($parameters, "s.soldepentrega = '$departamento'" );
    if($ciudad != "")
        array_push($parameters, "s.solciuentrega = '$ciudad'" );
    if($desde != "" && $hasta != "")
        array_push($parameters, "solfecha BETWEEN '$desde' AND '$hasta'" );

    if(!isset($_GET['ajustar_cobrador']))
    {
        // Retorna una lista con todos los contratos a los cuales ya han sido relacionados a los recibos a cobrarse
        $temp = "SELECT contrato FROM recibos_a_cobrarse WHERE cobrarse = '$fechacobrarse'";
        array_push($parameters, "s.solid not in ($temp)");
        

        // Consulta base recibos
        $consulta_base = "SELECT s.cobro, s.solfactura, s.solid, s.solrelacionista, ca.carcuota, s.soldepcobro, 
        d.depnombre, d.depid, c.ciunombre, c.ciuid
        FROM solicitudes as s
        INNER JOIN carteras as ca on ca.carfactura = s.solfactura
        LEFT JOIN departamentos as d on d.depid = s.soldepcobro
        LEFT JOIN ciudades as c on (c.ciuid = s.solciucobro and c.ciudepto = s.soldepcobro)";
        
        $group = "";
        $ord = " ORDER BY s.cobro, s.solid, c.ciunombre ASC";
    }
    // En caso de que el indice status exista entonces
    if(isset($_GET['ajustar_cobrador']))
    {
        // Si la fecha esta definida, asignara una cadena Base que retorna los usuarios en base a los parametros establecidos,
        // como por ejemplo la fecha
        if($cobro_start != "" && $cobro_end != "")
        {
            // Consulta base cobradores con contratos asignados
            $consulta_base = "SELECT u.usuid, u.usunombre
            FROM solicitudes as s
            INNER JOIN carteras as ca on ca.carfactura = s.solfactura
            LEFT JOIN departamentos as d on d.depid = s.soldepcobro
            LEFT JOIN ciudades as c on (c.ciuid = s.solciucobro and c.ciudepto = s.soldepcobro)
            INNER JOIN usuarios as u on u.usuid = s.solrelacionista";
        
            $group = " GROUP BY u.usuid, u.usunombre";
            $ord = " ORDER BY u.usunombre ASC";
        }
        else
        {
            // En caso de que la fecha no sea establecida se retornará todos los cobradores
            $cobradores = $db->query("SELECT usuid, usunombre FROM usuarios where usurelacionista = 1 ORDER BY usunombre ASC");

            $cobradores = $cobradores->fetchAll(PDO::FETCH_ASSOC);
            echo json_encode($cobradores);
            exit();
        }

    }

    $sql = $consulta_base;
    foreach ($parameters as $index => $parameter) {
        // Se agregan los parametros del WHERE
        if($index == 0)
            $sql .= " WHERE " . $parameter;
        else
            $sql .= " AND " . $parameter;
    }
    $sql .= $group;
    $sql .= $ord;
    $recibos = $db->query($sql);


$recibos = $recibos->fetchAll(PDO::FETCH_ASSOC);
echo json_encode($recibos);

?>