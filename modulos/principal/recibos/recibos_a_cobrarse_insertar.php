<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

?>

<!doctype html>
<html lang="es">

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-not-form.php');
    ?>
    <style>
        #form input {
            text-transform: uppercase;
        }
        .divisor {
            margin-left: 5px;
            margin-right: 5px;
            margin-top: 15px;
            margin-bottom: 15px;
            padding-top: 10px;
            padding-bottom: 2px;
            background-color: #004593;
            color: #fff;
            border-radius: 8px;
        }
        .divisor h6 {
            font-weight: bold; 
        }
        tr td {
            position: relative;
        }
    </style>
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

	<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>
	<script type="text/javascript">
		var $select_cobradores = "";
		$(document).ready(function() {
			$(".fecha").datepicker({
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
            });
            
            $("input").prop("autocomplete","off"); 
            
            var ruta = 'ajax/cobradores.php';
            $select_cobradores += "<select class='disabled-input cobradores-edit form-control validate[required]' name='detalle-cobrador[]'>";
            $select_cobradores += "<option value=''></option>";
            $.get(ruta, function(res){
                res = JSON.parse(res);
                res.forEach(function(data){
                    nombre = "";
                    if(data.usunombre != "")
                        nombre += "/" + data.usunombre;
                    $select_cobradores += "<option value='"+ data.usuid +"'> "+ data.usuid + nombre +"</option>";
                });
            $select_cobradores += "</select>";
            });

            $("#form").keypress(function(e) {
                if (e.which == 13) {
                    generar_lista();
                    return false;
                }
            });

            // Almacenar las ciudades
            ruta = 'ajax/ciudades.php';
            $ciudades = [];
            $.get(ruta, function(res){
                res = JSON.parse(res);
                res.forEach(function(data){

                    var dep = data.ciudepto;
                    var ciu = data.ciuid;
                    var nombre = data.ciunombre;
                    // Si es diferente a null significa que el arreglo en el indice x ya posee datos
                    if($ciudades[dep] != null)
                    {
                        $ciudades[dep].push([ciu, nombre]);
                    }
                    else
                    {
                        // Si es la primera vez se asigna el primer dato como un arreglo
                        $ciudades[dep] = [[ciu, nombre]];
                    }
                });
            });

			$('#form').validationEngine({
				showOneMessage: true,
				onValidationComplete: function(form, status) {
					if (status) {
						return true;
					}
				}
			});
		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="<?php echo $r ?>index.php">Principal</a>
				<div class="mapa_div"></div><a>Contrato</a>
				<div class="mapa_div"></div><a class="current">Agregar Recibos a Cobrarse</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="guardar_recibos_cobrarse.php" method="post">
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12">
						<legend class="ui-widget ui-widget-header ui-corner-all">AGREGAR RECIBOS A COBRARSE</legend>
                        

                        <div class="row mb-3">
                            <div class="col-md-2">
                                <label for=""><span style="color:red;">*</span>Día a cobrarse</label>
							    <input type="text" class="form-control not-w validate[required, custom[integer], min[1], max[31]]" id="fecha-cobro" name="fecha-cobro" value=""/> 
                            </div>
                            <div class="col-md-3">
                                <label for="">Departamento</label>
                                <select id="departamento" name="departamento" class="selectpicker" data-live-search="true" title="Seleccione un departamento" data-width="100%">
                                    <option value="">Todos los departamentos</option>
                                    <option value="0">Contrato sin departamento</option>
                                <?php
                                    $query = $db->query("SELECT * FROM departamentos ORDER BY depnombre ASC");
                                    while($row = $query->fetch(PDO::FETCH_ASSOC))
                                    {
                                        $codigo = $row['depid'];
                                        $nombre = $row['depnombre'];
                                        echo "<option value='". $codigo ."'>$nombre</option>";
                                    }
                                ?>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label for="">Ciudades</label>
                                <select id="ciudad" name="ciudad" class="selectpicker" data-live-search="true" title="Selecciona una ciudad" data-width="100%">
                                    <option value="">Todas las ciudades</option>
                                    <option value="0">Contrato sin ciudades</option>
                                </select>
                            </div>

                            <div class="col-md-4">
                                <label for="">Cobrador</label>
                                <select id="cobrador" name="cobrador" class="selectpicker" data-live-search="true" title="Seleccione un cobrador" data-width="100%">
                                    <option value="">Todos los cobradores de la fecha</option>
                                    <option value="null">Contratos sin cobrador asignado</option>
                                <?php
                                    $query = $db->query("SELECT usuid, usunombre FROM usuarios WHERE usurelacionista = 1 ORDER BY usuid");
                                    while($row = $query->fetch(PDO::FETCH_ASSOC))
                                    {
                                        $codigo = $row['usuid'];
                                        $nombre = "";
                                        if(TRIM($row['usunombre']) != "")
                                            $nombre .= "/" . $row['usunombre'];
                                        echo "<option value='". $row['usuid'] ."'>$codigo $nombre</option>";
                                    }
                                ?>
                                </select>
                            </div>

                        </div>
                        <div class="row mb-3">
                            <div class="col-md-3">
                                <label for=""><span style="color:red;">*</span>Fecha a cobrarse</label>
							    <input type="text" class="form-control fecha not-w validate[required, custom[date]]" id="fecha-cobrarse" name="fecha-cobrarse" value="<?php echo date("Y-m-d");?>"/> 
                            </div>
                            
                            <div class="col-md-3">
                                <label for="">Contratos desde</label>
							    <input type="text" class="form-control fecha not-w validate[custom[date]]" id="contratos-desde" name="contratos-desde" value=""/> 
                            </div>
                            <div class="col-md-3">
                                <label for="">Contratos Hasta</label>
							    <input type="text" class="form-control fecha not-w validate[custom[date]]" id="contratos-hasta" name="contratos-hasta" value=""/> 
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-3 text-center mb-1">
                                <button class="btn btn-info btn-block" type="button" onclick="update_cobrador();">Ajustar Cobradores</button>
                            </div>
                            <div class="col-md-3 text-center mb-1">
                                <button class="btn btn-dark btn-block" type="button" onclick="generar_lista();">Generar Lista</button>
                            </div>
                            <div class="col-md-3 text-center mb-1">
                                <button class="btn btn-success btn-block" type="button" data-toggle="modal" data-target="#add-manual">Agregar 1 manual</button>
                            </div>
                            
                        </div>
                        
                        <div class="row" style="overflow-x: auto;">
                            <table id="tabla-recibos" class="table table-hover" style="min-width: 1260px;">
                                <thead>
                                    <tr>
                                        <th style="width:5%; text-align:center;"></th>
                                        <th style="width:5%; text-align:center;">#</th>
                                        <th style="width:15%;"><spam class="text-danger">*</spam>Contrato</th>
                                        <th style="width:5%;"><spam class="text-danger">*</spam>Recibo</th>
                                        <th style="width:5%;"><spam class="text-danger">*</spam>Monto</th>
                                        <th style="width:20%;"><spam class="text-danger">*</spam>F. Pago</th>
                                        <th style="width:20%;"><spam class="text-danger">*</spam>Cobrador</th>
                                        <th style="width:13%;">Dep</th>
                                        <th style="width:12%;">Ciu</th>
                                        <th >Comentario</th>
                                    </tr>
                                </thead>
                                <tbody id="tb-recibo">
                                </tbody>
                                <tbody id="tb-recibo-manual">
                                </tbody>
                            </table>
                        </div>
                    </fieldset>


                    <div class="row d-flex justify-content-center">
                        <div class="col-md-6">
                            <button id="guardar" type="submit" name="guardar-contrato" class="btn btn-primary btn-block">GUARDAR</button>
                        </div>
                    </div>
                </form>
            </article>
        </article>
    </section>
    <!-- Modal -->
    <div class="modal fade" id="add-manual" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Agregar Manual</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- <div class="alert alert-danger" role="alert">
                    El contrato no existe
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div> -->
                <div class="row">
                    <div class="col-md-12">
                        <label for=""># DE CONTRATO</label>
                        <input type="text" class="form-control" id="contrato-manual">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="add_recibo_manual();" >Agregar</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            </div>
            </div>
        </div>
    </div>
	<?php
	if (isset($_GET['error'])) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['error'] . '</div>';
	elseif (isset($_GET['mensaje'])) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['mensaje'] . '</div>';
	?>
    
</body>
<script>
    var prueba;
    var index_recibos = 1;

    function generar_lista()
    {
        var cobrador = $("#cobrador option:selected");
        var codigo = cobrador.val();
        var fecha = $("#fecha-cobro").val();
        var dep = $("#departamento").val();
        var ciu = $("#ciudad").val();
        var fecha_cobrarse = $("#fecha-cobrarse").val();
        var desde = $("#contratos-desde").val();
        var hasta = $("#contratos-hasta").val();
        $("#fecha-cobro").validationEngine('validate');
        var format_desde = $("#contratos-desde").validationEngine('validate');
        var format_hasta = $("#contratos-hasta").validationEngine('validate');
        if(fecha != "" && fecha_cobrarse !="" && !format_desde && !format_hasta)
        {
            carga();
            var nombre = cobrador.text(); 
            //$("#cobrador-aux").text(nombre);
            ruta = "ajax/recibos_cobrarse.php?fecha=" + fecha + "&departamento=" + dep + "&ciudad=" + ciu + "&cobrador=" + codigo + "&desde=" + desde + "&hasta=" + hasta;
            
            $('#tb-recibo').empty();
            index_recibos = 1;
            $.get(ruta, function(res) {
                res = JSON.parse(res);
                res.forEach(function(data){
                    recibo_factura = data.solfactura;
                    recibo_contrato = data.solid;
                    recibo_numero = 0;//data.movnumero; 
                    recibo_monto = 0; //data.movvalor; 
                    recibo_fecha = fecha_cobrarse;
                    recibo_codigo = data.solrelacionista;
                    recibo_nombre = "";
                    recibo_dep = data.depnombre;
                    recibo_ciu = data.ciunombre;
                    
                    add_recibo("#tb-recibo", recibo_factura,recibo_contrato, recibo_numero, recibo_monto, recibo_fecha, recibo_codigo, recibo_nombre, recibo_dep, recibo_ciu);
                });
                input_fecha();
                actualizar_select();
                $('#carga').dialog('close');
            });
            /* for(var i = 0; i < cantidad_registros; i++)
            {
                add_recibo(fecha, codigo, nombre);
            } */
        } 
    }



    function add_recibo_manual()
    {
        var contrato = $("#contrato-manual").val();
        if(contrato != "")
        {
            carga();
            ruta = "ajax/search_contrato.php?contrato=" + contrato;
            
            $.get(ruta, function(data) {
                data = JSON.parse(data);
                var fecha_cobrarse = $("#fecha-cobrarse").val();
                if(data)
                {
                    recibo_factura = data.solfactura;
                    recibo_contrato = data.solid;
                    recibo_numero = 0;//data.movnumero; 
                    recibo_monto = 0; //data.movvalor; 
                    recibo_fecha = fecha_cobrarse;
                    recibo_codigo = data.solrelacionista;
                    recibo_nombre = "";
                    recibo_dep = data.depnombre;
                    recibo_ciu = data.ciunombre;
                    add_recibo("#tb-recibo-manual", recibo_factura,recibo_contrato, recibo_numero, recibo_monto, recibo_fecha, recibo_codigo, recibo_nombre, recibo_dep, recibo_ciu);
                }
                else
                {
                    var cobrador = $("#cobrador").val();
                    add_recibo("#tb-recibo-manual", "null", contrato, "0", "0", fecha_cobrarse, "", "", "-", "-");
                }
                $("#add-manual").modal("hide");
                $('#carga').dialog('close');
            });
        }
    }

    function update_cobrador()
    { 
        var fecha = $("#fecha-cobro").val();
        var dep = $("#departamento").val();
        var ciu = $("#ciudad").val();
        var desde = $("#contratos-desde").val();
        var hasta = $("#contratos-hasta").val();
        var format_desde = $("#contratos-desde").validationEngine('validate');
        var format_hasta = $("#contratos-hasta").validationEngine('validate');
        if(!format_desde && !format_hasta)
        {
            carga(); 
            //$("#cobrador-aux").text(nombre);
            ruta = "ajax/recibos_cobrarse.php?fecha=" + fecha + "&departamento=" + dep + "&ciudad=" + ciu +  "&cobrador=&status=1&desde=" + desde + "&hasta=" + hasta;
            
            $.get(ruta, function(res) {
                res = JSON.parse(res);
                var cobrador_codigo;
                var cobrador_nombre;
                $("#cobrador").empty();
                // Por defecto
                var html = '<option value="">Todos los cobradores de la fecha</option>'+
                            '<option value="null">Contratos sin cobrador asignado</option>';
                            
                $("#cobrador").append(html);

                res.forEach(function(data){
                    cobrador_codigo = data.usuid;
                    cobrador_nombre = data.usunombre;
                    
                    generate_select_cobradores(cobrador_codigo, cobrador_nombre);
                });
                $('#cobrador').selectpicker('refresh');
                input_fecha();
                actualizar_select();
                $('#carga').dialog('close');
            });
            /* for(var i = 0; i < cantidad_registros; i++)
            {
                add_recibo(fecha, codigo, nombre);
            } */
        } 
    }

    function generate_select_cobradores(codigo,nombre)
    {
        var cobrador = $("#cobrador");
        var html = "<option value='" + codigo + "'>" + codigo + " / " + nombre + "</option>";
        $("#cobrador").append(html);
    }


    function actualizar_select()
    {
        var codigos = $(".codigo-cobrador-recibo");
        var cant = codigos.length;
        var select_cobradores = $(".cobradores-edit");
        for(var i = 0; i < cant; i++)
        {
            select_cobradores.eq(i).val(codigos.eq(i).val());
        }
        
    }

    function add_recibo(obj, factura, contrato, recibo, monto, fecha, codigo, cobrador, dep, ciu)
    {
        html = "<tr class='recibo-item'>" +
            "<td><button type='button' class='btn btn-danger' onclick='remove_recibo(this);'>-</button></td>"+
            "<td class='text-center'><span class='index-recibos'>"+ index_recibos +"</span></td>" +
            "<td>"+
                "<input value='" + factura + "' type='hidden' class='factura-recibo'>" +
                "<input value='" + contrato + "' type='hidden' name='detalle-contrato[]' class='contrato-original'>" +
                "<input disabled value='" + contrato + "' type='text' class='disabled-input contrato-recibo form-control validate[required]'  autocomplete='off'>"+
            "</td>" +
            "<td>" + 
                "<input value='" + recibo + "' type='hidden' class='recibo-original' name='detalle-recibo[]'>"+
                "<input disabled value='" + recibo + "' type='text' class='disabled-input num_recibo form-control validate[required, custom[integer], ajax[ajaxRecibo]]' name='detalle-recibo[]' onkeypress='return check(event)' autocomplete='off'>"+
            "</td>" +
            "<td><input value='" + monto + "' type='text' class='disabled-input monto-recibo form-control validate[required, custom[number], min[0]]' name='detalle-monto[]' autocomplete='off' ></td>" +
            "<td><input value='" + fecha + "' type='text' class='disabled-input detalle-fpago form-control fecha validate[required]' name='detalle-fpago[]' autocomplete='off' value='"+ fecha +"'></td>" +
            "<td>"+
                "<input type='hidden' class='codigo-cobrador-recibo' value='"+ codigo +"'>" +
                $select_cobradores +
            "</td>" +
            "<td>"+
                "<p>"+ dep +"</p>" +
            "</td>" +
            "<td>"+
                "<p>"+ ciu +"</p>" +
            "</td>" +
            "<td>"+
                "<textarea style='width:200px;' row='1' class='form-control r-comentario validate[maxSize[199]]' name='detalle-comentarios[]' value=''></textarea>" +
            "</td>" +
        "</tr>";

        $(obj).append(html);
        index_recibos++;
    } 

    function remove_recibo(element)
    {
        var fila = $(element);
        prueba = fila;
        fila.parents(".recibo-item").remove();
        index_recibos--;
        actualizar_index();
    }

    function input_fecha()
    {
        $(".fecha").datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
            maxDate: "+0D",
        });
    }



    function check(e) {
        tecla = (document.all) ? e.keyCode : e.which;

        //Tecla de retroceso para borrar, siempre la permite
        if (tecla == 8) {
            return true;
        }

        // Patron de entrada, en este caso solo acepta numeros y letras
        patron = /[A-Za-z0-9]/;
        tecla_final = String.fromCharCode(tecla);
        return patron.test(tecla_final);
    }

    function actualizar_index()
    {
        var filas = $(".index-recibos").length;
        for(var i = 1; i <= filas; i++)
        {
            $(".index-recibos").eq(i-1).text(i);
        }
    }
    
    $("#departamento").change(function(){
        $("#ciudad").empty();
        // Por defecto
        var html = '<option value="">Todas las ciudades</option>'+
                    '<option value="0">Contrato sin ciudades</option>';
        $("#ciudad").append(html);

        dep = $(this).val();
		if(dep == "")
			cant = 0;
		else
        cant = $ciudades[dep].length;
        for(var i = 0; i < cant; i++)
        {
            html = "<option value='" + $ciudades[dep][i][0] + "'>" + $ciudades[dep][i][1] + "</option>";
            $("#ciudad").append(html);
        }
        $('#ciudad').selectpicker('refresh');
    });
    $('#add-manual').on('hidden.bs.modal', function (event) {
        $("#contrato-manual").val("");    
    });
    $('#add-manual').on('shown.bs.modal', function (event) {
        $("#contrato-manual").focus();    
    });

    $("#form").submit(function(){
        carga();
        if(!$("#form").validationEngine('validate'))
            $('#carga').dialog('close');
    });

    $("#form-descargar").submit(function(e){
        var fecha = $("#fecha-cobrarse").val();
        var cobrador = $("#cobrador").val();
        var format_fecha = $("#fecha-cobrarse").validationEngine('validate');
        var fecha_format = $("#fecha-cobrarse").val();
        $("#fecha-cobrarse").validationEngine('validate');
        if(fecha != "" && cobrador != "" && !format_fecha)
        {
            $("#pdf-Fecha").val(fecha);
            $("#pdf-Asignado").val(cobrador);
        }
        else
            e.preventDefault();

        if(cobrador == "")
            alert("Debe especificar el cobrador");
    });
</script>

