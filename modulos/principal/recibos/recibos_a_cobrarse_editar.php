<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

?>

<!doctype html>
<html lang="es">

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-not-form.php');
    ?>
    <style>
        #form input {
            text-transform: uppercase;
        }
        .divisor {
            margin-left: 5px;
            margin-right: 5px;
            margin-top: 15px;
            margin-bottom: 15px;
            padding-top: 10px;
            padding-bottom: 2px;
            background-color: #004593;
            color: #fff;
            border-radius: 8px;
        }
        .divisor h6 {
            font-weight: bold; 
        }
        tr td {
            position: relative;
        }
    </style>
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

	<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>
	<script type="text/javascript">
		var $select_cobradores = "";
		$(document).ready(function() {
			$(".fecha").datepicker({
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
            });
            
            $("input").prop("autocomplete","off"); 
            
            var ruta = 'ajax/cobradores.php';
            $select_cobradores += "<select class='disabled-input cobradores-edit form-control validate[required]' name='detalle-cobrador[]'>";
            $select_cobradores += "<option value=''></option>";
            $.get(ruta, function(res){
                res = JSON.parse(res);
                res.forEach(function(data){
                    nombre = "";
                    if(data.usunombre != "")
                        nombre += "/" + data.usunombre;
                    $select_cobradores += "<option value='"+ data.usuid +"'> "+ data.usuid + nombre +"</option>";
                });
                $select_cobradores += "</select>";
            });

            $("#form").keypress(function(e) {
                if (e.which == 13) {
                    generar_lista();
                    return false;
                }
            });

            // Almacenar las ciudades
            ruta = 'ajax/ciudades.php';
            $ciudades = [];
            $.get(ruta, function(res){
                res = JSON.parse(res);
                res.forEach(function(data){

                    var dep = data.ciudepto;
                    var ciu = data.ciuid;
                    var nombre = data.ciunombre;
                    // Si es diferente a null significa que el arreglo en el indice x ya posee datos
                    if($ciudades[dep] != null)
                    {
                        $ciudades[dep].push([ciu, nombre]);
                    }
                    else
                    {
                        // Si es la primera vez se asigna el primer dato como un arreglo
                        $ciudades[dep] = [[ciu, nombre]];
                    }
                });
            });

			$('#form').validationEngine({
				showOneMessage: true,
				onValidationComplete: function(form, status) {
					if (status) {
						return true;
					}
				}
			});
		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="<?php echo $r ?>index.php">Principal</a>
				<div class="mapa_div"></div><a>Contrato</a>
				<div class="mapa_div"></div><a class="current">Editar Recibos a Cobrarse</a>
			</article>
			<article id="contenido">
                <div class="row">
                    <div class="col-md-4 text-right">
                        <label for="" class="not-w"><strong>Recibos entregados por:</strong></label>
                    </div>
                    <div class="col-md-2">
                        <form id="form-cobrador" name="form-descarga" action="<?php echo $r; ?>modulos/informes/informehelper.php" method="post" target="_blank">
                            <input type="hidden" value="22" name="idiid">
                            <input id="Empresa" name="Empresa"  value="16027020274133" type="hidden">
                            <input id="pdf-Fecha" name="Fecha" value="" type="hidden">
                            <input id="pdf-Asignado" name="Asignado" value="" type="hidden">
                            <input id="Moneda" name="Moneda" value="C$" type="hidden">
                            <input id="exportOption" name="exportOption" value="pdf" type="hidden">
                            <button class="btn btn-danger" type="submit">COBRADOR</button>
                        </form>
                    </div>
                    <div class="col-md-6">
                        <form id="form-dep" name="form-descarga" action="<?php echo $r; ?>modulos/informes/informehelper.php" method="post" target="_blank">
                            <input type="hidden" value="32" name="idiid">
                            <input id="Empresa" name="Empresa"  value="16027020274133" type="hidden">
                            <input id="pdf-dep-Fecha" name="Fecha" value="" type="hidden">
                            <input id="pdf-dep-Asignado" name="Asignado" value="" type="hidden">
                            <input id="pdf-dep-Nombre" name="Departamento" value="" type="hidden">
                            <input id="Moneda" name="Moneda" value="C$" type="hidden">
                            <input id="exportOption" name="exportOption" value="pdf" type="hidden">
                            <button class="btn btn-danger" type="submit">DEPARTAMENTO Y COBRADOR</button>
                        </form>
                    </div>
                </div>
                
				<form id="form" name="form" action="recibos_cobrarse_actualizar.php" method="post">
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12">
						<legend class="ui-widget ui-widget-header ui-corner-all">EDITAR RECIBOS A COBRARSE</legend>
                        

                        <div class="row mb-3">
                            <div class="col-md-3">
                                <label for=""><span style="color:red;">*</span>Fecha a cobrarse</label>
							    <input type="text" class="form-control fecha not-w validate[required, custom[date]]" id="fecha-cobrarse" name="fecha-cobrarse" value="<?php echo date("Y-m-d");?>"/> 
                            </div>

                            <div class="col-md-5">
                                <label for="">Cobrador</label>
                                <select id="cobrador" name="cobrador" class="selectpicker" data-live-search="true" title="Seleccione un cobrador" data-width="100%">
                                    <option value="">Todos los cobradores de la fecha</option>
                                    <option value="null">Contratos sin cobrador asignado</option>
                                <?php
                                    $query = $db->query("SELECT usuid, usunombre FROM usuarios WHERE usurelacionista = 1 ORDER BY usuid");
                                    while($row = $query->fetch(PDO::FETCH_ASSOC))
                                    {
                                        $codigo = $row['usuid'];
                                        $nombre = "";
                                        if(TRIM($row['usunombre']) != "")
                                            $nombre .= "/" . $row['usunombre'];
                                        echo "<option value='". $row['usuid'] ."'>$codigo $nombre</option>";
                                    }
                                ?>
                                </select>
                            </div>
                            <div class="col-md-3 text-center mb-1">
                                <label for="" style="visibility:hidden;">Ajustar cobrador</label>
                                <button class="btn btn-info btn-block" type="button" onclick="update_cobrador();">Ajustar Cobradores</button>
                            </div>

                        </div>
                        <div class="row mb-3">
                            
                            <div class="col-md-3">
                                <label for="">Departamento</label>
                                <select id="departamento" name="departamento" class="selectpicker" data-live-search="true" title="Seleccione un departamento" data-width="100%">
                                    <option value="">Todos los departamentos</option>
                                    <option value="0">Contrato sin departamento</option>
                                <?php
                                    $query = $db->query("SELECT * FROM departamentos ORDER BY depnombre ASC");
                                    while($row = $query->fetch(PDO::FETCH_ASSOC))
                                    {
                                        $codigo = $row['depid'];
                                        $nombre = $row['depnombre'];
                                        echo "<option value='". $codigo ."'>$nombre</option>";
                                    }
                                ?>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label for="">Ciudades</label>
                                <select id="ciudad" name="ciudad" class="selectpicker" data-live-search="true" title="Selecciona una ciudad" data-width="100%">
                                    <option value="">Todas las ciudades</option>
                                    <option value="0">Contrato sin ciudades</option>
                                </select>
                            </div>
                            <div class="col-md-4 text-center mb-1">
                                <label for="" style="visibility:hidden;">Buscar recibos a cobrarse</label>
                                <button class="btn btn-dark btn-block" type="button" onclick="generar_lista();">Buscar recibos a cobrarse</button>
                            </div>
                            
                        </div>
                        
                        <div class="row" style="overflow-x: auto;">
                            <table id="tabla-recibos" class="table table-hover" style="min-width: 1260px;">
                                <thead>
                                    <tr>
                                        <th style="width:5%; text-align:center;">#</th>
                                        <th style="width:15%;"><spam class="text-danger">*</spam>Contrato</th>
                                        <th style="width:5%;"><spam class="text-danger">*</spam>Recibo</th>
                                        <th style="width:5%;"><spam class="text-danger">*</spam>Monto</th>
                                        <th style="width:20%;"><spam class="text-danger">*</spam>F. Pago</th>
                                        <th style="width:20%;"><spam class="text-danger">*</spam>Cobrador</th>
                                        <th style="width:13%;">Dep</th>
                                        <th style="width:12%;">Ciu</th>
                                        <th >Comentario</th>
                                    </tr>
                                </thead>
                                <tbody id="tb-recibo">
                                </tbody>
                                <tbody id="tb-recibo-manual">
                                </tbody>
                            </table>
                        </div>

                        <div>

                        </div>
                    </fieldset>


                    <div class="row d-flex justify-content-center">
                        <div class="col-md-6">
                            <button id="guardar" type="submit" name="guardar-contrato" class="btn btn-primary btn-block">ACTUALIZAR</button>
                        </div>
                    </div>
                </form>
            </article>
        </article>
    </section>
	<?php
	if (isset($_GET['error'])) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['error'] . '</div>';
	elseif (isset($_GET['mensaje'])) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['mensaje'] . '</div>';
	?>
    
</body>
<script>
    var prueba;
    var index_recibos = 1;

    function generar_lista()
    {
        var cobrador = $("#cobrador option:selected");
        var codigo = cobrador.val();
        var dep = $("#departamento").val();
        var ciu = $("#ciudad").val();
        var fecha_cobrarse = $("#fecha-cobrarse").val();
        $("#fecha-cobrarse").validationEngine('validate');
        if(fecha_cobrarse !="")
        {
            carga();
            var nombre = cobrador.text(); 
            //$("#cobrador-aux").text(nombre);
            ruta = "ajax/rc_cobrarse.php?fecha=" + fecha_cobrarse + "&departamento=" + dep + "&ciudad=" + ciu + "&cobrador=" + codigo;
            
            $('#tb-recibo').empty();
            index_recibos = 1;
            $.get(ruta, function(res) {
                res = JSON.parse(res);
                res.forEach(function(data){
                    numero = data.id;
                    recibo_factura = data.solfactura;
                    recibo_contrato = data.contrato;
                    recibo_numero = data.recibo;//data.movnumero; 
                    recibo_monto = data.monto; //data.movvalor; 
                    recibo_fecha = fecha_cobrarse;
                    recibo_codigo = data.asignado;
                    recibo_nombre = "";
                    recibo_dep = data.depnombre;
                    recibo_ciu = data.ciunombre;
                    recibo_comentario = data.observaciones;
                    
                    add_recibo("#tb-recibo", numero, recibo_factura,recibo_contrato, recibo_numero, recibo_monto, recibo_fecha, recibo_codigo, recibo_nombre, recibo_dep, recibo_ciu, recibo_comentario);
                });
                input_fecha();
                actualizar_select();
                $('#carga').dialog('close');
            });
        } 
    }


    function update_cobrador()
    { 
        var fecha = $("#fecha-cobrarse").val();
        var dep = $("#departamento").val();
        var ciu = $("#ciudad").val();
    
        carga(); 
        //$("#cobrador-aux").text(nombre);
        ruta = "ajax/rc_cobradores.php?fecha_cobrarse=" + fecha;
        
        $.get(ruta, function(res) {
            res = JSON.parse(res);
            var cobrador_codigo;
            var cobrador_nombre;
            $("#cobrador").empty();
            // Por defecto
            var html = '<option value="">Todos los cobradores de la fecha</option>'+
                        '<option value="null">Contratos sin cobrador asignado</option>';
                        
            $("#cobrador").append(html);

            res.forEach(function(data){
                cobrador_codigo = data.usuid;
                cobrador_nombre = data.usunombre;
                
                generate_select_cobradores(cobrador_codigo, cobrador_nombre);
            });
            $('#cobrador').selectpicker('refresh');
            $('#carga').dialog('close');
        });
        
        
    }

    function generate_select_cobradores(codigo,nombre)
    {
        var cobrador = $("#cobrador");
        var html = "<option value='" + codigo + "'>" + codigo + " / " + nombre + "</option>";
        $("#cobrador").append(html);
    }


    function actualizar_select()
    {
        var codigos = $(".codigo-cobrador-recibo");
        var cant = codigos.length;
        var select_cobradores = $(".cobradores-edit");
        for(var i = 0; i < cant; i++)
        {
            select_cobradores.eq(i).val(codigos.eq(i).val());
        }
        
    }

    function add_recibo(obj, numero, factura, contrato, recibo, monto, fecha, codigo, cobrador, dep, ciu, comentario)
    {
        html = "<tr class='recibo-item'>" +
            "<td class='text-center'><span class='index-recibos'>"+ index_recibos +"</span></td>" +
            "<td>"+
                "<input value='" + numero + "' type='hidden' name='numeros[]'>" +
                "<input value='" + contrato + "' type='text' name='detalle-contrato[]' class='disabled-input contrato-recibo form-control validate[required]'  autocomplete='off'>"+
            "</td>" +
            "<td>" + 
                "<input value='" + recibo + "' type='text' name='detalle-recibo[]' class='disabled-input num_recibo form-control validate[required, custom[integer]]' name='detalle-recibo[]' onkeypress='return check(event)' autocomplete='off'>"+
            "</td>" +
            "<td><input value='" + monto + "' type='text' class='disabled-input monto-recibo form-control validate[required, custom[number], min[0]]' name='detalle-monto[]' autocomplete='off' ></td>" +
            "<td><input value='" + fecha + "' type='text' class='disabled-input detalle-fpago form-control fecha validate[required]' name='detalle-fpago[]' autocomplete='off' value='"+ fecha +"'></td>" +
            "<td>"+
                "<input type='hidden' class='codigo-cobrador-recibo' value='"+ codigo +"'>" +
                $select_cobradores +
            "</td>" +
            "<td>"+
                "<p>"+ dep +"</p>" +
            "</td>" +
            "<td>"+
                "<p>"+ ciu +"</p>" +
            "</td>" +
            "<td>"+
                "<textarea style='width:200px;' row='1' class='form-control r-comentario validate[maxSize[199]]' name='detalle-comentarios[]' value=''>"+comentario+"</textarea>" +
            "</td>" +
        "</tr>";

        $(obj).append(html);
        index_recibos++;
    } 

    function remove_recibo(element)
    {
        var fila = $(element);
        prueba = fila;
        fila.parents(".recibo-item").remove();
        index_recibos--;
        actualizar_index();
    }

    function input_fecha()
    {
        $(".fecha").datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
            maxDate: "+0D",
        });
    }



    function check(e) {
        tecla = (document.all) ? e.keyCode : e.which;

        //Tecla de retroceso para borrar, siempre la permite
        if (tecla == 8) {
            return true;
        }

        // Patron de entrada, en este caso solo acepta numeros y letras
        patron = /[A-Za-z0-9]/;
        tecla_final = String.fromCharCode(tecla);
        return patron.test(tecla_final);
    }

    function actualizar_index()
    {
        var filas = $(".index-recibos").length;
        for(var i = 1; i <= filas; i++)
        {
            $(".index-recibos").eq(i-1).text(i);
        }
    }
    
    $("#departamento").change(function(){
        $("#ciudad").empty();
        // Por defecto
        var html = '<option value="">Todas las ciudades</option>'+
                    '<option value="0">Contrato sin ciudades</option>';
        $("#ciudad").append(html);

        dep = $(this).val();
		if(dep == "")
			cant = 0;
		else
        cant = $ciudades[dep].length;
        for(var i = 0; i < cant; i++)
        {
            html = "<option value='" + $ciudades[dep][i][0] + "'>" + $ciudades[dep][i][1] + "</option>";
            $("#ciudad").append(html);
        }
        $('#ciudad').selectpicker('refresh');
    });
    $('#add-manual').on('hidden.bs.modal', function (event) {
        $("#contrato-manual").val("");    
    });
    $('#add-manual').on('shown.bs.modal', function (event) {
        $("#contrato-manual").focus();    
    });

    $("#form").submit(function(){
        carga();
        if(!$("#form").validationEngine('validate'))
            $('#carga').dialog('close');
    });

    $("#form-cobrador").submit(function(e){
        var fecha = $("#fecha-cobrarse").val();
        var cobrador = $("#cobrador").val();
        var format_fecha = $("#fecha-cobrarse").validationEngine('validate');
        var fecha_format = $("#fecha-cobrarse").val();
        $("#fecha-cobrarse").validationEngine('validate');
        if(fecha != "" && cobrador != "" && !format_fecha)
        {
            $("#pdf-Fecha").val(fecha);
            $("#pdf-Asignado").val(cobrador);
        }
        else
            e.preventDefault();

        if(cobrador == "")
            alert("Debe especificar el cobrador");
    });
    $("#form-dep").submit(function(e){
        var fecha = $("#fecha-cobrarse").val();
        var cobrador = $("#cobrador").val();
        var format_fecha = $("#fecha-cobrarse").validationEngine('validate');
        var departamento = $("#departamento").val();
        if(departamento != '')
        {
            $("#fecha-cobrarse").validationEngine('validate');
            if(fecha != "" && cobrador != "" && !format_fecha)
            {
                $("#pdf-dep-Fecha").val(fecha);
                $("#pdf-dep-Asignado").val(cobrador);
                $("#pdf-dep-Nombre").val(departamento);
            }
            else
                e.preventDefault();
                
            if(cobrador == "")
                alert("Debe especificar el cobrador");
        }
        else {
            e.preventDefault();
            alert('Debe especificar el departamento');
        }

    });
</script>

