
    <div class="modal fade" id="modal-reporte" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">LISTAS DE RECIBOS A COBRARSE</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4">
                        <label for="">Fecha de cobro <span style="color:red">(obligatorio)</span></label>
                        <input type="text" class="form-control fecha not-w validate[required, custom[date]]" id="fecha-cobrarse-reporte" name="fecha-cobrarse" value=""/> 
                    </div>
                    <div class="col-md-8">
                        <label for="">Cobrador asignado <span style="color:red">(obligatorio)</span></label>
                        <select id="cobrador-reporte" name="cobrador" class="selectpicker validate[required]" data-live-search="true" title="Seleccione un cobrador" data-width="100%">
                            <?php
                                $query = $db->query("SELECT usuid, usunombre FROM usuarios WHERE usurelacionista = 1 ORDER BY usunombre");
                                while($row = $query->fetch(PDO::FETCH_ASSOC))
                                {
                                    $codigo = $row['usuid'];
                                    $nombre = "";
                                    if(TRIM($row['usunombre']) != "")
                                        $nombre .= "/" . $row['usunombre'];
                                    echo "<option value='". $row['usuid'] ."'>$codigo $nombre</option>";
                                }
                            ?>
                        </select>
                    </div>
                </div>
                

                <div class="row">
                    
                    <div class="col-md-6">
                        <label for="">Departamento</label>
                        <select id="departamento-reporte" name="departamento" class="selectpicker" data-live-search="true" title="Seleccione un departamento" data-width="100%">
                            <option value="">Todos los departamentos</option>
                            <option value="0">Contrato sin departamento</option>
                            <?php
                                $query = $db->query("SELECT * FROM departamentos ORDER BY depnombre ASC");
                                while($row = $query->fetch(PDO::FETCH_ASSOC))
                                {
                                    $codigo = $row['depid'];
                                    $nombre = $row['depnombre'];
                                    echo "<option value='". $codigo ."'>$nombre</option>";
                                }
                            ?>
                        </select>
                    </div>
                    <div class="col-md-6">
                        <label for="">Ciudades</label>
                        <select id="ciudad-reporte" name="ciudad" class="selectpicker" data-live-search="true" title="Selecciona una ciudad" data-width="100%">
                            <option value="">Todas las ciudades</option>
                            <option value="0">Contrato sin ciudades</option>
                        </select>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-12">
                        <form id="form-cobrador" class="text-center" name="form-descarga" action="<?php echo $r; ?>modulos/informes/informehelper.php" method="post" target="_blank">
                            <input type="hidden" value="22" name="idiid">
                            <input id="pdf-co-empresa" name="Empresa"  value="F02900100885652" type="hidden">
                            <input id="pdf-co-fecha" name="Fecha" value="" type="hidden">
                            <input id="pdf-co-asignado" name="Asignado" value="" type="hidden">
                            <input id="pdf-co-moneda" name="Moneda" value="C$" type="hidden">
                            <input id="pdf-co-exp" name="exportOption" value="pdf" type="hidden">
                            <button class="btn btn-info" type="submit">POR COBRADOR</button>
                        </form>
                    </div>
                </div> 

                <div class="row my-2">
                    <div class="col-md-12">
                        <form id="form-dep" class="text-center" name="form-descarga" action="<?php echo $r; ?>modulos/informes/informehelper.php" method="post" target="_blank">
                            <input type="hidden" value="32" name="idiid">
                            <input id="pdf-dep-empresa" name="Empresa"  value="F02900100885652" type="hidden">
                            <input id="pdf-dep-fecha" name="Fecha" value="" type="hidden">
                            <input id="pdf-dep-asignado" name="Asignado" value="" type="hidden">
                            <input id="pdf-dep-dep" name="Departamento" value="" type="hidden">
                            <input id="pdf-dep-moneda" name="Moneda" value="C$" type="hidden">
                            <input id="pdf-dep-exp" name="exportOption" value="pdf" type="hidden">
                            <button class="btn btn-info" type="submit">POR COBRADOR Y DEPARTAMENTO</button>
                        </form>
                    </div> 
                </div>
                <div class="row my-2">
                    <div class="col-md-12">
                        <form id="form-ciu" class="text-center" name="form-descarga" action="<?php echo $r; ?>modulos/informes/informehelper.php" method="post" target="_blank">
                            <input type="hidden" value="33" name="idiid">
                            <input id="pdf-ciu-empresa" name="Empresa"  value="F02900100885652" type="hidden">
                            <input id="pdf-ciu-fecha" name="Fecha" value="" type="hidden">
                            <input id="pdf-ciu-asignado" name="Asignado" value="" type="hidden">
                            <input id="pdf-ciu-dep" name="Departamento" value="" type="hidden">
                            <input id="pdf-ciu-ciu" name="Ciudad" value="" type="hidden">
                            <input id="pdf-ciu-moneda" name="Moneda" value="C$" type="hidden">
                            <input id="pdf-ciu-exp" name="exportOption" value="pdf" type="hidden">
                            <button class="btn btn-info" type="submit">POR COBRADOR DEPARTAMENTO Y CIUDAD</button>
                        </form>
                    </div> 
                </div>  
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
            </div>
        </div>
    </div>

                    