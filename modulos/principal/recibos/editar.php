<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

?>

<!doctype html>
<html lang="es">

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-not-form.php');
    ?>
    <style>
        #form input {
            text-transform: uppercase;
        }
        .divisor {
            margin-left: 5px;
            margin-right: 5px;
            margin-top: 15px;
            margin-bottom: 15px;
            padding-top: 10px;
            padding-bottom: 2px;
            background-color: #004593;
            color: #fff;
            border-radius: 8px;
        }
        .divisor h6 {
            font-weight: bold; 
        }
        tr td {
            position: relative;
        }
    </style>
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

	<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>
	<script type="text/javascript">
		var $select_cobradores = "";
        $(document).ready(function() {
			$(".fecha").datepicker({
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
            });

			$('#form').validationEngine({
				onValidationComplete: function(form, status) {
                    if (status) {
                        return true;
                    }
				}
			});

            
            $("input").prop("autocomplete","off");
            
            ruta = 'ajax/cobradores.php';
            $select_cobradores += "<select disabled class='disabled-input cobradores-edit form-control validate[required]'>";
            $select_cobradores += "<option value=''>SIN ASIGNAR</option>";
            $.get(ruta, function(res){
                res = JSON.parse(res);
                res.forEach(function(data){
                    nombre = "";
                    if(data.usunombre != "")
                        nombre += "/" + data.usunombre;
                    $select_cobradores += "<option value='"+ data.usuid +"'> "+ data.usuid + nombre +"</option>";
                });
            $select_cobradores += "</select>";
            });
		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="<?php echo $r ?>index.php">Principal</a>
				<div class="mapa_div"></div><a>Contrato</a>
				<div class="mapa_div"></div><a class="current">Editar Recibos Cobrados</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="#" method="post">
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12">
						<legend class="ui-widget ui-widget-header ui-corner-all">EDITAR RECIBOS COBRADOS</legend>
                        
                        <div class="row mb-3">
                            <div class="col-md-5">
                                <label for="">Cobrador</label>
                                <select id="cobrador" name="cobrador" class="selectpicker" data-live-search="true" title="Seleccione un cobrador" data-width="100%">
                                    <option value="">Todos los cobradores de la fecha</option>
                                    <option value="null">Sin asignar</option>
                                <?php
                                    $query = $db->query("SELECT usuid, usunombre FROM usuarios WHERE usurelacionista = 1 ORDER BY usunombre");
                                    while($row = $query->fetch(PDO::FETCH_ASSOC))
                                    {
                                        $codigo = $row['usuid'];
                                        $nombre = "";
                                        if(TRIM($row['usunombre']) != "")
                                            $nombre .= "/" . $row['usunombre'];
                                        echo "<option value='". $row['usuid'] ."'>$codigo $nombre</option>";
                                    }
                                ?>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label for="">Fecha cobrado</label>
							    <input type="text" class="form-control not-w fecha validate[required]" id="fecha-cobrado" name="fecha-cobrado" value="<?php echo date("Y-m-d");?>"/> 
                            </div>
                            <div class="col-md-2 text-center">
                                <label for="" style="visibility:hidden;">Aplicar</label>
                                <button class="btn btn-info" type="button" onclick="generar_lista();">BUSCAR</button>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-4">
                                <label for="">Total Recolectado</label>
                                <input type="text" readonly class="form-control" name="recolectado" id="recolectado">
                            </div>
                        </div>
                        
                        <div class="row" style="overflow-x: auto;">
                            <table id="tabla-recibos" class="table table-hover" style="min-width: 700px;">
                                <thead>
                                    <tr>
                                        <th style="width:5%; text-align:center;">#</th>
                                        <th style="width:15%;"><spam class="text-danger">*</spam>Contrato</th>
                                        <th style="width:15%;"><spam class="text-danger">*</spam>Recibo</th>
                                        <th style="width:20%;"><spam class="text-danger">*</spam>Monto</th>
                                        <th style="width:20%;"><spam class="text-danger">*</spam>F. Pago</th>
                                        <th style="width:20%;"><spam class="text-danger">*</spam>Cobrador</th>
                                        <th style="width:5%;"></th>
                                    </tr>
                                </thead>
                                <tbody id="tb-recibo">
                                </tbody>
                            </table>
                        </div>
                    </fieldset>

                    <input type="hidden" id="factura" value="">
                </form>
            </article>
        </article>
    </section>
    
	<?php
	if (isset($_GET['error'])) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['error'] . '</div>';
	elseif (isset($_GET['mensaje'])) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['mensaje'] . '</div>';
	?>
    
</body>
<script>
    var prueba;
    var index_recibos = 1;
    function generar_lista()
    {
        var cobrador = $("#cobrador option:selected");
        var codigo = cobrador.val();
        var fecha = $("#fecha-cobrado").val();
        if(fecha != "")
        {
            carga();
            var nombre = cobrador.text(); 
            //$("#cobrador-aux").text(nombre);
            ruta = "ajax/recibos_fecha.php?fecha=" + fecha;
            if(codigo != "")
                ruta += "&cobrador=" + codigo;
            
            $('#tb-recibo').empty();
            index_recibos = 1;
            $.get(ruta, function(res) {
                res = JSON.parse(res);
                res.forEach(function(data){
                    recibo_factura = data.solfactura;
                    recibo_contrato = data.solid;
                    recibo_numero = data.movnumero; 
                    recibo_monto = data.movvalor; 
                    recibo_fecha = data.movfecha;
                    recibo_codigo = data.usuid;
                    recibo_nombre = data.usunombre;
                    
                    add_recibo(recibo_factura,recibo_contrato, recibo_numero, recibo_monto, recibo_fecha, recibo_codigo, recibo_nombre);
                });
                // Configura todos los inputs de tipo fecha (calendario)
                input_fecha();
                // Ajusta los correspondientes cobradores en los select
                actualizar_select();
                // Calcula el monto total
                calcular();
                $('#carga').dialog('close');
            });
            /* for(var i = 0; i < cantidad_registros; i++)
            {
                add_recibo(fecha, codigo, nombre);
            } */
        } 
    }

    function actualizar_select()
    {
        var codigos = $(".codigo-cobrador-recibo");
        var cant = codigos.length;
        var select_cobradores = $(".cobradores-edit");
        for(var i = 0; i < cant; i++)
        {
            select_cobradores.eq(i).val(codigos.eq(i).val());
            if(select_cobradores.eq(i).val()  == "")
            {
                var cobrador_desconocido = codigos.eq(i).val();
                var option_manual = "<option val='"+cobrador_desconocido+"' selected>"+cobrador_desconocido+"</option>";
                select_cobradores.eq(i).append(option_manual);
            }
        }
        
    }

    function add_recibo(factura, contrato, recibo, monto, fecha, codigo, cobrador)
    {
        html = "<tr class='recibo-item'>" +
            "<td class='text-center'><span class='index-recibos'>"+ index_recibos +"</span></td>" +
            "<td>"+
                "<input value='" + factura + "' type='hidden' class='factura-recibo'>" +
                "<input value='" + contrato + "' type='hidden' class='contrato-original'>" +
                "<input disabled value='" + contrato + "' type='text' class='disabled-input contrato-recibo form-control validate[required, ajax[ajaxContratoExist]]' name='detalle-contrato[]' autocomplete='off'>"+
            "</td>" +
            "<td>" + 
                "<input value='" + recibo + "' type='hidden' class='recibo-original'>"+
                "<input disabled value='" + recibo + "' type='text' class='disabled-input num_recibo form-control validate[custom[integer], ajax[ajaxRecibo]]' name='detalle-recibo[]' onchange='change_factura(this);' autocomplete='off'>"+
            "</td>" +
            "<td><input disabled value='" + monto + "' type='text' class='disabled-input monto-recibo form-control validate[required, custom[number], min[0]]' name='detalle-monto[]' autocomplete='off' onkeyup='calcular();' onchange='calcular();'></td>" +
            "<td><input disabled value='" + fecha + "' type='text' class='disabled-input detalle-fpago form-control fecha validate[required]' name='detalle-fpago[]' autocomplete='off' value='"+ fecha +"'></td>" +
            "<td>"+
                "<input type='hidden' class='codigo-cobrador-recibo' value='"+ codigo +"'>" +
                $select_cobradores +
            "</td>" +
            "<td class='td-button'>" +
                "<button type='button' class='btn btn-warning' title='Editar' onclick='editar_click(this)'><img src='../../../imagenes/iconos/lapiz.png' class='grayscale'></button>"+
            "</td>" +
        "</tr>";

        $("#tb-recibo").append(html);
        index_recibos++;
    } 
    function change_factura(element)
    {
        var celda = $(element).parent();
        row = celda.parents(".recibo-item");
        var $input_factura = row.find(".factura-recibo");
        $("#factura").val($input_factura.val());
    }
    var prueba;
    function editar_click(element)
    {
        var html = "<button type='button' onclick='guardar_click(this)' class='btn btn-success' title='Guardar'><img src='../../../imagenes/iconos/disk.png' class='grayscale'></button>";
        var celda = $(element).parent();
        prueba = celda;
        celda.empty();
        celda.append(html);
        var row = celda.parents("tr");
        row.find(".disabled-input").prop("disabled", false);
    }
    var row;
    function guardar_click(element)
    {

        var celda = $(element).parent();
        row = celda.parents(".recibo-item");
        var factura_num = row.find(".factura-recibo").val();
        var recibo_num = row.find(".num_recibo").val();
        var ruta = 'ajax/numero_recibo_edit.php?factura='+factura_num+"&recibo="+recibo_num;
        $.get(ruta, function(res)
        {
            carga();
            console.log(res);
            res = JSON.parse(res);
            if(res)
            {

                var $input_contrato = row.find(".contrato-recibo").validationEngine('validate');
                var $input_monto = row.find(".monto-recibo").validationEngine('validate');

                if($input_contrato && !$input_monto)
                {
                    var factura = row.find('.factura-recibo').val();
                    var contrato_original = row.find('.contrato-original').val();
                    var contrato_edit = row.find('.contrato-recibo').val();
                    var recibo_original = row.find('.recibo-original').val();
                    var recibo_edit = row.find('.num_recibo').val();
                    var monto_recibo = row.find('.monto-recibo').val();
                    var detalle_fpago = row.find('.detalle-fpago').val();
                    var cobrador = row.find('.cobradores-edit').val();
                    senddata = {
                            "factura": factura,
                            "contrato_orignal": contrato_original,
                            "contrato_edit": contrato_edit,
                            "recibo_original": recibo_original,
                            "recibo_edit": recibo_edit,
                            "monto_recibo": monto_recibo,
                            "detalle_fpago": detalle_fpago,
                            "cobrador": cobrador,
                            };

                    route = "ajax/udpate_recibo.php";
                    $.ajax({
                        url: route,
                        type: 'POST',
                        dataType: 'json',
                        data: senddata,
                        success: function(res){
                            if(res == true)
                            {
                                var html = "<button type='button' onclick='editar_click(this)' class='btn btn-warning' title='Editar'><img src='../../../imagenes/iconos/lapiz.png' class='grayscale'></button>";
                                celda.empty();
                                celda.append(html);
                                console.log("Exitoso");
                                row.find(".disabled-input").prop("disabled", true);
                                generar_lista();
                            }
                            else
                            {
                                console.log("No se pudo guardar");
                            }
                            row.find(".disabled-input").prop("disabled", true);
                            $('#carga').dialog('close');
                        }
                    }).fail( function( jqXHR, textStatus, errorThrown ) {
                        console.log("ERROR");
                        row.find(".disabled-input").prop("disabled", true);
                        $('#carga').dialog('close');
                    });
                }
            }
            else
            {
                $('#carga').dialog('close');
                alert("No se guardo, debido a que el # de recibo ya existe");
            }

        });
        

    }

    function input_fecha()
    {
        $(".fecha").datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
        });
    }

    function check(e) {
        tecla = (document.all) ? e.keyCode : e.which;

        //Tecla de retroceso para borrar, siempre la permite
        if (tecla == 8) {
            return true;
        }

        // Patron de entrada, en este caso solo acepta numeros y letras
        patron = /[A-Za-z0-9]/;
        tecla_final = String.fromCharCode(tecla);
        return patron.test(tecla_final);
    }

    function actualizar_index()
    {
        var filas = $(".index-recibos").length;
        for(var i = 1; i <= filas; i++)
        {
            $(".index-recibos").eq(i-1).text(i);
        }
    }

    var incompleto = false;
    var numero_recibos = [];
    function verificar_inputs()
    {
        var total = 0;
        var valor = 0;
        var cant = $(".num_recibo").length;
        
        incompleto = false;
        numero_recibos = [];
        for (i = 0; i < cant; i++)
        {
            valor = parseFloat($(".monto-recibo").eq(i).val());
            var num_recibo = $(".num_recibo").eq(i).val();
            var fpago = $(".detalle-fpago").eq(i).val();

            // Llenamos un arreglo con los # de recibos para verificar si hay alguno repetido
            numero_recibos.push(num_recibo);

            if( (valor >= 0) && (fpago != "") )
                total += valor;
            else
            {
                incompleto = true;
                
                $(".contrato-recibo").eq(i).validationEngine('validate');
                $(".num_recibo").eq(i).validationEngine('validate');
                $(".detalle-fpago").eq(i).validationEngine('validate');
                $(".monto-recibo").eq(i).validationEngine('validate');
            }
        }
        $("#recolectado").val(total);
    }

    function recibo_repetido()
    {
        var duplicate = false;
        var cantidad = numero_recibos.length;
        $(".num_recibo").css("color", "black");
        for(var i = 0; i < cantidad - 1; i++)
        {
            for(var j = i + 1; j < cantidad; j++ )
            {
                if(numero_recibos[i] == numero_recibos[j])
                {
                    if(numero_recibos[i] != "")
                    {
                        duplicate = true;
                        $(".num_recibo").eq(i).css("color", "red");
                        $(".num_recibo").eq(j).css("color", "red");
                        $(".num_recibo").eq(i).focus();
                        break;
                    }
                }
            }
            if(duplicate == true)
                break;
        }
        return duplicate;
    }


    function actualizar()
    {
        calcular();
    }

    function calcular()
    {
        /* var recolectado = $("#recolectado").val()*1;
        var actual = $(element).val()*1;

        if(recolectado == "")
            recolectado = 0;
            
        console.log(actual);
        if(Number.isInteger(actual))
        {
            var total = recolectado + actual;
            $("#recolectado").val(total);
            console.log("escribe "+ recolectado + actual)
        } */
        var filas = $(".monto-recibo").length;
        var total = 0;
        var monto = 0;
        for(var i = 0; i < filas; i++)
        {
            monto = $(".monto-recibo").eq(i).val() * 1;
            total += monto;
        }
        $("#recolectado").val(total);
    }

</script>
