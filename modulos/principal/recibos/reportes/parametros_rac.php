<?php
$r = '../../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

?>

<!doctype html>
<html lang="es">

<head>
    <title>IMPRIMIR REPORTES - RECIBOS A COBRARSE</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-not-form.php');
    ?>
    <style>
        #form input {
            text-transform: uppercase;
        }
        .divisor {
            margin-left: 5px;
            margin-right: 5px;
            margin-top: 15px;
            margin-bottom: 15px;
            padding-top: 10px;
            padding-bottom: 2px;
            background-color: #004593;
            color: #fff;
            border-radius: 8px;
        }
        .divisor h6 {
            font-weight: bold; 
        }
        tr td {
            position: relative;
        }
        .f-size-15 {
            font-size: 15px;
        }
        .nota-importante{
            border: #000 solid 2px;
            padding: 10px;
            border-radius: 18px;
            text-align: center;
            background: #00205f;
            color: white;
        }
        .div-fixed-start {
            position: fixed;
            z-index: 10;
            right: 10px;
            bottom: 10px;
        }
        .div-fixed-end {
            position: fixed;
            z-index: 10;
            right: 10px;
            bottom: 50px;
        }
        .excluidos {
            background: #ffe2fd;
        }
    </style>
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
    <link rel="stylesheet" href="<?php echo $r.'incluir/librerias/fontawesome/css/all.css'?>" />


    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
    <script src="<?php echo $r.'incluir/librerias/fontawesome/js/all.js'?>"></script>

	<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>


	<script type="text/javascript">
        var $ciudades = [];
		$(document).ready(function() {
			$(".fecha").datepicker({
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
            });
            
            $("input").prop("autocomplete","off"); 

            $("#form").keypress(function(e) {
                if (e.which == 13) {
                    recibos_a_cobrarse_x_rango();
                    return false;
                }
            });

            // Almacenar las ciudades
            ruta = '../ajax/ciudades.php';
            $.get(ruta, function(res){
                res = JSON.parse(res);
                res.forEach(function(data){

                    var dep = data.ciudepto;
                    var ciu = data.ciuid;
                    var nombre = data.ciunombre;
                    // Si es diferente a null significa que el arreglo en el indice x ya posee datos
                    if($ciudades[dep] != null)
                    {
                        $ciudades[dep].push([ciu, nombre]);
                    }
                    else
                    {
                        // Si es la primera vez se asigna el primer dato como un arreglo
                        $ciudades[dep] = [[ciu, nombre]];
                    }
                });
            });
		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="<?php echo $r ?>index.php">Principal</a>
				<div class="mapa_div"></div><a>Contrato</a>
				<div class="mapa_div"></div><a>Reporte</a>
				<div class="mapa_div"></div><a class="current">Imprimir Recibos a Cobrarse</a>
			</article>
			<article id="contenido">
            <div class="row">
                <div class="col-md-6">
                    <form id="form" name="form" action="#" method="post">
                        <fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12">
                            <legend class="ui-widget ui-widget-header ui-corner-all">Recibos a cobrarse por rango</legend>
                            <label for="Departamento">Departamento</label>
                            <select name="dep" id="departamento" class="form-control selectpicker" data-live-search="true" data-width="100%">
                                <option value=""></option>
                                <?php
                                $departamentos = $db->query("select  * from departamentos order by depnombre;");
                                while ($dep = $departamentos->fetch(PDO::FETCH_ASSOC)) { ?>
                                    <option value="<?php echo $dep['depid'] ?>"><?php echo $dep['depnombre'] ?></option>
                                <?php } ?>
                            </select>
                            <label for="">Ciudades a excluir</label>
                            <div class="input-group mb-3 pl-0">
                                <select id="ciudad" name="ciudad" class="selectpicker col-md-8 px-0" data-live-search="true" title="Selecciona una ciudad">
                                    <option value=""></option>
                                </select>
                                <div class="input-group-append col-md-4 px-0">
                                    <button id="btn-excluir" class="btn btn-block btn-danger" type="button" onclick="excluir_ciudad()">Excluir</button>
                                </div>
                            </div>
                            <div id="ciudades-excluidas">

                            </div>
                            <p>
                                <label for="">Cobrador</label>
                                <select id="cobrador" name="cobrador" class="selectpicker" data-live-search="true" title="Seleccione un cobrador" data-width="100%">
                                    <option value="">Todos los cobradores</option>
                                    <option value="null">Contratos sin cobrador asignado</option>
                                    <?php
                                        $query = $db->query("SELECT usuid, usunombre FROM usuarios WHERE usurelacionista = 1 ORDER BY usunombre");
                                        while($row = $query->fetch(PDO::FETCH_ASSOC))
                                        {
                                            $codigo = $row['usuid'];
                                            $nombre = "";
                                            if(TRIM($row['usunombre']) != "")
                                                $nombre .= "/" . $row['usunombre'];
                                            echo "<option value='". $row['usuid'] ."'>$codigo $nombre</option>";
                                        }
                                    ?>
                                </select>
                            </p>
                            <p>
                                <label for="CobroDesde">Día de Cobro Desde</label>
                                <input id="cobro_desde" name="cd" type="number" class="form-control validate[required, custom[integer]]">
                            </p>
                            <p>
                                <label for="CobroHasta">Día de Cobro Hasta</label>
                                <input id="cobro_hasta" name="ch" type="number" class="form-control validate[required, custom[integer]]">
                            </p>
                            <p>
                                <label for="Desde">Fecha de contrato Desde</label>
                                <input id="desde" name="desde" type="text" class="form-control fecha not-w validate[required, custom[date]]" value="2020-01-01" autocomplete="off">
                            </p>
                            <p>
                                <label for="Hasta">Fecha de contrato Hasta</label>
                                <input id="hasta" name="hasta" type="text" class="form-control fecha not-w validate[required, custom[date]]" value='<?php echo date('Y-m-d')?>' autocomplete="off">
                            </p>
                            <label for="Moneda">Moneda</label>
                            <select id="moneda" name="moneda" class="form-control validate[required] selectpicker" data-live-search="true" data-width="100%">
                                <?php
                                $monedas = $db->query("select * from monedas order by espordefecto desc;");
                                while ($moneda = $monedas->fetch(PDO::FETCH_ASSOC)) { ?>
                                    <option value="<?php echo $moneda['simbolo'] ?>"><?php echo $moneda['nombre'] ?></option>
                                <?php } ?>
                            </select>
                            <button id="consultar" onclick="recibos_a_cobrarse_x_rango();" type="button" class="btnconsulta btn btn-primary btn-block mt-2" name="consultar" value="Buscar">
                                Consultar
                            </button>
                        </fieldset>
                    </form>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-12">
                            <form id="form2" name="form2" class="form-style" action="#" method="post">

                                <fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12">
                                    <legend class="ui-widget ui-widget-header ui-corner-all">Recibo a cobrarse por contrato</legend>
                                    <p>
                                        <label for="contrato"># Contrato</label>
                                        <input id="contrato" name="contrato" type="text" class="form-control not-w validate[required]" autocomplete="off">
                                    </p>
                                    <p>
                                        <label for="contrato">Proximo cobro</label>
                                        <input id="proximo" name="proximo" type="text" class="form-control not-w" autocomplete="off">
                                    </p>
                                    <button onclick="recibos_a_cobrarse_x_contrato();" type="button" class="btnconsulta btn btn-primary btn-block my-2" name="consultar" value="Buscar">
                                        Consultar
                                    </button>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <form id="form-rblanco" class="form-style" name="form-descarga" action="<?php echo $r?>modulos/informes/informehelper.php" method="post" target="_blank">
                                <input type="hidden" value="20" name="idiid" autocomplete="off">

                                <fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12">
                                    <legend class="ui-widget ui-widget-header ui-corner-all">Recibo en blanco</legend>
                                    <input id="pdf-recibo-exportOption" name="exportOption" value="pdf" type="hidden" autocomplete="off">
                                    <p>
                                        <label for="Moneda3">Moneda</label>
                                        <select id="moneda3" name="Moneda" class="form-control validate[required] selectpicker" data-live-search="true" data-width="100%">
                                            <?php
                                            $monedas = $db->query("select * from monedas order by espordefecto desc;");
                                            while ($moneda = $monedas->fetch(PDO::FETCH_ASSOC)) { ?>
                                                <option value="<?php echo $moneda['simbolo'] ?>"><?php echo $moneda['nombre'] ?></option>
                                            <?php } ?>
                                        </select>
                                    </p>
                                    <p>
                                        <button type="subnmit" class="btnconsulta btn btn-primary btn-block mt-2">Consultar</button>
                                    </p>
                                    
                                </fieldset>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
            </article>
        </article>
    </section>
	<?php
	if (isset($_GET['error'])) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['error'] . '</div>';
	elseif (isset($_GET['mensaje'])) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['mensaje'] . '</div>';
	?>

    <!-- Modal -->
    <div class="modal fade" id="recibos" tabindex="-1" role="dialog" aria-labelledby="Recibos a cobrarse" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document" style="max-width: 1080px!important;">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="">Recibos a cobrarse</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="modal-body">
                <div class="row">
                    <div class="col-md-12 text-center" id="pdf-recibos">

                    </div>
                </div>
                
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
            </div>
        </div>
    </div>

	<?php require($r . 'incluir/src/loading.php'); ?>
</body>
<script>


    function recibos_a_cobrarse_x_contrato()
    {
        var contrato = $("#contrato").val();
        var proximo = $("#proximo").val();
        var form = $("#form2").validationEngine('validate');

        if(form && contrato != "")
        {
            ruta = "pdf_recivos_a_cobrarse_x_contrato.php?contrato=" + contrato + "&proximo=" + proximo;
            $("#loading").css("display","block");
            //
            var xhr = new XMLHttpRequest();
            xhr.open('GET', ruta , true);
            xhr.responseType = 'blob';

            xhr.onload = function(e) {
            if (this.status == 200) {
                    var blob = new Blob([this.response], {type: 'application/pdf'});
                    var link = document.createElement('a');
                    link.href = window.URL.createObjectURL(blob);
                    link.download = "recibo_a_cobrarse_" + contrato + ".pdf";
                    link.click(); 
                    $("#loading").css("display","none");   
                }
                else
                    $("#loading").css("display","none");   
            };

            xhr.send();
        }
    }
    
    function recibos_a_cobrarse_x_rango()
    {
        var dep = $("#departamento").val();
        var dep_nombre = $("#departamento option:selected").text();
        var desde = $("#desde").val();
        var hasta = $("#hasta").val();
        var cobro_desde = $("#cobro_desde").val();
        var cobro_hasta = $("#cobro_hasta").val();
        var moneda = $("#moneda").val();
        var cobrador = $("#cobrador").val();

        var form = $("#form").validationEngine('validate');


        if(form && desde != "" && hasta != "" &&  cobro_desde != "" &&  cobro_hasta != "" &&  moneda != "" )
        {
            //ruta = "recivos_a_cobrarse_v2.php?dep="+ dep +"&desde="+ desde +"&hasta="+ hasta +"&cd="+ cobro_desde +"&ch="+ cobro_hasta +"&moneda="+ moneda;
            var ruta = "recivos_a_cobrarse_v2.php?";
            ruta += $("#form").serialize();
            var pdf_name = "recibos_cobrarse_" + dep_nombre +" cobro desde "+cobro_desde+" hasta "+ cobro_hasta + ".pdf";

            $("#loading").css("display","block");
            //
            var xhr = new XMLHttpRequest();
            xhr.open('GET', ruta , true);
            xhr.responseType = 'blob';

            xhr.onload = function(e) {
            if (this.status == 200) {
                    var blob = new Blob([this.response], {type: 'application/pdf'});
                    var link = document.createElement('a');
                    link.href = window.URL.createObjectURL(blob);
                    link.download = pdf_name;
                    link.click(); 
                    $("#loading").css("display","none");
                }
                else
                    $("#loading").css("display","none");
            };

            xhr.send();
        }
    }
    $("#departamento").change(function(){
        $("#ciudad").empty();
        // Por defecto
        var html = '<option value=""></option>';
        $("#ciudad").append(html);

        dep = $(this).val();
		if(dep == "")
			cant = 0;
		else
            cant = $ciudades[dep].length;
        for(var i = 0; i < cant; i++)
        {
            html = "<option value='" + $ciudades[dep][i][0] + "'>" + $ciudades[dep][i][1] + "</option>";
            $("#ciudad").append(html);
        }
        $('#ciudad').selectpicker('refresh');
        // Limpiamos las ciudades excluidas del dep anterior
        $("#ciudades-excluidas").empty();
    });
    function excluir_ciudad()
    {
        var codigo = $("#ciudad").val();
        var nombre = $("#ciudad option:selected").text();
        if($("#ciudad-"+codigo).length < 1)
        {
            var html = '<div class="input-group mb-3 pl-0" id="ciudad-' + codigo + '">'+
                '<input type="hidden" value="' + codigo + '" name="ciu-excluidas[]">' +
                '<label class="not-w col-md-8 px-0 mx-0 excluidos">' + nombre + '</label>' +
                '<div class="input-group-append col-md-4 mx-0 px-0">' +
                    '<button id="btn-excluir" class="btn btn-block btn-danger" type="button" onclick="remover_ciudad_excluida(this)">Remover</button>' +
                '</div>' +
            '</div>';
            $("#ciudades-excluidas").append(html);
        }
    }
    var test; 
    function remover_ciudad_excluida(element)
    {
        var fila = $(element).parent().parent();
        fila.remove();
    }
</script>