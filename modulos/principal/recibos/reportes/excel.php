<?php
$r = '../../../../';

require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
require($r . 'incluir/phpexcel/Classes/PHPExcel.php');
require($r . 'incluir/funciones.php');

require $r . 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

$consulta_excel = 1;
if ($consulta_excel != "") {

    switch ($consulta_excel) {
        // 1 = Consulta manual para productos de manera dinamica (permite campos "vacios")
        case 1:
            $cd = $_GET['cd'];
            $ch = $_GET['ch'];
            $dep = $_GET['dep']; 
            if($cd == "" || $ch == "")
            {
                echo "Debe especificar todos los parametros solicitados";
                exit();
            }

            
            $excluidas = "";
            if(isset($_GET['ciu-excluidas']))
            {
                $excluidas='';
                $cantidad = count($_GET['ciu-excluidas']);
                for($index_excluido = 0; $index_excluido < $cantidad; $index_excluido++)
                {
                    $excluidas .= "'".$_GET['ciu-excluidas'][$index_excluido]."'";
                    if($index_excluido + 1 < $cantidad)
                        $excluidas .= ', ';
                }
            }


            $con = "SELECT solId as Contrato,
            upper(concat(cliNombre, ' ', cliNom2, ' ', cliApe1, ' ', cliApe2)) as Cliente,
            solcliente as \"Cedula\",
            depNombre as Departamento,
            ciunombre as Ciudad,
            solcobro as Direccion,
            soltelentrega as \"Telefono 1\",
            soltelcobro as \"Telefono 2\",
            cliTelfamiliar as \"Telefono Conyuge\",
            solFecha as \"Fecha del contrato\",
            cobro as \"Fecha de Cobro\",
            soltotal as \"Monto del contrato\",
            solcuota as Prima,
            f.movDescuento as Descuento,
            carTotal as Neto,
            carCuota as Cuota,
            (cartotal-carsaldo) as \"Pagado en Recibos\",
            carsaldo as \"Saldo Pendiente\",
            solObservacion as Observaciones,
            solasesor as Asesor,
            solrelacionista as Cobrador
            from solicitudes
            inner join clientes on solCliente = cliId
            left join departamentos on depId = soldepcobro
            left join ciudades on (solciucobro = ciuid and soldepcobro= ciudepto)
            left join movimientos as f on f.movPrefijo = 'FV' and f.movDocumento = solFactura and f.movEmpresa = solEmpresa
            left join carteras on carFactura = solFactura and carEmpresa = solEmpresa
            left join usuarios usuCobrador on usuCobrador.usuId = solRelacionista
            left join estadoscartera on estaDescripcion = carEstado";

            $ord = " group by solId, upper(concat(cliNombre, ' ', cliNom2, ' ', cliApe1, ' ', cliApe2)), solcliente,
            ciuNombre, depNombre, solcobro, soltelentrega, soltelcobro, cliTelfamiliar, solFecha, soltotal , solcuota , carTotal,
            carcuota, solasesor, f.movDescuento, solObservacion, solrelacionista,carcuota, cobro, carsaldo 
            ORDER BY cobro, solid ASC";
            $consultaFin = crearConsulta($con, $ord,
                array(true, "estaId = 1"),
                array(true, "solRelacionista != 'CALLCENTER'"),
                array(true, "cobro between $cd and $ch"),
                array($dep, "depId = '$dep'"),
                array($excluidas, "ciuid not in ($excluidas)"),
            );
        break;
    }
        
    $qryDatos = $db->query($consultaFin);
    $cantidadDeFilas = $qryDatos->rowCount();

    $objPHPExcel = new Spreadsheet();

    $styleHeader = [
        'font' => [
            'bold' => true,
        ],
        'alignment' => [
            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            'shrinkToFit' => true,
        ],
        'borders' => [
            'allBorders' => [
                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                'color' => ['argb' => '828282']
            ],
        ],
        'fill' => [
            'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
            'startColor' => [
                'argb' => 'A9A9A9',
            ]
        ],
    ];


    $styleCell = [
        'alignment' => [
            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
            'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            'wrapText' => true, 
        ],
        'borders' => [
            'allBorders' => [
                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                'color' => ['argb' => '828282']
            ],
        ],
    ];


    $current = $objPHPExcel->getActiveSheet();

    $bandera = 1;
    while ($preResult = $qryDatos->fetch(PDO::FETCH_ASSOC)) {


        $banderaInterna = 0;

        foreach ($preResult as $key => $value) {

            $coordenada = obtenerNombreDeColumnaExcel($banderaInterna);

            if ($bandera == 1) {
                $current->getStyle(($coordenada . $bandera))->applyFromArray($styleHeader);
                $current->setCellValue(($coordenada . $bandera), $key);
                $current->getStyle(($coordenada . ($bandera + 1)))->applyFromArray($styleCell);

                $aux = mb_convert_encoding($value,"UTF-8","auto");
                $current->setCellValue(($coordenada . ($bandera + 1)), utf8_decode($aux));

                $objPHPExcel->getActiveSheet()->getColumnDimension($coordenada)->setWidth(strlen($key) * 2);
                // $objPHPExcel->getActiveSheet()->getColumnDimension($coordenada)->setAutoSize(true);

            } else {

                $current->getStyle(($coordenada . $bandera))->applyFromArray($styleCell);
                $aux = mb_convert_encoding($value,"UTF-8","auto");
                $current->setCellValue(($coordenada . $bandera), $aux);
            }

            $banderaInterna++;

        }

        if ($bandera == 1) {
            $bandera = $bandera + 2;
        } else {
            $bandera = $bandera + 1;
        }

    }

    $objPHPExcel->getActiveSheet()->setAutoFilter(
        $objPHPExcel->getActiveSheet()
            ->calculateWorksheetDimension()
    );

// Redirect output to a client’s web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=utf-8');
    header('Content-Disposition: attachment;filename="contratos_para_cobrarse.xlsx"');
    header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
    header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header('Pragma: public'); // HTTP/1.0

    $objWriter = IOFactory::createWriter($objPHPExcel, 'Xlsx');
    $objWriter->save('php://output');

}