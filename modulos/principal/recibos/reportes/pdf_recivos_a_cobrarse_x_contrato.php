<?php

$r = '../../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
require($r . 'incluir/fpdf/fpdf.php');

$param_contrato = strtoupper(trim($_GET['contrato']));
$proximo = $_GET['proximo'];

$datos_contratos = $db->query('select solId                                                                                         as Contrato,
       upper(concat(cliNombre, \' \', cliNom2, \' \', cliApe1, \' \', cliApe2))                            as Cliente,
       solcliente                                                                                         as "Cedula",
       solcobro                                                                              as Direccion,
       ciuNombre                                                                                     as Ciudad,
       depNombre                                                                                     as Departamento,
       soltelentrega                                                                                    as "Telefono 1",
       soltelcobro                                                                              as "Telefono 2",
       cliNomfamiliar                                                                                as Conyuge,
       cliTelfamiliar                                                                                as "Telefono Conyuge",
       solfecha                                                                                   as "Fecha de contrato",
       solFechdespacho                                                                               as "Fecha de entrega",
       solFecha                                                                                      as Fecha,
       soltotal                                                                                      as "Monto del contrato",
       solcuota                                                                                      as Prima,
       carTotal                                                                                      as Neto,
       movDescuento                                                                                  as Descuento,
       sum(dcaValor)                                                                                 as "Suma De Monto",
       carNcuota                                                                                     as "Numero de cuotas",
       carCuota                                                                                      as "Monto de cuotas",
       carTotal - movDescuento                                                                       as "Proxima cuota",
       solasesor                                                                          as Asesor,
       estaId                                                                                        as Estado,
       coalesce(estaDescripcion, carestado)                                                          as "Estado texto",
       solObservacion                                                                                as Observaciones,
       \'\'                                                                                            as Razonamiento,
       solrelacionista                                                                       as Cobrador,
       cliEmpresa                                                                                    as "Lugar de trabajo",
       cliEmpfamiliar                                                                                as "Lugar de trabajo conyuge",
       cliRefnombre1                                                                                 as "Referencia 1",
       cliRefcelular1                                                                                as "Celular referencia I",
       cliRefTelefono1                                                                                as "Telefono referencia I",
       sum(iif(detcarteras.dcavalor > 0, 1, 0))                                                      as Pagos,
       cliRefnombre2                                                                                 as "Referencia 2",
       cliRefcelular2                                                                                as "Celular referencia II",
       cliRefTelefono2                                                                                as "Telefono referencia II",
       clase,
       coalesce(gruNombre, \'Sin grupo\')                                                              as Grupo,
       --- INFO DE REPORTE
       now()                                                                                         as Hoy,
       carNcuota * 30.5                                                                              as Dias,
       carTotal / (carNcuota * 30.5)                                                                 as cuoxdia,
       sum(dcaValor) / (carTotal / (carNcuota * 30.5))                                               as diaspagados,
       (select solcompromiso + interval \'1\' day * (carNcuota * 30.5) )                                                                            as "Fecha de cobro",
       extract(day from now() - solcompromiso)                                                  as LosDiasQueLleva,
       (extract(day from now() - solcompromiso)) - (sum(dcaValor) / (carTotal / (carNcuota * 30.5))) as retraso,
       carsaldo                                        as actual,
       ((((extract(day from now() - solcompromiso)) - (sum(dcaValor) / (carTotal / (carNcuota * 30.5))))/30.5) * (carsaldo * 0.05))                as Mora,
       estudiante,
       escuela,
       grado,
       cobro,
       solcompromiso, 
       cuenta
from solicitudes
         inner join clientes
                    on solCliente = cliId
         left join departamentos on depId = soldepcobro
         left join ciudades on
        solciucobro = ciuid
        and soldepcobro = ciudepto
         left join movimientos on movPrefijo = \'FV\' and movDocumento = solFactura and movEmpresa = solEmpresa
         left join carteras on carFactura = solFactura and carEmpresa = solEmpresa
         left join detcarteras on dcaFactura = solFactura and dcaEmpresa = solEmpresa
         left join usuarios usuAsesor on solAsesor = usuAsesor.usuId
         left join usuarios usuCobrador on usuCobrador.usuId = solRelacionista
         left join estadoscartera on estaDescripcion = carEstado
         left join grupos on usuAsesor.usuGrupo = grunombre
where solid = \''. $param_contrato . '\'
group by solId, upper(concat(cliNombre, \' \', cliNom2, \' \', cliApe1, \' \', cliApe2)), solcliente, solcobro, ciuNombre,
         depNombre,soltelentrega, soltelcobro, cliNomfamiliar, cliTelfamiliar, solfecha, solFechdespacho,
         solFecha, soltotal, solcuota, carTotal, movDescuento, carNcuota, carCuota, carTotal - movDescuento,
         solasesor, estaId, carestado, estaDescripcion, solObservacion, solrelacionista, cliEmpresa,
         cliEmpfamiliar, cliRefnombre1, cliRefTelefono1, cliRefcelular1, cliRefnombre2, cliRefTelefono2, cliRefcelular2, clase,
         coalesce(gruNombre, \'Sin grupo\'), estudiante, escuela, solcompromiso, carsaldo,
         grado, cobro,         solcompromiso, cuenta
         order by cobro, solid;') or die ( $db->errorInfo()[2]);


$datos_recibos = $db->query('SELECT solid as "contrato",
    movvalor as "valor",                                                                                
    movfecha as "fecha",                                                                                 
    movcobrador as "cobrador"
    from solicitudes
    left join departamentos on depId = soldepcobro
    left join ciudades on
    solciucobro = ciuid
    and soldepcobro = ciudepto
    inner join movimientos as rc on rc.movPrefijo = \'RC\' and rc.movDocumento = solFactura
    left join carteras on carFactura = solFactura and carEmpresa = solEmpresa
    left join estadoscartera on estaDescripcion = carEstado
    where solid = \''. $param_contrato . '\'
    order by cobro asc, solid asc, movfecha desc') or die ( $db->errorInfo()[2]);


$datos_productos = $db->query('SELECT solid as "contrato",
    d.detproducto as "codigo",
    pronombre as "nombre",
    d.detcantidad as "cantidad"
    from solicitudes
    left join departamentos on depId = soldepcobro
    left join ciudades on
    solciucobro = ciuid
    and soldepcobro = ciudepto
    left join detsolicitudes as d on d.detsolicitud = solid
    left join productos on proid = d.detproducto
    left join carteras on carFactura = solFactura and carEmpresa = solEmpresa
    left join estadoscartera on estaDescripcion = carEstado
    where solid = \''. $param_contrato . '\'
    order by cobro, solid, d.created_at') or die ( $db->errorInfo()[2]);




$moneda = $db->query("SELECT simbolo FROM monedas where espordefecto = true")->fetch(PDO::FETCH_ASSOC);
$moneda = $moneda['simbolo'];

//$pdf = new PDF();
$pdf = new FPDF('P','mm','Letter');
$pdf->AddFont('LucidaConsole', '', 'lucon.php');
$pdf->AliasNbPages();
$pdf->AddPage();
// 190 es la longitud max
// Cada mitad es de 95 pero por la separación se le restan 2 a cada lado
// 93 cada mitad con una div total de 4

$parte = 1;
$count_contratos = 0;
$first = true;


$row_pago = $datos_recibos->fetch(PDO::FETCH_ASSOC);
$row_productos = $datos_productos->fetch(PDO::FETCH_ASSOC);

while ($data = $datos_contratos->fetch(PDO::FETCH_ASSOC)) {

    //Variable
    $nombre = substr($data['cliente'], 0, 29);
    $tel1 = $data['Telefono 1'];
    $tel2 = $data['Telefono 2'];
    $direccion = $data['direccion'];
    $ciudad = substr($data['ciudad'], 0, 25);
    $depa = substr($data['departamento'], 0, 25);
    $trabajo = substr($data['Lugar de trabajo'], 0, 42);
    $conyuge = substr($data['conyuge'], 0, 37);
    $tel_conyugue = substr($data['Telefono Conyuge'], 0, 11);
    $trabajo_con = substr($data['Lugar de trabajo conyuge'], 0, 42);
    $ref1 = substr($data['Referencia 1'], 0, 40);
    $cel_ref1 = $data['Celular referencia I']; 
    $tel_ref1 = $data['Telefono referencia I'];
    $ref2 = substr($data['Referencia 2'], 0, 40);
    $cel_ref2 = $data['Celular referencia II'];
    $tel_ref2 = $data['Telefono referencia II'];
    
    
    $telefono_referencia1 = "";
    if($tel_ref1 != "")
    $telefono_referencia1 = substr($tel_ref1, 0, 15);
    else if($cel_ref1 != "")
    $telefono_referencia1 = substr($cel_ref1, 0, 15);
    
    $telefono_referencia2 = "";
    if($tel_ref2 != "")
    $telefono_referencia2 = substr($tel_ref2, 0, 15);
    else if($cel_ref2 != "")
    $telefono_referencia2 = substr($cel_ref2, 0, 15);




    $observaciones = $data['observaciones'];


    $cobrador = substr($data['cobrador'], 0, 20);
    $asesor = substr($data['asesor'], 0, 20);
    $contrato = substr($data['contrato'], 0, 16);
    $descuento = $data['descuento'];
    $fecha_contrato = $data['Fecha de contrato'];
    $primer_cobro = $data['solcompromiso'];
    $proximo_cobro = $data['cobro'];
    $numero_cuotas = $data['Numero de cuotas'];

    $hoy = $data['hoy'];
    $dias_contrato = $data['dias'];
    $retraso = $data['retraso'];

    $monto = $data['Monto del contrato'];
    $prima = $data['prima'];
    $cuota = $data['Monto de cuotas'];
    $neto = $data['neto'];
    $abonado = $data['Suma De Monto'];
    $pagos = $data['pagos'];

    $cuenta = $data['cuenta'];
    switch ($cuenta) {
        case '2':
            $cuenta = "A";
            break;
        case '3':
            $cuenta = "B";
            break;
        case '4':
            $cuenta = "C";
            break;
        default:
            $cuenta = "";
            break;
    }
    


    $saldo_sin_interes = round($neto-$descuento-$abonado, 2);
    $cuota_por_dia = $data['cuoxdia'];
    $fecha_ultimo_pago = $data['Fecha de cobro'];
    $dias_pagados = $data['diaspagados'];
    $dias_que_lleva = $data['losdiasquelleva'];
    $mora = $data['mora'];
    $cuotal_para_aldia = $retraso*$cuota_por_dia;
    $saldo_aldia = round($saldo_sin_interes, 2);//$data['actual'];

    $aux_fecha_contrato = strtotime($fecha_contrato);
    $y_contrato = date("Y", $aux_fecha_contrato);
    $m_contrato = date("m", $aux_fecha_contrato);
    $d_contrato = date("d", $aux_fecha_contrato);


    $detalle_productos;
    $detalle_pagado;

    if($parte == 1)
    {
        if(!$first)
        {
            $pdf->AddPage();
        }
        else
            $first = false;

        $parte = 2;
        
        $start_second_part2 = 15;
        $pdf->Cell(0, $start_second_part2, "" , 0, 1, 'C');
        
        // MARCO
        //iz - de top
        /*$pdf->line(10, 10 + $start_second_part2, 103, 10 + $start_second_part2);
        // iz - de bot
        $pdf->line(10, 117 + $start_second_part2, 103, 117 + $start_second_part2);
        // top - bot iz
        $pdf->line(10, 10 + $start_second_part2, 10, 117 + $start_second_part2);
        // top - bot de
        $pdf->line(103, 10 + $start_second_part2, 103, 117 + $start_second_part2);*/
    }
    else
    {
        $start_second_part2 = $pdf->getY()+24;
        $start_second_part1 = $pdf->getY()+34;
        $pdf->SetY($start_second_part1);
        
        // MARCO
        //iz - de top
        /*$pdf->line(10, 10 + $start_second_part2, 103, 10 + $start_second_part2);
        // iz - de bot
        $pdf->line(10, 117 + $start_second_part2, 103, 117 + $start_second_part2);
        // top - bot iz
        $pdf->line(10, 10 + $start_second_part2, 10, 117 + $start_second_part2);
        // top - bot de
        $pdf->line(103, 10 + $start_second_part2, 103, 117 + $start_second_part2);*/
        $parte = 1;
    }


    // Variables de control
    $alto = 4;
    // font size estandar
    $fz = 7;
    // estado del borde
    $borde = 0;
    // Altura mas chica
    $h_slim = 3;


    // Fila auxiliar para dar inicio a una altura especifica
    $margen_top = 20;
    //$pdf->Cell(0, 20, "" , 0, 0, 'C');

    $pdf->SetFont('Arial', '', 13);

    //$pdf->Cell(88, 1, '-----------------------------------------------------', 0, 1, 'C');

    //Fila 1
    // inicio en x = 10.00125
    $pdf->Cell(93, 7, "" , 0, 1, 'C');
    //$pdf->Cell(4, 5, '', 1, 0, 'C');

    //Fila 2
    $pdf->SetFont('Arial', 'B', $fz);
    $pdf->cell(12, $alto, "Cliente:", $borde, 0, 'L' );
    $pdf->SetFont('Arial', '', $fz);
    $pdf->cell(46, $alto, "$nombre", $borde, 0, 'L' );

    $tel_mix = substr("$tel1/$tel2", 0, 17);

    $pdf->SetFont('Arial', 'B', $fz);
    $pdf->cell(6, $alto, "Tel:", $borde, 0, 'L' );
    $pdf->SetFont('Arial', '', $fz);
    $pdf->cell(29, $alto, "$tel_mix", $borde, 1, 'L' );


    //fila 4
    $direccion1 = substr($direccion,0, 50);
    $direccion2 = substr($direccion,50, 60);
    $direccion3 = substr($direccion,110, 60);

    $pdf->SetFont('Arial', 'B', $fz);
    $pdf->cell(14, $alto, utf8_decode("Dirección:"), $borde, 0, 'L' );
    
    $pdf->SetFont('Arial', '', $fz);
    $pdf->cell(79, $alto, utf8_decode($direccion1), $borde, 1, 'L' );


    // Col1
    $pdf->cell(93, $alto, utf8_decode($direccion2), $borde, 1, 'L' );
    // Col1
    $pdf->cell(93, $alto, utf8_decode($direccion3), $borde, 1, 'L' );
    //COL1
    $pdf->cell(46, $alto, utf8_decode($depa), $borde, 0, 'L' );
    $pdf->cell(47, $alto, " / ".utf8_decode($ciudad), $borde, 1, 'L' );

    // Fila5
    // COL1
    $pdf->SetFont('Arial', 'B', $fz);
    $pdf->cell(22, $alto, "Lugar de trabajo:", $borde, 0, 'L' );
    $pdf->SetFont('Arial', '', $fz);
    $pdf->cell(71, $alto, utf8_decode($trabajo), $borde, 1, 'L' );

    $pdf->line(11, 41 + $start_second_part2, 99, 41 + $start_second_part2);

    //Fila 6
    // COL 1
    $pdf->SetFont('Arial', 'B', $fz);
    $pdf->cell(13, $alto, utf8_decode("Cónyuge:"), $borde, 0, 'L' );
    $pdf->SetFont('Arial', '', $fz);
    $pdf->cell(56, $alto, "$conyuge", $borde, 0, 'L' );

    $pdf->SetFont('Arial', 'B', $fz);
    $pdf->cell(6, $alto, "Tel:", $borde, 0, 'L' );
    $pdf->SetFont('Arial', '', $fz);
    $pdf->cell(18, $alto, "$tel_conyugue", $borde, 1, 'L' );


    // Fila 7
    // COL 1
    $pdf->SetFont('Arial', 'B', $fz);
    $pdf->cell(22, $alto, "Lugar de trabajo:", $borde, 0, 'L' );
    $pdf->SetFont('Arial', '', $fz);
    $pdf->cell(71, $alto, utf8_decode($trabajo_con), $borde, 1, 'L' );

    $pdf->line(11, 49 + $start_second_part2, 99, 49 + $start_second_part2);

    //Fila 8
    
    $borde = 0;
    $pdf->SetFont('Arial', 'B', $fz);
    $pdf->cell(8, $alto, "Ref 1:", $borde, 0, 'L' );
    $pdf->SetFont('Arial', '', $fz);
    $pdf->cell(61, $alto, "$ref1", $borde, 0, 'L' );

    $pdf->SetFont('Arial', 'B', $fz);
    $pdf->cell(6, $alto, "Tel:", $borde, 0, 'L' );
    $pdf->SetFont('Arial', '', $fz);
    $pdf->cell(18, $alto, "$tel_ref1", $borde, 1, 'L' );
    
    $borde = 0;
    $pdf->SetFont('Arial', 'B', $fz);
    $pdf->cell(8, $alto, "Ref 2:", $borde, 0, 'L' );
    $pdf->SetFont('Arial', '', $fz);
    $pdf->cell(61, $alto, "$ref2", $borde, 0, 'L' );

    $pdf->SetFont('Arial', 'B', $fz);
    $pdf->cell(6, $alto, "Tel:", $borde, 0, 'L' );
    $pdf->SetFont('Arial', '', $fz);
    $pdf->cell(18, $alto, "$tel_ref2", $borde, 1, 'L' );


    //fila 9
    $pdf->line(11, 57 + $start_second_part2, 99, 57 + $start_second_part2);

    //$borde = 1;
    $observaciones1 = substr($observaciones, 0, 65);
    $observaciones2 = substr($observaciones, 65, 65);
    $observaciones3 = substr($observaciones, 130, 65);
    $observaciones4 = substr($observaciones, 195, 65);

    $pdf->SetFont('Arial', '', $fz);
    $pdf->cell(93, $alto, utf8_decode("$observaciones1"), $borde, 1, 'L' );
    //fila 10
    $pdf->cell(93, $alto, utf8_decode("$observaciones2"), $borde, 1, 'L' );
    //fila 11
    $pdf->cell(93, $alto, utf8_decode("$observaciones3"), $borde, 1, 'L' );
    $pdf->cell(93, $alto, utf8_decode("$observaciones4"), $borde, 1, 'L' );

    $pdf->line(11, 73 + $start_second_part2, 99, 73 + $start_second_part2);
    $pdf->line(11, 98 + $start_second_part2, 99, 98 + $start_second_part2);
    
    $borde = 0;
    // Fila 21
    $pdf->Ln(1);
    // Fila22
    $pdf->SetFont('Arial', 'B', $fz+2);
    $pdf->cell(22, $alto, "CONTRATO:", $borde, 0, 'L' );
    $pdf->cell(40, $alto, $contrato, $borde, 1, 'L' );
    $pdf->SetFont('Arial', '', $fz);
    //$pdf->SetFont('Arial', 'B', $fz);
    $pdf->cell(12, $alto, "Cobrador:", $borde, 0, 'L' );
    $pdf->cell(35.5, $alto, utf8_decode($cobrador), $borde, 0, 'L' );
    $pdf->cell(10, $alto, "Asesor:", $borde, 0, 'L' );
    $pdf->cell(35.5, $alto, utf8_decode($asesor), $borde, 1, 'L' );

    $pdf->SetFont('Arial', '', $fz);
    /* $pdf->cell(14, $alto, "Descuento:", $borde, 0, 'L' );
    $pdf->cell(29, $alto, $descuento, $borde, 1, 'L' ); */

    //Fila 23
    $pdf->cell(22, $alto, "Fecha de contrato:", $borde, 0, 'L' );
    $pdf->cell(35, $alto, "$d_contrato/$m_contrato/$y_contrato", $borde, 1, 'R' );


    //Fila 24
    $newformat = date("d/m/Y", strtotime($primer_cobro));

    $pdf->cell(27, $alto, "Fecha de primer cobro:", $borde, 0, 'L' );
    $pdf->cell(30, $alto, "$newformat", $borde, 1, 'R' );

    //Fila 25
    $pdf->cell(27, $alto, utf8_decode("Próximo cobro:"), $borde, 0, 'L' );
    $pdf->cell(30, $alto, "$proximo_cobro", $borde, 1, 'R' );
    
    //Fila 26
    $pdf->cell(27, $alto, utf8_decode("Número de cuotas:"), $borde, 0, 'L' );
    $pdf->cell(30, $alto, "$numero_cuotas", $borde, 1, 'R' );


    $pdf->ln(1);
    $pdf->cell(12, $alto+4, utf8_decode("Abono:"), $borde, 0, 'R' );
    $borde = 1;
    $pdf->cell(30, $alto+4, "", $borde, 1, 'R' );
    
    $pdf->ln(1);
    
    $borde = 0;
    $pdf->cell(12, $alto+4, utf8_decode("Saldo:"), $borde, 0, 'R' );
    $borde = 1;
    $pdf->cell(30, $alto+4, "", $borde, 1, 'R' );
    $borde = 0;

    
    $pdf->line(55, 104 + $start_second_part2, 99, 104 + $start_second_part2);
    $pdf->line(55, 110 + $start_second_part2, 99, 110 + $start_second_part2);
    
    // COL 2
    // Pos inicio X = 107.00125
    // Pos inicio Y = 10.00125
    $col2_x = 118;
    $col2_y = $start_second_part2 + 10.00125;
    $pdf->setXY($col2_x, $col2_y);

    
    $pdf->SetFont('Arial', '', 13);
    $pdf->Cell(93, 7, "" , $borde, 1, 'C');
    
    $pdf->SetFont('Arial', '', $fz);

    $pdf->setX($col2_x);
    // Col2
    $pdf->SetFont('Arial', 'B', $fz);
    $pdf->cell(12, $alto, "Cliente:", $borde, 0, 'L' );
    $pdf->SetFont('Arial', '', $fz);
    $pdf->cell(46, $alto, "$nombre", $borde, 0, 'L' );

    $pdf->SetFont('Arial', 'B', $fz);
    $pdf->cell(20, $alto, utf8_decode("Calificación:"), $borde, 0, 'L' );
    $pdf->cell(15, $alto, "$cuenta", $borde, 1, 'L' );
    $pdf->SetFont('Arial', '', $fz);

    //Col2
    $pdf->setX($col2_x);
    $hoy = date("d/m/Y", strtotime($hoy));

    $pdf->cell(7, $alto, "Hoy:", $borde, 0, 'L' );
    $pdf->cell(33, $alto, $hoy, $borde, 0, 'L' );
    $pdf->cell(23, $alto, "Su contrato es por:", $borde, 0, 'L' );
    $pdf->cell(30, $alto, round($dias_contrato, 2) . utf8_decode(" días"), $borde, 1, 'L' );


    // Col2
    $pdf->setX($col2_x);
    $pdf->cell(20, $alto, utf8_decode("Días atrasados:"), $borde, 0, 'L' );
    $pdf->cell(14, $alto, round($retraso,2) , $borde, 0, 'L' );
    $pdf->SetFont('Arial', 'B', $fz);
    $pdf->cell(10, $alto, "Monto:", $borde, 0, 'L' );
    $pdf->SetFont('Arial', '', $fz);
    $pdf->cell(20, $alto, $moneda.round($monto, 2), $borde, 0, 'L' );
    $pdf->SetFont('Arial', 'B', $fz);
    $pdf->cell(9, $alto, "Prima:", $borde, 0, 'L' );
    $pdf->SetFont('Arial', '', $fz);
    $pdf->cell(20, $alto, $moneda.round($prima,2) , $borde, 1, 'L' );

    // Col2
    $pdf->setX($col2_x);

    $pdf->SetFont('Arial', 'B', $fz);
    $pdf->cell(10, $alto, "Cuota:", $borde, 0, 'L' );
    $pdf->SetFont('Arial', '', $fz);
    $pdf->cell(19, $alto, $moneda.round($cuota, 2), $borde, 0, 'L' );
    $pdf->SetFont('Arial', 'B', $fz);
    $pdf->cell(10, $alto, "Neto:", $borde, 0, 'L' );
    $pdf->SetFont('Arial', '', $fz);
    $pdf->cell(21, $alto, $moneda.round($neto, 2), $borde, 0, 'L' );
    $pdf->SetFont('Arial', 'B', $fz);
    $pdf->cell(12, $alto, "Abonado:", $borde, 0, 'L' );
    $pdf->SetFont('Arial', '', $fz);
    $pdf->cell(21, $alto, $moneda.round($abonado,2) , $borde, 1, 'L' );


    // Col2
    $pdf->setX($col2_x);

    //Fila 12
    $borde = 1;
    $pdf->cell(15, $alto, utf8_decode("Código"), $borde, 0, 'C' );
    $pdf->cell(63, $alto, utf8_decode("Descripción"), $borde, 0, 'C' );
    $pdf->cell(10, $alto, utf8_decode("Cant"), $borde, 1, 'C' );
    
    //$detalle_productos = $db->query("SELECT detsolicitudes.*, pronombre FROM detsolicitudes INNER JOIN productos ON proid = detproducto WHERE detsolicitud = '$contrato'");

    // Fila 14, 15, 16, 18, 19, 20
    $limit_row = 4;
    $index = 0;
    //while($row = $detalle_productos->fetch(PDO::FETCH_ASSOC))
    // Ciclo infinito para recorrer el detalle del contrato (los productos)
    for(;;)
    {
        // Si la lista no esta vacia
        
        if($row_productos)
        {
            // Si el detalle es correspondiente al actual contrato
            if($row_productos['contrato'] == $contrato)
            {
                $pdf->setX($col2_x);
                // Dibujamos la fila correspondiente al producto
                $nombre_producto = substr($row_productos['nombre'], 0,  40);
                $pdf->cell(15, $h_slim, $row_productos['codigo'], $borde, 0, 'C' );
                $pdf->cell(63, $h_slim, utf8_decode($nombre_producto), $borde, 0, 'L' );
                $pdf->cell(10, $h_slim, $row_productos["cantidad"], $borde, 1, 'C' );
                $index++;
                // Siguiente producto
                $row_productos = $datos_productos->fetch(PDO::FETCH_ASSOC);
                // Si la cantidad de filas actuales es igual al limite establecido entonces debemos dejar de dibujar ya que no hay mas espacio disponible
                if($index == $limit_row)
                {
                    // Ciclo infinito para recorrer todos los registros necesarios para pasar al correspondiente detalle del contrato consecuente
                    for(;;)
                    {
                        // Si es diferente al contrato actual entonces ya encontramos el primer detalle del siguiente contrato
                        if($row_productos['contrato'] != $contrato)
                            break;

                        // Si el contrato del detalle es el mismo al contrato en ejecución entonces pasamos al siguiente en la lista
                        $row_productos = $datos_productos->fetch(PDO::FETCH_ASSOC);
                    }
                    break;
                }
            }
            else
                break;
        }
        else
            break;
    }
    $borde = 0;
    for($index; $index < $limit_row; $index++)
    {
        $pdf->cell(15, $h_slim, "", $borde, 0, 'C' );
        $pdf->cell(63, $h_slim, "", $borde, 0, 'L' );
        $pdf->cell(10, $h_slim, "", $borde, 1, 'C' );
    }

    
    $pdf->setX($col2_x);
    $pdf->SetFont('Arial', 'B', $fz);
    $pdf->cell(50, $alto, ("DETALLE DE PAGOS"), $borde, 0, 'C' );
    $pdf->SetFont('Arial', '', $fz);
    $pdf->cell(10, $alto, ("Pagos:"), $borde, 0, 'L' );
    $pdf->cell(33, $alto, $pagos, $borde, 1, 'L' );

    // Col2
    $pdf->setX($col2_x);
    $original_y = $pdf->getY();
    
    $borde = 1;
    $pdf->cell(14, $alto, ("Monto"), $borde, 0, 'C' );
    $pdf->cell(14, $alto, ("Fecha"), $borde, 0, 'C' );
    $pdf->cell(22, $alto, ("Cobrador"), $borde, 1, 'C' );


    // Col2
    /* $detalle_pagado = $db->query("SELECT movimientos.* FROM solicitudes 
    inner join movimientos on movprefijo = 'RC' AND movdocumento = solfactura
    WHERE solid = '$contrato'
    ORDER BY movfecha desc"); */

    $pdf->SetFont('Arial', '', $fz-1);
    $index_pagos = 0;
    $limit_pagos = 15;
    $before_recibo_y = $pdf->getY();
    //while($row_pago = $detalle_pagado->fetch(PDO::FETCH_ASSOC))
    for(;;)
    {
        if($row_pago)
        {
            if($row_pago['contrato'] == $contrato)
            {
                if($row_pago['valor'] > 0)
                {
                    $pdf->setX($col2_x);
                    $newformat = date("d/m/Y", strtotime($row_pago['fecha']));
                    $pdf->cell(14, $h_slim, $moneda.$row_pago['valor'], $borde, 0, 'C' );
                    $pdf->cell(14, $h_slim, $newformat, $borde, 0, 'C' );
                    $pdf->cell(22, $h_slim, substr($row_pago['cobrador'], 0, 14), $borde, 1, 'L' );
                }
                $index_pagos++;
                // Siguiente pago
                $row_pago = $datos_recibos->fetch(PDO::FETCH_ASSOC);
    
                if($index_pagos == $limit_pagos)
                {
                    // Ciclo infinito para recorrer todos los registros necesarios para pasar al primer pago del contrato consecuente
                    for(;;)
                    {
                        // Si es diferente al contrato actual entonces ya encontramos el primer pago del siguiente contrato
                        if($row_pago['contrato'] != $contrato)
                            break;
                            
                        // Si el contrato del pago es el mismo al contrato en ejecución entonces pasamos al siguiente en la lista
                        $row_productos = $datos_productos->fetch(PDO::FETCH_ASSOC);
                    }
                    break;
                }
            }
            else
                break;
        }
        else
            break;
    }
    
    $borde = 0;
    $pdf->SetXY($col2_x + 50, $original_y);
    
    $pdf->SetX($col2_x +50);
    $pdf->cell(12, $alto+3, utf8_decode("Abono:"), $borde, 0, 'L' );
    $borde = 1;
    $pdf->cell(25, $alto+3, "", $borde, 1, 'R' );
    
    $pdf->ln(1);
    $pdf->SetX($col2_x +50);
    $borde = 0;
    $pdf->cell(12, $alto+3, utf8_decode("Saldo:"), $borde, 0, 'L' );
    $borde = 1;
    $pdf->cell(25, $alto+3, "", $borde, 1, 'R' );
    $borde = 0;

    $pdf->ln(2);
    
    $pdf->SetX($col2_x + 50);
    $pdf->cell(20, $alto, utf8_decode("Saldo al día:"), $borde, 0, 'L' );
    $pdf->cell(18, $alto, $moneda.round($saldo_aldia, 2), $borde, 1, 'R' );
    
    $pdf->SetX($col2_x + 50);
    $pdf->line($pdf->getX()+1, $pdf->getY()+$alto-1, $pdf->getX()+37, $pdf->getY()+$alto-1);
    $pdf->cell(20, $alto, utf8_decode("Este pago:"), $borde, 0, 'L' );
    $pdf->cell(18, $alto, "-".$moneda.round($cuota, 2), $borde, 1, 'R' );

    $saldo_temp = round($saldo_aldia-$cuota, 2);
    $pdf->SetX($col2_x + 50);
    $pdf->cell(20, $alto, utf8_decode("Nuevo saldo:"), $borde, 0, 'L' );
    $pdf->cell(18, $alto, $moneda.round($saldo_temp, 2), $borde, 1, 'R' );
    $borde = 0;

    $after_recibo_y = $before_recibo_y + ($limit_pagos * $h_slim);
    $pdf->SetXY($col2_x, $after_recibo_y);
    $pdf->SetFont('Arial', 'B', $fz+2);
    $pdf->cell(20, $alto+2, utf8_decode("CONTRATO:"), $borde, 0, 'L' );
    $pdf->SetFont('Arial', '', $fz+2);
    $pdf->cell(20, $alto+2, $contrato, $borde, 0, 'L' );
    $pdf->SetFont('Arial', 'B', $fz+2);
    $pdf->cell(33, $alto+2, utf8_decode("FECHAS DE COBRO:"), $borde, 0, 'L' );
    $pdf->SetFont('Arial', '', $fz+2);
    $pdf->cell(15, $alto+2, $proximo_cobro, $borde, 1, 'L' );
    
    $pdf->SetFont('Arial', '', $fz);




    $pdf->SetX($col2_x);
    $pdf->cell(88, $alto, utf8_decode("ESTIMADO CLIENTE, REVISE SIEMPRE EL DETALLE, SI TIENE"), $borde, 1, 'C' );
    $pdf->SetX($col2_x);
    $pdf->cell(88, $alto, utf8_decode("ALGUNA DUDA O COMENTARIO, NO OLVIDE LLAMARNOS AL 2227-0700"), $borde, 1, 'C' );



    $count_contratos++;
    /* if($count_contratos == 4)
    break; */
}

//$pdf->setDisplayMode('fullpage', 'two');


$pdf->Output('recibos_a_cobrarse_contrato_'.$param_contrato.'.pdf', 'D');
?>