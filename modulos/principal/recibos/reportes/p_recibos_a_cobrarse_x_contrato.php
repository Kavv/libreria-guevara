<?php
$r = '../../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

?>

<!doctype html>
<html lang="es">

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-not-form.php');
    ?>
    <style>
        #form input {
            text-transform: uppercase;
        }
        .divisor {
            margin-left: 5px;
            margin-right: 5px;
            margin-top: 15px;
            margin-bottom: 15px;
            padding-top: 10px;
            padding-bottom: 2px;
            background-color: #004593;
            color: #fff;
            border-radius: 8px;
        }
        .divisor h6 {
            font-weight: bold; 
        }
        tr td {
            position: relative;
        }
        .f-size-15 {
            font-size: 15px;
        }
        .nota-importante{
            border: #000 solid 2px;
            padding: 10px;
            border-radius: 18px;
            text-align: center;
            background: #00205f;
            color: white;
        }
        .div-fixed-start {
            position: fixed;
            z-index: 10;
            right: 10px;
            bottom: 10px;
        }
        .div-fixed-end {
            position: fixed;
            z-index: 10;
            right: 10px;
            bottom: 50px;
        }
    </style>
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
    <link rel="stylesheet" href="<?php echo $r.'incluir/librerias/fontawesome/css/all.css'?>" />


    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
    <script src="<?php echo $r.'incluir/librerias/fontawesome/js/all.js'?>"></script>

	<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>


	<script type="text/javascript">
		$(document).ready(function() {
			$(".fecha").datepicker({
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
            });
            
            $("input").prop("autocomplete","off"); 

            $("#form").keypress(function(e) {
                if (e.which == 13) {
                    generarpdf();
                    return false;
                }
            });

		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="<?php echo $r ?>index.php">Principal</a>
				<div class="mapa_div"></div><a>Contrato</a>
				<div class="mapa_div"></div><a>Reporte</a>
				<div class="mapa_div"></div><a class="current">Imprimir Recibos a Cobrarse</a>
			</article>
			<article id="contenido">

                <div class="row">
                    <div class="col-md-4">
                        <form id="form" name="form" action="#" method="post">

                            <fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12">
                                <legend class="ui-widget ui-widget-header ui-corner-all">Parametros</legend>
                                <p>
                                    <label for="contrato"># Contrato</label>
                                    <input id="contrato" name="contrato" type="text" class="form-control not-w validate[required]" autocomplete="off">
                                </p>
                                <p>
                                    <label for="contrato">Proximo cobro</label>
                                    <input id="proximo" name="proximo" type="text" class="form-control not-w" autocomplete="off">
                                </p>
                            </fieldset>
                            
                            <button id="consultar" onclick="recibos_a_cobrarse_x_contrato();" type="button" class="btnconsulta btn btn-primary btn-block mt-2" name="consultar" value="Buscar">
                                Consultar
                            </button>
                        </form>
                    </div>
                </div>
                <div class="row">
                    <a href="#pdf-div" class='d-none'></a>
                    <div class="col-md-12" id="pdf-div">
                    </div>
                </div>
            </article>
        </article>
    </section>
	<?php
	if (isset($_GET['error'])) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['error'] . '</div>';
	elseif (isset($_GET['mensaje'])) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['mensaje'] . '</div>';
	?>


    <!-- Modal -->
    <div class="modal fade" id="mensaje" data-backdrop="static" data-keyboard='false' tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <label for="">Este proceso puede demorar <strong>varios segundos</strong> <br>No recargue la página <br>Espere pacientemente</label>
                </div>
            </div>
        </div>
    </div>
</body>
<script>

    function recibos_a_cobrarse_x_contrato()
    {
        var contrato = $("#contrato").val();
        var proximo = $("#proximo").val();
        var form = $("#form").validationEngine('validate');

        if(form && contrato != "")
        {
            ruta = "pdf_recivos_a_cobrarse_x_contrato.php?contrato=" + contrato + "&proximo=" + proximo;
            $("#mensaje").modal('show');
            //
            var xhr = new XMLHttpRequest();
            xhr.open('GET', ruta , true);
            xhr.responseType = 'blob';

            xhr.onload = function(e) {
            if (this.status == 200) {
                    var blob = new Blob([this.response], {type: 'application/pdf'});
                    var link = document.createElement('a');
                    link.href = window.URL.createObjectURL(blob);
                    link.download = "recibo_a_cobrarse_" + contrato + ".pdf";
                    link.click(); 
                    $("#mensaje").modal('hide');      
                }
                else
                    $("#mensaje").modal('hide');      
                
            };

            xhr.send();
            //var html = "<iframe onload='frameLoaded();' id='iframe-recibo' src='pdf_recivos_a_cobrarse_x_contrato.php?contrato=" + contrato +  "&proximo=" + proximo + "' width='1500' height='700'></iframe>";
            
            //$("#pdf-div").append(html);
        }
    }


</script>