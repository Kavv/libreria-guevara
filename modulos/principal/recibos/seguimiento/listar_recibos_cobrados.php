<?php
$r = '../../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
    
$contrato = strtoupper($_GET['contrato']);
$cobrador = $_GET['cobrador'];
$dep = $_GET['dep'];
$ciu = $_GET['ciudad'];
$desde = $_GET['rcd'];
$hasta = $_GET['rch'];

$con = "SELECT m.*, upper(usunombre) as nombre, i.comentario, i.digitador, solid FROM movimientos as m 
LEFT JOIN usuarios ON usuid = m.movcobrador
LEFT JOIN infoextra as i ON i.id = movinfo
LEFT JOIN solicitudes ON (solfactura = movdocumento and movprefijo = 'RC')";
$ord = ' ORDER BY m.created_at';

/* Los parametros de la consulta sql se genera dinamicamente 
en base a los datos recibidos para delimitar los resultados */
$parameters = [];
array_push($parameters, "solid != 'PERDIDO'" );
if($contrato != "")
    array_push($parameters, "solid = '$contrato'" );
if($cobrador != "")
    array_push($parameters, "m.movcobrador = '$cobrador'" );
if($dep != "")
    array_push($parameters, "departamento = '$dep'" );
if($ciu != "")
    array_push($parameters, "ciudad = '$ciu'" );
if($desde != "" && $hasta != "")
    array_push($parameters, "m.movfecha between '$desde' AND '$hasta'" );
    
    
if(count($parameters) <= 1)
{
    echo 1;
    exit();
}

// Consulta base
$sql = $con;
foreach ($parameters as $index => $parameter) {
    // Se agregan los parametros del WHERE
    if($index == 0)
        $sql .= " WHERE " . $parameter;
    else
        $sql .= " AND " . $parameter;
}
// Se completa la consulta
$sql .= $ord;
$qry = $db->query($sql);

$recibos = $qry->fetchAll(PDO::FETCH_ASSOC);
echo json_encode($recibos);
?>