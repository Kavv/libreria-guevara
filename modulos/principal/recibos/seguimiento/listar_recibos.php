<?php
$r = '../../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
    
$contrato = strtoupper($_GET['contrato']);
$cobrador = $_GET['cobrador'];
$dep = $_GET['dep'];
$ciu = $_GET['ciudad'];
$desde = $_GET['rcd'];
$hasta = $_GET['rch'];

$con = "SELECT r.*, upper(usunombre) as nombre, depnombre, ciunombre FROM recibos_a_cobrarse as r 
LEFT JOIN usuarios ON usuid = r.asignado
LEFT JOIN departamentos on depid = r.departamento
LEFT JOIN ciudades on (ciudepto = r.departamento AND ciuid = r.ciudad)";
$ord = ' ORDER BY procesado';

/* Los parametros de la consulta sql se genera dinamicamente 
en base a los datos recibidos para delimitar los resultados */
$parameters = [];
if($contrato != "")
    array_push($parameters, "contrato = '$contrato'" );
if($cobrador != "")
    array_push($parameters, "asignado = '$cobrador'" );
if($dep != "")
    array_push($parameters, "departamento = '$dep'" );
if($ciu != "")
    array_push($parameters, "ciudad = '$ciu'" );
if($desde != "" && $hasta != "")
    array_push($parameters, "cobrarse between '$desde' AND '$hasta'" );
    
if(count($parameters) == 0)
{
    echo 1;
    exit();
}

// Consulta base
$sql = $con;
foreach ($parameters as $index => $parameter) {
    // Se agregan los parametros del WHERE
    if($index == 0)
        $sql .= " WHERE " . $parameter;
    else
        $sql .= " AND " . $parameter;
}
// Se completa la consulta
$sql .= $ord;
$qry = $db->query($sql);

$recibos = $qry->fetchAll(PDO::FETCH_ASSOC);
echo json_encode($recibos);
?>