<?php
$r = '../../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

?>

<!doctype html>
<html lang="es">

<head>
    <title>SEGUIMIENTO DE RECIBOS - </title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-not-form.php');
    ?>
    <style>
        #form input {
            text-transform: uppercase;
        }
        .divisor {
            margin-left: 5px;
            margin-right: 5px;
            margin-top: 15px;
            margin-bottom: 15px;
            padding-top: 10px;
            padding-bottom: 2px;
            background-color: #004593;
            color: #fff;
            border-radius: 8px;
        }
        .divisor h6 {
            font-weight: bold; 
        }
        tr td {
            position: relative;
        }
        .f-size-15 {
            font-size: 15px;
        }
        .nota-importante{
            border: #000 solid 2px;
            padding: 10px;
            border-radius: 18px;
            text-align: center;
            background: #00205f;
            color: white;
        }
        .div-fixed-start {
            position: fixed;
            z-index: 10;
            right: 10px;
            bottom: 10px;
        }
        .div-fixed-end {
            position: fixed;
            z-index: 10;
            right: 10px;
            bottom: 50px;
        }
        .excluidos {
            background: #ffe2fd;
        }
        .dataTables_scrollBody
        {
            max-height: 30em!important;
        }
    </style>
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
    <link rel="stylesheet" href="<?php echo $r.'incluir/librerias/fontawesome/css/all.css'?>" />


    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
    <script src="<?php echo $r.'incluir/librerias/fontawesome/js/all.js'?>"></script>

	<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>


    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css" />
    <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
     
     <script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
     <script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>

    <script src="<?php echo $r.'incluir/kavv_js/kavvdt.js'?>" ></script>

	<script type="text/javascript">
        var $ciudades = [];
		$(document).ready(function() {
			$(".fecha").datepicker({
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
            });
            
            $("input").prop("autocomplete","off"); 

            $("#form").keypress(function(e) {
                if (e.which == 13) {
                    buscar_recibos();
                    return false;
                }
            });
            $("#form2").keypress(function(e) {
                if (e.which == 13) {
                    buscar_recibos_cobrados();
                    return false;
                }
            });

            // Almacenar las ciudades
            ruta = '../ajax/ciudades.php';
            $.get(ruta, function(res){
                res = JSON.parse(res);
                res.forEach(function(data){

                    var dep = data.ciudepto;
                    var ciu = data.ciuid;
                    var nombre = data.ciunombre;
                    // Si es diferente a null significa que el arreglo en el indice x ya posee datos
                    if($ciudades[dep] != null)
                    {
                        $ciudades[dep].push([ciu, nombre]);
                    }
                    else
                    {
                        // Si es la primera vez se asigna el primer dato como un arreglo
                        $ciudades[dep] = [[ciu, nombre]];
                    }
                });
            });
		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="<?php echo $r ?>index.php">Principal</a>
				<div class="mapa_div"></div><a>Contrato</a>
				<div class="mapa_div"></div><a>Reporte</a>
				<div class="mapa_div"></div><a class="current">Rastrear Recibos</a>
			</article>
			<article id="contenido">
            <div class="row">
                <div class="col-md-4">
                    <form id="form" name="form" class="form-style" action="#" method="post">

                        <fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12">
                            <legend class="ui-widget ui-widget-header ui-corner-all">Rastreo de recibo "a" cobrarse</legend>
                            <p>
                                <label for="contrato"># Contrato</label>
                                <input id="contrato" name="contrato" type="text" class="form-control not-w" autocomplete="off">
                            </p>
                            <p>
                                <label for="">Cobrador</label>
                                <select id="cobrador" name="cobrador" class="selectpicker" data-live-search="true" title="Seleccione un cobrador" data-width="100%">
                                    <option value="">Todos los cobradores de la fecha</option>
                                    <option value="null">Contratos sin cobrador asignado</option>
                                    <?php
                                        $query = $db->query("SELECT usuid, usunombre FROM usuarios WHERE usurelacionista = 1 ORDER BY usunombre");
                                        while($row = $query->fetch(PDO::FETCH_ASSOC))
                                        {
                                            $codigo = $row['usuid'];
                                            $nombre = "";
                                            if(TRIM($row['usunombre']) != "")
                                                $nombre .= "/" . $row['usunombre'];
                                            echo "<option value='". $row['usuid'] ."'>$codigo $nombre</option>";
                                        }
                                    ?>
                                </select>
                            </p>
                            <p>
                                <label for="">Recibo a cobrarse desde</label>
                                <input id="rcd" name="rcd" type="text" class="fecha form-control not-w" autocomplete="off">
                            </p>
                            <p>
                                <label for="">Recibo a cobrarse hasta</label>
                                <input id="rch" name="rch" type="text" class="fecha form-control not-w" autocomplete="off" value='<?php echo date('Y-m-d')?>'>
                            </p>
                            <p class="d-none">
                                <label for="Departamento">Departamento</label>
                                <select name="dep" id="departamento" class="form-control selectpicker" data-live-search="true" title="Selecciona un departamento"  data-width="100%">
                                    <option value=""></option>
                                    <?php
                                    $departamentos = $db->query("select  * from departamentos order by depnombre;");
                                    while ($dep = $departamentos->fetch(PDO::FETCH_ASSOC)) { ?>
                                        <option value="<?php echo $dep['depid'] ?>"><?php echo $dep['depnombre'] ?></option>
                                    <?php } ?>
                                </select>
                            </p>
                            <p class="d-none">
                                <label for="Departamento">Ciudades</label>
                                <select id="ciudad" name="ciudad" class="selectpicker" data-live-search="true" title="Selecciona una ciudad" data-width="100%">
                                    <option value=""></option>
                                </select>
                            </p>
                            <button onclick="buscar_recibos();" type="button" class="btnconsulta btn btn-primary btn-block my-2" name="consultar" value="Buscar">
                                Consultar
                            </button>
                        </fieldset>
                        
                    </form>
                </div>
                <div class="col-md-4">
                    <form id="form2" name="form2" class="form-style" action="#" method="post">

                        <fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12">
                            <legend class="ui-widget ui-widget-header ui-corner-all">Rastreo de recibos "ya" cobrados</legend>
                            <p>
                                <label for="contrato"># Contrato</label>
                                <input id="contrato2" name="contrato" type="text" class="form-control not-w" autocomplete="off">
                            </p>
                            <p>
                                <label for="recibo"># Recibo</label>
                                <input id="recibo" name="recibo" type="text" class="form-control not-w" autocomplete="off">
                            </p>
                            <p>
                                <label for="">Cobrador</label>
                                <select id="cobrador2" name="cobrador" class="selectpicker" data-live-search="true" title="Seleccione un cobrador" data-width="100%">
                                    <option value="">Todos los cobradores de la fecha</option>
                                    <option value="null">Contratos sin cobrador asignado</option>
                                    <?php
                                        $query = $db->query("SELECT usuid, usunombre FROM usuarios WHERE usurelacionista = 1 ORDER BY usunombre");
                                        while($row = $query->fetch(PDO::FETCH_ASSOC))
                                        {
                                            $codigo = $row['usuid'];
                                            $nombre = "";
                                            if(TRIM($row['usunombre']) != "")
                                                $nombre .= "/" . $row['usunombre'];
                                            echo "<option value='". $row['usuid'] ."'>$codigo $nombre</option>";
                                        }
                                    ?>
                                </select>
                            </p>
                            <p>
                                <label for="">Recibo cobrado desde</label>
                                <input id="rcd2" name="rcd" type="text" class="fecha form-control not-w" autocomplete="off">
                            </p>
                            <p>
                                <label for="">Recibo cobrado hasta</label>
                                <input id="rch2" name="rch" type="text" class="fecha form-control not-w" autocomplete="off" value='<?php echo date('Y-m-d')?>'>
                            </p>
                            <p class="d-none">
                                <label for="Departamento">Departamento</label>
                                <select name="dep" id="departamento2" class="form-control selectpicker" data-live-search="true" title="Selecciona un departamento"  data-width="100%">
                                    <option value=""></option>
                                    <?php
                                    $departamentos = $db->query("select  * from departamentos order by depnombre;");
                                    while ($dep = $departamentos->fetch(PDO::FETCH_ASSOC)) { ?>
                                        <option value="<?php echo $dep['depid'] ?>"><?php echo $dep['depnombre'] ?></option>
                                    <?php } ?>
                                </select>
                            </p>
                            <p class="d-none">
                                <label for="Departamento">Ciudades</label>
                                <select id="ciudad2" name="ciudad" class="selectpicker" data-live-search="true" title="Selecciona una ciudad" data-width="100%">
                                    <option value=""></option>
                                </select>
                            </p>
                            <button onclick="buscar_recibos_cobrados();" type="button" class="btnconsulta btn btn-primary btn-block my-2" name="consultar" value="Buscar">
                                Consultar
                            </button>
                        </fieldset>
                        
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <table id="tabla-recibos" class="table table-hover" style="min-width: 1260px; max-height: 20em;">
                            <thead>
                                <tr>
                                    <th style="width:3%;">#</th>
                                    <th style="width:7%;">Contrato</th>
                                    <th style="width:15%;">Código Cobrador</th>
                                    <th style="width:20%;">Nombre Cobrador</th>
                                    <th style="width:10%;">Fecha cobrarse</th>
                                    <th style="width:10%;">Fecha digitado</th>
                                    <th style="width:20%;">Observacion</th>
                                </tr>
                            </thead>
                            <tbody id="tb-recibo">
                            </tbody>
                        </table>
                        <table id="tabla-recibos-cobrados" class="table table-hover" style="min-width: 1260px; max-height: 20em;">
                            <thead>
                                <tr>
                                    <th style="width:3%;">#</th>
                                    <th style="width:7%;">Contrato</th>
                                    <th style="width:7%;">Recibo</th>
                                    <th style="width:15%;">Código Cobrador</th>
                                    <th style="width:20%;">Nombre Cobrador</th>
                                    <th style="width:10%;">Fecha cobrarse</th>
                                    <th style="width:10%;">Fecha digitado</th>
                                    <th style="width:10%;">Digitador</th>
                                    <th style="width:20%;">Observacion</th>
                                </tr>
                            </thead>
                            <tbody id="tb-recibo-cobrados">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            </article>
        </article>
    </section>
	<?php
	if (isset($_GET['error'])) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['error'] . '</div>';
	elseif (isset($_GET['mensaje'])) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['mensaje'] . '</div>';
	?>

    <!-- Modal -->
    <div class="modal fade" id="mensaje" data-backdrop="static" data-keyboard='false' tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <label for="">Este proceso puede demorar <strong>varios segundos</strong> <br>No recargue la página <br>Espere pacientemente</label>
                </div>
            </div>
        </div>
    </div>
    
	<?php require($r . 'incluir/src/loading.php'); ?>
</body>
<script>
    var dt, dt2;
    var very_fast = false;
    function buscar_recibos()
    {
        var recibo = $("#recibo").val();
        var contrato = $("#contrato").val();
        var cobrador = $("#cobrador").val();
        var departamento = $("#departamento").val();
        var ciudad = $("#ciudad").val();
        var rcd = $("#rcd").val();
        var rch = $("#rch").val();
        

        var form = $("#form").validationEngine('validate');
        var ruta  = "listar_recibos.php?";
        ruta += $("#form").serialize();
        
        if(recibo != "" || contrato != "" || cobrador != "" || departamento != "" || ciudad != "" || (rcd != "" && rch != "") )
        {
            if(dt != null)
            {
                dt.destroy();
                dt = null;
            }
            if(dt2 != null)
            {
                dt2.destroy();
                dt2 = null;
            }
                
            $('#tabla-recibos-cobrados').css('display','none');
            $('#tabla-recibos').css('display','block');
            $('#tb-recibo').empty();
            $("#loading").css("display","block");;
            setTimeout(fixed_bug_modal, 1000);
            very_fast = false;
            $.get(ruta, function(res){
                res = JSON.parse(res);
                if(res != 1)
                {
                    $("#tb-recibo").empty();
                    var index = 1;
                    res.forEach(function(data){
                        
                        var html = "<tr>" +
                            "<td>"+index+"</td>"+
                            "<td>"+data.contrato+"</td>"+
                            "<td>"+data.asignado+"</td>"+
                            "<td>"+data.nombre+"</td>"+
                            "<td>"+data.cobrarse+"</td>"+
                            "<td>"+data.procesado.slice(0, -7)+"</td>"+
                            "<td>"+data.observaciones+"</td>"+
                        "</tr>";
                        index++;
                        $("#tb-recibo").append(html);
                    });
                }
                $("#loading").css("display","none");;
                dt = createdt($("#tabla-recibos"),{buttons: bootstrapButtons, dom: bootstrapDesingSlimButtons});
                very_fast = true;
            });
        }
        
    }

    function fixed_bug_modal()
    {
        if(very_fast)
        $("#loading").css("display","none");;
    }

    function buscar_recibos_cobrados()
    {
        var recibo = $("#recibo").val();
        var contrato = $("#contrato2").val();
        var cobrador = $("#cobrador2").val();
        var departamento = $("#departamento2").val();
        var ciudad = $("#ciudad2").val();
        var rcd = $("#rcd2").val();
        var rch = $("#rch2").val();
        

        var form = $("#for2").validationEngine('validate');
        var ruta  = "listar_recibos_cobrados.php?";
        ruta += $("#form2").serialize();
        
        if(recibo != "" || contrato != "" || recibo != "" || cobrador != "" || departamento != "" || ciudad != "" || (rcd != "" && rch != "") )
        {
            if(dt != null)
            {
                dt.destroy();
                dt = null;
            }
            if(dt2 != null)
            {
                dt2.destroy();
                dt2 = null;
            }
            
            $('#tabla-recibos').css('display','none');
            $('#tabla-recibos-cobrados').css('display','block');
            $('#tb-recibo-cobrados').empty();
            $("#loading").css("display","block");;
            setTimeout(fixed_bug_modal, 1000);
            very_fast = false;
            $.get(ruta, function(res){
                res = JSON.parse(res);
                if(res != 1)
                {
                    $("#tb-recibo-cobrados").empty();
                    var index = 1;
                    res.forEach(function(data){
                        if(data.digitador == null)
                            var digitador = "-";
                        if(data.observaciones == null)
                            var digitador = "-";
                        var html = "<tr>" +
                            "<td>"+index+"</td>"+
                            "<td>"+data.solid+"</td>"+
                            "<td>"+data.movnumero+"</td>"+
                            "<td>"+data.movcobrador+"</td>"+
                            "<td>"+data.nombre+"</td>"+
                            "<td>"+data.movfecha+"</td>"+
                            "<td>"+data.created_at.slice(0, -7)+"</td>"+
                            "<td>"+data.digitador+"</td>"+
                            "<td>"+data.comentario+"</td>"+
                        "</tr>";
                        index++;
                        $("#tb-recibo-cobrados").append(html);
                    });
                }
                dt2 = createdt($("#tabla-recibos-cobrados"),{buttons: bootstrapButtons, dom: bootstrapDesingSlimButtons});
                $("#loading").css("display","none");;
                very_fast = true;
            });
        }
        
    }


    
    function recibos_a_cobrarse_x_rango()
    {
        var dep = $("#departamento").val();
        var dep_nombre = $("#departamento option:selected").text();
        var desde = $("#desde").val();
        var hasta = $("#hasta").val();
        var cobro_desde = $("#cobro_desde").val();
        var cobro_hasta = $("#cobro_hasta").val();
        var moneda = $("#moneda").val();

        var form = $("#form").validationEngine('validate');


        if(form && dep != "" && desde != "" && hasta != "" &&  cobro_desde != "" &&  cobro_hasta != "" &&  moneda != "" )
        {
            //ruta = "recivos_a_cobrarse_v2.php?dep="+ dep +"&desde="+ desde +"&hasta="+ hasta +"&cd="+ cobro_desde +"&ch="+ cobro_hasta +"&moneda="+ moneda;
            var ruta = "recivos_a_cobrarse_v2.php?";
            ruta += $("#form").serialize();
            var pdf_name = "recibos_cobrarse_" + dep_nombre +" cobro desde "+cobro_desde+" hasta "+ cobro_hasta + ".pdf";

            $("#loading").css("display","block");
            //
            var xhr = new XMLHttpRequest();
            xhr.open('GET', ruta , true);
            xhr.responseType = 'blob';

            xhr.onload = function(e) {
            if (this.status == 200) {
                    var blob = new Blob([this.response], {type: 'application/pdf'});
                    var link = document.createElement('a');
                    link.href = window.URL.createObjectURL(blob);
                    link.download = pdf_name;
                    link.click(); 
                    $("#loading").css("display","none");      
                }
                else
                    $("#loading").css("display","none");      
                
            };

            xhr.send();
            //var html = "<iframe onload='frameLoaded();' id='iframe-recibo' src='pdf_recivos_a_cobrarse_x_contrato.php?contrato=" + contrato +  "&proximo=" + proximo + "' width='1500' height='700'></iframe>";
            
            //$("#pdf-div").append(html);
        }
    }

    $("#departamento").change(function(){
        $("#ciudad").empty();
        // Por defecto
        var html = '<option value=""></option>';
        $("#ciudad").append(html);

        dep = $(this).val();
		if(dep == "")
			cant = 0;
		else
        cant = $ciudades[dep].length;
        for(var i = 0; i < cant; i++)
        {
            html = "<option value='" + $ciudades[dep][i][0] + "'>" + $ciudades[dep][i][1] + "</option>";
            $("#ciudad").append(html);
        }
        $('#ciudad').selectpicker('refresh');
    });
    
</script>