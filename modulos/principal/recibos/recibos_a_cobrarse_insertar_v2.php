<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

?>

<!doctype html>
<html lang="es">

<head>
    <title>AGREGAR RECIBOS A COBRARSE</title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-not-form.php');
    ?>
    <style>
        #form input {
            text-transform: uppercase;
        }
        .divisor {
            margin-left: 5px;
            margin-right: 5px;
            margin-top: 15px;
            margin-bottom: 15px;
            padding-top: 10px;
            padding-bottom: 2px;
            background-color: #004593;
            color: #fff;
            border-radius: 8px;
        }
        .divisor h6 {
            font-weight: bold; 
        }
        tr td {
            position: relative;
        }
        .f-size-15 {
            font-size: 15px;
        }
        .nota-importante{
            border: #000 solid 2px;
            padding: 10px;
            border-radius: 18px;
            text-align: center;
            background: #00205f;
            color: white;
        }
        .div-fixed-start {
            position: fixed;
            z-index: 10;
            right: 10px;
            bottom: 10px;
        }
        .div-fixed-end {
            position: fixed;
            z-index: 10;
            right: 10px;
            bottom: 50px;
        }
        .border-dark{
            border: 1px solid #505050 !important;
        }
    </style>
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
    <link rel="stylesheet" href="<?php echo $r.'incluir/librerias/fontawesome/css/all.css'?>" />


    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
    <script src="<?php echo $r.'incluir/librerias/fontawesome/js/all.js'?>"></script>

	<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>
	

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css" />
    <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>

    <script src="<?php echo $r.'incluir/kavv_js/kavvdt.js'?>" ></script>

    <script type="text/javascript">
		var $select_cobradores = "";
		$(document).ready(function() {
			$(".fecha").datepicker({
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
            });
            
            $("input").prop("autocomplete","off"); 
            
            var ruta = 'ajax/cobradores.php';
            $select_cobradores += "<select class='disabled-input cobradores-edit form-control validate[required] selectpicker' data-live-search='true' title='Seleccione un cobrador'  name='detalle-cobrador[]'>";
            $select_cobradores += "<option value=''></option>";
            $.get(ruta, function(res){
                res = JSON.parse(res);
                res.forEach(function(data){
                    nombre = "";
                    if(data.usunombre != "")
                        nombre += "/" + data.usunombre;
                    $select_cobradores += "<option value='"+ data.usuid +"'> "+ data.usuid + nombre +"</option>";
                });
            $select_cobradores += "</select>";
            });

            $("#form").keypress(function(e) {
                if (e.which == 13) {
                    generar_lista();
                    return false;
                }
            });

            // Almacenar las ciudades
            ruta = 'ajax/ciudades.php';
            $ciudades = [];
            $.get(ruta, function(res){
                res = JSON.parse(res);
                res.forEach(function(data){

                    var dep = data.ciudepto;
                    var ciu = data.ciuid;
                    var nombre = data.ciunombre;
                    // Si es diferente a null significa que el arreglo en el indice x ya posee datos
                    if($ciudades[dep] != null)
                    {
                        $ciudades[dep].push([ciu, nombre]);
                    }
                    else
                    {
                        // Si es la primera vez se asigna el primer dato como un arreglo
                        $ciudades[dep] = [[ciu, nombre]];
                    }
                });
            });

            
            $('#form2').validationEngine({
				showOneMessage: true,
				onValidationComplete: function(form, status) {
					if (status) {
						return true;
					}
				}
			});
		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="<?php echo $r ?>index.php">Principal</a>
				<div class="mapa_div"></div><a>Contrato</a>
				<div class="mapa_div"></div><a class="current">Agregar Recibos a Cobrarse</a>
			</article>
            <div class="div-fixed-start">
                <div>
                    <a href="#end-list" class="btn btn-info">Bajar <i class="fas fa-arrow-circle-down"></i></a>
                </div>
            </div>
            <div class="div-fixed-end">
                <div>
                    <a href="#start-list" class="btn btn-info">Subir <i class="fas fa-arrow-circle-up"></i></a>
                </div>
            </div>
			<article id="contenido">
                <div id="msj-parametros"></div>
				<form id="form" name="form" method="post">
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12">
						<legend class="ui-widget ui-widget-header ui-corner-all">AGREGAR RECIBOS A COBRARSE</legend>
                        
                        <p class="mb-2 f-size-15 nota-importante">Esta funcionalidad permite <strong style='color: #ff8d00'>mostrar los contratos a cobrarse</strong> en base a los parametros que se establezcan</p>

                        <div class="row">
                            <div class="col-md-4">
                                <label for=""><span style="color:red;">*</span>Día a cobrarse - inicio <span style="color:red">(obligatorio)</span></label>
							    <input placeholder="Digite un numero entre 1 a 31" type="text" class="form-control not-w validate[required, custom[integer], min[1], max[31]]" id="fecha-cobro" name="fecha-cobro" value=""/> 
                            </div>
                            <div class="col-md-4">
                                <label for=""><span style="color:red;">*</span>Día a cobrarse - fin <span style="color:red">(obligatorio)</span></label>
							    <input placeholder="Digite un numero entre 1 a 31" type="text" class="form-control not-w validate[required, custom[integer], min[1], max[31]]" id="fecha-cobro-end" name="fecha-cobro-end" value=""/> 
                            </div>

                            
                            <div class="col-md-4">
                            
                                <label for="">Fecha de cobro <span style="color:red">(obligatorio)</span></label>
							    <input type="text" class="form-control fecha not-w validate[required, custom[date]]" id="fecha-cobrarse" name="fecha-cobrarse" value=""/> 
                            </div>
                        </div>



                        <div class="row">
                        
                            <div class="col-md-6">
                                <label for="">Departamento <span style="color:red">(obligatorio)</span></label>
                                <select id="departamento" name="departamento" class="selectpicker validate[required]" data-live-search="true" title="Seleccione un departamento" data-width="100%">
                                    <option value="">Todos los departamentos</option>
                                    <option value="0">Contrato sin departamento</option>
                                    <?php
                                        $query = $db->query("SELECT * FROM departamentos ORDER BY depnombre ASC");
                                        while($row = $query->fetch(PDO::FETCH_ASSOC))
                                        {
                                            $codigo = $row['depid'];
                                            $nombre = $row['depnombre'];
                                            echo "<option value='". $codigo ."'>$nombre</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label for="">Ciudades</label>
                                <select id="ciudad" name="ciudad" class="selectpicker" data-live-search="true" title="Selecciona una ciudad" data-width="100%">
                                    <option value="">Todas las ciudades</option>
                                    <option value="0">Contrato sin ciudades</option>
                                </select>
                            </div>
                        </div>

                        <div class="row">
                        </div>

                        <p class="f-size-15 my-4 nota-importante">En caso de querer <strong><span style="color:#ff8d00">excluir</span></strong> contratos de algun año debe establecer un rango de fecha (opcional) 
                        <br>Al no establecer un rango de fecha se tomaran en cuenta <strong style="color:#ff8d00">todos los contratos</strong></p>
                        <div class="row">
                            <div class="col-md-6">
                                <label for="">Contratos desde</label>
							    <input type="text" class="form-control fecha not-w validate[custom[date]]" id="contratos-desde" name="contratos-desde" value=""/> 
                            </div>
                            <div class="col-md-6">
                                <label for="">Contratos Hasta</label>
							    <input type="text" class="form-control fecha not-w validate[custom[date]]" id="contratos-hasta" name="contratos-hasta" value=""/> 
                            </div>
                        </div>

                        <p class="f-size-15 my-4 nota-importante">En caso de querer filtrar los contratos a cobrarse <strong><span style="color:#ff8d00">por el cobrador</span></strong>, 
                        debe dar click en el boton "Ajustar Cobradores" para facilitar la busqueda de los cobradores con contratos asignados 
                        (en base a los parametros previamente ingresado)</p>
                        <div class="row">
                            <div class="col-md-4">
                                <button class="btn btn-info btn-block" type="button" onclick="update_cobrador();">Ajustar Cobradores</button>
                            </div>
                            <div class="col-md-8">
                                <select id="cobrador" name="cobrador" class="selectpicker" data-live-search="true" title="Seleccione un cobrador" data-width="100%">
                                    <option value="">Todos los cobradores de la fecha</option>
                                    <option value="null">Contratos sin cobrador asignado</option>
                                    <?php
                                        $query = $db->query("SELECT usuid, usunombre FROM usuarios WHERE usurelacionista = 1 ORDER BY usunombre");
                                        while($row = $query->fetch(PDO::FETCH_ASSOC))
                                        {
                                            $codigo = $row['usuid'];
                                            $nombre = "";
                                            if(TRIM($row['usunombre']) != "")
                                                $nombre .= "/" . $row['usunombre'];
                                            echo "<option value='". $row['usuid'] ."'>$codigo $nombre</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <p class="f-size-15 my-4 nota-importante"><strong>Una vez especifique los campos <strong style='color: #ff8d00;'>obligatorios</strong> ya puede generar la correspondiente lista de contratos a cobrarse</strong></p>
                        <div class="row my-3">
                            <div class="col-md-3 text-center mb-1">
                                <button class="btn btn-success btn-block" type="button" onclick="generar_lista();">Generar Lista</button>
                            </div>
                            <div class="col-md-3 text-center mb-1">
                                <button class="btn btn-success btn-block" type="button" data-toggle="modal" data-target="#add-manual">Agregar 1 manual</button>
                            </div>
                        </div>
                    </fieldset>
                </form>
                <form id="form2" name="form2" method="post">
                    <div id='msj'></div>
                    <div class="row" style="overflow-x: auto;" id="start-list">
                        <table id="tabla-recibos" class="table table-hover" style="min-width: 1400px; width:100%;">
                            <thead>
                                <tr>
                                    <th style="width:5%; text-align:center;"></th>
                                    <th style="width:5%; text-align:center;">#</th>
                                    <th style="width:5%;">Cobro</th>
                                    <th style="width:10%;"><spam class="text-danger">*</spam>Contrato</th>
                                    <th style="width:12%;"><spam class="text-danger">*</spam>F. Pago</th>
                                    <th style="width:20%;"><spam class="text-danger">*</spam>Cobrador</th>
                                    <th style="width:13%;">Dep</th>
                                    <th style="width:12%;">Ciu</th>
                                    <th >Comentario</th>
                                </tr>
                            </thead>
                            <tbody id="tb-recibo">
                            </tbody>
                            <tbody id="tb-recibo-manual">
                            </tbody>
                        </table>
                    </div>
                </form>
                        
                <p class="f-size-15 my-4 nota-importante">Para <strong><span style="color:#ff8d00">cambiar de forma masiva el cobrador</span></strong> que se muestra en la lista, 
                <br>Debe especificar los siguientes parametros, y dar click al botón </strong>"Cambiar Cobrador"</strong></p>
                
                <div id="msj2" class="alert alert-success alert-dismissible fade show" role="alert" style="display:none">
                    <strong>¡Se han cambiado los cobradores en el rango especificado!</strong>
                    <button type="button" class="close" onclick="$('#msj2').slideUp();"  aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div id="msjerror" class="alert alert-danger alert-dismissible fade show" role="alert" style="display:none">
                    <strong>¡Error! ¡Almenos un parametro esta incorrecto!</strong>
                    <button type="button" class="close" onclick="$('#msjerror').slideUp();" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label for="">Cobrador que reemplaza al original</label>
                    
                        <select id="cobrador-reemplaza" name="cobrador-reemplaza" class="selectpicker" data-live-search="true" title="Seleccione un cobrador" data-width="100%">
                            <?php
                                $query = $db->query("SELECT usuid, usunombre FROM usuarios WHERE usurelacionista = 1 ORDER BY usunombre");
                                while($row = $query->fetch(PDO::FETCH_ASSOC))
                                {
                                    $codigo = $row['usuid'];
                                    $nombre = "";
                                    if(TRIM($row['usunombre']) != "")
                                        $nombre .= "/" . $row['usunombre'];
                                    echo "<option value='". $row['usuid'] ."'>$codigo $nombre</option>";
                                }
                            ?>
                        </select>

                    </div>
                    <div class="col-md-3">
                        <label for="">Desde la fila #</label>
                        <input type="text" class="form-control not-w validate[custom[integer]] border-dark" id="rango-desde" name="rango-desde" value=""/> 
                    </div>
                    <div class="col-md-3">
                        <label for="">Hasta la fila #</label>
                        <input type="text" class="form-control not-w validate[custom[integer]] border-dark" id="rango-hasta" name="rango-hasta" value=""/> 
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-md-12">
                        <button class="btn btn-danger" id="btn-cobrador-cambio" type="button" onclick="pre_cambiar_cobrador();">Cambiar Cobrador</button>
                    </div>
                </div>


                <div class="row d-flex justify-content-center mt-4" id="end-list">
                    <div class="col-md-6">
                        <button id="guardar" type="button" name="guardar-contrato" class="btn btn-primary btn-block">GUARDAR</button>
                    </div>      
                    <div class="col-md-6">
                        <button type="button" class="btn btn-block btn-secondary" data-toggle="modal" data-target="#modal-reporte">IMPRIMIR LISTA</button>
                    </div>
                </div>
            </article>
        </article>
    </section>
    <!-- Modal -->
    <div class="modal fade" id="add-manual" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Agregar Manual</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- <div class="alert alert-danger" role="alert">
                    El contrato no existe
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div> -->
                <div id='msj-modal'></div>
                <div class="row">
                    <div class="col-md-6">
                        <label for=""># DE CONTRATO <span style="color:red">(obligatorio)</span></label>
                        <input type="text" class="form-control border-dark uppercase" id="contrato-manual">
                    </div>
                    <div class="col-md-6">
                        <label for="">Cobrador</label>
                        <select id="cobrador-manual" name="cobrador-manual" class="selectpicker" data-live-search="true" title="Seleccione un cobrador" data-width="100%">
                            <?php
                                $query = $db->query("SELECT usuid, usunombre FROM usuarios WHERE usurelacionista = 1 ORDER BY usunombre");
                                while($row = $query->fetch(PDO::FETCH_ASSOC))
                                {
                                    $codigo = $row['usuid'];
                                    $nombre = "";
                                    if(TRIM($row['usunombre']) != "")
                                        $nombre .= "/" . $row['usunombre'];
                                    echo "<option value='". $row['usuid'] ."'>$codigo $nombre</option>";
                                }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label for="">Departamento</label>
                        <select id="dep-manual" class="selectpicker validate[required]" data-live-search="true" title="Seleccione un departamento" data-width="100%">
                            <?php
                                $query = $db->query("SELECT * FROM departamentos ORDER BY depnombre ASC");
                                while($row = $query->fetch(PDO::FETCH_ASSOC))
                                {
                                    $codigo = $row['depid'];
                                    $nombre = $row['depnombre'];
                                    echo "<option value='". $codigo ."'>$nombre</option>";
                                }
                            ?>
                        </select>
                    </div>
                    <div class="col-md-6">
                        <label for="">Ciudades</label>
                        <select id="ciu-manual" class="selectpicker" data-live-search="true" title="Selecciona una ciudad" data-width="100%">
                            <option value="">Todas las ciudades</option>
                            <option value="0">Contrato sin ciudades</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="add_recibo_manual();" >Agregar</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            </div>
            </div>
        </div>
    </div>
	<?php
	if (isset($_GET['error'])) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['error'] . '</div>';
	elseif (isset($_GET['mensaje'])) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['mensaje'] . '</div>';
	?>
    
    <!-- Modal -->
    <div class="modal fade" id="mensaje" data-backdrop="static" data-keyboard='false' tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <label for="">Este proceso puede demorar <strong>varios segundos</strong> <br>No recargue la página <br>Espere pacientemente</label>
                </div>
            </div>
        </div>
    </div>

	<?php require('modal_listas.php'); ?>
</body>
<script>
    var prueba;
    var index_recibos = 1;
    var dt = null;
    function generar_lista()
    {
        var cobrador = $("#cobrador option:selected");
        var codigo = cobrador.val();
        var cobro_start = $("#fecha-cobro").val();
        var cobro_end = $("#fecha-cobro-end").val();
        var dep = $("#departamento").val();
        var ciu = $("#ciudad").val();
        var fecha_cobrarse = $("#fecha-cobrarse").val();
        var desde = $("#contratos-desde").val();
        var hasta = $("#contratos-hasta").val();
        var fechacobrarse = $("#fecha-cobrarse").val();
        $("#fecha-cobro").validationEngine('validate');
        $("#fecha-cobro-end").validationEngine('validate');
        $("#departamento").validationEngine('validate');
        $("#fecha-cobrarse").validationEngine('validate');
        var format_desde = $("#contratos-desde").validationEngine('validate');
        var format_hasta = $("#contratos-hasta").validationEngine('validate');
        if(cobro_start != "" && cobro_end != "" && fecha_cobrarse !="" && !format_desde && !format_hasta && dep != "" && fechacobrarse != "" )
        {
            carga();
            var nombre = cobrador.text(); 
            //$("#cobrador-aux").text(nombre);
            ruta = "ajax/recibos_cobrarse_v2.php?cobro_start=" + cobro_start + "&cobro_end=" + cobro_end + "&departamento=" + dep + "&ciudad=" + ciu + "&cobrador=" + codigo + "&desde=" + desde + "&hasta=" + hasta + '&fechacobrarse='+ fechacobrarse;
            
            if(dt != null)
                dt.destroy();
            $('#tb-recibo').empty();
            index_recibos = 1;
            $("#rango-desde").val(0);
            $("#rango-hasta").val(0);
            $.get(ruta, function(res) {
                res = JSON.parse(res);
                res.forEach(function(data){
                    recibo_factura = data.solfactura;
                    recibo_contrato = data.solid;
                    recibo_numero = 0;//data.movnumero; 
                    recibo_monto = 0; //data.movvalor; 
                    recibo_fecha = fecha_cobrarse;
                    recibo_codigo = data.solrelacionista;
                    recibo_nombre = "";
                    recibo_dep = data.depnombre;
                    recibo_ciu = data.ciunombre;
                    recibo_cobro = data.cobro;
                    recibo_dep_id = data.depid;
                    recibo_ciu_id = data.ciuid;
                    
                    add_recibo("#tb-recibo", recibo_cobro, recibo_factura, recibo_contrato, recibo_numero, 
                    recibo_monto, recibo_fecha, recibo_codigo, recibo_nombre, recibo_dep, recibo_ciu,
                    recibo_dep_id, recibo_ciu_id);
                });
                if(index_recibos == 1)
                    $("#rango-desde").val(0);
                else
                    $("#rango-desde").val(1);
                $("#rango-hasta").val(index_recibos-1);
                input_fecha();
                actualizar_select();
                dt = createdt($("#tabla-recibos"));
                $('#carga').dialog('close');
            });
            /* for(var i = 0; i < cantidad_registros; i++)
            {
                add_recibo(fecha, codigo, nombre);
            } */
        } 
    }



    function add_recibo_manual()
    {
        var contrato = $("#contrato-manual").val();
        var cobrador = $("#cobrador-manual").val();
        var dep = $("#dep-manual").val();
        var ciu = $("#ciu-manual").val();
        if(contrato != "")
        {
            carga();
            ruta = "ajax/search_contrato.php?contrato=" + contrato;
            
            $.get(ruta, function(data) {
                data = JSON.parse(data);
                var fecha_cobrarse = $("#fecha-cobrarse").val();
                if(data)
                {
                    recibo_factura = data.solfactura;
                    recibo_contrato = data.solid;
                    recibo_numero = 0;//data.movnumero; 
                    recibo_monto = 0; //data.movvalor; 
                    recibo_fecha = fecha_cobrarse;
                    recibo_nombre = "";
                    recibo_dep = data.depnombre;
                    recibo_ciu = data.ciunombre;
                    recibo_cobro = data.cobro;
                    recibo_dep_id = data.depid;
                    recibo_ciu_id = data.ciuid;
                    recibo_codigo = data.solrelacionista;
                    
                    if(cobrador != "")
                        recibo_codigo = cobrador;
                    else
                        recibo_codigo = recibo_codigo;


                    add_recibo("#tb-recibo-manual", recibo_cobro, recibo_factura, recibo_contrato, recibo_numero, 
                    recibo_monto, recibo_fecha, recibo_codigo, recibo_nombre, recibo_dep, recibo_ciu,
                    recibo_dep_id, recibo_ciu_id);

                    input_fecha();
                        
                    $ultimo_select = $("#tb-recibo-manual select.cobradores-edit").last().eq(0);
                    $ultimo_select.selectpicker('refresh');
                    $ultimo_select.val(recibo_codigo);
                    $ultimo_select.selectpicker('refresh');
                    
                    $("#add-manual").modal("hide");
                    $('#msj-modal').slideUp();
                }
                else
                {
                    if(cobrador != "" && dep != "" && ciu != "")
                    {
                        var fecha_cobrarse = $("#fecha-cobrarse").val();
                        var nombre_dep = $("#dep-manual option:selected").text();
                        var nombre_ciu = $("#ciu-manual option:selected").text();
                        add_recibo("#tb-recibo-manual", "-", "null", contrato, "0", 
                        "0", fecha_cobrarse, cobrador, "", nombre_dep, nombre_ciu,
                        dep, ciu);
                        
                        input_fecha();
                        actualizar_select();
                        $("#add-manual").modal("hide");
                        $('#msj-modal').slideUp();
                    }
                    else
                        make_alert({object :$('#msj-modal'), type : 'danger', message : 'El # de contrato no existe por lo que debe llenar obligatoriamente todos los datos'})

                }

                $('#carga').dialog('close');
            });
        }
        else
        {
            make_alert({object :$('#msj-modal'), type : 'danger', message : 'Debe especificar un número de contrato'})
        }
    }

    function update_cobrador()
    { 
        var cobro_start = $("#fecha-cobro").val();
        var cobro_end = $("#fecha-cobro-end").val();
        var dep = $("#departamento").val();
        var ciu = $("#ciudad").val();
        var desde = $("#contratos-desde").val();
        var hasta = $("#contratos-hasta").val();
        var format_desde = $("#contratos-desde").validationEngine('validate');
        var format_hasta = $("#contratos-hasta").validationEngine('validate');
        if(!format_desde && !format_hasta)
        {
            carga(); 
            //$("#cobrador-aux").text(nombre);
            ruta = "ajax/recibos_cobrarse_v2.php?cobro_start=" + cobro_start + "&cobro_end=" + cobro_end + "&departamento=" + dep + "&ciudad=" + ciu +  "&cobrador=&ajustar_cobrador=1&desde=" + desde + "&hasta=" + hasta + '&fechacobrarse=';
            
            $.get(ruta, function(res) {
                res = JSON.parse(res);
                var cobrador_codigo;
                var cobrador_nombre;
                $("#cobrador").empty();
                // Por defecto
                var html = '<option value="">Todos los cobradores de la fecha</option>'+
                            '<option value="null">Contratos sin cobrador asignado</option>';
                            
                $("#cobrador").append(html);

                res.forEach(function(data){
                    cobrador_codigo = data.usuid;
                    cobrador_nombre = data.usunombre;
                    
                    generate_select_cobradores(cobrador_codigo, cobrador_nombre);
                });
                $('#cobrador').selectpicker('refresh');
                input_fecha();
                actualizar_select();
                $('#carga').dialog('close');
            });
            /* for(var i = 0; i < cantidad_registros; i++)
            {
                add_recibo(fecha, codigo, nombre);
            } */
        } 
    }

    function generate_select_cobradores(codigo,nombre)
    {
        var cobrador = $("#cobrador");
        var html = "<option value='" + codigo + "'>" + codigo + " / " + nombre + "</option>";
        $("#cobrador").append(html);
    }


    function actualizar_select()
    {
        $(".cobradores-edit").selectpicker();
        var codigos = $(".codigo-cobrador-recibo");
        var cant = codigos.length;
        var select_cobradores = $("select.cobradores-edit");
        for(var i = 0; i < cant; i++)
        {
            select_cobradores.eq(i).val(codigos.eq(i).val());
        }
        select_cobradores.selectpicker('refresh');
        
    }

    function add_recibo(obj, cobro, factura, contrato, recibo, monto, fecha, codigo, cobrador, dep, ciu, depid, ciuid)
    {
        html = "<tr class='recibo-item'>" +
            "<td><button type='button' class='btn btn-danger' onclick='remove_recibo(this);'>-</button></td>"+
            "<td class='text-center'><span class='index-recibos'>"+ index_recibos +"</span></td>" +
            "<td class='text-center'>"+ cobro +"</td>" +
            "<td>"+
                "<input value='" + factura + "' type='hidden' class='factura-recibo'>" +
                "<input value='" + contrato + "' type='hidden' name='detalle-contrato[]' class='contrato-original'>" +
                contrato +
            "</td>" +
            "<td><input value='" + fecha + "' type='text' class='disabled-input detalle-fpago form-control fecha validate[required]' name='detalle-fpago[]' autocomplete='off' value='"+ fecha +"'></td>" +
            "<td>"+
                "<input type='hidden' class='codigo-cobrador-recibo' value='"+ codigo +"'>" +
                $select_cobradores +
            "</td>" +
            "<td>"+
                "<input value='" + depid + "' type='hidden' class='depid-recibo' name='detalle-depid[]'>" +
                "<p>"+ dep +"</p>" +
            "</td>" +
            "<td>"+
                "<input value='" + ciuid + "' type='hidden' class='ciuid-recibo' name='detalle-ciuid[]'>" +
                "<p>"+ ciu +"</p>" +
            "</td>" +
            "<td>"+
                "<textarea style='width:200px;' row='1' class='form-control r-comentario validate[maxSize[199]]' name='detalle-comentarios[]' value=''></textarea>" +
            "</td>" +
        "</tr>";

        $(obj).append(html);
        index_recibos++;
    } 

    function remove_recibo(element)
    {
        var fila = $(element);
        prueba = fila;
        fila.parents(".recibo-item").remove();
        index_recibos--;
        actualizar_index();
    }

    function input_fecha()
    {
        $(".fecha").datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
        });
    }



    function check(e) {
        tecla = (document.all) ? e.keyCode : e.which;

        //Tecla de retroceso para borrar, siempre la permite
        if (tecla == 8) {
            return true;
        }

        // Patron de entrada, en este caso solo acepta numeros y letras
        patron = /[A-Za-z0-9]/;
        tecla_final = String.fromCharCode(tecla);
        return patron.test(tecla_final);
    }

    function actualizar_index()
    {
        var filas = $(".index-recibos").length;
        for(var i = 1; i <= filas; i++)
        {
            $(".index-recibos").eq(i-1).text(i);
        }
    }
    
    $("#departamento").change(function(){
        $("#ciudad").empty();
        // Por defecto
        var html = '<option value="">Todas las ciudades</option>'+
                    '<option value="0">Contrato sin ciudades</option>';
        $("#ciudad").append(html);

        dep = $(this).val();
		if(dep == "")
			cant = 0;
		else
        cant = $ciudades[dep].length;
        for(var i = 0; i < cant; i++)
        {
            html = "<option value='" + $ciudades[dep][i][0] + "'>" + $ciudades[dep][i][1] + "</option>";
            $("#ciudad").append(html);
        }
        $('#ciudad').selectpicker('refresh');
    });



    $("#dep-manual").change(function(){
        $("#ciu-manual").empty();
        // Por defecto
        var html = '<option value="">Todas las ciudades</option>'+
                    '<option value="0">Contrato sin ciudades</option>';
        $("#ciu-manual").append(html);

        dep = $(this).val();
		if(dep == "")
			cant = 0;
		else
        cant = $ciudades[dep].length;
        for(var i = 0; i < cant; i++)
        {
            html = "<option value='" + $ciudades[dep][i][0] + "'>" + $ciudades[dep][i][1] + "</option>";
            $("#ciu-manual").append(html);
        }
        $('#ciu-manual').selectpicker('refresh');
    });

    $('#add-manual').on('hidden.bs.modal', function (event) {
        $("#contrato-manual").val("");    
    });
    $('#add-manual').on('shown.bs.modal', function (event) {
        $("#contrato-manual").focus();    
    });

    $("#form").submit(function(){
        carga();
        if(!$("#form").validationEngine('validate'))
            $('#carga').dialog('close');
    });

    $("#form-descargar").submit(function(e){
        var fecha = $("#fecha-cobrarse").val();
        var cobrador = $("#cobrador").val();
        var format_fecha = $("#fecha-cobrarse").validationEngine('validate');
        var fecha_format = $("#fecha-cobrarse").val();
        $("#fecha-cobrarse").validationEngine('validate');
        if(fecha != "" && cobrador != "" && !format_fecha)
        {
            $("#pdf-Fecha").val(fecha);
            $("#pdf-Asignado").val(cobrador);
        }
        else
            e.preventDefault();

        if(cobrador == "")
            alert("Debe especificar el cobrador");
    });
    var cargando = false;
    function pre_cambiar_cobrador()
    {
        if(cargando == false)
        {
            $("#btn-cobrador-cambio").prop('disabled', true);
            cargando = true;
            var cobrador_nuevo = $("#cobrador-reemplaza").val();
            var desde = $("#rango-desde").val();
            var hasta = $("#rango-hasta").val();
            $("#msj2").slideUp();
            $("#msjerror").slideUp();
            carga();
            if(cobrador_nuevo != "" && desde > 0 && hasta > 0 && (hasta > desde))
            {
                setTimeout(cambiar_cobrador, 1000);
            }
            else
            {
                $("#msjerror").slideDown();
                $("#btn-cobrador-cambio").prop('disabled', false);
                cargando = false;
                $("#carga").dialog('close');
            }
        }
    }
    function cambiar_cobrador()
    {
        console.log("entra");
        var cobrador_nuevo = $("#cobrador-reemplaza").val();
        var desde = $("#rango-desde").val();
        var hasta = $("#rango-hasta").val();
        for(var i = (desde-1); i < hasta; i++)
        {
            $('select.cobradores-edit').eq(i).val(cobrador_nuevo);
            $('select.cobradores-edit').eq(i).selectpicker('refresh');
        }
        $("#msj2").slideDown();
        $("#btn-cobrador-cambio").prop('disabled', false);
        
        cargando = false;
        $("#carga").dialog('close');
    }

    $("#guardar").click(function(){
        $("#guardar").prop('disabled', true);
        if($("#form2").validationEngine('validate'))
        {
            var total_fila = $('#tb-recibo tr').length + $('#tb-recibo-manual tr').length;
            if(total_fila > 0)
            {
                carga();
                //$("#mensaje").modal('show');
                $.ajax({
                    // la URL para la petición
                    url : 'ajax/guardar_recibos_cobrarse_v2.php',
                    data : $("#form2").serialize(),
                    type : 'POST',
                    
                    success : function(json) {
                        $('#tb-recibo').empty();
                        $('#tb-recibo-manual').empty();
                        make_alert({'message':'La lista se ha guardado exitosamente'});
                        
                        $("#carga").dialog('close');
                    },
                    
                    error : function(xhr, status) {
                        
                        $("#carga").dialog('close');
                        make_alert({'type':'danger', 'message':'Mientras se guardaban los registros ocurrio un error :('});
                    },
    
                    complete : function(xhr, status) {
                        //$("#mensaje").modal('hide');
                        $("#guardar").prop('disabled', false);
                        $("#carga").dialog('close');
                    }
                });
            }
            else
            {
                $("#carga").dialog('close');
                $("#guardar").prop('disabled', false);
            }
        }
        else
            $("#guardar").prop('disabled', false);

    });

    function make_alert({object = $('#msj'), type = 'success', message = ''})
    {
        object.slideUp();
        object.empty();

        var alerta = 
        '<div class="alert alert-'+ type +' alert-dismissible fade show" role="alert">'+
            message +
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
                '<span aria-hidden="true">&times;</span>'+
            '</button>'+
        '</div>';

        object.append(alerta);
        object.slideDown();
    }
    
    $("#form-cobrador").submit(function(e){
        var fecha = $("#fecha-cobrarse-reporte").val();
        var cobrador = $("#cobrador-reporte").val();
        var format_fecha = $("#fecha-cobrarse-reporte").validationEngine('validate');
        var v_cobrador = $("#cobrador-reporte").validationEngine('validate');
        
        if(fecha != "" && cobrador != "" && !format_fecha && !v_cobrador)
        {
            $("#pdf-co-fecha").val(fecha);
            $("#pdf-co-asignado").val(cobrador);
        }
        else
        {
            e.preventDefault();
        }
    });
    $("#form-dep").submit(function(e){
        var fecha = $("#fecha-cobrarse-reporte").val();
        var cobrador = $("#cobrador-reporte").val();
        var format_fecha = $("#fecha-cobrarse-reporte").validationEngine('validate');
        var v_cobrador = $("#cobrador-reporte").validationEngine('validate');
        var departamento = $("#departamento-reporte").val();
        $("#departamento-reporte").validationEngine('validate');
        
        if(fecha != "" && cobrador != "" && !format_fecha && departamento != "" && !v_cobrador)
        {
            $("#pdf-dep-fecha").val(fecha);
            $("#pdf-dep-asignado").val(cobrador);
            $("#pdf-dep-dep").val(departamento);
        }
        else
        {
            e.preventDefault();
        }

    });
    $("#form-ciu").submit(function(e){
        var fecha = $("#fecha-cobrarse-reporte").val();
        var cobrador = $("#cobrador-reporte").val();
        var format_fecha = $("#fecha-cobrarse-reporte").validationEngine('validate');
        var v_cobrador = $("#cobrador-reporte").validationEngine('validate');
        var departamento = $("#departamento-reporte").val();
        var ciudad = $("#ciudad-reporte").val();
        $("#departamento-reporte").validationEngine('validate');
        $("#ciudad-reporte").validationEngine('validate');
        if(fecha != "" && cobrador != "" && !format_fecha && departamento != "" && ciudad != "" && !v_cobrador)
        {
            $("#pdf-ciu-fecha").val(fecha);
            $("#pdf-ciu-asignado").val(cobrador);
            $("#pdf-ciu-dep").val(departamento);
            $("#pdf-ciu-ciu").val(ciudad);
        }
        else
        {
            e.preventDefault();
        }

    });
    
    $("#departamento-reporte").change(function(){
        $("#ciudad-reporte").empty();
        // Por defecto
        var html = '<option value="">Todas las ciudades</option>'+
                    '<option value="0">Contrato sin ciudades</option>';
        $("#ciudad-reporte").append(html);

        dep = $(this).val();
		if(dep == "")
			cant = 0;
		else
        cant = $ciudades[dep].length;
        for(var i = 0; i < cant; i++)
        {
            html = "<option value='" + $ciudades[dep][i][0] + "'>" + $ciudades[dep][i][1] + "</option>";
            $("#ciudad-reporte").append(html);
        }
        $('#ciudad-reporte').selectpicker('refresh');
    });
</script>

