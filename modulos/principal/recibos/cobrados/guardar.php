<?php
    $r = '../../../../';
    require($r . 'incluir/session.php');
    require($r . 'incluir/connection.php');
    require($r . 'incluir/funciones.php');
    require('funciones_contrato.php');


    $fecha = trim($_POST['fecha']);
    $contrato = strtoupper(trim($_POST['contrato']));
    $recibo = trim($_POST['recibo']);
    $monto = trim($_POST['monto']);
    $cobrador = trim($_POST['cobrador']);
    $comentario = trim(strtoupper(str_replace(["'","<",">"], "", $_POST['comentario'])));

    $result = new stdClass();

    $contratos_fail = "";

    // Aplicamos los pagos en caso de existir
    $movimiento_nuevo = true;

    // Obtenemos el ultimo recibo con numero negativo para poder asignar a todos aquellos pagos sin numero de recibo
    $recibo_automatico = $db->query("select nextval('sec_factura_negativa') as factura")->fetch(PDO::FETCH_ASSOC);
    if($recibo_automatico)
        $recibo_automatico = $recibo_automatico['factura'];
    else 
        $recibo_automatico = -1;


    $contrato_aux = strupperEsp($contrato);
    $data = $db->query("SELECT 
    solempresa, solfactura, solcliente, carcuota, clinombre, clinom2, cliape1, cliape2 
    FROM solicitudes
    INNER JOIN carteras ON carfactura = solfactura
    INNER JOIN clientes ON cliid = solcliente 
    WHERE solid = '$contrato_aux' ")->fetch(PDO::FETCH_ASSOC);
    if($data)
    {
        $empresa = $data['solempresa'];
        $factura = $data['solfactura'];
        $cedula = $data['solcliente'];
        $cuota = $data['carcuota'];
        // La siguiente cuota
        $detcuota = $db->query("SELECT dcacuota, dcavalor FROM detcarteras 
        WHERE dcafactura = $factura AND dcaestado = 'ACTIVA' 
        ORDER BY dcacuota ASC LIMIT 1")->fetch(PDO::FETCH_ASSOC);
        if(!$detcuota)
        {
            $detcuota = $db->query("SELECT dcacuota, dcavalor FROM detcarteras 
            WHERE dcafactura = $factura 
            ORDER BY dcacuota DESC LIMIT 1")->fetch(PDO::FETCH_ASSOC);
        }

        $num_cuota = $detcuota['dcacuota'];
        $dcavalor = $detcuota['dcavalor'];

        
        // Si num_cuota es mayor a 1 o dcavalor es diferente a 0 entonces significa que no es la primer cuota de este contrato
        if($num_cuota > 1 || $dcavalor != 0)
            $comision = false;
        else
            $comision = true;

        // completar
        $nombre = $data['clinombre'];
        if( $data['clinom2'] != "" )
            $nombre .= " " . $data['clinom2'];
        if( $data['cliape1'] != "" )
            $nombre .= " " . $data['cliape1'];
        if( $data['cliape2'] != "" )
            $nombre .= " " . $data['cliape2'];
            

        $numero_recibo = $recibo;
        if($numero_recibo == "")
        {
            $numero_recibo = $recibo_automatico;
        }
        $digitador = $_SESSION['id'];
        
        $db->query("INSERT INTO infoextra 
        (comentario, digitador)
        VALUES ('$comentario', '$digitador');") or die($db->errorInfo()[2]);
        
        $id_info = $db->lastInsertId();

        $codigo = pagar($db, $contrato_aux, $empresa, $factura, $cedula,
        $cuota, $num_cuota, $monto, 0, $fecha, 1, $cobrador, 
        $nombre, $numero_recibo, $id_info,
        $movimiento_nuevo, $comision, 0);

        $result->comentario = $comentario;
        $result->contrato = $contrato_aux;
        $result->monto = $monto;
        $result->fecha = $fecha;
        $result->cobrador = $cobrador;
        $result->recibo = $numero_recibo;
        $result->codigo = $codigo;
        echo json_encode($result);
        exit();
    }
    else
    {
        // No existe el contrato
        $codigo = 4;
        $result->codigo = $codigo;
        echo json_encode($result);
        exit();
    }


?>