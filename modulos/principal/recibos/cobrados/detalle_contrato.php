<?php
$r = '../../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

$contrato = strtoupper(trim($_GET['contrato']));

if($contrato == "")
{
    http_response_code(500);
    exit();
}

$consulta = "SELECT s.soltotal as total, solcuota as prima, c.cartotal AS neto, 
m.movdescuento as des, c.carsaldo as saldo, carestado as estado
FROM solicitudes as s
INNER JOIN movimientos as m on m.movdocumento = s.solfactura and m.movprefijo = 'FV'
INNER JOIN carteras as c ON c.carfactura = s.solfactura
WHERE solid = '$contrato'";

$data_contrato = $db->query($consulta);

if(!$data_contrato)
{
    http_response_code(500);
    exit();
}

$cartera = $data_contrato->fetch(PDO::FETCH_ASSOC);


echo json_encode($cartera);
exit();


?>