<?php
$r = '../../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');

?>

<!doctype html>
<html lang="es">

<head>
    <title>RECIBOS COBRADOS - </title>
    <link rel="shortcut icon" href="<?php echo $r?>incluir/img/icon-naciente.png">
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-not-form.php');
    ?>
    <style>
        #form input {
            text-transform: uppercase;
        }
        .divisor {
            margin-left: 5px;
            margin-right: 5px;
            margin-top: 15px;
            margin-bottom: 15px;
            padding-top: 10px;
            padding-bottom: 2px;
            background-color: #004593;
            color: #fff;
            border-radius: 8px;
        }
        .divisor h6 {
            font-weight: bold; 
        }
        tr td {
            position: relative;
        }
        .f-size-15 {
            font-size: 15px;
        }
        .nota-importante{
            border: #000 solid 2px;
            padding: 10px;
            border-radius: 18px;
            text-align: center;
            background: #00205f;
            color: white;
        }
        .div-fixed-start {
            position: fixed;
            z-index: 10;
            right: 10px;
            bottom: 10px;
        }
        .div-fixed-end {
            position: fixed;
            z-index: 10;
            right: 10px;
            bottom: 50px;
        }
        .excluidos {
            background: #ffe2fd;
        }
        .dataTables_scrollBody
        {
            max-height: 30em!important;
        }
        .dataTables_wrapper{
            overflow: hidden!important;
        }
        .buttons-excel  {
            color: #fff!important;
            background-color: #28a745!important;
            border-color: #28a745!important;
            font-size: 15px!important;
        }
        .buttons-pdf  {    
            color: #fff!important;
            background-color: #dc3545!important;
            border-color: #dc3545!important;
            font-size: 15px!important;
        }
    </style>
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
    <link rel="stylesheet" href="<?php echo $r.'incluir/librerias/fontawesome/css/all.css'?>" />


    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
    <script src="<?php echo $r.'incluir/librerias/fontawesome/js/all.js'?>"></script>

	<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>


    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.7.0/css/buttons.dataTables.min.css" />
    <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>

    
    <script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
    
    <script src="<?php echo $r.'incluir/kavv_js/kavvdt.js'?>" ></script>

	<script type="text/javascript">
        var $ciudades = [];
		$(document).ready(function() {
			$(".fecha").datepicker({
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
            });
			$('#form').validationEngine({
				onValidationComplete: function(form, status) {
					if (status) {
                        return true;
					}
				}
			});
            
            $("input").prop("autocomplete","off"); 

            $("#form").keypress(function(e) {
                if (e.which == 13) {
                    addlist();
                    return false;
                }
            });
            $("#form2").keypress(function(e) {
                if (e.which == 13) {
                    //buscar_recibos_cobrados();
                    return false;
                }
            });

            // Almacenar las ciudades
            ruta = '../ajax/ciudades.php';
            $.get(ruta, function(res){
                res = JSON.parse(res);
                res.forEach(function(data){

                    var dep = data.ciudepto;
                    var ciu = data.ciuid;
                    var nombre = data.ciunombre;
                    // Si es diferente a null significa que el arreglo en el indice x ya posee datos
                    if($ciudades[dep] != null)
                    {
                        $ciudades[dep].push([ciu, nombre]);
                    }
                    else
                    {
                        // Si es la primera vez se asigna el primer dato como un arreglo
                        $ciudades[dep] = [[ciu, nombre]];
                    }
                });
            });

            excelname = 'Recibos Cobrados';
            dt = createdt($("#tabla-recibos"),{col:2, buttons: bootstrapButtons, dom: bootstrapDesingSlimButtons});
		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="<?php echo $r ?>index.php">Principal</a>
				<div class="mapa_div"></div><a>Recibos</a>
				<div class="mapa_div"></div><a class="current">Cobrados</a>
			</article>
			<article id="contenido">
            <div class="row">
                <div class="col-md-12">
                    <form id="form" name="form" class="form-style" action="#" method="post">

                        <fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12">
                            <legend class="ui-widget ui-widget-header ui-corner-all">AGREGAR RECIBOS COBRADOS</legend>
                            <div id="msj"></div>
                            
                            <div class="row mb-3  justify-content-center">
                                <div class="col-md-5">
                                    <label for="">Cobrador</label>
                                    <select id="cobrador" name="cobrador" class="selectpicker validate[required]" data-live-search="true" title="Seleccione un cobrador" data-width="100%">
                                        <option value=""></option>
                                    <?php
                                        $query = $db->query("SELECT usuid, usunombre FROM usuarios WHERE usurelacionista = 1 ORDER BY usunombre");
                                        while($row = $query->fetch(PDO::FETCH_ASSOC))
                                        {
                                            $codigo = $row['usuid'];
                                            $nombre = "";
                                            if(TRIM($row['usunombre']) != "")
                                                $nombre .= "/" . $row['usunombre'];
                                            echo "<option value='". $row['usuid'] ."'>$codigo $nombre</option>";
                                        }
                                    ?>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label for="">Fecha cobrado</label>
                                    <input type="text" class="form-control not-w fecha validate[required, custom[date]]" id="fecha" name="fecha" value=""/> 
                                </div>
                            </div>
                            <div class="row mb-3  justify-content-center">
                                <div class="col-md-3">
                                    <label for="">Contrato</label>
                                    <input type="text" class="form-control not-w validate[required]" id="contrato" name="contrato"> 
                                </div>
                                <div class="col-md-3">
                                    <label for="">Recibo</label>
                                    <input type="text" class="form-control not-w validate[number]" id="recibo" name="recibo"> 
                                </div>
                                <div class="col-md-3">
                                    <label for="">Monto</label>
                                    <input type="text" class="form-control not-w validate[required, number]" id="monto" name="monto"> 
                                </div>
                            </div>
                            <div class="row mb-3  justify-content-center">
                                <div class="col-md-9">
                                    <label for="">Comentario</label>
                                    <textarea type="text" class="form-control not-w validate[maxSize[399]]" id="comentario" name="comentario" rows='1'> </textarea>
                                </div>
                            </div>
                            
                            <div class="row mb-4 d-flex justify-content-between">
                                <div class="col-md-3 text-center">
                                    <label for="" style="visibility:hidden;">Guardar</label>
                                    <button class="btn btn-primary btn-block" type="button" onclick="addlist();" id="guardar">GUARDAR PAGO</button>
                                </div>
                                <div class="col-md-4 text-center">
                                    <label for="" style="visibility:hidden;">Mostrar recibos cobrados</label>
                                    <button class="btn btn-info btn-block" type="button" onclick="recargar_pagos();" id="listar">MOSTRAR RECIBOS COBRADOS</button>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-md-12">
                                    <p>
                                        NOTA: El boton "<strong>Mostrar recibos cobrados</strong>" permite la visualización de todos los pagos que ya han sido guardados.<br>
                                        Estos pagos se pueden filtrar por <strong>cobrador, fecha cobro, contrato, recibo y la combinación de los mismos</strong>.
                                    </p>
                                </div>
                            </div>
                        </fieldset>
                        
                    </form>
                </div>
            </div>
            <div class="row my-2">
                <div class="col-md-12 d-flex justify-content-center">
                    <div class="col-md-4 text-center">
                        <label for=""><strong>TOTAL RECOLECTADO</strong></label>
                        <input type="text" readonly class="form-control" name="recolectado" id="recolectado" style="text-align: center; font-size: 20px!important;">
                    </div>
                    <div class="col-md-8 text-center" id="detalle-cartera" style="display:none">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <label class="font-weight-bold" for="">Estado: <span id="cartera-estado" class="text-danger"></span></label>                        
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2 text-center">
                                <label class="font-weight-bold" for="">Total: <br><span id="cartera-total" class="text-danger"></span></label>                        
                            </div>
                            <div class="col-md-2 text-center">
                                <label class="font-weight-bold" for="">Prima: <br><span id="cartera-prima" class="text-danger"></span></label>                        
                            </div>
                            <div class="col-md-2 text-center">
                                <label class="font-weight-bold" for="">Neto: <br><span id="cartera-neto" class="text-danger"></span></label>                        
                            </div>
                            <div class="col-md-3 text-center">
                                <label class="font-weight-bold" for="">Descuento: <br><span id="cartera-des" class="text-danger"></span></label>                        
                            </div>
                            <div class="col-md-2 text-center">
                                <label class="font-weight-bold" for="">Saldo: <br><span id="cartera-saldo" class="text-danger"></span></label>                        
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="msj2"></div>
            <div class="row" style="max-height: 35em;">
                <div class="col-md-12">
                    <div class="row">
                        <table id="tabla-recibos" class="table table-hover" style="min-width: 1500px; max-height: 20em; width:100%">
                            <thead>
                                <tr>
                                    <th style="width:5%;" data-orderable="false"></th>
                                    <th style="width:5%;" data-orderable="false"></th>
                                    <th style="width:5%; text-align:center;">#</th>
                                    <th style="width:13%;">Contrato</th>
                                    <th style="width:12%;">Recibo</th>
                                    <th style="width:10%;">Monto</th>
                                    <th style="width:10%;">F. Pago</th>
                                    <th style="width:15%;">Cobrador</th>
                                    <th style="width:10%;">Digitador</th>
                                    <th style="width:15%;">Comentario</th>
                                </tr>
                            </thead>
                            <tbody id="tb-recibo">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            </article>
        </article>
    </section>

    <!-- Modal -->
    <div class="modal fade" id="mensaje" data-backdrop="static" data-keyboard='false' tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <label for="">Este proceso puede demorar <strong>varios segundos</strong> <br>No recargue la página <br>Espere pacientemente</label>
                </div>
            </div>
        </div>
    </div>
    
	<?php require($r . 'incluir/src/loading.php'); ?>

    <div class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">EDITAR RECIBO</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <div id="msj3"></div>
                    <div class="row mb-3 justify-content-center">
                        <div class="col-md-5">
                            <label for="">*Cobrador</label>
                            <select id="cobrador-edit" class="selectpicker validate[required]" data-live-search="true" title="Seleccione un cobrador" data-width="100%">
                                <option value=""></option>
                            <?php
                                $query = $db->query("SELECT usuid, usunombre FROM usuarios WHERE usurelacionista = 1 ORDER BY usunombre");
                                while($row = $query->fetch(PDO::FETCH_ASSOC))
                                {
                                    $codigo = $row['usuid'];
                                    $nombre = "";
                                    if(TRIM($row['usunombre']) != "")
                                        $nombre .= "/" . $row['usunombre'];
                                    echo "<option value='". $row['usuid'] ."'>$codigo $nombre</option>";
                                }
                            ?>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label for="">*Fecha cobrado</label>
                            <input type="text" class="form-control not-w fecha validate[required, custom[date]]" id="fecha-edit"> 
                        </div>
                    </div>
                    <div class="row mb-3 justify-content-center">
                        <div class="col-md-3">
                            <label for="">*Contrato</label>
                            <input type="hidden"  id="contrato-original"> 
                            <input type="text" class="form-control not-w validate[required]" id="contrato-edit"> 
                        </div>
                        <div class="col-md-3">
                            <label for="">Recibo</label>
                            <input type="hidden"  id="recibo-original"> 
                            <input type="text" class="form-control not-w validate[number]" id="recibo-edit"> 
                        </div>
                        <div class="col-md-3">
                            <label for="">*Monto</label>
                            <input type="text" class="form-control not-w validate[required, number]" id="monto-edit"> 
                        </div>
                    </div>
                    <div class="row mb-3 justify-content-center">
                        <div class="col-md-9">
                            <label for="">Comentario</label>
                            <textarea type="text" class="form-control not-w validate[maxSize[399]]" id="comentario-edit" rows='1'> </textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">CANCELAR</button>
                    <button type="button" class="btn btn-warning" onclick="actualizar_pago();">EDITAR</button>
                </div>
            </div>
        </div>
    </div>

    <!-- MODAL DE ELIMINACIÓN -->
    <div class="modal fade" id="modal-remove" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">ELIMINAR RECIBO</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <div class="row text-center">
                        <label class="col-md-12">Estas seguro que desea eliminar el pago con # de recibo </label>
                        <label class="col-md-12"><strong id="texto-recibo"></strong></label>
                        <label class="col-md-12">Esta acción no es reversible</label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">CANCELAR</button>
                    <button type="button" class="btn btn-danger" onclick="eliminar_recibo()">ELIMINAR</button>
                </div>
            </div>
        </div>
    </div>
</body>
<script>
    var dt, dt2;
    var very_fast = false;

    function addlist()
    {
        var cobrador = $("#cobrador").val();
        var fecha = $("#fecha").val();
        var contrato = $("#contrato").val();
        var recibo = $("#recibo").val();
        var monto = $("#monto").val();
        var comentario = $("#comentario").val();
        if(recibo === 0)
        {
            make_alert({'type':'danger', 'message':'El número de recibo debe ser diferente a 0'});
            return 0;   
        }

        if($("#form").validationEngine('validate'))
        {
            var validacion_ajax = 'validacion_contrato_recibo.php?';
            var form_serialize = $("#form").serialize();
            validacion_ajax += form_serialize;
            
            $("#guardar").prop("disabled", true);
            $("#loading").css('display', 'block');
            $.get(validacion_ajax, function(res){
                var data = JSON.parse(res);
                if(data == 1)
                {
                    var url = "guardar.php";
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: $("#form").serialize(),
                        success:function(res){ 
                            res = JSON.parse(res);
                            if(res.codigo == 1)
                            {
                                make_alert({'message':'Pago guardado exitosamente'});
                                add_recibo(res.fecha, res.contrato, res.recibo, res.monto, res.cobrador, res.comentario);
                                //calcular();
                                $("#comentario").val("");
                                $("#contrato").val("");
                                $("#recibo").val("");
                                $("#monto").val("");
                                $("#contrato").focus();
                            }
                            else if (res.codigo == 2)
                                make_alert({'type':'danger', 'message':'No se guardo el pago, ocurrio un error al momento de guardar el nuevo pago'});
                            else if (res.codigo == 3)
                                make_alert({'type':'danger', 'message':'No se guardo el pago, este # de recibo ya existe'});
                            else if (res.codigo == 4)
                                make_alert({'type':'danger', 'message':'No se guardo el pago, no se encuentra el contrato digitado'});
                            else
                                make_alert({'type':'danger', 'message':'Ocurrio un error al momento de guardar el pago, intentelo nuevamente, si el error persiste notificar a Kevin Valverde 8244-9347'});
                            
                            $("#guardar").prop("disabled", false);
                            $("#loading").css('display', 'none');
                        }, fail: function(){
                            make_alert({'type':'danger', 'message':'No se guardo el pago, ocurrio un error mientras se guardaba, intentalo nuevamente'});
                            $("#guardar").prop("disabled", false);
                            $("#loading").css('display', 'none');
                        }
                    });
                }
                else if(data == 2)
                {
                    make_alert({'type':'danger', 'message':'NO se guardo el pago. El contrato no existe'});
                    $("#guardar").prop("disabled", false);
                    $("#loading").css('display', 'none');
                }
                else if(data == 3)
                {
                    make_alert({'type':'danger', 'message':'NO se guardo el pago. El número de recibo ya existe'});
                    $("#guardar").prop("disabled", false);
                    $("#loading").css('display', 'none');
                }
                else
                {
                    make_alert({'type':'danger', 'message':'Ocurrio un error con la validación de los datos, intentelo nuevamente, si el error persiste notificar a Kevin Valverde 8244-9347'});
                    $("#guardar").prop("disabled", false);
                    $("#loading").css('display', 'none');
                }
            });
            
        }
    }

    function recargar_pagos()
    {
        //dt.destroy();
        var fecha = $("#fecha").val();
        var cobrador = $("#cobrador").val();
        var contrato = $("#contrato").val();
        var recibo = $("#recibo").val();
        var ruta = "listar_pagos.php";
        if(fecha != "" || contrato != "" || recibo != "" )
        {
            var contrato_org = contrato;
            $("#loading").css('display', 'block');
            //dt.rows().remove();
            //dt.draw();
            dt.destroy();
            $("#tb-recibo").empty();
            recolectado = 0;
            index_recibos = 1;
            $.ajax({
                type: "GET",
                url: ruta,
                data: {fecha: fecha, cobrador: cobrador, contrato: contrato, recibo: recibo},
                success:function(res){
                    res = JSON.parse(res);
                    res.forEach(function(data){
                        cobrador_codigo = data.usuid;
                        cobrador_nombre = data.usunombre;
                        fecha = data.movfecha;
                        contrato = data.solid;
                        recibo = data.movnumero;
                        monto = data.movvalor;
                        cobrador = data.movcobrador;
                        comentario = data.comentario;
                        digitador = data.digitador;

                        add_recibo_v2(fecha,contrato, recibo, monto, cobrador, digitador, comentario)
                    });
                    dt = createdt($("#tabla-recibos"),{col:2, buttons: bootstrapButtons, dom: bootstrapDesingSlimButtons});
                    $("#loading").css('display', 'none');
                    var pos = $("#tabla-recibos").offset().top;
                    $('html, body').animate({scrollTop: pos}, 'slow');
                    $("#recolectado").val(recolectado.toFixed(2));
                    
                    //Consultamos el detalle de la cartera en caso de que se ingresara contrato
                    if(contrato_org != "")
                    {
                        detalle_cartera(contrato_org);
                        $("#detalle-cartera").css("display", "block");
                    }
                    else
                        $("#detalle-cartera").css("display", "none");
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                dt = createdt($("#tabla-recibos"),{col:2, buttons: bootstrapButtons, dom: bootstrapDesingSlimButtons});
                make_alert({'object': $("#msj2"), 'type':'danger', 'message':'Ocurrio un error, intentelo nuevamente, si el error persiste notificar a Kevin Valverde 8244-9347'});
                $("#loading").css('display', 'none');
                $("#recolectado").val(0);
            });

        }

    }

    
    function detalle_cartera($contrato)
    {
        ruta = "detalle_contrato.php?contrato="+$contrato;
        $.get(ruta, function(res){
            cartera = JSON.parse(res);
            if(cartera.code != 0)
            {
                $("#cartera-estado").text(cartera.estado);
                $("#cartera-total").text(cartera.total);
                $("#cartera-prima").text(cartera.prima);
                $("#cartera-neto").text(cartera.neto);
                $("#cartera-des").text(cartera.des);
                $("#cartera-saldo").text(cartera.saldo);
            }
        });
    }

    function make_alert({object = $('#msj'), type = 'success', message = ''})
    {
        object.slideUp();
        object.empty();

        var alerta = 
        '<div class="alert alert-'+ type +' alert-dismissible fade show" role="alert">'+
            message +
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
                '<span aria-hidden="true">&times;</span>'+
            '</button>'+
        '</div>';

        object.append(alerta);
        object.slideDown();
    }
    index_recibos = 1;
    recolectado = 0;
    function add_recibo(fecha,contrato, recibo, monto, cobrador, comentario = "", digitador="")
    {
        var newrow = dt.row.add(
        [
            '<button onclick="edit_recibo(this, '+recibo+', '+monto+')" class="btn btn-warning"><i class="fas fa-pencil-alt"></i></button>',
            '<button onclick="select_row(this, '+recibo+', \''+contrato+'\', '+monto+')"class="btn btn-danger"><i class="fas fa-trash"></i></button>',
            index_recibos,
            contrato,
            recibo,
            monto,
            fecha,
            cobrador,
            digitador,
            comentario
        ]).draw().node();
        recolectado += parseFloat(monto);
        $("#recolectado").val(recolectado.toFixed(2));
        index_recibos++;
    } 

    function add_recibo_v2(fecha,contrato, recibo, monto, cobrador, digitador, comentario = "")
    {
        var html = 
                    "<tr>"+
                        "<td>"+ '<button onclick="edit_recibo(this, '+recibo+', '+monto+')" class="btn btn-warning"><i class="fas fa-pencil-alt"></i></button>' +"</td>"+
                        "<td>"+ '<button onclick="select_row(this, '+recibo+', \''+contrato+'\', '+monto+')"class="btn btn-danger"><i class="fas fa-trash"></i></button>' +"</td>"+
                        "<td>"+ index_recibos +"</td>"+
                        "<td>"+ contrato +"</td>"+
                        "<td>"+ recibo +"</td>"+
                        "<td>"+ monto +"</td>"+
                        "<td>"+ fecha +"</td>"+
                        "<td>"+ cobrador +"</td>"+
                        "<td>"+ digitador +"</td>"+
                        "<td>"+ comentario +"</td>"+
                    "</tr>";
        $("#tb-recibo").append(html);
        
        recolectado += parseFloat(monto);
        index_recibos++;
    }


    function fixed_bug_modal()
    {
        if(very_fast)
        $("#mensaje").modal("hide");
    }

    $("#departamento").change(function(){
        $("#ciudad").empty();
        // Por defecto
        var html = '<option value=""></option>';
        $("#ciudad").append(html);

        dep = $(this).val();
		if(dep == "")
			cant = 0;
		else
        cant = $ciudades[dep].length;
        for(var i = 0; i < cant; i++)
        {
            html = "<option value='" + $ciudades[dep][i][0] + "'>" + $ciudades[dep][i][1] + "</option>";
            $("#ciudad").append(html);
        }
        $('#ciudad').selectpicker('refresh');
    });

    function limpiar_modal()
    {
        $("#contrato-edit").val("");
        $("#recibo-edit").val("");
        $("#monto-edit").val("");
        $("#fecha-edit").val("");
        $("#cobrador-edit").val("");
        $("#comentario-edit").val("");
        $("#cobrador-edit").selectpicker("refresh");
    }

    var row_edit, monto_edit;
    function edit_recibo(element, recibo, monto)
    {
        //Dejamos almacenada temporalmente la fila en la que clickeamos editar
        row_edit = $(element).parents('tr');
        monto_edit = monto;
        $("#loading").css('display', 'block');
        $("#modal-edit").modal("show");

        var ruta = "buscar_x_recibo.php";
        $.ajax({
            type: "GET",
            url: ruta,
            data: {recibo: recibo},
            success:function(res){
                res = JSON.parse(res);
                $("#contrato-original").val(res.solid);
                $("#contrato-edit").val(res.solid);
                $("#recibo-original").val(res.movnumero);
                $("#recibo-edit").val(res.movnumero);
                $("#monto-edit").val(res.movvalor);
                $("#fecha-edit").val(res.movfecha);
                $("#cobrador-edit").val(res.movcobrador);
                $("#comentario-edit").val(res.comentario);

                
                $("#contrato-original-edit").val(res.contrato);
                $("#recibo-original-edit").val(res.recibo);

                $("#cobrador-edit").selectpicker("refresh");
                
                $("#loading").css('display', 'none');
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            make_alert({'object': $("#msj2"), 'type':'danger', 'message':'Ocurrio un error, intentelo nuevamente, si el error persiste notificar a Kevin Valverde 8244-9347'});
            $("#modal-edit").modal("hide");
            $("#loading").css('display', 'none');
        });
    }
    
    function actualizar_pago()
    {
        $("#loading").css('display', 'block');
        $("#modal-edit").modal("show");

        var recibo_original = $("#recibo-original").val();
        var recibo_nuevo = $("#recibo-edit").val();
        var contrato_original = $("#contrato-original").val();
        var contrato_nuevo = $("#contrato-edit").val();
        var monto = $("#monto-edit").val();
        var cobrador = $("#cobrador-edit").val();
        var fecha = $("#fecha-edit").val();
        var comentario = $("#comentario-edit").val();

        if(contrato_nuevo == "" || monto == "" || cobrador == "" || fecha == "")
        {
            make_alert({'object': $("#msj3"), 'type':'danger', 'message':'Es obligatorio ingresar todos los datos que tienen "*" '});
            $("#loading").css('display', 'none');
            return 0;
        }

        var ruta = "editar_pago.php";
        $.ajax({
            type: "POST",
            url: ruta,
            data: {
                recibo_original: recibo_original,
                recibo_nuevo: recibo_nuevo,
                contrato_original: contrato_original,
                contrato_nuevo: contrato_nuevo,
                monto: monto,
                cobrador: cobrador,
                fecha: fecha,
                comentario: comentario
            },
            success:function(res){
                res = JSON.parse(res);
                if(res.codigo == 0)
                {
                    make_alert({'object': $("#msj2"), 'message':res.message});
                    var btnedit = '<button onclick="edit_recibo(this, '+recibo_nuevo+', '+monto+')" class="btn btn-warning"><i class="fas fa-pencil-alt"></i></button>';
                    var btndelete = '<button onclick="select_row(this, '+recibo+', \''+contrato_nuevo+'\', '+monto+')"class="btn btn-danger"><i class="fas fa-trash"></i></button>';
                    
                    dt.cell(row_edit.children('td')[0]).data( btnedit );
                    dt.cell(row_edit.children('td')[1]).data( btndelete );

                    dt.cell(row_edit.children('td')[3]).data(contrato_nuevo);
                    dt.cell(row_edit.children('td')[4]).data(recibo_nuevo);
                    dt.cell(row_edit.children('td')[5]).data(monto);
                    dt.cell(row_edit.children('td')[6]).data(fecha);
                    dt.cell(row_edit.children('td')[7]).data(cobrador);
                    dt.cell(row_edit.children('td')[8]).data(res.digitador);
                    dt.cell(row_edit.children('td')[9]).data(comentario);

                    var temp = $("#recolectado").val();
                    var temp = parseFloat(temp) - parseFloat(monto_edit) + parseFloat(monto);
                    $("#recolectado").val(temp.toFixed(2));
                    $("#modal-edit").modal("hide");
                }
                else if (res.codigo == 1)
                    make_alert({'object': $("#msj3"), 'type':'danger', 'message':res.message});
                else
                    make_alert({'object': $("#msj3"), 'type':'danger', 'message':'Ocurrio un error al momento de guardar el pago, intentelo nuevamente, si el error persiste notificar a Kevin Valverde 8244-9347'});
                    
                $("#loading").css('display', 'none');
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            make_alert({'object': $("#msj3"), 'type':'danger', 'message':'Ocurrio un error, intentelo nuevamente, si el error persiste notificar a Kevin Valverde 8244-9347'});
            $("#loading").css('display', 'none');
        });

    }

    var row_remove, recibo_remove, monto_remover;
    function select_row(element, recibo, contrato, monto)
    {
        row_remove = $(element).parents('tr');//Dejamos almacenada temporalmente la fila en la que clickeamos eliminar

        recibo_remove = recibo;
        monto_remover = monto;
        $("#texto-recibo").empty();
        $("#texto-recibo").text(recibo + " asignado al contrato " +contrato+ ' con un monto de '+ monto)
        $("#modal-remove").modal("show");
    }


    function eliminar_recibo()
    {
        var ruta = "eliminar_pago.php";
        $("#loading").css('display', 'block');
        $.ajax({
            type: "GET",
            url: ruta,
            data: {recibo: recibo_remove},
            success:function(res){
                res = JSON.parse(res);
                
                if(res.codigo == 0)
                {
                    make_alert({'object': $("#msj2"), 'message':res.message});
                    dt.row(row_remove).remove().draw( false );
                    var temp = $("#recolectado").val();
                    var temp = parseFloat(temp)-parseFloat(monto_remover);
                    $("#recolectado").val(temp.toFixed(2));
                }
                else if (res.codigo == 1)
                    make_alert({'object': $("#msj2"), 'type':'danger', 'message':res.message});
                else
                    make_alert({'object': $("#msj2"), 'type':'danger', 'message':'Ocurrio un error al momento de guardar el pago, intentelo nuevamente, si el error persiste notificar a Kevin Valverde 8244-9347'});
                
                $("#loading").css('display', 'none');
                $("#modal-remove").modal("hide");
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            make_alert({'object': $("#msj2"), 'type':'danger', 'message':'Ocurrio un error, intentelo nuevamente, si el error persiste notificar a Kevin Valverde 8244-9347'});
            $("#modal-remove").modal("hide");
            $("#loading").css('display', 'none');
        });
    }

    $('#modal-edit').on('hidden.bs.modal', function (e) {
        limpiar_modal();
    });
    $("#contrato").keypress(function(e) {
        if (e.which == 13) {
            recargar_pagos();
            return false;
        }
    });
</script>