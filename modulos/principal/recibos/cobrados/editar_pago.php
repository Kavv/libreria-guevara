<?php
$r = '../../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
require('funciones_contrato.php');

$recibo = trim($_POST['recibo_original']);
$new_recibo = trim($_POST['recibo_nuevo']);
$original_contrato = strtoupper(trim($_POST['contrato_original']));
$new_contrato = strtoupper(trim($_POST['contrato_nuevo']));

$new_valor = trim($_POST['monto']);
$new_cobrador = trim($_POST['cobrador']);
$new_fecha = trim($_POST['fecha']);
$new_comentario = strtoupper(trim($_POST['comentario']));

$new_factura = "";

$result = new stdClass();


if($new_recibo == "" || $new_recibo == 0 || $new_recibo =="null" || $new_recibo == null)
{
    $recibo_automatico = $db->query("select nextval('sec_factura_negativa') as factura")->fetch(PDO::FETCH_ASSOC);
    if($recibo_automatico)
        $new_recibo = $recibo_automatico['factura'];
    else 
        $new_recibo = -1;
}


if($new_contrato === "" || $new_contrato === 0 || $new_contrato === "null" || $new_contrato === null)
{
    $result->codigo = 1;
    $result->message = 'No se edito el pago. El # del contrato ingresado no es valido';
    $result->con = $new_contrato;
    echo json_encode($result);
    exit();
}

// EN caso de querer cambiar el # de contrato de un pago
if($original_contrato != $new_contrato)
{
    // Verificamos que exista el nuevo contrato
    $aux_contrato = $db->query("SELECT solfactura FROM solicitudes where solid = '$new_contrato'");

    if(!$aux_contrato)
    {
        $result->codigo = 1;
        $result->message = 'No se edito el pago. El nuevo contrato no existe';
        echo json_encode($result);
        exit();
    }
    $aux_contrato = $aux_contrato->fetch(PDO::FETCH_ASSOC);
    if(!$aux_contrato)
    {
        $result->codigo = 1;
        $result->message = 'No se edito el pago. El nuevo contrato no existe';
        echo json_encode($result);
        exit();
    }
    $new_factura = $aux_contrato['solfactura'];
}

// EN caso de querer cambiar el # de recibo de un pago
if($new_recibo != $recibo)
{
    // Verificamos que no exista el nuevo recibo
    $aux_recibo = $db->query("SELECT movnumero FROM movimientos 
    where movprefijo = 'RC' AND movnumero = '$new_recibo'");

    if($aux_recibo->rowCount() > 0)
    {
        $result->codigo = 1;
        $result->message = 'No se edito el pago. El nuevo # de recibo ya se encuentra en uso';
        echo json_encode($result);
        exit();
    }
}



$consulta = "SELECT solid, movnumero, movvalor, movfecha, movcobrador, movdocumento, movimientos.created_at as procesado, 
comentario, infoextra.digitador as digitador FROM movimientos 
INNER JOIN solicitudes on movdocumento = solfactura 
INNER JOIN infoextra on movinfo = infoextra.id 
WHERE movprefijo = 'RC' AND solid != 'PERDIDO' ";

if($recibo != "" && $recibo != 0 && $recibo !="null" && $recibo != null)
    $consulta .= " AND movnumero = '$recibo'";
else
{
    $result->codigo = 1;
    $result->message = 'No se edito el pago. El # de recibo no fue encontrado';
    echo json_encode($result);
    exit();
}

$data = $db->query($consulta);
if($data)
{
    $data = $data->fetch(PDO::FETCH_ASSOC);

    $contrato = $data['solid'];
    $factura = $data['movdocumento'];
    $monto = $data['movvalor'];
    $cobrador = $data['movcobrador'];
    $fecha = $data['movfecha'];
    $procesado = $data['procesado'];
    $exdigitador = $data['digitador'];
}
else
{
    $result->codigo = 1;
    $result->message = 'No se edito el pago. Ocurrio un error al momento de obtener los datos del pago';
    echo json_encode($result);
    exit();
}

// Con los datos obtenidos, procedemos a eliminar el registro movimiento
$seguro = $db->query("SELECT * FROM movimientos where movprefijo = 'RC' AND movnumero = '$recibo'");
if($seguro)
{
    $cantidad = $seguro->rowCount();
    if($cantidad == 1)
    {
        $aux_data = $seguro->fetch(PDO::FETCH_ASSOC);
        // Si es vacio, significa que es la misma factura/contrato
        if($new_factura == "")
            $new_factura = $aux_data['movdocumento'];

        $status = $db->query("UPDATE movimientos SET 
        movnumero = '$new_recibo', movdocumento = '$new_factura', movvalor = '$new_valor', 
        movcobrador = '$new_cobrador', movfecha = '$new_fecha' where movprefijo = 'RC' and movnumero = '$recibo';");
        
        if(!$status)
        {
            $result->codigo = 1;
            $result->message = 'El pago no fue editado, Ocurrio un error durante el proceso de edición';
            echo json_encode($result);
            exit();
        }

        $digitador = $_SESSION['id'];
        $recibo_info = $aux_data['movinfo'];
        $status = $db->query("UPDATE infoextra SET comentario = '$new_comentario', digitador = '$digitador' WHERE id = '$recibo_info'"); 

        if(!$status)
        {
            $result->codigo = 1;
            $result->message = 'El pago se edito pero ocurrio un error durante el proceso de edición';
            echo json_encode($result);
            exit();
        }

        $direccion_remota = $_SERVER['REMOTE_ADDR'];
        $mensaje = "edito el recibo # $recibo con un monto previo de $monto asignado previamente al contrato $original_contrato, previo cobrado por $cobrador el $fecha, previo digitado el $procesado por el usuario $exdigitador ";

        $db->query("INSERT INTO logs (id_usuario , ip_usuario , accion_usuario, fecha_accion) VALUES 
            ('$digitador', '$direccion_remota', '$mensaje' , NOW() );") or die($db->errorInfo()[2]);
            
        // Sincoronizamos el contrato al que se ha asignado el nuevo pago
        $status = procesarar_contrato($new_contrato);

        if(!$status)
        {
            $result->codigo = 1;
            $result->message = 'El pago fue editador, pero ocurrio un error durante la sincronización de la cartera, notificar a Kevin Valverde 8244-9347';
            echo json_encode($result);
            exit();
        }

        // Si es un contrato diferente sincronizamos el contrato que antes tenia asignado el pago
        if($original_contrato != $new_contrato)
            $status = procesarar_contrato($original_contrato);
        
        if($status)
        {
            $result->codigo = 0;
            $result->message = '¡El pago se edito correctamente!';
            $result->digitador = $digitador;
            echo json_encode($result);
            exit();
        }
        else
        {
            $result->codigo = 1;
            $result->message = 'Se edito el pago pero ocurrio un error durante la edición (SC), notificar a Kevin Valverde 8244-9347';
            echo json_encode($result);
            exit();
        }

    }
    else
    {
        $result->codigo = 1;
        $result->message = 'No se edito el pago. Ocurrio un error se encontraron dos registros con el mismo # de recibo, notificar a Kevin Valverde 8244-9347';
        echo json_encode($result);
        exit();
    }
}
else
{
    $result->codigo = 1;
    $result->message = 'No se edito el pago. Ocurrio un error, intente nuevamente, si el error persiste solicite asistencia';
    echo json_encode($result);
    exit();
}


// Recalculamos el contrato en base a los movimientos existentes




        

function procesarar_contrato($id)
{
    global $db;
    $data = $db->query("SELECT cartotal, f.movdescuento as descuento, 
    ROUND( cast(carcuota as numeric), 2)  as cuota, 
    solicitudes.*, upper(concat(cliNombre, ' ', cliNom2, ' ', cliApe1, ' ', cliApe2)) as cliente
    from solicitudes
    left join clientes on cliid = solcliente
    inner join carteras on carfactura = solfactura
    INNER JOIN movimientos as f ON (f.movprefijo = 'FV' AND f.movdocumento = solfactura)
    where solid = '$id' and solid != 'PERDIDO'");

    if(!$data)
        return false;

    $row_contrato = $data->fetch(PDO::FETCH_ASSOC);

    $cantidad = 0;
    // Cliente
    $cedula = $row_contrato['solcliente'];// strupperEsp(str_replace("'", "", $_POST['cedula']));
    $nombre = $row_contrato['cliente'];//strupperEsp(str_replace("'", "", $_POST['nombre']));

    // Solicitud
    $empresa = "16027020274133";
    $contrato = $row_contrato['solid'];//strupperEsp(str_replace("'", "", $_POST['contrato']));
    $fecha_contrato = $row_contrato['solfecha'];//str_replace("'", "", $_POST['fecha-contrato']);
    
    $asesor = $row_contrato['solasesor'];
    $entregador = $row_contrato['solenvio'];
    $cobrador = $row_contrato['solrelacionista'];
    // Por defecto

    // Datos de la cartera a agenerarse
    $contrato_original = $row_contrato['solid'];//$_POST['contrato-original'];
    $fecha_cobro = $row_contrato['solcompromiso'];//$_POST['fecha-cobro'];

    // Generar el neto de otra forma, no es fidedigno
    //$neto = $row_contrato['cartotal'];//floatval($_POST['neto']);
    
    //CORRECTO
    $neto = $row_contrato['soltotal'] - $row_contrato['solcuota'];
    
    $descuento = $row_contrato['descuento'];//floatval($_POST['descuento']);
    $ncuotas = $row_contrato['solncuota'];//$_POST['ncuotas'];
    $cuota = $row_contrato['cuota'];//floatval($_POST['cuota']);
    $saldo = round($neto - $descuento, 2);
    
    // aqui
    $factura = $row_contrato['solfactura'];

    // CARTERA 
    // ACTUALIZAMOS AL SALDO ORIGINAL (Punto de arranque en el saldo de la cartera)
    $status = $db->query("UPDATE carteras SET
    carsaldo = '$saldo'
    WHERE carempresa = '$empresa' AND carfactura = '$factura'");

    if(!$status)
        return false;


    // DETCARTERAS
    $fechacom = $fecha_cobro;
    // Reestablecemos el detalle de la cartera existente
    $status = $db->query("UPDATE detcarteras SET dcavalor = 0, dcadescuento = 0, dcasaldo = 0, dcafepag = null, 
    dcapromotor = null, dcaestado = 'ACTIVA' 
    WHERE dcaempresa = '$empresa' AND dcafactura = '$factura'");

    if(!$status)
        return false;

    // RECIBOS
    $movimiento_nuevo = false;
    $comision = false;
    $aux_saldo = $saldo;

    $status = $recibos = $db->query("SELECT * FROM movimientos where movprefijo = 'RC' AND movdocumento = '$factura' ");
    
    if(!$status)
        return false;

    while ($row_recibo = $recibos->fetch(PDO::FETCH_ASSOC)) {
        
            $movcobrador = trim(strtoupper($row_recibo['movcobrador']));
            if($movcobrador == '')
                $movcobrador = 'PERDIDO';
                
            // Numero del recibo
            $num_recibo = $row_recibo['movnumero'];
            
            $original_aux = $row_recibo['movnumero'];
            //$new_recibo = $detalle_recibo[$i];
            $movvalor = $row_recibo['movvalor'];
            // En base al (neto - descuento) lo cual es equivalende al primer saldo del contrato (sin incluir algun pago)
            // Esto definira el nuevo saldo en cada movimiento ya existente, ya que el monto puede ser modificado
            $aux_saldo = round($aux_saldo - $movvalor, 2);
            $fecha_de_pago = $row_recibo['movfecha'];
            $status = $db->query("UPDATE movimientos SET
            movsaldo = '$aux_saldo'
            WHERE movprefijo='RC' AND movempresa='$empresa' AND movnumero='$original_aux'");
            
            if(!$status)
                return false;
            
            $infoextra = $row_recibo['movinfo'];
            
            // Obtenemos La siguiente cuota
            $detcuota = $db->query("SELECT dcacuota, dcavalor FROM detcarteras 
            WHERE dcafactura = $factura AND dcaestado = 'ACTIVA' 
            ORDER BY dcacuota ASC LIMIT 1");
            if(!$detcuota)
                return false;
            
            $detcuota = $detcuota->fetch(PDO::FETCH_ASSOC);
            if(!$detcuota)
            {
                $detcuota = $db->query("SELECT dcacuota, dcavalor FROM detcarteras 
                WHERE dcafactura = $factura 
                ORDER BY dcacuota DESC LIMIT 1");
                
                if(!$detcuota)
                    return false;

                $detcuota = $detcuota->fetch(PDO::FETCH_ASSOC);
            }

            $num_cuota = $detcuota['dcacuota'];

            // Trabajamos el detalle de la cartera pero sin crear un movimiento nuevo
            $status = pagar($db, $contrato, $empresa, $factura, $cedula,
            $cuota, $num_cuota, $movvalor, 0, $fecha_de_pago, 1, $movcobrador, 
            $nombre, $num_recibo, $infoextra,
            $movimiento_nuevo, $comision, 2);

            if($status != 1)
                return false;
    }

    return true;


}