<?php
$r = '../../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');


$recibo = trim($_GET['recibo']);

$consulta = "SELECT solid, movnumero, movvalor, movfecha, movcobrador, comentario, infoextra.digitador as digitador FROM movimientos 
INNER JOIN solicitudes on movdocumento = solfactura 
INNER JOIN infoextra on movinfo = infoextra.id";

$consulta .= " WHERE movprefijo = 'RC' AND solid != 'PERDIDO' ";

if($recibo != "")
    $consulta .= " AND movnumero = '$recibo'";
else
{
    http_response_code(500);
    exit();
}

$data_recibo = $db->query($consulta);

if(!$data_recibo)
{
    http_response_code(500);
    exit();
}

$recibo = $data_recibo->fetch(PDO::FETCH_ASSOC);

echo json_encode($recibo);
exit();


?>