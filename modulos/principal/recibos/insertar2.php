<?php
$r = '../../../';
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
require($r . 'incluir/funciones.php');
require('funciones_contrato.php');


if(isset($_POST['guardar-pagos']))
{
    $contratos = [];
    $recibos = [];
    $montos = [];
    $fecha_pagos = [];
    $cobradores = [];
    $contratos_fail = "";
    if(isset($_POST['detalle-contrato']))
        $contratos = $_POST['detalle-contrato'];
    if(isset($_POST['detalle-recibo']))
        $recibos = $_POST['detalle-recibo'];
    if(isset($_POST['detalle-monto']))
        $montos = $_POST['detalle-monto'];
    if(isset($_POST['detalle-fpago']))
        $fecha_pagos = $_POST['detalle-fpago'];
    if(isset($_POST['detalle-codigo']))
        $cobradores = $_POST['detalle-codigo'];
    
    // Aplicamos los pagos en caso de existir
    $cantidad_pagos = count($contratos);
    $movimiento_nuevo = true;
    
    // Obtenemos el ultimo recibo con numero negativo para poder asignar a todos aquellos pagos sin numero de recibo
    $recibo_automatico = $db->query("SELECT movnumero FROM movimientos WHERE movprefijo = 'RC' AND movnumero < 0 ORDER BY movnumero ASC LIMIT 1")->fetch(PDO::FETCH_ASSOC);
    if($recibo_automatico)
        $recibo_automatico = $recibo_automatico['movnumero'] - 1;
    else 
        $recibo_automatico = -1;
    
    
    if($cantidad_pagos == 0)
    {
        $error = "NO DIGITO NINGUN PAGO";
        header("Location:insertar.php?error=$error");
        exit();
    }
    
    for($i = 0; $i < $cantidad_pagos; $i++)
    {
        $contrato_aux = strupperEsp($contratos[$i]);
        $data = $db->query("SELECT 
        solempresa, solfactura, solcliente, carcuota, clinombre, clinom2, cliape1, cliape2 
        FROM solicitudes
        INNER JOIN carteras ON carfactura = solfactura
        INNER JOIN clientes ON cliid = solcliente 
        WHERE solid = '$contrato_aux' ")->fetch(PDO::FETCH_ASSOC);
        if($data)
        {
            $empresa = $data['solempresa'];
            $factura = $data['solfactura'];
            $cedula = $data['solcliente'];
            $cuota = $data['carcuota'];
            // La siguiente cuota
            $detcuota = $db->query("SELECT dcacuota, dcavalor FROM detcarteras 
            WHERE dcafactura = $factura AND dcaestado = 'ACTIVA' 
            ORDER BY dcacuota ASC LIMIT 1")->fetch(PDO::FETCH_ASSOC);
            if(!$detcuota)
            {
                $detcuota = $db->query("SELECT dcacuota, dcavalor FROM detcarteras 
                WHERE dcafactura = $factura 
                ORDER BY dcacuota DESC LIMIT 1")->fetch(PDO::FETCH_ASSOC);
            }
    
            $num_cuota = $detcuota['dcacuota'];
            $dcavalor = $detcuota['dcavalor'];
    
            
            // Si num_cuota es mayor a 1 o dcavalor es diferente a 0 entonces significa que no es la primer cuota de este contrato
            if($num_cuota > 1 || $dcavalor != 0)
                $comision = false;
            else
                $comision = true;
    
            // completar
            $nombre = $data['clinombre'];
            if( $data['clinom2'] != "" )
                $nombre .= " " . $data['clinom2'];
            if( $data['cliape1'] != "" )
                $nombre .= " " . $data['cliape1'];
            if( $data['cliape2'] != "" )
                $nombre .= " " . $data['cliape2'];
                
    
            $numero_recibo = $recibos[$i];
            if($numero_recibo == "")
            {
                $numero_recibo = $recibo_automatico;
                $recibo_automatico--;
            }
            $digitador = $_SESSION['id'];
            $comentario = "";
            $db->query("INSERT INTO infoextra 
            (comentario, digitador)
            VALUES ('$comentario', '$digitador');") or die($db->errorInfo()[2]);
            
            $id_info = $db->lastInsertId();
    
            pagar($db, $contrato_aux, $empresa, $factura, $cedula,
            $cuota, $num_cuota, $montos[$i], 0, $fecha_pagos[$i], 1, $cobradores[$i], 
            $nombre, $numero_recibo, $id_info,
            $movimiento_nuevo, $comision, 0);
            
        }
        else
        {
            $contratos_fail = " " . $contrato_aux . ",";
        }
    }
    
    if($contratos_fail != "")
    {
        $error = "¡ERROR! El contrato $contrato_aux no fue encontrado y sus pagos no se han registrado";
        header("Location:insertar.php?error=$error");
        exit();
    }
    else
    {
        $mensaje = "¡Guardo Exitoso!";
        header("Location:insertar.php?mensaje=$mensaje");
        exit();
    }
}


?>

<!doctype html>
<html lang="es">

<head>
	<?php
	require($r . 'incluir/src/head.php');
	require($r . 'incluir/src/head-not-form.php');
    ?>
    <style>
        #form input {
            text-transform: uppercase;
        }
        .divisor {
            margin-left: 5px;
            margin-right: 5px;
            margin-top: 15px;
            margin-bottom: 15px;
            padding-top: 10px;
            padding-bottom: 2px;
            background-color: #004593;
            color: #fff;
            border-radius: 8px;
        }
        .divisor h6 {
            font-weight: bold; 
        }
        tr td {
            position: relative;
        }
    </style>
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

	<script type="text/javascript" src="<?php echo $r ?>incluir/jquery/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$(".fecha").datepicker({
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true,
            });
			$('#form').validationEngine({
				onValidationComplete: function(form, status) {
					if (status) {
                        carga();
                        return true;
					}
                    else
                        $("#carga").dialog("close");
				}
			});

            $("input").prop("autocomplete","off");
		});
	</script>
</head>

<body>
	<?php require($r . 'incluir/src/login.php') ?>
	<section id="principal">
		<?php require($r . 'incluir/src/cabeza.php') ?>
		<?php require($r . 'incluir/src/menu.php') ?>
		<article id="cuerpo">
			<article class="mapa">
				<a href="<?php echo $r ?>index.php">Principal</a>
				<div class="mapa_div"></div><a>Contrato</a>
				<div class="mapa_div"></div><a class="current">Agregar Recibos Cobrados</a>
			</article>
			<article id="contenido">
				<form id="form" name="form" action="insertar.php" method="post">
					<fieldset class="ui-widget ui-widget-content ui-corner-all col-md-12">
						<legend class="ui-widget ui-widget-header ui-corner-all">AGREGAR RECIBOS COBRADOS</legend>
                        
                        <div class="row mb-3">
                            <div class="col-md-5">
                                <label for="">Cobrador</label>
                                <select id="cobrador" name="cobrador" class="selectpicker" data-live-search="true" title="Seleccione un cobrador" data-width="100%">
                                <?php
                                    $query = $db->query("SELECT usuid, usunombre FROM usuarios WHERE usurelacionista = 1 ORDER BY usunombre");
                                    while($row = $query->fetch(PDO::FETCH_ASSOC))
                                    {
                                        $codigo = $row['usuid'];
                                        $nombre = "";
                                        if(TRIM($row['usunombre']) != "")
                                            $nombre .= "/" . $row['usunombre'];
                                        echo "<option value='". $row['usuid'] ."'>$codigo $nombre</option>";
                                    }
                                ?>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label for="">Fecha cobrado</label>
							    <input type="text" class="form-control not-w fecha validate[required]" id="fecha-cobrado" name="fecha-cobrado" value="<?php echo date("Y-m-d");?>"/> 
                            </div>
                            <div class="col-md-2">
                                <label for="">Cantidad a digitar</label>
							    <input type="text" class="form-control not-w validate[custom[integer], min[1]]" id="cantidad-registros" name="cantidad-registros" value="1" /> 
                            </div>
                            <div class="col-md-2 text-center">
                                <label for="" style="visibility:hidden;">Aplicar</label>
                                <button class="btn btn-info" type="button" onclick="generar_lista();">Aplicar</button>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-4">
                                <label for="">Total Recolectado</label>
                                <input type="text" readonly class="form-control" name="recolectado" id="recolectado">
                            </div>
                        </div>
                        
                        <div class="row" style="overflow-x: auto;">
                            <table id="tabla-recibos" class="table table-hover" style="min-width: 700px;">
                                <thead>
                                    <tr>
                                        <th style="width:5%; text-align:center;">#</th>
                                        <th style="width:15%;"><spam class="text-danger">*</spam>Contrato</th>
                                        <th style="width:15%;"><spam class="text-danger">*</spam>Recibo</th>
                                        <th style="width:20%;"><spam class="text-danger">*</spam>Monto</th>
                                        <th style="width:20%;"><spam class="text-danger">*</spam>F. Pago</th>
                                        <th style="width:20%;"><spam class="text-danger">*</spam>Cobrador</th>
                                        <th style="width:5%;"></th>
                                    </tr>
                                </thead>
                                <tbody id="tb-recibo">
                                </tbody>
                            </table>
                        </div>
                    </fieldset>


                    <div class="row d-flex justify-content-center">
                        <div class="col-md-6">
                            <input type="hidden" name="guardar-pagos">
                            <button id="guardar" type="submit" class="btn btn-primary btn-block" onclick="carga();">GUARDAR</button>
                        </div>
                    </div>
                </form>
            </article>
        </article>
    </section>
    
	<?php
	if (isset($_GET['error'])) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['error'] . '</div>';
	elseif (isset($_GET['mensaje'])) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $_GET['mensaje'] . '</div>';
	?>
    
</body>
<script>
    var prueba;
    var index_recibos = 1;
    function generar_lista()
    {
        var cobrador = $("#cobrador option:selected");
        var codigo = cobrador.val();
        var fecha = $("#fecha-cobrado").val();
        var cantidad_registros = $("#cantidad-registros").val();
        if(codigo != "" && fecha !="" && cantidad_registros > 0)
        {
            var nombre = cobrador.text(); 
            //$("#cobrador-aux").text(nombre);
            for(var i = 0; i < cantidad_registros; i++)
            {
                add_recibo(fecha, codigo, nombre);
            }
        } 
    }

    function add_recibo(fecha, codigo, cobrador)
    {
        html = "<tr class='recibo-item'>" +
            "<td class='text-center'><span class='index-recibos'>"+ index_recibos +"</span></td>" +
            "<td><input type='text' class='contrato-recibo form-control validate[ajax[ajaxContratoExist], required]' name='detalle-contrato[]' autocomplete='off'></td>" +
            "<td><input type='text' class='num_recibo form-control validate[ajax[ajaxRecibo], custom[integer]]' name='detalle-recibo[]' onkeypress='return check(event)' autocomplete='off'></td>" +
            "<td><input type='text' class='monto-recibo form-control validate[required, custom[number], min[0]]' name='detalle-monto[]' autocomplete='off' onkeyup='calcular(this);' onchange='calcular(this);'></td>" +
            "<td><input type='text' class='detalle-fpago form-control fecha validate[required]' name='detalle-fpago[]' autocomplete='off' value='"+ fecha +"'></td>" +
            "<td>"+
                "<input type='hidden' class='cobrador-recibo form-control validate[required]' name='detalle-codigo[]' readonly value='"+ codigo +"'>" +
                "<input type='text' class='cobrador-recibo form-control validate[required]' readonly value='"+ cobrador +"'>" +
            "</td>" +
            "<td><button type='button' class='btn btn-danger' onclick='remove_recibo(this);'>-</button></td>" +
        "</tr>";

        $("#tb-recibo").append(html);
        index_recibos++;
        input_fecha();
    } 

    function remove_recibo(element)
    {
        var fila = $(element);
        prueba = fila;
        fila.parents(".recibo-item").remove();
        index_recibos--;
        actualizar_index();
    }

    function input_fecha()
    {
        $(".fecha").datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true
        });
    }



    function check(e) {
        tecla = (document.all) ? e.keyCode : e.which;

        //Tecla de retroceso para borrar, siempre la permite
        if (tecla == 8) {
            return true;
        }

        // Patron de entrada, en este caso solo acepta numeros y letras
        patron = /[A-Za-z0-9]/;
        tecla_final = String.fromCharCode(tecla);
        return patron.test(tecla_final);
    }

    function actualizar_index()
    {
        var filas = $(".index-recibos").length;
        for(var i = 1; i <= filas; i++)
        {
            $(".index-recibos").eq(i-1).text(i);
        }
    }

    var incompleto = false;
    var numero_recibos = [];
    function verificar_inputs()
    {
        var total = 0;
        var valor = 0;
        var cant = $(".num_recibo").length;
        
        incompleto = false;
        numero_recibos = [];
        for (i = 0; i < cant; i++)
        {
            valor = parseFloat($(".monto-recibo").eq(i).val());
            var num_recibo = $(".num_recibo").eq(i).val();
            var fpago = $(".detalle-fpago").eq(i).val();
            var valid_contrato = $(".contrato-recibo").eq(i).validationEngine('validate');

            // Llenamos un arreglo con los # de recibos para verificar si hay alguno repetido
            numero_recibos.push(num_recibo);

            if( (valor >= 0) && (fpago != "") && (valid_contrato))
                total += valor;
            else
            {
                incompleto = true;
                
                $(".contrato-recibo").eq(i).validationEngine('validate');
                $(".num_recibo").eq(i).validationEngine('validate');
                $(".detalle-fpago").eq(i).validationEngine('validate');
                $(".monto-recibo").eq(i).validationEngine('validate');
            }
        }
        $("#recolectado").val(total);
    }

    function recibo_repetido()
    {
        var duplicate = false;
        var cantidad = numero_recibos.length;
        $(".num_recibo").css("color", "black");
        for(var i = 0; i < cantidad - 1; i++)
        {
            for(var j = i + 1; j < cantidad; j++ )
            {
                if(numero_recibos[i] == numero_recibos[j])
                {
                    if(numero_recibos[i] != "")
                    {
                        duplicate = true;
                        $(".num_recibo").eq(i).css("color", "red");
                        $(".num_recibo").eq(j).css("color", "red");
                        $(".num_recibo").eq(i).focus();
                        break;
                    }
                }
            }
            if(duplicate == true)
                break;
        }
        return duplicate;
    }

    $("#guardar").click(function(e){
        verificar_inputs();
        if(!incompleto)
        {
            if(recibo_repetido())
            {
                alert("Error, hay núrmeros de recibos repetidos.")
            }
        }
    });

    function actualizar()
    {
        calcular();
    }

    function calcular(element)
    {
        /* var recolectado = $("#recolectado").val()*1;
        var actual = $(element).val()*1;

        if(recolectado == "")
            recolectado = 0;
            
        console.log(actual);
        if(Number.isInteger(actual))
        {
            var total = recolectado + actual;
            $("#recolectado").val(total);
            console.log("escribe "+ recolectado + actual)
        } */
        var filas = $(".monto-recibo").length;
        var total = 0;
        var monto = 0;
        for(var i = 0; i < filas; i++)
        {
            monto = $(".monto-recibo").eq(i).val() * 1;
            total += monto;
        }
        $("#recolectado").val(total);
    }

</script>
