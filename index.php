<?php
$r = '';

session_start();

if (isset($_SESSION['id'])) {
    header('Location:' . $r . 'index2.php');
    exit();
}

require($r . 'incluir/connection.php');

if (isset($_POST['insertar'])) {

    $id = strtoupper(trim($_POST['id']));

    $qry = $db->prepare("SELECT * FROM usuarios WHERE usuid = :id");

    $qry->bindParam(':id', $id);
    $qry->execute();

    $num = $qry->fetch(PDO::FETCH_ASSOC);

    if ($num) {

        $pass = sha1($_POST['password']);
        $qry = $db->prepare("SELECT * FROM usuarios WHERE usuid = :id AND usupassword = :password");

        $qry->bindParam(':id', $id);
        $qry->bindParam(':password', $pass);
        $qry->execute();


        $row = $qry->fetch(PDO::FETCH_ASSOC);

        if ($row) {
            $_SESSION['id'] = $row['usuid'];
            $_SESSION['perfil'] = $row['usuperfil'];
            header('Location:index2.php');

            //Log de informacion
            $qrylogsregister = $db->prepare("SELECT * FROM usuarios INNER JOIN perfiles ON usuperfil = perid WHERE usuid = :sessionId"); //verificacion usuario por ID de sesion
            $qrylogsregister->bindParam(":sessionId", $_SESSION['id']);

            $qrylogsregister->execute();

            $rowlogsregister = $qrylogsregister->fetch(PDO::FETCH_ASSOC);
            $qrylogsregister = $db->prepare("INSERT INTO logs ( usuario, id_usuario , ip_usuario , accion_usuario, fecha_accion) VALUES (:usuario,:idusuario,:ipusuario,:accion ,now());"); //anexo de informacion de accion a la BD tabla LOGS

            $accionVar = 'INGRESO A SEPIRO';
            $qrylogsregister->bindParam(':usuario', $rowlogsregister['usunombre']);
            $qrylogsregister->bindParam(':idusuario', $_SESSION['id']);
            $qrylogsregister->bindParam(':ipusuario', $_SERVER['REMOTE_ADDR']);
            $qrylogsregister->bindParam(':accion', $accionVar);

            $qrylogsregister->execute();


            shell_exec('chmod 777 incluir/phpjasper/bin/jasperstarter/bin/jasperstarter');
            shell_exec('chmod 777 incluir/phpjasper/bin/jasperstarter/bin/jasperstarter.exe');
            shell_exec('chmod 777 incluir/phpjasper/src/PHPJasper.php');
            shell_exec('chmod -R 777 incluir/phpexcel/*');
            shell_exec('chmod 777 modulos/consultas/excelhelper.php');



        } else {
            $error = 'La contraseña no coincide con el del usuario';
        }
    } else {
        $error = 'Usuario no existe';
    }
}
?>
<!doctype html>
<html>

<head>
    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>
    <script>
        function carga() {
            $('#carga').dialog('open');
        }

        $(document).ready(function () {
            $('#carga').dialog({
                autoOpen: false,
                modal: true,
                draggable: false,
                resizable: false
            }).parent('.ui-dialog').find('.ui-dialog-titlebar-close').remove();
        });
    </script>
    <style type="text/css">
        html {
            background-image: url("incluir/img/Login.jpg");
            background-size: cover;
            background-repeat: no-repeat;
            height: 100%;
        }

        body {
            background: white;
        }

        #mydiv {
            position: absolute;
            top: 50%;
            margin-top: -100px;
            left: 50%;
            width: 350px;
            margin-left: -200px;
            padding: 10px;
        }

        #form fieldset {
            padding: 10px;
            display: block;
            width: 300px;
            margin: 5px auto
        }

        #form legend {
            font-weight: bold;
            margin-left: 5px;
            padding: 5px;
            font-size: 1.5em
        }

        #form p {
            margin: 5px 0
        }
    </style>
</head>

<body>
<div id="carga" title="Cargando...">
    <p><br/><img src="<?php echo $r ?>imagenes/loader.gif" style="height:60px; width:60px"/></p>
</div>
<div id="mydiv" class="ui-widget ui-widget-content ui-corner-all">
    <form id="form" name="form" action="index.php" method="post">
        <fieldset class="ui-widget ui-widget-content ui-corner-all">
            <legend class="text-center ui-widget ui-widget-header ui-corner-all">Login</legend>
            <div class="col-md-12 text-center">
                <label for="id">Usuario:</label>
            </div>
            <div class="col-md-12">
                <input type="text" name="id" class="form-control id validate[required] text-input"
                       title="Digite el usuario"/>
            </div>
            <div class="col-md-12 text-center">
                <label for="nombre">Contraseña:</label>
            </div>
            <div class="col-md-12">
                <input type="password" name="password" class="form-control password validate[required] text-input"
                       title="Digite el password"/>
            </div>
            <p class="boton">
                <button type="submit" class="btn btn-primary btninsertar" name="insertar" value="insertar">ingresar
                </button>
            </p>
        </fieldset>
    </form>
</div>
<?php
if (isset($error))
    echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>';
?>
</body>

</html>