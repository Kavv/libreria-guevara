<!-- Cabecera -->
<?php
require($r . 'incluir/src/head.php');
require($r . 'incluir/src/head-form.php');
?>

<link rel="stylesheet" type="text/css" href="<?php echo $r ?>incluir/datatables/media/css/jquery.dataTables_themeroller.css" />
<script type="text/javascript" src="<?php echo $r ?>incluir/datatables/media/js/jquery.dataTables.js"></script>

<script>
    /* PDF */
    if (validation_file(newSrc) == 200)
        $('#modal').html("<iframe src='" + newSrc + "' width='100%' height='100%'></iframe>");
    else
        $('#modal').html("<spam>El PDF no fue encontrado</spam>");

    /* Ambas Fechas o ninguna  */
    /* Valida que ambas fechas sean especificadas */
    $('#fecha1').change(function() {
        both_date();
    });

    $('#fecha2').change(function() {
        both_date();
    });

    function both_date() {
        if ($('#fecha2').val() != "" || $('#fecha1').val() != "") {
            $('#fecha1').attr("required", true);
            $('#fecha2').attr("required", true);
        } else {
            $('#fecha1').removeAttr("required", false);
            $('#fecha2').removeAttr("required", false);
        }
    }
</script>





<div class="row">
    <div class="col-md-12 col-lg-6">
        <button type="submit" class="btnconsulta btn btn-primary btn-block mt-2" name="consultar" value="Buscar">consultar</button> <!-- BOTON CONSULTAR -->
    </div>
    <div class="col-md-12 col-lg-6">
        <button type="submit" class="btnconsulta btn btn-primary btn-block mt-2" name="show-all" value="Mostrar-Todo">Mostrar Todo</button>
    </div>
</div>


<?php

/* Consulta dinamica sql */

/* Los parametros de la consulta sql se genera dinamicamente 
    en base a los datos recibidos para delimitar los resultados */
$parameters = [];
if ($empresa != "")
    array_push($parameters, "solempresa LIKE '%$empresa%'");
if ($id != "")
    array_push($parameters, "solid LIKE '$id'");
if ($cliente != "")
    array_push($parameters, "solcliente LIKE '%$cliente%'");
if ($asesor != "" && $asesor != "TODOS")
    array_push($parameters, "solasesor = '$asesor'");

if ($fecha1 != "")
    array_push($parameters, "solfecha BETWEEN '$fecha1' AND '$fecha2'");
if ($estado != "")
    array_push($parameters, "solestado = '$estado'");

// Consulta base
$sql = $con;
foreach ($parameters as $index => $parameter) {
    // Se agregan los parametros del WHERE
    if ($index == 0)
        $sql .= " WHERE " . $parameter;
    else
        $sql .= " AND " . $parameter;
}
// Se completa la consulta
$sql .= $ord;


?>


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

$("#usuario").selectpicker();
<select id="usuario" name="usuario" class="selectpicker" required data-live-search="true" title="Seleccione un usuario" data-width="100%">

    <!-- BOTON X PARA TODOS LOS MODALS -->
    <button type="button" class="ui-button ui-corner-all ui-widget ui-button-icon-only ui-dialog-titlebar-close" title="Close">
        <span class="ui-button-icon ui-icon ui-icon-closethick"></span>
        <span class="ui-button-icon-space"> </span>
    </button>


    <!-- ERROR MYSQL -->
    echo "<br><br>" . $db->errorInfo()[2];
    $id = $pdo->lastInsertId();
    <!-- Comentarios para buscar luego -->
    <!-- Evaluar en otro momento -->
    /* Evaluar en otro momento */
    <!-- ESTO REALMENTE IMPORTA? -->
    /* ESTO REALMENTE IMPORTA? */
    /* IMPORTANTE AJUSTAR */
    /* ELIMINAR */
    <!-- IMPORTANTE AJUSTAR -->
    /* OBSOLETO BY: KAVV */
<!-- Para la impresion directa debo acticar intl en php.ini -->



$usuario = $_SESSION['id'];
$msj = "INSERTO DATOS EN TESORERIA SIENDO UN ABONO CON LA SIGUIENTE INFORMACION NUM FACTURA $num_fac, PORCENTAJE DE ABONO $porcentaje_fi, VALOR FINAL CON ABONO $final, FECHA $fecha1, CONCEPTO $concepto, VALOR $valor, EMPRESA $empresa, PROVEEDOR $proveedor";
$ip =  $_SERVER['REMOTE_ADDR'];
$qrylogsregister = $db->query("INSERT INTO logs (id_usuario , ip_usuario , accion_usuario, fecha_accion) VALUES ( 
'$usuario', '$ip' , '$msj'  , NOW() );"); 





    <link rel="stylesheet" href="<?php echo $r.'incluir/librerias/fontawesome/css/all.css'?>" />
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.7.0/css/buttons.dataTables.min.css" />
    <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>

    
    <script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
    
    <script src="<?php echo $r.'incluir/kavv_js/kavvdt.js'?>" ></script>