<?php
// LA ACCION PRINCIPAL DE ESTA PAGINA ES INSERTAR UNA NUEVA EMPRESA

$r = '';
//INCLUIR SESION Y CONECCION
require($r . 'incluir/session.php');
require($r . 'incluir/connection.php');
require($r . 'incluir/phpmailer/PHPMailer/class.phpmailer.php');
require($r . 'incluir/phpmailer/PHPMailer/class.smtp.php');

$resid = $_GET['resid'];

$qrypri = $db->query("SELECT * FROM pqrs WHERE pqrsid = " . $resid . ";");
$rowpri = $qrypri->fetch(PDO::FETCH_ASSOC);

if ($_GET['responder']) { // VALIDAMOS QUE POST RESPONDER SEA TRUE
    $fecha1 = $_GET['fecha1'];
    $tipo = strtoupper(trim($_GET['tipo'])); // CAMBIAMOS EL TEXTO RECIBIDO A MAYUSCULA
    $area = $_GET['area'];
    $implicado = $_GET['implicado'];
    $comentarios = strtoupper(trim($_GET['comentarios'])); // CAMBIAMOS EL TEXTO RECIBIDO A MAYUSCULA
    $usuario = $_SESSION['id'];

    $qry = $db->query("INSERT INTO pqrs (pqrsfechasuceso, pqrstipo, pqrsarea, pqrsimplicado, pqrscomentarios, pqrsusuario) VALUES ('$fecha1', '$tipo', '$area', '$implicado', '$comentarios', '$usuario');"); // INSERTAMOS NUEVO USUARIO
    if ($qry) {

        $qryusuario = $db->query("SELECT * FROM directorio LEFT JOIN usuarios on usuid = dircedula WHERE dircedula = " . $usuario . ";");
        $rowusuario = $qryusuario->fetch(PDO::FETCH_ASSOC);

        $qryimplicado = $db->query("SELECT * FROM directorio LEFT JOIN usuarios on usuid = dircedula WHERE dircedula = " . $implicado . ";");
        $rowimplicado = $qryimplicado->fetch(PDO::FETCH_ASSOC);


        $body = '
		<h2>Respuesta PQRS ' . $rowimplicado["usunombre"] . '</h2>
		<p>
		<ul>
		<li>Fecha de Suceso = ' . $fecha1 . '</li>
		<li>Tipo = ' . $tipo . '</li>
		<li>Area = ' . $area . '</li>
		<li>Implicado = ' . $rowimplicado["dirnombre"] . '</li>
		<li>Gestionado por = ' . $rowusuario["dirnombre"] . '</li>
		</ul>
		</p>
		
		<p>' . $comentarios . '</p>
		<p style="font-size:11px;">los signos de puntuacion y caracteres especiales han sido omitidos de manera predeterminada para evitar inconvenientes con los diferentes servidores de correo</p>
		
		';

        $mail = new PHPMailer();
        $mail->IsSMTP(); // telling the class to use SMTP
        $mail->Host = "smtp.gmail.com"; // SMTP server
        $mail->SMTPDebug = 0;                     // enables SMTP debug information (for testing)
        // 1 = errors and messages
        // 2 = messages only
        $mail->SMTPAuth = true;                  // enable SMTP authentication
        $mail->SMTPSecure = "ssl";                 // sets the prefix to the servier
        $mail->Host = "smtp.gmail.com";      // sets GMAIL as the SMTP server
        $mail->Port = 465;                   // set the SMTP port for the GMAIL server
        $mail->Username = "no-reply@idencorp.co";  // GMAIL username
        $mail->Password = "IDT*2015!";            // GMAIL password
        $mail->SetFrom("no-reply@idencorp.co", "PQRS IDENCORP");
        $mail->AddReplyTo("no-reply@idencorp.co", "PQRS IDENCORP");
        $mail->Subject = "Respuesta Caso PQRS ";
        $mail->MsgHTML($body);
        $address = $rowimplicado['diremail'];
        $address_copy = $rowusuario['diremail'];
        $mail->AddAddress($address);
        $mail->AddCC($address_copy);
        if (!$mail->Send()) {
            $error = "Se respondio el caso pero no se pudo enviar el email por la siguiente causa: " . $mail->ErrorInfo;
            header('Location:pqrs.php?error=' . $error);
        } else {
            $mensaje = 'Se respondio correctamente el nuevo caso';
            header('Location:pqrs.php?mensaje=' . $mensaje);
        }
    } else {
        $error = 'No se puedo responder el caso porfavor contacte con el administrador';
        header('Location:pqrs.php?error=' . $error);
    }

}

$mensaje = $_GET['mensaje'];
$error = $_GET['error'];
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <!-- INSERTAR JQUERYS Y ESTILOS NECESARIAS PARA CORRECTO FUNCIONAMIENTO Y VISUALIZACION -->
    <?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
    ?>
    <script type="text/javascript" src="<?php echo $r ?>incluir/funciones.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#fecha').datepicker({ //VALIDACION DE CAMPO FECHA
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true,
                maxDate: "+0D",
            }).keypress(function (event) {
                event.preventDefault()
            });


            $('#fecha1').datepicker({ // VALIDACION DE LA FECHA UNO
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true,
            }).keypress(function (event) {
                event.preventDefault()
            });
        });
    </script>
    <!-- ESTILOS EMBEBIDOS PARA MANEJAR FORMULARIO INDEPENDIENTEMENTE -->
    <style>
        #form {
            width: 680px;
            margin: 20px auto
        }

        #form fieldset {
            padding: 10px;
            display: block
        }

        #form legend {
            font-weight: bold;
            margin-left: 5px;
            padding: 5px
        }

        #form label {
            display: inline-block;
            width: 130px;
            text-align: right;
            margin: 0.3em 2% 0 0
        }

        #form p {
            margin: 5px 0
        }
    </style>
</head>
<body>
<?php require($r . 'incluir/src/login.php') ?> <!-- INCLUIR BARRA DE SESION Y HORA (REQUERIDO) -->
<section id="principal">
    <?php require($r . 'incluir/src/cabeza.php') ?>
    <!-- INCLUIR CABEZA DEL DOCUMENTO = SECCION DONDE SE ALMACENA EL TITULO -->
    <?php require($r . 'incluir/src/menu.php') ?> <!-- INCLUIMOS MENU PRINCIPAL -->
    <article id="cuerpo">

        <article id="contenido">
            <form id="form" name="form" enctype="multipart/form-data" action="responderpqrs.php" method="get">
                <!-- FORMULARIO ENVIADO POR POST A SI MISMO -->
                <fieldset class="ui-widget ui-widget-content ui-corner-all col-md-6">
                    <legend class="ui-widget ui-widget-header ui-corner-all">Respuesta a PQRS</legend>

                    <input type="hidden" id="resid" name="resid" value="<?php echo $resid; ?>"/>

                    <center><p><?php echo $rowpri['pqrscomentarios'] ?></p></center>
                    <br><br>

                    <p>
                        <label for="fecha1">Fecha Suceso:</label> <!-- CAMPO FECHA-->
                        <input type="text" id="fecha1" name="fecha1" value="<?php echo $rowpri['pqrsfechasuceso'] ?>"
                               class="fecha validate[required]" readonly/>
                    </p>
                    <p>
                        <label for="tipo">Tipo:</label> <!-- CAMPO TIPO -->
                        <select name="tipo" class="validate[required]">
                            <option value="<?php echo $rowpri['pqrstipo'] ?>"> <?php echo $rowpri['pqrstipo'] ?></option>
                        </select>
                    </p>
                    <p>
                        <label for="area">Area:</label> <!-- CAMPO AREA -->
                        <select name="area" class="validate[required]">
                            <option value="<?php echo $rowpri['pqrsarea'] ?>"> <?php echo $rowpri['pqrsarea'] ?> </option>
                        </select>
                    </p>
                    <p>
                        <label for="implicado">Implicado:</label> <!-- CAMPO IMPLICADO -->
                        <select name="implicado" class="validate[required]">
                            <?php
                            $qry = $db->query("SELECT * FROM directorio where dircedula =  " . $rowpri['pqrsimplicado'] . " ORDER BY dirnombre");
                            while ($rowpri = $qry->fetch(PDO::FETCH_ASSOC))
                                echo '<option value=' . $rowpri['dircedula'] . '>' . $rowpri['dirnombre'] . '</option>';
                            ?>
                        </select>
                    </p>
                    <p>
                        <label for="comentarios">Comentarios:</label> <!-- CAMPO IMPLICADO -->
                        <textarea name="comentarios" rows="5" cols="60"
                                  class="validate[required] text-input"></textarea>
                    </p>
                    </br>
                    <p class="boton">
                        <button type="submit" class="btnresponder" name="responder" value="responder">Responder</button>
                        <!-- BOTON RESPONDER -->
                        <button type="button" class="btnatras" onClick="carga(); location.href='pqrs.php'">Atras
                        </button>
                    </p>
                </fieldset>
            </form>
        </article>
    </article>
    <?php require($r . 'incluir/src/pie.php') ?> <!-- BOTON PARA VOLVER A LA PARTE SUPERIOR -->
</section>
<?php
if (isset($error)) echo '<div id="dialog-message" title="Error"><span class="ui-icon ui-icon-circle-close" style="float:left; margin:3px 7px 7px 0;"></span>' . $error . '</div>'; //VENTANA MODAL ERROR
elseif (isset($mensaje)) echo '<div id="dialog-message" title="Correcto"><span class="ui-icon ui-icon-circle-check" style="float:left; margin:3px 7px 7px 0;"></span>' . $mensaje . '</div>'; //VENTANA MODAL EXITOSO
?>
</body>
</html>