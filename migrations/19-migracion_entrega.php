<?php
/***********
 * NORMALIZAMOS LOS DATOS DE LA TABLA entrega EN LA BD access_emundo
 * SE LIMPIAN DATOS BASURA
 * SE CORRIGEN LA RELACION DEL PRODUCTO VERDADERO CON EL REGISTRO EN entrega
 * EJEMPLO: EL LIBRO BIBLIA EN entrega TIENE ASIGNADO EL CODIGO 50 PERO EN LA TABLA productos EL VERDADERO CODIGO ES 25
 * SE CORRIGEN EN BASE EL CODIGO LOS REGISTROS EN entrega CON DESCRIPCION NULL
 * SE AJUSTAN MANUALMENTE GRAN CANTIDAD DE REGISTROS
 * 
 * 
 * ORDEN DE EJECUCIÓN:
 * Todos los registros de entrega que no tienen contratos son asignados al contrato auxiliar para no perder los datos 
 * http://localhost/emundo/migrations/19-migracion_entrega.php?sin_contrato=1
 * 
 * Todos los registrs de entrega que no tienen ni codigo ni descripcion (producto imposible de ubicar) se asignan a un producto auxiliar para no perder los datos
 * http://localhost/emundo/migrations/19-migracion_entrega.php?only_contrato=1
 * 
 * Modificación manual de registros
 * http://localhost/emundo/migrations/19-migracion_entrega.php?manual=1
 * Modificacion de registros unicos identificados por el nombre
 * http://localhost/emundo/migrations/19-migracion_entrega.php?unique=1
 * Modificiación de registros multiples en base al codigo
 * http://localhost/emundo/migrations/19-migracion_entrega.php?multiple=1&priority=1
 * Modificiación de registros multiples en base al nombre del producto
 * http://localhost/emundo/migrations/19-migracion_entrega.php?multiple=1
 * Modificación de registro que no tienen descripcion en base a codigo
 * http://localhost/emundo/migrations/19-migracion_entrega.php?description=1
 * Modificacion de registros con codigo = 0 e inserción de productos no identificados
 * http://localhost/emundo/migrations/19-migracion_entrega.php?no_identificado=1
 * 
 * 
 * UNA VEZ FINALICEMOS ESTE PROCESO DEBEMOS EJECUTAR NUEVAMENTE 17-migracion_productos.php, ya que los productos se ven afectados
 * 
 */

    $r = '../';
    
    require($r . 'incluir/connection-emundo2.php');
    require($r . 'incluir/migration_connection.php');

    $entregados = $db_access->query("SELECT * FROM Entrega");
    $entrega_nombre = $db_access->query("SELECT e.* FROM entrega e WHERE EXISTS ( SELECT 1 FROM productos p WHERE TRIM(p.NombreProducto) = TRIM(e.DESCRIPCION) ) AND e.CONTRATO IS NOT NULL AND e.DESCRIPCION IS NOT NULL ORDER BY e.CONTRATO ASC");
    $entrega_not_found = $db_access->query("SELECT e.* FROM entrega e WHERE NOT EXISTS ( SELECT 1 FROM productos p WHERE TRIM(p.NombreProducto) = TRIM(e.DESCRIPCION) ) AND e.CONTRATO IS NOT NULL AND e.DESCRIPCION IS NOT NULL ORDER BY e.CONTRATO ASC");
    $entrega_codigo = $db_access->query("SELECT * FROM entrega WHERE CONTRATO IS NOT NULL AND DESCRIPCION IS NULL AND CODIGO IS NOT NULL order BY CONTRATO ASC");

    $entregados_sin_contrato = $db_access->query("SELECT e.* FROM entrega e WHERE CONTRATO IS NULL OR CONTRATO = '' AND (CODIGO IS NOT NULL OR DESCRIPCION IS NOT NULL);");
    $entregados_only_contrato = $db_access->query("SELECT e.* FROM entrega e WHERE (DESCRIPCION IS NULL OR DESCRIPCION = '') AND (CODIGO IS NULL OR CODIGO = '');");

    echo "<br> Registros que coinciden: ".$entrega_nombre->rowCount();
    echo "<br> Registros que no coinciden: ".$entrega_not_found->rowCount();
    echo "<br> Registros con codigo sin nombre: ".$entrega_codigo->rowCount();
    echo "<br> TOTAL DE REGISTROS: ".( $entrega_nombre->rowCount() +$entrega_not_found->rowCount() +$entrega_codigo->rowCount());
    
    //echo "UPDATE entrega SET CODIGO = 100 WHERE DESCRIPCION = 'COD. 100 MI PRIMERA ENCICLOPEDIA' ";

    

    $new_product = [];
    $updates = [];
    $updates_duplicate = [];

    $show_unique = $result_multiple = $show_cero = $code_priority = $manual = $description_null = $no_identificados = $sin_contrato = $only_contrato = false;

    // http://localhost/emundo/migrations/19-migracion_entrega.php?manual=1
    if(isset($_GET['manual']))
    {
        $archivo = 'a-manual.sql';
        $manual = true;
    }   
    // http://localhost/emundo/migrations/19-migracion_entrega.php?unique=1
    if(isset($_GET['unique']))
    {
        $archivo = 'b-unicos.sql';
        $show_unique = true;
    }
    // http://localhost/emundo/migrations/19-migracion_entrega.php?multiple=1
    if(isset($_GET['multiple']))
    {
        $archivo = 'd-multiple_without_priority.sql';
        $result_multiple = true;
    }
    // El orden de ejecucion entre multiple y priority es importante ya que sobreescribe el nombre del archivo

    // http://localhost/emundo/migrations/19-migracion_entrega.php?multiple=1&priority=1
    if(isset($_GET['priority']))
    {
        $archivo = 'c-multiple_code_priority.sql';
        $code_priority = true;
    }

    // http://localhost/emundo/migrations/19-migracion_entrega.php?description=1
    if(isset($_GET['description']))
    {
        $archivo = 'e-descripcion_null.sql';
        $description_null = true;
    }

    // http://localhost/emundo/migrations/19-migracion_entrega.php?no_identificado=1
    // Este script ejecutarlo copiando y pegando el contenido en una consola sql en datagrip
    if(isset($_GET['no_identificado']))
    {
        $archivo = 'f-no_identificado.sql';
        $no_identificados = true;
    }

    // http://localhost/emundo/migrations/19-migracion_entrega.php?sin_contrato=1
    // Este script ejecutarlo copiando y pegando el contenido en una consola sql en datagrip
    if(isset($_GET['sin_contrato']))
    {
        $archivo = 'f-sin_contrato.sql';
        $sin_contrato = true;
    }


    // http://localhost/emundo/migrations/19-migracion_entrega.php?only_contrato=1
    // Este script ejecutarlo copiando y pegando el contenido en una consola sql en datagrip
    if(isset($_GET['only_contrato']))
    {
        $archivo = 'g-only_contrato.sql';
        $only_contrato = true;
    }

    

        /* echo "<br>";
        $x = $db_access->query("SELECT * FROM entrega WHERE DESCRIPCION = 'SANTA BIBLIA STAMPLEY REYNA VALERA 1960'");
        //echo $x->rowCount();
        foreach ($x as $valor) {
            var_dump($valor['DESCRIPCION']);
            exit();
        }
        exit(); */
    $contador = 0;


    // OBTENEMOS EL NUEVO CODIGO QUE SE ASIGNARA A LOS NUEVOS PRODUCTOS (REGISTROS IMPOSIBLES DE IDENTIFICAR)
    $last_code = $db_access->query("SELECT * FROM productos WHERE Código > 0 ORDER BY Código DESC");
    $last_code = $last_code->fetch(PDO::FETCH_ASSOC)['Código'];
    $new_code = $last_code + 1;



    $ruta_scripts = 'D:\Migracion-PC\Sistema EMUNDO\0-emundo-pg\19-migracion_entrega';
    $fh = fopen("$ruta_scripts/$archivo", 'w') or die("Se produjo un error al crear el archivo");
    $text_consultas = '';

    if($manual)
    {
        $text_consultas .= "\n"."UPDATE entrega SET CODIGO = 1073, DESCRIPCION = 'BIBLIA DEL MINISTRO' WHERE TRIM(DESCRIPCION) = '1073 BIBLIA DEL MINISTRO' AND CODIGO = 0;";
        $text_consultas .= "\n"."UPDATE entrega SET CODIGO = 1073, DESCRIPCION = 'BIBLIA DEL MINISTRO' WHERE TRIM(DESCRIPCION) = '1073 BIBLIA DEL MINISTRO' AND CODIGO IS NULL;";
        $text_consultas .= "\n"."UPDATE entrega SET CODIGO = 778, DESCRIPCION = 'SAGRADA BIBLIA CATOLICA' WHERE TRIM(DESCRIPCION) = 'BIBLIA CATOLICA - COD 1070' AND CODIGO = 0;";
        $text_consultas .= "\n"."UPDATE entrega SET CODIGO = 1065, DESCRIPCION = 'BIBLIA JERUSALEN COLOR AZUL' WHERE TRIM(DESCRIPCION) = '1065 BIBLIA JERUSALEM' AND CODIGO IS NULL;";
        $text_consultas .= "\n"."UPDATE entrega SET CODIGO = 1065, DESCRIPCION = 'BIBLIA JERUSALEN COLOR AZUL' WHERE TRIM(DESCRIPCION) = '1065 BILIA JERUSALEM' AND CODIGO IS NULL;";
        $text_consultas .= "\n"."UPDATE entrega SET CODIGO = 1071, DESCRIPCION = 'SANTA BIBLIA LETRA GRANDE CON REFERENCIA' WHERE TRIM(DESCRIPCION) = '1071 BIBLIA LETRA SUPER GIGANTE' AND CODIGO IS NULL;";
        $text_consultas .= "\n"."UPDATE entrega SET CODIGO = 1073, DESCRIPCION = 'BIBLIA DEL MINISTRO' WHERE TRIM(DESCRIPCION) = '1073 BI BLIA DEL MINISTRO' AND CODIGO IS NULL;";

        $text_consultas .= "\n"."UPDATE entrega SET CODIGO = 424, DESCRIPCION = 'MUNDO DE LAS HECHICERAS' WHERE TRIM(DESCRIPCION) = '424 HECHICERAS' AND CODIGO IS NULL;";
        $text_consultas .= "\n"."UPDATE entrega SET CODIGO = 652, DESCRIPCION = '1001 PREGUNTAS Y RESPUESTAS' WHERE TRIM(DESCRIPCION) = '652 UN MIL PREGUNTAS Y RESPUESTAS' AND CODIGO = 0;";
        $text_consultas .= "\n"."UPDATE entrega SET CODIGO = 808, DESCRIPCION = 'GUIA PARA LA SALUD - CELIAQUIA' WHERE TRIM(DESCRIPCION) = '808-GUIA PARA LA SALUD - CELIAQUIA' AND CODIGO = 0;";
        $text_consultas .= "\n"."UPDATE entrega SET CODIGO = 808, DESCRIPCION = 'GUIA PARA LA SALUD - CELIAQUIA' WHERE TRIM(DESCRIPCION) = '808-ODSEQUIO' AND CODIGO IS NULL;";
        $text_consultas .= "\n"."UPDATE entrega SET CODIGO = 981, DESCRIPCION = 'BIBLIA PESHITTA' WHERE TRIM(DESCRIPCION) = '981 BIBLIA PESHITTA' AND CODIGO IS NULL;";
        $text_consultas .= "\n"."UPDATE entrega SET CODIGO = 981, DESCRIPCION = 'BIBLIA PESHITTA' WHERE TRIM(DESCRIPCION) = '981 BIBLIA PESHITTA' AND CODIGO = 0;";
        $text_consultas .= "\n"."UPDATE entrega SET CODIGO = 819, DESCRIPCION = 'BIBLIA LAS AMERICAS' WHERE TRIM(DESCRIPCION) = 'BIBLIA AMERICA' AND CODIGO = 0;";
        $text_consultas .= "\n"."UPDATE entrega SET CODIGO = 819, DESCRIPCION = 'BIBLIA LAS AMERICAS' WHERE TRIM(DESCRIPCION) = 'BIBLIA DE AMERICA' AND CODIGO = 0;";
        $text_consultas .= "\n"."UPDATE entrega SET CODIGO = 819, DESCRIPCION = 'BIBLIA LAS AMERICAS' WHERE TRIM(DESCRIPCION) = 'BIBLIA DE AMERICA' AND CODIGO IS NULL;";
        $text_consultas .= "\n"."UPDATE entrega SET CODIGO = 936, DESCRIPCION = 'BIBLIA DE ESTUDIOS- ARCO IRIS' WHERE TRIM(DESCRIPCION) = 'BIBLIA ARCO IRIS' AND CODIGO = 0;";
        $text_consultas .= "\n"."UPDATE entrega SET CODIGO = 936, DESCRIPCION = 'BIBLIA DE ESTUDIOS- ARCO IRIS' WHERE TRIM(DESCRIPCION) = 'BIBLIA ARCOIRIS' AND CODIGO = 0;";
        
        $text_consultas .= "\n"."UPDATE entrega SET CODIGO = 744, DESCRIPCION = 'BIBLIA APOLOGETICA' WHERE TRIM(DESCRIPCION) = 'BIBLIA DE ESTUDIO APOLOGETICA' AND CODIGO = 0;";
        $text_consultas .= "\n"."UPDATE entrega SET CODIGO = 743, DESCRIPCION = 'BIBLIA HOLMAN' WHERE TRIM(DESCRIPCION) = 'BIBLIA DE ESTUDIO HOLMAN' AND CODIGO = 0;";
        $text_consultas .= "\n"."UPDATE entrega SET CODIGO = 743, DESCRIPCION = 'BIBLIA HOLMAN' WHERE TRIM(DESCRIPCION) = 'BIBLIA DE ESTUDIO HOLMAN' AND CODIGO IS NULL;";

        
        $text_consultas .= "\n"."UPDATE entrega SET CODIGO = 774, DESCRIPCION = 'BIBLIA DE ESTUDIO MACARTHUR' WHERE TRIM(DESCRIPCION) = 'BIBLIA DE ESTUDIO MACARTHURT' AND CODIGO = 0;";
        $text_consultas .= "\n"."UPDATE entrega SET CODIGO = 819, DESCRIPCION = 'BIBLIA LAS AMERICAS' WHERE TRIM(DESCRIPCION) = 'BIBLIA DE LA AMERICA' AND CODIGO = 0;";
        $text_consultas .= "\n"."UPDATE entrega SET CODIGO = 819, DESCRIPCION = 'BIBLIA LAS AMERICAS' WHERE TRIM(DESCRIPCION) = 'BIBLIA DE LA AMERICA' AND CODIGO IS NULL;";
        $text_consultas .= "\n"."UPDATE entrega SET CODIGO = 819, DESCRIPCION = 'BIBLIA LAS AMERICAS' WHERE TRIM(DESCRIPCION) = 'BIBLIA DE LAS AMERICA' AND CODIGO = 0;";
        $text_consultas .= "\n"."UPDATE entrega SET CODIGO = 819, DESCRIPCION = 'BIBLIA LAS AMERICAS' WHERE TRIM(DESCRIPCION) = 'BIBLIA DE LAS AMERICA' AND CODIGO IS NULL;"; 
        $text_consultas .= "\n"."UPDATE entrega SET CODIGO = 819, DESCRIPCION = 'BIBLIA LAS AMERICAS' WHERE TRIM(DESCRIPCION) = 'BIBLIA DE LAS AMERICAS' AND CODIGO = 0;";
        $text_consultas .= "\n"."UPDATE entrega SET CODIGO = 819, DESCRIPCION = 'BIBLIA LAS AMERICAS' WHERE TRIM(DESCRIPCION) = 'BIBLIA DE LAS AMERICAS' AND CODIGO IS NULL;";
        
        $text_consultas .= "\n"."UPDATE entrega SET CODIGO = 819, DESCRIPCION = 'BIBLIA LAS AMERICAS' WHERE TRIM(DESCRIPCION) = 'BIBLIA DE LAS AMERICS' AND CODIGO = 0;";
        $text_consultas .= "\n"."UPDATE entrega SET CODIGO = 819, DESCRIPCION = 'BIBLIA LAS AMERICAS' WHERE TRIM(DESCRIPCION) = 'BIBLIA EDICION DE LAS AMERICAS' AND CODIGO = 0;";
        $text_consultas .= "\n"."UPDATE entrega SET CODIGO = 744, DESCRIPCION = 'BIBLIA APOLOGETICA' WHERE TRIM(DESCRIPCION) = 'BIBLIA ESTUDIO APOLOGETICA' AND CODIGO = 0;";
        $text_consultas .= "\n"."UPDATE entrega SET CODIGO = 743, DESCRIPCION = 'BIBLIA HOLMAN' WHERE TRIM(DESCRIPCION) = 'BIBLIA ESTUDIO HOLMAN' AND CODIGO = 0;";
        $text_consultas .= "\n"."UPDATE entrega SET CODIGO = 1048, DESCRIPCION = 'BIBLIA LUZ EN MI CAMINO' WHERE TRIM(DESCRIPCION) = 'BIBLIA LUZ EN MI CAMINO NVI' AND CODIGO = 0;";
        $text_consultas .= "\n"."UPDATE entrega SET CODIGO = 706, DESCRIPCION = 'MI PRIMERA BIBLIA 3D 2V+CD Y ANTEOJOS 3D' WHERE TRIM(DESCRIPCION) = 'BIBLIA MI PRIMERA BIBLIA' AND CODIGO IS NULL;";


        $text_consultas .= "\n"."UPDATE entrega SET CODIGO = 989, DESCRIPCION = 'SANTA BIBLIA PARA REGALOS Y PREMIOS' WHERE TRIM(DESCRIPCION) = 'BIBLIA PREMIOS Y REGALOS' AND CODIGO IS NULL;";
        $text_consultas .= "\n"."UPDATE entrega SET CODIGO = 745, DESCRIPCION = 'BIBLIA LETRA SUPER GIGANTE' WHERE TRIM(DESCRIPCION) = 'BIBLIA SUPER GIGANTE' AND CODIGO = 0;";
        $text_consultas .= "\n"."UPDATE entrega SET CODIGO = 745, DESCRIPCION = 'BIBLIA LETRA SUPER GIGANTE' WHERE TRIM(DESCRIPCION) = 'BIBLIA SUPER GIGANTE HOLMAN' AND CODIGO = 0;";
        
        // Genera el archivo sql
        $texto = <<<_END
        $text_consultas
        _END;
        fwrite($fh, $texto) or die("No se pudo escribir en el archivo");
        fclose($fh);
        echo "<br><br>#FIN";
        
        exit();
    }
    // EJECUCION #4
    if($description_null)
    {
        while($row = $entrega_codigo->fetch(PDO::FETCH_ASSOC))
        {
            $org_code = $row['CODIGO'];
            $found = $db_access->query("SELECT * FROM productos WHERE Código = $org_code");
            if($found)
            {
                $product_name = $found->fetch(PDO::FETCH_ASSOC)['NombreProducto'];
                // Que tome en cuenta todos los registros incluyendo los 0 
                // (en su momento se considero que gran parte de los 0 deben ser errores pero de igual forma seran ingresados)
                if($org_code >= 0)
                {
                    //echo "<BR>#TIENE CODIGO<BR>";
                    //echo "<br>UPDATE entrega SET DESCRIPCION = '$product_name' WHERE TRIM(DESCRIPCION) = '$producto' AND CODIGO = $org_code; ";
                
                    $key = $org_code;
                    if(!isset($updates[$key]))
                    {
                        $updates[$key] = $key;
                        $text_consultas .= "\n"."UPDATE entrega SET DESCRIPCION = '$product_name' WHERE TRIM(DESCRIPCION) IS NULL AND CODIGO = $org_code; ";
                    }
                }
            }
            else
            {
                echo "NO SE ENCONTRO EL CODIGO $org_code DEL CONTRATO ". $row['CONTRATO'];
                exit();
            }
        }

        
        if($text_consultas != "")
        {
            // Genera el archivo sql
            $texto = <<<_END
            $text_consultas
            _END;
            fwrite($fh, $texto) or die("No se pudo escribir en el archivo");
            fclose($fh);
        }
        else
        echo "<br>No se encontro ningun registro";
        
        echo "<br>Finalizo correctamente";
        exit();
    }

    // EJECUCION #-1
    if($sin_contrato)
    {
        $text_consultas = "UPDATE entrega SET CONTRATO = 'PERDIDO' WHERE CONTRATO = '' OR CONTRATO IS NULL ";
    
        if($text_consultas != "")
        {
            // Genera el archivo sql
            $texto = <<<_END
            $text_consultas
            _END;
            fwrite($fh, $texto) or die("No se pudo escribir en el archivo");
            fclose($fh);
        }
        else
        echo "<br>No se encontro ningun registro";
        exit();
    }

    // EJECUCION #0
    if($only_contrato)
    {
        
        $text_consultas = "UPDATE entrega SET CODIGO = '-1', DESCRIPCION = 'PRODUCTO DESCONOCIDO' WHERE CONTRATO IS NOT NULL AND (CODIGO IS NULL OR CODIGO = '') AND (DESCRIPCION IS NULL OR DESCRIPCION = '') ";
    
        if($text_consultas != "")
        {
            // Genera el archivo sql
            $texto = <<<_END
            $text_consultas
            _END;
            fwrite($fh, $texto) or die("No se pudo escribir en el archivo");
            fclose($fh);
        }
        else
        echo "<br>No se encontro ningun registro";
        exit();
    }
    
    
    while($row = $entrega_not_found->fetch(PDO::FETCH_ASSOC))
    {
        $producto = trim($row['DESCRIPCION']);
        $contrato = $row['CONTRATO'];

        // COMENTARIAR TODO EL CONTENIDO DEL ELS SI QUIERES SOLO VER LOS PRODUCTOS QUE TIENEN CODIGO
        $unico = true;
        $found = $db_access->query("SELECT * FROM productos WHERE TRIM(NombreProducto) LIKE '%$producto%'");
        if($found->rowCount() > 0)
        {
            $data = $found->fetch(PDO::FETCH_ASSOC);
            $codigo = $data['Código'];
            $product_name = $data['NombreProducto'];
            if($found->rowCount() > 1)
                $unico = false;
            
            if($result_multiple && !$unico)
            {
                $org_code = $row['CODIGO'];
                // EJECUCION #2
                if($org_code > 0 && $code_priority)
                {
                    $found = $db_access->query("SELECT * FROM productos WHERE Código = $org_code");
                    $product_name = trim($found->fetch(PDO::FETCH_ASSOC)['NombreProducto']);
                    //echo "<br>UPDATE entrega SET DESCRIPCION = '$product_name' WHERE TRIM(DESCRIPCION) = '$producto' AND CODIGO = $org_code; ";
                

                    $key = $org_code."-".$producto;
                    if($contrato == "")
                        $key .= "-PERDIDO";
                    if(!isset($updates[$key]))
                    {
                        $updates[$key] = $producto;
                        
                        if($contrato != "")
                            $text_consultas .= "\n"."UPDATE entrega SET DESCRIPCION = '$product_name' WHERE TRIM(DESCRIPCION) = '$producto' AND CODIGO = $org_code; ";
                        else
                            $text_consultas .= "\n"."UPDATE entrega SET DESCRIPCION = '$product_name', CONTRATO = 'PERDIDO' WHERE (CONTRATO IS NULL OR CONTRATO = '') AND TRIM(DESCRIPCION) = '$producto' AND CODIGO = $org_code; ";
                    }
                }
                else
                {
                    // EJECUCION #3 - parcial porque el 0 se aplica con el #2 en caso de existir algun caso
                    if(!$code_priority || $org_code == 0 || $org_code == "")
                    {
                        $text_consultas .= "\n"."UPDATE entrega SET CODIGO = $codigo, DESCRIPCION = '$product_name' WHERE TRIM(DESCRIPCION) = '$producto'; ";
                    
                        if($contrato != "")
                            $text_consultas .= "\n"."UPDATE entrega SET CODIGO = $codigo, DESCRIPCION = '$product_name' WHERE TRIM(DESCRIPCION) = '$producto'; ";
                        else
                            $text_consultas .= "\n"."UPDATE entrega SET CODIGO = $codigo, DESCRIPCION = '$product_name', CONTRATO = 'PERDIDO' WHERE TRIM(DESCRIPCION) = '$producto' AND (CONTRATO IS NULL OR CONTRATO = ''); ";
                    }

                }
            }

            // EJECUCION #1
            if(!isset($updates[$producto]) && $unico && $show_unique)
            {
                $updates[$producto] = $producto;
                // MOSTRARA LAS ACTUALIZACIONES NECESARIAS EN BASE A LOS NOMBRES DE LOS PRODUCTOS NO IDENTIFICADOS
                // (LOS CUALES DEBEN REVISARSE CON CUIDADO)
                $text_consultas .= "\n"."UPDATE entrega SET CODIGO = $codigo, DESCRIPCION = '$product_name' WHERE TRIM(DESCRIPCION) = '$producto'; ";
            }
            
        }
        else
        {
            
            // EJECUCION #5
            if($no_identificados)
            {

                $org_code = $row['CODIGO'];
                if($org_code > 0)
                {
                    //echo "<BR>#TIENE CODIGO<BR>";
                    $found = $db_access->query("SELECT * FROM productos WHERE Código = $org_code");
                    if($found)
                    {
                        $product_name = $found->fetch(PDO::FETCH_ASSOC)['NombreProducto'];
                        //echo "<br>UPDATE entrega SET DESCRIPCION = '$product_name' WHERE TRIM(DESCRIPCION) = '$producto' AND CODIGO = $org_code; ";
                    
                        $key = $org_code."-".$producto;
                        if(!isset($updates[$key]))
                        {
                            $updates[$key] = $producto;
                            $text_consultas .= "\n"."UPDATE entrega SET DESCRIPCION = '$product_name' WHERE TRIM(DESCRIPCION) = '$producto' AND CODIGO = $org_code;"."\n";
                        }
                    }
                    else
                        echo "<br>NO SE ENCONTRO EL PRODUCTO CON EL CODIGO: $org_code";
                }
                else
                {
                    if(!isset($new_product[$producto]))
                    {
                        $new_product[$producto] = $producto;
                        //echo "No identificado $producto";
                        $text_consultas .= "\n"."INSERT INTO productos (Código, NombreProducto, DescripciónProducto, Vols) VALUES ($new_code, '$producto', 'AGREADO DURANTE EL DESARROLLO POR INCONSISTENCIA DE DATOS', 1);"."\n";
                        $new_code++;
                    }
                    $contador++;
                }
            }
        } 
    }
    
    echo "<br> CANTIDAD DE PRODUCTOS UNICOS NO IDENTIFICADOS: ". count($new_product);
    echo "<br> CANTIDAD DE REGISTROS NO IDENTIFICADOS: ". $contador;

    if($text_consultas != "")
    {
        // Genera el archivo sql
        $texto = <<<_END
        $text_consultas
        _END;
        fwrite($fh, $texto) or die("No se pudo escribir en el archivo");
        fclose($fh);
    }
    else
    echo "<br>No se encontro ningun registro";

    
    echo "<br>Finalizo correctamente";
    exit();

?>



