<?php
/******************************* 
 * AJUSTE A LOS REGISTROS DE productos EN LA BD MYSQL
 * 
 * EJECUTA:
 * http://localhost/emundo/migrations/17-migracion_productos_ajuste.php
********************************/ 

$r = '../';
    require($r . 'incluir/migration_connection.php');
    $productos = $db_access->query("SELECT * FROM productos");
?>
<!doctype html>
<html lang="es">

<head>
<?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
?>

</head>

<body>

    <div class="row">
        <div class="col-12">
            <?php 
                $ruta_scripts = 'D:\Migracion-PC\Sistema EMUNDO\0-emundo-pg\17-migracion_productos';
                $fh = fopen("$ruta_scripts/1-productos_ajustes.sql", 'w') or die("Se produjo un error al crear el archivo");
                $text_consultas = '';
                $aux_nombnre = 1;
                while($producto = $productos->fetch(PDO::FETCH_ASSOC))
                {
                    $codigo = $producto['Código'];
                    $nombre = str_replace("'", "", $producto['NombreProducto']);

                    if($nombre == '')
                    {
                        $nombre = "PRODUCTO SIN NOMBRE $aux_nombnre";
                        $text_consultas .= "\n " . "UPDATE productos SET NombreProducto = '$nombre' WHERE Código = $codigo";
                        $aux_nombnre++;
                    }
                }
                // Genera el archivo sql
                $texto = <<<_END
                $text_consultas
                _END;
                fwrite($fh, $texto) or die("No se pudo escribir en el archivo");
                fclose($fh);
                echo "<br><br>#FIN";
                exit();
            ?>
        </div>

    </div>

</body>

</html>


