<?php
/******************************* 
 * Si es la primer vez que migramos 'grupos' de la BD ACCESS, entonces en la base de datos del sistema ejecutamos
 * ALTER TABLE grupos ADD supervisor VARCHAR(30) NULL AFTER grunombre, ADD telefono VARCHAR(15) NULL AFTER supervisor, ADD digitador VARCHAR(30) NULL AFTER telefono, ADD created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER digitador;
********************************/ 

$r = '../';
    require($r . 'incluir/migration_connection.php');
    $grupos = $db_access->query("SELECT * FROM grupos");
?>
<!doctype html>
<html>

<head>
<?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
?>

</head>

<body>

    <div class="row">
        <div class="col-12">
            <?php 
                $ruta_scripts = 'D:\Migracion-PC\Sistema EMUNDO\0-emundo-pg\12-migracion_grupos';
                $fh = fopen("$ruta_scripts/grupos.sql", 'w') or die("Se produjo un error al crear el archivo");
                $text_consultas = '';
                $count = 1;
                while($grupo = $grupos->fetch(PDO::FETCH_ASSOC))
                {
                    $text_consultas .= "\n#" . $count . "\n";

                    $supervisor = $grupo['Supervisor'];
                    $nombre = $grupo['Código'];
                    $telefono = $grupo['Teléfono'];
                    $procesado = $grupo['Procesado'];
                    $digitador = $grupo['Digitador'];
                    $text_consultas .= "\n INSERT INTO grupos(grunombre, supervisor, telefono, digitador, created_at)
                    VALUES ('$nombre', '$supervisor', '$telefono', '$digitador', '$procesado');\n";
                    $count++;
                }
                // Genera el archivo sql
                $texto = <<<_END
                $text_consultas
                _END;
                fwrite($fh, $texto) or die("No se pudo escribir en el archivo");
                fclose($fh);

                echo "<br><br>#FIN";
                exit();
            ?>
        </div>

    </div>

</body>

</html>


