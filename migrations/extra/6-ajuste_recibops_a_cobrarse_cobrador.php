<?php
/***********
 * Esta consulta se realiza debido a que en el sistema access se guardan los nombres y no los codigos de los cobradores
 * Por lo tanto es necesario realizar el ajuste para que este funcione orientado al id del usuario
 * 
 * UPDATE recibos_a_cobrarse SET asignado = usuid FROM usuarios WHERE (usunombre = asignado AND usurelacionista = 1);
 * 
 * Ejecuta la siguiente consulta para identificar los registros que no lograron encontrar a su correspondiente cobrador
 * Select * from recibos_a_cobrarse as x where x.numero not in (select r.numero from usuarios inner join recibos_a_cobrarse r on usuid = r.asignado
 * where usurelacionista = 1
 * group by r.numero, usuid, usunombre) and x.cobrarse between date('2019-01-01') and date('2022-01-01');

 */

