<?php
/*****
 * GENERA EL DETALLE DE CADA UNA DE LAS CARTETAS
 * ESTO ES EQUIVALENTE A CREAR CADA N CANTIDAD DE REGISTRO DONDE ESPECIFICARE CADA CUOTA DE LA CARTERA
 * EJ: UNA CARTERA TIENE 4 CUOTAS, ENTONCES SE CREAN 4 REGISTROS CADA UNO EQUIVALENTE A 1 CUOTA
 * 
 * PARA REINICIAR TODO ESTE SCRIPT
 * DELETE FROM detcarteras where 1=1;
 * 
 * EJECUTAR
 * http://localhost/emundo/migrations/4-detallecarteras.php
 * 
 */
$r = '../';
    require($r . 'incluir/connection-emundo2.php');
    require($r . 'incluir/migration_connection.php');
    $carteras = $db->query("SELECT * FROM carteras INNER JOIN solicitudes ON carfactura = solfactura");
    $contratos = $db_access->query("SELECT * FROM clientes");
    //echo $db->query("SELECT * FROM clientes where Cédula IS NULL ")->rowCount();
?>
<!doctype html>
<html>

<head>
<?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
?>

</head>

<body>

    <div class="row">
        <div class="col-12">
        <?php
            $count = 0;
            while($cartera = $carteras->fetch(PDO::FETCH_ASSOC))
            {
                $cantidad_cuotras = $cartera['carncuota'];
                $empresa = $cartera['carempresa'];
                $factura = $cartera['carfactura'];
                $cuota = $cartera['carcuota'];
                $fecha = $cartera['solcompromiso'];
                $estado = 'ACTIVA';
                $created = $cartera['created_at'];

                for($i = 1; $i <= $cantidad_cuotras; $i++)
                {
                    $db->query("INSERT INTO detcarteras(dcaempresa, dcafactura, dcacuota, dcafecha, dcaestado, created_at)
                    VALUES ('$empresa', '$factura', '$i','$fecha','$estado','$created');") or die("ERROR $count");
                    
                    $fecha = date('Y-m-d', strtotime($fecha . ' next month'));
                    $count++;
                }
            }
            echo "<br>FIN";
        

        ?>
        </div>
    </div>
</body>
</html>
