<?php
/****************************************************
 * DATOS BASE PARA LA FUNCIONALIDAD DEL SISTEMA
 * EJECUTAR EN EL GESTOR DE BD
 * INSERT INTO tipdocumentos VALUES ('AE','AJUSTES DE INVENTARIO X ENTRADA','ENTINV');
 * INSERT INTO tipdocumentos VALUES ('AN','FACTURADAS ANULADAS','ENTFAC');
 * INSERT INTO tipdocumentos VALUES ('AS','AJUSTES DE INVENTARIO X SALIDA','SALINV');
 * INSERT INTO tipdocumentos VALUES ('AV','AVERIAS','SALINV');
 * INSERT INTO tipdocumentos VALUES ('DV','DEVOLUCIONES','ENTINV');
 * INSERT INTO tipdocumentos VALUES ('FC','FACTURA DE COMPRA','ENTINV');
 * INSERT INTO tipdocumentos VALUES ('FV','FACTURA DE VENTA','SALFAC');
 * INSERT INTO tipdocumentos VALUES ('NC','NOTA CREDITO','SALCAR');
 * INSERT INTO tipdocumentos VALUES ('ND','NOTAS DEBITO','ENTCAR');
 * INSERT INTO tipdocumentos VALUES ('NR','NOTA RECUPERACION','ENTCAR');
 * INSERT INTO tipdocumentos VALUES ('OP','ORDEN DE PRODUCCION','SALINV');
 * INSERT INTO tipdocumentos VALUES ('OT','ORDEN DE PRODUCCION TERMINADO','ENTINV');
 * INSERT INTO tipdocumentos VALUES ('RC','RECIBO DE CAJA','ENTFAC');
 * INSERT INTO tipdocumentos VALUES ('RCC','RECIBO DE CAJA CARTERA CASTIGADA','ENTCAR');
 * INSERT INTO tipdocumentos VALUES ('RP','REMISION DE PROVEEDOR','ENTINV');
 * INSERT INTO tipdocumentos VALUES ('SI','SALDOS INICIALES','ENTINV');
****************************************************/
