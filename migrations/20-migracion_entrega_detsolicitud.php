<?php
/***********
 * NORMALIZAMOS LOS DATOS DE LA TABLA entrega EN LA BD access_emundo
 * SE LIMPIAN DATOS BASURA
 * SE CORRIGEN LA RELACION DEL PRODUCTO VERDADERO CON EL REGISTRO EN entrega
 * EJEMPLO: EL LIBRO BIBLIA EN entrega TIENE ASIGNADO EL CODIGO 50 PERO EN LA TABLA productos EL VERDADERO CODIGO ES 25
 * SE CORRIGEN EN BASE EL CODIGO LOS REGISTROS EN entrega CON DESCRIPCION NULL
 * SE AJUSTAN MANUALMENTE GRAN CANTIDAD DE REGISTROS
 * 
 * SI AUN NO SE HA CREADO LA EXTENCION UNACCENT DEBEMOS EJECUTAR EL SIGUIENTE COMANDO
 * CREATE EXTENSION unaccent;
 * 
 * 
 * PARA REINICIAR EL SCRIPT ES:
 * DELETE FROM detmovimientos;
 * DELETE FROM detsolicitudes;
 * 
 * PARA EJECUTAR
 * http://localhost/emundo/migrations/20-migracion_entrega_detsolicitud.php?start=1&end=n&archivo=name
 * 
 */

    $r = '../';
    require($r . 'incluir/connection-emundo2.php');
    require($r . 'incluir/migration_connection.php');

    //$entregados = $db_access->query("SELECT * FROM entrega");
    $entrega_nombre = $db_access->query("SELECT e.* FROM entrega e WHERE EXISTS ( SELECT 1 FROM productos p WHERE TRIM(p.NombreProducto) = TRIM(e.DESCRIPCION) ) AND e.CONTRATO IS NOT NULL AND e.DESCRIPCION IS NOT NULL ORDER BY e.CONTRATO ASC");

    $insert = 0;
    $actualizar = 0;
    $not_found = 0;
    $start = 0;
    $end = 0;
    $i = 1;
    $archivo = "aux.sql";
    if(isset($_GET['start']))
        $start = $_GET['start'];
    if(isset($_GET['end']))
        $end = $_GET['end'];
    if(isset($_GET['archivo']))
        $archivo = $_GET['archivo'];

    $key = [];




    $ruta_scripts = 'D:\Migracion-PC\Sistema EMUNDO\0-emundo-pg\20-migracion_entrega_detsolicitud';
    $fh = fopen("$ruta_scripts/$archivo", 'w') or die("Se produjo un error al crear el archivo");
    $text_consultas = '';
    
    while($row = $entrega_nombre->fetch(PDO::FETCH_ASSOC))
    {
        if($i >= $start)
        {
            $contrato = trim($row['CONTRATO']);
            $producto = trim($row['DESCRIPCION']);
    
            $solicitud = $db->query("SELECT solid, solfactura, created_at FROM solicitudes WHERE TRIM(solid) = '$contrato'")->fetch(PDO::FETCH_ASSOC);
            if(!$solicitud)
            {
                $solicitud = $db->query("SELECT solid, solfactura, created_at FROM solicitudes WHERE TRIM(solid) = 'PERDIDO'")->fetch(PDO::FETCH_ASSOC);
                $not_found++;
                $contrato = $solicitud['solid'];
            }

            $row = $db->query("SELECT * FROM productos WHERE UNACCENT(TRIM(pronombre)) = UNACCENT('$producto')")->fetch(PDO::FETCH_ASSOC);
            if($row)
            {
    
                $codigo = $row['proid'];
                $empresa = 'F02900100885652';
                $key = "$empresa-$contrato-$codigo";
                $numero = $solicitud['solfactura'];
                $created_at = $solicitud['created_at'];

                if(!isset($unicos[$key]))
                {
                    $unicos[$key] = $key;
                    //$qry = $db->query("INSERT INTO detsolicitudes (detsolicitud, detempresa, detproducto, detcantidad, detprecio) VALUES ('$contrato', 'F02900100885652', '$codigo', 1, 0)");
                    $text_consultas .= "\n -- Detalle Solicitud $i";
                    $text_consultas .= "\n"."INSERT INTO detsolicitudes (detsolicitud, detempresa, detproducto, detcantidad, detprecio, created_at) VALUES ('$contrato', '$empresa', '$codigo', 1, 0, '$created_at');";
                    $text_consultas .= "\n -- Detalle Movimiento";
                    $text_consultas .= "\n"."INSERT INTO detmovimientos (dmoempresa, dmoprefijo, dmonumero, dmoproducto, dmocantidad, dmounitario, dmototal, dmoprecio, dmoptotal, created_at) VALUES ('$empresa', 'FV', '$numero', '$codigo', 1, 0, 0, 0, 0, '$created_at' );";
                
                    $insert++;
                }
                else
                {
                    $text_consultas .= "\n -- ACTUALIZACION";
                    $text_consultas .= "\n"."UPDATE detsolicitudes SET detcantidad = (detcantidad + 1) WHERE detempresa = '$empresa' AND detsolicitud = '$contrato' AND detproducto = '$codigo';";
                    $text_consultas .= "\n"."UPDATE detmovimientos SET dmocantidad = (dmocantidad + 1) WHERE dmoempresa = '$empresa' AND dmoprefijo = 'FV' AND dmonumero = '$numero' AND dmoproducto = '$codigo';";
                
                    $actualizar++;
                }
            }
            else
            {
                echo "<br>No se encontro el producto: $producto del contrato $contrato";
            }

            
            if($i >= $end)
            {
                        
                if($text_consultas != "")
                {
                    // Genera el archivo sql
                    $texto = <<<_END
                    $text_consultas
                    _END;
                    fwrite($fh, $texto) or die("No se pudo escribir en el archivo");
                    fclose($fh);
                }
                else
                echo "<br>No se encontro ningun registro";

                echo "<br>#TOTAL DE REGISTROS A INGRESARSE: $insert";
                echo "<br>#TOTAL DE REGISTROS A ACTUALIZARSE: $actualizar";
                echo "<br>#TOTAL DE CONTRATOS NO ENCONTRADOS: $not_found";
                echo "<br>#Ultimo registro recorrido: $i";
                echo "<br>#FIN";    
                exit();
            }
        }
        if($i >= $end)
        {
                    
            if($text_consultas != "")
            {
                // Genera el archivo sql
                $texto = <<<_END
                $text_consultas
                _END;
                fwrite($fh, $texto) or die("No se pudo escribir en el archivo");
                fclose($fh);
            }
            else
            echo "<br>No se encontro ningun registro";


            echo "<br>#TOTAL DE REGISTROS A INGRESARSE: $insert";
            echo "<br>#TOTAL DE REGISTROS A ACTUALIZARSE: $actualizar";
            echo "<br>#TOTAL DE CONTRATOS NO ENCONTRADOS: $not_found";
            echo "<br>#Ultimo registro recorrido: $i";
            echo "<br>#FIN";    
            exit();
        }
        $i++;
    }
                        
    if($text_consultas != "")
    {
        // Genera el archivo sql
        $texto = <<<_END
        $text_consultas
        _END;
        fwrite($fh, $texto) or die("No se pudo escribir en el archivo");
        fclose($fh);
    }
    else
    echo "<br>No se encontro ningun registro";


    echo "<br>#TOTAL DE REGISTROS A INGRESARSE: $insert";
    echo "<br>#TOTAL DE REGISTROS A ACTUALIZARSE: $actualizar";
    echo "<br>#TOTAL DE CONTRATOS NO ENCONTRADOS: $not_found";
    echo "<br>#Ultimo registro recorrido: $i";
    echo "<br>#FIN";    
    exit();

    



?>



