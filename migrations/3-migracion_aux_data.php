<?php 
/*********************************************************
 * ESTO SE LLEVA A CABO EN LA BD DE MYSQL
 * Registro auxiliar que guardara pagos y entregas que no tengan asignado un contrato
 * INSERT INTO clientes (Contrato, Clase, Cliente, Cédula, Dirección, CódigoCantón, Cantón, Provincia, `Telefono 1`, `Telefono 2`, Cónyugue, `Teléfono Conyuge`, `Fecha de contrato`, `Fecha de entrega`, Entregador, `fecha de cobro`, Fecha, `Monto del contrato`, Prima, Neto, `Número de cuotas`, `Monto Cuotas`, `Próxima Cuota`, Asesor, Descuento, Estado, `Estado texto`, Observaciones, Procesado, Digitador, Clasificacion, Cuenta, Razonamiento, Cobrador, `Lugar de trabajo`, `Lugar de trabajo Conyugue`, Estudiante, Escuela, Grado, `Referencia 1`, `Telefono referencia I`, `Referencia 2`, `Telefono referencia II`, Salida) VALUES ('PERDIDO', '1', 'PERDIDO', 'PERDIDO', 'PERDIDO', 501, 'PERDIDO', 'PERDIDO', null, null, null, null, '2021-02-02 00:00:00', '2021-02-02 00:00:00', 'PERDIDO', '2021-02-02 00:00:00', 2, 0.0000, 0.0000, 0.0000, 1, 0.0000, 0.0000, 'PERDIDO', 0.0000, '1', 'Activo', 'Contrato auxiliar para guardar todos los registros sin contratos', '2021-02-02 00:00:00', 'PERDIDO', 'A', '1', null, 'PERDIDO', null, null, null, null, null, null, null, null, null, 1);
 * 
 * Registro auxiliar que se asignara a los registros de entrega que sea imposible reconocer el producto
 * INSERT INTO productos (Código, NombreProducto, DescripciónProducto, Vols, IdCategoría, PrecioUnidad) VALUES (-1, 'PRODUCTO DESCONOCIDO', 'PRODUCTO AUXILIAR - MIGRACIONES', 1, null, null);
 * 
 * ACTUALIZAMOS LOS CONTRATOS CON NCUOTAS = 0 A NCUOTAS = 1, CON EL FIN DE QUE SE GENEREN CORRECTAMENTE LAS CARTERAS Y EL DET CARTERA.
 * UPDATE clientes SET `Número de cuotas` = 1 WHERE `Número de cuotas` = 0;
 * 
 * Agregamos otros registros auxiliares para los datos perdidos?
 * VENDEDOR
 * INSERT INTO vendedores (Procesado, Digitador, Nombre, Asesor, Teléfono, Domicilio, Grupo) VALUES ('2021-02-02 09:36:14', 'Admin', 'PERDIDO', 'PERDIDO', 0, 'MANAGUA', 'PERDIDO');
 * ENTREGADOR
 * INSERT INTO entregadores (PROCESADO, DIGITADOR, ENTREGADOR, CODIGO, Identidad) VALUES ('2021-02-02 09:38:35', 'ADMIN', 'PERDIDO', 'PERDIDO', null);
 * COBRADOR
 * INSERT INTO cobradores (Cobrador, Codigo, Identidad, PROCESADO, DIGITADOR) VALUES ('PERDIDO', 'PERDIDO', null, '2021-02-02 09:40:04', 'Admin');
 * 
 * 
**********************************************************/

