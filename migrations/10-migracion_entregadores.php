<?php
/******************************* 
 * Si es la primer vez que migramos entregadores de la BD ACCESS, entonces en la base de datos del sistema ejecutamos
 * ALTER TABLE mdespachos ADD codigo VARCHAR(10) NULL AFTER mdeestado, ADD created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER codigo;
 * ALTER TABLE mdespachos CHANGE mdeguia mdeguia TINYINT(4) NULL, CHANGE mdeestado mdeestado TINYINT(4) NULL;
 * ALTER TABLE mdespachos ADD digitador VARCHAR(30) NULL AFTER created_at;
 * 
 * Para reiniciar 
 * DELETE FROM mdespachos WHERE 1=1
 * 
 * EJECUTAR
 * http://localhost/emundo/migrations/10-migracion_entregadores.php

********************************/

$r = '../';
    require($r . 'incluir/migration_connection.php');
    $entregadores = $db_access->query("SELECT * FROM entregadores ORDER BY PROCESADO");
?>
<!doctype html>
<html>

<head>
<?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
?>

</head>

<body>

    <div class="row">
        <div class="col-12">
            <?php 
            $index_cedula = 1;
            $index_codigo = 1;

            $ruta_scripts = 'D:\Migracion-PC\Sistema EMUNDO\0-emundo-pg\10-migracion_entregadores';
            $fh = fopen("$ruta_scripts/entregadores.sql", 'w') or die("Se produjo un error al crear el archivo");
            $text_consultas = '';
            $count = 1;
            while($entregador = $entregadores->fetch(PDO::FETCH_ASSOC))
            {
                $text_consultas .= "\n#" . $count . "\n";
                $procesado = $entregador['PROCESADO'];
                $digitador = $entregador['DIGITADOR'];
                $nombre = $entregador['ENTREGADOR'];
                $codigo = $entregador['CODIGO'];
                $cedula = $entregador['Identidad'];
                if($cedula == "")
                {
                    $cedula = "sin-cedula".$index_cedula;
                    $index_cedula++;
                }
                if($codigo == "")
                {
                    $codigo = "entregador-emundo".$index_codigo;
                    $index_codigo++;
                }

                $text_consultas .= "\n INSERT INTO mdespachos(mdeid, mdenombre, mdeestado, cedula, created_at, digitador) 
                VALUES ('$codigo', '$nombre', 1, '$cedula', '$procesado', '$digitador');\n";
                
                $count++;
            }
            
            // Genera el archivo sql
            $texto = <<<_END
            $text_consultas
            _END;
            fwrite($fh, $texto) or die("No se pudo escribir en el archivo");
            fclose($fh);

            echo "<br><br>#FIN";
            exit();
            ?>
        </div>

    </div>

</body>

</html>


