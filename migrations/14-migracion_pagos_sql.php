<?php
/*************************************
 * Todos los registros: 339093
 * Registros con montos > 0: 338760
 * Diff: 333
 * 
 * Monto Total vendido/contatos: 254913230.27
 * Monto Total cobrado/pagos: 166906358.76
 * Diff: 88006871.51
 * Recibos NULL: 55431
 * Contratos no identificados: 
 * Pagos con numero de recibo duplicado: 6523
 * 
 * SI ALGO MALO PASA EN LA EJECUCIÓN PARA REINICIAR TODO LO MODIFICADO EJECUTE:
 * DELETE FROM movimientos where movprefijo = 'RC';
 * UPDATE detcarteras SET dcavalor = 0, dcasaldo = 0, dcafepag = null, dcapromotor = null where dcavalor != 0;
 * UPDATE carteras SET carsaldo = (cartotal-movdescuento) FROM movimientos where movdocumento = carfactura AND cartotal != carsaldo AND movprefijo = 'FV';
 * 
 * 
 * EJECUTA:
 * http://localhost/emundo/migrations/14-migracion_pagos_sql.php
 * 
 * 
 */



$r = '../';
    require($r . 'incluir/connection-emundo2.php');
    require($r . 'incluir/migration_connection.php');
    $pagos = $db_access->query("SELECT * FROM pagos ORDER BY Contrato, Fecha ASC");
    $contratos = $db->query("SELECT * FROM solicitudes");
    //echo $db->query("SELECT * FROM clientes where Cédula IS NULL ")->rowCount();
    echo "<br>INICIO<br>";
?>
<!doctype html>
<html lang="es">

<head>
<?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
?>

</head>

<body>

    <div class="row">
        <div class="col-12">
        <?php
            /* $todo = $pagos->rowCount();
            $count = $monto_cobrado = $monto_vendido = 0;
            while($pago = $pagos->fetch(PDO::FETCH_ASSOC))
            {
                if($pago['Contrato'] != "" && $pago['Monto'] > 0)
                {
                    $count++;
                    $monto_cobrado += $pago['Monto'];
                }
            }
            while($contrato = $contratos->fetch(PDO::FETCH_ASSOC))
            {
                if($contrato['Monto del contrato'] > 0)
                {
                    $monto_vendido += $contrato['Monto del contrato'];
                }
            }
            echo $todo;
            echo "<br>Total: ".$count;
            echo "<br>Diff: ".($todo - $count);
            echo "<br><br>Monto Total vendido: ".$monto_vendido;
            echo "<br>Monto Total cobrado: ".$monto_cobrado;
            echo "<br>Diff: ".($monto_vendido - $monto_cobrado); */
            /* echo "limpiamos";
            exit(); */
            
            $ruta_scripts = 'D:\Migracion-PC\Sistema EMUNDO\0-emundo-pg\14-migracion_pagos';
            $fh = fopen("$ruta_scripts/pagos.sql", 'w') or die("Se produjo un error al crear el archivo");
            $text_consultas = '';
            $count_registros = 1;

            $count = $monto_cobrado = $monto_vendido = 0;
            $total = 0;
            $negativo = -1;
            $no_identificados = 0;
            $consulta = $db->query("SELECT movnumero FROM movimientos WHERE movprefijo = 'RC' AND movnumero < 0 ORDER BY movnumero ASC;")->fetch(PDO::FETCH_ASSOC);
            if($consulta)
            {
                $negativo = $consulta['movnumero'] - 1;
            }
            $inicio = false;
            $index = 1;
            $start = 1;
            $stop = 100000;
            while($pago = $pagos->fetch(PDO::FETCH_ASSOC))
            {
                
                $text_consultas .= "\n#" . $count_registros . "\n";
                if($stop == $index)
                {
                    $texto = <<<_END
                    $text_consultas
                    _END;
                    fwrite($fh, $texto) or die("No se pudo escribir en el archivo");
                    fclose($fh);

                    echo "<br>NO SE ENCONTRARON $no_identificados CONTRATOS. <br>FIN";
                    echo "RANGO: $start - $index";
                    exit();
                }

                if($index == $start)
                    $inicio = true;
                
                //Las condiciones de contratos especifico, indican que deben omitirse los registros de ese contrato por algun motivo
                // El inicio define que ya se han recorrido los pagos de contratos anteriores y comenzamos a insertar nuevamente
                if($pago['Monto'] > 0 && $inicio)
                {
                    /* echo $pago['Contrato'] ;
                    exit(); */
                    $contrato = $pago['Contrato'];
                    $idpago = $pago['Numero'];
                    
                    $data = $db->query("SELECT solempresa, solfactura, solcliente, carcuota, carncuota, carsaldo 
                    FROM solicitudes
                    INNER JOIN carteras ON carfactura = solfactura
                    WHERE solid = '$contrato';")->fetch(PDO::FETCH_ASSOC);

                    if(!$data)
                    {
                        $data = $db->query("SELECT solempresa, solfactura, solcliente, carcuota, carncuota, carsaldo 
                        FROM solicitudes
                        INNER JOIN carteras ON carfactura = solfactura
                        WHERE solid = 'PERDIDO';")->fetch(PDO::FETCH_ASSOC);
                    }

                    if($data)
                    {
                        $solicitud = $contrato;
                        $empresa = $data['solempresa'];
                        $numero = $data['solfactura'];
                        $cliente = $data['solcliente'];
                        
                        $saldo = $data['carsaldo'];
                        $vcuota = $data['carcuota'];
                        
                        $aux_query = $db->query("SELECT dcacuota FROM detcarteras WHERE dcafactura='$numero' AND dcaempresa = '$empresa' AND dcaestado = 'ACTIVA' ORDER BY dcacuota ASC;");
                        //$num = $aux_query->rowCount();
                        $detalle_cartera = $aux_query->fetch(PDO::FETCH_ASSOC);
                        $overflow2 = false;
                        // Si retorna vacio, significa que es un overflow sobre una cartera que ya esta en overflow
                        if(!$detalle_cartera)
                        {
                            // Obtenemos la ultima cuota para hacer el proceso sobre ella misma generando un overflow sobre ella otra vez
                            $detalle_cartera = $db->query("SELECT dcacuota FROM detcarteras WHERE dcafactura='$numero' AND dcaempresa = '$empresa' AND dcaestado = 'CANCELADO' ORDER BY dcacuota DESC;")->fetch(PDO::FETCH_ASSOC);
                            $overflow2 = true;
                        }

                        if($detalle_cartera)
                        {
                            $cuota = $detalle_cartera['dcacuota'];
                            $vaplicar = $pago['Monto'];
                            $cobrador = $pago['Cobrador'];
                            
                            $created = $pago['Procesado'];
                            $descuento = 0;
                            // Valor original
                            $vaplicar_org = $vaplicar;
                            // Descuento original
                            $descuento_org = $descuento;
                            
                            $fecpago = $pago['Fecha'];
                            if($fecpago == "")
                                if($pago['Procesado'] != "")
                                    $fecpago = $pago['Procesado'];
                                else
                                    $fecpago = '2021-01-26'; // Fecha estatica ya que es obligatorio

                            // Forma de pago, en la bd definimos 1 como EFECTIVO
                            $fpago = 1;
    
                            // numeroaplica = Redondear hacia arriba((valor a pagarse + descuento)/ valor de la cuota)
                            /* $numapli = ceil(($vaplicar + $descuento) / $vcuota);
                            // Si la cantidad de cuotas activas es inferior al numeroaplica entonces retorna como un error
                            if ($num < $numapli) 
                            {
                                echo "<br>";
                                echo "<br>Error - FALTAN CUOTAS - Contrato: $contrato  Factura: $numero - idpago: " . $idpago . "<br>";
                                echo "<br>";
                                exit();
                            } */
                            //echo "<br>";
                            //$qry = $db->query("UPDATE clientes SET cliemail = '" . $_POST['email'] . "' WHERE cliid = '$cliente'");
                            $text_consultas .= "\n UPDATE carteras SET carsaldo = carsaldo - ($vaplicar + $descuento) WHERE carempresa = '$empresa' AND carfactura = '$numero' \n";
                            $recibo = $pago['Recibo'];
                            // Si el recibo es null
                            if($recibo == "")
                            {
                                //echo "<br>";
                                //echo "NO # DE RECIBO DEL CONTRATO $contrato con ID $idpago SE ASIGNA EL # $negativo";
                                //echo "<br>";
                                    
                                $recibo = $negativo;
                                $negativo--;
                            }
                            else
                            {
                                $recibo_duplicado = $db->query("SELECT movnumero FROM movimientos WHERE  movprefijo = 'RC' AND movnumero = $recibo")->fetch(PDO::FETCH_ASSOC);
                                if($recibo_duplicado)
                                {
                                    $recibo = $negativo;
                                    $negativo--;
                                }
                            }
                            $total = $vaplicar;
                            while ($vaplicar >= 0.01) {
                                $overflow = false;
                                if($overflow2)
                                {
                                    // Se incrementa en 1 la cuota para que en la siguiente consulta no se encuentre la cuota y se realice el proceso de overflow
                                    $cuota++;
                                }
                                //echo "SELECT * FROM detcarteras WHERE dcaempresa = '$empresa' AND dcafactura = '$numero' AND dcacuota = $cuota"."<br>";
                                $rowdet = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '$empresa' AND dcafactura = '$numero' AND dcacuota = $cuota")->fetch(PDO::FETCH_ASSOC);
                                if($rowdet)
                                {
                                    $acumulado = $rowdet['dcavalor'] + $rowdet['dcadescuento'];
                                }
                                else
                                {
                                    // Si entra aqui es por que no encontro la cuota, por lo tanto significa que aun hay un valor por aplicar superitor a 1
                                    // Esto indica un que lo que se pago es mayor al valor total a pagarse
                                    // Se forzara una edicion en la ultima cuota dejandola con valores negativos que demuestren el exceso de cobro
                                    $cuota--;
                                    $overflow = true;
                                    $rowdet = $db->query("SELECT * FROM detcarteras WHERE dcaempresa = '$empresa' AND dcafactura = '$numero' AND dcacuota = $cuota")->fetch(PDO::FETCH_ASSOC);
                                    if($rowdet)
                                    {
                                        $temp_valor = round($rowdet['dcavalor'], 2);
                                        $temp_saldo = round($rowdet['dcasaldo'], 2);
                                        $overflow_saldo = round(($temp_saldo - $vaplicar), 2);
                                        $overflow_valor = round(($temp_valor + $vaplicar), 2);
                                        $text_consultas .= "\n UPDATE detcarteras SET dcavalor = $overflow_valor , dcasaldo = $overflow_saldo, dcafepag = '$fecpago', dcapromotor = '$cobrador', dcaestado = 'CANCELADO' WHERE dcaempresa = '$empresa' AND dcafactura = '$numero' AND dcacuota = $cuota \n";
                                        $vaplicar = 0;
                                        $saldo = $overflow_saldo;
                                        break;
                                    }
                                    else
                                    {
                                        echo "ERROR DE OVERFLOW, NO SE PUDO REALIZAR LA ACCION - FACTURA: $numero CONTRATO: $contrato - IDPAGO: $idpago";
                                        exit();
                                    }
                                }
                                if(!$overflow)
                                {
                                    if ($acumulado == 0) {
                                        if ($vaplicar >= $vcuota) {
                                            $saldo = round($saldo - $vcuota, 2);
                                            //echo "$vaplicar >= $vcuota";
                                            //echo "<br>";
                                            $text_consultas .= "\n UPDATE detcarteras SET dcavalor = $vcuota, dcasaldo = $saldo, dcafepag = '$fecpago', dcapromotor = '$cobrador', dcaestado = 'CANCELADO' WHERE dcaempresa = '$empresa' AND dcafactura = '$numero' AND dcacuota = $cuota \n";
                                            $vaplicar = round(($vaplicar - $vcuota), 2);
                                            $cuota++;
                                        } else {
                                            $saldo = round($saldo - $vaplicar, 2);
                                            //echo "$vaplicar < $vcuota";
                                            //echo "<br>";
                                            $text_consultas .= "\n UPDATE detcarteras SET dcavalor = $vaplicar, dcasaldo = $saldo, dcafepag = '$fecpago', dcapromotor = '$cobrador' WHERE dcaempresa = '$empresa' AND dcafactura = '$numero' AND dcacuota = $cuota \n";
                                            $vaplicar = 0;
                                        }
                                    } else {
                                        if ($vaplicar >= ($vcuota - $acumulado)) {
                                            //echo "$vaplicar >= $vcuota - $acumulado";
                                            $saldo = round($saldo - ($vcuota - $acumulado), 2);
                                            //echo "<br>";
                                            $text_consultas .= "\n UPDATE detcarteras SET dcavalor = $vcuota - dcadescuento, dcasaldo = $saldo, dcafepag = '$fecpago', dcapromotor = '$cobrador', dcaestado = 'CANCELADO' WHERE dcaempresa = '$empresa' AND dcafactura = '$numero' AND dcacuota = $cuota \n";
                                            $vaplicar = round(($vaplicar - ($vcuota - $acumulado)), 2);
                                            $cuota++;
                                        } else {
                                            $saldo = round($saldo - $vaplicar, 2);
                                            //echo "$vaplicar < $vcuota";
                                            //echo "<br>";
                                            $text_consultas .= "\n UPDATE detcarteras SET dcavalor = dcavalor + $vaplicar, dcasaldo = $saldo, dcafepag = '$fecpago', dcapromotor = '$cobrador' WHERE dcaempresa = '$empresa' AND dcafactura = '$numero' AND dcacuota = $cuota \n";
                                            $vaplicar = 0;
                                        }
                                    }

                                }
                                else
                                {
                                    echo "SE TRATO DE HACER EL PROCESO SIENDO OVERFLOW - FACTURA: $numero CONTRATO: $contrato - IDPAGO: $idpago";
                                    exit();
                                }
                            }
                        }
                        else
                        {
                            echo "<br>";
                            echo "NO SE ENCONTRO DETALLE ACTIVO DE LA CARTERA CON FACTURA $numero DE LA EMPRESA $empresa - id del pago $idpago";
                            echo "<br>";
                            exit();
                        } 
                        //echo "INSERT INTO movimientos (movempresa, movprefijo, movnumero, movtercero, movdocumento, movfecha, movvalor, movdescuento, movsaldo, movfpago, movestado) VALUES ('$empresa', 'RC', '$recibo', '$cliente', '$numero', '$fecpago', $vaplicar_org, $descuento_org, $saldo, '$fpago', 'FINALIZADO')";
                        //echo "<br>";
                        $text_consultas .= "\n INSERT INTO movimientos (movempresa, movprefijo, movnumero, movtercero, movdocumento, movfecha, movvalor, movdescuento, movsaldo, movfpago, movestado, created_at) VALUES ('$empresa', 'RC', '$recibo', '$cliente', '$numero', '$fecpago', $vaplicar_org, $descuento_org, $saldo, '$fpago', 'FINALIZADO', '$created') \n";
                        //echo "<br> Saldo: ". $saldo;
                        
                        if ($saldo < 1 ) {
                            //echo "<br>";
                            $text_consultas .= "\n UPDATE carteras SET carestado = 'CANCELADO' WHERE carempresa = '$empresa' AND carfactura = '$numero' \n";
                        }

                        /* $rowsoli = $db->query("SELECT * FROM solicitudes where solid ='$solicitud' AND solempresa = '$empresa' ")->fetch(PDO::FETCH_ASSOC);
                        $asesorsoli = $rowsoli['solasesor'];
                        $empresasoli = $rowsoli['solempresa'];
                
                        $rowsoli = $db->query("SELECT * FROM usuarios WHERE usuid = '$asesorsoli'")->fetch(PDO::FETCH_ASSOC);
                        $qry = $db->query("UPDATE solicitudes SET solcomision = '" . $rowsoli['usucomision'] . "', solcomision1 = '" . $rowsoli['usucomision1'] . "', solcomision2 = '" . $rowsoli['usucomision2'] . "', solcomision3 = '" . $rowsoli['usucomision3'] . "', solover1 = '" . $rowsoli['usuover1'] . "', solpover1 = '" . $rowsoli['usupover1'] . "', solover2 = '" . $rowsoli['usuover2'] . "', solpover2 = '" . $rowsoli['usupover2'] . "', solover3 = '" . $rowsoli['usuover3'] . "', solpover3 = '" . $rowsoli['usupover3'] . "', solover4 = '" . $rowsoli['usuover4'] . "', solpover4 = '" . $rowsoli['usupover4'] . "' WHERE solempresa = '$empresasoli' AND solid = '$solicitud'");
                        
                        //Log de informacion que se le envia a American Logistic 
                        $qrylogsregister = $db->query("SELECT * FROM usuarios INNER JOIN perfiles ON usuperfil = perid WHERE usuid = '".$_SESSION['id']."'"); //verificacion usuario por ID de sesion
                        $rowlogsregister = $qrylogsregister->fetch(PDO::FETCH_ASSOC);	
                        $qrylogsregister = $db->query("INSERT INTO logs ( usuario, id_usuario , ip_usuario , accion_usuario, fecha_accion) VALUES ( '".$rowlogsregister['usunombre']."', '".$_SESSION['id']."', '".$_SERVER['REMOTE_ADDR']."' , ' SE GENERO RECIBO DE CAJA NUMERO  ".$recibo." - ".$empresa." A LA cuenta/FACTURA ".$row['movdocumento']." A NOMBRE DEL CLIENTE ".$row['clinombre']." ".$row['clinom2']." ".$row['cliape1']." ".$row['cliape2']." POR UN VALOR DE ".$row['movvalor']." QUEDANDO UN SALDO DE ".$row['movsaldo']."  ' , '".date("Y-m-d H:i:s")."' );"); //anexo de informacion de accion a la BD tabla LOGS

                        header("location:finalizar.php?".$filtro."&recibo=".$recibo."&empresa=".$empresa.'&activas='.$num);
                        exit(); */
                        
                    }
                    else
                    {
                        //echo "<br>";
                        $aux_contrato = $contrato;
                        if($contrato == "")
                            $aux_contrato = "VACIO";
                        echo "NO SE ENCONTRO EL CONTRATO $aux_contrato con ID $idpago";
                        //echo "<br>";
                        $no_identificados++;
                    }
                }
                $index++;
                $count_registros++;
            }

            
            // Genera el archivo sql
            $texto = <<<_END
            $text_consultas
            _END;
            fwrite($fh, $texto) or die("No se pudo escribir en el archivo");
            fclose($fh);

            echo "<br>NO SE ENCONTRARON $no_identificados CONTRATOS. <br>FIN";
            echo "TOTAL DE ITERACIONES: $index";

            exit();
            

        ?>
        </div>
    </div>
</body>
</html>