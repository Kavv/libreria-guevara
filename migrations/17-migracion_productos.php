<?php
/******************************* 
 * Si es la primer vez que migramos productos de la BD ACCESS, entonces en la base de datos del sistema ejecutamos
 * ALTER TABLE productos ADD categoria INT NULL AFTER prodesactivado;
 * INSERT INTO categorias (id, nombre) VALUES (-1, 'SIN categoria');
 * 
 * 
 * Si quieres reiniciar la informacion generada por el script debes ejecutar en la bd del sistem web:
 * DELETE FROM productos where 1=1;
 * 
 * Ejecutar:
 * http://localhost/emundo/migrations/17-migracion_productos.php?prev=1
 * Luego de ejecutar 19-migracion_entrega.php ejecutar nuevamente cambiando el parametro
 * http://localhost/emundo/migrations/17-migracion_productos.php?post=1
 * 
********************************/ 

$r = '../';
    require($r . 'incluir/migration_connection.php');
    $productos = $db_access->query("SELECT * FROM productos");
?>
<!doctype html>
<html>

<head>
<?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
?>

</head>

<body>

    <div class="row">
        <div class="col-12">
            <?php 
                $archivo = "aux.sql";
                if(isset($_GET['prev']))
                    $archivo = "2-productos_prev.sql";
                if(isset($_GET['post']))
                    $archivo = "3-productos_post.sql";

                $ruta_scripts = 'D:\Migracion-PC\Sistema EMUNDO\0-emundo-pg\17-migracion_productos';
                $fh = fopen("$ruta_scripts/$archivo", 'w') or die("Se produjo un error al crear el archivo");
                $text_consultas = '';
                $aux_nombnre = 1;
                while($producto = $productos->fetch(PDO::FETCH_ASSOC))
                {
                    $codigo = $producto['Código'];
                    $nombre = str_replace("'", "", $producto['NombreProducto']);
                    $descripcion = str_replace("'", "", $producto['DescripciónProducto']);
                    $vols = $producto['Vols'];
                    $categoria = $producto['IdCategoría'];
                    $precio = $producto['PrecioUnidad'];

                    if($precio == "")
                        $precio = 0;
                    if($vols == "")
                        $precio = 0;
                    if($categoria == "")
                        $categoria = 8;// el id de la categoria SIN categoria


                    $text_consultas .= "\n INSERT INTO productos(proid, pronombre, prodescr, procantidad, proprecio, proestado, protipo, prodesactivado, categoria)
                    VALUES ($codigo, '$nombre', '$descripcion', $vols, $precio , 'OK', 'PT', 0, $categoria);\n";
                    

                }
                // Genera el archivo sql
                $texto = <<<_END
                $text_consultas
                _END;
                fwrite($fh, $texto) or die("No se pudo escribir en el archivo");
                fclose($fh);
                echo "<br><br>#FIN";
                exit();
            ?>
        </div>

    </div>

</body>

</html>


