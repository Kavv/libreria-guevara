<?php
/***********
 * Agrega o actualizar los usuarios al sistema web emundo en base a los cobradores del sistema access
 * 
 * Si algo sale mal entonces:
 * DELETE FROM usuarios WHERE usurelacionista = 1;
 * 
 * EJECUTAR:
 * http://localhost/emundo/migrations/22-migracion_cobradores.php
 * 
 * Recuerda que una vez migraste todos los usuarios debes asignar el perfil de "SIN PERMISOS"
 * ejemplo UPDATE usuarios SET usuperfil = -1 where  usuperfil is null;
 * 
 */

    $r = '../';
    require($r . 'incluir/connection-emundo2.php');
    require($r . 'incluir/migration_connection.php');

    $cobradores = $db_access->query("SELECT * FROM cobradores ORDER BY Cobrador, PROCESADO ASC");
    

    $usuarios = [];
    $usu_aux;
    $count = 0;

    $ruta_scripts = 'D:\Migracion-PC\Sistema EMUNDO\0-emundo-pg\22-migracion_cobradores';
    $fh = fopen("$ruta_scripts/usuarios.sql", 'w') or die("Se produjo un error al crear el archivo");
    $text_consultas = '';

    while($row = $cobradores->fetch(PDO::FETCH_ASSOC))
    {
        $nombre = strupperEsp($row['Cobrador']);
        $codigo = $row['Codigo'];
        $telefono = '';
        $direccion = '';
        $grupo = '';
        $digitador = $row['DIGITADOR'];
        $created_at = $row['PROCESADO'];
        $empresa = 'F02900100885652';
        $cargo = 'Cobrador';

        $contra = sha1('Emundo');
        
        if($codigo == "")
        {
            usuario($nombre);

            $text_consultas .= "\n"."INSERT INTO usuarios(usuid, usunombre, usupassword, usurelacionista, usutelefono, usudireccion, usuGrupo, usuempresa, usucargo, digitador, created_at)
            VALUES ('$usu_aux', '$nombre', '$contra', 1, '$telefono', '$direccion', '$grupo', '$empresa', '$cargo', '$digitador', '$created_at');";
        }
        else
        {
            $duplicado = $db->query("SELECT usuid FROM usuarios WHERE usuid = '$codigo'")->fetch(PDO::FETCH_ASSOC);
            if($duplicado)
            {
                $text_consultas .= "\n"."UPDATE usuarios SET usurelacionista = 1 WHERE usuid = '$codigo'; ";
            }
            else
            {
                $text_consultas .= "\n"."INSERT INTO usuarios(usuid, usunombre, usupassword, usurelacionista, usutelefono, usudireccion, usuGrupo, usuempresa, usucargo, digitador, created_at)
                VALUES ('$codigo', '$nombre', '$contra', 1, '$telefono', '$direccion', '$grupo', '$empresa', '$cargo', '$digitador', '$created_at');";
            }
        }
        
        $count++;
    }

    if($text_consultas != "")
    {
        // Genera el archivo sql
        $texto = <<<_END
        $text_consultas
        _END;
        fwrite($fh, $texto) or die("No se pudo escribir en el archivo");
        fclose($fh);
    }
    else
    echo "<br>No se encontro ningun registro";

    echo "<br>#Total de usuarios $count";
    exit();    

    function usuario($nombre = "")
    {
        global $usu_aux;
        global $usuarios;
        $usu_aux = "";
        if($nombre != "")
        {
            $partes = explode(" ",$nombre);
            
            for($i = 0; $i < count($partes); $i++)
            {
                if($partes[$i] != "")
                {
                    $key = quitar_tildes($partes[$i]);
                    if(!isset($usuarios[$key]))
                    {
                        $usu_aux = $key;
                        $usuarios[$key] = $usu_aux;
                        break;
                    }
                }
            }
            if($usu_aux == "")
            {
                $valido = false;
                $index = 1;
                $index_name = 0;
                while(!$valido)
                {
                    if($partes[$index_name] != "")
                    {
                        $key = quitar_tildes($partes[$index_name]) . "-" . $index;
                        if(!isset($usuarios[$key]))
                        {
                            $usu_aux = $key;
                            $usuarios[$key] = $usu_aux;
                            $valido = true;
                        }
                        else
                        {
                            $index++;
                        }
                    }
                    else
                    {
                        $index_name++;
                    }
                }
            }
        }
        else
        {
            $valido = false;
            $index = 1;
            while(!$valido)
            {
                $key = "Usuario-" . $index;
                if(!isset($usuarios[$key]))
                {
                    $usu_aux = $key;
                    $usuarios[$key] = $usu_aux;
                    $valido = true;
                }
                else
                {
                    $index++;
                }
            }
        }
    }

    
    function quitar_tildes ($cadena)
    {
        $cadBuscar = array("á", "Á", "é", "É", "í", "Í", "ó", "Ó", "ú", "Ú", "ñ", "Ñ");
        $cadPoner = array("a", "A", "e", "E", "i", "I", "o", "O", "u", "U", "n", "N");
        $cadena = str_replace ($cadBuscar, $cadPoner, $cadena);
        return $cadena;
    }
?>



