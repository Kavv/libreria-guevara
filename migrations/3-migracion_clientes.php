<?php

/************************************************************************
*Clientes totales en la BD Access: 59049
*Clientes unicos: 53786 (excluye datos dupliacados en la bd Access y las cedulas que no son de longitud 14)
*Cedulas diferentes: 1271 (Cedulas con longitud diferente a 14)
*Clientes duplicados: 1858 (Cedulas duplicadas identificadas)
*Nulos: 2134 (Registros que no poseen cedula)
*total: 59049 (La suma de los datos encontrados)


* PARA REINICIAR TODO ESTE SCRIPT
* DELETE FROM clientes where 1=1;
* DELETE FROM solicitudes where 1=1;
* DELETE FROM movimientos where 1=1 and movprefijo = 'FV';
* DELETE FROM carteras where 1=1;
* DELETE FROM hissolicitudes where 1=1;

* ORDEN DE EJECUCUION:
* http://localhost/emundo/migrations/3-migracion_clientes.php?inicio=1&fin=70000&unicos=1&archivo=unicos.sql
* http://localhost/emundo/migrations/3-migracion_clientes.php?inicio=1&fin=70000&duplicados=1
* http://localhost/emundo/migrations/3-migracion_clientes.php?inicio=1&fin=70000&nulos=1

*************************************************************************/


$r = '../';
    require($r . 'incluir/connection-emundo2.php');
    require($r . 'incluir/migration_connection.php');
    $clientes = $db_access->query("SELECT * FROM clientes ORDER BY Contrato ASC");
    //echo $db->query("SELECT * FROM clientes where Cédula IS NULL ")->rowCount();
?>
<!doctype html>
<html>

<head>
<?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
?>

</head>

<body>

    <div class="row">
        <div class="col-12">
            <?php 
            $c_unique = [];
            $duplicados = [];
            $sin_cedula = [];
            //$c_diff_long = [];
            $nulos = 0;
            $i = 0;
            $i_d = 0;
            //PROCESO PARA ESPECIFICAR A PROFUNDIDAD LA INFORMACION DE LA TABLA CLIENTES DE ACCESS
            /* while($cliente = $clientes->fetch(PDO::FETCH_ASSOC))
            {
                if($cliente['Cédula'] != NULL)
                {
                    $cedula = trim($cliente['Cédula']);
                    $cedula = str_replace("-", "", $cedula);
                    $cedula = strtoupper($cedula);
                    $cedula_lon = strlen($cedula);
                    
                    if($cedula_lon == 14)
                    {
                        if(!isset($c_unique[$cedula]))
                        {
                            $c_unique[$cedula] = $cliente;
                        }
                        else
                        {
                            if(isset($duplicados[$cedula]))
                            {
                                array_push($duplicados[$cedula], $cliente);
                            }
                            else
                                $duplicados[$cedula] = [$cliente];
                            $i_d++;
                        }
                    }
                    else
                    {
                        if(!isset($c_diff_long[$cedula]))
                        {
                            $c_diff_long[$cedula] = $cliente;
                        }
                        else
                        {
                            if(isset($duplicados[$cedula]))
                            {
                                array_push($duplicados[$cedula], $cliente);
                            }
                            else
                                $duplicados[$cedula] = [$cliente];
                            $i_d++;
                        }
                    }
                }
                else
                {
                    $nulos++;
                }
                $i++;
            }
           
            echo "<br><br>Unicos: ".count($c_unique)
            ."<br><br>Long Diff: ".count($c_diff_long)
            ."<br><br>Duplicados: ".$i_d
            ."<br><br>Nulos: ".$nulos
            ."<br><br>total: ".($nulos + count($c_unique) + count($c_diff_long) + $i_d); 
            exit(); */


            /* 
                $aux = $db->query("SELECT * FROM solicitudes Where solfactura > 55000 ORDER BY solfactura DESC");
                $contar = 0;
                while($sol = $aux->fetch(PDO::FETCH_ASSOC))
                {
                    if($contar > 0)
                        if($sol['solfactura'] != $temp-1)
                            echo "<br>ERROR DE SECUENCIA<br>";

                        
                    $temp = $sol['solfactura'];
                    echo $sol['solid']."-".$sol['solfactura'];
                    echo "<br>";
                    $contar++;
                }
                exit(); 
            */
            
            
            /* $aux = $db->query("SELECT * FROM solicitudes RIGHT JOIN carteras ON carfactura = solfactura WHERE solfactura > 55000");
            //$aux = $db->query("SELECT * FROM carteras WHERE carfactura > 55000");
            //$aux = $db->query("SELECT * FROM solicitudes WHERE solid NOT IN (SELECT solid FROM solicitudes INNER JOIN carteras ON carfactura = solfactura WHERE solfactura > 55000)  AND solfactura > 55000");
            $contar = 0;
            while($sol = $aux->fetch(PDO::FETCH_ASSOC))
            {
                echo "<br>$contar<br>";
                echo $sol['solid'];
                $contar++;
            }
            exit(); */
  

            $text_consultas = '';
            
            /* Para borrar todo lo que genera este script
                DELETE FROM clientes where 1=1;
                DELETE FROM solicitudes where 1=1;
                DELETE FROM movimientos where 1=1;
                DELETE FROM carteras where 1=1;
                DELETE FROM hissolicitudes where 1=1;
            */
            
            while($cliente = $clientes->fetch(PDO::FETCH_ASSOC))
            {
                if($cliente['Cédula'] != NULL)
                {
                    $cedula = trim($cliente['Cédula']);
                    $cedula = str_replace("-", "", $cedula);
                    $cedula = strtoupper($cedula);
                    $cedula_lon = strlen($cedula);
                    
                    if(!isset($c_unique[$cedula]))
                    {
                        $c_unique[$cedula] = $cliente;
                    }
                    else
                    {
                        if(isset($duplicados[$cedula]))
                        {
                            array_push($duplicados[$cedula], $cliente);
                        }
                        else
                            $duplicados[$cedula] = [$cliente];
                        $i_d++;
                    }
                }
                else
                {
                    array_push($sin_cedula, $cliente);
                    $nulos++;
                }
                $i++;
            }
            echo "<br>";
            //var_dump($sin_cedula);
            
            $factura = 0;
            $count = 1;
            $inicio = 1;
            $ver_unicos = false;
            $ver_duplicados = false;
            $ver_nulos = false;
            $archivo = 'error';


            if(isset($_GET['inicio']))
            {
                $inicio = $_GET['inicio'];
                $factura = $inicio;
            }
            // http://localhost/emundo/migrations/3-migracion_clientes.php?inicio=1&fin=70000&unicos=1&archivo='unicos.sql';
            if(isset($_GET['unicos']))
            {
                $ver_unicos = true;
                $archivo = $_GET['archivo'];
            }
            // http://localhost/emundo/migrations/3-migracion_clientes.php?inicio=1&fin=70000&duplicados=1;
            if(isset($_GET['duplicados']))
            {
                $ver_duplicados = true;
                $archivo = 'duplicados.sql';
            }
            // http://localhost/emundo/migrations/3-migracion_clientes.php?inicio=1&fin=70000&nulos=1;
            if(isset($_GET['nulos']))
            {
                $ver_nulos = true;
                $archivo = 'nulos.sql';
            }

            $ruta_scripts = 'D:\Migracion-PC\Sistema EMUNDO\0-emundo-pg\3-migracion_clientes';
            $fh = fopen("$ruta_scripts/$archivo", 'w') or die("Se produjo un error al crear el archivo");
            
            if($ver_unicos)
            {
                $fin = count($c_unique);
                if(isset($_GET['fin']))
                    $fin = $_GET['fin'];
                    
                foreach ($c_unique as $cedula_temp => $cliente) {
                    if($count >= $inicio)
                    {
                        $text_consultas .= "\n#" . $count . "\n";
                        $nombre_completo = $cliente['Cliente'];
                        $nombre1 = "";
                        $nombre2 = "";
                        $apellido1 = "";
                        $apellido2 = "";
                        $key_dep = 0;
                        $key_can = 0;
                        asignar_nombre($nombre_completo);
                        
                        asignar_codigos($cliente['CódigoCantón'], $cliente['Cantón'], $cliente['Provincia']);
                            
                        if($key_dep == "")
                            $key_dep = 0;
                        if($key_can == "")
                            $key_can = 0;
    
                        // Limpiamos en todas las cadenas de texto las comillas simples en caso de existir
                        $nombre1 = str_replace("'", "", $nombre1);
                        $nombre2 = str_replace("'", "", $nombre2);
                        $apellido1 = str_replace("'", "", $apellido1);
                        $apellido2 = str_replace("'", "", $apellido2);
                        $direccion = substr(str_replace("'", "", $cliente['Dirección']),0, 99);
                        $tel1 = str_replace("'", "", $cliente['Telefono 1']);
                        $tel2 = str_replace("'", "", $cliente['Telefono 2']);
                        $conyugue = str_replace("'", "", $cliente['Cónyugue']);
                        $conyugue_tel = str_replace("'", "", $cliente['Teléfono Conyuge']);
                        $trabajo = substr(str_replace("'", "", $cliente['Lugar de trabajo']),0, 99);
                        $conyugue_trabajo = substr(str_replace("'", "", $cliente['Lugar de trabajo Conyugue']),0, 99);
                        $estudiante = str_replace("'", "", $cliente['Estudiante']);
                        $escuela = str_replace("'", "", $cliente['Escuela']);
                        $grado = str_replace("'", "", $cliente['Grado']);
                        $ref1 = substr(str_replace("'", "", $cliente['Referencia 1']),0, 99);
                        $ref1_tel = str_replace("'", "", $cliente['Telefono referencia I']);
                        $ref2 = substr(str_replace("'", "", $cliente['Referencia 2']),0, 99);
                        $ref2_tel = str_replace("'", "", $cliente['Telefono referencia II']);
                        $fecha_contrato = str_replace("'", "", $cliente['Fecha de contrato']);
                        $salida = str_replace("'", "", $cliente['Salida']);
                        $clase = $cliente['Clase'];
                        if($clase == "")
                            $clase = 0;
                        if($fecha_contrato == "")
                            $fecha_contrato = $cliente['Procesado'];
                        $procesado = $cliente['Procesado'];
                        if($procesado == "")
                            $procesado = $fecha_contrato;
    
                        $text_consultas .= "\n#CLIENTE\n";
                        // Generamos la insercion del cliente
                        $text_consultas .= "INSERT INTO clientes(clase, clinombre, clinom2, cliape1, cliape2, cliid, clidirresidencia, cliciuresidencia, clidepresidencia, clicelular, clitelresidencia, clinomfamiliar, clitelfamiliar, cliempresa, cliempfamiliar, clirefnombre1, clirefcelular1, clirefnombre2, clirefcelular2, created_at) 
                        VALUES ('$clase', '".$nombre1."', '".$nombre2."', '".$apellido1."', '".$apellido2."', '".$cliente['Cédula']."', '".$direccion."', '$key_can', $key_dep, '".$tel1."', '".$tel2."', '".$conyugue."', '".$conyugue_tel."', '".$trabajo."', '".$conyugue_trabajo."', '".$ref1."', '".$ref1_tel."', '".$ref2."', '".$ref2_tel."', '".$procesado."'); ";
                    
                        
                        $empresa = "F02900100885652";
                        $contrato = trim($cliente['Contrato']);
                        $asesor = ($cliente['Asesor']);
                        $monto_contrato = floatval($cliente['Monto del contrato']);
                        $prima = floatval($cliente['Prima']);
                        $n_cuotas = ($cliente['Número de cuotas']);
                        $cobrador = ($cliente['Cobrador']);
                        $f_cobro = ($cliente['fecha de cobro']);
                        $recoger_cobro = ($cliente['Fecha']);
                        $digitador = ($cliente['Digitador']);
                        $entregador = ($cliente['Entregador']);
                        $f_entrega = ($cliente['Fecha de entrega']);
                        $observaciones = str_replace(["'","<",">"], "", $cliente['Observaciones']);
                        $cuenta = ($cliente['Cuenta']);
                        $salida = ($cliente['Salida']);
                        if($salida == '')
                            $salida = 0;
                        if($cuenta == '')
                            $cuenta = 0;
                        if($f_cobro == '')
                            $f_cobro = $fecha_contrato;
                        if($f_entrega == '')
                            $f_entrega = $fecha_contrato;
                        if($recoger_cobro == '')
                            $recoger_cobro = 0;

                        
                        
                        $text_consultas .= "\n#SOLICITUD\n";
                        $text_consultas .= "INSERT INTO solicitudes(solempresa, solid, soltipo, solfecha, solcliente, solasesor, soltotal, solbase, solcuota, solncuota, solrelacionista, soldepentrega, solciuentrega, solentrega, soltelentrega, soldepcobro, solciucobro, solcobro, soltelcobro, solcompromiso, cobro, solestado, solfechreg, solusufac, solfechafac, solenvio, solfechdespacho, solfechdescarga, solfisico, solobservacion, created_at, digitador, cuenta, salida, solfactura, estudiante, escuela, grado )
                        VALUES ('$empresa', '$contrato', 'NORMAL', '$fecha_contrato', '". $cliente['Cédula'] ."', '$asesor', $monto_contrato, $monto_contrato, '$prima', '$n_cuotas', '$cobrador', '$key_dep', '$key_can', '$direccion', '$tel1', '$key_dep', '$key_can', '$direccion', '$tel1', '$f_cobro', '$recoger_cobro', 'ENTREGADO', '$fecha_contrato', '$digitador', '$fecha_contrato', '$entregador', '$f_entrega', '$f_entrega', 0, '$observaciones', '$procesado', '$digitador', '$cuenta', '$salida', $factura, '$estudiante', '$escuela', '$grado');";
                        
                        // Utilizo mov_numero solamente para representar que "movnumero" no es igual a movdocumento
                        // movnumero es el numero del movimiento es totalmente ageno a la # de factura. 
                        $mov_numero = $factura;

                        $descuento = round(floatval($cliente['Descuento']), 2);
                        $neto = round(floatval($cliente['Neto']), 2);
                        $saldo_cartera = round($neto - $descuento, 2);
                        
                        $text_consultas .= "\n#MOVIMIENTO\n";
                        // Se genera un movimiento que representa la factura
                        $text_consultas .= "INSERT INTO movimientos(movempresa, movprefijo, movnumero, movtercero, movdocumento, movfecha, movvalor, movdescuento, movsaldo, movestado, created_at) 
                        VALUES ('$empresa', 'FV', '$mov_numero', '". $cliente['Cédula'] ."', '$factura', '$fecha_contrato', $monto_contrato, $descuento, $neto, 'FACTURADO', '$procesado');";

                        // En caso de que el # de cuotas sea mayor a 0 entonces se genera una cartera
                        if($n_cuotas > 0)
                        {
                            $v_cuota = round(($neto/$n_cuotas), 2);
                            $f_castigada = "";
                            $estado_texto = "";
                            $estado = $cliente['Estado'];
                            switch ($estado) {
                                case 1:
                                    $estado_texto = "ACTIVA";
                                    break;
                                case 2:
                                    $estado_texto = "CANCELADO";
                                    break;
                                case 3:
                                    $estado_texto = "RECOLECCION";
                                    break;
                                case 4:
                                    $estado_texto = "PENDIENTE";
                                    break;
                                case 5:
                                    $estado_texto = "MOROSO";
                                    break;
                                case 6:
                                    $estado_texto = "RECOLECCION RAZONADA";
                                    break;
                                case 7:
                                    $estado_texto = "ILOCALIZABLE";
                                    break;
                                case 8:
                                    $estado_texto = "CASTIGADA";
                                    break;
                            }
                            $text_consultas .= "\n#CARTERA\n";
                            $text_consultas .= "INSERT INTO carteras(carempresa, carfactura, carcliente, cartotal, carcuota, carsaldo, carncuota, carfecha, carestado, created_at)
                            VALUES ('$empresa', $factura, '". $cliente['Cédula'] ."', $neto, $v_cuota, $saldo_cartera, $n_cuotas, '$fecha_contrato', '$estado_texto', '$procesado');";
                        }
                        $razonamiento = str_replace(["'","<",">"], "", $cliente['Razonamiento']);
                        // En caso de existir razonamiento se agrega como historial de la solicitud
                        if($razonamiento != "")
                        {
                            
                            $text_consultas .= "\n#HISTORIAL\n";
                            $text_consultas .= "INSERT INTO hissolicitudes(hsoempresa, hsosolicitud, hsofecha, hsousuario, hsonota) 
                            VALUES ('$empresa', '$contrato', '$fecha_contrato', '$digitador', '$razonamiento');";

                        }
                        $factura++;
                        
                    }
                    if($count == $fin)
                    {

                        // Genera el archivo sql
                        $texto = <<<_END
                        $text_consultas
                        _END;
                        fwrite($fh, $texto) or die("No se pudo escribir en el archivo");
                        fclose($fh);

                        echo "<br><br>#Registros desde el #" . $inicio . " hasta el #" . $fin;
                        exit();
                        break;
                        
                    }
                    
                    $count++;

                    // Consultas de apoyo para la creacion del producto final                   
                        //echo "INSERT INTO solicitudes (solid, solfecha, solfechdescarga, solenvio, solcompromiso, cobro, soltotal, solbase, solcuota, solncuota, solasesor, solobservacion, created_at, digitador, solrelacionista , cuenta, salida);";
                        // Contrato, --Fecha de contrato, Fecha de entrega, Entregador, fecha de cobro, Fecha, Monto del contrato, Monto del contrato, Prima, Número de cuotas, Asesor, Observaciones, procesado, digitador, Cobrador, cuenta, Salida
                        //echo "INSERT INTO carteras (cartotal, carcuota, carestado, created_at);";
                        // Neto, Monto Cuotas, Próxima Cuota, Descuento, Estado, Procesado,
                        //echo "INSERT INTO detcarteras (dcadescuento, );";
                        // Descuento, Procesado, digitador
                        //echo "INSERT INTO hissolicitudes (hsonota);";
                        // Razonamiento

                        //"(clase, clinombre, clinom2, cliape1, cliape2, cliid, clidirresidencia, cliciuresidencia, clidepresidencia, clicelular, clitelresidencia, clinomfamiliar, clitelfamiliar, cliempresa, cliempfamiliar, estudiante, escuela, grado, clirefnombre1, clirefcelular1, clirefnombre2, clirefcelular2, created_at, salida);"
                        //" . $nombre1 . " " .$nombre2 . " " .$apellido1 . " " .$apellido2 . " - " . $nombre_completo;
                    //Fin
                }
            }
            elseif($ver_duplicados)
            {
                // PARA EJECUTAR ESTE PROCESO EL SCRIPT LLAMADO unicos.sql DEBE HABER SIDO EJECUTADO EXITOSAMENTE

                //Optenemos la ultima factura ingresada
                $last_sol = $db->query("SELECT solfactura FROM solicitudes ORDER BY solfactura DESC LIMIT 1")->fetch(PDO::FETCH_ASSOC);
                $factura = $last_sol['solfactura'];

                $fin = $i_d;
                if(isset($_GET['fin']))
                    $fin = $_GET['fin'];
                foreach ($duplicados as $cedula_temp => $clientes) {
                    foreach ($clientes as $index => $cliente) {
                        if($count >= $inicio)
                        {
                            $text_consultas .= "\n#" . $count . "\n";
                            $nombre_completo = $cliente['Cliente'];
                            $nombre1 = "";
                            $nombre2 = "";
                            $apellido1 = "";
                            $apellido2 = "";
                            $key_dep = 0;
                            $key_can = 0;

                            asignar_nombre($nombre_completo);
                            
                            asignar_codigos($cliente['CódigoCantón'], $cliente['Cantón'], $cliente['Provincia']);

                            if($key_dep == "")
                                $key_dep = 0;
                            if($key_can == "")
                                $key_can = 0;
        
                            // Limpiamos en todas las cadenas de texto las comillas simples en caso de existir
                            $nombre1 = str_replace("'", "", $nombre1);
                            $nombre2 = str_replace("'", "", $nombre2);
                            $apellido1 = str_replace("'", "", $apellido1);
                            $apellido2 = str_replace("'", "", $apellido2);
                            $direccion = str_replace("'", "", $cliente['Dirección']);
                            $tel1 = str_replace("'", "", $cliente['Telefono 1']);
                            $tel2 = str_replace("'", "", $cliente['Telefono 2']);
                            $conyugue = substr(str_replace("'", "", $cliente['Cónyugue']), 0, 99);
                            $conyugue_tel = str_replace("'", "", $cliente['Teléfono Conyuge']);
                            $trabajo = substr(str_replace("'", "", $cliente['Lugar de trabajo']), 0, 99);
                            $conyugue_trabajo = substr(str_replace("'", "", $cliente['Lugar de trabajo Conyugue']), 0, 99);
                            $estudiante = str_replace("'", "", $cliente['Estudiante']);
                            $escuela = str_replace("'", "", $cliente['Escuela']);
                            $grado = str_replace("'", "", $cliente['Grado']);
                            $ref1 = substr(str_replace("'", "", $cliente['Referencia 1']), 0, 99);
                            $ref1_tel = str_replace("'", "", $cliente['Telefono referencia I']);
                            $ref2 = substr(str_replace("'", "", $cliente['Referencia 2']), 0, 99);
                            $ref2_tel = str_replace("'", "", $cliente['Telefono referencia II']);
                            $fecha_contrato = str_replace("'", "", $cliente['Fecha de contrato']);
                            $salida = str_replace("'", "", $cliente['Salida']);
                            
                            if($fecha_contrato == "")
                                $fecha_contrato = $cliente['Procesado'];
                            $procesado = $cliente['Procesado'];
                            if($procesado == "")
                                $procesado = $fecha_contrato;
                    
                            $empresa = "F02900100885652";
                            $contrato = trim($cliente['Contrato']);
                            $asesor = ($cliente['Asesor']);
                            $monto_contrato = floatval($cliente['Monto del contrato']);
                            $prima = floatval($cliente['Prima']);
                            $n_cuotas = ($cliente['Número de cuotas']);
                            $cobrador = ($cliente['Cobrador']);
                            $f_cobro = ($cliente['fecha de cobro']);
                            $recoger_cobro = ($cliente['Fecha']);
                            $digitador = ($cliente['Digitador']);
                            $entregador = ($cliente['Entregador']);
                            $f_entrega = ($cliente['Fecha de entrega']);
                            $observaciones = str_replace(["'","<",">"], "", $cliente['Observaciones']);
                            $cuenta = ($cliente['Cuenta']);
                            $factura++;
                            
                            if($salida == '')
                                $salida = 0;
                            if($cuenta == '')
                                $cuenta = 0;
                            if($f_cobro == '')
                                $f_cobro = $fecha_contrato;
                            if($f_entrega == '')
                                $f_entrega = $fecha_contrato;
                            if($recoger_cobro == '')
                                $recoger_cobro = 0;
                            
                            $text_consultas .= "\n#SOLICITUD\n";
                            $text_consultas .= "INSERT INTO solicitudes(solempresa, solid, soltipo, solfecha, solcliente, solasesor, soltotal, solbase, solcuota, solncuota, solrelacionista, soldepentrega, solciuentrega, solentrega, soltelentrega, soldepcobro, solciucobro, solcobro, soltelcobro, solcompromiso, cobro, solestado, solfechreg, solusufac, solfechafac, solenvio, solfechdespacho, solfechdescarga, solfisico, solobservacion, created_at, digitador, cuenta, salida, solfactura, estudiante, escuela, grado )
                            VALUES ('$empresa', '$contrato', 'NORMAL', '$fecha_contrato', '". $cliente['Cédula'] ."', '$asesor', $monto_contrato, $monto_contrato, '$prima', '$n_cuotas', '$cobrador', '$key_dep', '$key_can', '$direccion', '$tel1', '$key_dep', '$key_can', '$direccion', '$tel1', '$f_cobro', '$recoger_cobro', 'ENTREGADO', '$fecha_contrato', '$digitador', '$fecha_contrato', '$entregador', '$f_entrega', '$f_entrega', 0, '$observaciones', '$procesado', '$digitador', '$cuenta', '$salida', $factura, '$estudiante', '$escuela', '$grado');";
                            
                            // Utilizo mov_numero solamente para representar que "movnumero" no es igual a movdocumento
                            // movnumero es el numero del movimiento es totalmente ageno a la # de factura. 
                            $mov_numero = $factura;

                            $descuento = round(floatval($cliente['Descuento']), 2);
                            $neto = round(floatval($cliente['Neto']), 2);
                            $saldo_cartera = round($neto - $descuento, 2);
                            
                            $text_consultas .= "\n#MOVIMIENTO\n";
                            // Se genera un movimiento que representa la factura
                            $text_consultas .= "INSERT INTO movimientos(movempresa, movprefijo, movnumero, movtercero, movdocumento, movfecha, movvalor, movdescuento, movsaldo, movestado, created_at) 
                            VALUES ('$empresa', 'FV', '$mov_numero', '". $cliente['Cédula'] ."', '$factura', '$fecha_contrato', $monto_contrato, $descuento, $neto, 'FACTURADO', '$procesado');";

                            // En caso de que el # de cuotas sea mayor a 0 entonces se genera una cartera
                            if($n_cuotas > 0)
                            {
                                $v_cuota = ($neto/$n_cuotas);
                                $f_castigada = "";
                                $estado_texto = "";
                                $estado = $cliente['Estado'];
                                switch ($estado) {
                                    case 1:
                                        $estado_texto = "ACTIVA";
                                        break;
                                    case 2:
                                        $estado_texto = "CANCELADO";
                                        break;
                                    case 3:
                                        $estado_texto = "RECOLECCION";
                                        break;
                                    case 4:
                                        $estado_texto = "PENDIENTE";
                                        break;
                                    case 5:
                                        $estado_texto = "MOROSO";
                                        break;
                                    case 6:
                                        $estado_texto = "RECOLECCION RAZONADA";
                                        break;
                                    case 7:
                                        $estado_texto = "ILOCALIZABLE";
                                        break;
                                    case 8:
                                        $estado_texto = "CASTIGADA";
                                        break;
                                }
                                
                                $text_consultas .= "\n#CARTERA\n";
                                $text_consultas .= "INSERT INTO carteras(carempresa, carfactura, carcliente, cartotal, carcuota, carsaldo, carncuota, carfecha, carestado, created_at)
                                VALUES ('$empresa', $factura, '". $cliente['Cédula'] ."', $neto, $v_cuota, $saldo_cartera, $n_cuotas, '$fecha_contrato', '$estado_texto', '$procesado');";
                            }
                            $razonamiento = str_replace(["'","<",">"], "", $cliente['Razonamiento']);
                            // En caso de existir razonamiento se agrega como historial de la solicitud
                            if($razonamiento != "")
                            {
                                $text_consultas .= "\n#HISTORIAL\n";
                                $text_consultas .= "INSERT INTO hissolicitudes(hsoempresa, hsosolicitud, hsofecha, hsousuario, hsonota) 
                                VALUES ('$empresa', '$contrato', '$fecha_contrato', '$digitador', '$razonamiento');";
                            }
                            
                        }
                        if($count == $fin)
                            break;
        
                        $count++;
                    }
                    if($count == $fin+1)
                    {
                        // Genera el archivo sql
                        $texto = <<<_END
                        $text_consultas
                        _END;
                        fwrite($fh, $texto) or die("No se pudo escribir en el archivo");
                        fclose($fh);

                        echo "<br><br>#Registros desde el #" . $inicio . " hasta el #" . $fin;
                        exit();
                    }
                }
            }
            elseif($ver_nulos)
            {
                $fin = count($sin_cedula);
                if(isset($_GET['fin']))
                    $fin = $_GET['fin'];
                    
                //Optenemos la ultima factura ingresada
                $last_sol = $db->query("SELECT solfactura FROM solicitudes ORDER BY solfactura DESC LIMIT 1")->fetch(PDO::FETCH_ASSOC);


                if($last_sol)
                    $factura = $last_sol['solfactura'];
                else
                    $factura = 1;
                foreach ($sin_cedula as $index => $cliente) {
                    if($count >= $inicio)
                    {
                        $text_consultas .= "\n#" . $count . "\n";
                        $nombre_completo = $cliente['Cliente'];
                        $nombre1 = "";
                        $nombre2 = "";
                        $apellido1 = "";
                        $apellido2 = "";
                        $key_dep = 0;
                        $key_can = 0;
                        asignar_nombre($nombre_completo);
                        
                        asignar_codigos($cliente['CódigoCantón'], $cliente['Cantón'], $cliente['Provincia']);
                            
                        if($key_dep == "")
                            $key_dep = 0;
                        if($key_can == "")
                            $key_can = 0;
    
                        // Limpiamos en todas las cadenas de texto las comillas simples en caso de existir
                        $nombre1 = str_replace("'", "", $nombre1);
                        $nombre2 = str_replace("'", "", $nombre2);
                        $apellido1 = str_replace("'", "", $apellido1);
                        $apellido2 = str_replace("'", "", $apellido2);
                        $direccion = str_replace("'", "", $cliente['Dirección']);
                        $tel1 = str_replace("'", "", $cliente['Telefono 1']);
                        $tel2 = str_replace("'", "", $cliente['Telefono 2']);
                        $conyugue = substr(str_replace("'", "", $cliente['Cónyugue']), 0, 99);
                        $conyugue_tel = str_replace("'", "", $cliente['Teléfono Conyuge']);
                        $trabajo = substr(str_replace("'", "", $cliente['Lugar de trabajo']), 0, 99);
                        $conyugue_trabajo = substr(str_replace("'", "", $cliente['Lugar de trabajo Conyugue']), 0, 99);
                        $estudiante = str_replace("'", "", $cliente['Estudiante']);
                        $escuela = str_replace("'", "", $cliente['Escuela']);
                        $grado = str_replace("'", "", $cliente['Grado']);
                        $ref1 = substr(str_replace("'", "", $cliente['Referencia 1']), 0, 99);
                        $ref1_tel = str_replace("'", "", $cliente['Telefono referencia I']);
                        $ref2 = substr(str_replace("'", "", $cliente['Referencia 2']), 0, 99);
                        $ref2_tel = str_replace("'", "", $cliente['Telefono referencia II']);
                        $fecha_contrato = str_replace("'", "", $cliente['Fecha de contrato']);
                        $salida = str_replace("'", "", $cliente['Salida']);
                        $clase = $cliente['Clase'];
                        if($clase == "")
                            $clase = 0;
                        if($fecha_contrato == "")
                            $fecha_contrato = $cliente['Procesado'];
                        $procesado = $cliente['Procesado'];
                        if($procesado == "")
                            $procesado = $fecha_contrato;
                        
                        $cedula_temp = "SinCedula-$index";
                        $text_consultas .= "\n#CLIENTE\n";
                        $text_consultas .= "INSERT INTO clientes(clase, clinombre, clinom2, cliape1, cliape2, cliid, clidirresidencia, cliciuresidencia, clidepresidencia, clicelular, clitelresidencia, clinomfamiliar, clitelfamiliar, cliempresa, cliempfamiliar, clirefnombre1, clirefcelular1, clirefnombre2, clirefcelular2, created_at) 
                        VALUES ('$clase', '".$nombre1."', '".$nombre2."', '".$apellido1."', '".$apellido2."', '".$cedula_temp."', '".$direccion."', $key_can, $key_dep, '".$tel1."', '".$tel2."', '".$conyugue."', '".$conyugue_tel."', '".$trabajo."', '".$conyugue_trabajo."', '".$ref1."', '".$ref1_tel."', '".$ref2."', '".$ref2_tel."', '".$procesado."'); ";
                    
                        $empresa = "F02900100885652";
                        $contrato = trim($cliente['Contrato']);
                        $asesor = ($cliente['Asesor']);
                        $monto_contrato = floatval($cliente['Monto del contrato']);
                        $prima = floatval($cliente['Prima']);
                        $n_cuotas = ($cliente['Número de cuotas']);
                        $cobrador = ($cliente['Cobrador']);
                        $f_cobro = ($cliente['fecha de cobro']);
                        $recoger_cobro = ($cliente['Fecha']);
                        $digitador = ($cliente['Digitador']);
                        $entregador = ($cliente['Entregador']);
                        $f_entrega = ($cliente['Fecha de entrega']);
                        $observaciones = str_replace(["'","<",">"], "", $cliente['Observaciones']);
                        $cuenta = ($cliente['Cuenta']);
                        $salida = ($cliente['Salida']);
                        $factura++;

                        
                        if($salida == '')
                            $salida = 0;
                        if($cuenta == '')
                            $cuenta = 0;
                        if($f_cobro == '')
                            $f_cobro = $fecha_contrato;
                        if($f_entrega == '')
                            $f_entrega = $fecha_contrato;
                        if($recoger_cobro == '')
                            $recoger_cobro = 0;
                        
                        $text_consultas .= "\n#SOLICITUD\n";
                        $text_consultas .= "INSERT INTO solicitudes(solempresa, solid, soltipo, solfecha, solcliente, solasesor, soltotal, solbase, solcuota, solncuota, solrelacionista, soldepentrega, solciuentrega, solentrega, soltelentrega, soldepcobro, solciucobro, solcobro, soltelcobro, solcompromiso, cobro, solestado, solfechreg, solusufac, solfechafac, solenvio, solfechdespacho, solfechdescarga, solfisico, solobservacion, created_at, digitador, cuenta, salida, solfactura, estudiante, escuela, grado )
                        VALUES ('$empresa', '$contrato', 'NORMAL', '$fecha_contrato', '". $cedula_temp ."', '$asesor', $monto_contrato, $monto_contrato, '$prima', '$n_cuotas', '$cobrador', '$key_dep', '$key_can', '$direccion', '$tel1', '$key_dep', '$key_can', '$direccion', '$tel1', '$f_cobro', '$recoger_cobro', 'ENTREGADO', '$fecha_contrato', '$digitador', '$fecha_contrato', '$entregador', '$f_entrega', '$f_entrega', 0, '$observaciones', '$procesado', '$digitador', '$cuenta', '$salida', $factura, '$estudiante', '$escuela', '$grado');";
                        
                        // Utilizo mov_numero solamente para representar que "movnumero" no es igual a movdocumento
                        // movnumero es el numero del movimiento es totalmente ageno a la # de factura. 
                        $mov_numero = $factura;

                        $descuento = round(floatval($cliente['Descuento']), 2);
                        $neto = round(floatval($cliente['Neto']), 2);
                        $saldo_cartera = round($neto - $descuento, 2);
                        
                        $text_consultas .= "\n#MOVIMIENTO\n";
                        // Se genera un movimiento que representa la factura
                        $text_consultas .= "INSERT INTO movimientos(movempresa, movprefijo, movnumero, movtercero, movdocumento, movfecha, movvalor, movdescuento, movsaldo, movestado, created_at) 
                        VALUES ('$empresa', 'FV', '$mov_numero', '". $cedula_temp ."', '$factura', '$fecha_contrato', $monto_contrato, $descuento, $neto, 'FACTURADO', '$procesado');";

                        // En caso de que el # de cuotas sea mayor a 0 entonces se genera una cartera
                        if($n_cuotas > 0)
                        {
                            $v_cuota = ($neto/$n_cuotas);
                            $f_castigada = "";
                            $estado_texto = "";
                            $estado = $cliente['Estado'];
                            switch ($estado) {
                                case 1:
                                    $estado_texto = "ACTIVA";
                                    break;
                                case 2:
                                    $estado_texto = "CANCELADO";
                                    break;
                                case 3:
                                    $estado_texto = "RECOLECCION";
                                    break;
                                case 4:
                                    $estado_texto = "PENDIENTE";
                                    break;
                                case 5:
                                    $estado_texto = "MOROSO";
                                    break;
                                case 6:
                                    $estado_texto = "RECOLECCION RAZONADA";
                                    break;
                                case 7:
                                    $estado_texto = "ILOCALIZABLE";
                                    break;
                                case 8:
                                    $estado_texto = "CASTIGADA";
                                    break;
                            }
                            $text_consultas .= "\n#CARTERA\n";
                            $text_consultas .= "INSERT INTO carteras(carempresa, carfactura, carcliente, cartotal, carcuota, carsaldo, carncuota, carfecha, carestado, created_at)
                            VALUES ('$empresa', $factura, '". $cedula_temp ."', $neto, $v_cuota, $saldo_cartera, $n_cuotas, '$fecha_contrato', '$estado_texto', '$procesado');";
                        }
                        $razonamiento = str_replace(["'","<",">"], "", $cliente['Razonamiento']);
                        // En caso de existir razonamiento se agrega como historial de la solicitud
                        if($razonamiento != "")
                        {
                            $text_consultas .= "\n#HISTORIAL\n";
                            $text_consultas .= "INSERT INTO hissolicitudes(hsoempresa, hsosolicitud, hsofecha, hsousuario, hsonota) 
                            VALUES ('$empresa', '$contrato', '$fecha_contrato', '$digitador', '$razonamiento');";
                        }
                    }
                    if($count == $fin)
                    {
                        // Genera el archivo sql
                        $texto = <<<_END
                        $text_consultas
                        _END;
                        fwrite($fh, $texto) or die("No se pudo escribir en el archivo");
                        fclose($fh);

                        echo "<br><br>#Registros desde el #" . $inicio . " hasta el #" . $fin;
                        exit();
                    }
    
                    $count++;
                }
            }



            // Genera el archivo sql
            $texto = <<<_END
            $text_consultas
            _END;
            fwrite($fh, $texto) or die("No se pudo escribir en el archivo");
            fclose($fh);

            echo "<br><br>#FINAL: Registros desde el #" . $inicio . " hasta el #" . $fin;


            function asignar_nombre($nombre_completo)
            {
                $nombre_completo = ltrim($nombre_completo);
                global  $nombre1, $nombre2, $apellido1, $apellido2;
                $partes = explode(" ", $nombre_completo);
                for($i = 0; $i < count($partes); $i++)
                {
                    if($i == 0)
                        $nombre1 = $partes[$i];
                    if($i == 1)
                        $nombre2 = $partes[$i];
                    if($i == 2)
                        $apellido1 = $partes[$i];
                    if($i == 3)
                        $apellido2 = $partes[$i];
                    if($i > 3)
                        $apellido2 .= " ".$partes[$i];
                }
            }
            function asignar_codigos($codigo, $canton, $provincia)
            {
                global $key_dep, $key_can, $db;
                // Si no tienen codigo de canton
                if($codigo == 0)
                {
                    // Pero si tiene digitado el nombre del canton
                    if($canton != "")
                    {
                        // Consultamos si coincide alguna ciudad con el nombre digitado en canton
                        $existe = $db->query("SELECT ciuid, ciudepto FROM ciudades WHERE ciunombre = '$canton'")->fetch(PDO::FETCH_ASSOC);
                        if($existe)
                        {
                            // En caso de existir podemos extraer tanto la ciudad como el departamento
                            $key_can = $existe['ciuid'];
                            $key_dep = $existe['ciudepto'];
                        }
                    }
                    // En caso de que tampoco este digitado el nombre del canton
                    else
                    {
                        // Consultamos si esta digitado el nombre de la provincia
                        if($provincia != "")
                        {
                            // Consultamos si coincide algun departamento con el nombre digitado en provincia
                            $existe = $db->query("SELECT depid FROM departamentos WHERE depnombre = '$provincia'")->fetch(PDO::FETCH_ASSOC);
                            // En caso de existir solo podemos optener el departamento
                            if($existe)
                                $key_dep = $existe['depid'];
                        }
                    }
                }
                else
                {
                    // En caso de que exista el codigo entonces manipulamos el string
                    $key_dep = substr($codigo,0,-2);
                    $key_can = substr($codigo,-2);
                }
            }

            ?>
        </div>

    </div>

</body>

</html>
