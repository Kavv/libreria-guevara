<?php
/***********
 * Agrega los usuarios al sistema web emundo en base a los vendedores del sistema access
 * 
 * Si algo sale mal entonces:
 * DELETE FROM usuarios WHERE usuasesor = 1;
 * 
 * Antes de iniciar el proceso de migración debe insertar este registro
 * USUARIO ADMIN EN SISTEMA WEB EMUNDO
 * INSERT INTO public.usuarios (id, usuid, usunombre, usupassword, usuemail, usutelefono, usugenero, usuextension, usudireccion, usuperfil, usudepto, usuciudad, ususede, usudirectorcall, usucallcenter, usumodalidad, usujefecallcenter, usujefeasesor, usufechaingreso, usufecharetiro, ususalariocotizacion, usuauxiliotransporte, usutopeauxiliotransporte, usuempresa, usucargo, usucodigohelisa, usustaff, usudirector, usudirjefe, usuasesor, usupromotor, usugrupo, usurelacionista, usucomision, usucomision1, usucomision2, usucomision3, usuover1, usupover1, usuover2, usupover2, usuover3, usupover3, usuover4, usupover4, usujeferel, usupjeferel, usudirectorcorp, usucorporativo, usuarea, digitador, created_at) VALUES (830, '001-010197-0012K', 'Kevin Valverde', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'Kavv1997@hotmail.com', '82449347', 'M', null, 'Iglesia Rios de agua viva media cuadra al sur', 30, 'Manag', 'Manag', 'Manag', 123, 123, '123', '123456', '123465', null, null, null, null, null, null, null, null, 1, 1, '1', 1, 1, null, 1, 1.00, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2020-12-19 03:30:24.000000');
 * 
 * EJECUTAR:
 * http://localhost/emundo/migrations/21-migracion_vendedores.php
 * 
 * 
 * Recuerda que una vez migraste todos los usuarios debes asignar el perfil de "SIN PERMISOS"
 * ejemplo UPDATE usuarios SET usuperfil = -1 where  usuperfil is null;
 */

    $r = '../';
    require($r . 'incluir/connection-emundo2.php');
    require($r . 'incluir/migration_connection.php');

    $vendedores = $db_access->query("SELECT * FROM vendedores ORDER BY Nombre, Procesado ASC");
    

    $usuarios = [];
    $usu_aux;
    $count = 0;


    $ruta_scripts = 'D:\Migracion-PC\Sistema EMUNDO\0-emundo-pg\21-migracion_vendedores';
    $fh = fopen("$ruta_scripts/usuarios.sql", 'w') or die("Se produjo un error al crear el archivo");
    $text_consultas = '';

    while($row = $vendedores->fetch(PDO::FETCH_ASSOC))
    {
        $nombre = strupperEsp($row['Nombre']);
        $codigo = $row['Asesor'];
        $telefono = $row['Teléfono'];
        $direccion = str_replace(["'","<",">"], "", $row['Domicilio']);
        $grupo = $row['Grupo'];
        $digitador = $row['Digitador'];
        $created_at = $row['Procesado'];
        $empresa = 'F02900100885652';
        $cargo = 'Vendedor';

        $contra = sha1('Emundo');
        
        if($codigo == "")
        {
            usuario($nombre);

            $text_consultas .= "\n"."INSERT INTO usuarios(usuid, usunombre, usupassword, usuasesor, usutelefono, usudireccion, usuGrupo, usuempresa, usucargo, digitador, created_at)
            VALUES ('$usu_aux', '$nombre', '$contra', 1, '$telefono', '$direccion', '$grupo', '$empresa', '$cargo', '$digitador', '$created_at');";
        }
        else
        {
            // Correcto
            $text_consultas .= "\n"."INSERT INTO usuarios(usuid, usunombre, usupassword, usuasesor, usutelefono, usudireccion, usuGrupo, usuempresa, usucargo, digitador, created_at)
            VALUES ('$codigo', '$nombre', '$contra', 1, '$telefono', '$direccion', '$grupo', '$empresa', '$cargo', '$digitador', '$created_at');";
        }
        
        $count++;
    }


    if($text_consultas != "")
    {
        // Genera el archivo sql
        $texto = <<<_END
        $text_consultas
        _END;
        fwrite($fh, $texto) or die("No se pudo escribir en el archivo");
        fclose($fh);
    }
    else
    echo "<br>No se encontro ningun registro";

    echo "<br>#Total de usuarios $count";
    exit();    

    function usuario($nombre = "")
    {
        global $usu_aux;
        global $usuarios;
        $usu_aux = "";
        if($nombre != "")
        {
            $partes = explode(" ",$nombre);
            
            for($i = 0; $i < count($partes); $i++)
            {
                if($partes[$i] != "")
                {
                    $key = quitar_tildes($partes[$i]);
                    if(!isset($usuarios[$key]))
                    {
                        $usu_aux = $key;
                        $usuarios[$key] = $usu_aux;
                        break;
                    }
                }
            }
            if($usu_aux == "")
            {
                $valido = false;
                $index = 1;
                $index_name = 0;
                while(!$valido)
                {
                    if($partes[$index_name] != "")
                    {
                        $key = quitar_tildes($partes[$index_name]) . "-" . $index;
                        if(!isset($usuarios[$key]))
                        {
                            $usu_aux = $key;
                            $usuarios[$key] = $usu_aux;
                            $valido = true;
                        }
                        else
                        {
                            $index++;
                        }
                    }
                    else
                    {
                        $index_name++;
                    }
                }
            }
        }
        else
        {
            $valido = false;
            $index = 1;
            while(!$valido)
            {
                $key = "Usuario-" . $index;
                if(!isset($usuarios[$key]))
                {
                    $usu_aux = $key;
                    $usuarios[$key] = $usu_aux;
                    $valido = true;
                }
                else
                {
                    $index++;
                }
            }
        }
    }

    
    function quitar_tildes ($cadena)
    {
        $cadBuscar = array("á", "Á", "é", "É", "í", "Í", "ó", "Ó", "ú", "Ú", "ñ", "Ñ");
        $cadPoner = array("a", "A", "e", "E", "i", "I", "o", "O", "u", "U", "n", "N");
        $cadena = str_replace ($cadBuscar, $cadPoner, $cadena);
        return $cadena;
    }
?>



