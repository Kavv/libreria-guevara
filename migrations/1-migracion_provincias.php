<?php

$r = '../';
    require($r . 'incluir/migration_connection.php');
    $departamentos = $db_access->query("SELECT * FROM provincias");
?>
<!doctype html>
<html>

<head>
<?php
    require($r . 'incluir/src/head.php');
    require($r . 'incluir/src/head-form.php');
?>

</head>

<body>

    <div class="row">
        <div class="col-12">
            <?php 
            $key_dep = 0;
            $key_canon = 0;
            $deps = [];
            $canton = [];
            while($departamento = $departamentos->fetch(PDO::FETCH_ASSOC))
            {
                $key_dep = substr($departamento['Codigo'],0,-2);
                $key_can = substr($departamento['Codigo'],-2);
                if(isset($deps[$departamento['Provincia']]))
                {
                    array_push($deps[$departamento['Provincia']], [$departamento['Cantón'], $key_dep, $key_can]);
                }
                else
                {
                    $deps[$departamento['Provincia']] = [[$departamento['Cantón'], $key_dep, $key_can]];
                }
            }
            foreach ($deps as $depa => $depas) {
                $first = true;
                foreach ($depas as $key_c => $canton) {
                    if($first)
                    {
                        echo "INSERT INTO departamentos(depid, depnombre) VALUES ( $canton[1], '$depa');";
                        echo "<br><br>";
                        $first = false;
                    }
                    
                    echo "INSERT INTO ciudades(ciudepto, ciuid, ciunombre) VALUES ( $canton[1], '$canton[2]', '$canton[0]');";
                    echo "<br>";
                }
                echo "<hr style='border-top: 4px solid rgb(0 0 0)'>";
            }
            ?>
        </div>

    </div>

</body>

</html>